<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


class ClientCredentialsRepository extends EntityRepository
{
    /**
     * @param $phone
     * @return ClientCredentials|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByPhone($phone) {
        return $this->createQueryBuilder('u')
            ->where('u.phone LIKE :phone')
            ->setParameters([
                'phone' => $phone
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOldClients()
    {
        return $this->createQueryBuilder('c')
            ->where('c.card is not null')
            ->leftJoin('c.card', 'cc')
            ->setParameters([
//                'card' => "NULL"
            ])
            ->orderBy('cc.createdAt', "DESC")
            ->getQuery()
            ->getResult();
    }

    public function findNewClients()
    {
        return $this->createQueryBuilder('c')
            ->addSelect('cc')
            ->where('cc.id is null')
            ->leftJoin('c.card', 'cc')
            ->setParameters([
//                'card' => "NULL"
            ])
            ->getQuery()
            ->getResult();
    }

    public function countNewClients() {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c) as clCount')
            ->where('cc.id is null')
            ->leftJoin('c.card', 'cc')
            ->setParameters([
//                'card' => "NULL"
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
}
