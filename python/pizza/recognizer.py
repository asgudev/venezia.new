from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Activation, Dropout, Flatten, Dense
from tensorflow.python.keras.applications import VGG16
from tensorflow.python.keras.optimizers import Adam
from PIL import Image

from keras.models import model_from_json
import numpy as np
import os
import yaml
import MySQLdb
import MySQLdb.cursors
import json
import sys

CLASSES = ['Нет', 'Пицца Ассортита', 'Пицца Белорусская (закрытая)', 'Пицца Боскайола', 'Пицца Гавайская', 'Пицца Деревенская', 'Пицца Дьявола с маслинами', 'Пицца Каприччёза', 'Пицца Карбонара', 'Пицца Маргарита', 'Пицца Маринара', 'Пицца Ностромо', 'Пицца Прошутто', 'Пицца Три салями', 'Пицца Голоза', 'Пицца Кальцоне (закрытая)', 'Пицца с курицей и ананасом', 'Пицца Дзингара', 'Пицца Дьявола', 'Пицца Прошутто с грибами', 'Пицца с салями и луком', 'Пицца с курицей и грибами']


class Recognizer():
    vgg16_net = VGG16(weights='imagenet', include_top=False, input_shape=(224, 224, 3))

    vgg16_net.trainable = True
    trainable = False
    for layer in vgg16_net.layers:
        if layer.name == 'block5_conv1':
            trainable = True
        layer.trainable = trainable

    model = Sequential()
    model.add(vgg16_net)
    model.add(Flatten())
    model.add(Dense(768))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(CLASSES)))
    model.add(Activation('sigmoid'))
    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(),
                  metrics=['accuracy'])

    # json_file = open('pizza_modelvgg16.json', 'r')
    # loaded_model_json = json_file.read()
    # json_file.close()
    # model = model_from_json(loaded_model_json)
    model.load_weights('model_data/pizza_768_9031.hdf5')

    def recognize(self, image):
        resized_image = image.resize((224, 224), Image.ANTIALIAS)
        img_data = np.array(resized_image, dtype='float32')
        img_data /= 255
        img_data = np.expand_dims(img_data, axis=0)
        # img = image.load_img(input_image, target_size=(224, 224))

        predict = self.model.predict(img_data)
        result = dict(zip(CLASSES, predict[0].tolist()))
        sorted_result = sorted(result.items(), key=lambda x: x[1], reverse=True)
        return result
