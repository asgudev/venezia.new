<?php

namespace RestaurantBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopTimesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('times1', ShopTimeType::class, [
                'label' => 'Понедельник',
                'property_path' => '[1]',
            ])
            ->add('times2', ShopTimeType::class, [
                'label' => 'Вторник',
                'property_path' => '[2]',
            ])
            ->add('times3', ShopTimeType::class, [
                'label' => 'Среда',
                'property_path' => '[3]',
            ])
            ->add('times4', ShopTimeType::class, [
                'label' => 'Четверг',
                'property_path' => '[4]',
            ])
            ->add('times5', ShopTimeType::class, [
                'label' => 'Пятница',
                'property_path' => '[5]',
            ])
            ->add('times6', ShopTimeType::class, [
                'label' => 'Суббота',
                'property_path' => '[6]',
            ])
            ->add('times0', ShopTimeType::class, [
                'label' => 'Воскресенье',
                'property_path' => '[0]',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
//        $resolver->setDefaults(array(
//            'data_class' => 'RestaurantBundle\Entity\ShopTime',
//        ));
    }
}