<?php

namespace DishBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DishBundle\Entity\Ingredient;


/**
 * @package DishBundle\Entity
 * @ORM\Table(name="warehouse_ingredient")
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\IngredientInWarehouseRepository")
 */
class IngredientInWarehouse
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Ingredient")
     */
    private $ingredient;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Warehouse", inversedBy="ingredients")
     */
    private $warehouse;

    /**
     * @ORM\Column(name="quantity", type="float", nullable=true, options={"default" = 0})
     */
    private $quantity;

    public function __construct($ingredient, $warehouse)
    {
        $this->ingredient = $ingredient;
        $this->warehouse = $warehouse;
        $this->quantity = 0;
    }


    public function addQuantity($quantity) {
        $this->quantity += $quantity;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param mixed $ingredient
     * @return IngredientInWarehouse
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * @param mixed $warehouse
     * @return IngredientInWarehouse
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return IngredientInWarehouse
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }
}
