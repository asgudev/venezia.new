<?php

namespace UserBundle\EventListeners;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use UserBundle\Services\UserKey;


class KernelRequestListener
{
    private $container;
    private $token_storage;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, TokenStorageInterface $token_storage)
    {
        // assign value(s)
        $this->container = $container;
        $this->token_storage = $token_storage;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();
        if ($request->getRequestFormat() !== 'html') {
            return;
        }



        if ($this->token_storage->getToken()) {
            $user = $this->token_storage->getToken()->getUser();
            if (($user) && ($user != "anon.") && $user->getIs2FA()) {
                $twoFAPassed = $this->container->get("redis.helper")->get(UserKey::getRedisKey($user));
                $twoFARoute = $this->container->get("router")->generate("user_2fa");

                if (!$twoFAPassed && $event->getRequest()->getPathInfo() != $twoFARoute) {
                    $event->setResponse(new RedirectResponse($twoFARoute));
                }
            }
        }

        /*
                if ($this->token_storage->getToken()) {
                    $user = $this->token_storage->getToken()->getUser();
                    if (($user) && ($user != "anon.")) {
                        if ($request->getLocale() != $user->getDefaultLocale()) {
                            $params = $request->get('_route_params');
                            $params['_locale'] = $user->getDefaultLocale();

                            $event->setResponse(new RedirectResponse($this->container->get('router')->generate($request->get('_route'), $params) ));
                        }
                    }
                }*/


    }
}