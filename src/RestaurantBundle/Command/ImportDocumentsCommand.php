<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class ImportDocumentsCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('resto:import:document')
            ->setDescription('Import files entities')
            ->addOption('file', '-f', InputOption::VALUE_OPTIONAL, 'File', 0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try
        {
            $importName = $input->getOption('file');
            $importParser = $this->getContainer()->get('restaurant.import_parser');
            $xml = $importParser->loadXML($importName);
            $importParser->parseFile($importName);

            die();
        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}
