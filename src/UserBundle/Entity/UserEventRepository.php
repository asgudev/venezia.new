<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;

class UserEventRepository extends EntityRepository
{
    public function findAllEvents()
    {

    }

    /**
     * @return UserEvent[]
     */
    public function findTimesheetEventsInRange($dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('e')
            ->addSelect('u')
            ->where('e.date >= :dateStart')
            ->andWhere('e.date <= :dateEnd')
            ->andWhere('e.eventType < 100')
            ->leftJoin('e.user', 'u')
            ->setParameters([
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->orderBy('e.date', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /**
     * @param User $user
     * @return UserEvent|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastEvent(User $user) {
        return $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->setParameters([
                'user' => $user,
            ])
            ->orderBy("e.date", "DESC")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $shop
     * @return UserEvent|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastShopEvent($shop)
    {
        return $this->createQueryBuilder('e')
            ->where('e.shop = :shop')
            ->setParameters([
                'shop' => $shop
            ])
            ->orderBy("e.date", "DESC")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findReceiveEvents($start, $end)
    {
        return $this->createQueryBuilder('e')
            ->where('e.date >= :start')
            ->andWhere('e.date <= :end')
            ->andWhere("e.eventType = 101")
            ->setParameters([
                'start' => $start,
                'end' => $end
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $start
     * @param $end
     * @return UserEvent[]
     */
    public function findAllPizzaInDateRange($start, $end)
    {
        return $this->createQueryBuilder('e')
            ->where('e.user = 19327')
            ->andWhere('e.date >= :start')
            ->andWhere('e.date <= :end')
//            ->andWhere("e.eventType IN (0,1)")
            ->setParameters([
                'start' => $start,
                'end' => $end
            ])
            ->getQuery()
            ->getResult();

    }

    /**
     * @return UserEvent[]
     */
    public function findAfter(UserEvent $event)
    {

        return $this->createQueryBuilder('e')
            ->where('e.user = :user')
            ->andWhere('e.date > :date')
            ->setParameters([
                'user' => $event->getUser(),
                'date' => $event->getDate(),
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @return UserEvent[]
     */
    public function findAllInRange($shop, $dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('e')
            ->addSelect('u')
            ->addSelect('s')
            ->where('s.id = :shop')
            ->andWhere('e.date >= :dateStart')
            ->andWhere('e.date <= :dateEnd')
            ->andWhere('e.eventType < 100')
            ->leftJoin('e.user', 'u')
            ->leftJoin('e.shop', 's')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->orderBy('e.date', 'ASC')
            ->getQuery()
            ->getResult();
    }

}
