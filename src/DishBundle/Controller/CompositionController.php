<?php


namespace DishBundle\Controller;


use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;
use DishBundle\Entity\Composition;
use DishBundle\Form\Type\CompositionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/admin")
 */
class CompositionController extends Controller
{

    /**
     * @Route("/composition", name="composition_index")
     */
    public function indexAction(Request $request)
    {
        $compositions = $this->getDoctrine()->getRepository("DishBundle:Composition")->findAll();

        return $this->render("DishBundle:Composition:index.html.twig", [
            'compositions' => $compositions
        ]);
    }

    /**
     * @Route("/composition/search", name="search_composition")
     */
    public function searchCompositionAction(Request $request)
    {
        $compositions = $this->getDoctrine()->getRepository("DishBundle:Composition")->findByNameOrId($request->get("q"));

        $response = [];
        foreach ($compositions as $__composition) {
            $response[] = [
                "id" => $__composition->getId(),
                "text" => $__composition->getName(),
            ];
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/composition/new", name="create_composition")
     */
    public function createCompositionAction(Request $request)
    {
        $composition = new Composition();

        $form = $this->createForm(CompositionType::class, $composition);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $composition->setCreatedAt(new \DateTime());
                $composition->setCreatedBy($this->getUser());
                foreach ($composition->getIngredients() as $__ingr) {
                    $__ingr->setComposition($composition);
                    $em->persist($__ingr);
                }

                $em->persist($composition);
                $em->flush();

                return $this->redirectToRoute("edit_composition", [
                    'id' => $composition->getId(),
                ]);
            }
        }

        return $this->render("DishBundle:Composition:edit.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/composition/{id}/print_code", name="composition_print_code")
     */
    public function printCompositionCodeAction(Request $request, Composition $composition)
    {
        $options = new QROptions([
            'version' => 5,
            'outputType' => QRCode::OUTPUT_MARKUP_SVG,
            'eccLevel' => QRCode::ECC_L,
//            'dataMode' => $
            'quietzoneSize' => 0,
            'addQuietzone' => false,
            'svgDefs' => '
		<linearGradient id="g2">
			<stop offset="0%" stop-color="#39F" />
			<stop offset="100%" stop-color="#F3F" />
		</linearGradient>
		<linearGradient id="g1">
			<stop offset="0%" stop-color="#F3F" />
			<stop offset="100%" stop-color="#39F" />
		</linearGradient>
		<style>rect{shape-rendering:crispEdges}</style>',
            'moduleValues' => [
                1536 => 'url(#g1)', // dark (true)
                6 => '#fff', // light (false)
                2560 => 'url(#g1)',
                10 => '#fff',
                3072 => 'url(#g1)',
                12 => '#fff',
                3584 => 'url(#g1)',
                14 => '#fff',
                4096 => 'url(#g1)',
                16 => '#fff',
                1024 => 'url(#g2)',
                4 => '#fff',
                512 => 'url(#g1)',
                8 => '#fff',
                18 => '#fff',
            ],
        ]);
        $data = "https://venezia.by/dish/" . base64_encode($composition->getId() . "|" . $this->getUser()->getId(). "|" . $this->getUser()->getName() . "|" . time() . "|" . $_SERVER["REMOTE_ADDR"]);

        $img = (new QRCode($options))->render($data);
        return new Response($img);
    }

    /**
     * @Route("/composition/{id}/print", name="print_composition")
     */
    public function printCompositionAction(Request $request, Composition $composition)
    {
        if (!$composition)
            throw new NotFoundHttpException();

        $dish = $this->getDoctrine()->getRepository('DishBundle:Dish')->findOneBy([
            'composition' => $composition,
        ]);

        return $this->render("DishBundle:Composition:print.html.twig", [
            'composition' => $composition,
            'dish' => $dish,
        ]);
    }

    /**
     * @Route("/composition/{id}/delete", name="delete_composition")
     */
    public function deleteCompositionAction(Request $request, Composition $composition)
    {
        $composition->setIsDeleted(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($composition);
        $em->flush();


        return $request->isXmlHttpRequest() ? new JsonResponse() : $this->redirectToRoute("composition_index");
    }

    /**
     * @Route("/composition/{id}/edit", name="edit_composition")
     */
    public function editCompositionAction(Request $request, Composition $composition)
    {
        if (!$composition)
            throw new NotFoundHttpException();

        $form = $this->createForm(CompositionType::class, $composition);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $composition->setCreatedAt(new \DateTime());
                $composition->setCreatedBy($this->getUser());

                foreach ($composition->getIngredients() as $__ingr) {
//                    $__ingr->setComposition($composition);
                    if ($__ingr->getIngredient()->getId() == null) {
                        $newIngr = $__ingr->getIngredient();
                        $newIngr->setCreatedAt(new \DateTime());
                        $em->persist($newIngr);
                    }
                    $em->persist($__ingr);
                }


                $em->persist($composition);
                $em->flush();

                return $this->redirectToRoute("edit_composition", [
                    'id' => $composition->getId(),
                ]);
            }
        }

        $usedIn = $this->getDoctrine()->getRepository("DishBundle:Composition")->findUsedIn($composition);


        return $this->render("DishBundle:Composition:edit.html.twig", [
            'composition' => $composition,
            'usedIn' => $usedIn,
            "form" => $form->createView(),
        ]);
    }

}