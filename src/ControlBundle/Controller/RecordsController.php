<?php

namespace ControlBundle\Controller;

use ControlBundle\Entity\Task;
use ControlBundle\Entity\TaskComment;
use ControlBundle\Form\TaskCommentType;
use ControlBundle\Form\TaskType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UserBundle\Entity\User;

function convertArray($array) {
    $result = array();
    foreach ($array as $item) {
        $id = $item->getId();
        $result[$id] = $item;
    }
    return $result;
}

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class RecordsController extends Controller
{


    /**
     * @Route("/records/view/{session}", name="record_view")
     */
    public function recordsViewAction(Request $request, $session)
    {
        $app = $request->get("app") ?: 0;

        $conn = mysqli_connect("localhost", "records", "TmT8t2XwVi77FuLP", "records");
        $recordsQ = "SELECT * FROM `records` WHERE `session` = '".$session."' ORDER BY `date` asc";
        $recordsData = mysqli_query($conn, $recordsQ);
        $data = [];
        $events = [];
        $spool_path = $this->get('kernel')->getRootDir() . '/../var/records/';
        while ($row = mysqli_fetch_assoc($recordsData)) {
            $f = file_get_contents($spool_path . $row["record_file"]);
            $data = json_decode($f, true);

//var_dump($data["events"]);
//            $events = array_merge($events, $data["events"]);
            $events = array_merge($events, $data);
        }


         return $this->render('@Control/Records/view.html.twig', [
            'events' => json_encode($app == 0 ? $events["events"] : $events),
        ]);
    }

    /**
     * @Route("/records/list", name="records_list")
     */
    public function recordsListAction(Request $request)
    {

        $app = $request->get("app") ? "user = 2" : "user != 2";

        $conn = mysqli_connect("localhost", "records", "TmT8t2XwVi77FuLP", "records");

        $recordsQ = "SELECT * FROM `records` WHERE $app group by `session` order by `date` desc";
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        $users = convertArray($users);
        $recordsData = mysqli_query($conn, $recordsQ);
        $records = [];

        while ($row = mysqli_fetch_assoc($recordsData)) {
            $row["user"] = $users[$row["user"]];
            $records[] = $row;
        }

        return $this->render('@Control/Records/list.html.twig', [
            'records' => $records,
            'app' => $app == "user = 2",
        ]);
    }

}
