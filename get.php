<?php


// Database configuration
$servername = "localhost"; // Replace with your server name
$username = "root"; // Replace with your database username
$password = "159753QWEproto"; // Replace with your database password
$dbname = "venezia.by"; // Replace with your database name

// Create a connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// SQL query to fetch all UUIDs from the "tables" table
$sql = "SELECT CONCAT_WS(' ', p1.name, t1.name) as name, t1.uuid FROM tables t1 LEFT JOIN place p1 ON p1.id = t1.place_id WHERE place_id in (select id from place where shop_id = 15)";
$result = $conn->query($sql);

$uuids = [];

// Fetch data and store in array
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $uuids[$row['uuid']] = $row['name'];
    }
}

// Close the database connection
$conn->close();

// Convert the array to JSON
echo json_encode($uuids,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
