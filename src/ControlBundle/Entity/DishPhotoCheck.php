<?php

namespace ControlBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="ControlBundle\Entity\Repository\DishPhotoCheckRepository")
 * @ORM\Table(name="order_dish_photo_check")
 */
class DishPhotoCheck
{

    /*

    1 - слабо запечена
    2 - сильно запечена
    4 - мало ингредиентов
    8 - много ингредиентов
    16 - проблема с формой

     */

    const REVIEW_MASK = [
        1 => 'Слабо запечена (Too white)',
        2 => 'Сильно запечена (Too black)',
        4 => 'Мало ингредиентов (Few ingredients)',
        8 => 'Много ингредиентов (Many ingredients)',
        16 => 'Проблема с формой (Problem with form)',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", name="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ControlBundle\Entity\DishPhoto", inversedBy="checks")
     */
    private $dishPhoto;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish")
     */
    private $dish;

    /**
     * @ORM\Column(name="rate", type="integer", nullable=true)
     */
    private $rate;

    /**
     * @ORM\Column(name="review", type="integer", nullable=true)
     */
    private $review;






    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DishPhotoCheck
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     *
     * @return DishPhotoCheck
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return DishPhotoCheck
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dishPhoto
     *
     * @param \ControlBundle\Entity\DishPhoto $dishPhoto
     *
     * @return DishPhotoCheck
     */
    public function setDishPhoto(\ControlBundle\Entity\DishPhoto $dishPhoto = null)
    {
        $this->dishPhoto = $dishPhoto;

        return $this;
    }

    /**
     * Get dishPhoto
     *
     * @return \ControlBundle\Entity\DishPhoto
     */
    public function getDishPhoto()
    {
        return $this->dishPhoto;
    }

    /**
     * Set dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     * @return DishPhotoCheck
     */
    public function setDish(\DishBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \DishBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param mixed $review
     * @return DishPhotoCheck
     */
    public function setReview($review)
    {
        $this->review = $review;
        return $this;
    }

}
