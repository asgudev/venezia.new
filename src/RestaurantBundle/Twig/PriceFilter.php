<?php

namespace RestaurantBundle\Twig;


class PriceFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
        );
    }

    public function priceFilter($value)
    {
        return $this->format_price($value);
    }

    public function getName()
    {
        return 'price';
    }

    public function format_price($value)
    {
        return number_format($value, 2, '.', ' ');
    }
}
