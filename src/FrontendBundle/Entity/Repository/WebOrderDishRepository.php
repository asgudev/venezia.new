<?php

namespace FrontendBundle\Entity\Repository;

use TerminalBundle\Entity\DailyReport;
use Doctrine\ORM\EntityRepository;

use RestaurantBundle\Entity\Shop;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;



class WebOrderDishRepository extends EntityRepository
{
    public function findByShopInDates($shop, $category = null, $dateStart = null, $dateEnd = null)
    {

        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->addSelect("od.dishPrice as odDishPrice")
            ->addSelect("SUM(od.dishPrice * od.quantity) as odDishSum")
            ->addSelect("SUM(od.dishDiscount * od.quantity) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin("d.dishMetas", 'dm', 'WITH', 'dm.shop = :shop')
            ->where("o.shop = :shop")
            ->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $shop
            ]);

        if ($category) {
            $dishes->andWhere("d.category = :category")->setParameter("category", $category);
        }

        if ($dateStart) {
            $dateStart = new \DateTime($dateStart);
            $dishes->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", $dateStart);
        }
        if ($dateEnd) {
            $dateEnd = new \DateTime($dateEnd);
            if ($dateStart == $dateEnd) {
                $dateEnd = $dateEnd->modify("+1 day");
            }
            $dishes->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", $dateEnd);
        }
//dump($dishes->getQuery());die();

        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    public function findDeletionByDate($shop, $category = null, $dateStart = null, $dateEnd = null)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('o')
            ->addSelect('d')
            ->where('od.order IS NULL')
            ->andWhere('od.parentOrder = :shop')
            ->andWhere("o.createdAt > :dateStart")
            ->andWhere("o.createdAt < :dateEnd")
            ->innerJoin("od.parentOrder", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => (new \DateTime("today"))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime("today"))->setTime(23, 59, 59),
            ]);

        /*
        $dishes = $dishes->getQuery();
        dump($dishes); die();
        */

        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    public function findRawPizzaByReport(DailyReport $report)
    {
        return $this->createQueryBuilder('od')
            ->addSelect('o')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 1177')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.order', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->orderBy('d.name')
            ->getQuery()
            ->getResult();
    }

    public function findPizzaByReport(DailyReport $report, $type = true)
    {
        $pizzaDishes =  $this->createQueryBuilder('od')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 1177')
            ->andWhere('o.status < 5')
            ->addSelect('d')
            ->addSelect('di')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->orderBy('d.name');

        if ($type) {
            $pizzaDishes->leftJoin('od.parentOrder', 'o');
        } else {
            $pizzaDishes->leftJoin('od.order', 'o');
        }

        return $pizzaDishes->getQuery()->getResult();
    }

    public function findPizzaFromDate($shop, $date)
    {
        return $this->createQueryBuilder('od')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt > :dateStart ')
            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 1177')
            ->andWhere('o.status <= 4')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.parentOrder', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $date,
            ])
            ->orderBy('d.name')
            ->getQuery()
            ->getResult();
    }

    public function findPizzaSumFromDate($shop, \DateTime $date)
    {
        return $this->createQueryBuilder('od')
            ->addSelect('SUM(di.quantity * od.quantity) as pizzaSum')
            ->addSelect('dii.name as diiName')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt > :dateStart')
            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 1177')
            ->andWhere('o.status <= 4')
            ->andWhere('d.name NOT LIKE :str1')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.order', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->leftJoin('di.ingredient', 'dii')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $date,
                'str1' => '%добавка%',
            ])

            ->groupBy('dii')
            ->orderBy('dii.id')
            ->getQuery()
            ->getResult();
    }

    public function findPizzaByDate($shop, $date = 'today')
    {
        return $this->createQueryBuilder('od')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 1177')
            ->andWhere('o.status <= 4')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.parentOrder', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => (new \DateTime($date))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime($date))->setTime(23, 59, 59),
            ])
            ->orderBy('d.name')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findAllDishesFromDate($shop, $date)
    {
        $dishes = $this->createQueryBuilder('o')
            ->addSelect('od')
            ->addSelect('odc')
            ->addSelect('odcp')
            ->addSelect('odm')
            ->where('op.createdAt > :dateStart')
            ->andWhere('op.status <= 4')
            ->andWhere('op.shop = :shop')
//            ->andWhere('op.cashType = 0')
            ->innerJoin('o.parentOrder', 'op')
            ->innerJoin('o.dish', 'od')
            ->innerJoin('od.category', 'odc')
            ->innerJoin('odc.parent', 'odcp')
            ->leftJoin('od.dishMetas', 'odm', 'WITH', 'odm.shop = :shop')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $date,
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findAllDishesByDate($shop, $date = 'yesterday')
    {
        $dishes = $this->createQueryBuilder('o')
            ->addSelect('od')
            ->addSelect('odc')
            ->addSelect('odcp')
            ->addSelect('odm')
            ->where('op.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('op.status <= 4')
            ->andWhere('op.shop = :shop')
//            ->andWhere('op.cashType = 0')
            ->innerJoin('o.parentOrder', 'op')
            ->innerJoin('o.dish', 'od')
            ->innerJoin('od.category', 'odc')
            ->innerJoin('odc.parent', 'odcp')
            ->leftJoin('od.dishMetas', 'odm', 'WITH', 'odm.shop = :shop')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => (new \DateTime($date))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime($date))->setTime(23, 59, 59),
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findDishesByReport(DailyReport $dailyReport)
    {
        $dishes = $this->createQueryBuilder('o')
            ->addSelect('oo')
            ->addSelect('od')
            ->addSelect('odc')
            ->addSelect('odcp')
            ->addSelect('odm')
            ->addSelect('di')
            ->where('oo.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('oo.shop = :shop')
            ->andWhere('oo.localId > 0')
            ->andWhere('oo.status = :status')
            ->innerJoin('o.order', 'oo')
            ->innerJoin('o.dish', 'od')
            ->innerJoin('od.category', 'odc')
            ->innerJoin('odc.parent', 'odcp')
            ->leftJoin('od.dishMetas', 'odm')
            ->leftJoin('od.ingredients', 'di')
            ->setParameters([
                'shop' => $dailyReport->getShop(),
                'dateStart' => $dailyReport->getDateStarted(),
                'dateEnd' => $dailyReport->getDateClosed(),
                'status' => Order::STATUS_CLOSED,
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findGroupedDishesByReport(DailyReport $dailyReport)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect("SUM(od.dishPrice) as odDishPrice")
            ->addSelect("SUM(od.dishDiscount) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("o.cashType as oCashType")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->groupBy("od.dish")
            ->addGroupBy('o.cashType')
            ->setParameters([
                "shop" => $dailyReport->getShop(),
                'dateStart' => $dailyReport->getDateStarted(),
                'dateEnd' => $dailyReport->getDateClosed(),
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findGroupedDishesByPeriod($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect("od.dishPrice as odDishPrice")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("SUM(od.dishPrice * od.quantity) as odCashSum")
            ->addSelect("SUM(od.dishDiscount * od.quantity) as odDiscountSum")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->leftJoin('o.client', 'c', 'WITH', 'c.isExportable = true')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->groupBy("od.dish")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,

            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findClientTableDishesByPeriod($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect('t.name as table')
            ->addSelect("od.dishPrice as odDishPrice")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("SUM(od.dishPrice * od.quantity) as odCashSum")
            ->addSelect("SUM(od.dishDiscount * od.quantity) as odDiscountSum")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->leftJoin('o.table', 't')
            //->leftJoin('o.client', 'c', 'WITH', 'c.isExportable = true')

            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->andWhere('t.isClient = true')
            ->groupBy('t')
            ->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,

            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    public function findSummedByDish($shop, $category = null, $dateStart = null, $dateEnd = null)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect("SUM(od.dishPrice) as odDishPrice")
            ->addSelect("SUM(od.dishDiscount) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("o.cashType as oCashType")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->groupBy("od.dish")
            ->addGroupBy('o.cashType')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => (new \DateTime("yesterday"))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime("today"))->setTime(0, 0, 0),
            ]);

        /*
        $dishes = $dishes->getQuery();
        dump($dishes); die();
        */

        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    public function findLastOrders($dish)
    {
        $last = $this->createQueryBuilder('o')
            ->where('o.dish = :dish')
            ->addSelect('oo')
            ->addSelect('oos')
            ->innerJoin('o.order', 'oo')
            ->innerJoin('oo.shop', 'oos')
            ->setParameters([
                'dish' => $dish,
            ])
            ->orderBy('oo.createdAt', "DESC")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();

        return $last;
    }

    public function findGroupedByReport(DailyReport $dailyReport)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->addSelect('o')
            //->addSelect("od.dishPrice as odDishPrice")
            //->addSelect("SUM(od.dishPrice * od.quantity) as odDishSum")
            //->addSelect("SUM(od.dishDiscount * od.quantity) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("dc.name as dcName")
            ->leftJoin("od.order", "o")
            ->leftJoin("od.dish", 'd')
            ->leftJoin("d.category", 'dc')
            ->leftJoin("d.dishMetas", 'dm')
            ->where("o.shop = :shop")
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->groupBy("d")
            ->setParameters([
                "shop" => $dailyReport->getShop(),
                'dateStart' => $dailyReport->getDateStarted(),
                'dateEnd' => $dailyReport->getDateClosed(),
            ]);


        $dishes = $dishes->getQuery()->getResult();

        dump($dishes);
        die();

        return $dishes;
    }

    public function findAllDeletedDishes(Shop $shop)
    {
        return $this->createQueryBuilder('od')
            ->where('od.order IS NULL')
            ->andWhere('odp.shop = :shop')
            ->innerJoin('od.parentOrder', 'odp')
            ->setParameters([
                'shop' => $shop
            ])
            ->getQuery()
            ->getResult();
    }


    public function findSumByReport(DailyReport $report)
    {
        return $this->createQueryBuilder('o')
            ->select('SUM(o.dishPrice * o.quantity) as odSum')
            ->where('oo.shop = :shop')
            ->andWhere('oo.status <= 4')
            ->andWhere('oo.createdAt BETWEEN :dateStart AND :dateEnd')
            ->innerJoin('o.order', 'oo')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param $shop
     * @param string $date
     */
    public function findIngredientsByPeriod($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('dii.name as diName')
            ->addSelect('di')
            ->addSelect('d')
            ->addSelect('SUM(od.quantity * di.quantity) as diQuantity')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'd')
            ->innerJoin('d.ingredients', 'di')
            ->leftJoin('di.ingredient', 'dii')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->groupBy('dii')
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findDelegatedDishesByReport(DailyReport $report)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin("d.dishMetas", 'dm')
            ->where("o.shop = :shop")
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('od.delegate is not null')
            //->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ]);


        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findGroupedDelegatedDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as delegate')
            ->addSelect('os.name as odDelegate')
            ->addSelect('d.name as dName')
            ->addSelect('od.dishPrice  as odDishPrice')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->addSelect('SUM(od.dishDiscount * od.quantity) as odDiscountSum')
            ->addSelect('SUM(od.quantity) as odDishQuantity')
            ->innerJoin('od.order', 'o')
            ->innerJoin('o.shop', 'os')
            ->innerJoin('od.delegate', 'odd')
            ->innerJoin('od.dish', 'd')
            ->where('o.createdAt BETWEEN :dateStart AND :dateEnd')
            //->andWhere('o.shop = :shop')
            ->andWhere('od.delegate = :shop')
            ->addGroupBy('d')
            ->addGroupBy('os')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        //dump($dishes);die();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findDelegatedDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin("d.dishMetas", 'dm')
            ->where("o.shop = :shop")
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('od.delegate is not null')
            ->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findRequestedDishesByReport(DailyReport $report)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin("d.dishMetas", 'dm')
            ->where("o.shop = :shop")
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('od.delegate is not null')
            //->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ]);


        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @return OrderDish[]
     */
    public function findGroupedWriteoffDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as delegate')
            ->addSelect('d.name as dName')
            ->addSelect('d')
            ->addSelect('dc.name as dcName')
            ->addSelect('od.dishPrice  as odDishPrice')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->addSelect('SUM(od.dishDiscount * od.quantity) as odDiscountSum')
            ->addSelect('SUM(od.quantity) as odDishQuantity')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.client', 'c')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('d.category', 'dc')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('c.unp = :unp')
            ->addGroupBy('d')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
                'unp' => 'none',
            ]);


        $dishes = $dishes->getQuery()->getResult();
        //dump($dishes);die();

        return $dishes;
    }

    /**
     * @param $shop
     * @return OrderDish[]
     */
    public function findGroupedWriteoffBarDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as delegate')
            ->addSelect('d.name as dName')
            ->addSelect('d')
            ->addSelect('dc.name as dcName')
            ->addSelect('od.dishPrice  as odDishPrice')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->addSelect('SUM(od.dishDiscount * od.quantity) as odDiscountSum')
            ->addSelect('SUM(od.quantity) as odDishQuantity')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.client', 'c')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('d.category', 'dc')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('c.isExportable = false')
            ->addGroupBy('d')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        //dump($dishes);die();

        return $dishes;
    }

    public function findMarcoReportCashData(Shop $shop, $category = null, $parentCategory = null)
    {

        $orderData = $this->createQueryBuilder('od')
            ->addSelect('DATE(o.createdAt) AS oCreatedAtDate')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->leftJoin('od.order', 'o')
            ->where('o.shop = :shop')
            ->andWhere('o.status = 4')
            ->setParameters([
                "shop" => $shop,
            ])
            ->groupBy('oCreatedAtDate');

        if ($category != null) {
            $orderData
                ->leftJoin('od.dish', 'd')
                ->leftJoin('d.category', 'c')
                ->andWhere('c.id = :id')
                ->setParameter('id', $category);
        }
        if ($parentCategory != null) {
            $orderData
                ->leftJoin('od.dish', 'd')
                ->leftJoin('d.category', 'c')
                ->leftJoin('c.parent', 'cp')
                ->andWhere('cp.id = :id')
                ->setParameter('id', $parentCategory);
        }


        return $orderData->getQuery()->getResult();

    }

    /**
     * @param $shop
     * @param string $date
     */
    public function findMarcoReportIngrData($shop)
    {
        return $this->createQueryBuilder('od')
            ->addSelect('DATE(o.createdAt) AS oCreatedAtDate')
            ->addSelect('SUM(od.quantity * di.quantity) as diQuantity')
            ->addSelect('dii.name as dName')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'd')
            ->innerJoin('d.ingredients', 'di')
            ->leftJoin('di.ingredient', 'dii')
            ->where('o.shop = :shop')
            ->andWhere('o.status = 4')
            ->setParameters([
                "shop" => $shop,
            ])
            ->groupBy('oCreatedAtDate')
            ->addGroupBy('dii')
            ->getQuery()
            ->getResult();
    }


}