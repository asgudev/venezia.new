(function ($) {
    $.fn.collection = function (options) {
        var settings = $.extend({
            addButtonText: 'Добавить',
            deleteButtonText: 'Удалить',
            items: [],
            uniqueItems: [],
            unique: false,
        }, options);


        var $collectionHolder = this;

        var $addItemLink = $('<a href="#" class="md-btn md-btn-success">' + settings.addButtonText + '</a>');
        var $newItemLi = $('<div />').append($addItemLink);

        $collectionHolder.append($newItemLi);

        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addItemLink.on('click', function (e) {
            e.preventDefault();
            addItemForm($collectionHolder, $newItemLi);
        });

        $collectionHolder.find('div.item-row').each(function () {
            addItemFormDeleteLink($(this));
        });

        function addItemForm() {
            var prototype = $collectionHolder.data('prototype');

            var index = $collectionHolder.data('index');

            var newForm = prototype.replace(/__name__/g, index);



            $collectionHolder.data('index', index + 1);


            var $newFormLi = $('<div></div>').append(newForm);
            $newItemLi.before($newFormLi);


            if (settings.unique) {
                settings.uniqueItems.push($newFormLi);
                
                settings.unique();
            }

            //specific
            //$newFormLi.find(".customPrice").val($("#category_discounts").val());

            addItemFormDeleteLink($newFormLi);
        }

        function addItemFormDeleteLink($itemFormLi) {
            var $removeFormA = $('<div class="uk-width-medium-1-6"><a href="#" class="md-btn md-btn-danger" style="margin-top: 10px;">' + settings.deleteButtonText + '</a></div>');
            $itemFormLi.find(".uk-grid").append($removeFormA);

            $removeFormA.on('click', function (e) {
                e.preventDefault();
                $itemFormLi.remove();
            });
        }

        return this;
    };
}(jQuery));