<?php

namespace ServiceBundle\Services;

class Telegram
{
    const TOKEN = "705986542:AAEs_PjxThw5lDheCoL2Ux7BWJ8oo0fm6XI";

    private $ch;

    public function __construct()
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);

    }

    public function sendMessage($target, $text)
    {
        $data = [
            'chat_id' => $target,
            'text' => $text,
        ];
        curl_setopt($this->ch, CURLOPT_URL, "https://api.telegram.org/bot" . self::TOKEN . "/sendMessage");
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $response = curl_exec($this->ch);

        curl_close($this->ch);
        return $response;
    }

}