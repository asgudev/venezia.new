<?php


namespace DeliveryBundle\Services;


use OrderBundle\Entity\Order;
use Symfony\Component\DependencyInjection\Container;

class TaxiCity
{

    private $container;
    private $sessionId;


    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->sessionId = $this->container->get("redis.helper")->get("delivery.taxi.city.sessionid");

        if (!$this->sessionId)
            $this->auth("375298298877", "1111");

    }

    public function call($order, $time)
    {
        $timeComment = "";
        switch ($time) {
            case -1:
                $timeComment = "ЗА 15 МИНУТ ДО " . $order->getMeta()->getTime();
                break;
            case 0:
                $timeComment = "";
                break;
            default:
                $now = (new \DateTime())->modify("+" . $time . " minutes");
                $timeComment = " К " . $now->format("H:i");
                break;
        }
//var_dump("СОВЕТСКАЯ 112 КАФЕ ИТАЛИЯ ДОСТАВКА " . $order->getFullSum() . " " . ($order->getCashType() == 0 ? "НАЛИЧНЫЕ" : "ОНЛАЙН") . " " . $timeComment);
//die();
        $data = [
            "phone" => "+375298298877",
            "deliveryTime" => (new \DateTime())->format("Y-m-d H:i:s"),
            "deliveryMinutes" => 0,
            "idService" => 5434320465,
            "notes" => "СОВЕТСКАЯ 112 КАФЕ ИТАЛИЯ ДОСТАВКА " . $order->getFullSum() . " " . ($order->getCashType() == 0 ? "НАЛИЧНЫЕ" : "ОНЛАЙН") . " " . $timeComment,
            "markups" => [],
            "attributes" => [],
            "isNotCash" => false,
            "delivery" => [
                "idRegion" => 5000140320,
                "idAddress" => 5001080845,
                "idDistrict" => 5000140321,
                "idCity" => 5000140323,
                "idPlace" => null,
                "idStreet" => 5001080845,
                "house" => "112",
            ],
            "destinations" => [

            ]
        ];


        return $this->exec("Taxi.WebAPI.NewOrder", urlencode(json_encode($data, JSON_UNESCAPED_UNICODE)));
    }

    public function orders()
    {

        return $this->get(urlencode(json_encode([[
                "viewName" => "Taxi.Orders",

            ]], JSON_UNESCAPED_UNICODE))

        );
    }

    public function cancel($orderId)
    {
        return $this->exec("Taxi.WebAPI.Client.CancelOrder", $orderId);
    }

    private function auth($login, $password)
    {
        $ch = curl_init("http://city.is-by.us/WebAPITaxi/Login?app=CxTaxiClient&l=%2B" . $login . "&p=" . $password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_PORT, 8080);
        $response = json_decode(curl_exec($ch), true);

        $this->container->get("redis.helper")->set("delivery.taxi.city.sessionid", $response["sessionid"], 1800);
        $this->sessionId = $response["sessionid"];
    }

    public function calcDistance()
    {
        $from = "Брест советская 112";
        $to = "Брест интернациональная 5";

        $from = urlencode($from);
        $to = urlencode($to);

        $data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=ru-RU&sensor=false");

        $data = json_decode($data);
    }

    public function searchAddress($address)
    {
        $data = [[
            "viewName" => "Taxi.Addresses.Search",
            "params" => [
                ["n" => "SearchText", "v" => $address],
                ["n" => "IDParent", "v" => "5000140323"],
            ]
        ]];

        //var_dump(json_encode($data, JSON_UNESCAPED_UNICODE));


        $this->get(urlencode(json_encode($data, JSON_UNESCAPED_UNICODE)));
    }

    private function get($params)
    {
        $url = "http://city.is-by.us/WebAPITaxi/GetViewData?params=" . $params;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_PORT, 8080);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_COOKIE, 'JSESSIONID=' . $this->sessionId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-type: text/html;charset=UTF-8",
        ]);

        $response = json_decode(curl_exec($ch), true);

        //var_dump(curl_getinfo($ch));

        var_dump($response);
        die();
    }

    private function exec($method, $params)
    {
        $url = "http://city.is-by.us/WebAPITaxi/RemoteCall?method=" . $method . "&params=" . $params;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_PORT, 8080);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_COOKIE, 'JSESSIONID=' . $this->sessionId);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-type: text/html;charset=UTF-8",
        ]);

        $response = json_decode(curl_exec($ch), true);


        return $response;
    }
}
