<?php

namespace TerminalBundle\Controller;

use ControlBundle\Entity\IngredientData;
use DishBundle\Entity\CategoryDiscount;
use DishBundle\Entity\Dish;
use DishBundle\Entity\DishCategory;
use DishBundle\Entity\IngredientTransfer;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\ShopPlaceTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TerminalBundle\Entity\DailyReport;
use TerminalBundle\Form\Type\DailyReportType;
use TerminalBundle\Form\Type\OrderType;
use TerminalBundle\Service\Fiscal;
use UserBundle\Entity\User;

/**
 * Class DefaultController
 * @package TerminalBundle\Controller
 * @Route("/terminal")
 */
class DefaultController extends Controller
{


    /**
     * @Route("/", name="terminal_index")
     */
    public function indexAction(Request $request)
    {
        $request->getSession()->migrate();

        if ($this->getUser() != null) {
            $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findTerminalShop($this->getUser()->getShop());
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }


        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();

        if ($this->get("redis.helper")->get("blockedTill_" . $this->getUser()->getShop()->getId()) >= time()) {
            return $this->render('@Terminal/Default/locked.html.twig', [
                'till' => $this->get("redis.helper")->get("blockedTill_" . $this->getUser()->getShop()->getId()),
                'shop' => $shop,
            ]);
        }

        $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findMenuByShop($shop);

        $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($this->getUser()->getShop());
        $previousReport = count($previousReport) > 0 ? $previousReport[0] : null;

        if ($previousReport) {
            $semiClosedOrders = $this->getDoctrine()->getRepository(Order::class)->findSemiClosedOrders($previousReport);

            if (count($semiClosedOrders) > 0) {
                return $this->render('@Terminal/Default/lockedByOrders.html.twig', []);
            }
        }

        $reportStart = $this->get("redis.helper")->get("shift_start_" . $shop->getId());

        if (!$reportStart) {
            if (!$previousReport) {
                $previousReport = new DailyReport($shop);
                $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
            }
            $reportStart = $previousReport->getDateClosed()->format("Y-m-d H:i:s");
            $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $previousReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);
        }

        if (!$request->get("playback")) {
            $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftActive($reportStart, $shop, null, Order::STATUS_WAIT_FOR_CLOSE);

            if ($this->isGranted("ROLE_ADMINISTRATOR")) {
                $closed = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftClosed($shop, null, $reportStart);
            } else {
                $closed = [];
            }
        } else {
            $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findOrdersAtTime($request->get("playback"), $shop, null);

            foreach ($orders as $key => $__order) {
                $orders[$key]->setStatus(0);
            }
            $closed = [];
        }

        $places = [];

        /**
         * @var ShopPlace $place
         */
        foreach ($shop->getPlaces() as $place) {
            $places[$place->getId()] = [
                'name' => $place->getName(),
                'tables' => [],
            ];

            /**
             * @var ShopPlaceTable $table
             */
            foreach ($place->getTables() as $table) {
                $places[$place->getId()]['tables'][$table->getId()] = [
                    'id' => $table->getId(),
                    'name' => $table->getName(),
                    'coordinates' => json_decode($table->getMetaCoordinates()),
                    'isToGo' => $table->isToGo(),
                ];
            }
        }

        /*        $siteTable = $this->getDoctrine()->getRepository('RestaurantBundle:Table')->find(0);
                $places[$siteTable->getPlace()->getId()]['tables'][$siteTable->getId()] = [
                    'id' => $siteTable->getId(),
                    'name' => $siteTable->getName(),
                    'coordinates' => json_decode($siteTable->getMetaCoordinates()),
                ];
        */

        /**
         * @var Order $order
         */
        foreach ($orders as $order) {
            if ($order->getPlace() != null) {
                $places[$order->getPlace()->getId()]['tables'][$order->getTable()->getId()]['status'] = $order->getStatus();
                $places[$order->getPlace()->getId()]['tables'][$order->getTable()->getId()]['waiter'] = $order->getUser()->getName();
                $places[$order->getPlace()->getId()]['tables'][$order->getTable()->getId()]['orderId'] = $order->getId();
                $places[$order->getPlace()->getId()]['tables'][$order->getTable()->getId()]['orderIdLocal'] = $order->getLocalId();
            }
        }


        $order = new Order($this->getUser(), $shop);
        $form = $this->createForm(OrderType::class, $order, [
            'action' => $this->generateUrl("order_create"),
            'shop' => $shop,
            'attr' => [
                'id' => "order_form"
            ]
        ]);

        $userTpl = [];
        if ($this->isGranted("ROLE_ADMINISTRATOR")) {
            $users = $this->getDoctrine()->getRepository("UserBundle:User")->findByRoleInShop(User::ROLE_ADMINISTRATOR, $this->getParameter('shop.id'));
            foreach ($users as $user) {
                $userTpl[$user->getId()] = $user->getName();
            }
        }

        $isTablet = false;
        if (array_key_exists("asgumobileclient", $request->headers->all()) && ($request->headers->all()["asgumobileclient"][0] == true)) {
            $isTablet = true;
        }

        $promos = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findBy([
            'type' => PromoCard::TYPE_MANUAL,
            'isVisible' => true,
        ]);

        if ($this->getUser()->getId() == 120) {
            $clients = $this->getDoctrine()->getRepository("RestaurantBundle:Client")->findForDelivery();
        } else {
            $clients = $this->getDoctrine()->getRepository("RestaurantBundle:Client")->findAll();
        }

        $nForNPromos = $this->get("order.manager")->getNForNDiscounts($shop);

        return $this->render('TerminalBundle:Default:index.html.twig', [
            'orders' => $orders,
            'clients' => $clients,
            'closed' => $closed,
            'shop' => $shop,
            'form' => $form->createView(),
            'categories' => $categories,
            'places' => $places,
            'users' => $userTpl,
            'shops' => $shops,
            'isTablet' => $isTablet,
            'promos' => $promos,
            'nForNPromos' => json_encode($nForNPromos),
            //'json_categories' => $catArray,
        ]);
    }

    /**
     * @Route("/dishes", name="terminal_load_dishes")
     */
    public function loadDishesAction(Request $request)
    {
        $shop = $this->getUser()->getShop();

        $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findMenuByShop($shop);


        return $this->render('@Terminal/Default/terminalDishes.html.twig', [
            'categories' => $categories,
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/orders", name="terminal_load_orders")
     */
    public function loadOrdersAjaxAction(Request $request)
    {
        $request->getSession()->migrate();

        $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findTerminalShop($this->getUser()->getShop());

        $reportStart = $this->get("redis.helper")->get("shift_start_" . $shop->getId());

        if (!$reportStart) {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($this->getUser()->getShop());
            if (!$previousReport) {
                $previousReport = new DailyReport($shop);
                $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
            } else {
                $previousReport = $previousReport[0];
            }
            $reportStart = $previousReport->getDateClosed()->format("Y-m-d H:i:s");
            $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $previousReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);
        }

        if ($this->getUser()->getId() == 120) {
            $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftActive($reportStart, null, $this->getUser(), Order::STATUS_WAIT_FOR_CLOSE);
        } else {
            $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftActive($reportStart, $shop, null, Order::STATUS_WAIT_FOR_CLOSE);
        }


        if ($this->isGranted("ROLE_ADMINISTRATOR")) {
            $closed = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftClosed($shop, null, $reportStart);

        } else {
            $closed = [];
        }


        return $this->render('@Terminal/Default/terminalOrders.twig', [
            'orders' => $orders,
            'closed' => $closed,
        ]);
    }

    /**
     * @Route("/promos", name="terminal_load_promos")
     */
    public function loadPromosAjaxAction(Request $request)
    {

        $promosArray = $this->getPromosArray($this->getUser()->getShop());

        return new JsonResponse($promosArray);
    }

    private function getPromosArray($shop)
    {
        $promos = $this->getDoctrine()->getRepository('RestaurantBundle:Promo')->findCurrentActive($shop);
        $promosArray = [];

        foreach ($promos as $promo) {
            $promosArray[] = [
                'id' => $promo->getId(),
                'discount' => $promo->getDiscount(),
                'icon' => $promo->getIcon(),
            ];
        }

        return $promosArray;
    }

    /**
     * @Route("/close/request", name="terminal_close_request_ingredient")
     */
    public function closeRequestShiftAction(Request $request)
    {
        $ingredientData = $request->get("daily_report")["ingredients"];
        $requestData = [];

        foreach ($ingredientData as $__ingr) {
            if ($__ingr["request"] > 0) {
//                $ingr = $this->getDoctrine()->getRepository(Ingredient::class)->find($__ingr["ingredient"]);
                $requestData[mb_strtolower($__ingr["ingredient"])][$this->getUser()->getShop()->getId()] = $__ingr["request"];
            }
        }

        $this->get("redis.helper")->set("shop_ingredient_" . $this->getUser()->getShop()->getId(), json_encode($ingredientData, JSON_UNESCAPED_UNICODE), 86400);

//        $this->get("restaurant.warehouse.manager")->updateRequestTable($requestData);


        $this->get("api.service")->call($this->generateUrl('warehouse_request'), "POST", $requestData, true);

//        $data = $this->get("restaurant.warehouse.manager")->getRequestData();


        return new JsonResponse();

    }

    /**
     * @Route("/close/confirm", name="terminal_close_confirm_ingredient")
     */
    public function closeTestShiftAction(Request $request)
    {
        $ingredientData = $request->get("daily_report")["ingredients"];

        $shop = $this->getUser()->getShop();

        $previousReport = $this->getDoctrine()->getRepository(DailyReport::class)->findLastReport($shop)[0];

        $ingredientIncome = $this->getDoctrine()->getRepository(IngredientTransfer::class)->findInDateRange($shop->getId(), $previousReport->getDateClosed(), new \DateTime());

        $ingrData = [];
        foreach ($ingredientIncome as $__ingr) {
            if (!array_key_exists($__ingr->getIngredient()->getId(), $ingrData))
                $ingrData[$__ingr->getIngredient()->getId()]["income_1c"] = 0;

            $ingrData[$__ingr->getIngredient()->getId()]["income_1c"] += $__ingr->getQuantity();
        }

        $last = [];

        foreach ($ingredientData as $key => $__ingr) {
            if ($__ingr["ingredient"] != 1) {


                unset($ingredientData[$key]);
                continue;
            }
            if (!array_key_exists($__ingr["ingredient"], $ingrData))
                $ingrData[$__ingr["ingredient"]] = [
                    'balance_now' => 0,
                    'income_1c' => 0,
                    'income' => 0,
                    'waste' => 0,
                    'request' => 0,
                ];

            /*
            $ingrData[$__ingr["ingredient"]][] = [
                "income" => array_key_exists("income", $__ingr) ? $__ingr["income"] : 0,
                "waste" => array_key_exists("waste", $__ingr) ? $__ingr["waste"] : 0,
                "balance_now" => array_key_exists("balance_prev", $__ingr) ? $__ingr["balance_prev"] : 0,
            ];
*/

            $ingrData[$__ingr["ingredient"]]["income"] += array_key_exists("income", $__ingr) ? $__ingr["income"] : 0;
            $ingrData[$__ingr["ingredient"]]["waste"] += array_key_exists("waste", $__ingr) ? $__ingr["waste"] : 0;
            $ingrData[$__ingr["ingredient"]]["request"] += array_key_exists("request", $__ingr) ? $__ingr["request"] : 0;

            if (array_key_exists("balance_prev", $__ingr) && $__ingr["balance_prev"] != 0)
                $ingrData[$__ingr["ingredient"]]["balance_now"] = $__ingr["balance_prev"];

            $last = $__ingr;
        }

        $this->get("redis.helper")->set("shop_ingredient_" . $shop->getId(), json_encode($ingredientData, JSON_UNESCAPED_UNICODE), 86400);


//        $this->get("api.service")->call($this->generateUrl('warehouse_request'), "POST", $request);


        foreach ($previousReport->getIngredients() as $__ingr) {
            $ingrData[$__ingr->getIngredient()->getId()]["name"] = $__ingr->getIngredient()->getName();
            $ingrData[$__ingr->getIngredient()->getId()]["balance_prev"] = $__ingr->getBalance();
        }

        foreach ($ingrData as $_key => $_data) {
            if ($_key != 1) unset($ingrData[$_key]);
        }

        $data = [
            'name' => $ingrData[1]["name"],
            'balance_prev' => $ingrData[1]["balance_prev"],
            'balance_now' => $ingrData[1]['balance_now'],
            'income_1c' => $ingrData[1]['income_1c'],
            'income' => $last['income'],
            'waste' => $last['waste'],
            'request' => $last['request'],
        ];

        $event = [
            'user' => $this->getUser()->getId(),
            "1" => $data,
        ];

        $this->get("api.service")->call($this->generateUrl("api_receive_remains", [
            'id' => $this->getUser()->getShop()->getId()
        ]), "POST", $event);


//        dump($data);
//        die();
        if ($this->get('kernel')->getEnvironment() != "dev") {
            $this->get('mailer')->send(\Swift_Message::newInstance()
                ->setSubject("Подтверждение остатков " . $this->getUser()->getShop()->getName())
                ->setFrom('send@italbrest.com')
                ->setTo([
                  // "asgudev@gmail.com",
                    // 'julia.buh@italbrest.com',
                    // 'natasha@italbrest.com',
                    // 'margo@italbrest.com',
                    // 'marketing@italbrest.com',
                    // 'zav.proizvodstva@italbrest.com',
                    // 'lena@italbrest.com',
//                    'iprotoss@bk.ru',
                ])
                ->setBody(
                    $this->renderView("@Restaurant/Custom/revisionConfirmationEmail.html.twig", [
                        'ingrData' => [$data],
                    ]),
                    'text/html'
                ));
        }


        return new JsonResponse();
    }

    /**
     * @Route("/close", name="terminal_close_shift")
     */
    public function closeShiftAction(Request $request)
    {
        if ($this->getUser() != null) {
            $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findTerminalShop($this->getUser()->getShop());
        } else {
            return $this->redirectToRoute('login_tablet');
        }

        if ($this->get("redis.helper")->get("blockedTill_" . $this->getUser()->getShop()->getId()) >= time()) {
            return $this->render('@Terminal/Default/locked.html.twig', [
                'till' => $this->get("redis.helper")->get("blockedTill_" . $this->getUser()->getShop()->getId()),
                'shop' => $shop,
            ]);
        }

        $dailyReport = new DailyReport($shop);


        $form = $this->createForm(DailyReportType::class, $dailyReport);

        $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($this->getUser()->getShop());
        if (!$previousReport) {
            $previousReport = new DailyReport($shop);
            $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
        } else {
            $previousReport = $previousReport[0];
        }

        $fiscal = $this->get('terminal.fiscal')->getSums($shop->getFiscalPort());

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $dailyReport->setFiscalCashSum($fiscal["cash"]);
            $dailyReport->setFiscalCardSum($fiscal["card"]);
            $dailyReport->setUser($this->getUser());
            $dailyReport->setDateStarted($previousReport->getDateClosed());
            if ($dailyReport->getDateClosed() === null) {
                $dailyReport->setDateClosed(new \DateTime());
            } else {
                $dailyReport->setDateClosed($dailyReport->getDateClosed());
            }
            $dailyReport->setFiscalCashSum($fiscal["cash"]);
            $dailyReport->setFiscalCardSum($fiscal["card"]);

            $stackedIngrs = [];
            foreach ($dailyReport->getIngredients() as $__ingr) {
                if (!array_key_exists($__ingr->getIngredient()->getId(), $stackedIngrs)) {
                    $__stackedIngr = new IngredientData();
                    $__stackedIngr->setIngredient($__ingr->getIngredient());
                    $__stackedIngr->setReport($__ingr->getReport());

                    $stackedIngrs[$__ingr->getIngredient()->getId()] = $__stackedIngr;
                }
                /**
                 * @var IngredientData $__stackedIngr ;
                 */
                $__stackedIngr = $stackedIngrs[$__ingr->getIngredient()->getId()];
                $__stackedIngr->addBalance($__ingr->getBalance());
                $__stackedIngr->addWaste($__ingr->getWaste());
                $__stackedIngr->addIncome($__ingr->getIncome());
                $dailyReport->removeIngredient($__ingr);
            }
            foreach ($stackedIngrs as $__ingr) {
                $dailyReport->addIngredient($__ingr);
            }


            if ($shop->isRevisionLock()) {
//                if ($this->validateRevision($dailyReport)) {
                $this->get("shop.report.manager")->validateRevision($dailyReport);
                $this->closeShift($shop, $dailyReport);
//                }
            } else {
                $this->closeShift($shop, $dailyReport);
            }


            return $this->redirectToRoute('terminal_close_shift');
        }

        $openedOrders = $this->getDoctrine()->getRepository(Order::class)->findNonClosed($previousReport);

        $orders = $this->getDoctrine()->getRepository(Order::class)->findSums($shop, $previousReport->getDateClosed())[0];
        $dishSum = $this->getDoctrine()->getRepository(OrderDish::class)->findDishSum($shop, $previousReport->getDateClosed());

        $money = [
            'cash' => $orders['oCash'],
            'terminal' => $orders['oCard'],
            'site' => 0,
            'oplati' => $this->getDoctrine()->getRepository(Order::class)->findSumsByType($shop, $previousReport->getDateClosed(), Order::CASHTYPE_OPLATI)["oSum"],
        ];

        $separatedUsers = $this->getDoctrine()->getRepository('UserBundle:User')->findBy([
            'isSeparated' => true,
            'shop' => $shop,
        ]);

        $moneySeparated = [];
        foreach ($separatedUsers as $user) {
            $ordersSep = $this->getDoctrine()->getRepository(Order::class)->findSumsSeparated($shop, $previousReport->getDateClosed(), $user)[0];
            $moneySeparated[$user->getName()] = [
                'cash' => $ordersSep['oCash'],
                'terminal' => $ordersSep['oCard'],
            ];

            $money['cash'] -= $ordersSep['oCash'];
            $money['terminal'] -= $ordersSep['oCard'];
        }

        $ingredientIncome = $this->getDoctrine()->getRepository('DishBundle:IngredientTransfer')->findInDateRange($shop, $previousReport->getDateClosed(), new \DateTime());

        $ingrData = [];
        foreach ($ingredientIncome as $__ingr) {
            if (!array_key_exists($__ingr->getIngredient()->getId(), $ingrData))
                $ingrData[$__ingr->getIngredient()->getId()] = 0;

            $ingrData[$__ingr->getIngredient()->getId()] += $__ingr->getQuantity();
        }

        if ($this->getUser()->hasRole('ROLE_ADMIN')) {
            $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();
        } else {
            $shops = [];
        }

        $ingredients = $this->getDoctrine()->getRepository("DishBundle:Ingredient")->findAll();

        $ingrs = [];
        foreach ($ingredients as $ingredient) {
            $ingrs[$ingredient->getId()] = $ingredient->getName();
        }

        unset($ingrData[1]);

        $lastUserIngredientData = $this->getDoctrine()->getRepository("UserBundle:UserEvent")->findLastShopEvent($this->getUser()->getShop());
        if ($lastUserIngredientData) {
            $lastUserIngredientData = json_decode($lastUserIngredientData->getPayload(), true);
        } else {
            $lastUserIngredientData = false;
        }
        $eventData = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAllInRange($this->getUser()->getShop()->getId(), $previousReport->getDateClosed(), new \DateTime());


        $events = [];
        foreach ($eventData as $__event) {
            if ($__event->getEventType() == 101) {
                $events[] = json_decode($__event->getPayload(), true);
            }
        }

        $userIngrData = [
            "Шарик большой" => [
                "balance_prev" => 0,
                "income" => 0,
                "waste" => 0,
            ]
        ];

        foreach ($events as $__event) {
            $userIngrData["Шарик большой"]["balance_prev"] = $__event["balance_prev"];
            $userIngrData["Шарик большой"]["income"] += $__event["income"];
            $userIngrData["Шарик большой"]["waste"] += $__event["waste"];
        }

        $userIngrData2 = $this->get("redis.helper")->get("shop_ingredient_" . $shop->getId());


        if (!$userIngrData2)
            $userIngrData2 = json_encode([]);

        $requestData = $this->get("restaurant.warehouse.manager")->getRequestData();
//var_dump($requestData);die();
//        dump($previousReport);die();

        return $this->render('@Terminal/Default/closeShift.html.twig', [
            'ingredientIncome' => $ingrData,
            'userIngrData' => $userIngrData,
            'userIngrData2' => $userIngrData2,
            'requestData' => $requestData,
            'form' => $form->createView(),
            'money' => $money,
            'dishSum' => $dishSum,
            'opened' => $openedOrders,
            'lastReport' => $previousReport,
            'moneySeparated' => $moneySeparated,
            'fiscal' => $fiscal,
            'ingredients' => json_encode($ingrs, JSON_UNESCAPED_UNICODE),
            'shops' => $shops,
            'lastUserIngredientData' => $lastUserIngredientData,
        ]);
    }

   

    

    

    private function closeShift(Shop $shop, DailyReport $dailyReport)
    {
        $this->get("redis.helper")->delete("shop_ingredient_" . $this->getUser()->getShop()->getId());
        $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $dailyReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);

        $em = $this->getDoctrine()->getManager();
        $em->persist($dailyReport);
        $em->flush();

        if ($this->getParameter('kernel.environment') == 'prod') {
            if (($shop->getFiscalType() == Shop::FISCAL_TYPE_TERMINAL) && ($this->getUser()->isFiscalGranted())) {
                $this->get('terminal.fiscal')->report(Fiscal::REPORT_Z, $shop->getFiscalModel(), $shop->getFiscalPort());
            }
        }

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $exportCommand = new ArrayInput(array(
            'command' => 'rest:report:export',
            '--report' => $dailyReport->getId(),
            '--env' => $this->getParameter('kernel.environment')
        ));
//        $updatePrices = new ArrayInput(array(
//            'command' => 'rest:changes:apply',
//            '--env' => $this->getParameter('kernel.environment')
//        ));
        $application->run($exportCommand);
//        $application->run($updatePrices);
    }

    /**
     * @Route("/fiscal/report", name="terminal_fiscal_report")
     */
    public function closeShiftCommandAction(Request $request)
    {

        $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findTerminalShop($this->getUser()->getShop()->getId());
        $reportType = $request->get('type');

        switch ($reportType) {
            case 'x':
                if (in_array($this->getUser()->getId(), [13, 37])) {
                    $this->get('terminal.fiscal')->report(Fiscal::REPORT_X, $shop->getFiscalModel(), '192.168.31.247:4422');
                } else {
                    $this->get('terminal.fiscal')->report(Fiscal::REPORT_X, $shop->getFiscalModel(), $shop->getFiscalPort());
                }
                break;
            case 'z':
                if ($this->getUser()->getShop() == 1) {
                    $this->get('terminal.fiscal')->report(Fiscal::REPORT_Z, $shop->getFiscalModel(), '192.168.31.247:4422');
                }
                $this->get('terminal.fiscal')->report(Fiscal::REPORT_Z, $shop->getFiscalModel(), $shop->getFiscalPort());
                break;
        }

        return new JsonResponse([
            'status' => true,
        ]);
    }

    /**
     * @Route("/fiscal/direct", name="terminal_send_direct_sum")
     */
    public function sendSumDirectToFiscalAction(Request $request)
    {
        $sum = $request->get('sum');
        $cashType = $request->get("cashtype");

        $this->get('terminal.fiscal')->sendDirectSum($sum, $cashType);

        return new JsonResponse([
            'status' => true,
        ]);
    }

    /**
     * @Route("/card", name="terminal_load_client_card")
     */
    public function loadClientCardAction(Request $request)
    {
        $cardId = $request->getContent();

        $card = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findOneBy([
            'cardId' => $cardId,
        ]);


        if (!$card) {
            $card = new PromoCard();
            $card->setCardId($cardId);
            $card->setCardDiscount(10);
            $card->setName('Новая карта');
            $card->setCreatedBy($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($card);
            $em->flush();

            return new JsonResponse([
                'error' => false,
                'id' => $card->getId(),
                'name' => $card->getName(),
                'cardDiscount' => $card->getCardDiscount(),
                'cardId' => $card->getCardId()
            ]);
        } else {
            return new JsonResponse([
                'error' => false,
                'id' => $card->getId(),
                'name' => $card->getName(),
                'cardDiscount' => $card->getCardDiscount(),
                'cardId' => $card->getCardId()
            ]);
        }
    }

    /**
     * @param DishCategory[] $categories
     * @return DishCategory[]
     */
    private function removeInactiveDishes($categories)
    {
        $now = new \DateTime();
        $offset = (new \DateTime())->setDate(1970, 1, 1);
        foreach ($categories as $__category) {
            /** @var Dish $__dish */
            foreach ($__category->getDishes() as $__dish) {
                if (count($__dish->getTimes()) > 0) {
                    $disable = true;

                    foreach ($__dish->getTimes() as $__time) {
                        if ($__time->getDayOfWeek() == $now->format('N')) {
                            if ($offset >= $__time->getTimeStart() && ($offset <= $__time->getTimeEnd())) {
                                $disable = false;
                            }
                        }
                    }

                    if ($disable) {
                        $__category->removeDish($__dish);
                    }
                }
            }
        }
        return $categories;
    }
}

/*

SELECT u.id, u.name, us.shop_id FROM `user_shop` us left join users u on u.id =us.user_id WHERE `user_id` in (110, 139, 155, 256, 32567)


 */