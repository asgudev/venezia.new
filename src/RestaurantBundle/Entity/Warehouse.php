<?php

namespace RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Restaurant
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Warehouse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="name1c", type="string", length=255, nullable=true)
     */
    private $name1c;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\IngredientTransfer", mappedBy="to")
     */
    private $income;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\IngredientTransfer", mappedBy="from")
     */
    private $outcome;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop", inversedBy="warehouses")
     */
    private $shop;





    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\IngredientInWarehouse", mappedBy="warehouse")
     */
    private $ingredients;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Warehouse
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName1c()
    {
        return $this->name1c;
    }

    /**
     * @param mixed $name1c
     * @return Warehouse
     */
    public function setName1c($name1c)
    {
        $this->name1c = $name1c;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIncome()
    {
        return $this->income;
    }

    /**
     * @param mixed $income
     * @return Warehouse
     */
    public function setIncome($income)
    {
        $this->income = $income;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutcome()
    {
        return $this->outcome;
    }

    /**
     * @param mixed $outcome
     * @return Warehouse
     */
    public function setOutcome($outcome)
    {
        $this->outcome = $outcome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @param mixed $ingredients
     * @return Warehouse
     */
    public function setIngredients($ingredients)
    {
        $this->ingredients = $ingredients;
        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     * @return Warehouse
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        return $this;
    }


}
