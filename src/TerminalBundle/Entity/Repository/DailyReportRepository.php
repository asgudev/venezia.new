<?php

namespace TerminalBundle\Entity\Repository;

use TerminalBundle\Entity\DailyReport;
use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;

class DailyReportRepository extends EntityRepository
{
    /**
     * @param Shop $shop
     * @param string $date
     * @return DailyReport
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findData(Shop $shop, $date = 'yesterday')
    {
        return $this->createQueryBuilder('r')
            ->where('r.shop = :shop')
            ->andWhere('r.date BETWEEN :dayStart AND :dayEnd')
            ->addSelect('ii')
            ->addSelect('iii')
            ->innerJoin('r.ingredients', 'ri')
            ->innerJoin('ri.ingredient', 'rii')
            ->setParameters([
                'shop' => $shop,
                'dayStart' => (new \DateTime($date))->setTime(0, 0, 0),
                'dayEnd' => (new \DateTime($date))->setTime(23, 59, 59),

            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Shop $shop
     * @return DailyReport[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findShopReports(Shop $shop)
    {
        return $this->createQueryBuilder('r')
            ->addSelect('ri')
            ->where('r.shop = :shop')
            ->leftJoin('r.ingredients', 'ri')
            ->setParameters([
                'shop' => $shop
            ])
            ->orderBy('r.dateStarted', "ASC")
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @return DailyReport[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findShopReportsFromDate(Shop $shop, \DateTime $date)
    {
        return $this->createQueryBuilder('r')
            ->addSelect('ri')
            ->where('r.shop = :shop')
            ->andWhere('r.dateClosed >= :date')
            ->leftJoin('r.ingredients', 'ri')
            ->setParameters([
                'shop' => $shop,
                'date' => $date
            ])
            ->orderBy('r.dateStarted', "ASC")
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @param int $limit
     * @return DailyReport[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findReportsByDate($date = 'yesterday')
    {
        return $this
            ->createQueryBuilder('r')
//            ->where('r.shop = :shop')
            ->andWhere('r.dateStarted < :date')
            ->andWhere('r.dateClosed > :date')
            ->addSelect('ri')
            ->addSelect('rii')
            ->leftJoin('r.ingredients', 'ri')
            ->leftJoin('ri.ingredient', 'rii')
            ->setParameters([
//                'shop' => $shop,
                'date' => new \DateTime($date)
            ])
            ->orderBy('r.dateStarted', "ASC")
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @param int $limit
     * @return DailyReport[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findReportsByPeriod($dateStart = 'yesterday', $dateEnd = 'today', $shop = null)
    {
        $orders = $this
            ->createQueryBuilder('r')
            ->where('r.dateStarted >= :dateStart')
            ->andWhere('r.dateClosed <= :dateEnd')
            ->andWhere('rs.id != 14')
            ->addSelect('ri')
            ->addSelect('rii')
            ->leftJoin('r.ingredients', 'ri')
            ->leftJoin('ri.ingredient', 'rii')
            ->leftJoin('r.shop', 'rs')
            ->setParameters([
                'dateStart' => new \DateTime($dateStart),
                'dateEnd' => new \DateTime($dateEnd),
            ])
            ->orderBy('r.dateStarted', "ASC");

        if ($shop) {
            $orders->andWhere('r.shop = :shop')
                ->setParameter('shop', $shop);
        }

        return $orders
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @param int $limit
     * @return DailyReport[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastReportGreaterThenDate(Shop $shop, $date, $limit = 1)
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.shop = :shop')
            ->andWhere('r.dateStarted > :date')
//            ->andWhere('rii.id = 3')
            ->addSelect('ri')
            ->addSelect('rii')
            ->leftJoin('r.ingredients', 'ri')
            ->leftJoin('ri.ingredient', 'rii')
            ->setParameters([
                'shop' => $shop,
                'date' => $date,
            ])
            ->orderBy('r.dateClosed', "ASC")
            ->addOrderBy('rii.id', "ASC")
            //->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @param int $limit
     * @return DailyReport[]
     */
    public function findLastReport(Shop $shop, $limit = 1)
    {
        $qb = $this
            ->createQueryBuilder('r');

        $reportId = $this
            ->createQueryBuilder("r2")
            ->select("MAX(r2.dateClosed)")
            ->where('r2.shop = :shop')
            ->getDQL();

        $qb
            ->where('r.shop = :shop')
            ->andWhere($qb->expr()->eq("r.dateClosed", "(" . $reportId . ")"))
            ->addSelect('ri')
            ->addSelect('rii')
            ->leftJoin('r.ingredients', 'ri')
            ->leftJoin('ri.ingredient', 'rii')
            ->setParameters([
                'shop' => $shop,
            ])
            ->orderBy('r.dateClosed', "DESC")
            ->addOrderBy('rii.id', "ASC");

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Shop $shop
     * @param int $limit
     * @return DailyReport[]
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findReportByDate(Shop $shop, $name)
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.shop = :shop')
            ->andWhere('r.name = :name')
            ->addSelect('ri')
            ->addSelect('rii')
            ->leftJoin('r.ingredients', 'ri')
            ->leftJoin('ri.ingredient', 'rii')
            ->setParameters([
                'shop' => $shop,
                'name' => $name,

            ])
            ->orderBy('r.dateClosed', "DESC")
            ->addOrderBy('rii.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DailyReport $report
     * @return DailyReport
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findPreviousReport(DailyReport $report)
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.shop = :shop')
            ->andWhere('r.dateClosed = :date')
            ->addSelect('ri')
            ->addSelect('rii')
            ->leftJoin('r.ingredients', 'ri')
            ->leftJoin('ri.ingredient', 'rii')
            ->setParameters([
                'shop' => $report->getShop(),
                'date' => $report->getDateStarted(),
            ])
            ->orderBy('r.dateClosed', "DESC")
            ->getQuery()
            ->getOneOrNullResult();
    }


}
