#!/usr/bin/env bash

SLAVE_NAME=$3
SLAVE_IP=$1
SLAVE_PASS=$2
BACKUP_DIR=$4

mysql -h $SLAVE_IP -ubackup -p$SLAVE_PASS -P 3306 venezia_new < $BACKUP_DIR/data_master.sql &
wait

echo "FIN $3"

