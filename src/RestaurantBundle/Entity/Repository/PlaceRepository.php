<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 06.07.2016
 * Time: 11:56
 */

namespace RestaurantBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;

class PlaceRepository extends EntityRepository
{
    public function findShopPlaces(Shop $shop)
    {
        return $this->createQueryBuilder('p')
            ->where('p.shop = :shop')
            ->setParameters([
                'shop' => $shop
            ])
            ->getQuery()
            ->getResult();
    }
}