<?php

namespace RestaurantBundle\Service;

class BitToArray
{

    public function bitToArray($n, $array)
    {
        $bin_powers = array();
        for ($bit = 0; $bit < count($array); $bit++) {
            $bin_power = 1 << $bit;
            if ($bin_power & $n) $bin_powers[$bit] = $bin_power;
        }
        return $bin_powers;
    }

    public function bitToReadableArray($n, $array)
    {
        $bits = $this->bitToArray($n, $array);

        $res = [];
        foreach ($bits as $key => $bit)
            $res[] = $array[$bit];

        return $res;
    }
}