<?php

namespace RestaurantBundle\Service;

use Doctrine\ORM\EntityManager;
use DishBundle\Entity\DishCategory;
use DishBundle\Entity\Dish;
use DishBundle\Entity\DishHistory;
use DishBundle\Entity\DishMeta;
use DishBundle\Entity\IngredientInWarehouse;
use DishBundle\Entity\IngredientTransfer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MixPendingPrices
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    /**
     * @param DishCategory $category
     * @param DishHistory[] $changes
     */
    public function mixToCategory(DishCategory $category, $changes)
    {
        foreach ($category->getChildren() as $child) {
            $this->mixToCategory($child, $changes);
        }

        foreach ($category->getDishes() as $dish) {
            foreach ($changes as $change) {
                if ($dish->getId() == $change->getDish()->getId()) {
                    $this->mixToDish($dish, $change);
                }
            }
        }
    }

    public function mixToDish(Dish $dish, DishHistory $change)
    {
        switch ($change->getField()) {
            case 'name':
                $dish->setName($change->getValue());
                break;
            case 'base_price':
                foreach ($dish->getDishMetas() as $dishMeta) {
                    if ($dishMeta->getPrice() == $dish->getPrice()) {
                        $dishMeta->setPrice($change->getValue());
                    }
                }
                break;
            case 'price':
                $dishMeta = $dish->getShopMeta($change->getShop());
                if ($dishMeta) {
                    $dishMeta->setPrice($change->getValue());
                    if (!$dish->hasShop($change->getShop()))
                        $dish->addShop($change->getShop());
                } else {
                    $dishMeta = new DishMeta();
                    $dishMeta->setDish($dish)
                        ->setShop($change->getShop())
                        ->setPrice($change->getValue());
                    if (!$dish->hasShop($change->getShop()))
                        $dish->addShop($change->getShop());
                }
        }
    }
}