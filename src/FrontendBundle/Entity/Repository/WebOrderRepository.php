<?php

namespace FrontendBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

use FrontendBundle\Entity\WebOrder;
use RestaurantBundle\Entity\Shop;

class WebOrderRepository extends EntityRepository
{

    public function findPayed() {
        return $this->createQueryBuilder('o')
            ->where('o.status >= :status')
            ->setParameters([
                'status' => WebOrder::STATUS_PAID
            ])
            ->orderBy("o.createdAt", "DESC")
            ->getQuery()
            ->getResult();
    }

    /**
     * @param mixed $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return WebOrder
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {

        return $this->createQueryBuilder('w')
            ->addSelect('od')
            ->leftJoin('w.dishes', 'od')
            ->where('w.id = :id')
            ->setParameters([
                'id' => $id
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Shop $shop
     * @return WebOrder[]
     */
    public function findActiveWebOrders(Shop $shop) {
        return $this->createQueryBuilder('w')
            ->where('w.pickupShop = :shop')
            ->andWhere('w.status >= 20')
            ->andWhere('w.status < 30')
            ->setParameters([
                'shop' => $shop,
            ])
            ->getQuery()
            ->getResult();
    }

    public function findActiveOrders(Shop $shop) {
        return $this->createQueryBuilder('w')
            ->where('w.shop = :shop')
            ->andWhere('w.status >= 20')
            ->andWhere('w.status < 30')
            ->setParameters([
                'shop' => $shop,
            ])
            ->getQuery()
            ->getResult();
    }

    public function findLastInShopWithId($shop, $localId)
    {
        $order = $this->createQueryBuilder('w')
            ->where("w.shop = :shop")
            ->andWhere("w.localId = :id")
            ->setParameters([
                'shop' => $shop,
                'id' => $localId,
            ])
            ->orderBy('w.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        return $order;
    }

    public function findLastInShop(Shop $shop)
    {
        $order = $this->createQueryBuilder('w')
            ->where("w.pickupShop = :shop")
            ->andWhere("w.localId <> 0")
            ->setParameters([
                'shop' => $shop,
            ])
            ->orderBy('w.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        return $order;
    }

}