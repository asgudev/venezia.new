<?php

namespace DishBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * DishTime
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class DishTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish", inversedBy="times")
     */
    private $dish;

    /**
     * @ORM\Column(name="day_of_week", type="smallint", nullable=false)
     */
    private $dayOfWeek;

    /**
     * @ORM\Column(name="time_start", type="time", nullable=false)
     */
    private $timeStart;

    /**
     * @ORM\Column(name="time_end", type="time", nullable=false)
     */
    private $timeEnd;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dayOfWeek.
     *
     * @param int $dayOfWeek
     *
     * @return DishTime
     */
    public function setDayOfWeek($dayOfWeek)
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    /**
     * Get dayOfWeek.
     *
     * @return int
     */
    public function getDayOfWeek()
    {
        return $this->dayOfWeek;
    }

    /**
     * Set timeStart.
     *
     * @param \DateTime $timeStart
     *
     * @return DishTime
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart.
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd.
     *
     * @param \DateTime $timeEnd
     *
     * @return DishTime
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd.
     *
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set dish.
     *
     * @param \DishBundle\Entity\Dish|null $dish
     *
     * @return DishTime
     */
    public function setDish(\DishBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish.
     *
     * @return \DishBundle\Entity\Dish|null
     */
    public function getDish()
    {
        return $this->dish;
    }
}
