<?php


namespace DishBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use DishBundle\Entity\Composition;

class CompositionRepository extends EntityRepository
{

    /**
     * @param Composition $composition
     * @return Composition[]
     */
    public function findUsedIn(Composition $composition) {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.ingredients', 'ci')
            ->where('ci.ingredient = :id')
            ->setParameters([
                'id' => $composition->getId(),
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Composition[]
     */
    public function findAll()
    {
        return $this->createQueryBuilder('c')
            ->where("c.isDeleted = false")
            ->andWhere("c.outputFinishedMass is not null")
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Composition[]
     */
    public function findByNameOrId($q)
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->where($qb->expr()->orX(
                $qb->expr()->like("c.id", ":q"),
                $qb->expr()->like("c.name", ":q")
            ))
            ->andWhere("c.isDeleted = false")
            ->setParameter("q", "%$q%");

        return $qb->getQuery()->getResult();
    }
}