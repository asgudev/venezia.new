<?php
/**
 * Created by IntelliJ IDEA.
 * User: User
 * Date: 24.04.2016
 * Time: 17:45
 */

namespace RestaurantBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', TextType::class,[
                'label' => 'ID',
                'required' => false,
                'attr' => [
                    'placeholder' => 'ID'
                ]
            ])
            ->add('datestart', DateType::class, [
                'label' => 'С',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('dateend', DateType::class, [
                'label' => 'По',
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('shop', EntityType::class, [
                'label' => 'Заведение',
                'class' => 'RestaurantBundle\Entity\Shop',
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => false,
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Поиск',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}