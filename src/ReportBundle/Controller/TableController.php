<?php

namespace ReportBundle\Controller;

use ReportBundle\Entity\Table;
use ReportBundle\Form\Type\TableType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/table")
 */
class TableController extends Controller
{
    /**
     * @Route("/list", name="table_list")
     */
    public function listTablesAction()
    {
        $table = new Table();

        $tables = $this->getDoctrine()->getRepository('ReportBundle:Table')->findAll();
        $form = $this->createForm(TableType::class, $table, [
            'action' => $this->generateUrl("table_create")

        ]);


        return $this->render('ReportBundle:Default:tableList.html.twig', [
            'tables' => $tables,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/create", name="table_create")
     */
    public function createTableAction(Request $request)
    {

        $table = new Table();
        $form = $this->createForm(TableType::class, $table, [
            'action' => $this->generateUrl("table_create")
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($table);
            $em->flush();
        }


        return $this->redirectToRoute('table_list');

    }

    /**
     * @Route("/{id}/show", name="show_table")
     */
    public function showTableAction(Request $request, Table $table)
    {

        return $this->render("@Report/Default/table.html.twig", [
            'table' => $table
        ]);
    }

}
