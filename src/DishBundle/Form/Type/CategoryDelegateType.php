<?php

namespace DishBundle\Form\Type;

use DishBundle\Entity\CategoryDelegate;
use DishBundle\Entity\DishCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryDelegateType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add("category", EntityType::class, [
                'class' => DishCategory::class,
                'choice_label' => 'name'
            ])
            ->add("delegate", EntityType::class, [
                'class' => 'RestaurantBundle\Entity\Shop',
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CategoryDelegate::class,
        ));
    }
}