<?php

namespace FrontendBundle\Entity;

use DishBundle\Entity\Dish;
use Doctrine\ORM\Mapping as ORM;
use OrderBundle\Entity\Order;


/**
 * @ORM\Entity(repositoryClass="FrontendBundle\Entity\Repository\WebOrderDishRepository")
 * @ORM\Table()
 */
class WebOrderDish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="FrontendBundle\Entity\WebOrder", inversedBy="dishes")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish", cascade={"persist"})
     */
    private $dish;

    /**
     * @ORM\Column(name="quantity", type="float", nullable=false)
     */
    private $quantity;

    /**
     * @ORM\Column(name="dish_price", type="float", nullable=false)
     */
    private $dishPrice;

    /**
     * @ORM\Column(name="dish_discount", type="float", nullable=false)
     */
    private $dishDiscount;

    public function __construct(Order $order = null)
    {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @param mixed $dish
     */
    public function setDish($dish)
    {
        $this->dish = $dish;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getDishPrice()
    {
        return $this->dishPrice;
    }

    /**
     * @param mixed $dishPrice
     */
    public function setDishPrice($dishPrice)
    {
        $this->dishPrice = $dishPrice;
    }

    /**
     * @return mixed
     */
    public function getDishDiscount()
    {
        return $this->dishDiscount;
    }

    /**
     * @param mixed $dishDiscount
     */
    public function setDishDiscount($dishDiscount)
    {
        $this->dishDiscount = $dishDiscount;
    }


    public function getCashSum()
    {
        return ($this->dishPrice * $this->quantity);
    }

    public function getDiscountSum()
    {
        return ($this->dishDiscount * $this->quantity);
    }

    public function getFullPrice()
    {
        return ($this->dishPrice + $this->dishDiscount);
    }

    public function getFullSum()
    {
        return ($this->getFullPrice() * $this->quantity);
    }
}