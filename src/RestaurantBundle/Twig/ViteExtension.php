<?php

namespace RestaurantBundle\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ViteExtension extends AbstractExtension
{
    private $isDev;

    public function __construct(string $environment)
    {
        $this->isDev = $environment === 'dev';
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('vite_entry_script_tags', [$this, 'viteEntryScriptTags'], ['is_safe' => ['html']])
        ];
    }

    public function viteEntryScriptTags(string $entry): string
    {
        if ($this->isDev) {
            return <<<HTML
<!doctype html>
<html lang="en">
  <head>
    <script type="module">
import RefreshRuntime from "http://localhost:5173/@react-refresh"
RefreshRuntime.injectIntoGlobalHook(window)
window.\$RefreshReg$ = () => {}
window.\$RefreshSig$ = () => (type) => type
window.__vite_plugin_react_preamble_installed__ = true
</script>

    <script type="module" src="http://localhost:5173/@vite/client"></script>

    <meta charset="UTF-8" />
    <link rel="icon" type="image/svg+xml" href="/vite.svg" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Vite + React + TS</title>
  </head>
  <body>
    <div id="root"></div>
    <script type="module" src="http://localhost:5173/src/main.tsx"></script>
  </body>
</html>
HTML;
        }

        return <<<HTML
<script type="module" src="/build/assets/{$entry}.js"></script>
HTML;
    }
}