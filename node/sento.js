const net = require('net');
const client = new net.Socket();
const Iconv = require('iconv').Iconv;
let bodyParser = require("body-parser");
let conv = new Iconv('utf-8', 'windows-1251');

//18-34
//17-47
//21-32

//20-24

//22-17 cash

const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'venezia_new'
});

connection.connect();


const express = require('express');
const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


app.post("/order", (request, response) => {
    sequence.seq = [];

    let __orderId = request.body.order;

    connection.query('SELECT od.*, d.* FROM `order_dish` od LEFT JOIN `dish` d ON d.id = od.dish_id WHERE `order_id` = ?', [__orderId], function (error, results, fields) {
        console.log(results);

        let sum = 0;

        results.forEach((__dish) => {
            //console.log(__dish)


            let tmp = [
                'D*5',
                __dish.dish_id.toString().padEnd(18, " "),
                'A',
                (__dish.dish_price * 100).toString().padStart(10, " "),
                (__dish.quantity * 1000).toString().padStart(10, " "),
                (__dish.dish_price * __dish.quantity * 100).toString().padStart(10, " "),
                '                ',

            ];

            let data = [
                Buffer.from(tmp.join('')),
                converter(__dish.name.toString().padStart(35, ' '))
            ];


            sequence.seq.push(sequenceCommand(data));

            sum += (__dish.dish_price * __dish.quantity);

        });


        sum = Math.trunc(sum * 100);
        console.log(sum);

        sequence.seq.push(
            sequenceCommand(
                ['D*4',
                    (sum).toString().padStart(10, " "), //итого
                    (sum).toString().padStart(10, " "), //наличные
                    '          ', //чек Оо
                    '          ', //карта
                    '          ', //купон
                    '          ', //кредит
                ].join('')
            )
        );

        console.log(sequence.seq);
        // sequence.seq.push(data);

        sequence.start();

    });

    console.log(request.body);

    response.send('order');
});

app.post("/report", (request, response) => {
    sequence.seq = [];
    if (request.body.type === '1') {
        sequence.seq.push(sequenceCommand('D*R1'));
        response.send('report z')
    } else if (request.body.type === '2') {
        sequence.seq.push(sequenceCommand('D*R2'));
        response.send('report x')
    }
    sequence.start();
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
});


converter = (string) => {
    let a = conv.convert(string);

    return a;
    console.log(a)

    let __str = [];

    for (let i = 0; i < a.length; i++) {
        __str.push(a[i].toString(16))
    }

    return hex2a(__str.join(''));


}

function hex2a(hexx) {
    let hex = hexx.toString();//force conversion
    let str = '';
    for (let i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}


convertCrc = (crc) => {
    crc = crc.toString(16).toUpperCase();
    return crc.padStart(4, '0');
};


bufferAppend = (buffer, add) => {
    let a = new Buffer([add]);
    return Buffer.concat([buffer, a]);
};


calcBufferCRC = (packet) => {
    let __crc = 0;

    for (let i = 0; i < packet.length; i++) {
        c = packet[i];
        __crc ^= c << (i % 9);
    }
    return convertCrc(__crc);
};


let packetType = {
    'START': 'S',
    'DATA': 'D',
};

let command = {
    'STX': 0x02,
    'ETX': 0x03,
    'EOT': 0x04,
    'ENQ': 0x05,
    'ACK': 0x06,
    'WACK': 0x09,
    'RVI': 0x40,
};

let transmissionType = {
    '0': "0".charCodeAt(0),
};


let packet = [
    //command.STX,
    packetType.START,
    "*",
    "1",
    '                  ',
    '                  ',

];


let sendData = [
    new Buffer([0x02]),
    Buffer.from(packet.join(''), 'utf-8'),
    new Buffer([0x03]),
];

let crc = calcBufferCRC(Buffer.concat([sendData[1], sendData[2]]))

sendData.push(Buffer.from(crc, 'utf-8'));

let sendBuffer = Buffer.concat(sendData);

sendCommand = (command, comment) => {
    console.log(comment);
    client.write(command);
}


sequenceCommand = (__command) => {
    return {
        step: 0,
        start: function () {
            this.step = 1;
            this.send();
        },
        send: function () {
            if (this[this.step].crc) {

                let __data;
                if (typeof __command === 'object') {
                    __data = Buffer.concat(__command);
                } else {
                    __data = Buffer.from(this[this.step].data.join(''));
                }

                let __packet = Buffer.concat([__data, new Buffer([0x03])]);
                let crc = Buffer.from(calcBufferCRC(__packet), 'utf-8');
                let __crcPacket = Buffer.concat([new Buffer([0x02]), __packet, crc,]);
                console.log(__crcPacket);
                client.write(__crcPacket);
            } else {
                let __data = this[this.step].data;
                console.log(__data);
                client.write(new Buffer([__data]));
            }
        },
        next: function () {
            if (this[this.step].end === true) {
                sequence.next();
                return;
            }

            this.step += 1;
            this.send();
        },
        1: {
            'data': command.ENQ,
            'success': command.ACK,
            'crc': false,
            'error': (data) => {
                console.log(data);
            }
        }, //ENQ
        15: {
            'data': [
                'S*1                                    ',
            ],
            'crc': true,
            'success': command.ACK,
            'error': (data) => {
                console.log(data);
            }
        }, //START PACKET
        2: {
            'data': [
                __command
            ],
            'success': command.WACK,
            'crc': true,
            'error': (data) => {
                console.log(data);
            }
        }, //DATA PACKET
        3: {
            'data': command.EOT,
            'success': command.EOT,
            'crc': false,
            'end': true,
            'error': (data) => {
                console.log(data.toString());
            }
        }, //EOT
    }
};

let __command = false;

let sequence = {
    step: 0,
    start: function () {
        __command = this.seq[this.step];
        __command.start();
    },
    next: function () {
        this.step += 1;
        if (this.seq[this.step] !== undefined) {
            __command = this.seq[this.step];
            __command.start();
        } else {
            client.destroy();
        }
    },
    seq: [
        // открытие смены
        //sequenceCommand('D*C0         0'),


        // нефискальный
        // sequenceCommand('D*N0                                                '),
        // sequenceCommand([Buffer.from('D*N1 ', 'utf-8'), converter('ПРИВЕТ МИР'), Buffer.from('!!                                  ')]),
        // sequenceCommand('D*N1HELLO WORLD !!                                  '),
        // sequenceCommand('D*N1HELLO WORLD !!                                  '),
        // sequenceCommand('D*N3                                                '),
        // sequenceCommand('D*N2HELLO WORLD   !!                                  '),
        // sequenceCommand('D*N2HELLO WORLD !!                                  '),
        // sequenceCommand('D*N2HELLO WORLD !!                                  '),
        // sequenceCommand('D*N3                                                '),
        // sequenceCommand('D*N6                                                '),

        // продажа
        //
        //
        // sequenceCommand(
        //     [   'D*4',
        //         '      3704', //итого
        //         '      3704', //нал
        //         '          ', //чек Оо
        //         '          ', //карта
        //         '          ', //купон
        //         '          ', //кредит
        //     ].join('')
        // ),


        //x отчет


        // sequenceCommand('D*R1'),
        //sequenceCommand('S*0                                    '),
        //  sequenceCommand('Sp0                                   '),
    ]
};


client.connect(5001, '192.168.1.3', () => {
    console.log('Connected');
});

receiveErrorSeqGenerator = () => {
    return {
        step: 0,
        start: function () {
            __command = receiveErrorSeq;
            this.step = 1;
            this.send();
        },
        send: function () {
            if (this[this.step].crc) {

                let __data;
                if (typeof __command === 'object') {
                    __data = Buffer.concat(__command);
                } else {
                    __data = Buffer.from(this[this.step].data.join(''));
                }


                let __packet = Buffer.concat([__data, new Buffer([0x03])]);
                let crc = Buffer.from(calcBufferCRC(__packet), 'utf-8');
                let __crcPacket = Buffer.concat([new Buffer([0x02]), __packet, crc,]);

                console.log(__crcPacket);
                client.write(__crcPacket);
            } else {
                let __data = this[this.step].data;
                console.log(__data);
                client.write(new Buffer([__data]));
            }
        },
        next: function () {
            if (this[this.step].end === true) {
                sequence.next();
                return;
            }

            this.step += 1;
            this.send();
        },
        1: {
            'data': command.EOT,
            'success': command.ENQ,
            'crc': false,
            'error': (data) => {
                console.log(data);
            }
        }, //ENQ
        2: {
            'data': command.ACK,
            'crc': false,
            'success': command.ACK,
            'error': (data) => {
                console.log(data);
            }
        }, //START PACKET
        3: {
            'data': [
                __command
            ],
            'success': command.WACK,
            'crc': true,
            'error': (data) => {
                console.log(data);
            }
        }, //DATA PACKET
        4: {
            'data': command.EOT,
            'success': command.EOT,
            'crc': false,
            'end': true,
            'error': (data) => {
                console.log(data.toString());
            }
        }, //EOT
    }
};
receiveDataSeqGenerator = () => {
    return {
        step: 0,
        start: function () {
            __command = receiveDataSeq;
            this.step = 1;
            this.send();
        },
        send: function () {
            if (this[this.step].crc) {

                let __data;
                if (typeof __command === 'object') {
                    __data = Buffer.concat(__command);
                } else {
                    __data = Buffer.from(this[this.step].data.join(''));
                }


                let __packet = Buffer.concat([__data, new Buffer([0x03])]);
                let crc = Buffer.from(calcBufferCRC(__packet), 'utf-8');
                let __crcPacket = Buffer.concat([new Buffer([0x02]), __packet, crc,]);

                console.log(__crcPacket);
                client.write(__crcPacket);
            } else {
                let __data = this[this.step].data;
                console.log(__data);
                client.write(new Buffer([__data]));
            }
        },
        next: function () {
            if (this[this.step].end === true) {
                sequence.next();
                return;
            }

            this.step += 1;
            this.send();
        },
        1: {
            'data': command.ACK,
            'success': command.ACK,
            'crc': false,
            'error': (data) => {
                console.log(data);
            }
        }, //ENQ
        2: {
            'data': command.ACK,
            'crc': false,
            'success': command.ACK,
            'error': (data) => {
                console.log(data);
            }
        }, //START PACKET
        3: {
            'data': [
                __command
            ],
            'success': command.WACK,
            'crc': true,
            'error': (data) => {
                console.log(data);
            }
        }, //DATA PACKET
        4: {
            'data': command.EOT,
            'success': command.EOT,
            'crc': false,
            'end': true,
            'error': (data) => {
                console.log(data.toString());
            }
        }, //EOT
    }
};

let receiveErrorSeq = receiveErrorSeqGenerator();
let receiveDataSeq = receiveDataSeqGenerator();


client.on('data', (data) => {
    switch (data[0]) {
        case command.ACK:
            console.log('ack');
            console.log(data);

            __command.next();
            break;

        case command.WACK:
            console.log('wack');
            console.log(data);

            break;

        case command.RVI:
            console.log('rvi');
            console.log(data);
            receiveErrorSeq.start();
            break;

        case command.ENQ:
            console.log('enq');
            console.log(data);
            receiveDataSeq.start();
            break;

        case command.EOT:
            console.log('eot');
            console.log(data);
            __command.next();
            break;

        default:
            console.log('other data');
            console.log(data);
            console.log(data.toString());
    }

});