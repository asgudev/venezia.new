<?php

namespace DishBundle\Form\Type;

use DishBundle\Entity\Composition;
use DishBundle\Entity\Dish;
use DishBundle\Entity\DishCategory;
use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Service\BitToArray;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class DishType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $shops = $options["shops"];

        $builder
            ->add("name", TextType::class, [
                'label' => 'Название блюда',
            ])
            ->add("price", NumberType::class, [
                'label' => 'Базовая цена',
                'attr' => [
                    'placeholder' => 'Базовая цена',
                    'class' => 'customPrice md-input',
                ]
            ])
            ->add("dishMetas", CollectionType::class, [
                'entry_type' => DishMetaType::class,
                'label' => 'Специальные цены',
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false,
            ])
            ->add("shops", EntityType::class, [
                'label' => 'Доступно в заведениях',
                'query_builder' => function (EntityRepository $qb) use ($shops) {
                    return $qb->createQueryBuilder('s')
                        ->where('s.id IN (:shops)')
                        ->setParameters([
                            'shops' => $shops,
                        ]);
                },
                'class' => Shop::class,
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'choice_label' => 'name',
            ])
            ->add("category", EntityType::class, [
                'label' => 'Категория',
                'class' => DishCategory::class,
                'multiple' => false,
                'expanded' => false,
                'required' => true,
                'choice_label' => 'name',
            ])
            ->add('times', CollectionType::class, [
                'label' => 'Время работы',
                'entry_type' => DishTimeType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                'by_reference' => false,
            ])
            ->add("weight", TextType::class, [
                'label' => "Порция",
                'required' => false,
            ])
            ->add("composition", Select2EntityType::class, [
                "multiple" => false,
                "label" => "Тех. карта",
                "class" => Composition::class,
                "text_property" => "name",
                'minimum_input_length' => 2,
                'page_limit' => 10,
                'placeholder' => 'Тех. карта',
                'remote_route' => 'search_composition',
            ])
            ->add("description", TextareaType::class, [
                'label' => "Описание блюда",
                'required' => false,
            ])
            ->add("imageFile", FileType::class, [
                'label' => 'Фото',
                'required' => false,
                'file_path' => 'image',
            ])
            ->add("ingredients", CollectionType::class, [
                'entry_type' => DishIngredientType::class,
                'label' => 'Ингредиенты',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false,
            ])
            ->add('isInMenu', CheckboxType::class, [
                'label' => 'Отображать в меню',
                'required' => false,
            ])
            ->add("orderTypes", ChoiceType::class, [
                'label' => "Типы заказов",
                'multiple' => true,
                'expanded' => true,
                'choices' => Dish::ORDER_TYPE
            ])
            ->add('isExportable', CheckboxType::class, [
                'label' => 'Выгружать в 1с',
                'required' => false,
            ])
            ->add('ignoreDiscount', CheckboxType::class, [
                'label' => 'Игнорировать скидку',
                'required' => false,
            ])
            ->add('isWeight', CheckboxType::class, [
                'label' => 'Весовое',
                'required' => false,
            ])
            ->add("delete", SubmitType::class, [
                'label' => 'Удалить',
            ]);

        $builder
            ->get('orderTypes')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    $bittoarray = new BitToArray();
                    return $bittoarray->bitToArray($output, Dish::ORDER_TYPE);
                },
                function ($input) {
                    $bitmask = 0;
                    foreach ($input as $mask) {
                        $bitmask += $mask;
                    }
                    return $bitmask;
                }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Dish::class,
            'shops' => null,
        ));
    }
}
