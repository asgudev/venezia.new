const https = require('https');
const request = require('request');
let psl = require('psl');

function getDomainRedirect(domain) {

    request({url: "https://" + domain, followRedirect: false}, function (err, res, body) {
        if (!err) {
            let redirect_to = res.headers.location;
            redirect_to = psl.get(extractHostname(redirect_to));
            connection.query('UPDATE `sites` SET `redirect_to`=? WHERE `domain` = ?', [
                redirect_to,
                domain,
            ], function (error, results, fields) {
                console.log(error);
            });
        } else {
            console.log(err);
        }
    });
}

function extractHostname(url) {
    let hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

getDomainRedirect("sexonsk.life");