<?php


namespace TerminalBundle\Promotions;


use DishBundle\Entity\Dish;
use Doctrine\ORM\EntityManagerInterface;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use RestaurantBundle\Entity\Shop;

class NForOne
{

    private $category;
    private $number;
    private $affectedDishes;
    private $em;


    public function __construct($category, $number, EntityManagerInterface $em)
    {

        $this->category = $category;
        $this->number = explode("/", $number);
        $this->em = $em;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function execute(Order $order)
    {
        $this->affectedDishes = $this->getAffectedDishes($order);

        if (count($this->affectedDishes) == 0) return $order;

        $splitByOne = $this->splitByOne($this->affectedDishes);

        $discounted = $this->applyPromotion($splitByOne);
        $grouped = $this->groupByPriceAndDiscount($discounted);

        $order = $this->applyToOrder($order, $grouped);

        return $order;
    }

    private function getAffectedDishes(Order $order)
    {
        return array_filter($order->getDishes()->toArray(), function (OrderDish $orderDish) {
            return $orderDish->getDish()->getCategory()->getId() == $this->category;
        });
    }

    /**
     * @param OrderDish[] $dishes
     * @return array
     */
    private function splitByOne($dishes)
    {
        $separated = [];
        foreach ($dishes as $orderDish) {

            for ($i = 1; $i <= $orderDish->getQuantity(); $i++) {
                $separated[] = [
                    'dishId' => $orderDish->getDish()->getId(),
                    'dishName' => $orderDish->getDish()->getName(),
                    'orderDishId' => $orderDish->getId(),
                    'delegateTo' => $orderDish->getDelegate() ? $orderDish->getDelegate()->getId() : null,
                    'price' => $orderDish->getFullPrice(),
                    'discount' => 0,
                ];
            }
        }

        usort($separated, [$this, 'sortByPrice']);

        return $separated;
    }

    private function applyPromotion($dishes)
    {


        $discountGroups = [];

        $discountedInGroup = $this->number[0] - $this->number[1];
        $nonDiscountedInGroup = $this->number[1];

        while (count($dishes) !== 0) {
            $group = [
                "free" => [],
                "paid" => [],
            ];

            for ($i = 0; $i < $nonDiscountedInGroup; $i++) {
                if (count($dishes) > 0) {
                    array_push($group["paid"], array_shift($dishes));
                }
            }

            if (count($dishes) > 0) {
                array_push($group["free"], array_pop($dishes));
            }

            array_push($discountGroups, $group);
        }

        foreach ($discountGroups as &$group) {
            $sumFree = array_reduce($group["free"], function ($a, $b) {
                return $a + $b["price"];
            }, 0);

            $count = count($group["free"]) + count($group["paid"]);

            $discount = floor($sumFree / $count * 100) / 100;

            $group["paid"] = array_map(function ($item) use ($discount) {
                $item["discount"] = $discount;
                return $item;
            }, $group["paid"]);

            $group["free"] = array_map(function ($item) use ($sumFree, $discount, $group) {
                $discountLeft = $sumFree - $discount * count($group["paid"]);
                $item["discount"] = $discountLeft;
                return $item;
            }, $group["free"]);
        }

        $promoDishesAfterDiscount = array_map(function ($group) {
            return array_merge($group["free"], $group["paid"]);
        }, $discountGroups);

        $promoDishesAfterDiscount = array_reduce($promoDishesAfterDiscount, function ($carry, $item) {
            return array_merge($carry, $item);
        }, []);

        return $promoDishesAfterDiscount;
    }

    private function groupByPriceAndDiscount($dishes)
    {

        $grouped = [];

        foreach ($dishes as $__dish) {

            $key = $__dish['dishName'];
            if (!array_key_exists($key, $grouped)) {
                $grouped[$key] = [];
            }

            $priceKey = (string)$__dish['discount'];
            if (!array_key_exists($priceKey, $grouped[$key])) {
                $grouped[$key][$priceKey] = [
                    'dishId' => $__dish['dishId'],
                    'dishName' => $__dish['dishName'],
                    'orderDishId' => $__dish['orderDishId'],
                    'delegateTo' => $__dish['delegateTo'],
                    'quantity' => 1,
                    'price' => $__dish['price'],
                    'discount' => $__dish['discount'],
                ];
            } else {
                $grouped[$key][$priceKey]['quantity'] += 1;
            }
        }

        return $grouped;
    }

    private function applyToOrder(Order $order, $discounts)
    {

        foreach ($order->getDishes() as $orderDish) {
            if (in_array($orderDish, $this->affectedDishes)) {
//                $order->removeDish($orderDish);
//                $this->em->remove($orderDish);
                $orderDish->setOrder(null);
            }
        }

        foreach ($discounts as $__dishGroup) {
            foreach ($__dishGroup as $price => $__dish) {
                $orderDish = new OrderDish();
                $orderDish->setDish($this->em->getReference(Dish::class, $__dish["dishId"]));
                $orderDish->setQuantity($__dish["quantity"]);
                $orderDish->setDishPrice($__dish["price"] - $__dish["discount"]);
                $orderDish->setDishDiscount($__dish["discount"]);

                if ($__dish["delegateTo"]) {
                    $orderDish->setDelegate($this->em->getReference(Shop::class, $__dish["delegateTo"]));
                }

                // add delegation

                $order->addDish($orderDish);
                $orderDish->setParentOrder($order);
            }
        }

        return $order;
    }

    private function sortByPrice($a, $b)
    {
        if ($a['price'] == $b['price']) {
            return 0;
        }

        return ($b['price'] < $a['price']) ? -1 : 1;
    }
}
