<?php

namespace FrontendBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use FrontendBundle\Entity\WebShopArticle;


class WebShopArticleRepository extends EntityRepository
{

    /**
     * @param $category
     * @return WebShopArticle[]
     */
    public function findByCategory($category) {
        return $this->createQueryBuilder('wa')
            ->addSelect('ws')
            ->addSelect('s')
            ->where("wa.category = :category")
            ->leftJoin("wa.shop" , "ws")
            ->leftJoin("ws.shop", 's')
            ->andWhere('wa.isDeleted = false')
            ->andWhere('wa.isPublished = true')
            ->setParameters([
                'category' => $category
            ])
            ->getQuery()
            ->getResult();
    }
}