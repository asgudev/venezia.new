function initMap() {
    if (document.getElementById('google-map') != undefined) {
        var latitude = 52.0929625,
            longitude = 23.7133437,
            map_zoom = 12;

        var locations = [
            ['Caffe Venezia', 52.0834194, 23.6885787, 1],
            ['Caffe Italia', 52.0872551, 23.6961331, 2],
            ['Metromilano', 52.0899185, 23.6944667, 3],
            ['Urban Soup', 52.0909858, 23.6937707, 4],
            ['Pizza Al Taglio', 52.1013982, 23.6558455, 5],
            ['Pizza Al Taglio', 52.0918908, 23.6934345, 6],
            ['Venezia Fusion', 52.0863745, 23.6838645, 7],
            ['Pizza Al Taglio 24', 52.1032147, 23.7865752, 8],
            ['Draft House', 52.0916679, 23.6935022, 9],
            ['GranCaffe', 52.0899185, 23.6944667, 15],
        ];

        var marker_url = 'img/template-assets/map-marker1.png';

        var main_color = '#bd8c60',
            saturation_value = -100,
            brightness_value = -1;

        var style = [
            {
                elementType: "labels",
                stylers: [
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "poi",
                elementType: "labels",
                stylers: [
                    {visibility: "on"}
                ]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels',
                stylers: [
                    {visibility: "on"}
                ]
            },
            {
                featureType: "road.local",
                elementType: "http://gourmet.arenaofthemes.com/js/labels.icon",
                stylers: [
                    {visibility: "on"}
                ]
            },
            {
                featureType: "road.arterial",
                elementType: "http://gourmet.arenaofthemes.com/js/labels.icon",
                stylers: [
                    {visibility: "on"}
                ]
            },
            {
                featureType: "road",
                elementType: "geometry.stroke",
                stylers: [
                    {visibility: "on"}
                ]
            },
            {
                featureType: "transit",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "poi",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "poi.government",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "poi.sport_complex",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "poi.attraction",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "poi.business",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "transit",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "transit.station",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "landscape",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]

            },
            {
                featureType: "road",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "road.highway",
                elementType: "http://gourmet.arenaofthemes.com/js/geometry.fill",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            },
            {
                featureType: "water",
                elementType: "geometry",
                stylers: [
                    {hue: main_color},
                    {visibility: "on"},
                    {lightness: brightness_value},
                    {saturation: saturation_value}
                ]
            }
        ];

        var map_options = {
            center: new google.maps.LatLng(latitude, longitude),
            zoom: map_zoom,
            panControl: false,
            zoomControl: false,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: style,
        };

        var map = new google.maps.Map(document.getElementById('google-map'), map_options);

        for (let i = 0; i < locations.length; i++) {
            let marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                visible: true,
                icon: marker_url,
            });
            let infowindow = new google.maps.InfoWindow({
                content: locations[i][0]
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        }


        //add custom buttons for the zoom-in/zoom-out on the map
        function CustomZoomControl(controlDiv, map) {
            //grap the zoom elements from the DOM and insert them in the map
            var controlUIzoomIn = document.getElementById('cd-zoom-in'),
                controlUIzoomOut = document.getElementById('cd-zoom-out');
            controlDiv.appendChild(controlUIzoomIn);
            controlDiv.appendChild(controlUIzoomOut);

            // Setup the click event listeners and zoom-in or out according to the clicked element
            google.maps.event.addDomListener(controlUIzoomIn, 'click', function () {
                map.setZoom(map.getZoom() + 1)
            });
            google.maps.event.addDomListener(controlUIzoomOut, 'click', function () {
                map.setZoom(map.getZoom() - 1)
            });
        }

        var zoomControlDiv = document.createElement('div');
        var zoomControl = new CustomZoomControl(zoomControlDiv, map);

        //insert the zoom div on the top left of the map
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
    }
}

  