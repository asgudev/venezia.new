<?php

namespace ControlBundle\Controller;

use TerminalBundle\Entity\DailyReport;
use DishBundle\Entity\Dish;
use RestaurantBundle\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderChange;
use OrderBundle\Entity\OrderDish;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ReportController extends Controller
{
    const API_PATH = 'https://admin.venezia.by';

    /**
     * @Route("/report/custom", name="report_custom")
     */
    public function customReport(Request $request)
    {
        $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findCustom();


        return $this->render('@Control/Report/custom.html.twig', [
            'orders' => $orders,
        ]);
    }


    /**
     * @Route("/report/changes/{dateStart}/{dateEnd}/{shop}", name="report_daily_changes")
     */
    public function dailyChangesAction(Request $request, $dateStart = 'now', $dateEnd = 'now', $shop = null)
    {
        $changesData = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findByDate($dateStart, $dateEnd, $shop);

        $changes = [];
        $deletions = [];

        foreach ($changesData as $__data) {
            $changes[$__data->getOrderDish()->getParentOrder()->getShop()->getName()]['changesDishes'][] = $__data;
        }

        $deletedOrders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findDeletedOrders($dateStart, $dateEnd);

        foreach ($deletedOrders as $__order) {
            $changes[$__order->getShop()->getName()]['deletedOrders'][] = $__order;
        }

        $changesByUserData = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findGroupedByUser($dateStart, $dateEnd);

        $changesByUser = [];
        $dates = [];

        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod (new \DateTime($dateStart), $interval, new \DateTime($dateEnd));

        $dates = array();
        foreach ($period as $key => $__date) {
            $dates[$__date->format('Y-m-d')] = [];
        }

        foreach ($changesByUserData as $__change) {
            $user = $__change->getOrderDish()->getParentOrder()->getUser();

            if (!array_key_exists($user->getName(), $changesByUser)) {
                $changesByUser[$user->getName()] = $dates;
            }
            $changesByUser[$user->getName()][$__change->getDatetime()->format('Y-m-d')][] = $__change;
        }


        return $this->render('@Control/Report/reportChanges.html.twig', [
            'changesData' => $changes,
            'changesByUser' => $changesByUser,
            'deletedData' => $deletions,
            'dateStart' => (new \DateTime($dateStart)),
            'dateEnd' => (new \DateTime($dateEnd))
        ]);

    }

    /**
     * @Route("/report/deletions/{dateStart}/{dateEnd}/{shop}",
     *     name="report_daily_deletions_period",
     *     defaults={"dateStart" = null, "dateEnd" = null, "shop" = 0}
     * )
     */
    public function dailyDeletionAction(Request $request, $dateStart = 'now', $dateEnd = 'now', $shop = 0)
    {

        if ($shop != 0) {
            $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find($shop);
        }

        $deletedOrders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findDeletedOrdersByPeriod($dateStart, $dateEnd, $shop);

        $deleted = [];

        foreach ($deletedOrders as $__order) {
            $deleted[$__order->getShop()->getName()][] = $__order;
        }

        return $this->render('@Control/Report/reportDeletions.html.twig', [
            'deleted' => $deleted,
            'dateStart' => (new \DateTime($dateStart)),
            'dateEnd' => (new \DateTime($dateEnd)),
            'shop' => $shop,
        ]);

    }


    /**
     * @Route("/report/{shop}/dish/{dish}/daily", name="daily_dish_sales_report")
     */
    public function dailyDishSales(Request $request, Shop $shop, Dish $dish)
    {
        $dailyDishData = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findDailyDish($shop, $dish);
        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();

        return $this->render('@Control/Report/dailyDishSales.html.twig', [
            'data' => $dailyDishData,
            'dish' => $dish,
            'shop' => $shop,
            'shops' => $shops,
        ]);
    }

    /**
     * @Route("/report/shop/{id}/ingredientdate", name="ingredient_daily_report_by_date")
     */
    public function dailyIngredientByDate(Request $request, Shop $shop)
    {

        $date = '2016-12-28';
        $dataCash = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportCashData($shop);
        $dataIngr = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportIngrData($shop);

        $barCash = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportCashData($shop, null, 38);
        $barData = [];
        foreach ($barCash as $cash) {
            $barData[$cash['oCreatedAtDate']] = $cash['odCashSum'];
        }


        $kitchenCash = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportCashData($shop, null, 22);
        $kitchenData = [];
        foreach ($kitchenCash as $cash) {
            $kitchenData[$cash['oCreatedAtDate']] = $cash['odCashSum'];
        }

        $desertCash = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportCashData($shop, 29);
        $desertData = [];
        foreach ($desertCash as $cash) {
            $desertData[$cash['oCreatedAtDate']] = $cash['odCashSum'];
        }


        $icecreamCash = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportCashData($shop, 31);
        $icecreamData = [];
        foreach ($icecreamCash as $cash) {
            $icecreamData[$cash['oCreatedAtDate']] = $cash['odCashSum'];
        }


        $sushiCash = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportCashData($shop, 33);
        $sushiData = [];
        foreach ($sushiCash as $cash) {
            $sushiData[$cash['oCreatedAtDate']] = $cash['odCashSum'];
        }

        $pizzaCash = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findMarcoReportCashData($shop, 32);
        $pizzaData = [];
        foreach ($pizzaCash as $cash) {
            $pizzaData[$cash['oCreatedAtDate']] = $cash['odCashSum'];
        }


        $dataArr = [];
        $ingrData = [];

        foreach ($dataIngr as $ingr) {
            $ingrData[$ingr['oCreatedAtDate']][$ingr['dName']] = $ingr['diQuantity'];
        }
        foreach ($dataIngr as $ingr) {
            $ingrData[$ingr['oCreatedAtDate']][$ingr['dName']] = $ingr['diQuantity'];
        }


        foreach ($dataCash as $dailyData) {
            $dataArr[$dailyData['oCreatedAtDate']] = [
                'big' => [
                    'new' => 0,
                    'waste' => 0,
                    'used' => array_key_exists('Шарик большой', $ingrData[$dailyData['oCreatedAtDate']]) ? $ingrData[$dailyData['oCreatedAtDate']]['Шарик большой'] : 0,
                    'balance' => 0,
                ],
                'small' => [
                    'new' => 0,
                    'waste' => 0,
                    'used' => array_key_exists('Шарик малый', $ingrData[$dailyData['oCreatedAtDate']]) ? $ingrData[$dailyData['oCreatedAtDate']]['Шарик малый'] : 0,
                    'balance' => 0,
                ],
                'cash' => [
                    'bar' => array_key_exists($dailyData['oCreatedAtDate'], $barData) ? $barData[$dailyData['oCreatedAtDate']] : 0,
                    'kitchen' => array_key_exists($dailyData['oCreatedAtDate'], $kitchenData) ? $kitchenData[$dailyData['oCreatedAtDate']] : 0,
                    'pizza' => array_key_exists($dailyData['oCreatedAtDate'], $pizzaData) ? $pizzaData[$dailyData['oCreatedAtDate']] : 0,
                    'icecream' => array_key_exists($dailyData['oCreatedAtDate'], $icecreamData) ? $icecreamData[$dailyData['oCreatedAtDate']] : 0,
                    'sushi' => array_key_exists($dailyData['oCreatedAtDate'], $sushiData) ? $sushiData[$dailyData['oCreatedAtDate']] : 0,
                    'desert' => array_key_exists($dailyData['oCreatedAtDate'], $desertData) ? $desertData[$dailyData['oCreatedAtDate']] : 0,
                    'total' => $dailyData['odCashSum'],
                ]
            ];
        }


        return $this->render('@Control/Report/ingrReportByDate.html.twig', [
            'shop' => $shop,
            'shops' => $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll(),
            'data' => $dataArr,
        ]);
    }

    /**
     * @Route("/report/shop/{id}/ingredient", name="ingredient_daily_report")
     */
    public function dailyIngredient(Request $request, Shop $shop)
    {
        $reports = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findShopReportsFromDate($shop, new \DateTime('2016-12-31'));

        $data = [];
        //$report->getDateStarted()->format("d.m.Y")
        foreach ($reports as $report) {
            $data[$report->getId()] = [
                'date' => $report->getName(),
                'big' => [
                    'new' => 0,
                    'waste' => 0,
                    'used' => 0,
                    'balance' => 0,
                ],
                'small' => [
                    'new' => 0,
                    'waste' => 0,
                    'used' => 0,
                    'balance' => 0,
                ],
                'cash' => [
                    'bar' => 0,
                    'kitchen' => 0,
                    'pizza' => 0,
                    'icecream' => 0,
                    'sushi' => 0,
                    'desert' => 0,
                    'total' => 0,
                ]
            ];

            $ingredientData = $report->getIngredients();

            foreach ($ingredientData as $ingrData) {
                if ($ingrData->getIngredient()->getId() == 1) {
                    $data[$report->getId()]['big']['new'] = $ingrData->getIncome();
                    $data[$report->getId()]['big']['waste'] = $ingrData->getWaste();
                    $data[$report->getId()]['big']['balance'] = $ingrData->getBalance();
                }
                if ($ingrData->getIngredient()->getId() == 2) {
                    $data[$report->getId()]['small']['new'] = $ingrData->getIncome();
                    $data[$report->getId()]['small']['waste'] = $ingrData->getWaste();
                    $data[$report->getId()]['small']['balance'] = $ingrData->getBalance();
                }
            }


            $dishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findDishesByReport($report);
            $pizzas = [];
            $pizzaSum = 0;
            foreach ($dishes as $orderDish) {
                if (($orderDish->getDish()->getCategory()->getId() == 32) && ($orderDish->getDish()->getId() != 11177)) {
                    $pizzaSum += $orderDish->getCashSum();
                    $pizzas[] = $orderDish;
                }

                if ($orderDish->getDish()->getCategory()->getId() == 32) continue;

                //bar
                if ($orderDish->getDish()->getCategory()->getParent()->getId() == 38) {
                    $data[$report->getId()]['cash']['bar'] += $orderDish->getCashSum();
                    $data[$report->getId()]['cash']['total'] += $orderDish->getCashSum();
                    continue;
                }

                //icecream
                if ($orderDish->getDish()->getCategory()->getId() == 31) {
                    $data[$report->getId()]['cash']['icecream'] += $orderDish->getCashSum();
                    $data[$report->getId()]['cash']['total'] += $orderDish->getCashSum();
                    continue;
                }

                //sushi
                if ($orderDish->getDish()->getCategory()->getId() == 33) {
                    $data[$report->getId()]['cash']['icecream'] += $orderDish->getCashSum();
                    $data[$report->getId()]['cash']['total'] += $orderDish->getCashSum();
                    continue;
                }

                //desert
                if ($orderDish->getDish()->getCategory()->getId() == 29) {
                    $data[$report->getId()]['cash']['desert'] += $orderDish->getCashSum();
                    $data[$report->getId()]['cash']['total'] += $orderDish->getCashSum();
                    continue;
                }

                $data[$report->getId()]['cash']['kitchen'] += $orderDish->getCashSum();
                $data[$report->getId()]['cash']['total'] += $orderDish->getCashSum();
            }
            /*
                        dump($pizzas);
                        $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findRawPizzaByReport($report);
                        dump($pizzas);die();
            */
            $pizzaData = $this->getPizzaData($pizzas, []);


            foreach ($pizzaData['ingr'] as $ingr => $quantity) {
                if ($ingr == "Шарик большой") {
                    $data[$report->getId()]['big']['used'] = $quantity;
                }
                if ($ingr == "Шарик малый") {
                    $data[$report->getId()]['small']['used'] = $quantity;
                }
            }
            $data[$report->getId()]['cash']['pizza'] = $pizzaSum;
            $data[$report->getId()]['cash']['total'] += $pizzaSum;
        }

        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();


        return $this->render('@Control/Report/ingrReport.html.twig', [
            'shop' => $shop,
            'reports' => $reports,
            'data' => $data,
            'shops' => $shops,
            //'pizza' => $pizzaData,
            /*'pizza' => $pizzaData,
            'yesterday' => $previousData,
            'form' => $form->createView()*/
        ]);

    }

    /**
     * @Route("/registry/{id}/daily/print/{date}", name="print_daily_registry", defaults={"id" = 1})
     */
    public function dailyRegistryPrint(Request $request, Shop $shop, $date = 'today')
    {
        $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findAllByDate($shop, $date);

        $ordersByUser = [];
        $terminalOrders = [];
        $invoiceOrders = [];

        /**
         * @var Order $order
         */
        foreach ($orders as $order) {
            $user = $order->getUser();
            if ($order->getStatus() == Order::STATUS_DELETED) continue;


            if (($order->getCashType() == Order::CASHTYPE_INVOICE) || ($order->getCashType() == Order::CASHTYPE_INTERNAL)) {
                $invoiceOrders[$order->getId()] = [
                    'order' => $order,
                    'total' => 0,
                    'sum' => [
                        'КУХНЯ' => 0,
                        'БАР' => 0,
                        'total' => 0,
                    ]
                ];
            } elseif ($order->getCashType() == Order::CASHTYPE_CASH) {
                if (!array_key_exists($user->getId(), $ordersByUser)) {
                    $ordersByUser[$user->getId()] = [
                        'name' => $user->getName(),
                        'total' => 0,
                        'totalCash' => 0,
                        'orders' => [],
                    ];
                }
                $ordersByUser[$user->getId()]['orders'][$order->getId()] = [
                    'order' => $order,
                    'sum' => [
                        'КУХНЯ' => 0,
                        'БАР' => 0,
                        'cash' => 0,
                        'card' => 0,
                    ]
                ];
            }

            foreach ($order->getDishes() as $orderDish) {
                $rootCategory = $orderDish->getDish()->getCategory()->getParent()->getName();
                if ($order->getCashType() == Order::CASHTYPE_INVOICE) {
                    $invoiceOrders[$order->getId()]['sum'][$rootCategory] += $orderDish->getCashSum();
                    $invoiceOrders[$order->getId()]['sum']['total'] += $orderDish->getCashSum();
                } elseif ($order->getCashType() == Order::CASHTYPE_CASH) {
                    $ordersByUser[$user->getId()]['orders'][$order->getId()]['sum'][$rootCategory] += $orderDish->getCashSum();


                    $ordersByUser[$user->getId()]['total'] += $orderDish->getCashSum();
                }
            }
            $ordersByUser[$user->getId()]['orders'][$order->getId()]['sum']['cash'] += $order->getClientMoneyCash();
            $ordersByUser[$user->getId()]['orders'][$order->getId()]['sum']['card'] += $order->getClientMoneyCard();

            $ordersByUser[$user->getId()]['totalCash'] += ($order->getClientMoneyCash() + $order->getClientMoneyCard());

        }

        return $this->render('@Control/Report/dailyRegistryPrint.html.twig', [
            'userReport' => $ordersByUser,
            'terminal' => $terminalOrders,
            'invoice' => $invoiceOrders,
            'date' => $date,
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/registry/{id}/report/print", name="print_report_registry", defaults={"id" = 1})
     */
    public function reportRegistryPrint(Request $request, DailyReport $report)
    {
        $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findOrdersByReport($report);

        $ordersByUser = [];
        $terminalOrders = [];
        $invoiceOrders = [];


        /**
         * @var Order $order
         */
        foreach ($orders as $order) {
            $user = $order->getUser();
            if ($order->getStatus() == Order::STATUS_DELETED) continue;

            if (($order->getCashType() == Order::CASHTYPE_INVOICE) || ($order->getCashType() == Order::CASHTYPE_INTERNAL)) {
                $invoiceOrders[$order->getId()] = [
                    'order' => $order,
                    'total' => 0,
                    'sum' => [
                        'КУХНЯ' => 0,
                        'БАР' => 0,
                        'total' => 0,
                    ]
                ];
            } elseif ($order->getCashType() == Order::CASHTYPE_CASH) {
                if (!array_key_exists($user->getId(), $ordersByUser)) {
                    $ordersByUser[$user->getId()] = [
                        'name' => $user->getName(),
                        'total' => 0,
                        'orders' => [],
                    ];
                }
                $ordersByUser[$user->getId()]['orders'][$order->getId()] = [
                    'order' => $order,
                    'sum' => [
                        'КУХНЯ' => 0,
                        'БАР' => 0,
                    ]
                ];
            }

            foreach ($order->getDishes() as $orderDish) {
                $rootCategory = $orderDish->getDish()->getCategory()->getParent()->getName();
                if ($order->getCashType() == Order::CASHTYPE_INVOICE) {
                    $invoiceOrders[$order->getId()]['sum'][$rootCategory] += $orderDish->getCashSum();
                    $invoiceOrders[$order->getId()]['sum']['total'] += $orderDish->getCashSum();
                } elseif ($order->getCashType() == Order::CASHTYPE_CASH) {
                    $ordersByUser[$user->getId()]['orders'][$order->getId()]['sum'][$rootCategory] += $orderDish->getCashSum();
                    $ordersByUser[$user->getId()]['total'] += $orderDish->getCashSum();
                }
            }
        }

        return $this->render('@Control/Report/dailyRegistryPrint.html.twig', [
            'userReport' => $ordersByUser,
            'terminal' => $terminalOrders,
            'invoice' => $invoiceOrders,
            'date' => $report->getDateStarted(),
            'shop' => $report->getShop(),
        ]);
    }

    /**
     * @Route("/report/{id}/clear", name="clear_daily_bugs")
     */
    public function clearDailyBugsAction(Request $request, Shop $shop)
    {
        $em = $this->getDoctrine()->getManager();
        $dishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findAllDeletedParentDishes($shop);
        $dishesChilds = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findAllDeletedChildDishes($shop);
        $changes = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findAll();
        /*
                $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findBy([
                    'user' => 24,
                    'shop' => $shop
                ]);*/

        /**
         * @var OrderChange $orderChange
         */
        foreach ($changes as $orderChange) {
            $em->remove($orderChange);
        }
        $em->flush();


        foreach ($dishesChilds as $orderDish) {
            if (($orderDish->getParentDish() != null) && ($orderDish->getParentDish()->getId() == $orderDish->getId())) {
                $orderDish->setParentDish(null);
                $em->persist($orderDish);
                $em->flush();
            }
        }
        $em->flush();


        foreach ($dishes as $orderDish) {
            if (($orderDish->getParentDish() != null) && ($orderDish->getParentDish()->getId() == $orderDish->getId())) {
                $orderDish->setParentDish(null);
                $em->persist($orderDish);
                $em->flush();
            }
        }
        $em->flush();


        foreach ($dishesChilds as $orderDish) {

            $em->remove($orderDish);
        }
        $em->flush();

        foreach ($dishes as $orderDish) {

            $em->remove($orderDish);
        }
        $em->flush();


        return $this->redirectToRoute('admin_order_daily_report', [
            'id' => $shop->getId(),
        ]);
    }

    /**
     * @Route("/report/shop/{id}/daily/{date}", name="admin_order_daily_report", defaults={"id" = 1})
     */
    public function orderDailyReportAction(Request $request, Shop $shop, $date = null)
    {
        if (!$this->getUser()->getAdminShops()->contains($shop))
            throw new AccessDeniedHttpException();

        if (!$date) {
            $date = (new \DateTime())->format('d.m.Y');
        }

        $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findReportByDate($shop, $date);
        if ($previousReport) {
            $report = $previousReport[0];
            $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findOrdersByReport($report);
            $orderDishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findDishesByReport($report);
            $deletions = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findByReport($report);
            $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaByReport($report, false);
            $ingredientsTransfer = $this->getDoctrine()->getRepository('DishBundle:IngredientTransfer')->findAllPizzaInDateRange($report->getDateStarted(), $report->getDateClosed());
            $transportEvents = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAllPizzaInDateRange($report->getDateStarted(), $report->getDateClosed());
            $receiveEvents = $this->getDoctrine()->getRepository("UserBundle:UserEvent")->findReceiveEvents($report->getDateStarted(), $report->getDateClosed());

        } else {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($shop);
            $report = $previousReport[0];
            $date = $report->getDateClosed();
            $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findAllFromDate($shop, $date);
            $orderDishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findAllDishesFromDate($shop, $date);
            $deletions = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findAllFromDate($shop, $date);
            $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaFromDate($shop, $date);

            $ingredientsTransfer = $this->getDoctrine()->getRepository('DishBundle:IngredientTransfer')->findAllPizzaInDateRange($report->getDateClosed(), new \DateTime());
            $transportEvents = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAllPizzaInDateRange($report->getDateClosed(), new \DateTime());
            $receiveEvents = $this->getDoctrine()->getRepository("UserBundle:UserEvent")->findReceiveEvents($report->getDateClosed(), new \DateTime());

        }


        /**
         * @var Shop[] $shops
         */
        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();

        $total = [
            'sum' => 0,
            'minus' => 0
        ];

        $money = [
            'cash' => 0,
            'terminal' => 0,
            'invoice' => 0,
            'guarantee' => 0,
            'lost' => 0,
            'КУХНЯ' => [
                'cash' => 0,
                'terminal' => 0,
                'invoice' => 0,
                'guarantee' => 0,
                'lost' => 0,
            ],
            'БАР' => [
                'cash' => 0,
                'terminal' => 0,
                'invoice' => 0,
                'guarantee' => 0,
                'lost' => 0,
            ],

        ];
        $moneyBar = 0;

        $deletedDishes = [];

        /**
         * @var OrderChange $deletion
         */
        foreach ($deletions as $deletion) {
            $deletedDishes[] = $deletion->getOrderDish();
        }

        $losted = [];
        //dump($money);
        /**
         * @var OrderDish $orderDish
         */
        foreach ($orderDishes as $orderDish) {
            if (in_array($orderDish, $deletedDishes)) continue;
            if ($orderDish->getParentOrder()->getUser()->getId() == 24) continue;

            if (($orderDish->getParentOrder()->getUser()->getId() == 13) || ($orderDish->getParentOrder()->getUser()->getId() == 37)) {
                $moneyBar += $orderDish->getCashSum();
                continue;
            }

            $lost = false;
            if ($orderDish->getOrder() !== null) {
                $order = $orderDish->getOrder();
            } else {
                $lost = true;
                $order = $orderDish->getParentOrder();
            }

            $cat = $orderDish->getDish()->getCategory()->getParent()->getName();
            $dishSum = $orderDish->getCashSum();
            //dump($dishSum);
            switch ($order->getCashType()) {
                case Order::CASHTYPE_CASH:
                    if (!$lost) {
                        $money[$cat]['cash'] += $dishSum;
                        $total['sum'] += $dishSum;
                    }
                    break;
                case Order::CASHTYPE_INVOICE:
                    if (!$lost) {
                        $money['invoice'] += $dishSum;
                        $total['sum'] += $dishSum;
                    }
                    break;
            }
            if ($lost) {
                $money[$cat]['lost'] += $dishSum;
                $name = $orderDish->getDish()->getName();
                if (!array_key_exists($name, $losted)) {
                    $losted[$name] = [
                        'sum' => 0,
                        'quantity' => 0,
                    ];
                }
                $losted[$name]['quantity'] += ($orderDish->getQuantity());
                $losted[$name]['sum'] += $orderDish->getCashSum();
                $losted[$name]['cat'] = $orderDish->getDish()->getCategory()->getName();
            }
        }


        $ordersByUser = [];

        /**
         * @var Order $order
         */
        foreach ($orders as $order) {
            $user = $order->getUser();
            $money['cash'] += $order->getClientMoneyCash();
            $money['terminal'] += $order->getClientMoneyCard();

            if (!array_key_exists($user->getId(), $ordersByUser)) {
                $ordersByUser[$user->getId()] = [
                    'name' => $user->getName(),
                    'orders' => [],
                ];
            }
            $ordersByUser[$user->getId()]['orders'][$order->getId()] = [
                'order' => $order,
                'sum' => [
                    'КУХНЯ' => 0,
                    'БАР' => 0,
                ]
            ];

            foreach ($order->getDishes() as $orderDish) {
                $rootCategory = $orderDish->getDish()->getCategory()->getParent()->getName();
                $ordersByUser[$user->getId()]['orders'][$order->getId()]['sum'][$rootCategory] += $orderDish->getCashSum();
            }
        }

        $pizzaData = $this->getPizzaData($pizzas, $deletedDishes);

        $transfer = [];
        foreach ($shops as $__shop) {
            $transfer[$__shop->getId()] = [
                'name' => $__shop->getName(),
                'send' => 0,
                'send_date' => false,
                'send_log' => [],
                'delivered_date' => false,
                'received' => null,
                'received_log' => [],

                'delivery_error' => false,
                'receive_error' => false,

                'received_date' => false,
                'balance_prev' => 0,
                'balance_now' => 0,
            ];
        }

        foreach ($ingredientsTransfer as $__transfer) {
            if ($__transfer->getTo() == null || $__transfer->getTo()->getShop() == null) continue;

            $transfer[$__transfer->getTo()->getShop()->getId()]["send"] += $__transfer->getQuantity();
            $transfer[$__transfer->getTo()->getShop()->getId()]["send_date"] = $__transfer->getDate();

            $transfer[$__transfer->getTo()->getShop()->getId()]["send_log"][] = [
                'date' => $__transfer->getDate(),
                'quantity' => $__transfer->getQuantity(),
                'from' => $__transfer->getFrom()->getShop()->getName(),
            ];


            if ($__transfer->getFrom()->getShop()->getId() == $__transfer->getTo()->getShop()->getId()) continue;
            $transfer[$__transfer->getFrom()->getShop()->getId()]["send"] -= $__transfer->getQuantity();

            $transfer[$__transfer->getFrom()->getShop()->getId()]["send_log"][] = [
                'date' => $__transfer->getDate(),
                'quantity' => -$__transfer->getQuantity(),
                'from' => $__transfer->getTo()->getShop()->getName(),
            ];

        }


        foreach ($transportEvents as $__event) {
            $transfer[$__event->getShop()->getId()]["delivered_date"] = $__event->getDate();
//            dump($transfer[$__event->getShop()->getId()]["delivered_date"]);die();


        }


        foreach ($receiveEvents as $__event) {
            $payload = json_decode($__event->getPayload(), true);

            $transfer[$__event->getShop()->getId()]["received"] += $payload["income"] - $payload["waste"];
            $transfer[$__event->getShop()->getId()]["received_date"] = $__event->getDate();

            if ($payload == null) {
                continue;
            }

            if (!array_key_exists("balance_now", $payload)) {
                continue;
            }

            if ($payload["balance_now"] != 0) {
                $transfer[$__event->getShop()->getId()]["balance_now"] = $payload["balance_now"];
            }

            $transfer[$__event->getShop()->getId()]["balance_prev"] = $payload["balance_prev"];

            $__receiveEvent = [
                'date' => $__event->getDate(),
                'receive_error' => false,
                'payload' => $payload,
            ];

            /**
             * @var \DateTime $prevDate
             */
            $prevDate = $transfer[$__event->getShop()->getId()]["delivered_date"];
            if (!$prevDate)
                $prevDate = $transfer[$__event->getShop()->getId()]["send_date"];


            if ($payload["income"] == 0 && $transfer[$__event->getShop()->getId()]["send"] == 0)
                continue;

            if (!$prevDate) {
                continue;
            }


            $diff = abs($prevDate->getTimestamp() - $__event->getDate()->getTimestamp());
            $__receiveEvent["diff"] = $diff;
            $__receiveEvent["diff_hrs"] = sprintf('%02d:%02d', ($diff / 3600), ($diff / 60 % 60));

            $__receiveEvent["receive_error"] = $diff >= 7200;

            if ($__receiveEvent["receive_error"]) {

                $ch = curl_init(self::API_PATH . $this->generateUrl('api_pizza_sell'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
                    'start' => $prevDate->format("Y-m-d H:i:s"),
                    'end' => $__event->getDate()->format("Y-m-d H:i:s"),
                    'shop' => $__event->getShop()->getId(),
                ]));
                $response = json_decode(curl_exec($ch), true);

                $__receiveEvent["pizza_cnt"] = $response["cnt"];
            }

            $transfer[$__event->getShop()->getId()]["received_log"][] = $__receiveEvent;
        }


        return $this->render("@Control/Report/dailyReport.html.twig", [
            'orders' => $orders,
            'orderDishes' => $orderDishes,
            'deletions' => $deletions,
            'money' => $money,
            'moneyBar' => $moneyBar,
            'total' => $total,
            'userReport' => $ordersByUser,
            'losted' => $losted,
            'pizza' => $pizzaData,
            'date' => $date,
            'shops' => $shops,
            'shop' => $shop,
            'lastReport' => $report,
            'transfer' => $transfer,
        ]);
    }

    /**
     * @Route("/report/{id}/current", name="admin_shop_current_report", defaults={"id" = 1})
     */
    public function currentReportAction(Request $request, Shop $shop, $date = 'today')
    {
        $lastReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($shop)[0];

        $date = $lastReport->getDateClosed();

        $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findAllByDate($shop, $date);
        $orderDishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findAllDishesByDate($shop, $date);
        $deletions = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findAllByDate($shop, $date);
        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();

        $total = [
            'sum' => 0,
            'minus' => 0
        ];

        $money = [
            'БАР' => [
                'cash' => 0,
                'terminal' => 0,
                'invoice' => 0,
                'guarantee' => 0,
                'lost' => 0,
            ],
            'КУХНЯ' => [
                'cash' => 0,
                'terminal' => 0,
                'invoice' => 0,
                'guarantee' => 0,
                'lost' => 0,
            ],
        ];
        $moneyBar = 0;

        $deletedDishes = [];
        /**
         * @var OrderChange $deletion
         */
        foreach ($deletions as $deletion) {
            $deletedDishes[] = $deletion->getOrderDish();
        }

        $losted = [];

        /**
         * @var OrderDish $orderDish
         */
        foreach ($orderDishes as $orderDish) {
            if (in_array($orderDish, $deletedDishes)) continue;
            if ($orderDish->getParentOrder()->getUser()->getId() == 24) continue;

            if (($orderDish->getParentOrder()->getUser()->getId() == 13) || ($orderDish->getParentOrder()->getUser()->getId() == 37)) {
                $moneyBar += $orderDish->getCashSum();
                continue;
            }

            $lost = false;
            if ($orderDish->getOrder() !== null) {
                $order = $orderDish->getOrder();
            } else {
                $lost = true;
                $order = $orderDish->getParentOrder();
            }
            $cat = $orderDish->getDish()->getCategory()->getParent()->getName();
            $dishSum = $orderDish->getCashSum();

            switch ($order->getCashType()) {
                case Order::CASHTYPE_CASH:
                    if (!$lost) {
                        $money[$cat]['cash'] += $dishSum;
                        $total['sum'] += $dishSum;
                    }
                    break;
                case Order::CASHTYPE_INVOICE:
                    if (!$lost) {
                        $money[$cat]['invoice'] += $dishSum;
                        $total['sum'] += $dishSum;
                    }
                    break;
            }
            if ($lost) {
                $money[$cat]['lost'] += $dishSum;
                $name = $orderDish->getDish()->getName();
                if (!array_key_exists($name, $losted)) {
                    $losted[$name] = [
                        'sum' => 0,
                        'quantity' => 0,
                    ];
                }
                $losted[$name]['quantity'] += ($orderDish->getQuantity());
                $losted[$name]['sum'] += $orderDish->getCashSum();
            }
        }


        $ordersByUser = [];

        /**
         * @var Order $order
         */
        foreach ($orders as $order) {
            $user = $order->getUser();
            if (!array_key_exists($user->getId(), $ordersByUser)) {
                $ordersByUser[$user->getId()] = [
                    'name' => $user->getName(),
                    'orders' => [],
                ];
            }
            $ordersByUser[$user->getId()]['orders'][$order->getId()] = [
                'order' => $order,
                'sum' => [
                    'КУХНЯ' => 0,
                    'БАР' => 0,
                ]
            ];

            foreach ($order->getDishes() as $orderDish) {
                $rootCategory = $orderDish->getDish()->getCategory()->getParent()->getName();

                /*if (!array_key_exists($rootCategory, $ordersByUser[$user->getId()]['orders'][$order->getId()])) {
                    $ordersByUser[$user->getId()]['orders'][$order->getId()][$rootCategory] = 0;
                }*/
                $ordersByUser[$user->getId()]['orders'][$order->getId()]['sum'][$rootCategory] += $orderDish->getCashSum();

            }
        }

        $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaByDate($shop, $date);

        $pizzaData = $this->getPizzaData($pizzas, $deletedDishes);

        return $this->render("@Control/Report/dailyReport.html.twig", [
            'orders' => $orders,
            'orderDishes' => $orderDishes,
            'deletions' => $deletions,
            'money' => $money,
            'moneyBar' => $moneyBar,
            'total' => $total,
            'userReport' => $ordersByUser,
            'losted' => $losted,
            'pizza' => $pizzaData,
            'date' => $date,
            'shops' => $shops,
            'shop' => $shop,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/shop/{id}/report/list", name="report_list_by_shop")
     */
    public function reportListAction(Request $request, Shop $shop)
    {
        $reports = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findBy([
            'shop' => $shop,
        ]);

        dump($reports);
        die();
    }

    private function getPizzaData($pizzas, $deleted, $report = null)
    {

        $pizzaData = [
            'dishes' => [],
            'sum' => 0,
            'quantity' => 0,
            'ingr' => [],
            'delivery' => [
                'menuby' => 0,
                'justeat' => 0,
                'scope' => 0,
                'delivio' => 0,
            ],
            'takeaway' => [
                'sum' => 0,
                'quantity' => 0,
                'ingr' => 0,
            ],
        ];

        if ($report) {
            foreach ($report->getIngredients() as $ingr) {
                $pizzaData['ingr'][$ingr->getIngredient()->getName()] = 0;
            }
        }

        /**
         * @var OrderDish $pizza
         */
        foreach ($pizzas as $pizza) {
            if ((in_array($pizza, $deleted)) && ($pizza->getOrder() == null)) {
                //dump(123);
                continue;
            }
            if (mb_substr($pizza->getDish()->getName(), 0, 7) == 'Добавка') {
                //dump(234);
                continue;
            }
            if (!$pizza->getDish()->getIsExportable()) continue;

            $name = $pizza->getDish()->getName();
            if (!array_key_exists($name, $pizzaData['dishes'])) {
                $pizzaData['dishes'][$name] = [
                    'sum' => 0,
                    'quantity' => 0,
                    'ingr' => 0,
                ];
            }
            if ($pizza->getDish()->getId() == 11177) {
                $pizzaData['box'] += ($pizza->getQuantity());
                continue;
            }

            if ($pizza->getOrder() && $pizza->getOrder()->getClient()) {
                if (in_array($pizza->getOrder()->getClient()->getId(), [2, 207,307, 367])) {
                    $quantity = $pizza->getDish()->getIngredients()->first()->getQuantity()  * $pizza->getQuantity();
                    switch ($pizza->getOrder()->getClient()->getId()) {
                        case 2:
                            $pizzaData["delivery"]["menuby"] += $quantity;
                            break;
                        case 207:
                            $pizzaData["delivery"]["justeat"] += $quantity;
                            break;
                        case 307:
                            $pizzaData["delivery"]["scope"] += $quantity;
                            break;
                        case 367:
                            $pizzaData["delivery"]["delivio"] += $quantity;
                            break;
                    }
                }
            }


            if (!array_key_exists(0, $pizza->getDish()->getIngredients()->toArray())) {
                dump($pizza->getDish());
                die();
                continue;
            }

            if ($pizza->getParentOrder()->getTable()->getId() == 189) {
                $pizzaData['takeaway']['sum'] += ($pizza->getCashSum());
                $pizzaData['takeaway']['ingr'] += ($pizza->getDish()->getIngredients()->first()->getQuantity() * $pizza->getQuantity());
                $pizzaData['takeaway']['quantity'] += ($pizza->getQuantity());
            }


            $pizzaData['dishes'][$name]['quantity'] += ($pizza->getQuantity());
            $pizzaData['dishes'][$name]['sum'] += ($pizza->getDishPrice() * $pizza->getQuantity());
            $pizzaData['dishes'][$name]['ingr'] += ($pizza->getDish()->getIngredients()->first()->getQuantity() * $pizza->getQuantity());

            $pizzaData['sum'] += ($pizza->getDishPrice() * $pizza->getQuantity());
            $pizzaData['quantity'] += ($pizza->getQuantity());

            $dishIngredient = $pizza->getDish()->getIngredients()->first()->getIngredient();

            if (!array_key_exists($dishIngredient->getName(), $pizzaData['ingr'])) $pizzaData['ingr'][$dishIngredient->getName()] = 0;

            $pizzaData['ingr'][$dishIngredient->getName()] += ($pizza->getDish()->getIngredients()->first()->getQuantity() * $pizza->getQuantity());
        }

        return $pizzaData;
    }

    /**
     * @param Request $request
     * @Route("/report/delivery/{dateStart}/{dateEnd}", name="report_delivery")
     */
    public function reportDeliveryAction(Request $request, $dateStart = "first day of this month", $dateEnd = "now")
    {
        $dateStart = new \DateTime($dateStart);
        $dateEnd = new \DateTime($dateEnd);
//        if ((new \DateTime())->format('d') >= 16) {
//            $dateStart = new \DateTime(date("Y-m-01"));
//            $dateEnd = new \DateTime(date("Y-m-16"));
//        } else {
//            $dateStart = (new \DateTime(date("Y-m-16")))->modify('-1 month');
//            $dateEnd = (new \DateTime(date("Y-m-01")));
//        }


        $clients = $this->getDoctrine()->getRepository('RestaurantBundle:Client')->findBy([
            'trackInDelivery' => true,
        ]);

        $orders = [];
        $ordersByClient = [];
//        $ordersByShop = $this->getDoctrine()->getRepository('OrderBundle:Order')->findByShopDates($shop, $dateStart, $dateEnd);
        foreach ($clients as $client) {
            $ordersByClient = array_merge($ordersByClient, $this->getDoctrine()->getRepository('OrderBundle:Order')->findByClientDates($client, $dateStart, $dateEnd));
        }
//        foreach ($ordersByShop as $__order) {
////if ($__order->getDishes()[0] == null) var_dump($__order->getId());
//            $from = $__order->getDishes()[0]->getDelegate()->getName();
//            $orders[$from][$__order->getCreatedAt()->format('d-m-Y')][] = $__order;
//        }

        $clientTable = [];

        $dates = [];
        /** @var Order $__order */
        $total = [
            'sum' => 0,
            'pizza' => 0,
        ];
        foreach ($ordersByClient as $__order) {
            $orders[$__order->getClient()->getName()][$__order->getShop()->getName()][$__order->getCreatedAt()->format('d-m-Y')][] = $__order;
            $date = $__order->getCreatedAt()->format("m-d");
            $dates[] = $date;

            if (!array_key_exists($__order->getClient()->getName(), $clientTable))
                $clientTable[$__order->getClient()->getName()] = [
                    'data' => [],
                    'sum' => 0,
                    'pizza' => 0,
                ];

            if (!array_key_exists($date, $clientTable[$__order->getClient()->getName()]['data']))
                $clientTable[$__order->getClient()->getName()]['data'][$date] = [
                    'sum' => 0,
                    'pizza' => 0,
                ];



            $clientTable[$__order->getClient()->getName()]['data'][$date]['sum'] += $__order->getFullSum();

            foreach ($__order->getDishes() as $orderDish) {
                if (in_array($orderDish->getDish()->getCategory()->getId(), [32,82])) {
                    $clientTable[$__order->getClient()->getName()]['data'][$date]['pizza'] += $orderDish->getQuantity();
                    $clientTable[$__order->getClient()->getName()]['pizza'] += $orderDish->getQuantity();
                    $total['pizza'] += $orderDish->getQuantity();
                }
            }

            $clientTable[$__order->getClient()->getName()]['sum'] += $__order->getFullSum();
            $total['sum'] += $__order->getFullSum();

            ksort($orders[$__order->getClient()->getName()][$__order->getShop()->getName()]);
        }

        $dates = array_unique($dates);
        sort($dates);

//        dump($clientTable);die();

        return $this->render('@Control/Report/delivery.html.twig', [
            'ordersData' => $orders,
            'dates' => $dates,
            'clientTable' => $clientTable,
            'total' => $total,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/report/{id}/delete", name="report_delete")
     */
    public function reportDeleteAction(Request $request, DailyReport $report)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($report);
        $em->flush();

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @param Request $request
     * @Route("/shop/{shop}/pizza2/{dateStart}/{dateEnd}", name="report2_pizza_shop")
     */
    public function reportOrderPizza2ShopAction(Request $request, Shop $shop, $dateStart = null, $dateEnd = null)
    {

        if ($dateStart) {
            $dateStart = new \DateTime($dateStart);
        } else {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($shop)[0];
            $dateStart = $previousReport->getDateClosed();
        }

        $dateEnd = new \DateTime($dateEnd !== null ? $dateEnd : 'now');

        $pizzaData = [];
        $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findReport2($shop, $dateStart, $dateEnd);
        foreach ($pizzas as $pizza) {
            $pizzaData[$pizza->getOrder()->getLocalId()][] = $pizza;
        }
        ksort($pizzaData);


        return $this->render('@Control/Report/pizzaReport.html.twig', [
            'shop' => $shop,
            'pizza' => $pizzaData,
            'delegated' => [],
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);

    }

    /**
     * @param Request $request
     * @Route("/shop/{shop}/pizza/{dateStart}/{dateEnd}", name="report_pizza_shop")
     */
    public function reportOrderPizzaShopAction(Request $request, Shop $shop, $dateStart = null, $dateEnd = null)
    {

        if ($dateStart) {
            $dateStart = new \DateTime($dateStart);
        } else {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($shop)[0];
            $dateStart = $previousReport->getDateClosed();
        }

        $dateEnd = new \DateTime($dateEnd !== null ? $dateEnd : 'now');

        $pizzaData = [];
        $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaByPeriod($shop, $dateStart, $dateEnd);
        foreach ($pizzas as $pizza) {
            $pizzaData[$pizza->getOrder()->getLocalId()][] = $pizza;
        }
        ksort($pizzaData);

        $delegatedPizza = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findDelegatedDishesByPeriodToShop($shop, $dateStart, $dateEnd);
        $delegated = [];
        foreach ($delegatedPizza as $pizza) {

            if (!array_key_exists($pizza->getOrder()->getShop()->getName(), $delegated))
                $delegated[$pizza->getOrder()->getShop()->getName()] = [];

            if (in_array($pizza->getDish()->getCategory()->getId(), [32, 82, 87])) {
                $delegated[$pizza->getOrder()->getShop()->getName()][$pizza->getOrder()->getLocalId()][] = $pizza;
            }
        }

        return $this->render('@Control/Report/pizzaReport.html.twig', [
            'shop' => $shop,
            'pizza' => $pizzaData,
            'delegated' => $delegated,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);

    }

    /**
     * @param Request $request
     * @Route("/shop/{id}/report/{name}", name="report_shop")
     */
    public function reportShopAction(Request $request, Shop $shop, $name = null)
    {
        if (!$this->getUser()->getAdminShops()->contains($shop))
            throw new AccessDeniedHttpException();

        $reports = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findBy([
            'shop' => $shop,
        ], [
            'dateStarted' => 'DESC'
        ], 30);

        if ($name) {
            /** @var DailyReport $report */
            $report = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findReportByDate($shop, $name);
            if (array_key_exists(0, $report)) $report = $report[0];
            else throw new NotFoundHttpException();
        } else {
            /** @var DailyReport $report */
            $report = $reports[0];
        }


        $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findOrdersByReport($report);
        $dishes = $this->getDoctrine()->getRepository("OrderBundle:OrderDish")->findDishesByReport($report);

        $total = 0;
        $totalA = [
            'terminal' => 0,
            'cash' => 0,
            'invoice' => 0,
            'КУХНЯ' => 0,
            'БАР' => 0,
        ];

        foreach ($dishes as $dish) {
            $rdish = $dish->getDish();
            $rcat = $rdish->getCategory()->getParent()->getName();
            $total += ($dish->getCashSum());
            if (in_array($dish->getOrder()->getCashType(), [Order::CASHTYPE_CASH])) {
                $totalA[$rcat] += ($dish->getCashSum());
            } else {
                $totalA['invoice'] += ($dish->getCashSum());
            }
        }

        foreach ($orders as $order) {
            if ($order->getStatus() !== 4) continue;
            $totalA["cash"] += $order->getClientMoneyCash();
            $totalA["terminal"] += $order->getClientMoneyCard();
        }

        $groupedDishes = $this->getDoctrine()->getRepository("OrderBundle:OrderDish")->findGroupedDishesByPeriod($report->getShop(), $report->getDateStarted(), $report->getDateClosed());
        //dump($groupedDishes);die();
        $dishesArray = [];
        $totalB = [
            'sum' => 0,
            'quantity' => 0,
            'discount' => 0,
        ];

        foreach ($groupedDishes as $index => $dishData) {
            $dish = $dishData['root'];
            $dishesArray[$dish->getDish()->getId()] = [
                'name' => $dish->getDish()->getName(),
                'price' => $dishData["odDishPrice"],
                'sum' => $dishData["odCashSum"],
                'discount' => $dishData["odDiscountSum"],
                'quantity' => $dishData["odDishQuantity"],
                'category' => $dish->getDish()->getCategory()->getName(),
            ];
            $totalB['sum'] += $dishData['odCashSum'];
            $totalB['quantity'] += $dishData['odDishQuantity'];
            $totalB['discount'] += $dishData['odDiscountSum'];
        }

        //dump($dishesArray);die();

        //$delegatedDishes = $this->getDoctrine()->getRepository('TerminalBundle:DelegateDish')->findDelegatedDishesByReport($report);

        //$orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findByShift($report);
        $changes = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findByReport($report);

        $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findPreviousReport($report);
        $previousReportIngres = [];
//var_dump($report->getId());die();
	if ($previousReport) {
        foreach ($previousReport->getIngredients() as $__ingr) {
            $previousReportIngres[$__ingr->getIngredient()->getId()] = $__ingr;
        }
}
        $deletions = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findByReport($report);
        $deletedDishes = [];
        /**
         * @var OrderChange $deletion
         */
        foreach ($deletions as $deletion) {
            $deletedDishes[] = $deletion->getOrderDish();
        }

        $delegatedDishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findDelegatedDishesByPeriod($report->getShop(), $report->getDateStarted(), $report->getDateClosed());
//dump($report);die();
        $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaByReport($report);

        foreach ($delegatedDishes as $orderDish) {
            //$orderDish = $delegatedDish->getOrderDish();

            if ($orderDish->getDish()->getCategory()->getId() == 32) {
                if ($orderDish->getDish()->getId() != 11177) {
                    $pizzas[] = $orderDish;
                }
            }
        }

        $pizzaData = $this->getPizzaData($pizzas, $deletedDishes, $report);


        $reportsData = [];

        foreach ($reports as $rep) {
            $reportsData[$rep->getId()] = [
                'report' => $rep,
                'sum' => $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findSumByReport($rep)['odSum'],
                'changes' => $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findChangesByReport($rep),
            ];
        }

        //dump($totalA);die();

        return $this->render('@Control/Report/reportShop.html.twig', [
            'shop' => $shop,
            'name' => $name,
            'total' => $total,
            'totalB' => $totalB,
            'summary' => $totalA,
            'previousReport' => $previousReport,
            'previousReportIngres' => $previousReportIngres,
            'report' => $report,
            'dishes' => $dishesArray,
            'orders' => $orders,
            'changes' => $changes,
            'reports' => $reports,
            'reportsData' => $reportsData,
            'pizza' => $pizzaData,
            'delegatedDishes' => $delegatedDishes,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/ingredient/updateAjax", name="ingredient_update_ajax")
     */
    public function updateIngredientAction(Request $request)
    {
        $ingredientData = $this->getDoctrine()->getRepository('ControlBundle:IngredientData')->find($request->get('ingr'));

        switch ($request->get('field')) {
            case 'income':
                $ingredientData->setIncome($request->get('newVal'));
                break;
            case 'waste':
                $ingredientData->setWaste($request->get('newVal'));
                break;
            case 'balance':
                $ingredientData->setBalance($request->get('newVal'));
                break;
        }

        $pizzas = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaByReport($ingredientData->getReport());
        $pizzaData = $this->getPizzaData($pizzas, [], $ingredientData->getReport());

        $row = $this->get("twig")->loadTemplate("@Control/Report/reportShop.html.twig")->renderBlock("ingredient_table", [
            'report' => $ingredientData->getReport(),
            'previousReport' => $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findPreviousReport($ingredientData->getReport()),
            'pizza' => $pizzaData,
        ]);

        $em = $this->getDoctrine()->getManager();
        $em->persist($ingredientData);
        $em->flush();

        return new JsonResponse([
            'row' => $row,
        ]);
    }
}
