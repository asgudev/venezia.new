<?php

namespace FrontendBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\Validator\Constraints\DateTime;
use OrderBundle\Entity\Order;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;


/**
 * @ORM\Entity(repositoryClass="FrontendBundle\Entity\Repository\WebShopRepository")
 * @ORM\Table()
 */
class WebShop
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\OneToMany(targetEntity="FrontendBundle\Entity\WebShopArticle", mappedBy="shop")
     */
    private $articles;

    /**
     * @ORM\Column(name="times", type="string", length=255, nullable=true)
     */
    private $times;

    /**
     * @ORM\Column(name="slug", type="string", nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="FrontendBundle\Entity\WebShopImage", mappedBy="shop")
     */
    private $images;

    /**
     * @ORM\Column(type="text", name="description", nullable=true)
     */
    private $description;


    /**
     * @ORM\Column(type="text", name="short_description", nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\Column(type="text", name="kitchen", nullable=true)
     */
    private $kitchen;

    /**
     * @ORM\Column(type="array", name="social", nullable=true)
     */
    private $social;

    /**
     * @ORM\Column(type="array", name="delivery", nullable=true)
     */
    protected $delivery;

    /**
     * @ORM\Column(name="seo_title", nullable=true, type="string")
     */
    private $seoTitle;

    /**
     * @ORM\Column(name="seo_keywords", nullable=true, type="string")
     */
    private $seoKeywords;

    /**
     * @ORM\Column(name="seo_description", nullable=true, type="string")
     */
    private $seoDescription;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     * @return WebShop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        return $this;
    }


    public function hasTypedImage($type)
    {

        foreach ($this->getImages() as $__image) {
            if ($__image->getType() == $type)
                return true;
        }

        return false;
    }

    public function getTypedImages($type)
    {
        $images = [];
        foreach ($this->getImages() as $__image) {
            if ($__image->getType() == $type)
                $images[] = $__image;
        }

        //        if (($image == false) || (!file_exists($image))) {
        //            switch ($type) {
        //                case WebShopImage::TYPE_LOGO:
        //                    $filename = $this->getImageUploadDir() . '/base-logo.jpg';
        //                    return $filename;
        //                case WebShopImage::TYPE_PANORAMA:
        //                    return $this->getTypedImage(WebShopImage::TYPE_PREVIEW);
        //            }
        //        }

        return $images;
    }

    public function getTypedImage($type, $obj = false)
    {
        $image = false;
        foreach ($this->getImages() as $__image) {
            if ($__image->getType() == $type) {
                if ($obj) {
                    return $__image;
                } else {
                    $image = $this->getImageUploadDir() . '/' . $__image->getImage();
                }
            }
        }

        if (($image == false) || (!file_exists($image))) {
            switch ($type) {
                case WebShopImage::TYPE_LOGO:
                    $filename = $this->getImageUploadDir() . '/base-logo.jpg';
                    return $filename;
                case WebShopImage::TYPE_PANORAMA:
                    return $this->getTypedImage(WebShopImage::TYPE_PREVIEW, true);
            }
        }

        return $image;
    }


    /**
     * @return mixed
     */
    public function getTimes()
    {
        return $this->times;
    }

    /**
     * @param mixed $times
     * @return WebShop
     */
    public function setTimes($times)
    {
        $this->times = $times;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return WebShop
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param mixed $image
     * @return WebShop
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return WebShop
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param mixed $shortDescription
     * @return WebShop
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param mixed $social
     * @return WebShop
     */
    public function setSocial($social)
    {
        $this->social = $social;
        return $this;
    }

    protected function getImageUploadDir()
    {
        return 'uploads/images/shop';
    }

    /**
     * @return WebShopImage[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     * @return WebShop
     */
    public function addImages($image)
    {
        $this->images[] = $image;
        return $this;
    }

    public function getSocialIconCode($link)
    {
        if (preg_match("#vk.com#", $link)) {
            return "59162";
        }
        if (preg_match("#facebook.com#", $link)) {
            return "59128";
        }
        if (preg_match("#instagram.com#", $link)) {
            return "59148";
        }
        if (preg_match("#menu.by#", $link)) {
            return "59148";
        }
        return null;
    }

    public function getName()
    {
        return $this->getShop()->getName();
    }

    /**
     * @return mixed
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @param mixed $seoTitle
     * @return WebShop
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * @param mixed $seoKeywords
     * @return WebShop
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * @param mixed $seoDescription
     * @return WebShop
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
        return $this;
    }

	/**
	 * 
	 * @return mixed
	 */
	function getDelivery() {
		return $this->delivery;
	}
	
	/**
	 * 
	 * @param mixed $delivery 
	 * @return WebShop
	 */
	function setDelivery($delivery): self {
		$this->delivery = $delivery;
		return $this;
	}


    /**
	 * @return mixed
	 */
	function getKitchen() {
		return $this->kitchen;
	}
	
	/**
	 * @param mixed $kitchen 
	 * @return WebShop
	 */
	function setKitchen($kitchen): self {
		$this->kitchen = $kitchen;
		return $this;
	}
}
