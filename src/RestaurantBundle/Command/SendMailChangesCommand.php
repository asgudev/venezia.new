<?php

namespace RestaurantBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class SendMailChangesCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('report:changes:mail')
            ->setDescription('Send daily changes to mail');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {

            $date = '-1 day';
            $doctrine = $this->getContainer()->get('doctrine');
            $renderer = $this->getContainer()->get('templating');

            $changesData = $doctrine->getRepository('OrderBundle:OrderChange')->findByDate($date);
            $changes = [];

            foreach ($changesData as $__data)
                $changes[$__data->getOrderDish()->getParentOrder()->getShop()->getName()]['changesDishes'][] = $__data;

            $deletedOrders = $doctrine->getRepository('OrderBundle:Order')->findDeletedOrders($date);

            foreach ($deletedOrders as $__order)
                $changes[$__order->getShop()->getName()]['deletedOrders'][] = $__order;

            $mail = $renderer->render("@Control/Templates/__changesTableTemplate.html.twig", [
                'changesData' => $changes,
            ]);

            $message = \Swift_Message::newInstance()
                ->setSubject('Daily changes ' . (new \DateTime($date))->format('d.m.Y'))
                ->setFrom('send@italbrest.com')
                ->setTo([
                    'margo@italbrest.com',
                    'natasha@italbrest.com',
                ])
                ->setBody(
                    $mail,
                    'text/html'
                );
            $this->getContainer()->get('mailer')->send($message);


        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}
