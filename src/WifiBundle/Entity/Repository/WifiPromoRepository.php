<?php

namespace WifiBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use WifiBundle\Entity\WifiConnect;
use WifiBundle\Entity\WifiPromo;

class WifiPromoRepository extends EntityRepository
{

    public function findSentPromos($dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('wp')
            ->select('COUNT(wp) as wpCount')
            ->addSelect('wp.id as wpId')
            ->where('wp.isDeleted = false')
            ->leftJoin('wp.connects', 'wpc')
            ->andWhere('wpc.connectedAt BETWEEN :date_start AND :date_end')
            ->setParameters([
                'date_start' => new \DateTime($dateStart),
                'date_end' => new \DateTime($dateEnd),
            ])
            ->addGroupBy('wp.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param WifiConnect $wifiConnect
     * @return WifiPromo
     */
    public function findActualPromo(WifiConnect $wifiConnect)
    {
        return $this->createQueryBuilder('wp')
            ->where('wp.router = :router')
            ->andWhere('wp.timeStart <= TIME(:now)')
            ->andWhere('wp.timeEnd >= TIME(:now)')
            ->andWhere('wp.isDeleted = false')
            ->setParameters([
                'router' => $wifiConnect->getRouter(),
                'now' => $wifiConnect->getConnectedAt(),
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

}