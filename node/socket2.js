const express = require("express");
const http = require("http");
const socketIo = require("socket.io");

const app = express();
app.use(express.json());
const server = http.createServer(app);
const io = socketIo(server);

const PORT = process.env.PORT || 3001;

class SocketServer {
    clients = new Map();
    nameToSocket = new Map();

    start() {
        this.setup();

        server.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
        });
    }

    sendToAll(target, data) {
        console.log(target, JSON.stringify(data));
        const targetSockets = this.nameToSocket.get(target);
        targetSockets?.forEach(targetSocket => {
            if (targetSocket) {
                targetSocket.emit("message", data);
            } else {
                socket.emit("error", {message: "Target client not found"});
            }
        });
    }

    setup() {
        app.get("/", (req, res) => {
            res.send("");
        });

        app.get("/status", (req, res) => {
            res.json([...this.clients.values()].sort((a,b) => a.name.localeCompare(b.name)));
        });

        app.post("/command", (req, res) => {
            const {command, target, data} = req.body;
            console.log(`Command to ${target}`, command, data);

            this.sendToAll(target, data);

            res.json({command, target, data});
        });

        io.on("connection", (socket) => {
            const clientIp = socket.handshake.address.split(":").at(-1);

            console.log(new Date(), `Client connected: ${socket.id} ${clientIp}`);

            socket.on("ping", () => {
                if (this.clients.get(socket)) {
                    this.clients.get(socket).lastSeen = new Date();
                }
            });

            socket.on("register", (data) => {

                const name = typeof data == "string" ? data : data.name;

                socket.name = name;
                this.clients.set(socket, {
                    name,
                    lastSeen: new Date(),
                    version: typeof data == "object" ? data.version : -1,
                    ip: clientIp,
                });
                if (!(this.nameToSocket.has(name))) {
                    this.nameToSocket.set(name, new Set());
                }
                this.nameToSocket.get(name).add(socket);

                socket.emit("registered", {id: socket.id, name});
                console.log(new Date(),`Client registered: ${name} (${socket.id}) ${clientIp}`);
            });

            socket.on("message", (req) => {
                const { target, data } = req;
                this.sendToAll(target, data);
            });

            socket.on("disconnect", () => {
                console.log(new Date(),`Client disconnected: ${socket.name} ${socket.id} ${clientIp}`);
                this.nameToSocket.get(socket.name)?.delete(socket);
                this.clients.delete(socket);
            });
        });
    }
}

const socketServer = new SocketServer();

socketServer.start();
