<?php

namespace DishBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use OrderBundle\Entity\OrderDish;
use RestaurantBundle\Entity\Printer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\DishCategoryRepository")
 * @ORM\Table(
 *     name="category",
 * )
 * @ORM\HasLifecycleCallbacks
 */
class DishCategory implements Translatable
{
    const ORDER_TYPE = [
        'Доставка' => 1,
        'Навынос' => 2,
        'На месте' => 4,
        'Заказ стола' => 8,
        'Заказ с сайта' => 16,
        'Yandex' => 32,
    ];
    /**
     * @ORM\Column(type="string", name="image", length=255, nullable=true)
     */
    public $image;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string")
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\Dish", mappedBy="category")
     * @ORM\OrderBy({"name" = "asc"})
     */
    private $dishes;
    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\DishCategory", mappedBy="parent")
     */
    private $children;
    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\DishCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    /**
     * @ORM\Column(name="is_terminal", type="boolean", nullable=true)
     */
    private $isTerminal;
    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\CategoryDiscount", mappedBy="category", cascade={"ALL"}, orphanRemoval=true)
     * @ORM\OrderBy({"day" = "ASC"})
     */
    private $discounts;
    /**
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;
    /**
     * @ORM\OneToMany(targetEntity="RestaurantBundle\Entity\Printer", mappedBy="category", cascade={"all"}, orphanRemoval=true)
     */
    private $printers;
    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\CategoryDelegate", mappedBy="category")
     */
    private $delegates;
    /**
     * @ORM\Column(name="is_group_names", type="boolean", nullable=false)
     */
    private $isGroupNames;
    /**
     * @ORM\Column(name="sort_group_order", type="integer", nullable=false)
     */
    private $sortGroupOrder;
    /**
     * @ORM\Column(name="sort_order", type="integer", nullable=false)
     */
    private $sortOrder;
    /**
     * @ORM\Column(name="ignore_discount", type="boolean", nullable=true)
     */
    private $ignoreDiscount;
    /**
     * @ORM\Column(type="boolean", name="show_in_site")
     */
    private $showInSite;
    /**
     * @ORM\Column(type="boolean", name="show_in_app")
     */
    private $showInApp;
    /**
     * @Gedmo\Locale
     */
    private $locale;
    /**
     * @ORM\Column(name="order_types", type="smallint")
     */
    private $orderTypes;
    /**
     * @Assert\File(maxSize="6000000")
     */
    private $imageFile;
    private $temp;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dishes = new ArrayCollection();
        $this->discounts = new ArrayCollection();
        $this->printers = new ArrayCollection();
        $this->isTerminal = true;

        $this->showInSite = true;
        $this->showInApp = true;

        $this->isGroupNames = false;
        $this->sortOrder = 0;
        $this->sortGroupOrder = 0;
        $this->ignoreDiscount = false;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     * @return DishCategory
     */
    public function addDish(\DishBundle\Entity\Dish $dish)
    {
        $this->dishes[] = $dish;

        return $this;
    }

    /**
     * Remove dish
     *
     * @param \DishBundle\Entity\Dish $dish
     */
    public function removeDish(\DishBundle\Entity\Dish $dish)
    {
        $this->dishes->removeElement($dish);
    }

    /**
     * Get dishes
     *
     * @return \Doctrine\Common\Collections\Collection<OrderDish>
     */
    public function getDishes()
    {
        return $this->dishes;
    }

    public function setDishes($dishes)
    {
        $this->dishes = $dishes;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DishCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add child
     *
     * @param \DishBundle\Entity\DishCategory $child
     *
     * @return DishCategory
     */
    public function addChild(\DishBundle\Entity\DishCategory $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \DishBundle\Entity\DishCategory $child
     */
    public function removeChild(\DishBundle\Entity\DishCategory $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get parent
     *
     * @return \DishBundle\Entity\DishCategory
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param \DishBundle\Entity\DishCategory $parent
     *
     * @return DishCategory
     */
    public function setParent(\DishBundle\Entity\DishCategory $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get isTerminal
     *
     * @return boolean
     */
    public function getIsTerminal()
    {
        return $this->isTerminal;
    }

    /**
     * Set isTerminal
     *
     * @param boolean $isTerminal
     *
     * @return DishCategory
     */
    public function setIsTerminal($isTerminal)
    {
        $this->isTerminal = $isTerminal;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return DishCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getShopDiscount($shop)
    {
        $discountVal = 0;
        if (!$this->discounts->isEmpty()) {

            /**
             * @var CategoryDiscount $discount
             */
            foreach ($this->discounts as $discount) {
                if ($discount->getType() == "nforn") continue;
                if ($discount->getShop() != $shop) continue;

                $discountVal = 0;

                if (in_array(2 ** (date("N") - 1), $discount->getDaysArray())) {
                    $discountVal = $this->checkTime($discount);
                }

                if ($discountVal > 0) {
                    return $discountVal;
                }
            }
        }

        return $discountVal;
    }

    private function checkTime(CategoryDiscount $discount)
    {
        $time = new \DateTime();
        $discountVal = 0;
        if ($discount->getTimeStart() < $discount->getTimeEnd()) {
            if ($time->setDate(1970, 1, 1) > ($discount->getTimeStart()) && ($time->setDate(1970, 1, 1) < $discount->getTimeEnd())) {
                $discountVal = $discount->getDiscount();
            }
        } else {
            if ($time->setDate(1970, 1, 1) > ($discount->getTimeStart()) || ($time->setDate(1970, 1, 1) < $discount->getTimeEnd())) {
                $discountVal = $discount->getDiscount();
            }
        }

        return $discountVal;
    }

    /**
     * Add discount
     *
     * @param \DishBundle\Entity\CategoryDiscount $discount
     *
     * @return DishCategory
     */
    public function addDiscount(\DishBundle\Entity\CategoryDiscount $discount)
    {
        $this->discounts[] = $discount;
        $discount->setCategory($this);

        return $this;
    }

    /**
     * Remove discount
     *
     * @param \DishBundle\Entity\CategoryDiscount $discount
     */
    public function removeDiscount(\DishBundle\Entity\CategoryDiscount $discount)
    {
        $this->discounts->removeElement($discount);
    }

    /**
     * Get discounts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    public function getImage()
    {
        return null === $this->image
            ? $this->getImageUploadDir() . '/placeholder.jpg'
            : $this->getImageUploadDir() . '/' . $this->image;
    }

    /**
     * @return DishCategory
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    protected function getImageUploadDir()
    {
        return 'uploads/images/category';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preImageUpload()
    {
        if (null !== $this->getImageFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->image = $filename . '.' . $this->getImageFile()->guessExtension();
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;
        // check if we have an old image path
        if (isset($this->image)) {
            // store the old name to delete after the update
            $this->temp = $this->image;
            $this->image = null;
        } else {
            $this->image = 'initial';
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }
        $this->getImageFile()->move($this->getImageUploadRootDir(), $this->image);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getImageUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->imageFile = null;
    }

    protected function getImageUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getImageUploadDir();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeImageUpload()
    {
        $file = $this->getImageAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    public function getImageAbsolutePath()
    {
        return null === $this->image
            ? null
            : $this->getImageUploadRootDir() . '/' . $this->image;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return DishCategory
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Add printer
     *
     * @param \RestaurantBundle\Entity\Printer $printer
     *
     * @return DishCategory
     */
    public function addPrinter(\RestaurantBundle\Entity\Printer $printer)
    {
        $this->printers[] = $printer;
        $printer->setCategory($this);

        return $this;
    }

    /**
     * Remove printer
     *
     * @param \RestaurantBundle\Entity\Printer $printer
     */
    public function removePrinter(\RestaurantBundle\Entity\Printer $printer)
    {
        $this->printers->removeElement($printer);
    }

    /**
     * Get printers
     *
     * @return Printer
     */
    public function getPrinters()
    {
        return $this->printers;
    }

    public function getShopPrinter($shop)
    {
        /*if (!$this->printers->isEmpty()) {
            $printer = $this->printers[0];
            return $printer;
        } else {
            return null;
        }*/

        foreach ($this->printers as $printer) {
            if ($printer->getShop() == $shop) {
                return $printer;
            }
        }
        return null;
        //return $shop->getAdminPrinter();
    }

    /**
     * Add delegate
     *
     * @param \DishBundle\Entity\CategoryDelegate $delegate
     *
     * @return DishCategory
     */
    public function addDelegate(\DishBundle\Entity\CategoryDelegate $delegate)
    {
        $this->delegates[] = $delegate;

        return $this;
    }

    /**
     * Remove delegate
     *
     * @param \DishBundle\Entity\CategoryDelegate $delegate
     */
    public function removeDelegate(\DishBundle\Entity\CategoryDelegate $delegate)
    {
        $this->delegates->removeElement($delegate);
    }

    /**
     * Get delegates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDelegates()
    {
        return $this->delegates;
    }

    public function getDelegateTo2($shop, $type = 'html')
    {


          $delegate = null;
	    foreach ($this->delegates as $del) {
                if ($del->getShop()->getId() == $shop->getId()) {
                    $delegate = $del;
                }
            }
            
            if ($delegate) {

            $to = $delegate->getDelegate()->getId();
            if ($type == "html") {
                return 'data-category-delegate-to="' . $to . '"';
            } elseif ($type == "json") {
                return 'categoryDelegateTo: ' . $to . ',';
            } elseif ($type == "raw") {
                return $to;
            }

            }

        return false;
    }

    public function getDelegateTo($type = 'html')
    {
        //if (array_key_exists(0, $this->delegates))
        if ($this->delegates[0]) {

            /**
             * @var CategoryDelegate $delegate
             */
            $delegate = $this->delegates[0];
            $to = $delegate->getDelegate()->getId();
            if ($type == "html") {
                return 'data-category-delegate-to="' . $to . '"';
            } elseif ($type == "json") {
                return 'categoryDelegateTo: ' . $to . ',';
            } elseif ($type == "raw") {
                return $to;
            }
        }
        return false;
    }

    /**
     * Get isGroupNames
     *
     * @return boolean
     */
    public function getIsGroupNames()
    {
        return $this->isGroupNames;
    }

    /**
     * Set isGroupNames
     *
     * @param boolean $isGroupNames
     *
     * @return DishCategory
     */
    public function setIsGroupNames($isGroupNames)
    {
        $this->isGroupNames = $isGroupNames;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return boolean
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder
     *
     * @param boolean $sortOrder
     *
     * @return DishCategory
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortGroupOrder
     *
     * @return integer
     */
    public function getSortGroupOrder()
    {
        return $this->sortGroupOrder;
    }

    /**
     * Set sortGroupOrder
     *
     * @param integer $sortGroupOrder
     *
     * @return DishCategory
     */
    public function setSortGroupOrder($sortGroupOrder)
    {
        $this->sortGroupOrder = $sortGroupOrder;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIgnoreDiscount()
    {
        return $this->ignoreDiscount;
    }

    /**
     * @param mixed $ignoreDiscount
     * @return DishCategory
     */
    public function setIgnoreDiscount($ignoreDiscount)
    {
        $this->ignoreDiscount = $ignoreDiscount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShowInSite()
    {
        return $this->showInSite;
    }

    /**
     * @param mixed $showInSite
     * @return DishCategory
     */
    public function setShowInSite($showInSite)
    {
        $this->showInSite = $showInSite;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShowInApp()
    {
        return $this->showInApp;
    }

    /**
     * @param mixed $showInApp
     * @return DishCategory
     */
    public function setShowInApp($showInApp)
    {
        $this->showInApp = $showInApp;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getOrderTypes()
    {
        return $this->orderTypes;
    }

    /**
     * @param mixed $orderTypes
     * @return self
     */
    public function setOrderTypes($orderTypes)
    {
        $this->orderTypes = $orderTypes;
        return $this;
    }
}
