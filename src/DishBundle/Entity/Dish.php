<?php

namespace DishBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\Index;


/**
 * Dish
 *
 * @ORM\Table(
 *     name="dish",
 *     indexes={@Index(name="main_idx", columns={"category_id", "is_deleted"})}
 *     )
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\DishRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Dish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\DishMeta", mappedBy="dish", cascade={"all"}, orphanRemoval=true)
     */
    private $dishMetas;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\DishIngredient", mappedBy="dish", cascade={"persist"}, orphanRemoval=true)
     */
    private $ingredients;

    /**
     * @ORM\Column(name="weight", type="text", nullable=true)
     */
    private $weight;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="id_1c", type="integer", nullable=true)
     */
    private $id1c;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\DishCategory", inversedBy="dishes")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="RestaurantBundle\Entity\Shop", inversedBy="dishes")
     * @ORM\JoinTable(name="shop_dish")
     */
    private $shops;

    /**
     * @ORM\Column(name="is_exportable", type="boolean", nullable=true, options={"default" = true})
     */
    private $isExportable;

    /**
     * @ORM\Column(name="ignore_discount", type="boolean", nullable=true)
     */
    private $ignoreDiscount;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @ORM\Column(name="in_menu", type="boolean", nullable=true)
     */
    private $isInMenu;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\DishHistory", mappedBy="dish")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $changes;

    /**
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;

    /**
     * @ORM\Column(name="is_weight", type="boolean", nullable=false, options={"default" = false})
     */
    private $isWeight;

    /**
     * @Gedmo\Locale
     */
    private $locale;

    /**
     * @ORM\Column(name="is_container", type="boolean", nullable=false, options={"default" = false} )
     */
    private $isContainer;

    /**
     * @ORM\OneToMany(targetEntity="DishTime", mappedBy="dish", cascade={"all"})
     */
    private $times;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Composition")
     */
    private $composition;

    const ORDER_TYPE = [
        'Доставка' => 1,
        'Навынос' => 2,
        'На месте' => 4,
        'Заказ стола' => 8,
        'Заказ с сайта' => 16,
        'Yandex' => 32,
    ];

    /**
     * @ORM\Column(name="order_types", type="smallint")
     */
    private $orderTypes;

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->shops = new ArrayCollection();
        $this->changes = new ArrayCollection();
        $this->dishMetas = new ArrayCollection();
        $this->isDeleted = false;
        $this->isExportable = true;
        $this->isInMenu = true;
        $this->isWeight = false;
        $this->isContainer = false;
        $this->ignoreDiscount = false;
    }

    public function __toString()
    {
        return $this->name;
    }


    public function getShopMeta(Shop $shop)
    {
        /**
         * @var DishMeta $dishMeta
         */
        foreach ($this->dishMetas as $dishMeta) {
            if ($dishMeta->getShop() == $shop) {
                return $dishMeta;
            }
        }
        return null;
    }

    public function getShopPrice($shop = null)
    {
        $price = $this->price;
if ($shop == null) {
   return $price;
}
//var_dump($shop);die();
        $shopId = (gettype($shop) == "integer" ? $shop : $shop->getId());

        /**
         * @var DishMeta $dishMeta
         */
        foreach ($this->dishMetas as $dishMeta) {
            if ($dishMeta->getShop()->getId() == $shopId) {
                $price = $dishMeta->getPrice();
            }
        }


        return $price;
    }




    public function getMenuPrice()
    {
        if ($this->dishMetas->isEmpty()) {
            if ($this->changes->isEmpty()) {
                return $this->price;
            } else {
                foreach ($this->changes as $change) {
                    if ($change->getShop() == null) {
                        return $change->getPrice();
                    }
                }
                //return $this->changes[0]->getPrice();
            }
        } else {
            if ($this->changes->isEmpty()) {
                return $this->dishMetas[0]->getPrice();
            } else {
                foreach ($this->changes as $change) {
                    if ($change->getShop() != null) {
                        return $change->getPrice();
                    }
                }
                return $this->changes[0]->getPrice();
            }
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return Dish
     */
    public function addShop(\RestaurantBundle\Entity\Shop $shop)
    {
        $this->shops[] = $shop;

        return $this;
    }

    /**
     * Remove shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     */
    public function removeShop(\RestaurantBundle\Entity\Shop $shop)
    {
        $this->shops->removeElement($shop);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShops()
    {
        return $this->shops;
    }

    public function hasShop(Shop $shop)
    {
        if ($this->shops->contains($shop)) {
            return true;
        }
        return false;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Dish
     */
    public function setPrice($price)
    {
        $this->price = str_replace(',', '.', $price);

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Dish
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getIsExportable()
    {
        return $this->isExportable;
    }

    /**
     * @param mixed $isExportable
     */
    public function setIsExportable($isExportable)
    {
        $this->isExportable = $isExportable;
        return $this;
    }

    /**
     * Set id1c
     *
     * @param integer $id1c
     *
     * @return Dish
     */
    public function setId1c($id1c)
    {
        $this->id1c = $id1c;

        return $this;
    }

    /**
     * Get id1c
     *
     * @return integer
     */
    public function getId1c()
    {
        return $this->id1c;
    }

    /**
     * Set category
     *
     * @param \DishBundle\Entity\DishCategory $category
     *
     * @return Dish
     */
    public function setCategory(\DishBundle\Entity\DishCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DishBundle\Entity\DishCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add ingredient
     *
     * @param \DishBundle\Entity\DishIngredient $ingredient
     *
     * @return Dish
     */
    public function addIngredient(\DishBundle\Entity\DishIngredient $ingredient)
    {
        $this->ingredients[] = $ingredient;
        $ingredient->setDish($this);

        return $this;
    }

    /**
     * Remove ingredient
     *
     * @param \DishBundle\Entity\DishIngredient $ingredient
     */
    public function removeIngredient(\DishBundle\Entity\DishIngredient $ingredient)
    {
        $this->ingredients->removeElement($ingredient);
    }

    /**
     * Get ingredients
     *
     * @return CompositionIngredient[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Dish
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add dishMeta
     *
     * @param \DishBundle\Entity\DishMeta $dishMeta
     *
     * @return Dish
     */
    public function addDishMeta(\DishBundle\Entity\DishMeta $dishMeta)
    {
        $this->dishMetas[] = $dishMeta;
        return $this;
    }

    /**
     * Remove dishMeta
     *
     * @param \DishBundle\Entity\DishMeta $dishMeta
     */
    public function removeDishMeta(\DishBundle\Entity\DishMeta $dishMeta)
    {
        $this->dishMetas->removeElement($dishMeta);
    }

    /**
     * Get dishMetas
     *
     * @return DishMeta[]
     */
    public function getDishMetas()
    {
        return $this->dishMetas;
    }

    /**
     * @ORM\Column(type="string", name="image", length=255, nullable=true)
     */
    public $image;

    /**
     * @Assert\File(maxSize="60000000")
     */
    private $imageFile;

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;
        // check if we have an old image path
        if (isset($this->image)) {
            // store the old name to delete after the update
            $this->temp = $this->image;
            $this->image = null;
        } else {
            $this->image = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return string
     */
    public function getImageHash()
    {
        return $this->image;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getImageAbsolutePath()
    {
        return null === $this->image
            ? null
            : $this->getImageUploadRootDir() . '/' . $this->image;
    }

    public function getImage()
    {
        return null === $this->image
            ? $this->getImageUploadDir() . '/placeholder.jpg'
            : $this->getImageUploadDir() . '/' . $this->image;
    }

    public function getImagePath()
    {
        return null === $this->image
            ? $this->getImageUploadDir() . '/placeholder.jpg'
            : $this->getImageUploadDir() . '/' . $this->image;
    }

    protected function getImageUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        return 'uploads/images/item';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preImageUpload()
    {
        if (null !== $this->getImageFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->image = $filename . '.' . $this->getImageFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }
        $this->getImageFile()->move($this->getImageUploadRootDir(), $this->image);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getImageUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->imageFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeImageUpload()
    {
        $file = $this->getImageAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }


    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function oldPrice($price)
    {
        return $price * 10000;
    }

    public function newPrice($price)
    {
        $denomination = $price / 10000;
        $rub = intval($denomination);
        $cent = ($denomination - $rub) * 100;

        $newPrice = [];
        if ($rub != 0) {
            $newPrice[] = $rub . " р.";
        }

        if ($cent != 0) {
            $newPrice[] = $cent . " коп.";
        }

        return implode(" ", $newPrice);
    }

    /**
     * Add change
     *
     * @param \DishBundle\Entity\DishHistory $change
     *
     * @return Dish
     */
    public function addChange(\DishBundle\Entity\DishHistory $change)
    {
        $this->changes[] = $change;

        return $this;
    }

    /**
     * Remove change
     *
     * @param \DishBundle\Entity\DishHistory $change
     */
    public function removeChange(\DishBundle\Entity\DishHistory $change)
    {
        $this->changes->removeElement($change);
    }

    /**
     * Get changes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChanges()
    {
        return $this->changes;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return Dish
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isInMenu
     *
     * @param boolean $isInMenu
     *
     * @return Dish
     */
    public function setIsInMenu($isInMenu)
    {
        $this->isInMenu = $isInMenu;

        return $this;
    }

    /**
     * Get isInMenu
     *
     * @return boolean
     */
    public function getIsInMenu()
    {
        return $this->isInMenu;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Dish
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set isWeight
     *
     * @param integer $isWeight
     *
     * @return Dish
     */
    public function setIsWeight($isWeight)
    {
        $this->isWeight = $isWeight;

        return $this;
    }

    /**
     * Get isWeight
     *
     * @return integer
     */
    public function getIsWeight()
    {
        return $this->isWeight;
    }


    /**
     * @return boolean
     */
    public function getIsContainer()
    {
        return $this->isContainer;
    }

    /**
     * @return Dish
     */
    public function setIsContainer($isContainer)
    {
        $this->isContainer = $isContainer;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getIgnoreDiscount()
    {
        if ($this->getCategory() != null && $this->getCategory()->getIgnoreDiscount()) {
            return true;
        }
        return $this->ignoreDiscount;
    }

    /**
     * @param mixed $ignoreDiscount
     * @return Dish
     */
    public function setIgnoreDiscount($ignoreDiscount)
    {
        $this->ignoreDiscount = $ignoreDiscount;
        return $this;
    }


    /**
     * Add time.
     *
     * @param \DishBundle\Entity\DishTime $time
     *
     * @return Dish
     */
    public function addTime(\DishBundle\Entity\DishTime $time)
    {
        $this->times[] = $time;

        return $this;
    }

    /**
     * Remove time.
     *
     * @param \DishBundle\Entity\DishTime $time
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTime(\DishBundle\Entity\DishTime $time)
    {
        return $this->times->removeElement($time);
    }

    /**
     * Get times.
     *
     * @return DishTime[]
     */
    public function getTimes()
    {
        return $this->times;
    }


    /**
     * @return Composition
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param Composition $composition
     * @return Dish
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getOrderTypes()
    {
        return $this->orderTypes;
    }

    /**
     * @param mixed $orderTypes
     * @return self
     */
    public function setOrderTypes($orderTypes)
    {
        $this->orderTypes = $orderTypes;
        return $this;
    }
}
