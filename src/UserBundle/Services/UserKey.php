<?php


namespace UserBundle\Services;


class UserKey
{

    public static function getRedisKey($user)
    {
        return "user" . $user->getId() . "_2fa";
    }

    public static function getUserKey($user)
    {
        return "user" . self::numberToWord($user->getId());
    }

    static function numberToWord($number)
    {
        $words = array('zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine');

        $result = '';
        $digits = str_split($number);

        foreach ($digits as $digit) {
            if ($digit >= 0 && $digit <= 9) {
                $result .= $words[$digit];
            } else {
                $result = 'Invalid number';
                break;
            }
        }

        return $result;
    }
}