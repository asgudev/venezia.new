<?php

namespace ApiBundle\Controller;

use ControlBundle\Entity\IngredientData;
use DishBundle\Entity\CategoryDiscount;
use DishBundle\Entity\Dish;
use DishBundle\Entity\DishCategory;
use FrontendBundle\Entity\WebOrder;
use FrontendBundle\Entity\WebOrderDish;
use FrontendBundle\Entity\WebShop;
use FrontendBundle\Entity\WebShopImage;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use RestaurantBundle\Entity\Client;
use RestaurantBundle\Entity\Printer;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\ShopPlaceTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use TerminalBundle\Entity\DailyReport;
use UserBundle\Entity\User;
use UserBundle\Entity\UserEvent;

/**
 * Class ShopController
 * @package ApiBundle\Controller
 */
class ShopController extends Controller
{

    /**
     * @Route("/dashboard/{dateStart}", name="api_dashboard_data")
     */
    public function getDashboardData(Request $request, $dateStart = 'today')
    {
        if ($dateStart == 'today') {

            $shopData = $this->getDoctrine()->getRepository('OrderBundle:Order')->findShopDataForDashboard((new \DateTime($dateStart))->setTime(8, 0, 0));
            $shopsData = [];

            foreach ($shopData as $shop) {
                $shopsData[$shop['osId']] = [
                    'cash' => $shop['orderSum'],
                    'pizza' => 0,
                    'pizza_free' => 0,
                ];
                $pizza = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaSumFromDate($shop['osId'], (new \DateTime('today'))->setTime(9, 0, 0));
//dump($pizza);die();
                foreach ($pizza as $data) {
                    if ($data[0]->getDish()->getCategory()->getId() == 82) {
                        if ($data['diiName'] == "шарик большой") {
                            $shopsData[$shop['osId']]['pizza_free'] += $data['pizzaSum'];
                        }
                    } else {
                        if ($data['diiName'] == "шарик большой") {
                            $shopsData[$shop['osId']]['pizza'] += $data['pizzaSum'];
                        }
                    }
                }
            }
        } else {
            $reports = [];
            $dailyReports = $this->getDoctrine()->getRepository(DailyReport::class)->findBy([
                'name' => $dateStart
            ]);
            $shopsData = [];
            foreach ($dailyReports as $__report) {
                $shopData = $this->getDoctrine()->getRepository(Order::class)->findOrdersByReport($__report);

                /**
                 * @var Order $__order
                 */
                foreach ($shopData as $__order) {
                    $shopId = $__order->getShop()->getId();
                    if (!array_key_exists($__order->getShop()->getId(), $shopsData))
                        $shopsData[$__order->getShop()->getId()] = [
                            'pizza' => 0,
                            'pizza_free' => 0,
                            'cash' => 0,
                        ];


                    $shopsData[$shopId]['cash'] += $__order->getCashSum();
                }

                $pizza = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaByReport($__report);

                foreach ($pizza as $data) {
                    $shopsData[$shopId][$data->getDish()->getCategory()->getId() == 82 ? 'pizza_free' : 'pizza'] += $data->getQuantity();
                }
            }
        }

        return new JsonResponse($shopsData);
    }

    /*

    curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"phone":"375298271964","address":"new york", "comment": "tratata", dishes:[{"id":"11183","quantity":1},{"id":"11187","quantity":1},]}' \
  https://mytant.com/api/shop/1/order

     */

    /**
     * @Route("/shop/{id}/status", name="api_shop_status")
     * @param Request $request
     * @param Shop $shop
     */
    public function statusAction(Request $request, Shop $shop)
    {
        $data = json_decode($request->getContent(), true);

        $this->get("redis.helper")->set("version_" . $shop->getId(), $data["version"]);

        $spool_path = $this->get('kernel')->getRootDir() . '/../var/state/' . $shop->getId() . "/";

        if (!is_dir($spool_path)) mkdir($spool_path, 0777, true);

        $f = fopen($spool_path . time() . ".json", "a+");
        fputs($f, $request->getContent());
        fclose($f);

        return new JsonResponse(true);
    }

    /**
     * @Route("/shop/{id}/request", name="api_request")
     * @param Request $request
     * @param Shop $shop
     */
    public function requestAction(Request $request, Shop $shop)
    {
        $data = json_decode($request->getContent(), true);

        $spool_path = $this->get('kernel')->getRootDir() . '/../var/request/';
        $reportLog = fopen($spool_path . "request-" . $shop->getId() . "-" . (new \DateTime())->format("Y.m.d") . "-initial-" . time() . ".json", "a+");
        fputs($reportLog, $request->getContent() . "\n\n");
        fclose($reportLog);

        $requestData = $this->get("restaurant.warehouse.manager")->getRequestData();
        if (!$requestData) $requestData = [];


        foreach ($data["report"]["ingredients"] as $key => $ingr) {
            if (!array_key_exists($ingr["ingredientId"], $requestData)) {
                $requestData[$ingr["ingredientId"]] = [];
            }
            $requestData[$ingr["ingredientId"]][$shop->getId()] = $ingr["request"];
        }

        $this->get("restaurant.warehouse.manager")->saveRequestData($requestData);

        return new JsonResponse([]);
    }


    /**
     * @Route("/shop/{id}/delay", name="api_set_shop_delay")
     * @param Request $request
     * @param Shop $shop
     */
    public function setShopDelayAction(Request $request, Shop $shop)
    {
        $data = json_decode($request->getContent(), true);

        if ($data) {
            $r = $this->get("redis.helper")->set("shop_delay_" . $shop->getId(), $data["delay"]);
        }

        return new JsonResponse([]);
    }

    /**
     * @Route("/shop/{id}/report", name="api_report")
     * @param Request $request
     * @param Shop $shop
     */
    public function reportAction(Request $request, Shop $shop)
    {
        
        if ($shop->getId() != 17) {
            $isClosing = $this->get("redis.helper")->get("is_shift_closing_" . $shop->getId());
        } else $isClosing = false;

        if ($isClosing) {
            return new JsonResponse(["status" => false]);
        }

        $this->get("redis.helper")->set("is_shift_closing_" . $shop->getId(), 1, 300);

        $data = json_decode($request->getContent(), true);

        $spool_path = $this->get('kernel')->getRootDir() . '/../var/report/';

        $shopDir = $shop->getId();
        $this->checkAndCreateDirectory($spool_path . "/" . $shopDir . "/initial");
        $this->checkAndCreateDirectory($spool_path . "/" . $shopDir . "/parsed");

        $reportLogPath = $spool_path . "/" . $shopDir . "/initial/" . (new \DateTime())->format("Y.m.d") . "-" . time() . ".json";
        $reportLog = fopen($reportLogPath, "a+");
        fputs($reportLog, $request->getContent() . "\n\n");
        fclose($reportLog);

        $previousReport = $this->get('shop.report.manager')->getLastReport($shop);

        $newReport = $this->get('shop.report.manager')->newReport($previousReport);
/*
        foreach ($data["orders"] as $key => $orderData) {
            $order = $this->get("order.manager")->syncOrder($orderData);
            $data["orders"][$key]["masterId"] = $order->getId();
        }
*/
        $reportLog = fopen($spool_path . "/" . $shopDir . "/parsed/" . (new \DateTime())->format("Y.m.d") . "-" . time() . ".json", "a+");
        fputs($reportLog, json_encode($data));
        fclose($reportLog);

        $em = $this->getDoctrine()->getManager();
        $newReport->setUser($em->getReference('UserBundle:User', $data["userId"] ?: 1));

        $em->persist($newReport);

        if ($data["report"] && array_key_exists("ingredients", $data["report"])) {
            foreach ($data["report"]["ingredients"] as $key => $__ingr) {
                if (!$__ingr["ingredientId"]) continue;

                $ingrData = new IngredientData();
                $ingrData->setIngredient($em->getReference('DishBundle:Ingredient', $__ingr["ingredientId"]));

                $ingrData->setReport($newReport);
                $newReport->addIngredient($ingrData);
                $ingrData->setIncome($__ingr["income"]);
                $ingrData->setWaste($__ingr["outcome"]);
                $ingrData->setBalance($__ingr["rest"]);

                $em->persist($ingrData);
            }
        }

        $newReport->setFiscalCashSum($data["fiscalStats"]["cash"]);
        $newReport->setFiscalCardSum($data["fiscalStats"]["card"]);


        $em->flush();

        exec("node /home/importer/sync_command.js $reportLogPath " . $newReport->getShop()->getId() . " \"" . $newReport->getDateStarted()->format("Y-m-d H:i:s") . "\" \"" . $newReport->getDateClosed()->format("Y-m-d H:i:s") . "\"");

        $result = $this->get("shop.report.manager")->validateRevision($newReport);

//        if ($result) {
        $this->export($newReport->getId());

//        }
        $requestData = $this->get("restaurant.warehouse.manager")->getRequestData();
        if (!$requestData) $requestData = [];


        foreach ($data["report"]["ingredients"] as $key => $ingr) {
            if (!array_key_exists($ingr["ingredientId"], $requestData)) {
                $requestData[$ingr["ingredientId"]] = [];
            }
            $requestData[$ingr["ingredientId"]][$shop->getId()] = $ingr["request"];
        }

        $this->get("restaurant.warehouse.manager")->saveRequestData($requestData);

        return new JsonResponse([
            'id' => $newReport->getId(),
            'status' => $result,
        ]);
    }

    private function checkAndCreateDirectory($directory)
    {
        try {
            if (!is_dir($directory)) {
                if (!mkdir($directory, 0777, true)) {
                    throw new Exception("Error creating the directory.");
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    private function export($reportId)
    {
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $exportCommand = new ArrayInput(array(
            'command' => 'rest:report:export',
            '--report' => $reportId,
            '--env' => $this->getParameter('kernel.environment')
        ));
        $application->run($exportCommand);
    }

    /**
     * @Route("/shop/{id}/last-report", name="api_last_report")
     * @param Request $request
     * @param Shop $shop
     * @return JsonResponse
     */
    public function getLastReportAction(Request $request, Shop $shop)
    {
        $previousReport = $this->getDoctrine()->getRepository(DailyReport::class)->findLastReport($shop);
        if (!$previousReport) {
            $previousReport = new DailyReport($shop);
            $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
        } else {
            $previousReport = $previousReport[0];
        }

        $data = [];

        $data["ingredients"] = array_map(function (IngredientData $data) {
            return [
                'id' => $data->getId(),
                'ingredientId' => $data->getIngredient()->getId(),
                'last' => $data->getBalance(),
            ];
        }, $previousReport->getIngredients()->toArray());

        return new JsonResponse($data);
    }

    /**
     * @Route("/shop/{id}/remains", name="api_receive_remains")
     */
    public function receiveRemainsAction(Request $request, Shop $shop)
    {
        $em = $this->getDoctrine()->getManager();


        $event = new UserEvent();
        $event
            ->setEventType(UserEvent::EVENT_USER_REVISION)
            ->setDate(new \DateTime())
            ->setUser($em->getRepository("UserBundle:User")->find($request->get("user")))
            ->setShop($shop)
            ->setPayload(json_encode($request->get("1"), JSON_UNESCAPED_UNICODE));

        $em->persist($event);
        $em->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/shop/{id}/stats", name="api_shop_stats")
     */
    public function shopStatsAction(Request $request, Shop $shop)
    {
        $previousReport = $this->getDoctrine()->getRepository(DailyReport::class)->findLastReport($shop);
        if (!$previousReport) {
            $previousReport = new DailyReport($shop);
            $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
        } else {
            $previousReport = $previousReport[0];
        }

        $orders = $this->getDoctrine()->getRepository(Order::class)->findSums($shop, $previousReport->getDateClosed())[0];
        $dishSum = $this->getDoctrine()->getRepository(OrderDish::class)->findDishSum($shop, $previousReport->getDateClosed());


        return new JsonResponse([
            'cash' => $orders['oCash'] ? $orders['oCash'] : 0,
            'card' => $orders['oCard'] ? $orders['oCard'] : 0,
            'items' => $dishSum ? $dishSum : 0,
        ]);
    }

    /**
     * @Route("/shop/{id}/order", name="api_receive_order")
     */
    public function receiveOrderAction(Request $request, Shop $shop)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        $f = fopen('test_app.log', 'a+');
        fputs($f, (new \DateTime())->format('d.m.Y H:i') . ' ' . $request->getContent() . "\r\n");
        fclose($f);

        $data = json_decode($request->getContent(), true);

        $em = $this->getDoctrine()->getManager();
        $webOrder = new WebOrder();
        $webOrder->setPhone($data["phone"]);
        $webOrder->setAddress($data["address"]);
        $webOrder->setComment($data["comment"]);
        $webOrder->setPickupShop($shop);
        $webOrder->setLocalId(1);
        foreach ($data["dishes"] as $__dish) {
            $webOrderDish = new WebOrderDish();
            $webOrderDish->setOrder($webOrder);
            $webOrderDish->setDish($em->getReference('DishBundle\Entity\Dish', $__dish["id"]));
            $webOrderDish->setQuantity($__dish["quantity"]);
            $webOrderDish->setDishPrice(0);
            $webOrderDish->setDishDiscount(0);
            $em->persist($webOrderDish);
        }

        $em->persist($webOrder);
        $em->flush();

        return new JsonResponse($webOrder->getId());
    }

    /**
     * @Route("/shop/list", name="api_get_shop_list")
     */
    public function getJsonShopListAction(Request $request)
    {

        $this->get('user.logger')->info("Authorization " . $request->headers->get("authorization"));

        $user = $this->getDoctrine()->getRepository("UserBundle:User")->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user)
            throw new AccessDeniedHttpException();

        $shopsData = $this->getDoctrine()->getRepository(Shop::class)->findAll();
        $shops = [];
        /**
         * @var Shop $shop
         */
        foreach ($shopsData as $shop) {
            if (in_array($shop->getId(), [0, 3, ])) continue;
            $webShop = $this->getDoctrine()->getRepository(WebShop::class)->findOneBy([
                'shop' => $shop,
            ]);
            $__shop = [
                'id' => $shop->getId(),
                'alias' => $shop->getAlias(),
                'name' => $shop->getName(),
                'address' => $shop->getAddress(),
                'phone' => $shop->getPhone(),
                'times' => $shop->getTimes(),
                'orderTypes' => $shop->getOrderTypes(),
                'logo' => $shop->getImage(),
                "social" => $webShop ? $webShop->getSocial() : [],
                "delivery" => $webShop ? $webShop->getDelivery() : [],
                "kitchen" => $webShop ? $webShop->getKitchen() : "",
                'image' => $webShop ? $webShop->getTypedImage(WebShopImage::TYPE_PREVIEW) : null,
            ];

            if ($user->hasRole("ROLE_APP")) {
                $__shop['serverIp'] = $shop->getServerIP();
            }

            $shops[] = $__shop;
        }

        $response = new Response(json_encode($shops, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/shop/{id}/tables", name="api_get_tables")
     * @param Request $request
     * @param Shop $shop
     * @return Response
     */
    public function getJsonTablesAction(Request $request, Shop $shop)
    {
        $this->get('user.logger')->info("Authorization " . $request->headers->get("authorization"));
        $this->get('user.logger')->info("request tables");

        $user = $this->getDoctrine()->getRepository("UserBundle:User")->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user)
            throw new AccessDeniedHttpException();

        if (!$shop)
            throw new NotFoundHttpException();

        $places = [];

        foreach ($shop->getPlaces() as $place) {
            $places[] = [
                'id' => $place->getId(),
                'name' => $place->getName(),
                'tables' => array_map(function (ShopPlaceTable $table) {
                    return [
                        'id' => $table->getId(),
                        'name' => $table->getName(),
                        'coordinates' => json_decode($table->getMetaCoordinates()),
                    ];
                }, $place->getTables()->toArray()),
            ];
        }

        $response = new Response(json_encode($places, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/shop/{id}/menu", name="api_get_menu")
     * @param Request $request
     * @param Shop $shop
     * @return Response
     */
    public function getJsonMenuAction(Request $request, Shop $shop)
    {
        $this->get('user.logger')->info("Authorization " . $request->headers->get("authorization"));
        $this->get('user.logger')->info("request menu");
//var_dump($request->headers->get("authorization"));die();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);
//var_dump($user->getId());
/*        if (!$user)
            throw new AccessDeniedHttpException();

        if (!$shop)
            throw new NotFoundHttpException();*/

        $menu = $this->getDoctrine()->getRepository(DishCategory::class)->findAppMenuByShop($shop, null !== $request->get("full"));

        if ($user->hasRole("ROLE_DELIVERY")) {
            $menu = array_filter($menu, function (DishCategory $category) {
                return $category->getOrderTypes() & DishCategory::ORDER_TYPE['Доставка'];
            });
        } else if ($user->hasRole("ROLE_APP")) {
            $menu = $this->getDoctrine()->getRepository(DishCategory::class)->findAppMenuByShop($shop, null !== $request->get("full"));
        } else {
            $menu = $this->getDoctrine()->getRepository(DishCategory::class)->findAppMenuByShop($shop, null !== $request->get("full"));
        }

        /**
         * @var DishCategory $category
         */
        foreach ($menu as $key => $category) {
            if ($user->hasRole("ROLE_DELIVERY")) {
                $menu[$key]->setDishes($category->getDishes()->filter(function (Dish $dish) {
                    return $dish->getOrderTypes() & Dish::ORDER_TYPE['Доставка'];
                }));
            } else if ($user->hasRole("ROLE_APP")) {
                // replace condition required by app
                $menu[$key]->setDishes($category->getDishes()->filter(function (Dish $dish) {
                    return $dish->getOrderTypes() & Dish::ORDER_TYPE['На месте'];
                }));
            }
        }

        $menuToApi = $this->menuTree($menu, $shop);

        $response = new Response(json_encode($menuToApi, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function menuTree($menu, $shop)
    {
        $__menu = [];
        /** @var DishCategory $__category */
        foreach ($menu as $__category) {
// if (in_array($_SERVER["REMOTE_ADDR"], ["10.8.0.3", "178.172.245.198"]) ) dump($__category->getDelegateTo2($shop, "raw"));
            $__menu[] = [
                "id" => $__category->getId(),
                "name" => $__category->getName(),
                "description" => $__category->getDescription(),
                "delegateTo" => $__category->getDelegateTo2($shop, "raw"),
                "image" => [
                    "original" => $__category->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/" . $__category->getImage() : false,
                    "thumbnail" => $__category->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/media/cache/index/" . $__category->getImage() : false,
                ],
                "dishes" => $this->menuTreeDishes($__category->getDishes(), $shop),
                'ignoreDiscount' => $__category->getIgnoreDiscount(),
                'orderType' => $__category->getOrderTypes(),
                "order" => $__category->getOrder(),
            ];
        }
//if (in_array($_SERVER["REMOTE_ADDR"], ["10.8.0.3", "178.172.245.198"]) )  die();
        return $__menu;
    }

    private function menuTreeDishes($dishes, $shop)
    {
        $__dishes = [];

        /** @var Dish $__dish */
        foreach ($dishes as $__dish) {
            $__dishes[] = [
                "id" => $__dish->getId(),
                "name" => $__dish->getName(),
                "category" => $__dish->getCategory()->getId(),
                "image" => [
                    "original" => $__dish->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/" . $__dish->getImage() : false,
                    "thumbnail" => $__dish->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/media/cache/index/" . $__dish->getImage() : false,
                ],
                "price" => $__dish->getShopPrice($shop),
                "description" => $__dish->getDescription(),
                "weight" => $__dish->getWeight(),
                'isWeight' => $__dish->getIsWeight(),
                'ignoreDiscount' => $__dish->getIgnoreDiscount(),
            ];
        }
        return $__dishes;
    }

    /**
     * @Route("/shop/{id}/orders", name="api_get_shop_orders")
     * @param Request $request
     * @param Shop $shop
     * @return JsonResponse
     */
    public function getShopOrders(Request $request, Shop $shop)
    {
        if (!$shop) {
            throw new NotFoundHttpException();
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user)
            throw new AccessDeniedHttpException();

        $reportStart = $this->get("redis.helper")->get("shift_start_" . $shop->getId());

        if (!$reportStart) {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($shop);
            if (!$previousReport) {
                $previousReport = new DailyReport($shop);
                $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
            } else {
                $previousReport = $previousReport[0];
            }
            $reportStart = $previousReport->getDateClosed()->format("Y-m-d H:i:s");
            $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $previousReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);
        }

        $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftActive($reportStart, $shop, null, Order::STATUS_DELETED);

        return new JsonResponse(array_map(function ($order) {
            return $this->get("order.manager")->orderToJson($order);
        }, $orders));
    }

    /**
     * @Route("/shop/{id}/users", name="api_get_shop_users")
     * @param Request $request
     * @param Shop $shop
     * @return JsonResponse
     */
    public function getShopUsers(Request $request, Shop $shop)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user)
            throw new AccessDeniedHttpException();

        $users = $this->getDoctrine()->getRepository(User::class)->findUsersInShops(array_merge([$shop->getId()], $this->getParameter("relative_shops")));

        return new JsonResponse(array_map(function (User $user) {
            return [
                'id' => $user->getId(),
                'username' => $user->getName(),
//                'apiToken' => $user->getApiToken(),
            ];
        }, $users));
    }

    /**
     * @Route("/clients", name="api_get_clients")
     * @param Request $request
     * @return JsonResponse
     */
    public function getClients(Request $request)
    {
        $clients = $this->getDoctrine()->getRepository(Client::class)->findBy([
            'isDeleted' => false,
        ]);

        return new JsonResponse(array_map(function (Client $client) {
            return ['id' => $client->getId(), 'name' => $client->getName()];
        }, $clients));
    }

    /**
     * @Route("/table", name="api_get_shop_by_table")
     */
    public function findShopByTable(Request $request)
    {
        $this->get('user.logger')->info("Authorization " . $request->headers->get("authorization"));

        $table = $this->getDoctrine()->getRepository(ShopPlaceTable::class)->find($request->get("id"));

        if ($table) {
            $shop = $table->getPlace()->getShop()->getId();
        } else {
            $shop = false;
        }

        $response = new Response(json_encode(['shop' => $shop, 'table' => $table->getId(),], JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    /**
     * @Route("/unlock/{id}/{token}", name="api_shop_revision_unlock")
     * @param Request $request
     * @param Shop $shop
     * @param $token
     * @return Response
     */
    public function shopUnlockAction(Request $request, Shop $shop, $token)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        if ($shop->validateRevisionLockToken($token)) {

            $shop->setRevisionLockTill(null);

            $em = $this->getDoctrine()->getManager();
            $em->persist($shop);
            $em->flush();
            return new Response("success");

        } else {
            return new Response("fail");
        }

    }

    /**
     * @Route("/warehouse/request", name="warehouse_request")
     * @param Request $request
     * @return JsonResponse
     */
    public function getRequestAction(Request $request)
    {
        if ($request->getMethod() == Request::METHOD_GET) {
            $shops = $this->getDoctrine()->getRepository(Shop::class)->findAll();

            $warehouseManager = $this->get("restaurant.warehouse.manager");
            $data = $warehouseManager->calculateWarehouse($shops);

            return new JsonResponse([
                "request" => $data["request"],
                "requestCategories" => $data["ingrCategories"],
                'requestCategoriesNames' => $data['ingrCategoriesNames'],
            ]);
        } elseif ($request->getMethod() == Request::METHOD_POST) {

            $data = json_decode($request->getContent(), true);
            $this->get("restaurant.warehouse.manager")->updateRequestTable($data);


            return new JsonResponse();
        }
    }

    /**
     * @Route("/shop/{id}/config", name="shop_config")
     * @param Request $request
     * @param Shop $shop
     */
    public function getShopConfig(Request $request, Shop $shop)
    {
        $config = $this->shopToJson($shop);

        $users = $this->getDoctrine()->getRepository(User::class)->findUsersInShops(array_merge([$shop->getId()], $this->getParameter("relative_shops")));

        $config['users'] = array_map(function (User $user) {
            return [
                'id' => $user->getId(),
                'username' => $user->getName(),
                'enabled' => $user->isEnabled(),
                'isFiscalGranted' => $user->isFiscalGranted(),
                'position' => $user->getPosition(),
            ];
        }, $users);

        $config['places'] = array_map(function (ShopPlace $place) {
            return [
                'id' => $place->getId(),
                'name' => $place->getName(),
            ];
        }, $shop->getPlaces()->toArray());

        $clients = $this->getDoctrine()->getRepository(Client::class)->findBy([
            "isDeleted" => false,
        ]);
        $config["clients"] = array_map(function (Client $client) {
            return [
                'id' => $client->getId(),
                'name' => $client->getName(),
            ];
        }, $clients);

        $config['tables'] = [];
        foreach ($shop->getPlaces() as $place) {
            foreach ($place->getTables() as $table) {
                $config['tables'][] = [
                    'id' => $table->getId(),
                    'placeId' => $place->getId(),
                    'name' => $table->getName(),
                    'coordinates' => json_decode($table->getMetaCoordinates()),
                    'isTakeaway' => $table->isToGo(),
                ];
            }
        }

        $config['menu'] = $this->getShopMenu($shop);
        $config['delay'] = $this->get("redis.helper")->get("shop_delay_" . $shop->getId()) ?: 30;


        $conn = $this->getDoctrine()->getManager()->getConnection();
        $avgSqlQuery = "WITH orderSums AS ( SELECT SUM(o.client_money_cash + o.client_money_card) AS total_order_sum FROM (SELECT * FROM daily_report WHERE shop_id = " . $shop->getId() . " ORDER BY id DESC LIMIT 1) r JOIN orders o ON r.shop_id = o.shop_id WHERE o.created_at BETWEEN r.date_started AND r.date_closed GROUP BY o.id ) SELECT AVG(total_order_sum) AS average_order_sum FROM orderSums";
        $stmt = $conn->prepare($avgSqlQuery);
        $stmt->execute();

        $config["customParameters"]['avg'] = (float)$stmt->fetchAll()[0]["average_order_sum"];


        $manualDiscount = $this->getDoctrine()->getRepository(PromoCard::class)->findBy([
            'type' => PromoCard::TYPE_MANUAL,
            'isVisible' => true,
        ]);

        $autoDiscount = $this->getDoctrine()->getRepository(CategoryDiscount::class)->findBy([
            "shop" => $shop,
        ]);

        $config['customParameters'] = $shop->getCustomParams();


        $streams = [
            11 => "rtsp://admin:QazWsx123@82.209.235.111:554/ISAPI/Streaming/channels/101", // nnaber
            16 => "", // рябцева
            19 => "rtsp://admin:Videobrest2023@192.168.100.90/ISAPI/Streaming/channels/301", // махно

        ];

        if (array_key_exists($shop->getId(), $streams)) {
            $config["customParameters"]['stream'] = $streams[$shop->getId()];
        }

        $config["discounts"] = [];
        foreach ($autoDiscount as $__discount) {
            $config["discounts"][] = [
                "id" => $__discount->getId(),
                "type" => $__discount->getType(),
                "categoryId" => $__discount->getCategory()->getId(),
                "percent" => $__discount->getDiscount(),
                "discount" => $__discount->getDiscount(),

                "days" => $this->sundayFirst(array_keys($__discount->getDaysArray())),
                "timeStart" => $__discount->getTimeStart()->format("H:i:s"),
                "timeEnd" => $__discount->getTimeEnd()->format("H:i:s"),
            ];
        }

        foreach ($manualDiscount as $__discount) {
            $config["discounts"][] = [
                'id' => $__discount->getId(),
                'type' => "manual",
                '_id' => $__discount->getCardId(),
                'name' => $__discount->getClient()->getFirstName(),
                'discount' => $__discount->getCardDiscount(),
            ];
        }


        $printers = $this->getDoctrine()->getRepository(Printer::class)->findBy([
            "shop" => $shop,
        ]);

        $config["printers"] = array_map(function (Printer $printer) {
            return [
                'id' => $printer->getId(),
                'category' => $printer->getCategory()->getId(),
                'device' => [
                    'id' => $printer->getPrinterDevice()->getId(),
                    'name' => $printer->getPrinterDevice()->getDeviceName(),
                    'address' => $printer->getPrinterDevice()->getAddress(),
                    'type' => $printer->getPrinterDevice()->getType(),
                ],
            ];
        }, $printers);

        $config["global"] = [
            "socketAddress" => "http://venezia.by:3000",
        ];

        return new JsonResponse($config);
    }

    private function shopToJson(Shop $shop)
    {
        $webShop = $this->getDoctrine()->getRepository(WebShop::class)->findOneBy([
            'shop' => $shop,
        ]);

        $adminPrinterDTO = null;
        $adminPrinter = $shop->getAdminPrinterDevice();
        if ($adminPrinter) {
            $adminPrinterDTO = [
                'id' => $adminPrinter->getId(),
                'name' => $adminPrinter->getDeviceName(),
                'address' => $adminPrinter->getAddress(),
                'type' => $adminPrinter->getType(),
            ];
        }


        return [
            'id' => $shop->getId(),
            'name' => $shop->getName(),
            'address' => $shop->getAddress(),
            'phone' => $shop->getPhone(),
            'times' => $shop->getTimes(),
            'orderTypes' => $shop->getOrderTypes(),
            'logo' => $shop->getImage(),
            'fiscalAddress' => $shop->getFiscalPort(),
            "social" => $webShop ? $webShop->getSocial() : [],
            "delivery" => $webShop ? $webShop->getDelivery() : [],
            "kitchen" => $webShop ? $webShop->getKitchen() : "",
            'image' => $webShop ? $webShop->getTypedImage(WebShopImage::TYPE_PREVIEW) : null,
            'adminPrinter' => $adminPrinterDTO,
        ];
    }

    private function getShopMenu(Shop $shop)
    {
        $menu = $this->getDoctrine()->getRepository(DishCategory::class)->findAppMenuByShop($shop, true);

        return $this->menuTree($menu, $shop);
    }

    private function sundayFirst($array)
    {
        return array_map(function ($i) {
            return $i + 1 == 7 ? 0 : $i + 1;
        }, $array);
    }

    private function getShopTables(Shop $shop)
    {
        $places = [];

        foreach ($shop->getPlaces() as $place) {
            $places[] = [
                'id' => $place->getId(),
                'name' => $place->getName(),
            ];
        }

        return $places;
    }
}

