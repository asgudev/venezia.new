<?php


namespace DishBundle\Entity\Repository;


use DishBundle\Entity\IngredientInWarehouse;
use Doctrine\ORM\EntityRepository;
use DishBundle\Entity\Ingredient;
use RestaurantBundle\Entity\Warehouse;

class IngredientInWarehouseRepository extends EntityRepository
{

    /**
     * @param $warehouse
     * @param $ingredient
     * @return IngredientInWarehouse
     */
    public function findIngredientInWarehouse(Warehouse $warehouse, Ingredient $ingredient)
    {
        return $this->createQueryBuilder('i')
            ->where('i.warehouse = :warehouse')
            ->andWhere('i.ingredient = :ingredient')
            ->setParameters([
                'ingredient' => $ingredient,
                'warehouse' => $warehouse,
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

}