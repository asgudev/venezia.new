(function ($) {
    $.fn.collection = function (options) {
        var settings = $.extend({
            addButtonText: 'Добавить',
            deleteButtonText: 'Удалить',
            items: [],
            uniqueItems: [],
            unique: false,
            addItemCallback: function (item) {
            },
        }, options);


        var $collectionHolder = this;

        var $addItemLink = $('<a href="#" class="md-btn md-btn-success">' + settings.addButtonText + '</a>');
        var $newItemLi = $('<div />').append($addItemLink);

        $collectionHolder.append($newItemLi);

        $collectionHolder.data('index', $collectionHolder.find(':input').length);

        $addItemLink.on('click', function (e) {
            e.preventDefault();
            addItemForm($collectionHolder, $newItemLi);
        });

        $collectionHolder.find('div.item-row').each(function () {
            addItemFormDeleteLink($(this));
        });

        function addItemForm() {
            var prototype = $collectionHolder.data('prototype');

            var index = $collectionHolder.data('index');

            var newForm = prototype.replace(/__name__/g, index);


            $collectionHolder.data('index', index + 1);


            var $newFormLi = $('<div></div>').append(newForm);
            $newItemLi.before($newFormLi);


            if (settings.unique) {
                settings.uniqueItems.push($newFormLi);
                settings.unique();
            }

            //specific
            //$newFormLi.find(".customPrice").val($("#category_discounts").val());

            addItemFormDeleteLink($newFormLi);
            settings.addItemCallback($newFormLi);
        }

        function addItemFormDeleteLink($itemFormLi) {
            var $removeFormA = $('<div class="uk-width-medium-1-6"><a href="#" class="md-btn md-btn-danger" style="margin-top: 10px;">' + settings.deleteButtonText + '</a></div>');
            $itemFormLi.find(".uk-grid").append($removeFormA);

            $removeFormA.on('click', function (e) {
                e.preventDefault();
                $itemFormLi.remove();
            });
        }



        return this;
    };
}(jQuery));

(function ($) {
    $.fn.select2entity = function (options) {
        this.each(function () {
            var request;
            var $s2 = $(this),
                limit = $s2.data('page-limit') || 0,
                scroll = $s2.data('scroll'),
                prefix = Date.now(),
                cache = [];

            var reqParams = $s2.data('req_params');
            if (reqParams) {
                $.each(reqParams, function (key, value) {
                    $('*[name="' + value + '"]').on('change', function () {
                        $s2.val(null);
                        $s2.trigger('change');
                    });
                });
            }

            $s2.select2($.extend(true, {
                createTag: function (data) {
                    if ($s2.data('tags') && data.term.length > 0) {
                        var text = data.term + $s2.data('tags-text');
                        return {id: $s2.data('new-tag-prefix') + data.term, text: text};
                    }
                },
                ajax: {
                    url: $s2.data('ajax--url') || null,
                    transport: function (params, success, failure) {
                        // is caching enabled?
                        if ($s2.data('ajax--cache')) {
                            var key = prefix + ' page:' + (params.data.page || 1) + ' ' + params.data.q,
                                cacheTimeout = $s2.data('ajax--cacheTimeout');
                            if (typeof cache[key] == 'undefined' || (cacheTimeout && Date.now() >= cache[key].time)) {
                                return $.ajax(params).fail(failure).done(function (data) {
                                    cache[key] = {
                                        data: data,
                                        time: cacheTimeout ? Date.now() + cacheTimeout : null
                                    };
                                    success(data);
                                });
                            } else {
                                success(cache[key].data);
                            }
                        } else {
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax(params).fail(failure).done(success).always(function () {
                                request = undefined;
                            });

                            return request;
                        }
                    },
                    data: function (params) {
                        var ret = {
                            'q': params.term,
                            'field_name': $s2.data('name'),
                            'class_type': $s2.data('classtype')
                        };

                        var reqParams = $s2.data('req_params');
                        if (reqParams) {
                            $.each(reqParams, function (key, value) {
                                ret[key] = $('*[name="' + value + '"]').val()
                            });
                        }

                        // only send the 'page' parameter if scrolling is enabled
                        if (scroll) {
                            ret['page'] = params.page || 1;
                        }

                        return ret;
                    },
                    processResults: function (data, params) {
                        var results, more = false, response = {};
                        params.page = params.page || 1;

                        if ($.isArray(data)) {
                            results = data;
                        } else if (typeof data == 'object') {
                            // assume remote result was proper object
                            results = data.results;
                            more = data.more;
                        } else {
                            // failsafe
                            results = [];
                        }

                        if (scroll) {
                            response.pagination = {more: more};
                        }
                        response.results = results;

                        return response;
                    }
                }
            }, options || {}));
        });
        return this;
    };
})(jQuery);
