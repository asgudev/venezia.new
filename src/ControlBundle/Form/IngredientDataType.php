<?php

namespace ControlBundle\Form;

use ControlBundle\Entity\IngredientData;
use DishBundle\Entity\Ingredient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientDataType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ingredient', EntityType::class, [
                'label' => 'Ингредиент',
                'class' => Ingredient::class,
                'choice_label' => 'name'
            ])
            ->add("balance_prev", TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('income', TextType::class, [
                'label' => 'Новые',
                'required' => true,
                'data' => 0,
            ])
            ->add('waste', TextType::class, [
                'label' => 'Потери',
                'required' => true,
                'data' => 0,
            ])
            ->add('balance', TextType::class, [
                'label' => 'Осталось',
                'required' => true,
                'data' => 0,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => IngredientData::class,
        ));
    }
}