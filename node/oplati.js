const express = require("express");
const request = require('request');
const https = require('https');
const mysql = require('mysql');
const YAML = require('yamljs');
const redis = require("redis").createClient();


redis.on('error', err => console.log('Redis Client Error', err));

redis.connect().then(console.log).catch(console.log);

let config = YAML.load('../app/config/parameters.yml');

let app = express();

app.listen(3003, function () {
    console.log('Example app listening on port 3000!');
});

let baseUrl = 'https://cashboxapi.o-plati.by/ms-pay';

const connection = mysql.createConnection({
    host: config.parameters.database_host,
    user: config.parameters.database_user,
    password: config.parameters.database_password,
    database: config.parameters.database_name,
});
connection.connect();

setInterval(() => {
    connection.query("SELECT 1+1", [], function (res) {
        console.log(res)
    });
}, 200000);

class Oplati {
    constructor() {
        this.shop = {};
        this.authHeaders = {
            // "regNum": config.parameters.oplati_regnum,
            'token': false,
            'Content-Type': 'application/json',
        };
    }

    async init(shopId) {
console.log("init");
        return new Promise(async (resolve, reject) => {
            connection.query("SELECT * FROM shop WHERE id = ?", [shopId], async (error, results) => {
                this.shop = JSON.parse(JSON.stringify(results[0]));
console.log("init2");

                this.shop.custom_params = JSON.parse(this.shop.custom_params);
                this.authHeaders.regNum = this.shop.custom_params.oplatiId;
console.log(this.authHeaders.regNum);

                const reply = await redis.get("oplati_last_token_" + this.shop.id);
console.log("init3", reply);

                    if (!reply) {
                        this.token();
                    } else {
                        this.authHeaders.token = reply.toString();
                    }
                    resolve();
            })

        });
    };

    async token() {
        request.get({
            url: baseUrl + "/tokens",
            method: 'GET',
            headers: this.authHeaders,
        }, (error, response, body) => {
            console.log(body);

            if (!error && response.statusCode === 200) {
                this.authHeaders.token = response.headers.token;
                redis.set("oplati_last_token_" + this.shop.id, this.authHeaders.token);
            } else {
                console.log(error);
            }
        });
    };

    async initPayment(type = 3) {
        return new Promise(async (resolve, reject) => {
            if (!this.authHeaders.token) {
                await this.token();
            }
            request.post({
                url: baseUrl + "/pos/demandPayment",
                method: 'POST',
                json: {paymentMode: type},
                headers: this.authHeaders,
            }, (error, response, body) => {
                console.log(body);
                if (!error && response.statusCode === 200) {
                    this.authHeaders.token = response.headers.token;
                    redis.set("oplati_last_token_" + this.shop.id, this.authHeaders.token);
                    resolve(body);
                } else {
                    console.log(error);
                }
            });
        });
    };

    async getConsumerStatus(sessionId) {
        let url = baseUrl + "/pos/consumerStatus?sessionId=" + sessionId;
        return new Promise((resolve, reject) => {
            request.get({
                url: url,
                method: 'GET',
                headers: this.authHeaders,
                json: true,
            }, function (error, response, body) {
                if (!error && response.statusCode === 200) {
                    resolve(body);
                } else {
                    console.log(error);
                    console.log(body);
                    resolve(false);
                }
            });
        });
    };

    async status(id) {
        return new Promise((resolve, reject) => {
            request.get({
                url: baseUrl + "/pos/payments/" + id,
                method: 'GET',
                headers: this.authHeaders,
                json: true,
            }, function (error, response, body) {
                console.log(body);
                if (!error && response.statusCode === 200) {
                    resolve(body);
                } else {
                    console.log(error);
                }
            });
        });

    };

    async sendOrder(data) {
        return new Promise((resolve, reject) => {
            request.post({
                url: baseUrl + "/pos/payments",
                method: 'POST',
                json: data,
                headers: this.authHeaders,
            }, (error, response, body) => {
                console.log(body);
                console.log(response.headers);
                if (!error && response.statusCode === 200) {
                    this.authHeaders.token = response.headers.token;
                    redis.set("oplati_last_token_" + this.shop.id, this.authHeaders.token);
                    resolve(body);
                } else {
                    console.log(error);
                }
            });
        });
    };
}


async function closeOrder(id) {
    connection.query("UPDATE orders SET status=4, cash_type=4 WHERE id=?", [id], function (error, result) {
        console.log(error);
        console.log(result);
    });
}


async function getJSONOrder(id) {

    return new Promise((resolve, reject) => {
        connection.query("SELECT o.*, s.name, DATE(o.created_at) as oShift FROM orders o LEFT JOIN shop s on o.shop_id = s.id WHERE o.id=?", [id], function (error, result) {
            let orderData = result[0];
            let order = {
                "shift": orderData["oShift"],
                "regNum": config.parameters.oplati_regnum,
                "orderNumber": orderData["id"],
                "details": {
                    "receiptNumber": orderData["local_id"],
                    "items": [],
                    "footerInfo": orderData["name"],
                },
            };
            connection.query("SELECT t1.*, d.name FROM order_dish t1 LEFT JOIN dish d on t1.dish_id = d.id WHERE t1.order_id=?", [id], function (error, result) {
                let sum = 0;
                result.forEach(function (__dish) {
                    order.details.items.push({
                        "type": 1,
                        "name": __dish.name,
                        "quantity": __dish.quantity,
                        "price": __dish.dish_price,
                        "cost": (__dish.dish_price + __dish.dish_discount)
                    });
                    sum += (__dish.quantity * __dish.dish_price);
                });

                order.sum = sum;
                order.details.amountTotal = sum;
                resolve(order);
            });
        });
    });

}

let orders = {};
let statusChecker = false;

app.get('/status', async (req, res) => {
    let id = req.query.id;
    res.setHeader('Access-Control-Allow-Origin', "*");
    res.send(orders[req.query.id]);
});
app.get('/qr', async (req, res) => {
    let id = req.query.id,
        shop = req.query.shop;
    orders[id] = {
        status: 0,
    };

    let oplati = new Oplati();
    await oplati.init(shop);

    let init = await oplati.initPayment(2);

    res.setHeader('Access-Control-Allow-Origin', "*");
    res.send("https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=" + init.dynamicQR);
    console.log(typeof statusChecker);
    if ((typeof statusChecker) === "object") {
        clearInterval(statusChecker);
    }
    console.log(typeof statusChecker);

    statusChecker = setInterval(async () => {
        let status = await oplati.getConsumerStatus(init.sessionId);
        console.log(status);
        if (status.isConsumerReady) {
            clearInterval(statusChecker);
            let order = await getJSONOrder(id);

            let result = await oplati.sendOrder(order);
            let orderStatusChecker = setInterval(async () => {
                let status = await oplati.status(result.paymentId);
                console.log(status);
                console.log(id);
                if (status.status == 1) {
                    closeOrder(id);
                    orders[id] = {
                        status: 4,
                    };
                    clearInterval(orderStatusChecker);
                }
            }, 5000);

            setTimeout(() => {
                clearInterval(orderStatusChecker)
            }, 1200000);

            console.log(result);
        }
    }, 5000);

    setTimeout(() => {
        clearInterval(statusChecker)
    }, 60000);

});

app.get('/pin', async (req, res) => {
    let id = req.query.id,
        shop = req.query.shop;

    orders[id] = {
        status: 0,
    };

    let oplati = new Oplati();
    await oplati.init(shop);

    let init = await oplati.initPayment(1);

    res.setHeader('Access-Control-Allow-Origin', "*");
    res.send(init.pin);

    console.log(typeof statusChecker);
    if ((typeof statusChecker) === "object") {
        clearInterval(statusChecker);
    }
    console.log(typeof statusChecker);

    statusChecker = setInterval(async () => {
        let status = await oplati.getConsumerStatus(init.sessionId);
        console.log(status);
        if (status.isConsumerReady) {
            clearInterval(statusChecker);
            let order = await getJSONOrder(id);

            let result = await oplati.sendOrder(order);
            let orderStatusChecker = setInterval(async () => {
                let status = await oplati.status(result.paymentId);
                console.log(id);
                if (status.status == 1) {
                    closeOrder(id);
                    orders[id] = {
                        status: 4,
                    };
                    clearInterval(orderStatusChecker);
                }
            }, 5000);
            setTimeout(() => {
                clearInterval(orderStatusChecker)
            }, 1200000);

            console.log(result);
        }
    }, 5000);

    setTimeout(() => {
        clearInterval(statusChecker)
    }, 60000);

});
