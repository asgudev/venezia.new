<?php

namespace RestaurantBundle\Events;

use DishBundle\Entity\Dish;
use Symfony\Component\EventDispatcher\Event;

class DishEvent extends Event
{

    const TABLE_UPDATE = 'dish.table.update';

    private $data;

    private $type;

    private $dish;

    /**
     * @param Dish $dish
     */
    public function __construct(Dish $dish, $type, $data = null)
    {
        $this->dish = $dish;
        $this->type = $type;
        $this->data = $data;
    }

    /**
     * @return Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getType()
    {
        return $this->type;
    }



}