var cortage = [];

cortage.push({
    time: new Date(),
    client: [window.innerWidth, window.innerHeight],
    path: location.pathname,
});
/*
document.body.addEventListener('click', function (e) {
    cortage.push({
        time: new Date(),
        coords: [e.clientX, e.clientY],
    });
}, true);

setInterval(function () {
    $.ajax({
        url: "/tracker",
        type: "POST",
        json: true,
        data: JSON.stringify(cortage),
        success: function (response) {
            cortage = [];
        }
    });
}, 10000);*/


// if (typeof html2canvas == "function") {
    async function report() {
        html2canvas(document.querySelector("body")).then(canvas => {
            let formData = new FormData();
            let req = new XMLHttpRequest();

            // formData.append("box", JSON.stringify(box));
            formData.append("screenshot", canvas.toDataURL());

            req.open("POST", '/tracker');
            req.send(formData);
        });
    }

    async function makeScreenshot(selector = "body") {
        return new Promise((resolve, reject) => {
            let node = document.querySelector(selector);

            html2canvas(node, {
                onrendered: (canvas) => {
                    let pngUrl = canvas.toDataURL();
                    resolve(pngUrl);
                }
            });
        });
    }

