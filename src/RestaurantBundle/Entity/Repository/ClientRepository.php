<?php

namespace RestaurantBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use FrontendBundle\Entity\WebShop;
use FrontendBundle\Entity\WebShopImage;
use DishBundle\Entity\DishCategory;
use RestaurantBundle\Entity\Shop;

class ClientRepository extends EntityRepository
{
    public function findForDelivery() {
        return $this->createQueryBuilder('c')
            ->where("c.id in (:ids)")
            ->setParameters([
                'ids' => [2,207],
            ])
            ->getQuery()
            ->getResult();
    }


}
