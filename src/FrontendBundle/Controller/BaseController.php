<?php

namespace FrontendBundle\Controller;

use FrontendBundle\Entity\WebOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    const IS_PROMO = true;

    public function render($view, array $parameters = array(), Response $response = null)
    {
        if (!array_key_exists('orderSum', $parameters)) {
            $parameters['orderSum'] = 0;
        }

        if (!array_key_exists('order', $parameters)) {
            $parameters['order'] = $this->get("session.order.manager")->getOrderObjectFromSession();
            $parameters['orderSum'] = $this->calcOrderSum($parameters['order']);
        }

        return parent::render($view, $parameters, $response);
    }


    public function calcOrderSum(WebOrder $order)
    {
        $sum = 0;
        $dishes = [];

        if ($order->getDishes()) {
            foreach ($order->getDishes() as $webOrderDish) {
                if ($webOrderDish->getDish()->getCategory()->getId() != 32) {
                    $sum += $webOrderDish->getFullPrice() * $webOrderDish->getQuantity();
                } else {
                    for ($i = 0; $i < $webOrderDish->getQuantity(); $i++) {
                        $dishes[] = $webOrderDish->getDish()->getPrice();
                    }
                }
            }
            rsort($dishes);

            $discountedNum = (int)floor(count($dishes) / 2);

            for ($i = 0; $i < count($dishes) - $discountedNum; $i++) {
                $sum += $dishes[$i];
            }
        }

        return $sum;
    }

}