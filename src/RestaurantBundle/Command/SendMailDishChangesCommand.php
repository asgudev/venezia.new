<?php

namespace RestaurantBundle\Command;

use DishBundle\Entity\DishHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendMailDishChangesCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('restaurant:changes:email');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();


        try {

            /** @var DishHistory[] $changes */
            $changes = $doctrine->getRepository('DishBundle:DishHistory')->findNewChanges();
            if (count($changes) > 0) {
                $users = $doctrine->getRepository('UserBundle:User')->findByRole('ROLE_SHOP_MANAGER');
                $emails = [];
                foreach ($users as $user)
                    $emails[] = $user->getEmail();


                $mail = <<<EOD
There are new price changes |  Есть новые изменения цен
<a href="//admin.venezia.by/admin/dish/changes">Просмотреть</a>
EOD;


                $message = \Swift_Message::newInstance()
                    ->setSubject('There are new price changes Есть новые изменения цен')
                    ->setFrom('send@italbrest.com')
                    ->setTo($emails)
                    ->setBody(
                        $mail,
                        'text/html'
                    );
                $this->getContainer()->get('mailer')->send($message);
            }


        } catch (\Exception $em) {
            dump($em);
            $output->write('failed');
        }
    }


}
