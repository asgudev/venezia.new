<?php


namespace DTO;


use OrderBundle\Entity\Order;

class OrderDTO
{

    private $masterId;

    /**
     * @param Order $order
     * @return OrderDTO
     */
    static function fromObject(Order $order)
    {
        $dto = new self();
        $dto->masterId = $order->getId();

        return $dto;
    }
}