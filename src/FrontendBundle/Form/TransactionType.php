<?php

namespace FrontendBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class TransactionType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ap_storeid', HiddenType::class, [
                'data' => '123'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Оплатить'
            ])
        ;

    }
}