pathToPiceditor = $('script:last')[0].src.replace('editor.js', '');
$(function () {
    if (window.editPhoto) return;
    var r = 35,
        d = 2 * r,
        blur = 6,
        alpha = 0.6, html = '',
        img0 = new Image(),
        img1 = new Image(),
        img = new Image(),
        isMdown,
        flc = window.FlashCanvas,
        X = 0,
        Y = 0,
        w, h, target, callback, ctx, ct1;

    editPhoto = function (src, targ, cbk) {
        img0.src = img1.src = img.src = '';
        img0.src = src;
        target = targ;
        callback = cbk || function () {
            }
    };
    function getOE(event) {
        return event.originalEvent || event
    };
    $('body').append(html);
    $('#piceditor, #piceditor #closer').click(function (e) {
        if (e.target == this) $('#piceditor').fadeOut()
    })
    $('#piceditor #saveimg').click(function () {
        if (this.disabled) return;
        var el = this;
        $('#piceditor .result').html('&nbsp;<img src="' + pathToPiceditor + 'loading.gif" style="height:0.45em; width:80px">')
            .load(pathToPiceditor + 'saveimg.php', {
                target: (target || img0.src).replace(location.protocol + '//' + location.hostname, ''),
                image: canvas.toDataURL('image/jpeg', 0.9)
            }, function (a, b, c) {
                $(el).css('backgroundImage', '');
                callback(a, b, c)
            });
        $(this).css('backgroundImage', 'url("' + pathToPiceditor + 'loading.gif")')[0].disabled = true
    })
    $('#piceditor #cover').mousedown(function (event) {
        X = 0;
        Y = 0;
        isMdown = 1;
        event = getOE(event);
        draw(event);
        return false
    }).bind('dragstart slectstart', function () {
        return false
    })
        .hover(function () {
            $('#outln').fadeIn(flc ? 0 : 200)
        }, function () {
            $('#outln').fadeOut(flc ? 0 : 200)
        })
        .bind('mousewheel DOMMouseScroll', function (e) {
            e = getOE(e)
            $('.patern').removeClass('sel');
            r = (e.detail || -e.wheelDelta) > 0 ? r * 1.1 : r / 1.1;
            d = r * 2
            draw(X, Y)
            return false;
        }).mousemove(draw).dblclick(function (e) {
            var back = $('#outln').toggleClass("back").hasClass("back"), curImg = back ? img0 : img;
            ctx.fillStyle = ctx.strokeStyle = ctx.createPattern(curImg, 'no-repeat');
            ct1.fillStyle = ct1.createPattern(curImg, 'no-repeat');
            ct1.strokeStyle = back ? '#fff' : '#000';
            alpha = 1 - alpha;
            draw(e)
            $('.prop select')[0].selectedIndex = back ? 1 : 0
        })
    $('#piceditor .prop select:eq(0)').change(function () {
        $('#cover').dblclick()
    })
    $('#piceditor .prop select:eq(1)').change(function () {
        round = $('.patern').toggleClass("round").hasClass("round")
    }).change()
    $('#piceditor a[href="#blur"]').click(function () {
        ctx.drawImage(img, 0, 0);
        return false
    })
    $('#piceditor a[href="#restore"]').click(function () {
        ctx.drawImage(img0, 0, 0);
        return false
    })
    $('body').mouseup(function () {
        if (isMdown) {
//	$('#result')[0].value=canvas.toDataURL('image/jpeg', 0.9);
            $('#saveimg').removeAttr('disabled');
            isMdown = X = Y = 0
        }
    })
    $(window).resize(function () {
        $('#editorcontent').css({
            marginTop: function () {
                return Math.max($(window).height() / 2 - $(this).height() / 2 - parseInt($(this).css('paddingTop')), 0) + 'px'
            }
        })
    })
    var canvas = $(document.createElement('canvas')).attr('id', 'box2')
            .insertBefore('#piceditor #cover')[0],
        cursor = $(document.createElement('canvas')).attr('id', 'outln')
            .insertBefore('#piceditor #cover')[0];
    if (flc) {
        flc.initElement(canvas);
        flc.initElement(cursor);
        img = {};
        img1 = {}
    }
    ;
    try {
        ctx = canvas.getContext('2d');
        ct1 = cursor.getContext('2d')
    } catch (e) {
    }
    $('.patern').click(function () {
        $('.patern').removeClass('sel');
        d = $(this).addClass('sel').width();
        r = d / 2
    })
    img0.onload = function () {
        $('#piceditor .result').html('')
        $('#piceditor #saveimg').attr('disabled', 1);
        $('#piceditor .prop select')[0].selectedIndex = 0;
        canvas.width = cursor.width = w = img0.width;
        canvas.height = cursor.height = h = img0.height;
        $('#piceditor').fadeIn()
        $('#piceditor #cover').width(w).height(h)
        $('#piceditor #editorcontent').width(w)
        $(window).resize()
        if (flc) {
            onerror0 = onerror;
            onerror = function () {
                return 0
            }
            canvas.innerHTML = '';
            canvas.getContext = 0;
            try {
                flc.initElement(canvas)
            } catch (e) {
            }
            ;
            ctx = canvas.getContext('2d');
            ctx.loadImage(img0, fcLoad)
        } else {
            fcLoad()
        }
        ;
    }
    function fcLoad() {
//img0.src = img0.src + "?" + (new Date()).getTime();
        ctx.drawImage(img0, 0, 0, parseInt(w / blur), parseInt(h / blur))
        img1.src = canvas.toDataURL();
        if (flc) {
            onerror = onerror0;
            ctx.loadImage(img1, img1.onload)
        }
    };
    img1.onload = function () {
        ctx.drawImage(img1, 0, 0, w * blur, h * blur)
        img.src = canvas.toDataURL();
        if (flc)  ctx.loadImage(img, img.onload)
    }
    img.onload = function () {
        $('#piceditor #outln').removeClass("back")
        ctx.drawImage(img0, 0, 0)
        ctx.strokeStyle = ctx.createPattern(img, 'no-repeat');
        ctx.fillStyle = ctx.createPattern(img, 'no-repeat');
        ct1.fillStyle = ct1.createPattern(img, 'no-repeat');
        ct1.lineWidth = 0.8;
        ct1.strokeStyle = '#000'
    }
    function draw(x, y) {
        y = y || x.offsetY || x.layerY;
        x = x.offsetX || x.layerX || x;
        ct1.clearRect(0, 0, w, h);
        ct1.beginPath();
        ct1.globalAlpha = 1;
        round ? ct1.arc(x, y, r, 0, Math.PI * 2, false) : ct1.strokeRect(x - r, y - r, d, d)
        ct1.stroke()
        ct1.globalAlpha = alpha;
        round ? ct1.fill() : ct1.fillRect(x - r, y - r, d, d);
        ct1.closePath();
        if (!isMdown || !img.src) {
            X = x;
            Y = y;
            return false
        }
        ;
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.arc(x, y, r, 0, Math.PI * 2, false);
        ctx.fill();
        ctx.closePath();
        if (!round) ctx.fillRect(x - r, y - r, d, d)
        if ((X == 0) && (Y == 0)) {
            X = x;
            Y = y;
            return false;
        }
        ;
        ctx.lineWidth = d;
        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(X, Y);
        ctx.stroke();
        ctx.closePath();
        X = x;
        Y = y;
        return false
    };
})
