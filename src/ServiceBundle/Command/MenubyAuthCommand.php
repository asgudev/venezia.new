<?php

namespace ServiceBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class MenubyAuthCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {

        $this
            ->setName('service:menuby:auth')
            ->setDescription('Auth Menu.by');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $token = $this->getContainer()->get('menu_by.service')->login();

        $path = $this->getContainer()->get('kernel')->getRootDir();

        $f = fopen($path . '/config/menuby', 'w');
        fwrite($f, $token);
        fclose($f);
    }
}