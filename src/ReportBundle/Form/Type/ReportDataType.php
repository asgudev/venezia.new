<?php

namespace ReportBundle\Form\Type;

use ReportBundle\Entity\Report;
use ReportBundle\Entity\ReportData;
use ReportBundle\Entity\ReportElement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ReportDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('element', Select2EntityType::class, [
                "multiple" => false,
                "label" => "Параметр",
                "class" => ReportElement::class,
                "text_property" => "name",
                'minimum_input_length' => 2,
                'page_limit' => 10,
                'placeholder' => 'Параметр',
                'remote_route' => 'search_report_element',
                'allow_add' => [
                    'enabled' => true,
                    'new_tag_text' => ' (НОВЫЙ)',
                    'new_tag_prefix' => '__',
                    'tag_separators' => '[]'
                ],
            ])
            ->add('value', TextType::class, [
                'label' => 'Значение',
                'required' => true,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ReportData::class,
        ));
    }
}