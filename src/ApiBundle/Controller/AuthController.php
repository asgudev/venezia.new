<?php

namespace ApiBundle\Controller;


use RestaurantBundle\Entity\PromoCard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use UserBundle\Entity\ClientCredentials;


/**
 * Class ApiController
 * @package ApiBundle\Controller
 */
class AuthController extends Controller
{


    /**
     * @param Request $request
     * @Route("/auth", name="api_auth")
     */
    public function authAction(Request $request)
    {
        if ($request->getMethod() == Request::METHOD_OPTIONS) {
            $response = new Response();
            $response->headers->set('Access-Control-Allow-Methods', 'OPTIONS, GET, POST');
            return $response;
        } else {
            $phone = preg_replace("/[^0-9]/", "", json_decode($request->getContent(), true)["phone"]);

            $user = $this->getDoctrine()->getRepository(ClientCredentials::class)->findOneByPhone($phone);

            if (!$user) {
                return new JsonResponse(['false'], 500);
            }

            $response = new Response(json_encode([
                'token' => "123123123123",
                'user' => [
                    'id' => $user->getId(),
                    'name' => $user->getFirstName(),
                    'address' => $user->getAddress(),
                    'card' => [
                        'id' => $user->getCard()->getId(),
                        'localId' => $user->getCard()->getLocalId(),
                        'barCode' => $user->getCard()->getCardId(),
                        'qr' => $user->getCard()->getQrCode(),
                        'discount' => $user->getCard()->getCardDiscount(),
                        'sum' => $user->getCard()->getFullSum(),
                    ]
                ]
            ], JSON_UNESCAPED_UNICODE));
            $response->setCharset('UTF-8');
            $response->headers->set('Access-Control-Allow-Methods', 'OPTIONS, GET, POST');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

}
