<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DishDeleteUnusedCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('restaurant:dish:delete:unused')
            ->setDescription('Delete unused dishes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {

            $doctrine = $this->getContainer()->get('doctrine');
            $em = $doctrine->getManager();

            $dishes = $doctrine->getRepository('DishBundle:Dish')->findDishesWithShop();

            foreach ( $dishes as $dish) {
                $shops = $dish->getShops()->toArray();

                if (empty($shops)) {
                    $dish->setIsDeleted(true);
                    $em->flush();
                }


            }


        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}