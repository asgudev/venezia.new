<?php

namespace FrontendBundle\Form;

use FrontendBundle\Entity\WebOrder;
use FrontendBundle\Entity\WebShop;
use FrontendBundle\Entity\WebShopImage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebShopImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Название',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Описание',
                'required' => false,
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Тип',
                'choices' => array_flip(WebShopImage::IMAGE_TYPES),
//                'choices_as_values' => true,
                'multiple' => false,
                'expanded' => false,
            ])
            ->add("imageFile", FileType::class, [
                'label' => 'Фото',
                'required' => false,
                'file_path' => 'image',
            ])
            ->add('shop', EntityType::class, [
                'label' => 'Заведение',
                'class' => WebShop::class,
                'choice_label' => function ($wShop, $a, $b) {
                    return $wShop->getName();
                },
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('seoAlt', TextType::class, [
                'label' => 'Alt',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Добавить',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WebShopImage::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'photo';
    }
}
