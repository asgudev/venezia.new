<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new JMS\I18nRoutingBundle\JMSI18nRoutingBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\TranslationBundle\JMSTranslationBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Tetranz\Select2EntityBundle\TetranzSelect2EntityBundle(),
            
            new RestaurantBundle\RestaurantBundle(),
            new UserBundle\UserBundle(),
            new TerminalBundle\TerminalBundle(),
            new DeliveryBundle\DeliveryBundle(),
            new ControlBundle\ControlBundle(),
            new FrontendBundle\FrontendBundle(),
            new ApiBundle\ApiBundle(),
            new ServiceBundle\ServiceBundle(),
            new WifiBundle\WifiBundle(),

//            new OverrideDoctrineBundle\OverrideDoctrineBundle(),

        //,
            new ReportBundle\ReportBundle(),
            new DishBundle\DishBundle(),
            new OrderBundle\OrderBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
/**
RENAME TABLE `dish_ingridients` TO `dish_ingredients`;
RENAME TABLE `ingridients` TO `ingredients`;
RENAME TABLE `ingridient_data` TO `ingredient_data`;
ALTER TABLE `ingredient_data` CHANGE `ingridient_id` `ingredient_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `dish_ingredients` CHANGE `ingridient_id` `ingredient_id` INT(11) NULL DEFAULT NULL;

 */
