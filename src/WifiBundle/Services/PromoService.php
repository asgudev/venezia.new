<?php

namespace WifiBundle\Services;


use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use WifiBundle\Entity\WifiClient;
use WifiBundle\Entity\WifiConnect;

class PromoService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Container
     */
    private $container;


    public function __construct(Container $container, EntityManager $em)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function promo(WifiConnect $wifiConnect)
    {

        $promo = $this->container->get('doctrine')->getRepository('WifiBundle:WifiPromo')->findActualPromo($wifiConnect);

        if (($promo) && ($wifiConnect->getClient()->getLastMessageAt() < (new \DateTime())->modify('-7 days')) && ($wifiConnect->getClient()->getIsSubscribed())) {
            if (substr($wifiConnect->getClient()->getPhone(), 0, 4) == '+375') {
                $this->container->get('wifi.sms.service')->sendMessage($wifiConnect->getClient(), $promo->getMessage());
                $wifiConnect->getClient()->setLastMessageAt(new \DateTime());
                $wifiConnect->setPromoSent($promo);
            }
        }
    }

}