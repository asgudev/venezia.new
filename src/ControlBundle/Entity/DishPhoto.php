<?php

namespace ControlBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use OrderBundle\Entity\OrderDish;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="ControlBundle\Entity\Repository\DishPhotoRepository")
 * @ORM\Table(name="order_dish_photo")
 */
class DishPhoto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="photo_path", nullable=true)
     */
    private $photoPath;

    /**
     * @ORM\Column(type="datetime", name="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="integer", name="weight", nullable=true)
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity="OrderBundle\Entity\OrderDish")
     */
    private $orderDish;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="ControlBundle\Entity\DishPhotoCheck", mappedBy="dishPhoto")
     */
    private $checks;

    /**
     * @ORM\Column(name="check_times", type="integer", nullable=true)
     */
    private $checkTimes;

    /**
     * @ORM\Column(name="is_validated", type="boolean", nullable=false, options={"default" = false})
     */
    private $isValidated;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $validatedBy;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish")
     */
    private $validDish;


    public $problems;

    /**
     * @return mixed
     */
    public function getPredict()
    {
        return $this->predict;
    }

    /**
     * @param mixed $predict
     * @return DishPhoto
     */
    public function setPredict($predict)
    {
        $this->predict = $predict;
        return $this;
    }

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->checkTimes = 0;
        $this->isValidated = false;
        $this->predict = [];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set photoPath
     *
     * @param string $photoPath
     *
     * @return DishPhoto
     */
    public function setPhotoPath($photoPath)
    {
        $this->photoPath = $photoPath;

        return $this;
    }

    /**
     * Get photoPath
     *
     * @return string
     */
    public function getPhotoPath()
    {
        return $this->photoPath;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DishPhoto
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return DishPhoto
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return DishPhoto
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set orderDish
     *
     * @param \OrderBundle\Entity\OrderDish $orderDish
     *
     * @return DishPhoto
     */
    public function setOrderDish(\OrderBundle\Entity\OrderDish $orderDish = null)
    {
        $this->orderDish = $orderDish;

        return $this;
    }

    /**
     * Get orderDish
     *
     * @return OrderDish
     */
    public function getOrderDish()
    {
        return $this->orderDish;
    }

    /**
     * @return integer
     */
    public function getCheckTimes()
    {
        return $this->checkTimes;
    }

    /**
     * @param mixed $checkTimes
     * @return DishPhoto
     */
    public function setCheckTimes($checkTimes)
    {
        $this->checkTimes = $checkTimes;
        return $this;
    }

    public function addCheckTime()
    {
        $this->checkTimes += 1;
    }

    /**
     * Add check
     *
     * @param \ControlBundle\Entity\DishPhotoCheck $check
     *
     * @return DishPhoto
     */
    public function addCheck(\ControlBundle\Entity\DishPhotoCheck $check)
    {
        $this->checks[] = $check;

        return $this;
    }

    /**
     * Remove check
     *
     * @param \ControlBundle\Entity\DishPhotoCheck $check
     */
    public function removeCheck(\ControlBundle\Entity\DishPhotoCheck $check)
    {
        $this->checks->removeElement($check);
    }

    /**
     * Get checks
     *
     * @return DishPhotoCheck[]
     */
    public function getChecks()
    {
        return $this->checks;
    }

    /**
     * @return boolean
     */
    public function getisValidated()
    {
        return $this->isValidated;
    }

    /**
     * @param boolean $isValidated
     * @return DishPhoto
     */
    public function setIsValidated($isValidated)
    {
        $this->isValidated = $isValidated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidatedBy()
    {
        return $this->validatedBy;
    }

    /**
     * @param User $validatedBy
     * @return DishPhoto
     */
    public function setValidatedBy($validatedBy)
    {
        $this->validatedBy = $validatedBy;
        return $this;
    }

    public function getAverageRate()
    {
        $sum = 0;
        foreach ($this->checks as $__check) {
            $sum += $__check->getRate();
        }
        if (count($this->checks) > 0)
            return $sum / count($this->checks);
        else
            return 0;
    }

    public function getTopPredicts()
    {
        @arsort($this->predict);
        return @array_slice($this->predict, 0, 5, true);
    }

    /**
     * @return mixed
     */
    public function getValidDish()
    {
        if (($this->isValidated) && ($this->validDish == null)) {
            return ['name' => 'Нет', 'id' => -1];
        } else {
            return $this->validDish;
        }
    }

    /**
     * @param mixed $validDish
     * @return DishPhoto
     */
    public function setValidDish($validDish)
    {
        $this->validDish = $validDish;
        return $this;
    }

}
