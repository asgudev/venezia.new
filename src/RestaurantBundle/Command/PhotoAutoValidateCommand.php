<?php

namespace RestaurantBundle\Command;

use ControlBundle\Entity\DishPhoto;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PhotoAutoValidateCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('photo:validate')
            ->setDescription('Photo auto validate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $doctrine = $this->getContainer()->get('doctrine');
            $validator = $this->getContainer()->get('control.photo.validator');
            $em = $doctrine->getManager();
            $dishPhotos = $doctrine->getRepository('ControlBundle:DishPhoto')->findNotValidated();
//            $dishPhotos[] = $doctrine->getRepository('ControlBundle:DishPhoto')->find(66548);

            /**
             * @var DishPhoto[] $dishPhotos
             */
            foreach ($dishPhotos as $dishPhoto) {
                if ($validator->shouldBeAutoValidated($dishPhoto)) {
                    $dishPhoto->setIsValidated(true);
                    $dishPhoto->setValidatedBy($em->getReference('UserBundle:User', 1));
                    $dishPhoto->setValidDish($dishPhoto->getOrderDish()->getDish());
                    $em->persist($dishPhoto);
                }
            }
            $em->flush();

        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}