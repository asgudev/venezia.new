import imghdr
import os
import numpy as np
from keras import backend as K
from keras.models import load_model
from PIL import Image
from yad2k.models.keras_yolo import yolo_eval, yolo_head
from recognizer import Recognizer
import yaml
import MySQLdb
import MySQLdb.cursors
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
import cgi
import time


image_path = "../../web/uploads/photos/"
output_path = "../../web/uploads/cropped/"

if not os.path.exists(output_path):
    print('Creating output path {}'.format(output_path))
    os.mkdir(output_path)

sess = K.get_session()

with open("model_data/coco_classes.txt") as f:
    class_names = f.readlines()
class_names = [c.strip() for c in class_names]

with open("model_data/yolo_anchors.txt") as f:
    anchors = f.readline()
    anchors = [float(x) for x in anchors.split(',')]
    anchors = np.array(anchors).reshape(-1, 2)

yolo_model = load_model("model_data/yolo.h5")

model_output_channels = yolo_model.layers[-1].output_shape[-1]

model_image_size = yolo_model.layers[0].input_shape[1:3]

yolo_outputs = yolo_head(yolo_model.output, anchors, len(class_names))
input_image_shape = K.placeholder(shape=(2,))
boxes, scores, classes = yolo_eval(
    yolo_outputs,
    input_image_shape,
    score_threshold=0.3,
    iou_threshold=0.5)

recognizer = Recognizer()

conn = MySQLdb.connect(host="134.0.119.169", user="venezia", passwd="9WhsFYfbQjdQz7uR", db="venezia_new", cursorclass=MySQLdb.cursors.DictCursor, charset='utf8', use_unicode=True)
cursor = conn.cursor()
cursor.execute("SET NAMES utf8")

directory = os.listdir(image_path)
files_count = len(directory)
cur_index = 1
for image_file in directory:
    try:
        image_type = imghdr.what(os.path.join(image_path, image_file))
        if not image_type:
            continue
    except IsADirectoryError:
        continue
    print(image_file)
    if (os.path.isfile(output_path + "/" + image_file)):
        continue

    image = Image.open(os.path.join(image_path, image_file))
    # image = image.crop((((image.size[0] - image.size[1]) / 2), 0, (image.size[0] + image.size[1]) / 2, image.size[1]))
    # image.save("1.jpg")
    resized_image = image.resize(tuple(reversed(model_image_size)), Image.BICUBIC)
    image_data = np.array(resized_image, dtype='float32')

    image_data /= 255.
    image_data = np.expand_dims(image_data, 0)  # Add batch dimension.

    out_boxes, out_scores, out_classes = sess.run(
        [boxes, scores, classes],
        feed_dict={
            yolo_model.input: image_data,
            input_image_shape: [image.size[1], image.size[0]],
            K.learning_phase(): 0
        })

    pizza = False
    for i, c in reversed(list(enumerate(out_classes))):
        if (c != 53):
            continue
        predicted_class = class_names[c]
        box = out_boxes[i]
        score = out_scores[i]

        label = '{} {:.2f}'.format(predicted_class, score)

        top, left, bottom, right = box
        top = max(0, np.floor(top + 0.5).astype('int32'))
        left = max(0, np.floor(left + 0.5).astype('int32'))
        bottom = min(image.size[1], np.floor(bottom + 0.5).astype('int32'))
        right = min(image.size[0], np.floor(right + 0.5).astype('int32'))
        area = (bottom - top) * (right - left)
        print(area, label)
        if (area > 200000):
            pizza = True
            cropped = image.crop((left, top, right, bottom))

    if pizza:
        cropped = cropped.resize((640, 640), Image.ANTIALIAS)
        a = cropped.save(os.path.join(output_path, image_file), quality=90)
    else:
        cropped = image.resize((640, 640), Image.ANTIALIAS)
        cropped.save(os.path.join(output_path, image_file), quality=90)
        score = 0

    results = recognizer.recognize(cropped)
    query = "UPDATE `order_dish_photo` SET `predict` = '%s' WHERE `photo_path`='%s'" % (str(json.dumps(results, ensure_ascii=False)), image_file)
    cursor.execute(query)
    conn.commit()

    sorted_result = sorted(results.items(), key=lambda x: x[1], reverse=True)
    print(str(cur_index) + "/" + str(files_count), image_file, score, sorted_result[0])
    cur_index += 1

sess.close()
