<?php

namespace TerminalBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use DishBundle\Entity\Dish;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;

class OrderDishType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $shop = $options["shop"];

        $builder
            ->add('dish', EntityType::class, [
                'class' => Dish::class,
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('quantity', HiddenType::class, [
                'required' => false,
            ])
            ->add('time', HiddenType::class, [
                'required' => false,
            ])
            ->add('dishPrice', HiddenType::class, [
                'required' => false,
            ])
            ->add('dishDiscount', HiddenType::class, [
                'required' => false,
            ])
            ->add('delegate', EntityType::class, [
                'class' => Shop::class,
                'choice_label' => 'id',
                'multiple' => false,
                'expanded' => false,
                'required' => false,
            ])
            ->add('parentOrder', EntityType::class, [
                'class' => Order::class,
                'choice_label' => 'id',
                'multiple' => false,
                'expanded' => false,
                'query_builder' => function (EntityRepository $er) use ($shop) {
                    return $er->createQueryBuilder('o')
                        ->where('o.createdAt >= :date')
                        ->andWhere('o.shop = :shop')
                        ->setParameters([
                            'date' => (new \DateTime())->modify('-1 day'),
                            'shop' => $shop,
                        ]);
                },
            ])
            ->add('parentIndex', IntegerType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OrderDish::class,
            'shop' => null,
        ));
    }
}