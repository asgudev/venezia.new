<?php

namespace ReportBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use ReportBundle\Entity\Report;
use ReportBundle\Entity\ReportElement;

class ReportElementRepository extends EntityRepository
{

    /**
     * @param $q
     * @return ReportElement[]
     */
    public function findByNameOrId($q)
    {
        return $this->createQueryBuilder('re')
            ->where('re.id = :q')
            ->orWhere("re.name LIKE :qname")
            ->setParameters([
                'q' => $q,
                'qname' => "%" . $q . "%",
            ])
            ->getQuery()
            ->getResult();
    }

}