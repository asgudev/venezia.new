<?php

namespace UserBundle\Entity;


use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;

class UserRepository extends EntityRepository
{
    public function findByShop(Shop $shop)
    {
        return $this->createQueryBuilder('u')
            ->where("u.shop = :shop")
            ->andWhere("u.enabled = true")
            ->setParameters([
                'shop' => $shop
            ])
            ->getQuery()
            ->getResult();
    }

    public function findByIds(array $ids)
    {
        return $this->createQueryBuilder('u')
            ->where('u.id in (:ids)')
            ->setParameters([
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();
    }

    public function findByPosition($role, $shop)
    {
        return $this->createQueryBuilder('u')
//            ->where('u.position <= :role')
            ->andWhere("u.shop = :shop")
            ->andWhere('u.enabled = true')
            ->setParameters([
//                'role' => $role,
                'shop' => $shop,
            ])
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByRole($role)
    {
        return $this->createQueryBuilder('u')
//            ->where('u.roles LIKE :roles')
            ->andWhere('u.enabled = true')
            ->setParameters([
//                'roles' => '%"' . $role . '"%',
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $role
     * @param $shop
     * @return User[]
     */
    public function findByRoleInShop($role, $shop)
    {
        return $this->createQueryBuilder('u')
//            ->where('u.position <= :role')
            ->andWhere('u.shop = :shop')
            ->andWhere('u.enabled = true')
            ->setParameters([
//                'role' => $role,
                'shop' => $shop,
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $shops
     * @return User[]
     */
    public function findUsersInShops($shops)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.shop IN (:shops)')
//            ->andWhere('u.enabled = true')
            ->andWhere("u.position in (:pos)")
            ->setParameters([
                'shops' => $shops,
                'pos' => ["Официант", "Администратор"],
            ])
            ->getQuery()
            ->getResult();
    }

}
