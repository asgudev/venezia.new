<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Form\Type\PromoCardType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UserBundle\Entity\ClientCredentials;


/**
 * @Route("/admin")
 */
class PromoCardController extends Controller
{

    /**
     * @Route("/card/list", name="admin_promo_card_list")
     */
    public function promoCardListAction()
    {
        /*$promoCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findBy([
            'type' => PromoCard::TYPE_CARD,
            'isActive' => true,
        ], ['createdAt' => "DESC"], 10);*/

//        $newClients = $this->getDoctrine()->getRepository("UserBundle:ClientCredentials")->findNewClients();
//        $activeCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findActiveCards();
//
//        $issuedCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findIssued();
//
//        $notIssuedCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findNotIssued();
//        $employeeCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findEmployeeCards();
//        $touristCards = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findByType(PromoCard::TYPE_TOURIST);

        $newClients = $this->getDoctrine()->getRepository("UserBundle:ClientCredentials")->findNewClients();
        $activeCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findActiveCards();

        $issuedCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findIssued();

        $notIssuedCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findNotIssued();
        $employeeCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findEmployeeCards();
        $touristCards = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findByType(PromoCard::TYPE_TOURIST);


        return $this->render("@Restaurant/Card/cardList.html.twig", [
//            'cards' => $promoCards,
            'newClients' => $newClients,
            'activeCards' => $activeCards,
            'issuedCards' => $issuedCards,
            'notIssuedCards' => $notIssuedCards,
            'employeeCards' => $employeeCards,
            'touristCards' => $touristCards,
        ]);
    }


    /**
     * @Route("/card/create/{client}", name="admin_promo_card_create")
     */
    public function clientCreateAction(Request $request, ClientCredentials $client)
    {

        $newCard = new PromoCard();
        $newCard->setClient($client);
        $form = $this->createForm(PromoCardType::class, $newCard, [
            'action' => $this->generateUrl("admin_promo_card_create", [
                'client' => $client->getId(),
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
//                $promoCard->setType(PromoCard::TYPE_CARD);
                $promoCard = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findByCardId($newCard->getCardId());

                if (!$promoCard) {
                    $this->addFlash('error', "Карта не существует или уже использована");
                    return $this->redirectToRoute('admin_promo_card_create', [
                        'client' => $client->getId(),
                    ]);
                }


                $promoCard->setClient($newCard->getClient());
                $promoCard->setCreatedBy($this->getUser());
                $promoCard->setIsActive(true);
                $promoCard->setCreatedAt(new \DateTime());
                $promoCard->setLocalId($newCard->getLocalId());

                $client->setCard($promoCard);
                $client->getPromoOrder()->setPromo($promoCard);
                $client->setIsEmployee($form->get('isEmployee')->getData());

                $em = $this->getDoctrine()->getManager();
                $em->persist($promoCard);
                $em->persist($client);
                $em->flush();

                return $this->redirectToRoute("admin_promo_card_list");
            }
        }

/*        $promoCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findBy([
            'type' => PromoCard::TYPE_CARD,
            'isActive' => true,
        ], ['createdAt' => "DESC"], 10);
*/
        $newClients = $this->getDoctrine()->getRepository("UserBundle:ClientCredentials")->findNewClients();
        $activeCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findActiveCards();

        return $this->render("@Restaurant/Card/cardList.html.twig", [
            'form_title' => "Новая карта",
  //          'cards' => $promoCards,
            'newClients' => $newClients,
            'activeCards' => [],
            'cardForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/card/{id}/view", name="admin_promo_card_view")
     */
    public function clientViewAction(Request $request, PromoCard $promoCard)
    {
        if (!$promoCard)
            throw new NotFoundHttpException();

        $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findByPromoCard($promoCard);

        return $this->render('@Restaurant/Card/cardView.html.twig', [
            'promoCard' => $promoCard,
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/card/update", name="admin_promo_card_update")
     */
    public function clientCardUpdateAction(Request $request)
    {
        $isCodeExists = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findBy([
            'localId' => $request->get('localId'),
        ]);
        if ($isCodeExists) return new JsonResponse(false, 403);

        $promoCard = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->find($request->get('cardId'));
        $promoCard->setLocalId($request->get('localId'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($promoCard);
        $em->flush();

        return new JsonResponse();
    }


    /**
     * @Route("/card/{id}/edit", name="admin_promo_card_edit")
     */
    public function clientEditAction(Request $request, PromoCard $promoCard)
    {
        if (!$promoCard) {
            die("error");
        }
        $promoCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findAll();

        $form = $this->createForm(PromoCardType::class, $promoCard, [
            'action' => $this->generateUrl("admin_promo_card_edit", [
                'id' => $promoCard->getId()
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($promoCard);
                $em->flush();

                return $this->redirectToRoute("admin_promo_card_edit", [
                    'id' => $promoCard->getId()
                ]);
            }
        }

        //$orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findByClient($promoCard);

        return $this->render("@Restaurant/Card/cardList.html.twig", [
            'form' => $form->createView(),
            'client_cards' => $promoCards,
            'orders' => [],
            'form_title' => 'Редактирование карты'
        ]);
    }
}
