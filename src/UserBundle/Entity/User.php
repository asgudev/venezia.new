<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Entity\UserRepository")
 * @ORM\Table(name="users")
 *
 * @AttributeOverrides({
 *     @AttributeOverride(
 *         name="emailCanonical",
 *         column=@ORM\Column(
 *             name="emailCanonical",
 *             type="string",
 *             unique=false,
 *             nullable=true,
 *         )
 *     ),
 *     @AttributeOverride(
 *         name="email",
 *         column=@ORM\Column(
 *             name="email",
 *             type="string",
 *             unique=false,
 *             nullable=true,
 *         )
 *     )
 * })
 */
class User extends BaseUser
{
    const ROLE_WAITER = 0;
    const ROLE_ADMINISTRATOR = 1;
    const ROLE_BUHGALT = 2;
    const ROLE_ADMIN = 3;
    const ROLE_SUPER_ADMIN = 4;
    const ROLE_DEVELOPER = 5;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;
    /**
     * @ORM\Column(name="birth", type="date", nullable=true)
     */
    private $birth;
    /**
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    private $phone;
    /**
     * @ORM\Column(name="default_locale", type="string", nullable=true)
     */
    private $defaultLocale;
    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;
    /**
     * @ORM\ManyToMany(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $adminShops;
    /**
     * @ORM\Column(name="is_fiscal_granted", type="boolean", nullable=true, options={"default" = false})
     */
    private $isFiscalGranted;
    /**
     * @ORM\Column(name="is_calc_money_back", type="boolean", nullable=true, options={"default" = false})
     */
    private $isCalcMoneyBack;
    /**
     * @ORM\Column(name="is_separated", type="boolean", nullable=true)
     */
    private $isSeparated;
    /**
     * @ORM\Column(name="is_orders_validated", type="boolean", nullable=true)
     */
    private $isOrdersValidated;
    /**
     * @ORM\Column(name="card_id", type="text", nullable=true)
     */
    private $cardId;
    /**
     * @ORM\Column(name="telegram_chat_id", type="string", nullable=true)
     */
    private $telegramChatId;
    /**
     * @ORM\Column(name="api_token", type="string", nullable=true)
     */
    private $apiToken;
    /**
     * @ORM\Column(name="is2fa", type="boolean", nullable=true)
     */
    private $is2FA;

    /**
     * @ORM\Column(name="uid", type="string", nullable=true)
     */
    private $uid;

    /**
     * @ORM\Column(name="position", type="string", nullable=true)
     */
    private $position;

    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_USER');
        $this->defaultLocale = "ru";
        $this->adminShops = new ArrayCollection();
        $this->isOrdersValidated = false;

        $this->uid = md5(rand(0, 99999999999) . rand(0, 99999999999) . rand(0, 99999999999) . rand(0, 99999999999));
        $this->apiToken = md5(rand(0, 99999999999) . rand(0, 99999999999) . rand(0, 99999999999) . rand(0, 99999999999));
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->defaultLocale = "ru";

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     *
     * @return User
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get defaultLocale
     *
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * Set defaultLocale
     *
     * @param string $defaultLocale
     *
     * @return User
     */
    public function setDefaultLocale($defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return User
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Add adminShop
     *
     * @param \RestaurantBundle\Entity\Shop $adminShop
     *
     * @return User
     */
    public function addAdminShop(\RestaurantBundle\Entity\Shop $adminShop)
    {
        $this->adminShops[] = $adminShop;

        return $this;
    }

    /**
     * Remove adminShop
     *
     * @param \RestaurantBundle\Entity\Shop $adminShop
     */
    public function removeAdminShop(\RestaurantBundle\Entity\Shop $adminShop)
    {
        $this->adminShops->removeElement($adminShop);
    }

    /**
     * Get adminShops
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdminShops()
    {
        return $this->adminShops;
    }

    /**
     * Get isFiscalGranted
     *
     * @return boolean
     */
    public function isFiscalGranted()
    {
        return $this->isFiscalGranted;
    }

    /**
     * Get isFiscalGranted
     *
     * @return boolean
     */
    public function getIsFiscalGranted()
    {
        return $this->isFiscalGranted;
    }

    /**
     * Set isFiscalGranted
     *
     * @param boolean $isFiscalGranted
     *
     * @return User
     */
    public function setIsFiscalGranted($isFiscalGranted)
    {
        $this->isFiscalGranted = $isFiscalGranted;

        return $this;
    }

    /**
     * Get isCalcMoneyBack
     *
     * @return boolean
     */
    public function getIsCalcMoneyBack()
    {
        return $this->isCalcMoneyBack;
    }

    /**
     * Set isCalcMoneyBack
     *
     * @param boolean $isCalcMoneyBack
     *
     * @return User
     */
    public function setIsCalcMoneyBack($isCalcMoneyBack)
    {
        $this->isCalcMoneyBack = $isCalcMoneyBack;

        return $this;
    }

    /**
     * Get isSeparated
     *
     * @return boolean
     */
    public function getIsSeparated()
    {
        return $this->isSeparated;
    }

    /**
     * Set isSeparated
     *
     * @param boolean $isSeparated
     *
     * @return User
     */
    public function setIsSeparated($isSeparated)
    {
        $this->isSeparated = $isSeparated;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * @param mixed $cardId
     * @return User
     */
    public function setCardId($cardId)
    {
        $this->cardId = $cardId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelegramChatId()
    {
        return $this->telegramChatId;
    }

    /**
     * @param mixed $telegramChatId
     * @return User
     */
    public function setTelegramChatId($telegramChatId)
    {
        $this->telegramChatId = $telegramChatId;
        return $this;
    }


    /**
     * @return bool
     */
    public function isOrdersValidated(): bool
    {
        return $this->isOrdersValidated;
    }

    /**
     * @param bool $isOrdersValidated
     * @return User
     */
    public function setIsOrdersValidated(bool $isOrdersValidated): User
    {
        $this->isOrdersValidated = $isOrdersValidated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * @param mixed $apiToken
     * @return User
     */
    public function setApiToken($apiToken)
    {
        $this->apiToken = $apiToken;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIs2FA()
    {
        return $this->is2FA;
    }

    /**
     * @param mixed $is2FA
     * @return User
     */
    public function setIs2FA($is2FA)
    {
        $this->is2FA = $is2FA;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     * @return User
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }


}
