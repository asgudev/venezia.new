<?php
namespace TerminalBundle\EventListener;

use Doctrine\ORM\EntityManager;
use RestaurantBundle\Entity\Printer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TerminalBundle\Events\OrderEvent;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class PrinterListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager;
     */
    protected $em;

    /**
     * @var PrinterFunctionStorage
     */
    protected $functions;

    /**
     * @var Container
     */
    protected $container;


    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->functions = new PrinterFunctionStorage($em, $container);
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            OrderEvent::ORDER_PRINT => 'orderPrint',
            OrderEvent::ORDER_PRINT_KITCHEN => 'orderKitchensPrint',
            OrderEvent::ORDER_PRINT_BAR => 'orderBarPrint',
            OrderEvent::ORDER_PRINT_DELEGATE => 'orderPrintDelegate',
        ];
    }

    public function orderPrintDelegate(OrderEvent $event)
    {
        $this->functions->printDelegatedOrder($event->getPrinter(), $event->getOrder(), [$event->getDish()]);
    }

    public function orderPrint(OrderEvent $event)
    {
        $order = $event->getOrder();

        $this->functions->printAdministratorOrder($order);
        //$this->functions->printAdministratorOrder($order);
    }

    public function orderKitchensPrint(OrderEvent $event)
    {
        $order = $event->getOrder();

        $splitOrder = [];
        $separateByCatOrder = [];
        $delegates = [];
        foreach ($order->getDishes() as $dish) {

            if ($order->getShop()->getId() == 14) {
                $printer = $this->container->get('doctrine')->getRepository('RestaurantBundle:Printer')->findOneBy([
                    'shop' => $dish->getDelegate(),
                    'category' => $dish->getDish()->getCategory(),
                ]);
            } else {
                $printer = $dish->getDish()->getCategory()->getShopPrinter($order->getShop());
            }

            if ($printer == null) {
                $printer = new Printer();
                $printer->setName($order->getShop()->getAdminPrinter());

                if ($order->getShop()->getId() == 14) {
                    $printer->setName($dish->getDelegate()->getAdminPrinter());
                } else {
                    $printer->setName($order->getShop()->getAdminPrinter());
                }
            }
            if (($order->getShop()->getId() == 13) && ($dish->getDish()->getId() == 11160)) {
                $printer = new Printer();
                $printer->setName('bowling');
            }



            if (($printer == 'none') || ($printer == null)) continue;

            if ($dish->getDelegate() !== null) {
                $delegates[$printer->getName()]['dishes'][] = $dish;
                $delegates[$printer->getName()]['printer'] = $printer;
                continue;
            }

            if ($this->container->getParameter('shop.split_dishes')) {
                 $separateByCatOrder[$printer->getName()]['dishes'][$dish->getDish()->getCategory()->getId()][] = $dish;
                 $separateByCatOrder[$printer->getName()]['printer'] = $printer;

                 if (($dish->getDish()->getId() == 13905)) {
                     $printer = new Printer();
                     $printer->setName('cold');
                     $separateByCatOrder[$printer->getName()]['dishes'][$dish->getDish()->getCategory()->getId()][] = $dish;
                     $separateByCatOrder[$printer->getName()]['printer'] = $printer;
                 }


            } else {
                $splitOrder[$printer->getName()]['dishes'][] = $dish;
                $splitOrder[$printer->getName()]['printer'] = $printer;

                 if (($dish->getDish()->getId() == 13905)) {
                     $printer = new Printer();
                     $printer->setName('cold');
                     $splitOrder[$printer->getName()]['dishes'][] = $dish;
                     $splitOrder[$printer->getName()]['printer'] = $printer;
                 }


            }
        }

        foreach ($delegates as $printer => $data) {
            $this->functions->printDelegatedOrder($data['printer'], $order, $data['dishes']);
        }

        if ($this->container->getParameter('shop.split_dishes')) {
            foreach ($separateByCatOrder as $printer => $data) {
                foreach ($data['dishes'] as $cat => $dishes) {
                    $this->functions->printOrder($data['printer'], $order, $dishes);
                }

            }
        } else {
            foreach ($splitOrder as $printer => $data) {
                $this->functions->printOrder($data['printer'], $order, $data['dishes']);
            }
        }
    }

    public function orderBarPrint(OrderEvent $event)
    {
        $order = $event->getOrder();

        $this->functions->printAdministratorOrder($order, 0);
        $this->em->flush();
    }
}
