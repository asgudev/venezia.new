function Promo(config) {
    this.id = config.id;
    this.title = config.title;
    this.discount = config.discount;
    this.icon = config.icon;
    console.log(this);
}

function Dish(data) {
    var globalObject = this;
    this.id = 0;
    this.orderDishId = 0;
    this.index = null;
    this.name = null;
    this.price = 0;
    this.discount = 0;
    this.quantity = 0;
    this.isEditable = true;
    this.parentOrder = false;
    this.parentDish = false;
    this.parentDishBlock = false;
    this.isContainer = false;
    this.ignoreDiscount = false;
    this.isWeight = false;
    this.time = 1;

    this.categoryDelegateTo = false;
    this.categoryDiscount = 0;
    this.categoryGroup = 0;
    this.categoryGroupName = null;

    this.finalPrice = function () {
        //return (this.price * (100 - this.categoryDiscount) / 100);
        return (this.price);
    };

    this.create = function (data) {
        this.id = data.id;
        this.orderDishId = data.orderDishId;
        this.name = data.name;

        this.price = data.price;
        this.discount = data.discount === undefined ? 0 : data.discount;
        this.load = data.load === undefined ? false : data.load;

        this.quantity = data.quantity === undefined ? 1 : data.quantity;
        this.time = data.time === undefined ? 1 : data.time;

        this.isEditable = data.isEditable === undefined ? true : data.isEditable;
        this.isContainer = data.isContainer === '' ? false : true;

        this.ignoreDiscount = data.ignoreDiscount == '1';

        this.parentOrder = data.parentOrder === undefined ? false : data.parentOrder;
        this.parentDish = data.parentOrder === undefined ? false : data.parentDish;

        this.categoryId = data.categoryId === undefined ? false : data.categoryId;
        this.categoryDelegateTo = data.categoryDelegateTo === undefined ? false : data.categoryDelegateTo;
        this.categoryDiscount = data.categoryDiscount === undefined ? 0 : data.categoryDiscount;
        this.categoryGroup = data.categoryGroup;
        this.categoryGroupName = data.categoryGroupName;
        this.isWeight = data.isWeight === 1;


        /*
         if (this.parentOrder) {
         this.price = data.price + data.discount;
         }
         */
    };

    this.resetCategoryDiscount = function () {
        if ((this.discount > 0) && (this.order.promo == false)) {
            this.categoryDiscount = this.discount * 100 / (this.discount + this.price);
        }
    };

    this.setDiscount = function () {
        if ((this.price + this.discount) != 0) {
            this.categoryDiscount = this.discount * 100 / (this.discount + this.price);
        } else {
            this.categoryDiscount = 0;
        }
    };

    this.setPromoPrice = function () {
        this.resetPrice();

        if (!this.ignoreDiscount) {
            if (this.order.promo == 1) {
                if ( this.categoryId == 32) {
                    this.discount = Math.round(this.price * (this.order.promoDiscount / 100) * 100) / 100;
                    this.price = this.price - this.discount;
                }
            } else {
                this.discount = Math.round(this.price * (this.order.promoDiscount / 100) * 100) / 100;
                this.price = this.price - this.discount;
            }
        }
    };

    this.resetPromoPrice = function () {

        this.resetPrice();

        this.discount = Math.round(this.price * this.categoryDiscount) / 100;
        this.price = this.price - this.discount;
    };

    this.resetPrice = function () {
        this.price = this.price + this.discount;
        this.discount = 0;
    };

    this.resetDiscount = function () {
        this.price = this.price + this.discount;
        this.discount = 0;
    };

    this.updatePrices = function () {
        if ((typeof this.order !== null) && (this.order.promo) && (this.order.promoDiscount > 0)) {

            this.price = this.price + this.discount;
            if (this.price !== 0) this.categoryDiscount = this.discount * 100 / this.price;
            this.discount = 0;

            this.discount = Math.round(this.price * (orderEdit.promoDiscount)) / 100;
            this.price = this.price - this.discount;
        } else {

            if (this.categoryDiscount > 0) {
                this.discount = Math.round(this.price * (this.categoryDiscount)) / 100;
                this.price = this.price - this.discount;
            }
        }
    };

    this.create(data);
}

function Order() {
    this.id = null;
    this.options = {};
    this.dishGroups = {};
    this.container = null;
    this.isEditable = true;
    this.isSplitOrder = false;
    this.targetOrder = null;
    this.waiter = null;
    this.lastIndex = 0;
    this.userId = null;
    this.placeId = null;
    this.tableId = null;
    this.order = null;

    this.selectedMoneyType = 0;
    this.clientMoneyCash = 0;
    this.clientMoneyCard = 0;

    this.selectedDish = null;

    this.promo = false;
    this.promoDiscount = 0;

    this.isPromoActive = false;

    this.newDish = null;


    this.sum = 0;
    this.discountSum = 0;

    this.setPromo = function(promo) {
        this.promo = promo.id;
        this.promoDiscount = promo.discount;
        this.promoName = promo.title;

        this.renderOrder();
    };

    this.renderOrder = function () {
        var $order = $("<div />");
        var scrollTo = null;
        var that = this;

        this.resetSums();

        var groups = ['КУХНЯ', 'БАР'];


        for (var gr in groups) {
            var group = groups[gr];
            var $group = $("<div />");
            if (this.dishGroups.hasOwnProperty(group)) {
                var dishesGroup = this.dishGroups[group].dishes;
            } else {
                continue;
            }
            for (var i in dishesGroup) {
                var dish = dishesGroup[i];

                if (this.promo) {
                    dish.setPromoPrice();
                } else {
                    if (dish.load) {
                        dish.setDiscount();
                    } else {
                        dish.resetPromoPrice();
                    }
                }
                dish.load = false;


                var $dish = this.createDishBlock(dish);

                if (dish.scrollTo) {
                    scrollTo = $dish;
                    dish.scrollTo = false;
                }


                /*      -------------       */
                if (dish.parentDish) {
                    $group.find('.parent_' + dish.parentDish.index + ":last").after($dish);
                } else {
                    $group.append($dish);
                }


                this.dishGroups[group].groupSum += ((dish.price + dish.discount) * dish.quantity);

                this.sum += (dish.price * dish.quantity);
                this.discountSum += (dish.discount * dish.quantity);
            }

            var $groupSummary = '<div class="summary">' +
                '<p>Итого ' + group + ': <span class="summary">' + number_format(this.dishGroups[group].groupSum, 2) + '</span></p>' +
                '</div>'
            ;

            $group.append($groupSummary);
            $order.append($group);
        }
        $order.append(this.finalSumBlock());


        $('.promos').empty();
        if (typeof promos !== 'undefined') {
            promos.forEach(function (promo) {

                var $promoBtn = $("<a />", {
                    'class': "md-btn trm-btn"
                }).data({
                    'id': promo.id,
                    'discount': promo.discount,
                    'name': promo.title
                }).html(promo.icon);

                $promoBtn.on("click", {
                    context: that,
                }, that.promoBtnClick);

                if (that.promo == promo.id) {
                    $promoBtn.addClass('trm-btn-selected');

                }

                $('.promos').prepend($promoBtn);
            });
        }

        $order.append('<input type="hidden" name="order[clientMoneyCash]" value="' + that.clientMoneyCash + '">');
        $order.append('<input type="hidden" name="order[clientMoneyCard]" value="' + that.clientMoneyCard + '">');


        if (that.promo) {
            $order.append('<input type="hidden" name="order[promo]" value="' + that.promo + '">');
            $("#discount_info").html('Скидка: <b>' + that.promoName + '</b>').show();
        } else {
            $("#discount_info").hide();
        }

        this.container.html($order);

        if (scrollTo !== null) {
            //$(window).scrollTop(scrollTo.offset().top);
            scrollTo[0].scrollIntoView(false);

        }
    };

    this.promoBtnClick = function (event) {
        var context = event.data.context;
        var promo = $(this).data();

        if (context.promo) {
            if (context.promo == promo.id) {
                context.promo = false;
                context.promoDiscount = 0;
            } else {
                context.promo = promo.id;
                context.promoDiscount = promo.discount;
                context.promoName = promo.name;
            }
        } else {
            context.promo = promo.id;
            context.promoDiscount = promo.discount;
            context.promoName = promo.name;
        }


        context.renderOrder();
    };




    this.finalSumBlock = function () {
        var block = '';
        block += '<div class="summary">';
        block += '<p>Всего: <span class="summary_price">' + number_format((this.sum + this.discountSum), 2) + '</span></p>';

        if (this.discountSum > 0) {
            block += '<p>Скидка: <span class="summary_discount">' + number_format(this.discountSum, 2) + '</span></p>';
        }

        block += '<p>К ОПЛАТЕ: <span class="summary">' + number_format(this.sum, 2) + '</span></p>';
        block += '</div>';

        return block;
    };


    this.resetSums = function () {
        for (var group in this.dishGroups) {
            this.dishGroups[group].groupSum = 0;
        }
        this.sum = 0;
        this.discountSum = 0;
    };

    this.splitOrder = function (target) {
        this.targetOrder = target;
        this.isSplitOrder = !this.isSplitOrder;
        this.renderOrder();
    };

    this.removeDish = function (dish, context) {
        var index = context.dishGroups[dish.categoryGroupName].dishes.indexOf(dish);
        if (index !== -1) {
            //context.dishGroups[dish.categoryGroupName].dishes.splice(index, 1);
        }
    };


    this.moveToOrder = function (event) {
        event.stopPropagation();

        var dish = event.data.dish;
        var context = event.data.context;
        var target = event.data.target;

        dish.quantity -= 1;
        target.hasDish(dish);
        if (dish.quantity == 0) {
            context.removeDish(dish, context);
        }

        target.addDish(dish, true);

        context.renderOrder();
        target.renderOrder();
        return false;
    };


    this.addDish = function (dish, move) {
        if (this.dishGroups[dish.categoryGroupName] === undefined) {
            this.dishGroups[dish.categoryGroupName] = {
                dishes: [],
            };
        }

        if (move) {
            debugger;
        }


        this.dishGroups[dish.categoryGroupName].dishes.push(dish);


        /**
         * --------
         */
        for (var group in this.dishGroups) {
            var dishesGroup = this.dishGroups[group].dishes;
            for (var i in dishesGroup) {
                var localDish = dishesGroup[i];
                if ((localDish.orderDishId == dish.parentDish) && (dish.load)) {
                    dish.parentDish = localDish;
                }
            }
        }


    };

    this.setDishQuantity = function (dish, context) {

    };

    this.hasDish = function (dish) {
        var hasDish = false;
        for (var group in this.dishGroups) {
            var dishesGroup = this.dishGroups[group].dishes;
            for (var i in dishesGroup) {
                var localDish = dishesGroup[i];
                if (localDish == dish) hasDish = true;
            }
        }
        return hasDish;
    };

    this.moveDish = function (dish, target) {

        if (target.dishGroups[dish.categoryGroupName] === undefined) {
            target.dishGroups[dish.categoryGroupName] = {
                dishes: [],
            };
        }

        var index = target.dishGroups[dish.categoryGroupName].dishes.indexOf(dish);
        if (index !== -1) {
            target.dishGroups[dish.categoryGroupName].dishes.push(dish);
        }

        target.renderOrder();
    };

    this.orderPromo = function (promo) {
        if (typeof promo !== 'undefined') {
            var promoBtn = $("<a />", {
                'class': "md-btn trm-btn"
            }).html(promo.icon);

            promoBtn.click(function () {
                $(this).toggleClass("trm-btn-selected");
                return false;
            });
            $('.order-actions').prepend(promoBtn);
        }
    };

    this.createDishBlock = function (dish, index) {
        var $order = $("<div/>", {
            'class': 'dish',
        });

        if (dish.parentDish) {
            $order.css({
                'margin-left': '15px'
            });
        }

        if ((dish.parentDish !== undefined) && (dish.parentDish.isContainer)) {
            dish.price = 0;
        }

        //$order.append("<p class='dishname'>" + dish.name + "</p><p class='meta'>" + dish.metatext + "</p>");
        $order.append("<p class='dishname'>" + dish.name + "</p>");

        var $actions = $("<div />", {
            'class': 'dish_actions'
        });

        if (dish.isEditable) {
            var $deleteButton = $("<button />", {
                'class': 'md-btn trm-btn delete_from_order'
            }).html('<i class="material-icons">remove</i>');

            $deleteButton.on("click", {
                dish: dish,
                context: this,
            }, this.deleteDish);

            $actions.append($deleteButton);
        }

        var $timeButton = $("<button />", {
            'class': 'md-btn trm-btn time'
        }).text(dish.time);

        $timeButton.on("click", {
            dish: dish,
            context: this,
        }, this.showDishTimeBlock);

        if (dish.isEditable == false) {
            $timeButton.addClass('disabled');
        }
        //$actions.append($timeButton);

        var $quantityButton = $("<button />", {
            'class': 'md-btn trm-btn quantity'
        }).text(dish.quantity);

        $quantityButton.on("click", {
            dish: dish,
            context: this,
            container: $actions,

            //}, this.showDishQuantityBlock);
        }, this.showDishTimeBlock);

        if (dish.isEditable == false) {
            $quantityButton.addClass('disabled');
        }
        $actions.append($quantityButton);


        if (this.isSplitOrder) {
            var $moveFromOrderButton = $("<button />", {
                'class': 'md-btn trm-btn down move_from_order',
            }).html("&rarr;");

            $moveFromOrderButton.on("click", {
                dish: dish,
                context: this,
                target: this.targetOrder,
            }, this.moveToOrder);

            $actions.append($moveFromOrderButton);
        }


        $order.append($actions);
        $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][dish]" value="' + dish.id + '">');
        $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][quantity]" value="' + dish.quantity + '">');
        $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][time]" value="' + dish.time + '">');
        $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][dishPrice]" value="' + (dish.price) + '">');
        $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][dishDiscount]" value="' + (dish.discount) + '">');

        if (dish.parentOrder) {
            $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][parentOrder]" value="' + (dish.parentOrder) + '">');
        }

        if (dish.parentDish) {
            $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][parentIndex]" value="' + (dish.parentDish.index) + '">');
        }


        if (dish.parentDish) {
            $order.addClass('parent_' + dish.parentDish.index);
        } else {
            $order.addClass('parent_' + dish.index);
        }

        if (dish.categoryDelegateTo) {
            $order.append('<input type="hidden" name="order[dishes][' + (dish.index) + '][delegate]" value="' + (dish.categoryDelegateTo) + '">');
        }

        if (this.selectedDish == dish) {
            $order.css({
                'background': 'rgb(150, 255, 150)',
            });
        }

        $order.on("click", {
            dish: dish,
            context: this,
            target: this.targetOrder,
        }, this.dishSelect);

        return $order;
    };

    this.dishSelect = function (event) {
        var order = event.data.context,
            dish = event.data.dish;

        if (order.selectedDish == dish) {
            order.selectedDish = null;
        } else {
            order.selectedDish = event.data.dish;
        }


        order.renderOrder();
        return false;
    };

    this.deleteDish = function (event) {
        var dish = event.data.dish,
            context = event.data.context;

        event.preventDefault();
        event.stopPropagation();

        var index = context.dishGroups[dish.categoryGroupName].dishes.indexOf(dish);
        if (index !== -1) {
            context.dishGroups[dish.categoryGroupName].dishes.splice(index, 1);
        }
        if ($("#custom_order").data("status") == 0) {
            for (let __group in context.dishGroups) {
                context.dishGroups[__group].dishes.forEach(function(__dish) {
                    if (__dish.index > this) __dish.index -= 1;
                }, index);
            }
        }
        context.renderOrder();
        return false;
    };

    this.createDishObject = function (data, scrollTo) {
        if (scrollTo === undefined) scrollTo = false;
        var dish = new Dish(data),
            metatext = number_format((dish.price + dish.discount), 2) + "р.";

        if ((dish.categoryDiscount > 0)) {
            metatext += " (скидка " + dish.categoryDiscount + "%)";
        }
        dish.metatext = metatext;
        dish.index = this.lastIndex++;
        dish.scrollTo = scrollTo;

        dish.order = this;
        dish.resetCategoryDiscount();
        if (this.selectedDish) {
            dish.parentDish = this.selectedDish;
        }


        this.addDish(dish);
    };

    this.showDishQuantityBlock = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var dish = event.data.dish,

            $initiator = event.data.parentEvent.data.container,
            context = event.data.context,

            $quantityBlock = $("<div />", {
                'class': 'quantityBlock'
            });

        $quantityBlock.append($("<span />", {
            'class': 'title',
        }).text('Количество'));


        if (dish.isWeight) {
            if (dish.quantity !== 0) {
                var quantity = dish.quantity * 1000;
            } else {
                var quantity = 0;
            }
            var $customQuantityBlock = $("<div />", {
                'class': 'custom_quantity'
            }).text(quantity);
            $quantityBlock.append($customQuantityBlock);


            for (var i = 1; i < 10; i++) {
                var $button = $("<button />", {
                    'class': 'md-btn trm-btn'
                }).text(i);

                $button.data({
                    "key": i,
                    "val": i
                });

                $button.on("click", {
                    dish: dish,
                    context: context,
                    quantityBlock: $quantityBlock,
                    customQuantity: $customQuantityBlock,
                }, context.customDishQuantity);

                $quantityBlock.append($button);
            }

            var $okButton = $("<button />", {
                'class': 'md-btn trm-btn'
            }).text('OK');

            $okButton.on('click', {
                dish: dish,
                context: context,
                quantityBlock: $quantityBlock,
                customQuantity: $customQuantityBlock,
            }, context.setCustomQuantity);

            $quantityBlock.append($okButton);

            var $button = $("<button />", {
                'class': 'md-btn trm-btn'
            }).text(0);

            $button.data({
                "key": 0,
                "val": 0
            });

            $button.on("click", {
                dish: dish,
                context: context,
                quantityBlock: $quantityBlock,
                customQuantity: $customQuantityBlock,
            }, context.customDishQuantity);

            $quantityBlock.append($button);

            var $backspaceButton = $("<button />", {
                'class': 'md-btn trm-btn'
            }).text('←');

            $backspaceButton.on('click', {
                dish: dish,
                context: context,
                quantityBlock: $quantityBlock,
                customQuantity: $customQuantityBlock,
            }, context.quantityBackspace);

            $quantityBlock.append($backspaceButton);

            $quantityBlock.css({
                'width': '144px',
            });


        } else {
            var quantities = {
                '0.5': '1/2',
                '1': '1',
                '2': '2',
                '3': '3',
                '4': '4',
                '5': '5',
                '6': '6',
                '7': '7',
                '8': '8',
                '10': '10',
                '14': '14',
                '15': '15',
            };
            for (var key in quantities) {
                var $button = $("<button />", {
                    'class': 'md-btn trm-btn'
                }).text(quantities[key]);

                $button.data({
                    "key": key,
                    "val": quantities[key]
                });

                $button.on("click", {
                    dish: dish,
                    context: context,
                    quantityBlock: $quantityBlock,
                }, context.updateDishQuantity);

                $quantityBlock.append($button);
            }

        }
        $initiator.after($quantityBlock);

        $('body').on("click", function () {
            $quantityBlock.remove();
        });
        return false;


    };

    this.showDishTimeBlock = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var dish = event.data.dish,
            $initiator = event.data.container,
            context = event.data.context,
            $timeBlock = $("<div />", {
                'class': 'quantityBlock'
            });

        $timeBlock.append($("<span />", {
            'class': 'title',
        }).text('Очередь'));

        var times = {
            '1': '1',
            '2': '2',
            '3': '3',
        };
        for (var key in times) {
            var $button = $("<button />", {
                'class': 'md-btn trm-btn'
            }).text(times[key]);

            $button.data({
                "key": key,
                "val": times[key]
            });

            $button.on("click", {
                dish: dish,
                context: context,
                timeBlock: $timeBlock,
                parentEvent: event,
            }, context.updateDishTime);

            $timeBlock.append($button);
        }


        $initiator.after($timeBlock);

        $('body').on("click", function () {
            $timeBlock.remove();
        });
        return false;


    };

    this.setCustomQuantity = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var dish = event.data.dish,
            context = event.data.context,
            $customQuantity = event.data.customQuantity;

        dish.quantity = parseInt($customQuantity.text()) / 1000;

        event.data.quantityBlock.remove();

        context.renderOrder();
        return false;
    };

    this.quantityBackspace = function (event) {
        event.stopPropagation();
        var $customQuantity = event.data.customQuantity;

        $customQuantity.text(function (index, text) {
            if (text.slice(0, -1)) {
                return 0
            } else {
                return text.slice(0, -1);
            }
        });
        return false;
    };

    this.customDishQuantity = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var dish = event.data.dish,
            context = event.data.context,
            $customQuantity = event.data.customQuantity,
            digit = $(this).data('key');

        if ($customQuantity.text() == '0') $customQuantity.text('');

        $customQuantity.text(function (index, text) {
            return text + digit;
        });

        return false;
    };

    this.updateDishQuantity = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var dish = event.data.dish,
            context = event.data.context;
        dish.quantity = parseFloat($(this).data("key"));

        if ((dish.parentDish !== undefined) && (dish.parentDish.isContainer)) {
            dish.price = 0;
        }

        event.data.quantityBlock.hide();

        context.renderOrder();
        return false;
    };

    this.updateDishTime = function (event) {
        event.preventDefault();
        event.stopPropagation();

        var dish = event.data.dish,
            context = event.data.context;
        dish.time = parseFloat($(this).data("key"));

        event.data.timeBlock.hide();

        //context.renderOrder();

        context.showDishQuantityBlock(event);
        return false;
    };

    var that = this;
}
