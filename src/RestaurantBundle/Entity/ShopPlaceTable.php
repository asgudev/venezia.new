<?php

namespace RestaurantBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Table
 *
 * @ORM\Table(name="tables")
 * @ORM\Entity
 */
class ShopPlaceTable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="ShopPlace", inversedBy="tables")
     */
    private $place;

    /**
     * @ORM\Column(name="meta_coordinates", type="text")
     */
    private $metaCoordinates;


    /**
     * @ORM\Column(name="uuid", type="text")
     */
    private $uuid;

    /**
     * @ORM\Column(name="is_to_go", type="boolean", nullable=false, options={"default" = false})
     */
    private $isToGo;

    /**
     * @ORM\Column(name="is_client", type="boolean", nullable=true, options={"default" = false})
     */
    private $isClient;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    public function __construct()
    {
        $this->status = 0;
        $this->isToGo = false;
        $this->isClient = false;
        $this->uuid = $this->generateRandomString();
    }

    private function generateRandomString($length = 5)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ShopPlaceTable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get place
     *
     * @return \RestaurantBundle\Entity\ShopPlace
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set place
     *
     * @param \RestaurantBundle\Entity\ShopPlace $place
     *
     * @return ShopPlaceTable
     */
    public function setPlace(\RestaurantBundle\Entity\ShopPlace $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get metaCoordinates
     *
     * @return array
     */
    public function getMetaCoordinates()
    {
        return $this->metaCoordinates;
    }

    /**
     * Set metaCoordinates
     *
     * @param array $metaCoordinates
     *
     * @return ShopPlaceTable
     */
    public function setMetaCoordinates($metaCoordinates)
    {
        $this->metaCoordinates = $metaCoordinates;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return ShopPlaceTable
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get isToGo
     *
     * @return boolean
     */
    public function getIsToGo()
    {
        return $this->isToGo;
    }

    /**
     * Get isToGo
     *
     * @return boolean
     */
    public function isToGo()
    {
        return $this->isToGo;
    }

    /**
     * Set isToGo
     *
     * @param boolean $isToGo
     *
     * @return ShopPlaceTable
     */
    public function setIsToGo($isToGo)
    {
        $this->isToGo = $isToGo;

        return $this;
    }

    /**
     * Get isClient
     *
     * @return boolean
     */
    public function getIsClient()
    {
        return $this->isClient;
    }

    /**
     * Set isClient
     *
     * @param boolean $isClient
     *
     * @return ShopPlaceTable
     */
    public function setIsClient($isClient)
    {
        $this->isClient = $isClient;

        return $this;
    }
}
