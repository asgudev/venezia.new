<?php

namespace ControlBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class DocumentController extends Controller
{
    public function indexAction()
    {
        return new Response();
    }
}
