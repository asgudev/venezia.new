<?php

namespace RestaurantBundle\Service;

use Doctrine\DBAL\Platforms\MySqlPlatform;

/**
 * Override Platform to use Partitioning
 * https://stackoverflow.com/questions/6143029/doctrine2-and-mysql-partitioning/7013697
 */
class CustomMySqlPlatform extends MySqlPlatform
{
    public function supportsForeignKeyConstraints()
    {
        return false;
    }
}
