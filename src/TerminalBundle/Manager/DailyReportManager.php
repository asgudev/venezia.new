<?php


namespace TerminalBundle\Manager;


use Doctrine\ORM\EntityManagerInterface;
use RestaurantBundle\Entity\Shop;
use TerminalBundle\Entity\DailyReport;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class DailyReportManager
{

    private $em;

    public function __construct(EntityManagerInterface $entityManager, Container $container )
    {
        $this->em = $entityManager;
        $this->container = $container;
    }


    public function getLastReport(Shop $shop)
    {

        $previousReport = $this->em->getRepository(DailyReport::class)->findLastReport($shop);
        if (!$previousReport) {
            $previousReport = new DailyReport($shop);
            $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
        } else {
            $previousReport = $previousReport[0];
        }

        return $previousReport;
    }

    public function newReport(DailyReport $previousReport)
    {
        $report = new DailyReport($previousReport->getShop());
        $report->setDateStarted($previousReport->getDateClosed());
        $report->setDateClosed(new \DateTime());
//        $report->setName();

        return $report;
    }

    public function validateRevision(DailyReport $dailyReport)
    {


        $revision = [];

        $pizza = $this->container->get("doctrine")->getRepository('OrderBundle:OrderDish')->findPizzaByReport($dailyReport, false);

        $pizzaData = $this->getPizzaData($pizza, [], $dailyReport);

        if ($dailyReport->getShop()->getId() == 6) {
            $delegatedData = $this->getDelegatedPizzaData($dailyReport);
            foreach ($delegatedData as $type => $data)
                $pizzaData['ingr'][$type] += $data;
        }


        $previousReport = $this->container->get("doctrine")->getRepository('TerminalBundle:DailyReport')->findPreviousReport($dailyReport);

        $ingredientIncome = $this->container->get("doctrine")->getRepository('DishBundle:IngredientTransfer')->findInDateRange($previousReport->getShop(), $previousReport->getDateClosed(), new \DateTime());

        $ingrData = [];
        foreach ($ingredientIncome as $__ingr) {
            if (!array_key_exists($__ingr->getIngredient()->getId(), $ingrData))
                $ingrData[$__ingr->getIngredient()->getId()] = 0;

            $ingrData[$__ingr->getIngredient()->getId()] += $__ingr->getQuantity();
        }

        foreach ($previousReport->getIngredients() as $ingredient) {
            $revision[$ingredient->getIngredient()->getName()] = [
                'balance_previous' => $ingredient->getBalance(),
                'income' => 0,
                'income_1c' => array_key_exists($ingredient->getIngredient()->getId(), $ingrData) ? $ingrData[$ingredient->getIngredient()->getId()] : 0,
                'balance' => 0,
                'used' => 0,
                'status' => false,
                'shouldBeValidated' => true,
            ];
        }


        foreach ($dailyReport->getIngredients() as $ingredient) {
            $revision[$ingredient->getIngredient()->getName()]['income'] = $ingredient->getIncome();
            $revision[$ingredient->getIngredient()->getName()]['waste'] = $ingredient->getWaste();
            $revision[$ingredient->getIngredient()->getName()]['balance'] = $ingredient->getBalance();
            $revision[$ingredient->getIngredient()->getName()]['shouldBeValidated'] = $ingredient->getIngredient()->getShouldBeValidated();
        }

        foreach ($pizzaData['ingr'] as $ingredient => $quantity) {
            $revision[$ingredient]['used'] = $quantity;
        }

//if ($_SERVER["REMOTE_ADDR"] == "89.207.221.99") { dump($revision);die(); }


        $user = $dailyReport->getUser();

        $this->container->get('user.logger')->info($user->getName() . '(' . $user->getId() . ')' . ' tried to close shift');
        $status = true;
        foreach ($revision as $ingredient => $data) {
            if (!array_key_exists('income', $data)) {
                $this->container->get('user.logger')->info('no daily data provided');
                $this->container->get('user.logger')->info('failed');
                $status = false;
                break;
            }
            if ($data['shouldBeValidated'] == false) continue;

            $balance = $data['balance_previous'] + $data['income'] - $data['waste'] - $data['used'];

            if ($balance == $data['balance']) {
                $revision[$ingredient]['status'] = true;
            } else {
                $this->container->get('user.logger')->info('incorrect data for ' . $ingredient . " " . $data['balance_previous'] . "+" . $data['income'] . "-" . $data['waste'] . "-" . $data['balance'] . "!=" . $data['used']);
                $this->container->get('user.logger')->info('failed');
                $status = false;
            }
        }

        if ($status) {
            $this->container->get('user.logger')->info('success');
        }

//        if ($this->container->get("kernel")->getEnvironment() == "prod") {
            $this->sendRevisionEmail($dailyReport, $revision, $status);
//        }
        
        if (!$status) {
            $closeCount = $this->container->get("redis.helper")->get('closeCount_' . $user->getShop()->getId()) + 1;
            $this->container->get('redis.helper')->set('closeCount_' . $user->getShop()->getId(), $closeCount);
            $blockedTill = 0;

            if ($closeCount == 3) {
                $blockedTill = time() + 1800;
            }

            if ($closeCount == 6) {
                $blockedTill = time() + 3600;
            }

            if ($closeCount >= 9) {
                $blockedTill = time() + 86400;
            }

            if ($blockedTill) {
                $this->container->get("redis.helper")->set("blockedTill_" . $user->getShop()->getId(), $blockedTill);
            }

            return ['status' => $status, 'closeCount' => $closeCount, "blockedTill" => $blockedTill];
        } else {
            $this->container->get("redis.helper")->delete("blockedTill_" . $user->getShop()->getId());
            $this->container->get("redis.helper")->delete("closeCount_" . $user->getShop()->getId());
            return true;
        }

    }

    private function sendRevisionEmail(DailyReport $dailyReport, $data, $status)
    {
        $status_text = $status === true ? 'SUCCESS' : 'FAIL';

        $message = \Swift_Message::newInstance()
            ->setSubject('Revision ' . $status_text . ", " . $dailyReport->getShop()->getName() . ", " . $dailyReport->getShop()->getAddress())
            ->setFrom('send@italbrest.com')
            ->setTo($dailyReport->getShop()->getId() == 17 ? ['julia.buh@italbrest.com'] : [
//                'julia.buh@italbrest.com',
                'natasha@italbrest.com',
                'margo@italbrest.com',
                'marketing@italbrest.com',
                'zav.proizvodstva@italbrest.com',
            ])
            ->setBody(
                $this->container->get("twig")->render('@Restaurant/Custom/revisionEmail.html.twig', [
                    'report' => $dailyReport,
                    'data' => $data,
                ]),
                'text/html'
            );

        $this->container->get('mailer')->send($message);

        if ($status_text == "FAIL") {
            $message = \Swift_Message::newInstance()
                ->setSubject('Revision ' . $status_text . ", " . $dailyReport->getShop()->getName() . ", " . $dailyReport->getShop()->getAddress())
                ->setFrom('send@italbrest.com')
                ->setTo([
                    'julia.buh@italbrest.com',
                    'ipdesanta@mail.ru',
                ])
                ->setBody(
                    $this->container->get("twig")->render('@Restaurant/Custom/revisionEmail.html.twig', [
                        'report' => $dailyReport,
                        'data' => $data,
                    ]),
                    'text/html'
                );

            $this->container->get('mailer')->send($message);
        }

    }

    private function getPizzaData($pizzas, $deleted, $report = null)
    {

        $pizzaData = [
            'dishes' => [],
            'sum' => 0,
            'quantity' => 0,
            'ingr' => [],
            'takeaway' => [
                'sum' => 0,
                'quantity' => 0,
                'ingr' => 0,
            ],
        ];

        if ($report) {
            foreach ($report->getIngredients() as $ingr) {
                $pizzaData['ingr'][$ingr->getIngredient()->getName()] = 0;
            }
        }

        /**
         * @var OrderDish $pizza
         */
        foreach ($pizzas as $pizza) {
            if ((in_array($pizza, $deleted)) && ($pizza->getOrder() == null)) {
                continue;
            }
            if (mb_substr($pizza->getDish()->getName(), 0, 7) == 'Добавка') {
                continue;
            }

            $name = $pizza->getDish()->getName();
            if (!array_key_exists($name, $pizzaData['dishes'])) {
                $pizzaData['dishes'][$name] = [
                    'sum' => 0,
                    'quantity' => 0,
                    'ingr' => 0,
                ];
            }
            if ($pizza->getDish()->getId() == 11177) {
                $pizzaData['box'] += ($pizza->getQuantity());
                continue;
            }


            if (!array_key_exists(0, $pizza->getDish()->getIngredients()->toArray())) {
                var_dump($pizza->getDish()->getId());
                die();
                continue;
            }

            if ($pizza->getParentOrder()->getTable()->getId() == 189) {
                $pizzaData['takeaway']['sum'] += ($pizza->getCashSum());
                $pizzaData['takeaway']['ingr'] += ($pizza->getDish()->getIngredients()->first()->getQuantity() * $pizza->getQuantity());
                $pizzaData['takeaway']['quantity'] += ($pizza->getQuantity());
            }


            $pizzaData['dishes'][$name]['quantity'] += ($pizza->getQuantity());
            $pizzaData['dishes'][$name]['sum'] += ($pizza->getDishPrice() * $pizza->getQuantity());
            $pizzaData['dishes'][$name]['ingr'] += ($pizza->getDish()->getIngredients()->first()->getQuantity() * $pizza->getQuantity());

            $pizzaData['sum'] += ($pizza->getDishPrice() * $pizza->getQuantity());
            $pizzaData['quantity'] += ($pizza->getQuantity());

            $dishIngredient = $pizza->getDish()->getIngredients()->first()->getIngredient();

            if (!array_key_exists($dishIngredient->getName(), $pizzaData['ingr'])) $pizzaData['ingr'][$dishIngredient->getName()] = 0;

            $pizzaData['ingr'][$dishIngredient->getName()] += ($pizza->getDish()->getIngredients()->first()->getQuantity() * $pizza->getQuantity());
        }

        return $pizzaData;
    }

//  curl -d "shop=6&dateStart=2018-07-30 00:11:15&dateEnd=2018-07-30 17:33:38" -X POST http://10.8.0.22/api/pizza/delegated

    private function getDelegatedPizzaData(DailyReport $report)
    {
        $data = [
            'shop' => '6',
            'dateStart' => $report->getDateStarted()->format('Y-m-d H:i:s'),
            'dateEnd' => $report->getDateClosed()->format('Y-m-d H:i:s'),
        ];

        $ch = curl_init("http://10.8.0.8/api/pizza/delegated");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $res = json_decode(curl_exec($ch), true);

        return $res;
    }
}
