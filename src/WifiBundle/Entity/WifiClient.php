<?php

namespace WifiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Wifi Router
 *
 * @ORM\Table(name="wifi_client")
 * @ORM\Entity(repositoryClass="WifiBundle\Entity\Repository\WifiClientRepository")
 */
class WifiClient
{
    const CODE_LENGTH = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="phone", type="text", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(name="code", type="string")
     */
    private $code;

    /**
     * @ORM\Column(name="mac", type="string", unique=true, nullable=true,)
     */
    private $mac;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="is_subscribed", type="boolean")
     */
    private $isSubscribed;

    /**
     * @ORM\Column(name="token", type="string")
     */
    private $token;

    /**
     * @ORM\Column(name="is_banned", type="boolean")
     */
    private $isBanned;

    /**
     * @ORM\OneToMany(targetEntity="WifiBundle\Entity\WifiConnect", mappedBy="client")
     */
    private $connects;


    /**
     * @ORM\Column(name="last_message_at", type="datetime", nullable=true)
     */
    private $lastMessageAt;



    public function __construct($mac)
    {
        $this->isActive = false;
        $this->isSubscribed = true;
        $this->isBanned = false;
        $this->mac = $mac;


        $this->code = strtoupper(substr(preg_replace("#[a-z]#i", "", md5(uniqid('rid'))), 0, self::CODE_LENGTH));
        $this->token = md5(uniqid('rid', true));


        $this->lastMessageAt = new \DateTime("-6 days");
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return WifiClient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return WifiClient
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set mac
     *
     * @param string $mac
     *
     * @return WifiClient
     */
    public function setMac($mac)
    {
        $this->mac = $mac;

        return $this;
    }

    /**
     * Get mac
     *
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return WifiClient
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isSubscribed
     *
     * @param boolean $isSubscribed
     *
     * @return WifiClient
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->isSubscribed = $isSubscribed;

        return $this;
    }

    /**
     * Get isSubscribed
     *
     * @return boolean
     */
    public function getIsSubscribed()
    {
        return $this->isSubscribed;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return WifiClient
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set isBanned
     *
     * @param boolean $isBanned
     *
     * @return WifiClient
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;

        return $this;
    }

    /**
     * Get isBanned
     *
     * @return boolean
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * Set lastMessageAt
     *
     * @param \DateTime $lastMessageAt
     *
     * @return WifiClient
     */
    public function setLastMessageAt($lastMessageAt)
    {
        $this->lastMessageAt = $lastMessageAt;

        return $this;
    }

    /**
     * Get lastMessageAt
     *
     * @return \DateTime
     */
    public function getLastMessageAt()
    {
        return $this->lastMessageAt;
    }

    /**
     * Add connect
     *
     * @param \WifiBundle\Entity\WifiConnect $connect
     *
     * @return WifiClient
     */
    public function addConnect(\WifiBundle\Entity\WifiConnect $connect)
    {
        $this->connects[] = $connect;

        return $this;
    }

    /**
     * Remove connect
     *
     * @param \WifiBundle\Entity\WifiConnect $connect
     */
    public function removeConnect(\WifiBundle\Entity\WifiConnect $connect)
    {
        $this->connects->removeElement($connect);
    }

    /**
     * Get connects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConnects()
    {
        return $this->connects;
    }
}
