<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Client;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Form\Type\ClientType;
use RestaurantBundle\Service\KEscPos;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TerminalBundle\Entity\DailyReport;
use UserBundle\Entity\UserEvent;

/**
 * @Route("/admin")
 */
class CustomController extends Controller
{
    /**
     * @Route("/test", name="test_printer")
     */
    public function testAction(Request $request)
    {

        $escpos = new KEscPos('TM-T88IV AFU', "pizza", true, true);
        $escpos->UseTranslation(false);

//        $escpos->Font("A");
//        $escpos->Align("center");
//        $escpos->Height('double');
//        $escpos->Double(true);
        $line = "";
//        for ($i = 1024; $i <= 2048; $i++) {
//            $line .= $i . chr($i) . "-";
//        }
        $escpos->Line('tratrattatatrtar тратратратар');
        $escpos->Line('tratrattatatrtar');
        $escpos->Line('тратратратар');
//        $escpos->Line($line);
        $escpos->XFeed();
        $escpos->XFeed();
        $escpos->XFeed();
        $escpos->XFeed();
        $escpos->XFeed();
//        $escpos->Cut();
        $escpos->Close();
        dump($escpos);
        dump(123);
        die();
    }


    /**
     * @Route("/client/list", name="client_list")
     */
    public function clientsListAction(Request $request)
    {
        $clients = $this->getDoctrine()->getRepository("RestaurantBundle:Client")->findAll();
        $client = new Client();

        $form = $this->createForm(ClientType::class, $client, [
            'action' => $this->generateUrl("client_create")
        ]);

        return $this->render("@Restaurant/Custom/clientsList.html.twig", [
            'form' => $form->createView(),
            'form_title' => 'Новый клиент',
            'clients' => $clients,
        ]);
    }

    /**
     * @Route("/client/add", name="client_create")
     */
    public function clientCreateAction(Request $request)
    {
        $client = new Client();

        $form = $this->createForm(ClientType::class, $client, [
            'action' => $this->generateUrl("client_create")
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();

                return $this->redirectToRoute("client_list");
            }
        }
    }

    /**
     * @Route("/client/{id}/edit", name="client_edit")
     */
    public function clientEditAction(Request $request, Client $client)
    {
        if (!$client) {
            die("error");
        }
        $clients = $this->getDoctrine()->getRepository("RestaurantBundle:Client")->findAll();

        $form = $this->createForm(ClientType::class, $client, [
            'action' => $this->generateUrl("client_edit", [
                'id' => $client->getId()
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();

                return $this->redirectToRoute("client_edit", [
                    'id' => $client->getId()
                ]);
            }
        }

        $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findByClient($client);

        return $this->render("@Restaurant/Custom/clientsList.html.twig", [
            'form' => $form->createView(),
            'clients' => $clients,
            'orders' => $orders,
            'form_title' => 'Редактирование клиента'
        ]);
    }

    /**
     * @param Request $request
     * @Route("/export/shop/{id}/manual/{report}", name="manual_export_command")
     */
    public function exportManualAction(Request $request, Shop $shop, DailyReport $report)
    {
        $report = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->find($report);

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'restaurant:report:export',
            '--report' => $report
        ));

        $output = new BufferedOutput();
        $application->run($input, $output);

        $content = $output->fetch();

        return new JsonResponse($content);
    }


}
