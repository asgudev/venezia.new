<?php


namespace DishBundle\Form\Type;


use Doctrine\ORM\EntityRepository;
use DishBundle\Entity\Composition;
use DishBundle\Entity\CompositionIngredient;
use DishBundle\Entity\Ingredient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class CompositionIngredientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ingredient', Select2EntityType::class, [
                "multiple" => false,
                "label" => "Ингредиент",
                "class" => Composition::class,
                "text_property" => "name",
                'minimum_input_length' => 2,
                'page_limit' => 10,
                'placeholder' => 'Ингредиент',
                'remote_route' => 'search_composition',
                'allow_add' => [
                    'enabled' => true,
                    'new_tag_text' => ' (НОВ)',
                    'new_tag_prefix' => '__',
                    'tag_separators' => '[]'
                ],
            ])
            ->add("grossMass", TextType::class, [
                'label' => "Масса БРУТТО",
                'required' => false,
            ])
            ->add("netMass", TextType::class, [
                'label' => "Масса НЕТТО",
                'required' => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CompositionIngredient::class,
        ));
    }
}