<?php

namespace DishBundle\Entity\Repository;

use DishBundle\Entity\DishCategory;
use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use Gedmo\Translatable\TranslatableListener;
use RestaurantBundle\Entity\Shop;

class DishCategoryRepository extends EntityRepository
{


    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findDeliveryMenuByShop(Shop $shop, $internal = false)
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            ->leftJoin("c.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH',
                $qb->expr()->eq("dm.shop", ":shop")
            )
            ->setParameters([
                'shop' => $shop,
            ])
            ->andWhere("c.orderTypes & 5")
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('d.name', 'ASC');

        if (!$internal)
            $categories->andWhere('c.showInApp = true');


//        $categories->getQuery()->getResult();


        return $categories->getQuery()->getResult();
    }


    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findYandexMenuByShop(Shop $shop, $internal = false)
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            ->leftJoin("c.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH',
                $qb->expr()->eq("dm.shop", ":shop")
            )
            ->setParameters([
                'shop' => $shop,
            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('d.name', 'ASC');

       if (!$internal)
           $categories->andWhere('c.showInApp = true');


//        $categories->getQuery()->getResult();


        return $categories->getQuery()->getResult();
    }

    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findAppMenuByShop(Shop $shop, $internal = false)
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            ->leftJoin("c.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH',
                $qb->expr()->eq("dm.shop", ":shop")
            )
            ->setParameters([
                'shop' => $shop,
            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('d.name', 'ASC');

       if (!$internal)
           $categories->andWhere('c.showInApp = true');


//        $categories->getQuery()->getResult();


        return $categories->getQuery()->getResult();
    }


    /**
     * @return DishCategory[]
     */
    public function findParentCategories()
    {
        return $this->createQueryBuilder('c')
            ->where('c.parent is null')
            ->getQuery()
            ->getResult();
    }


    public function findByShop(Shop $shop)
    {
        $categories = $this->createQueryBuilder('c')
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            ->innerJoin("c.dishes", 'd', "WITH", 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('d.dishMetas', 'dm')
            ->where('d.isDeleted = false')
            ->setParameters([
                'shop' => $shop,
            ])
            ->orderBy('c.id', "ASC")
            ->getQuery()
            ->getResult();

        return $categories;
    }

    public function findFullCategory($id, $locale = 'ru')
    {
        $category = $this->createQueryBuilder('c')
            ->addSelect('cd')
            ->addSelect('cds')
            ->where('c.id = :id')
            ->leftJoin("c.dishes", 'cd', "WITH", 'cd.isDeleted = false')
            ->leftJoin("cd.shops", 'cds')
            ->setParameters([
                'id' => $id
            ]);

        $query = $category->getQuery();

        $query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
        $query->setHint(TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(TranslatableListener::HINT_FALLBACK, 1);


        return $query->getOneOrNullResult();
    }

    public function findAll()
    {
        return $this->createQueryBuilder('c')
            ->orderBy("c.order", "ASC")
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findMenuByShop(Shop $shop, $date = 'now')
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->where('c.parent is null')
            ->andWhere("c.isTerminal = true")
            //->andWhere("cc1.id NOT IN (:ignored)")
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            //->addSelect('dc')
            ->addSelect('cc1')
            ->addSelect('cc1d')
            ->addSelect('dt')
            ->addSelect('cc1Delegates')
            ->leftJoin('c.children', 'cc1')
            ->innerJoin("cc1.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('cc1.discounts', 'cc1d', 'WITH', 'cc1d.shop = :shop AND cc1d.type = :discount_type')
            ->leftJoin('d.dishMetas', 'dm', 'WITH',
                $qb->expr()->eq("dm.shop", ":shop")
            )
            ->leftJoin('d.times', 'dt')
            ->leftJoin('cc1.delegates', 'cc1Delegates', 'WITH', 'cc1Delegates.shop = :shop')
            ->setParameters([
                'shop' => $shop,
                'discount_type' => 'auto',
            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('cc1.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('d.name', 'ASC')
//            ->addOrderBy('dm.price', "ASC")
            //->addOrderBy('dc.date', 'DESC')//->getQuery();

            ->getQuery()->getResult();

        //dump($categories); die();

        return $categories;
    }

    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findPublicMenuByShop(Shop $shop, $date = 'now')
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->where('c.parent is null')
            ->andWhere("c.isTerminal = true")
            ->andWhere("d.isInMenu = true")
            //->andWhere("cc1.id NOT IN (:ignored)")
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            //->addSelect('dc')
            ->addSelect('cc1')
            ->addSelect('cc1d')
            ->addSelect('cc1Delegates')
            ->leftJoin('c.children', 'cc1')
            ->innerJoin("cc1.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('cc1.discounts', 'cc1d', 'WITH', 'cc1d.shop = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', $qb->expr()->eq("dm.shop", ":shop"))
            ->leftJoin('cc1.delegates', 'cc1Delegates', 'WITH', 'cc1Delegates.shop = :shop')
            ->setParameters([
                'shop' => $shop,
            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('cc1.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('d.name', 'ASC')
            //->addOrderBy('dc.date', 'DESC')//->getQuery();

            ->getQuery()->getResult();

        //dump($categories); die();

        return $categories;
    }

    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findDeliveryMenu($shop)
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->where('c.parent is null')
//            ->andWhere('cc1.id IN (32,51)')
            ->andWhere("c.isTerminal = true")
            //->andWhere("cc1.id NOT IN (:ignored)")
            ->addSelect('d')
//            ->addSelect('dm')
            ->addSelect('s')
            //->addSelect('dc')
            ->addSelect('cc1')
//            ->addSelect('cc1d')
//            ->addSelect('cc1Delegates')
            ->leftJoin('c.children', 'cc1')
            ->innerJoin("cc1.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's.id = :shop')
            ->leftJoin('cc1.discounts', 'cc1d', 'WITH', 'cc1d.shop = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', $qb->expr()->eq("dm.shop", ":shop"))
            ->leftJoin('cc1.delegates', 'cc1Delegates', 'WITH', 'cc1Delegates.shop = :shop')
            ->setParameters([
                'shop' => $shop,
            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('cc1.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('dm.price', 'ASC')
            ->addOrderBy('d.name', 'ASC')
            //->addOrderBy('dc.date', 'DESC')//->getQuery();

            ->getQuery()->getResult();

        //dump($categories); die();

        return $categories;
    }

    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findTakeawayMenu($shop)
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->where('c.parent is null')
            ->andWhere('cc1.id IN (32,51)')
            ->andWhere("c.isTerminal = true")
            //->andWhere("cc1.id NOT IN (:ignored)")
            ->addSelect('d')
//            ->addSelect('dm')
            ->addSelect('s')
            //->addSelect('dc')
            ->addSelect('cc1')
//            ->addSelect('cc1d')
//            ->addSelect('cc1Delegates')
            ->leftJoin('c.children', 'cc1')
            ->innerJoin("cc1.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's.id = :shop')
            ->leftJoin('cc1.discounts', 'cc1d', 'WITH', 'cc1d.shop = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', $qb->expr()->eq("dm.shop", ":shop"))
            ->leftJoin('cc1.delegates', 'cc1Delegates', 'WITH', 'cc1Delegates.shop = :shop')
            ->setParameters([
                'shop' => $shop,
            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('cc1.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('dm.price', 'ASC')
            ->addOrderBy('d.name', 'ASC')
            //->addOrderBy('dc.date', 'DESC')//->getQuery();

            ->getQuery()->getResult();

        //dump($categories); die();

        return $categories;
    }

    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findTakeawayMenuByShop(Shop $shop = null)
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->where('c.parent is null')
            ->andWhere('cc1.id IN (32,51)')
            ->andWhere("c.isTerminal = true")
            //->andWhere("cc1.id NOT IN (:ignored)")
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            //->addSelect('dc')
            ->addSelect('cc1')
            ->addSelect('cc1d')
            ->addSelect('cc1Delegates')
            ->leftJoin('c.children', 'cc1')
            ->innerJoin("cc1.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('cc1.discounts', 'cc1d', 'WITH', 'cc1d.shop = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', $qb->expr()->eq("dm.shop", ":shop"))
            ->leftJoin('cc1.delegates', 'cc1Delegates', 'WITH', 'cc1Delegates.shop = :shop')
            ->setParameters([
                'shop' => $shop,
            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('cc1.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('d.name', 'ASC')
            //->addOrderBy('dc.date', 'DESC')//->getQuery();

            ->getQuery()->getResult();

        //dump($categories); die();

        return $categories;
    }

    /**
     * @param Shop $shop
     * @param string $date
     * @return DishCategory[]
     */
    public function findMenuByShopId(Shop $shop, $date = 'now')
    {
        $qb = $this->createQueryBuilder('c');
        $categories = $qb
            ->where('c.parent is null')
            ->andWhere("c.isTerminal = true")
            ->addSelect('d')
            ->addSelect('dm')
            ->addSelect('s')
            ->addSelect('dc')
            ->addSelect('cc1')
            ->addSelect('cc1d')
            ->addSelect('cc1Delegates')
            ->leftJoin('c.children', 'cc1')
            ->innerJoin("cc1.dishes", 'd', 'WITH', 'd.isDeleted = false')
            ->innerJoin('d.shops', 's', 'WITH', 's = :shop')
            ->leftJoin('cc1.discounts', 'cc1d', 'WITH', 'cc1d.shop = :shop')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', $qb->expr()->eq("dm.shop", ":shop"))
            ->leftJoin('d.changes', 'dc', 'WITH', $qb->expr()->andX(
                $qb->expr()->lte("dc.date", ":date"),
                $qb->expr()->orX(
                    $qb->expr()->eq("dc.shop", ":shop"),
                    $qb->expr()->isNull("dc.shop")
                )
            ))
            /*  ->leftJoin('d.changes', 'dc', 'WITH', $qb->expr()->orX(
                  $qb->expr()->eq("dc.shop", ":shop"),
                  $qb->expr()->isNull("dc.shop")
              ))*/
            ->leftJoin('cc1.delegates', 'cc1Delegates', 'WITH', 'cc1Delegates.shop = :shop')
            ->setParameters([
                'shop' => $shop,
                'date' => (new \DateTime($date)),

            ])
            ->addOrderBy('c.order', 'ASC')
            ->addOrderBy('cc1.order', 'ASC')
            ->addOrderBy('d.order', 'ASC')
            ->addOrderBy('d.name', 'ASC')
            ->addOrderBy('dc.date', 'DESC')//->getQuery();

            ->getQuery()->getResult();

        //dump($categories); die();

        return $categories;
    }
}
