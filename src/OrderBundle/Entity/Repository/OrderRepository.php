<?php

namespace OrderBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use OrderBundle\Entity\Order;
use RestaurantBundle\Entity\Client;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;
use TerminalBundle\Entity\DailyReport;
use UserBundle\Entity\User;

class OrderRepository extends EntityRepository
{

    public function findClientOrdersByDate($shop, $dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('o')
            ->addSelect('c')
            ->leftJoin('o.client', 'c')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt >= :dateStart')
            ->andWhere('o.createdAt <= :dateEnd')
            ->andWhere('o.status = 4')
            ->andWhere('o.cashType = 2')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->getQuery()
            ->getResult();
    }

    //UPDATE `orders` SET `deleted_at`=`closed_at` where `status` = 5

    /**
     * @return Order[]
     */
    public function getDeliveryOrders($shop, $dateStart)
    {
        return $this->createQueryBuilder('o')
            ->where("o.shop = :shop")
            ->andWhere('o.status = -2')
            ->andWhere('o.createdAt >= :start')
            ->setParameters([
                "shop" => $shop,
                'start' => $dateStart,
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Order[]
     */
    public function findOrdersAtTime($timeStart, $shop, $user)
    {
        $orders = $this->createQueryBuilder('o');

        $orders->addSelect('d')
            ->leftJoin("o.dishes", 'd')
//            ->andWhere($orders->expr()->between(":time", "o.createdAt", "o.closedAt"))
            ->where('o.createdAt <= :time')
            ->andWhere("o.closedAt >= :time")
            ->setParameters([
                "time" => $timeStart,
            ]);

        if ($user !== null) {
            $orders
                ->andWhere("o.user = :user")
                ->setParameter("user", $user);
        }
        if ($shop !== null) {
            $orders
                ->andWhere("o.shop = :shop")
                ->setParameter("shop", $shop);
        }


        return $orders->getQuery()->getResult();
    }


    /**
     * @return Order[]
     */
    public function findCustom()
    {
        return $this->createQueryBuilder('o')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin('od.dish', 'd')
            ->where('d.id = :id')
            ->andWhere('o.localId in (:ids)')
            ->andWhere('o.status = 4')
            ->setParameters([
                'id' => 13715,
                'ids' => [57631, 57959, 58061, 58412],
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param PromoCard $promoCard
     * @return Order[]
     */
    public function findByPromoCard(PromoCard $promoCard)
    {
        return $this->createQueryBuilder('o')
            ->where('o.promo = :promo')
            ->andWhere('o.status = 4')
            ->setParameters([
                'promo' => $promoCard,
            ])
            ->orderBy('o.createdAt', "DESC")
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $client
     * @param $dateStart
     * @param $dateEnd
     * @return Order[]
     */
    public function findDeletedOrdersByPeriod($dateStart, $dateEnd, $shop = null)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('od')
            ->addSelect('odd')
            ->addSelect('u')
            ->where('o.status = 5')
            ->andWhere('o.createdAt > :dateStart')
            ->andWhere('o.createdAt < :dateEnd')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin('od.dish', 'odd')
            ->leftJoin('o.user', 'u')
            ->setParameters([
                'dateStart' => (new \DateTime($dateStart))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime($dateEnd))->setTime(23, 59, 59),
            ]);

        if ($shop != null) {
            $orders->andWhere('o.shop = :shop')->setParameter('shop', $shop);
        }

        return $orders->getQuery()->getResult();
    }

    /**
     * @param $client
     * @param $dateStart
     * @param $dateEnd
     * @return Order[]
     */
    public function findByShopDates($shop, $dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('o')
            ->addSelect('od')
            ->addSelect('odd')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin('od.dish', 'odd')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt >= :dateStart')
            ->andWhere('o.createdAt <= :dateEnd')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd
            ])
            ->getQuery()
            ->getResult();

    }

    /**
     * @param $client
     * @param $dateStart
     * @param $dateEnd
     * @return Order[]
     */
    public function findByClientDates($client, $dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('o')
            ->addSelect('od')
            ->leftJoin('o.dishes', 'od')
            ->where('o.client = :client')
            ->andWhere('o.createdAt >= :dateStart')
            ->andWhere('o.createdAt <= :dateEnd')
            ->setParameters([
                'client' => $client,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd
            ])
            ->getQuery()
            ->getResult();

    }


    public function findSumsSeparated($shop, $dateStart, $user)
    {
        return $this->createQueryBuilder('o')
            ->select('SUM(o.clientMoneyCash) as oCash')
            ->addSelect('SUM(o.clientMoneyCard) as oCard')
            ->where('o.shop = :shop')
            ->andWhere('o.user = :user')
            ->andWhere('o.createdAt > :dateStart')
            ->andWhere('o.status = 4')
            ->setParameters([
                'shop' => $shop,
                'user' => $user,
                'dateStart' => $dateStart,
            ])
            ->getQuery()
            ->getResult();
    }


    public function findSumsByType($shop, $dateStart, $type = 0)
    {
        $sums = $this->createQueryBuilder('o')
            ->select("SUM(od.dishPrice * od.quantity) as oSum")
            ->leftJoin('o.dishes', 'od')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt > :dateStart')
            ->andWhere('o.status = 4')
            ->andWhere('o.cashType = :type')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $dateStart,
                'type' => $type,
            ]);

        return $sums->getQuery()->getSingleResult();
    }

    public function findSums($shop, $dateStart, $type = 0)
    {
        $sums = $this->createQueryBuilder('o')
            ->select('SUM(o.clientMoneyCash) as oCash')
            ->addSelect('SUM(o.clientMoneyCard) as oCard')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt > :dateStart')
            ->andWhere('o.status = 4')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $dateStart,
            ]);

        return $sums->getQuery()->getResult();
    }

    /**
     * @param $date
     * @return Order[]
     */
    public function findDeletedOrders($dateStart, $dateEnd = 'now')
    {
        $deletedOrders = $this->createQueryBuilder('o')
            ->addSelect('s')
            ->addSelect('u')
            ->addSelect('od')
            ->addSelect('d')
            ->where('o.status = 5')
            ->andWhere('o.deletedAt >= :dateStart')
            ->andWhere('o.deletedAt <= :dateEnd')
            ->leftJoin('o.shop', 's')
            ->leftJoin('o.user', 'u')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin('od.dish', 'd')
            ->setParameters([
                'dateStart' => (new \DateTime($dateStart))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime($dateEnd))->setTime(23, 59, 59),
            ]);

        return $deletedOrders
            ->getQuery()
            ->getResult();
    }


    public function findAll()
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->innerJoin('o.dishes', 'd')
            ->getQuery()
            ->getResult();
        return $orders;
    }

    public function findLastInShopWithId($shop, $localId)
    {
        $order = $this->createQueryBuilder('o')
            ->where("o.shop = :shop")
            ->andWhere("o.localId = :id")
            ->setParameters([
                'shop' => $shop,
                'id' => $localId,
            ])
            ->orderBy('o.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        return $order;
    }

    /**
     * @param Shop $shop
     * @return Order
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastInShop(Shop $shop)
    {
        $order = $this->createQueryBuilder('o')
            ->where("o.shop = :shop")
            ->andWhere("o.localId <> 0")
            ->setParameters([
                'shop' => $shop,
            ])
            ->orderBy('o.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        return $order;
    }

    public function findByUser(User $user)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->innerJoin("o.dishes", 'd')
            ->where("o.user = :user")
            ->setParameter("user", $user)
            ->getQuery()
            ->getResult();
        return $orders;
    }

    public function findCurrentByUser(User $user, Shop $shop)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->innerJoin("o.dishes", 'd')
            ->where("o.user = :user")
            ->andWhere("o.createdAt BETWEEN :day_start AND :day_end")
            ->andWhere("o.shop = :shop")
            ->andWhere("o.status <> :status")
            ->setParameters([
                "user" => $user,
                "shop" => $shop,
                'day_start' => (new \DateTime())->setTime(00, 00, 00),
                'day_end' => (new \DateTime())->setTime(23, 59, 59),
                'status' => Order::STATUS_CLOSED,
            ])
            ->getQuery()
            ->getResult();
        return $orders;
    }

    public function findShopActiveToday(Shop $shop, User $user = null, $status = Order::STATUS_WAIT_FOR_KITCHEB)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->innerJoin("o.dishes", 'd')
            //->andWhere("o.createdAt BETWEEN :day_start AND :day_end")
            ->andWhere("o.shop = :shop")
            ->andWhere("o.status <= :status")
            ->orWhere("o.isEditable = true")
            ->setParameters([
                "shop" => $shop,
                //'day_start' => (new \DateTime())->setTime(00, 00, 00),
                //'day_end' => (new \DateTime())->setTime(23, 59, 59),
                'status' => $status,
            ]);

        if ($user !== null) {
            $orders
                ->andWhere("o.user = :user")
                ->setParameter("user", $user);
        }

        return $orders->getQuery()->getResult();
    }

    /**
     * @param $shiftStart
     * @param Shop|null $shop
     * @param User|null $user
     * @param int $status
     * @return Order[]
     */
    public function findShiftActive($shiftStart, Shop $shop = null, User $user = null, $status = Order::STATUS_WAIT_FOR_KITCHEB)
    {
        $orders = $this->createQueryBuilder('o');

        $orders
            ->addSelect('d')
            ->addSelect('od')
            ->addSelect('om')
            ->addSelect('u')
            ->leftJoin("o.dishes", 'od')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('o.user', 'u')
            ->leftJoin('o.meta', 'om')
            ->andWhere("o.createdAt > :shift_start")
            ->andWhere($orders->expr()->orX(
                $orders->expr()->lte("o.status", ":status"),
                $orders->expr()->eq("o.isEditable", "1")
            ))
            ->setParameters([
                'shift_start' => $shiftStart,
                'status' => $status,
            ]);

        if ($user !== null) {
            $orders
                ->andWhere("o.user = :user")
                ->setParameter("user", $user);
        }
        if ($shop !== null) {
            $orders
                ->andWhere("o.shop = :shop")
                ->setParameter("shop", $shop);
        }


        return $orders->getQuery()->getResult();
    }


    public function findShiftClosed(Shop $shop, User $user = null, $shiftStart)
    {
        $orders = $this->createQueryBuilder('o');

        $orders
            ->addSelect('d')
            ->addSelect('od')
            ->addSelect('om')
            ->addSelect('u')
            ->leftJoin("o.dishes", 'od')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('o.user', 'u')
            ->leftJoin('o.meta', 'om')
            ->andWhere("o.shop = :shop")
            ->andWhere("o.createdAt > :shift_start")
            ->andWhere("o.status IN (:status)")
//            ->andWhere($orders->expr()->orX(
//                $orders->expr()->in("o.status", ":status"),
//                $orders->expr()->eq("o.isEditable", "1")
//            ))
            ->setParameters([
                "shop" => $shop,
                'shift_start' => $shiftStart,
                'status' => [Order::STATUS_CLOSED, Order::STATUS_DELETED,]
            ]);

        if ($user !== null) {
            $orders
                ->andWhere("o.user = :user")
                ->setParameter("user", $user);
        }
        //dump($orders->getQuery());die();

        return $orders->getQuery()->getResult();
    }


    /**
     * @param $shop
     * @param string $date
     * @return Order[]
     */
    public function findOrdersByReport(DailyReport $dailyReport)
    {
        $dishes = $this->createQueryBuilder('o')
            ->where('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('o.shop = :shop')
            ->setParameters([
                "shop" => $dailyReport->getShop(),
                'dateStart' => $dailyReport->getDateStarted(),
                'dateEnd' => $dailyReport->getDateClosed(),
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }

    public function findShopClosedToday(Shop $shop)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->innerJoin("o.dishes", 'd')
            ->andWhere("o.createdAt BETWEEN :day_start AND :day_end")
            ->andWhere("o.shop = :shop")
            ->andWhere("o.status = :status")
            ->orWhere("o.isEditable = true")
            ->setParameters([
                "shop" => $shop,
                'day_start' => (new \DateTime())->setTime(12, 00, 00),
                'day_end' => (new \DateTime())->modify('+1 day')->setTime(12, 00, 00),
                'status' => Order::STATUS_CLOSED,
            ])
            ->orderBy('o.id', 'DESC')
            ->getQuery()
            ->getResult();
        return $orders;
    }

    public function findSemiClosedOrders(DailyReport $dailyReport)
    {
        return $this->createQueryBuilder('o')
            ->where('o.status = 6')
            ->andWhere('o.shop = :shop')
            ->andWhere('o.createdAt >= :start')
            ->andWhere('o.createdAt <= :end')
            ->setParameters([
                'shop' => $dailyReport->getShop(),
                'start' => $dailyReport->getDateStarted(),
                'end' => $dailyReport->getDateClosed(),
            ])
            ->getQuery()
            ->getResult();
    }

    public function findActiveToday()
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->innerJoin("o.dishes", 'd')
            ->andWhere("o.createdAt BETWEEN :day_start AND :day_end")
            ->andWhere("o.shop = :shop")
            ->andWhere("o.status <> :status")
            ->setParameters([
                'day_start' => (new \DateTime())->setTime(00, 00, 00),
                'day_end' => (new \DateTime())->setTime(23, 59, 59),
                'status' => Order::STATUS_CLOSED,
            ])
            ->getQuery()
            ->getResult();
        return $orders;
    }

    public function findAllToday($shop = null, $date = 'today')
    {
        $orders = $this->createQueryBuilder('o');

        $orders->addSelect('d')
            ->addSelect('ou')
            ->leftJoin('o.user', 'ou')
            ->innerJoin("o.dishes", 'd')
            ->where($orders->expr()->in("o.id", $this->createQueryBuilder('o2')
                ->where("o.createdAt BETWEEN :day_start AND :day_end")
                ->getDQL())
            )
            ->setParameters([
                'day_start' => (new \DateTime($date))->setTime(00, 00, 00),
                'day_end' => (new \DateTime($date))->setTime(23, 59, 59),
            ]);

        if ($shop !== null) {
            $orders->andWhere('o.shop = :shop')
                ->setParameter('shop', $shop);
        }

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findAllFromDate($shop = null, $date)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->addSelect('ou')
            ->addSelect('dd')
            ->addSelect('ddc')
            ->leftJoin('o.user', 'ou')
            ->innerJoin("o.dishes", 'd')
            ->leftJoin('d.dish', 'dd')
            ->leftJoin('dd.category', 'ddc')
            ->andWhere("o.createdAt > :dateStart")
            ->andWhere("o.status = :status")
            ->setParameters([
                'dateStart' => $date,
                'status' => Order::STATUS_CLOSED,
            ]);

        if ($shop !== null) {
            $orders->andWhere('o.shop = :shop')
                ->setParameter('shop', $shop);
        }

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findAllByDate($shop = null, $date = 'today')
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->addSelect('ou')
            ->addSelect('dd')
            ->addSelect('ddc')
            ->leftJoin('o.user', 'ou')
            ->innerJoin("o.dishes", 'd')
            ->leftJoin('d.dish', 'dd')
            ->leftJoin('dd.category', 'ddc')
            ->andWhere("o.createdAt BETWEEN :day_start AND :day_end")
            ->andWhere("o.status = :status")
            ->setParameters([
                'day_start' => (new \DateTime($date))->setTime(0, 0, 0),
                'day_end' => (new \DateTime($date))->setTime(23, 59, 59),
                'status' => Order::STATUS_CLOSED,
            ]);

        if ($shop !== null) {
            $orders->andWhere('o.shop = :shop')
                ->setParameter('shop', $shop);
        }

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findByNumber($id, $shop)
    {
        return $this->createQueryBuilder('o')
            ->where('o.id = :id')
            ->orWhere('o.localId = :id')
            ->andWhere('o.shop = :shop')
            ->setParameters([
                'id' => $id,
                'shop' => $shop,
            ])
            ->getQuery()
            ->getResult();
    }


    public function findAllInRange($datestart, $dateend, $shop = null)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('d')
            ->innerJoin("o.dishes", 'd')
            ->andWhere("o.createdAt BETWEEN :day_start AND :day_end")
            ->setParameters([
                'day_start' => (new \DateTime($datestart))->setTime(00, 00, 00),
                'day_end' => (new \DateTime($dateend))->setTime(23, 59, 59),
            ]);
        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }
        //dump($orders); die();
        //$orders = $orders->getQuery()->getResult();
        return $orders->getQuery()->getResult();
    }


    public function findSummedByCat($shop = null, $dateStart = 'yesterday', $dateEnd = 'today')
    {
        $orders = $this->createQueryBuilder('o')
            ->select('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->addSelect('SUM( od.quantity ) AS odQuantity')
            ->addSelect('SUM( od.dishDiscount * od.quantity ) AS odDiscount')
            ->addSelect('(oddc.name) AS oddcpCat')
            //->addSelect('(oddcp.name) AS oddcpCat')
            ->addSelect('(os.name) AS oSname')
            ->addSelect('(os.id) AS oSid')
            ->addSelect('(oddc.id) AS oddcpId')
            ->addSelect('oddcp.name AS oddcpn')
            ->where('o.status <= 4')
            // ->addSelect('(oddcp.id) AS oddcpId')
            ->innerJoin('o.dishes', 'od')
            ->innerJoin("od.dish", 'odd')
            ->innerJoin("odd.category", 'oddc')
            ->leftJoin('oddc.parent', 'oddcp')
            //->innerJoin("oddc.parent", 'oddcp')
            ->innerJoin('o.shop', 'os')
            ->orderBy('os.id', 'ASC')
            ->groupBy('oddc.id')
            ->addGroupBy('oddcpn')
            ->addGroupBy('os');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }

        if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }
        /*$orders = $orders->getQuery();
        dump($orders); die();*/
        $orders = $orders->getQuery()->getResult();


        return $orders;
    }

    public function findAverageSum($shop = null)
    {
        $orders = $this->createQueryBuilder('o')
            ->select('SUM( o.clientMoneyCash + o.clientMoneyCard ) AS odSum')
            ->addSelect('YEAR(o.createdAt) AS oCreatedAtYear')
            ->addSelect('MONTH(o.createdAt) AS oCreatedAtMonth')
            ->addSelect('(os.id) AS oSid')
//            ->addSelect('COUNT(o) as oCount')
//            ->leftJoin('o.dishes', 'od')
            ->leftJoin('o.shop', 'os')
            ->where('o.status = 4')
            ->andWhere("o.createdAt >= '2021-01-01'")
            ->addGroupBy("oCreatedAtMonth")
            ->addGroupBy("oCreatedAtYear")
            ->addGroupBy('os.id ');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }
        /*if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }*/

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findOrderCount($shop = null)
    {
        $orders = $this->createQueryBuilder('o')
//            ->select('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->select('YEAR(o.createdAt) AS oCreatedAtYear')
            ->addSelect('MONTH(o.createdAt) AS oCreatedAtMonth')
            ->addSelect('(os.id) AS oSid')
            ->addSelect('COUNT(o) as oCount')
            ->leftJoin('o.shop', 'os')
            ->where('o.status = 4')
            ->andWhere("o.createdAt >= '2021-01-01'")
            ->addGroupBy("oCreatedAtMonth")
            ->addGroupBy("oCreatedAtYear")
            ->addGroupBy('os.id');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }
        /*if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }*/

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findAvgOrderTime($shop = null)
    {
        $orders = $this->createQueryBuilder('o')
//            ->select('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->select('YEAR(o.createdAt) AS oCreatedAtYear')
            ->addSelect('MONTH(o.createdAt) AS oCreatedAtMonth')
            ->addSelect('(os.id) AS oSid')
            ->addSelect('(AVG(TIME_TO_SEC(TIME_DIFF(o.closedAt, o.createdAt))) / 60) as avgTime')
            ->leftJoin('o.shop', 'os')
            ->where('o.status = 4')
            ->andWhere('o.createdAt is not null')
            ->andWhere('o.closedAt is not null')
            ->addGroupBy("oCreatedAtMonth")
            ->addGroupBy("oCreatedAtYear")
            ->addGroupBy('os.id');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }
        /*if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }*/

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findDistributedByCat($shop = null, $dateStart = 'yesterday', $dateEnd = 'today')
    {
        $orders = $this->createQueryBuilder('o')
            ->select('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->addSelect('SUM( od.dishDiscount * od.quantity ) AS odDiscount')
            ->addSelect('DATE(o.createdAt) AS oCreatedAtDate')
            ->addSelect('(oddc.name) AS oddcpCat')
            ->addSelect('(os.id) AS oSid')
            ->addSelect('(oddc.id) AS oddcpId')
            ->addSelect('oddcp.name AS oddcpn')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin("od.dish", 'odd')
            ->leftJoin("odd.category", 'oddc')
            ->leftJoin("oddc.parent", 'oddcp')
            ->leftJoin('o.shop', 'os')
            ->groupBy('oddc.id')
            ->addGroupBy("oCreatedAtDate")
            ->addGroupBy('os.id');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }
        if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findSummedByHour($shop = null, $dateStart = 'yesterday', $dateEnd = 'today')
    {
        $orders = $this->createQueryBuilder('o')
            ->select('HOUR(o.createdAt) AS oCreatedAt')
            ->addSelect('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->addSelect('SUM( od.quantity ) AS odQuantity')
            ->addSelect('SUM( od.dishDiscount * od.quantity ) AS odDiscount')
            ->addSelect('(os.name) AS oSname')
            ->addSelect('(os.id) AS oSid')
            ->where('o.status <= 4')
            ->leftJoin('o.shop', 'os')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin("od.dish", 'odd')
            ->leftJoin("odd.category", 'oddc')
            ->orderBy('os.id', 'ASC')
            ->groupBy('oCreatedAt')
            ->addGroupBy('os.id');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }
        if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }

        $orders = $orders->getQuery()->getResult();
        //dump($orders);die();

        //    ->getResult();
        return $orders;
    }

    public function findSummedByHourAndCat($shops = null, $categories = null, $dateStart = 'yesterday', $dateEnd = 'today')
    {
        $orders = $this->createQueryBuilder('o')
            ->select('HOUR(o.createdAt) AS oCreatedAt')
            ->addSelect('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->addSelect('SUM(od.quantity) AS odQuantity')
            ->addSelect('SUM( od.dishDiscount * od.quantity ) AS odDiscount')
            ->addSelect('(os.name) AS oSname')
            ->addSelect('(os.id) AS oSid')
            ->leftJoin('o.shop', 'os')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin("od.dish", 'odd')
            ->leftJoin("odd.category", 'oddc')
            ->andWhere('o.status = 4')
            ->groupBy('oCreatedAt')
            ->addGroupBy('os.id');

        if ($categories) {
            $orders->andWhere("oddc IN(:categories)")->setParameter("categories", $categories);

            $orders->andWhere('odd.id != 1177');
        }

        if ($shops) {
            $orders->andWhere("o.shop IN(:shops)")->setParameter("shops", $shops);
        }
        if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }

        $orders = $orders->getQuery()->getResult();

        //    ->getResult();
        return $orders;
    }

    public function findDistributedByHour($shop = null, $dateStart = 'yesterday', $dateEnd = 'today')
    {
        $orders = $this->createQueryBuilder('o')
            ->select('HOUR(o.createdAt) AS oCreatedAtHour')
            ->addSelect('DATE(o.createdAt) AS oCreatedAtDate')
            ->addSelect('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->addSelect('SUM( od.dishDiscount * od.quantity ) AS odDiscount')
            ->addSelect('(os.name) AS oSname')
            ->addSelect('(os.id) AS oSid')
            ->addSelect('oddcp.name AS oddcpn')
            ->innerJoin('o.shop', 'os')
            ->innerJoin('o.dishes', 'od')
            ->innerJoin("od.dish", 'odd')
            ->innerJoin("odd.category", 'oddc')
            ->innerJoin("oddc.parent", 'oddcp')
            ->groupBy('oCreatedAtHour')
            ->addGroupBy("oCreatedAtDate")
            ->addGroupBy('os.id');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }

        if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function findDistributedByHourByCategory($category = null, $dateStart = 'yesterday', $dateEnd = 'today')
    {
        $orders = $this->createQueryBuilder('o')
            ->select('HOUR(o.createdAt) AS oCreatedAtHour')
            ->addSelect('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->addSelect('SUM( od.dishDiscount * od.quantity ) AS odDiscount')
            ->addSelect('SUM( od.quantity )  AS odQuantity')
            ->addSelect('(os.name) AS oSname')
            ->addSelect('(os.id) AS oSid')
            ->innerJoin('o.shop', 'os')
            ->innerJoin('o.dishes', 'od')
            ->innerJoin("od.dish", 'odd', 'WITH', 'odd.category = :category')
            ->innerJoin("odd.category", 'oddc')
            ->innerJoin("oddc.parent", 'oddcp')
            ->setParameters([
                'category' => $category
            ])
            ->groupBy('oCreatedAtHour')
            ->addGroupBy('os.id');
        //dump($orders);die();
        /*
                if ($category) {
                    $orders->andWhere("odd.category = :category")->setParameter("category", $category);
                }*/

        if ($category->getId() == 32) {
            $orders
                ->andWhere('odd.id <> 1177')
                ->andWhere('odd.name NOT LIKE :filter')
                ->setParameter('filter', '%добавка%');
        }

        if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    /**
     * @param $orderId
     * @return Order
     */
    public function findFullOrder($orderId)
    {
        return $this->createQueryBuilder('o')
            ->where('o.id = :order')
            ->addSelect('ot')
            ->addSelect('op')
            ->addSelect('od')
            ->addSelect('odd')
            ->addSelect('oddc')
            ->addSelect('oddcp')
            ->leftJoin('o.table', 'ot')
            ->leftJoin('o.place', 'op')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin('od.dish', 'odd')
            ->leftJoin('odd.category', 'oddc')
            ->leftJoin('oddc.parent', 'oddcp')
            ->setParameters([
                'order' => $orderId
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function findWithPrinters(Order $order)
    {
        $order = $this->createQueryBuilder('o')
            ->where('o = :order')
            ->addSelect('od')
            ->addSelect('odd')
            ->addSelect('oddc')
            ->addSelect('oddcp')
            ->addSelect('oddcpd')
            ->leftJoin("o.dishes", 'od')
            ->leftJoin('od.dish', 'odd')
            ->leftJoin('odd.category', 'oddc')
            ->leftJoin('oddc.printers', 'oddcp', 'WITH', 'oddcp.shop = :shop')
            ->leftJoin('oddc.delegates', 'oddcpd', 'WITH', 'oddcpd.shop = :shop')
            ->setParameters([
                'order' => $order,
                'shop' => $order->getShop(),
            ])
            ->getQuery()->getOneOrNullResult();

        return $order;
    }


    /**
     * @param Shop $shop
     * @param $type
     * @return Order[]
     */
    public function findSumByType(Shop $shop, $cashType, $shiftStart)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('SUM(od.dishPrice * od.quantity) as oSum')
            ->where('o.cashType = :cash_type')
            ->andWhere('o.shop = :shop')
            ->andWhere('o.status = 4')
            ->andWhere('o.localId > 0')
            ->andWhere("o.createdAt > :shift_start")
            ->leftJoin('o.dishes', 'od')
            ->setParameters([
                'shop' => $shop,
                'cash_type' => $cashType,
                'shift_start' => $shiftStart,
            ]);


        $orders = $orders->getQuery()->getSingleResult();
        //dump($orders);die();
        return $orders;
    }


    /**
     * @param DailyReport $report
     * @return Order[]
     */
    public function findByShift(DailyReport $report)
    {
        return $this->createQueryBuilder('o')
            ->addSelect('od')
            ->where('o.createdAt > :dateStart')
            ->andWhere('o.createdAt < :dateEnd')
            ->andWhere('o.shop = :shop')
            ->leftJoin('o.dishes', 'od')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DailyReport $report
     * @return Order
     */
    public function findFirstOrderInReport(DailyReport $report)
    {
        return $this->createQueryBuilder('o')
            ->where('o.createdAt > :dateStart')
            ->andWhere('o.createdAt < :dateEnd')
            ->andWhere('o.shop = :shop')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->setMaxResults(1)
            ->orderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function findByClient(Client $client)
    {
        return $this->createQueryBuilder('o')
            ->where('o.client = :client')
            ->setParameters([
                'client' => $client,
            ])
            ->orderBy('o.createdAt', 'DESC')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult();
    }

    public function findDataForDashboard($deep)
    {
        $today = new \DateTime('today');
        $start = (new \DateTime('today'))->modify("-$deep days");

        $orders = $this->createQueryBuilder('o')
            ->select('SUM(od.dishPrice * od.quantity) AS orderSum')
            ->addSelect('DATE(o.createdAt) AS orderCreatedAtDate')
            ->leftJoin('o.dishes', 'od')
            ->where('o.createdAt BETWEEN :start_date AND :end_date')
            ->setParameters([
                'start_date' => $start,
                'end_date' => $today
            ])
            ->groupBy('orderCreatedAtDate')
            ->getQuery()
            ->getResult();

        return $orders;
    }

    public function findNonClosed(DailyReport $report)
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o) as orders')
            ->where('o.status < 4')
            ->andWhere('o.createdAt > :end')
            ->andWhere('o.shop = :shop')
            ->setParameters([
                'end' => $report->getDateClosed(),
                'shop' => $report->getShop(),
            ])
            ->getQuery()
            ->getSingleResult();
    }

    public function findShopDataForDashboard(\DateTime $dateTime)
    {

        $q = $this->createQueryBuilder('o');
        $q->addSelect('SUM(od.dishPrice * od.quantity) AS orderSum')
            ->addSelect('od')
            ->addSelect('os.id as osId')
            ->leftJoin('o.dishes', 'od')
            ->leftJoin('o.shop', 'os')
/*            ->where($q->expr()->in(
                'o.id', $this->createQueryBuilder('o_s')
                ->select('o_s.id')
                ->where('o_s.createdAt >= :dateStart')
                ->andWhere('o_s.status < 5')
                ->getDQL()
            ))*/
            ->where("o.createdAt >= :dateStart")
            ->andWhere("o.status < 5")
            ->setParameters([
                'dateStart' => $dateTime,
            ])
            ->groupBy('os.id')
            ->orderBy('os.id');
        return $q->getQuery()->getResult();
    }

    /**
     * @param Shop $shop
     * @param $type
     * @return Order[]
     */
    public function findSumOfSeparated(Shop $shop, $cashType, $shiftStart, User $user)
    {
        $orders = $this->createQueryBuilder('o')
            ->addSelect('SUM(od.dishPrice * od.quantity) as oSum')
            ->where('o.cashType = :cash_type')
            ->andWhere('o.shop = :shop')
            ->andWhere('o.status = 4')
            ->andWhere('o.localId > 0')
            ->andWhere("o.createdAt > :shift_start")
            ->andWhere("o.user = :user")
            ->leftJoin('o.dishes', 'od')
            ->setParameters([
                'shop' => $shop,
                'cash_type' => $cashType,
                'shift_start' => $shiftStart,
                'user' => $user
            ]);


        $orders = $orders->getQuery()->getSingleResult();
        //dump($orders);die();
        return $orders;
    }


    public function findDistributedByMonth($year, $shop = null)
    {
        $orders = $this->createQueryBuilder('o')
            ->select('MONTH(o.createdAt) AS oCreatedAtMonth')
            ->addSelect('YEAR(o.createdAt) AS oCreatedAtYear')
            ->addSelect('SUM( od.dishPrice * od.quantity ) AS odSum')
            ->addSelect('SUM( od.dishDiscount * od.quantity ) AS odDiscount')
            ->addSelect('(os.name) AS oSname')
            ->addSelect('(os.id) AS oSid')
            ->where('o.status = 4')
            ->andWhere("YEAR(o.createdAt) >= :year")
            ->setParameters([
                'year' => $year,
            ])
            ->leftJoin('o.shop', 'os')
            ->leftJoin('o.dishes', 'od')
//            ->innerJoin("od.dish", 'odd')
            ->groupBy('oCreatedAtMonth')
            ->addGroupBy('oCreatedAtYear')
            ->addGroupBy('os.id');

        if ($shop) {
            $orders->andWhere("o.shop = :shop")->setParameter("shop", $shop);
        }

        /*
        if ($dateStart) {
            $orders->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $orders->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", new \DateTime($dateEnd));
        }
        */

        $orders = $orders->getQuery()->getResult();
        return $orders;
    }

    public function getPromoSum(PromoCard $card)
    {
        return $this->createQueryBuilder('o')
            ->select("SUM(o.clientMoneyCash + o.clientMoneyCard) as promoSum")
            ->where('o.promo = :promo')
            ->andWhere('o.status = 4')
            ->andWhere('o.cashType = 0')
            ->setParameters([
                'promo' => $card->getId(),
            ])
            ->getQuery()
            ->getSingleResult();
    }

    public function findLastOrderNumbers()
    {
        return $this->createQueryBuilder('o')
            ->select("o.localId")->distinct()
            ->where('o.createdAt >= :date')
            ->setParameters([
                'date' => (new \DateTime())->modify("-2 weeks"),
            ])
            ->getQuery()
            ->getScalarResult();

    }

}
