<?php


namespace RestaurantBundle\Manager;


use DishBundle\Entity\Dish;
use Doctrine\ORM\EntityManager;
use OrderBundle\Entity\OrderDish;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TerminalBundle\Entity\DailyReport;

class WarehouseManager
{

    private $em;
    private $state;
    private $request;
    private $redis;

    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->container = $container;
        $this->redis = $this->container->get("redis.helper");

        header("content-type: text/html; charset=windows-1251");

        $this->em = $em;
        $this->state = [];
        $this->request = [];
    }

    public function getRequestData() {
        return json_decode($this->redis->get("ingredient_request"), true);
    }

    public function saveRequestData($data) {
        $this->redis->set("ingredient_request", json_encode($data, JSON_UNESCAPED_UNICODE), 72000);

        return json_decode($this->redis->get("ingredient_request"), true);
    }

    public function updateRequestTable($data)
    {

        $ingrRequest = $this->getRequestData();

        if (!$ingrRequest)
            $ingrRequest = [];


        foreach ($data as $ingrId => $__data) {
            foreach ($__data as $shop => $quantity)
                $ingrRequest[$ingrId][$shop] = $quantity;
        }

        $this->saveRequestData($ingrRequest);
//        $this->container->get("redis.helper")->set("ingredient_request", json_encode($ingrRequest, JSON_UNESCAPED_UNICODE), 72000);

        return $ingrRequest;

    }


    public function calculateWarehouse($shops)
    {
        $ingredients = $this->em->getRepository('DishBundle:Ingredient')->findForWarehouse();
        $requestCorrection = json_decode($this->container->get("redis.helper")->get("ingredient_request"), true);
        $ingrTransferLog = $this->em->getRepository('DishBundle:IngredientTransfer')->findLastTransfersList((new \DateTime())->modify('-52 days'));
        $ingredientsInWarehouse = $this->em->getRepository('DishBundle:IngredientInWarehouse')->findBy([
            'warehouse' => [1, 20],
        ]);

        $ingrCategory = [];
        $ingrCategoryNames = [];


        foreach ($ingredients as $ingr) {
            if ($ingr->getCategory() == null) {
                //var_dump($ingr->getId());die();
            }

            $ingrCategory[$ingr->getId()] = $ingr->getCategory()->getId();
            $ingrCategoryNames[$ingr->getCategory()->getId()] = $ingr->getCategory()->getName();
        }

        $ingrToComposition = [];

        $conn = $this->em->getConnection();
        $stmt = $conn->prepare("select t1.id as ingr, t2.id as comp from ingredients t1 inner join composition t2 on t2.name = t1.name");
        $stmt->execute();
        $res = $stmt->fetchAll();
        foreach ($res as $__corr) {
            $ingrToComposition[$__corr['comp']] = $__corr['ingr'];
        }
//        dump($ingrToComposition);

        $reports = [];
        $data = [];
        $reportsRange = [];
        foreach ($shops as $__shop) {
//dump($__shop);
            $_r = $this->em->getRepository('TerminalBundle:DailyReport')->findLastReportGreaterThenDate($__shop, (new \DateTime())->modify('-9 days'));
            $reports[$__shop->getId()] = end($_r);
            $reportsRange[$__shop->getId()] = $_r;
        }


//dump($reports);die();

        $practicalConsumption = [];
        /** @var DailyReport $__report */

        foreach ($reports as $__report) {
            if (!$__report) continue;

            $__shopId = $__report->getShop()->getId();
            if (!$__report) continue;
            if (!array_key_exists($__shopId, $practicalConsumption))
                $practicalConsumption[$__shopId] = [];

            $orderDishes = $this->em->getRepository(OrderDish::class)->findDishesByReport($__report);
            $deleagtedDishes = $this->em->getRepository(OrderDish::class)->findDelegatedDishesByPeriodToShop($__report->getShop(), $__report->getDateStarted(), $__report->getDateClosed());

            $dishes = array_merge($orderDishes, $deleagtedDishes);
            $dishData = [];
            foreach ($dishes as $orderDish) {
                if (!array_key_exists($orderDish->getDish()->getId(), $dishData))
                    $dishData[$orderDish->getDish()->getId()] = [
                        "quantity" => 0,
                    ];

                $dishData[$orderDish->getDish()->getId()]["quantity"] += $orderDish->getQuantity();
            }

            $dishes = $this->em->getRepository(Dish::class)->findWithCompositionsByIds(array_keys($dishData));

            foreach ($dishes as $__dish) {
                if ($__dish->getComposition() != null) {
                    foreach ($__dish->getComposition()->getIngredients() as $ingredient) {

                        $__ingrId = $ingredient->getIngredient()->getId();
                        if (!array_key_exists($__ingrId, $ingrToComposition)) continue;

                        if (!array_key_exists($ingrToComposition[$__ingrId], $practicalConsumption[$__shopId]))
                            $practicalConsumption[$__shopId][$ingrToComposition[$__ingrId]] = ["quantity" => 0];

                        $practicalConsumption[$__shopId][$ingrToComposition[$__ingrId]]["quantity"] += $dishData[$__dish->getId()]["quantity"] * intval($ingredient->getGrossMass());
                    }
                }
            }
        }


        $sum = [
            '__warehouse' => 0,
        ];

        $consumptionSum = [
            "last" => [],
            "all" => [],
        ];

        //TODO: OPTIMIZE FOREACH HELL!!!!
        foreach ($ingredients as $__ingr) {

            $ingrName = mb_strtolower($__ingr->getId());
            $consumptionSum["last"][$ingrName] = [];
            $consumptionSum["all"][$ingrName] = [];

            $data[$ingrName] = [
                '__warehouse' => [
                    'balance' => 0,
                    'balanceItem' => 0,
                    'requirement' => 0,
                    'consumption' => 0,
                ]
            ];
            $ingrReq = 0;
            $ingrConsumption = 0;

            foreach ($shops as $__shop) {
                /** @var DailyReport $report * */
                $report = $reports[$__shop->getId()];
                if (!$report) continue;

                $data[$ingrName][$__shop->getId()] = [
                    'income' => 0,
                    'waste' => 0,
                    'balance' => 0,
                    'balance_now' => 0,
                    'balance_date' => false,
                    'consumption' => 0,
                    'consumption_last' => 0,
                    'income_live' => 0,
                    'restInDays' => 0,
                    'requirement' => 0,
                    'log' => '',
                ];

                foreach ($report->getIngredients() as $ingr) {
                    if (mb_strtolower($ingr->getIngredient()->getId()) == mb_strtolower($__ingr->getId())) {
                        $income = 0;
                        $consumption = 0;
                        $log = [];
                        $last = false;
                        $previousBalance = false;
                        $i = 0;
                        /** @var DailyReport $__rReport */
                        foreach ($reportsRange[$__shop->getId()] as $__rReport) {
                            foreach ($__rReport->getIngredients() as $__rIngr) {

                                if ($__rIngr->getIngredient()->getId() == $ingr->getIngredient()->getId()) {
                                    $log[] = $__rReport->getName() . " " . number_format($__rIngr->getIncome(), 3) . " " . number_format($__rIngr->getWaste(), 3) . " " . number_format($__rIngr->getBalance(), 3);

                                    if ($previousBalance !== false) {
                                        $dailyConsumption = $previousBalance + $__rIngr->getIncome() - $__rIngr->getWaste() - $__rIngr->getBalance();
                                    } else {
                                        $dailyConsumption = 0;
                                    }


                                    if ($dailyConsumption > 0) {
                                        $consumption += $dailyConsumption;
                                        $i++;


                                    }
                                    $last = $dailyConsumption;
                                    $previousBalance = $__rIngr->getBalance();
                                    $latestData = $__rIngr;
                                }
                            }
                        }
                        if ($i == 0) {
                            $consumption = 0;
                        } else {
                            $consumption /= $i;
                        }

                        $consumptionSum['all'][$ingrName][] = $consumption;

                        $requirement = 0;


                        $incomeLive = 0;
                        foreach ($ingrTransferLog as $transfer) {
                            if ($transfer->getTo() == null || ($transfer->getTo()->getShop() == null)) {
                                continue;
                            }

                            $__ingrTransfer = mb_strtolower($transfer->getIngredient()->getId());

                            $__shopToTransfer = $transfer->getTo()->getShop()->getId();

                            if ($__ingrTransfer == $ingrName && $__shopToTransfer == $__shop->getId() && $transfer->getDate() > $latestData->getReport()->getDateClosed()) {
                                $incomeLive += $transfer->getQuantity();
                            }

                            if ($transfer->getFrom() == null || $transfer->getFrom()->getShop() == null) {
                                if ($transfer->getFrom() == null) {
                                    //var_dump($transfer->getId());
                                }
                                continue;
                            }
                            $__shopFromTransfer = $transfer->getFrom()->getShop()->getId();
                            if ($__ingrTransfer == $ingrName && $__shopFromTransfer == $__shop->getId() && $transfer->getDate() > $latestData->getReport()->getDateClosed()) {
                                $incomeLive -= $transfer->getQuantity();
                            }
                        }


                        $__balance = $latestData->getBalance() + $incomeLive;
                        if ($ingrName == "шарик большой") {
                            $requirement = $this->ceiling(round($consumption * 1.8 - ($__balance)), 15);
                            $restInDays = 0;

                        } else if (preg_match("#мороженое#", $ingrName)) {
                            if ($__balance <= 2.5) $requirement += 1;
                            if ($consumption >= 3) $requirement += 1;
                            $restInDays = $consumption != 0 ? $__balance / $consumption : 0;
                            if (($restInDays < 3) && ($requirement == 0)) $requirement = 1;
                            if ($restInDays >= 3) $requirement = 0;
                            if ($consumption == 0) $requirement = 0;

                            if (!array_key_exists($__shop->getId(), $sum))
                                $sum[$__shop->getId()] = 0;

                            $sum[$__shop->getId()] += $__balance;

                        } else {
//                            $req = round($consumption * 1 - ($__balance));
                            $requirement = $this->ceiling($consumption * 1.5 - ($__balance), 0.1);

                            $restInDays = 0;
                        }


                        $consumptionSum['last'][$ingrName][] = $last;

                        $data[$ingrName][$__shop->getId()] = [
                            'income' => $latestData->getIncome(),
                            'waste' => $latestData->getWaste(),
                            'balance' => $latestData->getBalance(),
                            'balance_now' => $__balance,
                            'balance_date' => $latestData->getReport()->getDateClosed(),
                            'consumption_sum' => $consumptionSum,
                            'consumption_last' => $last,
                            'income_live' => $incomeLive,
                            'consumption' => $consumption,
                            'requirement' => $requirement,
                            'restInDays' => $restInDays,
                            'log' => implode("<br>", array_reverse($log)),
                        ];

                        $ingrReq += $requirement;
                        $ingrConsumption += $consumption;
                    }
                }
            }


            foreach ($ingredientsInWarehouse as $__ingrInWarehouse) {
                if (mb_strtolower($__ingrInWarehouse->getIngredient()->getId()) == mb_strtolower($__ingr->getId())) {
                    if ($__ingr->getId() == 1) {
                        $data[$ingrName]['__warehouse']["warehouseId"] = $__ingrInWarehouse->getWarehouse()->getId();
                        $data[$ingrName]['__warehouse']['requirement'] = $ingrReq;
                        $itemBalance = ceil($__ingrInWarehouse->getQuantity() / 0.21);
                        $data[$ingrName]['__warehouse']['balance'] = $__ingrInWarehouse->getQuantity();
                        $data[$ingrName]['__warehouse']['balanceItem'] = $itemBalance;

                    } else {
                        $itemBalance = ceil($__ingrInWarehouse->getQuantity() / 3.5);
                        $data[$ingrName]['__warehouse'] = [
                            'warehouseId' => $__ingrInWarehouse->getWarehouse()->getId(),
                            'balance' => $__ingrInWarehouse->getQuantity(),
                            'balanceItem' => $itemBalance,
                            'requirement' => 0,
                        ];
                        if (($itemBalance - $ingrReq) > 3) {
                            $data[$ingrName]['__warehouse']['requirement'] = 0;
                        }
                        if (($itemBalance - $ingrReq) <= 3) {
                            $data[$ingrName]['__warehouse']['requirement'] = 3;
                        }
                        if (($itemBalance - $ingrReq) <= 0) {
                            $data[$ingrName]['__warehouse']['requirement'] = 5;
                        }
                        $sum["__warehouse"] += $__ingrInWarehouse->getQuantity();
                    }
                    if ($ingrConsumption == 0) {
                        $data[$ingrName]['__warehouse']['requirement'] = 0;
                    }
                    break;
                }
            }
        }

        $request = [];


        foreach ($data as $ingr => $__ingrData) {
            foreach ($__ingrData as $shop => $__data) {
                $__shop = $shop == "__warehouse" ? "Производство" : $shop;
                // $__ingr = preg_replace("#, кг|, шт#", "", $ingr);
                $__ingr = $ingr;
                $request[$__shop][$__ingr] = (
                (
                    $requestCorrection != null &&
                    array_key_exists($ingr, $requestCorrection) &&
                    array_key_exists($shop, $requestCorrection[$ingr])
                ) ? $requestCorrection[$ingr][$shop] : $__data['requirement']);
            }
        }

        $categorizedRequest = [];

        foreach ($request as $shop => $__request) {
            foreach ($__request as $ingr => $quantity) {
                $categorizedRequest[$shop][$ingrCategory[$ingr]][$ingr] = $quantity;
            }
        }

        foreach ($categorizedRequest as $shop => $__requestCategories) {
            foreach ($__requestCategories as $__category => $__categoryData) {
                foreach ($__categoryData as $__ingr => $quantity) {
                    if ($quantity == 0) {
                        unset($categorizedRequest[$shop][$__category][$__ingr]);
                    }
                }
                if (array_sum($__categoryData) == 0) {
                    unset($categorizedRequest[$shop][$__category]);
                }
            }
            if (count($categorizedRequest[$shop]) == 0) {
                unset($categorizedRequest[$shop]);
            }
        }

        foreach ($consumptionSum["last"] as $__ingr => $__data) {
            $consumptionSum["last"][$__ingr] = array_sum($__data);
        }

        foreach ($consumptionSum["all"] as $__ingr => $__data) {
            $consumptionSum["all"][$__ingr] = array_sum($__data);
        }


        foreach ($request as $shop => $__data) {
            if (array_sum($__data) <= 0) {
                unset($request[$shop]);
            }
        }


        return [
            "practicalConsumption" => $practicalConsumption,
            "data" => $data,
            "sum" => $sum,
            "consumption_sum" => $consumptionSum,
            "ingrCategories" => array_unique(array_values($ingrCategory)),
            "ingrCategoriesNames" => $ingrCategoryNames,
            "request" => $categorizedRequest,
            "requestCorrection" => $requestCorrection,
            "transferLog" => $ingrTransferLog,

        ];
    }


    private function ceiling($number, $significance = 1)
    {
        return (is_numeric($number) && is_numeric($significance)) ? (ceil($number / $significance) * $significance) : false;
    }
}
