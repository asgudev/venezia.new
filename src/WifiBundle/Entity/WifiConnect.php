<?php

namespace WifiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Wifi Router
 *
 * @ORM\Table(name="wifi_connect")
 * @ORM\Entity(repositoryClass="WifiBundle\Entity\Repository\WifiConnectRepository")
 */
class WifiConnect
{
    const CODE_LENGTH = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="WifiBundle\Entity\WifiRouter", inversedBy="connects")
     */
    private $router;

    /**
     * @ORM\ManyToOne(targetEntity="WifiBundle\Entity\WifiClient", inversedBy="connects")
     */
    private $client;

    /**
     * @ORM\Column(name="token", type="text", nullable=true)
     */
    private $token;


    /**
     * @ORM\Column(name="connected_at", type="datetime")
     */
    private $connectedAt;

    /**
     * @ORM\ManyToOne(targetEntity="WifiBundle\Entity\WifiPromo", inversedBy="connects")
     */
    private $promoSent;


    public function __construct(WifiClient $wifiClient, WifiRouter $wifiRouter)
    {
        $this->client = $wifiClient;
        $this->router = $wifiRouter;
        $this->connectedAt = new \DateTime();
        $this->token = md5(uniqid('rid', true));
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return WifiConnect
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set connectedAt
     *
     * @param \DateTime $connectedAt
     *
     * @return WifiConnect
     */
    public function setConnectedAt($connectedAt)
    {
        $this->connectedAt = $connectedAt;

        return $this;
    }

    /**
     * Get connectedAt
     *
     * @return \DateTime
     */
    public function getConnectedAt()
    {
        return $this->connectedAt;
    }

    /**
     * Set router
     *
     * @param \WifiBundle\Entity\WifiRouter $router
     *
     * @return WifiConnect
     */
    public function setRouter(\WifiBundle\Entity\WifiRouter $router = null)
    {
        $this->router = $router;

        return $this;
    }

    /**
     * Get router
     *
     * @return \WifiBundle\Entity\WifiRouter
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * Set client
     *
     * @param \WifiBundle\Entity\WifiClient $client
     *
     * @return WifiConnect
     */
    public function setClient(\WifiBundle\Entity\WifiClient $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \WifiBundle\Entity\WifiClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set promoSent
     *
     * @param \WifiBundle\Entity\WifiPromo $promoSent
     *
     * @return WifiConnect
     */
    public function setPromoSent(\WifiBundle\Entity\WifiPromo $promoSent = null)
    {
        $this->promoSent = $promoSent;

        return $this;
    }

    /**
     * Get promoSent
     *
     * @return \WifiBundle\Entity\WifiPromo
     */
    public function getPromoSent()
    {
        return $this->promoSent;
    }
}
