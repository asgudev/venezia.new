<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use OrderBundle\Entity\OrderDish;

class ExportReportCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('restaurant:report:export')
            ->setDescription('Export data by report')
            ->addOption('report', '-r', InputOption::VALUE_REQUIRED, 'Export report');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $spool_path = $this->getContainer()->get('kernel')->getRootDir() . '/../var/export/';

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $renderer = $this->getContainer()->get('templating');


        try {
            $report = $doctrine->getRepository('TerminalBundle:DailyReport')->find($input->getOption('report'));
            $shop = $report->getShop();
            $orders = $doctrine->getRepository('OrderBundle:Order')->findOrdersByReport($report);
            $dishes = $doctrine->getRepository("OrderBundle:OrderDish")->findDishesByReport($report);
            $firstOrder = $doctrine->getRepository('OrderBundle:Order')->findFirstOrderInReport($report);

            if ($firstOrder !== null) {
                $date = $firstOrder->getCreatedAt();
            } else {
                $date = new \DateTime('01-01-1970 00:00');
            }

            if ($firstOrder) {
                $report->setName($firstOrder->getCreatedAt()->format('d.m.Y'));
            } else {
                $report->setName((new \DateTime())->format('d.m.Y'));
            }

            $total = 0;
            $totalA = [
                'terminal' => 0,
                'cash' => 0,
                'КУХНЯ' => 0,
                'БАР' => 0,
            ];

            /**
             * @var OrderDish[] $dishes
             */
            foreach ($dishes as $orderDish) {
                if (($orderDish->getOrder()->getClient() != null) && ($orderDish->getOrder()->getClient()->getId() == 4)) continue;
                $dish = $orderDish->getDish();
                $rcat = $dish->getCategory()->getParent()->getName();

                $totalA[$rcat] += $orderDish->getCashSum();
//                dump($totalA);
//                die();

                $total += $orderDish->getCashSum();
            }

            foreach ($orders as $__order) {
                $totalA['cash'] += $__order->getClientMoneyCash();
                $totalA['terminal'] += $__order->getClientMoneyCard();
            }

            $xml = $renderer->render("@Restaurant/Custom/export.xml.twig", [
                'shop' => $shop,
                'dishes' => $dishes,
                'total' => $total,
                'totalA' => $totalA,
                'date' => $date,
            ]);
//var_dump($xml);die();
            $dishDataFilename = "R" . $date->format("dmY") . "_" . $shop->getExportAlias() . '_' . time() . ".xml";

            $dishDataFile = fopen($spool_path . $dishDataFilename, "w");
            fputs($dishDataFile, $xml);
            fclose($dishDataFile);

            if ($this->getContainer()->getParameter("kernel.environment") == 'prod') {
                //var_dump('curl -T ' . $spool_path . $dishDataFilename . ' ftp://178.172.245.52:2222/' . $dishDataFilename . ' --user paulig:pakiller');
                exec('curl -T ' . $spool_path . $dishDataFilename . ' ftp://178.172.245.52:2222/' . $dishDataFilename . ' --user paulig:pakiller');
            }

            $delegated = $doctrine->getRepository("OrderBundle:OrderDish")->findRequestedDishesByReport($report);
            $exportDelegated = [];


            foreach ($delegated as $orderDish) {
                if ($report->getShop()->getId() == 14) {
                    if ($orderDish->getDelegate()->getId() == $this->getContainer()->getParameter("shop.id"))
                        $exportDelegated[$this->getContainer()->getParameter("shop.id")][] = $orderDish;

                } else {
                    if (!array_key_exists($orderDish->getDelegate()->getId(), $exportDelegated))
                        $exportDelegated[$orderDish->getDelegate()->getId()] = [];


                    $exportDelegated[$orderDish->getDelegate()->getId()][] = $orderDish;
                }
            }



            foreach ($exportDelegated as $shop_id => $orderDishData) {
                $shop = $doctrine->getRepository('RestaurantBundle:Shop')->find($shop_id);

                $total = 0;
                foreach ($orderDishData as $orderDish) {
                    $total += $orderDish->getCashSum();
                }

                $xml = $renderer->render("@Restaurant/Custom/delegate.xml.twig", [
                    'shop' => $shop,
                    'delegateTo' => $report->getShop(),
                    'dishes' => $orderDishData,
                    'total' => $total,
                    'date' => $date,
                ]);

                $delegateDishDataFilename = "R" . $date->format("dmY") . "_" . $shop->getExportAlias() . "_" . $report->getShop()->getExportAlias() . '_' . time() . ".xml";

                $delegateDishDataFile = fopen($spool_path . $delegateDishDataFilename, "w");
                fputs($delegateDishDataFile, $xml);
                fclose($delegateDishDataFile);

                if ($this->getContainer()->getParameter("kernel.environment") == 'prod') {
                    //var_dump('curl -T ' . $spool_path . $delegateDishDataFilename . ' ftp://178.172.245.52:2222/' . $delegateDishDataFilename . ' --user paulig:pakiller');
                    exec('curl -T ' . $spool_path . $delegateDishDataFilename . ' ftp://178.172.245.52:2222/' . $delegateDishDataFilename . ' --user paulig:pakiller');
                }
            }

            $em->flush();

            $output->write('success');

        } catch (\Exception $em) {
            dump($em);
            $output->write('failed');
        }
    }
}
