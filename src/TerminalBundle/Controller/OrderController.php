<?php

namespace TerminalBundle\Controller;

use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use RestaurantBundle\Entity\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TerminalBundle\Events\OrderEvent;
use TerminalBundle\Form\Type\OrderType;
use TerminalBundle\Service\Fiscal;

/**
 * Class OrderController
 * @package TerminalBundle\Controller
 * @Route("/terminal")
 */
class OrderController extends Controller
{
    const API_PATH = 'https://admin.venezia.by';

    /**
     * @Route("/order/create", name="order_create")
     */
    public function createOrderAction(Request $request)
    {
        $shop = $this->getUser()->getShop();
        $request->getSession()->migrate();

        $em = $this->getDoctrine()->getManager();

        $order = new Order($this->getUser(), $shop);

        $form = $this->createForm(OrderType::class, $order, [
            'shop' => $this->getUser()->getShop(),
            'action' => $this->generateUrl("order_create"),
        ]);


        $data = json_decode($request->getContent(), true);
        $metaData = $data["meta"];
        unset($data["meta"]);

        //$form->submit($data);


        if ($request->isMethod("POST")) {

            $order = $this->get("order.manager")->handleOrder($order, json_decode($request->getContent(), true), $this->getUser());

            if ($request->isXmlHttpRequest()) {

                if ($order->getTable()->isToGo()) {
                    $newOrder = new Order($this->getUser(), $shop);
                    $newOrderForm = $form = $this->createForm(OrderType::class, $newOrder, [
                        'shop' => $this->getUser()->getShop(),
                        'action' => $this->generateUrl("order_create"),
                    ]);
                    $orderForm = $form = $this->createForm(OrderType::class, $order, [
                        'shop' => $this->getUser()->getShop(),
                        'attr' => [
                            'id' => 'order_form',
                        ],
                        'action' => $this->generateUrl("order_edit", [
                            'id' => $order->getId(),

                        ]),
                    ]);
                    return new JsonResponse([
                        'status' => true,
                        'command' => '$("#custom_order").replaceWith(response.editForm);newOrder(response.createForm);$("#new_order").hide()',
                        'table' => [
                            'tableId' => $order->getTable()->getId(),
                            'orderId' => $order->getId(),
                            'orderIdLocal' => $order->getLocalId(),
                            'status' => $order->getStatus(),
                            'waiter' => $order->getUser()->getName(),
                        ],
                        //here
                        'editForm' => $this->renderTemplate('@Terminal/Order/orderEdit.html.twig', [
                            'form' => $orderForm->createView(),
                            'order' => $order,
                            'shop' => $shop,
                            //'promos' => ($this->getPromosArray($this->getUser()->getShop())),
                        ]),
                        'createForm' => $this->renderTemplate('@Terminal/Order/orderCreate.html.twig', [
                            'form' => $newOrderForm->createView(),
                            'order' => $newOrder,
                            'shop' => $shop,
                        ])

                    ]);
                } else {
                    $newOrder = new Order($this->getUser(), $shop);
                    $newOrderForm = $form = $this->createForm(OrderType::class, $newOrder, [
                        'shop' => $this->getUser()->getShop(),
                        'action' => $this->generateUrl("order_create"),
                    ]);
                    return new JsonResponse([
                        'status' => true,
                        //'command' => "location.href = '/login'",
                        'command' => 'newOrder(response.data); $(".workspace").hide();$("#table_control").show();',
                        'table' => [
                            'tableId' => $order->getTable()->getId(),
                            'orderId' => $order->getId(),
                            'orderIdLocal' => $order->getLocalId(),
                            'status' => $order->getStatus(),
                            'waiter' => $order->getUser()->getName(),
                        ],
                        'data' => $this->renderTemplate('@Terminal/Order/orderCreate.html.twig', [
                            'form' => $newOrderForm->createView(),
                            'shop' => $shop,
                        ])
                    ]);
                }
            } else {
                return $this->redirectToRoute('terminal_index');
            }
        } else {
            dump($form->getErrors());
//            die();
        }

        return $this->render('@Terminal/Order/orderEdit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function renderTemplate($template, $params = array())
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->get('twig');
        /** @var \Twig_Template $template */
        $template = $twig->loadTemplate($template);

        return $template->render($twig->mergeGlobals($params));
    }

    /**
     * @Route("/order/{id}/edit", name="order_edit")
     */
    public function editOrderAction(Request $request, $id)
    {
        $order = $this->getDoctrine()->getRepository("OrderBundle:Order")->findFullOrder((int)$id);
        if (!$order) {
            die("123");
        }

        $form = $this->createForm(OrderType::class, $order, [
            'action' => $this->generateUrl('order_edit', ['id' => $order->getId()], true),
            'shop' => $this->getUser()->getShop(),
            'attr' => [
                'id' => 'order_form',
            ],
        ]);


        if ($request->isMethod("POST")) {
            $order = $this->get("order.manager")->handleOrder($order, json_decode($request->getContent(), true), $this->getUser());

            return new JsonResponse([
                'status' => true,
                'table' => [
                    'tableId' => $order->getTable()->getId(),
                    'orderId' => $order->getId(),
                    'orderIdLocal' => $order->getLocalId(),
                    'status' => $order->getStatus(),
                    'waiter' => $order->getUser()->getName(),
                ],
                'command' => 'closeOrder();$(".workspace").hide();$("#table_control").show();$("#new_order").show();closeOrder();',
            ]);
        }

        $categories = [];
        foreach ($order->getDishes() as $orderDish) {
            $category = $orderDish->getDish()->getCategory()->getParent();
            if (!in_array($category, $categories)) $categories[] = $category;
        }

        if (($order->getPromo() != null) && ($order->getPromo()->getClient() != null) && ($order->getPromo()->getClient()->getBirthDate()->format("m-d") == (new \DateTime())->format("m-d"))) {
            $order->getPromo()->setCardDiscount($order->getPromo()->getCardDiscount() + 10);
        }


        return $this->render('@Terminal/Order/orderEdit.html.twig', [
            'order' => $order,
            'categories' => $categories,
            'form' => $form->createView(),
            //'promos' => ($this->getPromosArray($this->getUser()->getShop())),
        ]);
    }

    /**
     * @Route("/order/{id}/close", name="close_order")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderClose(Request $request, Order $order)
    {
        if (!$order) {
            die('error');
        }
        $close = $request->get('close');

        if ($close['client'] != '') {
            $client = $this->getDoctrine()->getRepository('RestaurantBundle:Client')->find($close['client']);
            $order->setClient($client);
            $order->setCashType(Order::CASHTYPE_INVOICE);
        }

        $dishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findBy([
            'order' => $order,
        ]);

        $sum = $order->getDishSum();

        if ($sum < 0) {
            /* @var OrderDish $orderDish */
            foreach ($dishes as $orderDish) {
                if ($orderDish->getCashSum() < 0) {
                    $orderDish->setDishPrice($orderDish->getDishPrice() - $sum);
                }
            }
        };

        $em = $this->getDoctrine()->getManager();

        $order->setStatus(Order::STATUS_CLOSED);
        $order->setClosedAt(new \DateTime());
        $order->setIsEditable(false);

        if ($order->getCashType() == Order::CASHTYPE_INVOICE) {
            $order->setCashType(Order::CASHTYPE_INVOICE);
            $this->printOrder($order);
        } else {
            if ($order->getCashType() != Order::CASHTYPE_ONLINE) {
                $order->setCashType(Order::CASHTYPE_CASH);
                $order->setClientMoneyCash($close['clientMoneyCash']);
                $order->setClientMoneyCard($close['clientMoneyCard']);
            }
        }

        $em->flush();

        $additional = '';
        if ($this->getParameter('is_primary') && $this->isGranted("ROLE_SHOP_MANAGER")) {
            $ch = curl_init("http://" . $order->getShop()->getVpnServerIP() . "/api/order/" . $order->getId() . "/close");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                "cash" => $close['clientMoneyCash'],
                "card" => $close['clientMoneyCard'],
            ]);
            $response = curl_exec($ch);
        } else {

            if ($order->getCashType() == Order::CASHTYPE_CASH) {

                if (bccomp($order->getCashSum(), round($order->getDishSum(), 2)) == 0) {

                    if ($this->getUser()->isFiscalGranted()) {
                        $this->get('terminal.fiscal')->printCheck($order, Fiscal::FISCAL_SELL, $order->getShop()->getFiscalModel(), $this->get('user.logger'));
                    }
                } else {
                    $order->setStatus(Order::STATUS_WAIT_FOR_MONEY);
                    $em->flush();
                }
            }
        }


        if (($this->getUser()->getIsCalcMoneyBack()) && ($order->getCashType() == Order::CASHTYPE_CASH) && ($order->getTable()->isToGo())) {
            $additional .= "calcMoneyBack(" . $order->getClientMoneyCash() . ");";
        }

        return new JsonResponse([
            'status' => true,
            'command' => $additional,
        ]);
    }

    private function printOrder(Order $order)
    {
        $event = new OrderEvent($order);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(OrderEvent::ORDER_PRINT, $event);
    }

    /**
     * @Route("/order/{id}/fiscal", name="close_order_fiscal")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderFiscalClose(Request $request, Order $order)
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->getUser()->isFiscalGranted()) {
            $this->get('terminal.fiscal')->printCheck($order->getId(), Fiscal::FISCAL_SELL);
        }

        $em->flush();
        return new JsonResponse([
            'status' => true,
        ]);
    }

    /**
     * @Route("/order/{id}/double-close", name="double_close_order")
     */
    public function doubleCloseOrderAction(Request $request, Order $order)
    {
        $em = $this->getDoctrine()->getManager();

        $newOrder = clone $order;

        $order->setStatus(Order::STATUS_SEMI_DELETED);
        $order->setDeletedAt(new \DateTime());
        $order->setIsEditable(false);

        $newOrder->setIsEditable(true);
        $newOrder->setLocalId($order->getLocalId());

        foreach ($order->getDishes() as $dish) {
            $newDish = clone $dish;
            $newDish->setOrder($newOrder);
            $newDish->setParentOrder($newOrder);
            $newOrder->addDish($newDish);
        }

        $newOrder->setMeta(clone $order->getMeta());
        $order->setCopyOf($newOrder);
        $order->getMeta()->setOther($request->get("comment"));

        $em->persist($order);
        $em->persist($newOrder);
        $em->flush();

        return new JsonResponse([
            'status' => true,
            'command' => 'closeOrder();$(".workspace").hide();$("#table_control").show();',
        ]);
    }

    /**
     * @Route("/order/{id}/confirm", name="confirm_order")
     */
    public function confirmOrderAction(Request $request, Order $order)
    {
        $em = $this->getDoctrine()->getManager();

        $order->setStatus(Order::STATUS_OPENED);

        $em->persist($order);
        $em->flush();

        if ($order->getMeta()->getConfirmUrl() != "") {
            $ch = curl_init($order->getMeta()->getConfirmUrl());
            $res = curl_exec($ch);
            $this->get('user.logger')->info('CONFIRM ' . $res);
        }

        if ($this->get("kernel")->getEnvironment() == "prod" && $order->getMeta()->getOrderType() == 1) {
            var_dump(123);
            die();
            $orderId = $this->get("delivery.taxi.city")->call($order, $request->get("time"));
            $this->get('user.logger')->info('TAXI ' . var_export($orderId, true));
        }

        $this->printOrder($order);
        $this->printOrderToKitchens($order);

        return new JsonResponse([
            'taxiorder' => $orderId
        ]);
    }

    private function printOrderToKitchens(Order $order)
    {
        $order = $this->getDoctrine()->getRepository("OrderBundle:Order")->findWithPrinters($order);
        $event = new OrderEvent($order);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(OrderEvent::ORDER_PRINT_KITCHEN, $event);
    }

    /**
     * @Route("/order/{id}/redirect", name="redirect_order")
     */
    public function redirectOrderAction(Request $request, Order $order)
    {
        $em = $this->getDoctrine()->getManager();

        $order = $this->get("order.manager")->delete($order);
        $this->get("order.manager")->redirectOrder($order, $em->getReference(Shop::class, $request->get('target')));

        $em->persist($order);
        $em->flush();

        return new JsonResponse([

        ]);
    }

    /**
     * @Route("/order/{id}/reject", name="reject_order")
     */
    public function rejectOrderAction(Request $request, Order $order)
    {
        $em = $this->getDoctrine()->getManager();

        if ($order->getMeta()->getRejectUrl() != "") {
            $ch = curl_init($order->getMeta()->getRejectUrl());
            $res = curl_exec($ch);
            $this->get('user.logger')->info('REJECT ' . $res . " " . $order->getId() . " " . $this->getUser()->getUsername());
        } else {
            $this->get('user.logger')->info('REJECT ' . $order->getId() . " " . $this->getUser()->getUsername());
        }

        $order = $this->get("order.manager")->delete($order);

        $em->persist($order);
        $em->flush();

        return new JsonResponse([

        ]);
    }

    /**
     * @Route("/order/{id}", name="order_show")
     */
    public function showOrderAction(Request $request, Order $order)
    {

//        if ($request->isXmlHttpRequest()) {
        $orderJson = $this->get("order.manager")->orderToJson($order);
        return new JsonResponse($orderJson);

//        } else {
//            return $this->render('@Terminal/Order/orderShow.html.twig', [
//                'order' => $order,
//            ]);
//        }
    }

    /**
     * @Route("/order/{id}/print", name="order_print")
     */
    public function printOrderAction(Request $request, Order $order)
    {
        $order = $this->getDoctrine()->getRepository('OrderBundle:Order')->findFullOrder($order->getId());

        switch ($request->get('type')) {
            case 1:
                $this->printOrder($order);
                $command = 'console.log(order);';
                break;
            case 2:
                $this->printOrderToKitchens($order);
                $command = 'console.log(order);';
                break;
            case 3:
                $this->printOrderToBar($order);
                $command = '';
                break;
            default:
                $this->printOrder($order);
                $command = 'console.log(order);';
                break;
        }

        return new JsonResponse([
            'status' => true,
            'command' => $command,
        ]);
    }

    private function printOrderToBar(Order $order)
    {
        $order = $this->getDoctrine()->getRepository("OrderBundle:Order")->findWithPrinters($order);
        $event = new OrderEvent($order);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(OrderEvent::ORDER_PRINT_BAR, $event);
    }

    private function getPromosArray($shop)
    {
        $promos = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findCurrentActive($shop);

        $promosArray = [];

        foreach ($promos as $promo) {
            $promosArray[] = [
                'id' => $promo->getId(),
                'discount' => $promo->getDiscount(),
                'icon' => $promo->getIcon(),
                'name' => $promo->getName(),
            ];
        }

        return $promosArray;
    }

    private function renderBlock($template, $block, $params = array())
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->get('twig');
        /** @var \Twig_Template $template */
        $template = $twig->loadTemplate($template);

        return $template->renderBlock($block, $twig->mergeGlobals($params));
    }


}
