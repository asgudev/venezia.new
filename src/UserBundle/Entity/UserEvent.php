<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RestaurantBundle\Entity\Workplace;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Entity\UserEventRepository")
 * @ORM\Table()
 */
class UserEvent
{

    const EVENT_USER_EXIT = 0;
    const EVENT_USER_ENTER = 1;

    const EVENT_USER_REVISION = 101;
    const EVENT_USER_SCREENSHOT = 102;

    const EVENT_STRING_TYPES = [
        self::EVENT_USER_EXIT => 'Выход',
        self::EVENT_USER_ENTER => 'Вход',
    ];


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Workplace")
     */
    private $workplace;

    /**
     * @ORM\Column(name="event_id", type="integer", nullable=true)
     */
    private $eventId;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(name="event_type", type="smallint", nullable=true)
     */
    private $eventType;


    /**
     * @ORM\Column(name="payload", type="text", nullable=true)
     */
    private $payload;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return UserEvent
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param mixed $eventId
     * @return UserEvent
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return UserEvent
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     * @return UserEvent
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param mixed $eventType
     * @return UserEvent
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEventTypeString()
    {
//        return self::EVENT_STRING_TYPES[$this->eventType];
        return ($this->eventType !== null ? self::EVENT_STRING_TYPES[$this->eventType] : "");
    }

    /**
     * @return mixed
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param mixed $payload
     * @return UserEvent
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return Workplace
     */
    public function getWorkplace()
    {
        return $this->workplace;
    }

    /**
     * @param mixed $workplace
     * @return UserEvent
     */
    public function setWorkplace($workplace)
    {
        $this->workplace = $workplace;
        return $this;
    }

}
