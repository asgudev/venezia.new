<?php

namespace RestaurantBundle\Entity;

use DishBundle\Entity\Dish;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Restaurant
 *
 * @ORM\Table(name="shop")
 * @ORM\Entity(repositoryClass="RestaurantBundle\Entity\Repository\ShopRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Shop
{
    const PRINTER_ADMINISTRATOR = 0;
    const PRINTER_KITCHEN = 1;
    const PRINTER_BAR = 2;

    const FISCAL_TYPE_CASH = 0;
    const FISCAL_TYPE_TERMINAL = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToMany(targetEntity="DishBundle\Entity\Dish", mappedBy="shops")
     */
    private $dishes;

    /**
     * @ORM\OneToMany(targetEntity="ShopPlace", mappedBy="shop", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "asc"})
     */
    private $places;

    /**
     * @ORM\Column(name="admin_printer", type="string", nullable=true)
     */
    private $adminPrinter;

    /**
     * @ORM\Column(name="invoice_printer", type="string", nullable=true)
     */
    private $invoicePrinter;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\PrinterDevice")
     */
    private $adminPrinterDevice;

    /**
     * @ORM\OneToMany(targetEntity="RestaurantBundle\Entity\Printer", mappedBy="shop", cascade={"all"}, orphanRemoval=true)
     */
    private $printers;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\DishHistory", mappedBy="shop")
     */
    private $changes;

    /**
     * @ORM\Column(name="alias", type="string", nullable=true)
     */
    private $alias;

    /**
     * @ORM\Column(name="export_alias", type="string", nullable=true)
     */
    private $exportAlias;

    /**
     * @ORM\Column(name="fiscal_model", type="string", nullable=true)
     */
    private $fiscalModel;

    /**
     * @ORM\Column(name="fiscal_port", type="string", nullable=true)
     */
    private $fiscalPort;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\CategoryDelegate", mappedBy="shop", cascade={"all"})
     */
    private $delegates;

    /**
     * @ORM\Column(name="is_bar_service", type="boolean", nullable=false)
     */
    private $isBarService;

    /**
     * @ORM\Column(name="fiscal_type", type="integer", options={"default" = 0})
     */
    private $fiscalType;

    /**
     * @ORM\Column(name="server_id", type="integer", nullable=true)
     */
    private $serverId;

    /**
     * @ORM\Column(name="server_ip", type="text", nullable=true)
     */
    private $serverIP;

    /**
     * @ORM\Column(name="vpn_server_ip", type="text", nullable=true)
     */
    private $vpnServerIP;

    /**
     * @ORM\Column(name="revision_lock", type="boolean", nullable=true, options={"default" = false})
     */
    private $revisionLock;

    /**
     * @ORM\Column(name="organisation", type="string", nullable=true)
     */
    private $organisation;

    /**
     * @ORM\Column(name="image", type="string", nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="RestaurantBundle\Entity\Warehouse", mappedBy="shop")
     */
    private $warehouses;

    /**
     * @ORM\Column(name="menu_template", type="text", nullable=true)
     */
    private $menuTemplate;

    /**
     * @ORM\Column(name="custom_params", type="json")
     */
    private $customParams = [
        "oplatiId" => false,
    ];


    const ORDER_TYPE = [
        'Доставка' => 1,
        'Навынос' => 2,
        'На месте' => 4,
        'Заказ стола' => 8,
        'Заказ с сайта' => 16,
    ];

    /**
     * @ORM\Column(name="order_types", type="smallint")
     */
    private $orderTypes;



    /**
     * @ORM\Column(name="times", type="json")
     */
    private $times = [
        0 => [],
        1 => [],
        2 => [],
        3 => [],
        4 => [],
        5 => [],
        6 => [],
    ];

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dishes = new ArrayCollection();
        $this->delegates = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     * @return Shop
     */
    public function addDish(\DishBundle\Entity\Dish $dish)
    {
        $this->dishes[] = $dish;

        return $this;
    }

    /**
     * Remove dish
     *
     * @param \DishBundle\Entity\Dish $dish
     */
    public function removeDish(\DishBundle\Entity\Dish $dish)
    {
        $this->dishes->removeElement($dish);
    }

    /**
     * Get dishes
     *
     * @return Dish[]
     */
    public function getDishes()
    {
        return $this->dishes;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Shop
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Shop
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Add place
     *
     * @param ShopPlace $place
     *
     * @return Shop
     */
    public function addPlace(ShopPlace $place)
    {
        $this->places[] = $place;
        $place->setShop($this);

        return $this;
    }

    /**
     * Remove place
     *
     * @param ShopPlace $place
     */
    public function removePlace(ShopPlace $place)
    {
        $this->places->removeElement($place);
    }

    /**
     * Get places
     *
     * @return ShopPlace[]
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Add printer
     *
     * @param Printer $printer
     *
     * @return Shop
     */
    public function addPrinter(Printer $printer)
    {
        $this->printers[] = $printer;

        return $this;
    }

    /**
     * Remove printer
     *
     * @param Printer $printer
     */
    public function removePrinter(Printer $printer)
    {
        $this->printers->removeElement($printer);
    }

    /**
     * Get printers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrinters()
    {
        return $this->printers;
    }

    /**
     * Get printers
     *
     * @return Printer
     */
    public function getPrinterByType($type)
    {
        foreach ($this->printers as $printer) {
            if ($printer->getType() == $type) return $printer;
        }
        return null;
    }


    /**
     * @Assert\File(maxSize="6000000")
     */
    private $imageFile;

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;
        // check if we have an old image path
        if (isset($this->image)) {
            // store the old name to delete after the update
            $this->temp = $this->image;
            $this->image = null;
        } else {
            $this->image = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getImageAbsolutePath()
    {
        return null === $this->image
            ? null
            : $this->getImageUploadRootDir() . '/' . $this->image;
    }

    public function getImage()
    {
        return null == $this->image
            ? ($this->getImageUploadDir() . '/common_sq.jpg')
            : ($this->getImageUploadDir() . '/' . $this->image);
    }

    protected function getImageUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        return 'uploads/images/shop';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preImageUpload()
    {
        if (null !== $this->getImageFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->image = $filename . '.' . $this->getImageFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }
        $this->getImageFile()->move($this->getImageUploadRootDir(), $this->image);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            //unlink($this->getImageUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->imageFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeImageUpload()
    {
        $file = $this->getImageAbsolutePath();
        if ($file) {
            //unlink($file);
        }
    }


    /**
     * @return Shop
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Add change
     *
     * @param \DishBundle\Entity\DishHistory $change
     *
     * @return Shop
     */
    public function addChange(\DishBundle\Entity\DishHistory $change)
    {
        $this->changes[] = $change;

        return $this;
    }

    /**
     * Remove change
     *
     * @param \DishBundle\Entity\DishHistory $change
     */
    public function removeChange(\DishBundle\Entity\DishHistory $change)
    {
        $this->changes->removeElement($change);
    }

    /**
     * Get changes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChanges()
    {
        return $this->changes;
    }

    /**
     * Set adminPrinter
     *
     * @param string $adminPrinter
     *
     * @return Shop
     */
    public function setAdminPrinter($adminPrinter)
    {
        $this->adminPrinter = $adminPrinter;

        return $this;
    }

    /**
     * Get adminPrinter
     *
     * @return string
     */
    public function getAdminPrinter()
    {
        return $this->adminPrinter;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Shop
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Add delegate
     *
     * @param \DishBundle\Entity\CategoryDelegate $delegate
     *
     * @return Shop
     */
    public function addDelegate(\DishBundle\Entity\CategoryDelegate $delegate)
    {
        $this->delegates[] = $delegate;
        $delegate->setShop($this);

        return $this;
    }

    /**
     * Remove delegate
     *
     * @param \DishBundle\Entity\CategoryDelegate $delegate
     */
    public function removeDelegate(\DishBundle\Entity\CategoryDelegate $delegate)
    {
        $this->delegates->removeElement($delegate);
    }

    /**
     * Get delegates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDelegates()
    {
        return $this->delegates;
    }

    public function getDelegatedPrinter()
    {
        if ($this->printers[0]) {
            return $this->printers[0]->getName();
        } else {
            return $this->getAdminPrinter();
        }
    }

    /**
     * Set exportAlias
     *
     * @param string $exportAlias
     *
     * @return Shop
     */
    public function setExportAlias($exportAlias)
    {
        $this->exportAlias = $exportAlias;

        return $this;
    }

    /**
     * Get exportAlias
     *
     * @return string
     */
    public function getExportAlias()
    {
        return $this->exportAlias;
    }

    /**
     * Set isBarService
     *
     * @param boolean $isBarService
     *
     * @return Shop
     */
    public function setIsBarService($isBarService)
    {
        $this->isBarService = $isBarService;

        return $this;
    }

    /**
     * Get isBarService
     *
     * @return boolean
     */
    public function getIsBarService()
    {
        return $this->isBarService;
    }

    /**
     * Set fiscalType
     *
     * @param integer $fiscalType
     *
     * @return Shop
     */
    public function setFiscalType($fiscalType)
    {
        $this->fiscalType = $fiscalType;

        return $this;
    }

    /**
     * Get fiscalType
     *
     * @return integer
     */
    public function getFiscalType()
    {
        return $this->fiscalType;
    }

    /**
     * Set invoicePrinter
     *
     * @param string $invoicePrinter
     *
     * @return Shop
     */
    public function setInvoicePrinter($invoicePrinter)
    {
        $this->invoicePrinter = $invoicePrinter;

        return $this;
    }

    /**
     * Get invoicePrinter
     *
     * @return string
     */
    public function getInvoicePrinter()
    {
        return $this->invoicePrinter;
    }

    /**
     * Set serverId
     *
     * @param integer $serverId
     *
     * @return Shop
     */
    public function setServerId($serverId)
    {
        $this->serverId = $serverId;

        return $this;
    }

    /**
     * Get serverId
     *
     * @return integer
     */
    public function getServerId()
    {
        return $this->serverId;
    }

    /**
     * Set revisionLock
     *
     * @param boolean $revisionLock
     *
     * @return Shop
     */
    public function setRevisionLock($revisionLock)
    {
        $this->revisionLock = $revisionLock;

        return $this;
    }


    /**
     * Get revisionLock
     *
     * @return boolean
     */
    public function isRevisionLock()
    {
        return $this->revisionLock;
    }

    /**
     * Get revisionLock
     *
     * @return boolean
     */
    public function getRevisionLock()
    {
        return $this->revisionLock;
    }


    /**
     * @return mixed
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * @param mixed $organisation
     * @return Shop
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getFiscalModel()
    {
        return $this->fiscalModel;
    }

    /**
     * @param mixed $fiscalModel
     * @return Shop
     */
    public function setFiscalModel($fiscalModel)
    {
        $this->fiscalModel = $fiscalModel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiscalPort()
    {
        return $this->fiscalPort;
    }

    /**
     * @param mixed $fiscalPort
     * @return Shop
     */
    public function setFiscalPort($fiscalPort)
    {
        $this->fiscalPort = $fiscalPort;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWarehouses()
    {
        return $this->warehouses;
    }

    /**
     * @param mixed $warehouses
     * @return Shop
     */
    public function setWarehouses($warehouses)
    {
        $this->warehouses = $warehouses;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getServerIP()
    {
        return $this->serverIP;
    }

    /**
     * @param mixed $serverIP
     * @return Shop
     */
    public function setServerIP($serverIP)
    {
        $this->serverIP = $serverIP;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMenuTemplate()
    {
        return $this->menuTemplate;
    }

    /**
     * @param mixed $menuTemplate
     * @return Shop
     */
    public function setMenuTemplate($menuTemplate)
    {
        $this->menuTemplate = $menuTemplate;
        return $this;
    }

    /**
     * Set times.
     *
     * @param array|null $times
     *
     * @return Shop
     */
    public function setTimes($times = null)
    {
        $this->times = $times;

        return $this;
    }

    /**
     * Get times.
     *
     * @return ShopTime[]|null
     */
    public function getTimes()
    {
        return $this->times;
    }

    /**
     * Set customParams.
     *
     * @param array|null $customParams
     *
     * @return Shop
     */
    public function setCustomParams($customParams = null)
    {
        $this->customParams = $customParams;

        return $this;
    }

    /**
     * Get customParams.
     *
     * @return array
     */
    public function getCustomParams()
    {
        return $this->customParams;
    }


    public function getTodayTimes()
    {
        $now = new \DateTime();
        $dayOfWeek = $now->format('w');
        /** @var ShopTime $__time */
        foreach ($this->times as $__day => $__time) {
            if ($__day == $dayOfWeek) {
                return $__time;
            }
        }
        return null;
    }

    /**
     * Add warehouse.
     *
     * @param \RestaurantBundle\Entity\Warehouse $warehouse
     *
     * @return Shop
     */
    public function addWarehouse(\RestaurantBundle\Entity\Warehouse $warehouse)
    {
        $this->warehouses[] = $warehouse;

        return $this;
    }

    /**
     * Remove warehouse.
     *
     * @param \RestaurantBundle\Entity\Warehouse $warehouse
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeWarehouse(\RestaurantBundle\Entity\Warehouse $warehouse)
    {
        return $this->warehouses->removeElement($warehouse);
    }

    /**
     * @return mixed
     */
    public function getOrderTypes()
    {
        return $this->orderTypes;
    }

    /**
     * @param mixed $orderTypes
     * @return Shop
     */
    public function setOrderTypes($orderTypes)
    {
        $this->orderTypes = $orderTypes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVpnServerIP()
    {
        return $this->vpnServerIP;
    }

    /**
     * @param mixed $vpnServerIP
     * @return Shop
     */
    public function setVpnServerIP($vpnServerIP)
    {
        $this->vpnServerIP = $vpnServerIP;
        return $this;
    }

    /**
     * @return PrinterDevice
     */
    public function getAdminPrinterDevice()
    {
        return $this->adminPrinterDevice;
    }

    /**
     * @param mixed $adminPrinterDevice
     * @return Shop
     */
    public function setAdminPrinterDevice($adminPrinterDevice)
    {
        $this->adminPrinterDevice = $adminPrinterDevice;
        return $this;
    }


}
