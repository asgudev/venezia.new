<?php

namespace OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\Common\Collections\ArrayCollection;

use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\ShopPlaceTable;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Client;
use UserBundle\Entity\User;

/**
 * Order
 *
 * @ORM\Table(
 *     name="orders",
 *     indexes={@Index(name="main_idx", columns={"shop_id", "created_at", "status"})}
 * )
 * @ORM\Entity(repositoryClass="OrderBundle\Entity\Repository\OrderRepository")
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="OrderBundle\Entity\OrderDish", mappedBy="order", cascade={"persist"})
     */
    private $dishes;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(name="delivered_at", type="datetime", nullable=true)
     */
    private $deliveredAt;

    /**
     * @ORM\Column(name="closed_at", type="datetime", nullable=true)
     */
    private $closedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(name="is_editable", type="boolean")
     */
    private $isEditable;

    /**
     * @ORM\Column(name="is_change_requested", type="boolean", nullable=true)
     */
    private $isChangeRequested;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(name="local_id", type="integer", nullable=false)
     */
    private $localId;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Client", inversedBy="orders")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\PromoCard", inversedBy="orders")
     */
    private $promo;

    /**
     * @ORM\OneToOne(targetEntity="OrderBundle\Entity\OrderMeta", inversedBy="order",cascade={"persist"})
     */
    private $meta;

    /**
     * @ORM\OneToOne(targetEntity="OrderBundle\Entity\Order")
     */
    private $copyOf;


    const STATUS_FAIL = -2;
    const STATUS_WAIT_FOR_CONFIRM = -1;
    const STATUS_OPENED = 0;
    const STATUS_WAIT_FOR_KITCHEB = 1;
    const STATUS_WAIT_FOR_CLOSE = 3;
    const STATUS_CLOSED = 4;
    const STATUS_DELETED = 5;
    const STATUS_SEMI_DELETED = 6;

    const STATUS_WAIT_FOR_MONEY = 2;
    const STATUS_PAID = 20;
    const STATUS_PAID_VERIFIED = 25;
    const STATUS_PRINTED = 30;
    const STATUS_READY = 40;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\ShopPlaceTable")
     */
    private $table;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\ShopPlace")
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    const CASHTYPE_CASH = 0;
    const CASHTYPE_ONLINE = 1;
    const CASHTYPE_INVOICE = 2;
    const CASHTYPE_INTERNAL = 3;
    const CASHTYPE_OPLATI = 4;

    /**
     * @ORM\Column(name="cash_type", type="smallint", nullable=true, options={"default" = 0})
     */
    private $cashType;

    /**
     * @ORM\Column(name="client_money_cash", type="decimal", scale=2, nullable=true, options={"default" = 0})
     */
    private $clientMoneyCash;

    /**
     * @ORM\Column(name="client_money_card", type="decimal", scale=2, nullable=true, options={"default" = 0})
     */
    private $clientMoneyCard;


    /**
     * Constructor
     */
    public function __construct(User $user, Shop $shop)
    {
        $this->user = $user;
        $this->shop = $shop;
        $this->dishes = new ArrayCollection();

        $this->createdAt = new \DateTime();

        $this->status = self::STATUS_OPENED;
        $this->cashType = self::CASHTYPE_CASH;
        $this->client = null;

        $this->isEditable = false;
        $this->isChangeRequested = false;

        $this->clientMoneyCash = 0;
        $this->clientMoneyCard = 0;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Add dish
     *
     * @param OrderDish $dish
     *
     * @return Order
     */
    public function addDish(OrderDish $dish)
    {
        $this->dishes[] = $dish;
        $dish->setOrder($this);

        return $this;

    }

    /**
     * Remove dish
     *
     * @param OrderDish $dish
     */
    public function removeDish(OrderDish $dish)
    {
        $this->dishes->removeElement($dish);
        $dish->setOrder(null);
        return $this;
    }

    /**
     * Get dishes
     *
     * @return ArrayCollection<OrderDish>
     */
    public function getDishes()
    {
        return $this->dishes;
    }

    /**
     * Get dishes
     *
     * @return OrderDish
     */
    public function clearDishes()
    {
        $this->dishes = [];
        return $this;
    }

    /**
     * @param $id
     * @return OrderDish|null
     */
    public function findDish($id) {
        foreach ($this->dishes as $__dish) {
            if ($__dish->getId() == $id) {
                return $__dish;
            }
        }

        return null;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return Order
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set closedAt
     *
     * @param \DateTime $closedAt
     *
     * @return Order
     */
    public function setClosedAt($closedAt)
    {
        $this->closedAt = $closedAt;

        return $this;
    }

    /**
     * Get closedAt
     *
     * @return \DateTime
     */
    public function getClosedAt()
    {
        return $this->closedAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set table
     * @return Order
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Get table
     *
     * @return ShopPlaceTable
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Order
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add table
     *
     * @param \RestaurantBundle\Entity\ShopPlaceTable $table
     *
     * @return Order
     */
    public function addTable(\RestaurantBundle\Entity\ShopPlaceTable $table)
    {
        $this->table[] = $table;

        return $this;
    }

    /**
     * Remove table
     *
     * @param \RestaurantBundle\Entity\ShopPlaceTable $table
     */
    public function removeTable(\RestaurantBundle\Entity\ShopPlaceTable $table)
    {
        $this->table->removeElement($table);
    }

    /**
     * Set cashType
     *
     * @param integer $cashType
     *
     * @return Order
     */
    public function setCashType($cashType)
    {
        $this->cashType = $cashType;

        return $this;
    }

    /**
     * Get cashType
     *
     * @return integer
     */
    public function getCashType()
    {
        $client = $this->getClient();
        if (($client != null) && (($client->getUnp() == 'none') || (!$client->getIsExportable()))) {
            return self::CASHTYPE_INTERNAL;
        }
        return $this->cashType;
    }

    public function getFullSum()
    {
        $sum = 0;
        foreach ($this->dishes as $dish) {
            $sum += ($dish->getFullSum());
        }
        return $sum;
    }

    public function getCashSum()
    {

        return ($this->getClientMoneyCash() + $this->getClientMoneyCard());
    }

    public function getDishSum()
    {
        $sum = 0;
        foreach ($this->dishes as $dish) {
            $sum += ($dish->getCashSum());
        }
        return $sum;
    }

    public function getSumDiscount()
    {
        $sumDiscount = 0;
        foreach ($this->dishes as $dish) {
            $sumDiscount += ($dish->getDiscountSum());
        }
        return $sumDiscount;
    }

    /**
     * Set deliveredAt
     *
     * @param \DateTime $deliveredAt
     *
     * @return Order
     */
    public function setDeliveredAt($deliveredAt)
    {
        $this->deliveredAt = $deliveredAt;

        return $this;
    }

    /**
     * Get deliveredAt
     *
     * @return \DateTime
     */
    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }

    /**
     * Set client
     *
     * @param \RestaurantBundle\Entity\Client $client
     *
     * @return Order
     */
    public function setClient(\RestaurantBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \RestaurantBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return ShopPlace
     */
    public function getPlace()
    {
        return $this->place;
    }

    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }


    /**
     * Set localId
     *
     * @param integer $localId
     *
     * @return Order
     */
    public function setLocalId($localId)
    {
        $this->localId = $localId;

        return $this;
    }

    /**
     * Get localId
     *
     * @return integer
     */
    public function getLocalId()
    {
        return $this->localId;
    }

    /**
     * Set isEditable
     *
     * @param boolean $isEditable
     *
     * @return Order
     */
    public function setIsEditable($isEditable)
    {
        $this->isEditable = $isEditable;

        return $this;
    }

    /**
     * Get isEditable
     *
     * @return boolean
     */
    public function getIsEditable()
    {
        return $this->isEditable;
    }

    public function getClientName()
    {
        $client = $this->client;
        if ($client != null) {
            if ($client->getUnp() == 'null') {
                return 'client';
            } else {
                return $client->getUnp();
            }
        } else {
            return 'client';
        }
    }

    public function getAddedDishes($dishes) {
        $old = $this->getDishesArray();

        foreach ($dishes as  $__newDish) {

        }

    }


    public function getDishesArray()
    {
        $dishes = [];

        /**
         * @var OrderDish $dish
         */
        foreach ($this->dishes as $dish) {
            $__dish = [
                "id" => $dish->getId(),
                "name" => $dish->getDish()->getName(),
                "dish" => $dish->getDish()->getId(),
                "dishPrice" => $dish->getDishPrice(),
                "price" => $dish->getDishPrice(),
                "dishDiscount" => $dish->getDishDiscount(),
                "quantity" => $dish->getQuantity(),
                "time" => $dish->getTime(),
            ];
            if ($dish->getDelegate())
                $__dish["delegate"] = $dish->getDelegate()->getId();

            $dishes[] = $__dish;
        }

        return $dishes;

    }


    /**
     * Set promo
     *
     * @param \RestaurantBundle\Entity\Promo $promo
     *
     * @return Order
     */
    public function setPromo(\RestaurantBundle\Entity\PromoCard $promo = null)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return \RestaurantBundle\Entity\PromoCard
     */
    public function getPromo()
    {
        return $this->promo;
    }


    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getClientMoneyCash()
    {
        return $this->clientMoneyCash;
    }

    /**
     * @param mixed $clientMoneyCash
     * @return Order
     */
    public function setClientMoneyCash($clientMoneyCash)
    {
        $this->clientMoneyCash = round($clientMoneyCash, 2);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClientMoneyCard()
    {
        return $this->clientMoneyCard;
    }

    /**
     * @param mixed $clientMoneyCard
     * @return Order
     */
    public function setClientMoneyCard($clientMoneyCard)
    {
        $this->clientMoneyCard = round($clientMoneyCard, 2);
        return $this;
    }


    /**
     * @return OrderMeta
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param mixed $meta
     * @return Order
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
        return $this;
    }

    /**
     * @return bool
     */
    public function isChangeRequested(): bool
    {
        return $this->isChangeRequested;
    }

    /**
     * @param bool $isChangeRequested
     * @return Order
     */
    public function setIsChangeRequested(bool $isChangeRequested): Order
    {
        $this->isChangeRequested = $isChangeRequested;
        return $this;
    }

    /**
     * @return Order | null
     */
    public function getCopyOf()
    {
        return $this->copyOf;
    }

    /**
     * @param mixed $copyOf
     * @return Order
     */
    public function setCopyOf($copyOf)
    {
        $this->copyOf = $copyOf;
        return $this;
    }





}
