<?php

namespace RestaurantBundle\Command;

use ControlBundle\Entity\DishPhoto;
use ControlBundle\Entity\DishPhotoCheck;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;
use UserBundle\Entity\UserEvent;

class ParsePersonTimingDataCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('person:timing:parse')
            ->setDescription('Parse guardsaas');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $doctrine = $this->getContainer()->get('doctrine');
            $em = $doctrine->getManager();
            $guardsaas = $this->getContainer()->get('guardsaas.service');

            $lastEvent = $em->getRepository("UserBundle:UserEvent")->findBy([], ['date' => 'DESC'], 1);

            $events = $guardsaas->getLastEvents($lastEvent ? $lastEvent[0]->getEventId() : 0);

            foreach ($events->items as $__event) {
                $dbEvent = $doctrine->getRepository('UserBundle:UserEvent')->findOneBy([
                    'eventId' => $__event->id,
                ]);
                if ($dbEvent) {
                    continue;
                }


                $user = $em->getRepository('UserBundle:User')->findOneBy([
                    'cardId' => mb_convert_case($__event->cardCode, MB_CASE_TITLE, 'UTF-8'),
                ]);

                if (!$user) {
                    $user = new User();
                    $user->setName(mb_convert_case($__event->employee, MB_CASE_TITLE, 'UTF-8'));
                    $user->setUserName(md5($__event->employee));
                    $user->setPassword(md5(md5($__event->employee)));
                    $user->setCardId($__event->cardCode);
                    $user->setShop($em->getReference('RestaurantBundle:Shop', 0));
                } else {
                    if ($user->getCardId() == null)
                        $user->setCardId($__event->cardCode);
                }

                $shop = $em->getRepository('RestaurantBundle:Shop')->findOneBy([
                    'exportAlias' => $__event->object,
                ]);

                $userEvent = new UserEvent();
                $userEvent->setEventId($__event->id);
                $userEvent->setDate(new \DateTime($__event->time));
                $userEvent->setUser($user);
                $userEvent->setEventType(UserEvent::EVENT_USER_ENTER);
                if ($shop)
                    $userEvent->setShop($shop);

                $user->setShop($userEvent->getShop());

                $em->persist($user);
                $em->persist($userEvent);
                $em->flush();

                if ($user->getTelegramChatId() != null) {
                    $telegram = $this->getContainer()->get('telegram.service');
                    $msg = $userEvent->getUser()->getName() . " " . $userEvent->getShop()->getName() . " " . $userEvent->getDate()->format("Y-m-d H:i:s");
                    $telegram->sendMessage($user->getTelegramChatId(), "Событие по карте получено " . $msg . " " . md5($msg));
                }
            }


        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}