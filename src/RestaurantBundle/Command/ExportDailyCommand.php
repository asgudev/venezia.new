<?php

namespace RestaurantBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use OrderBundle\Entity\Order;


class ExportDailyCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->defaultDate = '1 day ago';

        $this
            ->setName('restaurant:daily:export')
            ->setDescription('Export daily data')
            //->addOption('date', '-d', InputOption::VALUE_OPTIONAL, 'Export date', $this->defaultDate)
            ->addOption('shop', '-s', InputOption::VALUE_OPTIONAL, 'Export shop', 0);
    }

    private $exportDate;
    private $defaultDate;


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $spool_path = $this->getContainer()->get('kernel')->getRootDir() . '/../var/export/';
        //$date = new \DateTime($input->getOption('date'));

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $renderer = $this->getContainer()->get('templating');


        try {
            if ($input->getOption('shop') == 0) {
                $shops = $em->getRepository("RestaurantBundle:Shop")->findAll();
            } else {
                $shops = $em->getRepository("RestaurantBundle:Shop")->findBy([
                    'id' => $input->getOption('shop')
                ]);
            }
            foreach ($shops as $shop) {
                $report = $doctrine->getRepository('TerminalBundle:DailyReport')->findLastReport($shop);
                $dishes = $doctrine->getRepository("OrderBundle:OrderDish")->findDishesByReport($report);
                $firstOrder = $doctrine->getRepository('OrderBundle:Order')->findFirstOrderInReport($report);

                if ($firstOrder !== null) {
                    $date = $firstOrder->getCreatedAt();
                } else {
                    die();
                }

                $total = 0;
                $totalA = [
                    'КУХНЯ' => [
                        'terminal' => 0,
                        'cash' => 0,
                    ],
                    'БАР' => [
                        'terminal' => 0,
                        'cash' => 0,
                    ],
                ];
                foreach ($dishes as $dish) {
                    $rdish = $dish->getDish();
                    $rcat = $rdish->getCategory()->getParent()->getName();
                    $ctype = $dish->getOrder()->getCashType() == 1 ? 'terminal' : 'cash';

                    if (!array_key_exists(0, $rdish->getDishMetas()->toArray())) {
                        dump($dish);
                        die();
                    }
                    $total += ($dish->getQuantity() * $dish->getDishPrice());


                    if ($dish->getOrder()->getCashType() !== Order::CASHTYPE_INVOICE) {
                        $totalA[$rcat][$ctype] += ($dish->getQuantity() * $dish->getDishPrice());
                    }
                }


                /*
                foreach ($delegated as $dish) {
                    $total += ($dish->getDish()->getDishPrice() * $dish->getDish()->getQuantity());
                }
                */

                $xml = $renderer->render("@Restaurant/Custom/export.xml.twig", [
                    'shop' => $shop,
                    'dishes' => $dishes,
                    'total' => $total,
                    'date' => $date,
                    //  'delegated' => $delegated,
                ]);


                $dishDataFilename = "R" . $date->format("dmY") . "_" . $shop->getExportAlias() . '_' . time() . ".xml";

                $dishDataFile = fopen($spool_path . $dishDataFilename, "w");
                fputs($dishDataFile, $xml);
                fclose($dishDataFile);

                $csvDataFileName = $shop->getExportAlias() . '_' . $date->format("dmY") . '_' . time() . '.csv';
                $csvDataFile = fopen($spool_path . $csvDataFileName, 'w');
                fputs($csvDataFile, $shop->getAlias() . ';' . $date->format("Y-m-d") . "\r\n");
                fputs($csvDataFile, $totalA['БАР']['cash'] . ';' . $totalA['БАР']['terminal'] . "\r\n");
                fputs($csvDataFile, $totalA['КУХНЯ']['cash'] . ';' . $totalA['КУХНЯ']['terminal']);
                fclose($csvDataFile);



                dump($totalA);
                die();
            }
        } catch (\Exception $em) {
            dump($em);
            die("123");
        }
    }
}
