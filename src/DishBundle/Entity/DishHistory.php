<?php

namespace DishBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Dish
 *
 * @ORM\Table(name="dish_history")
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\DishHistoryRepository")
 */
class DishHistory
{

    const STATUS_NEW = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_DECLINED = 2;
    const STATUS_UPDATED = 3;

    const STATUS_TEXT = [
        self::STATUS_NEW => 'Ожидает подписи',
        self::STATUS_CONFIRMED => 'Подтверждено',
        self::STATUS_DECLINED => 'Отклонено',
        self::STATUS_UPDATED => 'Обновлено',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish", inversedBy="changes")
     */
    private $dish;
    
    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop", inversedBy="changes")
     */
    private $shop;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @ORM\Column(name="field", type="string", nullable=true)
     */
    private $field;

    /**
     * @ORM\Column(name="value", type="string", nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $confirmedBy;

    /**
     * @ORM\Column(name="confirmed_at", type="datetime", nullable=true)
     */
    private $confirmedAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="status", type="smallint", nullable=false, options={"default" : 0})
     */
    private $status;

    public function __construct(Dish $dish)
    {
        $this->date = new \DateTime();

        $this->dish = $dish;
        $this->status = self::STATUS_NEW;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DishHistory
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return DishHistory
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     * @return DishHistory
     */
    public function setDish(\DishBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \DishBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return DishHistory
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     * @return DishHistory
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return DishHistory
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmedBy()
    {
        return $this->confirmedBy;
    }

    /**
     * @param mixed $confirmedBy
     * @return DishHistory
     */
    public function setConfirmedBy($confirmedBy)
    {
        $this->confirmedBy = $confirmedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * @param mixed $confirmedAt
     * @return DishHistory
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getStatusText()
    {
        return self::STATUS_TEXT[$this->status];
    }

    /**
     * @param mixed $status
     * @return DishHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return DishHistory
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
