var table = $("table").dataTable({
    pageLength: 25,
    fixedHeader: true,
    dom: "Bftrip",

    buttons: [
        {
            extend: 'print',
            text: 'Печать',
            title: 'Блюда',
            autoPrint: true,
            customize: function (win) {
                $(win.document.body).find('td:first-child').css('white-space', 'nowrap');
            }
        },
        'excel',
    ],
    "lengthMenu": [10, 25, 50, -1],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;

        var length = api.columns()[0].length;

        for (var i = 0; i < length; i++) {
            var $current = $(api.column(i).footer());
            $current.data({
                'index': i,
            });

            if ($current.hasClass("toggle")) {
                $current.on("click", function () {
                    var column = api.column($(this).data("index"));
                    column.visible(!column.visible());
                });
            }
        }
    }
});
var table_api = table.api();
table_api.columns().every(function () {
    var that = this;
    $('input', this.header()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    }).on("click", function (event) {
        event.stopPropagation();
    });
});

$("#reset_visibility").on("click", function () {
    var columns = table_api.columns();
    columns[0].forEach(function (index) {
        table_api.column(index).visible(true);
    });
});

function hideAvailability() {
    $(".enabled, .disabled").each(function (key, value) {
        $(this).removeClass("enabled");
        $(this).removeClass("disabled");
    });

    $(this).one("click", showAvailability);
}
function showAvailability() {
    $("td").each(function (key, value) {
        var data = $(value).data();
        if (data.status !== undefined) {
            $(this).addClass(data.status);
        }
    });

    $(this).one("click", hideAvailability);
}
$("#reset_availability").one("click", hideAvailability);

var $cell;
var inputs = [];
$(document).on("dblclick", ".editable", function () {
    $cell = $(this);
    var content = $(this).text();
    var title = $(this).attr('title');
    var row = $(this).parent("tr");
    var data = {
        dish: row.data("id"),
    };
    if ($(this).hasClass("base_price")) {
        data.type = "base";
    } else if ($(this).hasClass("special_price")) {
        data.type = "special";
        data.shop = $cell.data("shop");
    } else {
        return false;
    }



    var $input = $("<input />", {
        'placeholder': 'Цена',
    })
        .val(content).css({
            'width': "100%",
            'font-weight': "bold",
            'display': 'block',
        })
        .data({
            baseVal: content,
            parent: $cell,
        })
        .keypress(function (e) {
            data.newPrice = $(this).val();
            var newPrice;
            var tmpRow = $(this).parents("tr");
            if (e.which == 13) {
                $.ajax({
                    url: "/admin/dish/updateAjax",
                    type: "POST",
                    row: tmpRow,
                    data: data,
                    success: function (response) {
                        //this.row.replaceWith(response.data);
                        //table.api().row(this.row).draw()
                    }
                });
            }
        })
        .click(function (event) {
            event.stopPropagation();
        });

    var $inputId1c = $("<input />", {
        'placeholder': 'ID 1c',
    })
        .val(title).css({
            'width': "100%",
            'font-weight': "bold",
            'display': 'block',
        })
        .data({
            baseVal: title,
            parent: $cell,
        })
        .keypress(function (e) {
            data.newId1c = $(this).val();
            var newPrice;
            var tmpRow = $(this).parents("tr");
            if (e.which == 13) {
                $.ajax({
                    url: "/admin/dish/updateAjax",
                    type: "POST",
                    row: tmpRow,
                    data: data,
                    success: function (response) {
                        //this.row.replaceWith(response.data);
                        //table.api().row(this.row).draw()
                    }
                });
            }
        })
        .click(function (event) {
            event.stopPropagation();
        });


    $cell.html($input);
//    $input.after($inputId1c);
    $input.focus().select();

    inputs.push($input);
//    inputs.push($inputId1c);
});

$(document).on("dblclick", ".editable_title", function () {
    $cell = $(this);
    var content = $(this).text();
    var row = $(this).parent("tr");
    var data = {
        dish: row.data("id"),
    };

    data.type = "name";
    data.locale = locale;

    var $input = $("<input />").val(content).css({
        'width': "100%",
        'font-weight': "bold",
    });
    $input.data({
        baseVal: content,
        parent: $cell,
    });
    $input.keypress(function (e) {
        data.value = $(this).val();
        var newPrice;
        var tmpRow = $(this).parents("tr");
        if (e.which == 13) {
            $.ajax({
                url: "/admin/dish/updateAjax",
                type: "POST",
                row: tmpRow,
                data: data,
                success: function (response) {
                    //this.row.replaceWith(response.data);
                }
            });
        }
    });
    $input.click(function (event) {
        event.stopPropagation();
    });

    $cell.html($input);
    $input.focus().select()
    inputs.push($input);
});

function removeEditable() {
    inputs.forEach(function (input, index, context) {
        var data = input.data();
        if (data.parent !== undefined) {
            data.parent.html(data.baseVal);
        }
        context.splice(index, 1);
    })
}

$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        removeEditable();
    }
});

var current_cat;
var content;
var row;
$(document).on("dblclick", ".editable_select", function () {
    $cell = $(this);
    current_cat = $(this).data("cat");
    content = $(this).text();
    row = $(this).parent("tr");

    $.ajax({
        url: "/admin/get_categories_ajax",
        type: "POST",
        success: function (response) {


            var $select = $("<select />");
            for (var val in response) {
                var cat = response[val];
                $("<option />", {value: cat.id, text: cat.name}).appendTo($select);
            }

            $select.data({
                baseVal: content,
                parent: $cell,
            });

            $select.css({
                'width': "100%",
                'font-weight': "bold",
            });

            $select.on("change", function () {
                var new_cat = $(this).val();

                $.ajax({
                    url: "/admin/dish/updateAjax",
                    type: "POST",
                    data: {
                        dish: row.data("id"),
                        type: "set_category",
                        value: new_cat
                    },
                    success: function (response) {
                        //row.replaceWith(response.data);
                    }
                });
            });

            $select.click(function (event) {
                event.stopPropagation();
            });

            $cell.html($select.val(current_cat));
            inputs.push($select);
        }
    });

});


$("body").click(function (e) {
    removeEditable();
});

$(document).on("click", ".delete_dish", function () {
    var link = $(this);
    var row = $(this).parents("tr");
    if (!confirm("Удалить это блюдо?")) {
        return false;
    } else {
        $.ajax({
            url: link.attr("href"),
            type: "GET",
            success: function (response) {
                if (response.status) {
                    if (response.command) {
                        eval(response.command);
                    }
                }
            }
        });
        return false;
    }
});

