<?php

namespace RestaurantBundle\Controller;

use DishBundle\Entity\DishCategory;
use RestaurantBundle\Entity\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class StatController
 * @package RestaurantBundle\Controller
 * @Route("/admin")
 */
class StatController extends Controller
{

    /**
     * @Route("/stat/pizza-month/update", name="stat_pizza_month_update")
     * @param Request $request
     * @return Response
     */
    public function statPizzaMonthUpdateAction(Request $request)
    {
        if ($request->get("month") != "") {
            $month = $request->get("month");
            $year = $request->get("year");
            $this->updatePizzaStat((new \DateTime($year . "-" . $month . "-01")));
        } else {
            $this->updatePizzaStat((new \DateTime('first day of this month')));
            $this->updatePizzaStat((new \DateTime('first day of last month')));
        }

        return $this->render('@Restaurant/Stat/__statPizzaMonthTable.html.twig', $this->getPizzaStat($request->get("year")));
    }

    private function updatePizzaStat(\DateTime $date)
    {
        $conn = $this->getDoctrine()->getEntityManager()->getConnection();


        $clearQuery = "delete from stat_pizza where month = " . ((int)$date->format("m")) . " and year = " . ((int)$date->format("Y"));
        $stmt = $conn->prepare($clearQuery);
        $stmt->execute();

        $updateQuery = "INSERT INTO stat_pizza (`year`, `month`, `shop`, `quantity`)
        SELECT  year(o1_.created_at) as yr, month(o1_.created_at) as mnt, `o1_`.`shop_id`, sum(`o0_`.`quantity`)
        FROM ( `order_dish` `o0_` JOIN `orders` `o1_` ON ((`o0_`.`parent_order_id` = `o1_`.`id`)) )

        WHERE ( `o0_`.`order_id` IN( SELECT `o4_`.`id` FROM `orders` `o4_`
            WHERE ( (`o4_`.`created_at` >= '" . $date->format("Y-m-d") . "') AND (`o4_`.`created_at` < '" . (new \DateTime($date->format("Y-m-d") . " first day of next month"))->format("Y-m-d") . "') AND (`o4_`.`status` = 4) ) ) AND `o0_`.`dish_id` IN( SELECT `d5_`.`id` FROM `dish` `d5_` WHERE (`d5_`.`category_id` IN(32, 82, 67,107, 147, 167, 187)) ) ) group by `o1_`.`shop_id`, mnt, yr";

//var_dump($updateQuery);die();

        $stmt = $conn->prepare($updateQuery);
        $stmt->execute();

    }

    private function getPizzaStat($year = null)
    {

        $__shops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAll();

        if (!$year) {
            $year = (new \DateTime())->format("Y");
        }

        if ($this->getParameter("is_primary")) {
            $conn = $this->getDoctrine()->getEntityManager()->getConnection();

            $stmt = $conn->prepare("SELECT * FROM stat_pizza WHERE year = ?");
            $stmt->bindValue(1, $year);
            $stmt->execute();
            $viewData = $stmt->fetchAll();
        } else {
            $ch = curl_init($this->getParameter("api_path") . $this->generateUrl("api_pizza_monthly", ['year' => $year,]));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $viewData = json_decode(curl_exec($ch), true);
        }


        $pizza = [];
        $shops = [];
        $month = [
            "JAN" => 31,
            "FEB" => 28,
            "MAR" => 31,
            "APR" => 30,
            "MAY" => 31,
            "JUN" => 30,
            "JUL" => 31,
            "AUG" => 31,
            "SEP" => 30,
            "OCT" => 31,
            "NOV" => 30,
            "DEC" => 31,
        ];

        foreach ($__shops as $shop) {
            $shops[$shop->getId()] = $shop->getName();
            foreach ($month as $key => $value) {
                $pizza[$shop->getId()][$key] = 0;
            }
        }


        foreach ($viewData as $__data) {
            $pizza[$__data["shop"]][array_keys($month)[$__data["month"] - 1]] += $__data["quantity"];
        }


        return [
            'pizza' => $pizza,
            'month' => $month,
            'shops' => $shops,
        ];
    }

    /**
     * @Route("/stat/pizza-month", name="stat_pizza_month")
     * @param Request $request
     * @return Response
     */
    public function statPizzaMonthAction(Request $request)
    {
        return $this->render('@Restaurant/Stat/statPizzaMonth.html.twig', $this->getPizzaStat($request->get("year")));
    }


    /**
     * @Route("/stat/pizza/{dateStart}/{dateEnd}", name="stat_pizza")
     */
    public function statPizzaAction(Request $request, $dateStart = "yesterday", $dateEnd = 'today')
    {
        $pizzaData = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaPerDateRange($dateStart, $dateEnd);
        $dailyReportsData = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findReportsByPeriod($dateStart, $dateEnd);

        $data = [];
        $dates = [];
        $shops = [];
        $pizza = [];
        foreach ($pizzaData as $_od) {
            $shop = $_od->getOrder()->getShop()->getName();
            $date = $_od->getOrder()->getCreatedAt()->format("Y-m-d");
            if (!in_array($shop, $shops)) $shops[] = $shop;
            if (!array_key_exists($date, $dates)) $dates[$date] = ["sell" => 0, "sell_free" => 0];

            if (!array_key_exists($shop, $pizza)) $pizza[$shop] = [];
            if (!array_key_exists($date, $pizza[$shop])) $pizza[$shop][$date] = ["sell" => 0, "sell_free" => 0];


            $data[$shop][$date]["dishes"] = $_od;
            $ingr = $_od->getDish()->getIngredients()[0];
            if ($ingr == null) {
                continue;
            }
            $quantity = ($ingr->getIngredient()->getId() == 1 ? $ingr->getQuantity() : $ingr->getQuantity() / 2) * $_od->getQuantity();

            if ($_od->getDish()->getCategory()->getId() == 32) {
                $pizza[$shop][$date]['sell'] += $quantity;
                $dates[$date]['sell'] += $quantity;
            } else {
                $pizza[$shop][$date]['sell_free'] += $quantity;
                $dates[$date]['sell_free'] += $quantity;

            }

        }

        return $this->render('@Restaurant/Stat/statPizza.html.twig', [
            'pizza' => $pizza,
            'dates' => $dates,
        ]);
    }

    /**
     * @Route("/stat/average", name="stat_average")
     */
    public function statAverageAction(Request $request)
    {
        $dataSum = $this->getDoctrine()->getRepository('OrderBundle:Order')->findAverageSum();
        $dataCount = $this->getDoctrine()->getRepository('OrderBundle:Order')->findOrderCount();
        $dataTime = $this->getDoctrine()->getRepository('OrderBundle:Order')->findAvgOrderTime();

        foreach ($dataCount as $__count) {
            $data[$__count['oSid']][$__count['oCreatedAtYear']][$__count['oCreatedAtMonth']]['count'] = $__count['oCount'];
            $data[$__count['oSid']][$__count['oCreatedAtYear']][$__count['oCreatedAtMonth']]['avgDay'] = ($__count['oCount'] / cal_days_in_month(CAL_GREGORIAN, $__count['oCreatedAtMonth'], $__count['oCreatedAtYear']));
            $data[$__count['oSid']][$__count['oCreatedAtYear']][$__count['oCreatedAtMonth']]['avgTime'] = 0;
        }

        foreach ($dataSum as $__sum) {
            $data[$__sum['oSid']][$__sum['oCreatedAtYear']][$__sum['oCreatedAtMonth']]['sum'] = $__sum['odSum'] ?: 0;
        }

        foreach ($dataTime as $__time) {
            $data[$__time['oSid']][$__time['oCreatedAtYear']][$__time['oCreatedAtMonth']]['avgTime'] = $__time['avgTime'] ?: 0;
        }

        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();
        $shopData = [];
        foreach ($shops as $__shop) {
            $shopData[$__shop->getId()] = $__shop->getName();
        }


        //dump($data);die();
        ksort($data);


        return $this->render('@Restaurant/Stat/average.html.twig', [
            'data' => $data,
            'shopData' => $shopData,
        ]);

    }

    /**
     * @Route("/stat/summary/{dateStart}/{dateEnd}", name="stat_summary", defaults={"dateStart" = null, "dateEnd" = null})
     */
    public function statSummaryAction(Request $request, $dateStart = "yesterday", $dateEnd = 'today')
    {
        $total_sum = 0;

        $ordersByHourArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findSummedByHour(null, $dateStart, $dateEnd);

        $ordersByCatArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findSummedByCat(null, $dateStart, $dateEnd);

        $resByHourArray = [];
        foreach ($ordersByHourArray as $data) {
            $resByHourArray[$data["oSid"]]["name"] = $data["oSname"];
            $resByHourArray[$data["oSid"]]["data"][$data["oCreatedAt"]] = [
                'sum' => $data["odSum"] * 1,
                "discount" => $data["odDiscount"] * 1,
                "quantity" => $data["odQuantity"] * 1,
            ];
            $total_sum += $data["odSum"] * 1;
        }

        foreach ($resByHourArray as $shop_id => $data) {
            $dataArray = $data["data"];

            $resByHourArray[$shop_id]['total'] = 0;
            $resByHourArray[$shop_id]['night'] = 0;

            for ($h = 0; $h <= 23; $h++) {
                if (!array_key_exists($h, $dataArray)) {
                    $resByHourArray[$shop_id]["data"][$h] = [
                        'sum' => 0,
                        "discount" => 0,
                    ];

                } else {
                    $resByHourArray[$shop_id]['total'] += $dataArray[$h]['sum'];
                    if (($h <= 9) || ($h >= 23)) $resByHourArray[$shop_id]['night'] += $dataArray[$h]['sum'];

                }


            }
            ksort($resByHourArray[$shop_id]["data"]);
        }

        $cats = [
            'БАР' => [],
            'КУХНЯ' => [],
        ];

        $resByCatArray = [];
        foreach ($ordersByCatArray as $data) {
            $resByCatArray[$data["oSid"]]["name"] = $data["oSname"];
            $resByCatArray[$data["oSid"]]["data"][$data["oddcpn"]][$data["oddcpId"]] = [
                'sum' => $data["odSum"] * 1,
                'discount' => $data["odDiscount"] * 1,
                "name" => $data["oddcpCat"],
                "quantity" => $data["odQuantity"] * 1,
            ];
            if (!in_array($data["oddcpId"], $cats[$data["oddcpn"]])) {
                $cats[$data["oddcpn"]][$data["oddcpId"]] = $data["oddcpCat"];
            }
        }

        foreach ($resByCatArray as $shop_id => $data) {

            $resByCatArray[$shop_id]['sum'] = 0;
            foreach ($cats as $group => $groupCat) {

                if (!array_key_exists($group, $data["data"])) {
                    $data["data"][$group] = [];
                }
                $dataArray = $data["data"][$group];

                foreach ($groupCat as $catId => $catName) {
                    if (!array_key_exists($catId, $dataArray)) {
                        $resByCatArray[$shop_id]["data"][$group][$catId] = [
                            'sum' => 0,
                            'discount' => 0,
                            "name" => $catName,
                        ];
                    }
                    $resByCatArray[$shop_id]['sum'] += $resByCatArray[$shop_id]["data"][$group][$catId]['sum'];
                }
            }
            ksort($resByCatArray[$shop_id]["data"]['БАР']);
            ksort($resByCatArray[$shop_id]["data"]['КУХНЯ']);
        }
        ksort($cats['БАР']);
        ksort($cats['КУХНЯ']);
        /*
                dump($resByHourArray);
                dump($resByCatArray);
                die();
        */

        return $this->render("@Restaurant/Orders/statSummary.html.twig", [
            'resByHourArray' => $resByHourArray,
            'resByCatArray' => $resByCatArray,
            'cats' => $cats,
            'totalSum' => $total_sum,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    /**
     * @Route("/stat/shop/{shop}/cat/{category}/{dateStart}/{dateEnd}", name="stat_summary_by_shop", defaults={"shop" = null, "cat" = null, "dateStart" = null, "dateEnd" = null})
     * @Route("/stat/cat/{category}/shop/{shop}/{dateStart}/{dateEnd}", name="stat_summary_by_cat", defaults={"shop" = null, "cat" = null, "dateStart" = null, "dateEnd" = null})
     */
    public function statByCatAndShopAction(Request $request, $shop = null, $category = null, $dateStart = "yesterday", $dateEnd = null)
    {
        $total_sum = 0;

        if ($shop) {
            if (strpos($shop, ',')) {
                $shops = explode(',', $shop);
            } else {
                $shops = $shop;
            }

            $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findById($shops);
        }

        if ($category) {
            if (strpos($category, ',')) {
                $categories = explode(',', $category);
            } else {
                $categories = $category;
            }

            $categories = $this->getDoctrine()->getRepository('DishBundle:DishCategory')->findById($categories);
        }


        if ($dateEnd == null) {
            $dateEnd = (new \DateTime($dateStart))->modify('+1 day')->format('d.m.Y');
        }

        $ordersByHourArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findSummedByHourAndCat($shops, $categories, $dateStart, $dateEnd);

        //dump($ordersByHourArray);die();

        $resByHourArray = [];
        foreach ($ordersByHourArray as $data) {
            $resByHourArray[$data["oSid"]]["name"] = $data["oSname"];
            $resByHourArray[$data["oSid"]]["data"][$data["oCreatedAt"]] = [
                'name' => $data['oSname'],
                'quantity' => $data["odQuantity"] * 1,
                'sum' => $data["odSum"] * 1,
                "discount" => $data["odDiscount"] * 1,
            ];
            $total_sum += $data["odSum"] * 1;
        }

        foreach ($resByHourArray as $shop_id => $data) {
            $dataArray = $data["data"];
            for ($h = 0; $h <= 23; $h++) {
                if (!array_key_exists($h, $dataArray)) $resByHourArray[$shop_id]["data"][$h] = [
                    'sum' => 0,
                    'quantity' => 0,
                    "discount" => 0,
                ];
            }
            ksort($resByHourArray[$shop_id]["data"]);
        }

        /* ---------------------- */

        $dishes = $this->getDoctrine()->getRepository("OrderBundle:OrderDish")->findByShopInDates($shop, $category, $dateStart, $dateEnd);

        $dishesArray = [];
        $total = [
            'sum' => 0,
            'quantity' => 0,
            'discount' => 0,
        ];
        foreach ($dishes as $index => $dishData) {
            $dish = $dishData[0];
            $dishesArray[$dish->getDish()->getId()] = [
                'name' => $dish->getDish()->getName(),
                'price' => $dishData["odDishPrice"],
                'sum' => $dishData["odDishSum"],
                'discount' => $dishData["odDishDiscount"],
                'quantity' => $dishData["odDishQuantity"],
            ];
            $total['sum'] += $dishData['odDishSum'];
            $total['quantity'] += $dishData['odDishQuantity'];
            $total['discount'] += $dishData['odDishDiscount'];
        }

        return $this->render('@Restaurant/Orders/statByCat.html.twig', [
            'shops' => $shops,
            'totalSum' => $total_sum,
            'category' => $category,
            'resByHourArray' => $resByHourArray,
            'dishes' => $dishesArray,
            'total' => $total,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd
        ]);
    }

    /**
     * @Route("/stat/mom", name="stat_mom_by_shop", defaults={"shop" = null})
     */
    public function statMoMByShopAction(Request $request, $shop = null)
    {
        $startYear = 2020;
        $currentYear = 2021;
        $monthData = $this->getDoctrine()->getRepository('OrderBundle:Order')->findDistributedByMonth($startYear);
        $data = [];
        $years = [];

        foreach ($monthData as $_data) {
            /* if (!array_key_exists('oSid', $data)) {
                 $data[$_data['oSid']] = [
                     'name' => $_data['oSname'],
                     'sum' => [],
                 ];
             }*/
            $data[$_data['oSid']]['name'] = $_data['oSname'];


            $data[$_data['oSid']]['sum'][$_data['oCreatedAtYear']][$_data['oCreatedAtMonth']] = $_data['odSum'];
            $data[$_data['oSid']]['discountSum'][$_data['oCreatedAtYear']][$_data['oCreatedAtMonth']] = $_data['odDiscount'];

            if (!in_array($_data['oCreatedAtYear'], $years)) $years[] = $_data['oCreatedAtYear'];
        }
        krsort($years);
        $sum = [];
        foreach ($data as $shopId => $shopData) {
            foreach ($shopData['sum'] as $year => $yearData) {
                foreach ($years as $__year) {
                    foreach (['sum', 'discountSum'] as $__param) {
                        if (!array_key_exists($__year, $data[$shopId][$__param])) {
                            $data[$shopId][$__param][$__year] = [$__param => 0];
                        }

                        for ($i = 1; $i <= 12; $i++) {
                            if (!array_key_exists($i, $data[$shopId][$__param][$__year]))
                                $data[$shopId][$__param][$__year][$i] = 0;
                        }
                    }
                }

                $data[$shopId]['sum'][$year]['sum'] = array_sum($data[$shopId]['sum'][$year]);
                ksort($data[$shopId]['sum'][$year]);

                $data[$shopId]['discountSum'][$year]['discountSum'] = array_sum($data[$shopId]['discountSum'][$year]);
                ksort($data[$shopId]['discountSum'][$year]);

                if (!array_key_exists($year, $sum)) $sum[$year] = 0;

                $sum[$year] += $data[$shopId]['sum'][$year]['sum'];

            }
            ksort($data[$shopId]['sum']);
            ksort($data[$shopId]['discountSum']);

        }

//        dump($data);die();

        return $this->render('@Restaurant/Stat/mom.html.twig', [
            'sum' => $sum,
            'data' => $data,
            'years' => $years,
            'startYear' => $startYear,
            'currentYear' => $currentYear,
        ]);
    }

    /**
     * @Route("/stat/shop/{shop}/{dateStart}/{dateEnd}", name="stat_by_shop", defaults={"shop" = null, "dateStart" = null, "dateEnd" = null})
     */
    public function statByShopAction(Request $request, $shop = null, $dateStart = "yesterday", $dateEnd = null)
    {
        $total_sum = 0;

        if ($shop) {
            $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find($shop);
        }
        if ($dateEnd == null) {
            $dateEnd = (new \DateTime($dateStart))->modify('+1 day')->format('d.m.Y');
        }

        $ordersByHourArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findDistributedByHour($shop, $dateStart, $dateEnd);
        $ordersByCatArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findDistributedByCat($shop, $dateStart, $dateEnd);

        $resByHourArray = [];
        foreach ($ordersByHourArray as $data) {
            $resByHourArray[$data["oCreatedAtDate"]]["data"][$data["oCreatedAtHour"]] = [
                'sum' => $data["odSum"] * 1,
                "discount" => $data["odDiscount"] * 1,
            ];
            $total_sum += $data["odSum"] * 1;
        }

        foreach ($resByHourArray as $date_id => $data) {
            $dataArray = $data["data"];
            for ($h = 0; $h <= 23; $h++) {
                if (!array_key_exists($h, $dataArray)) $resByHourArray[$date_id]["data"][$h] = [
                    'sum' => 0,
                    "discount" => 0,
                ];
            }
            ksort($resByHourArray[$date_id]["data"]);
        }

        $cats = [
            'БАР' => [],
            'КУХНЯ' => [],
        ];

        $resByCatArray = [];

        foreach ($ordersByCatArray as $data) {
            $resByCatArray[$data["oCreatedAtDate"]]["name"] = $data["oCreatedAtDate"];
            $resByCatArray[$data["oCreatedAtDate"]]["data"][$data["oddcpn"]][$data["oddcpId"]] = [
                'sum' => $data["odSum"] * 1,
                'discount' => $data["odDiscount"] * 1,
                "name" => $data["oddcpCat"],
            ];
            if (!in_array($data["oddcpId"], $cats[$data["oddcpn"]])) {
                $cats[$data["oddcpn"]][$data["oddcpId"]] = $data["oddcpCat"];
            }
        }

        foreach ($resByCatArray as $date_id => $data) {

            $resByCatArray[$date_id]['sum'] = 0;
            foreach ($cats as $group => $groupCat) {

                if (!array_key_exists($group, $data["data"])) {
                    $data["data"][$group] = [];
                }
                $dataArray = $data["data"][$group];

                foreach ($groupCat as $catId => $catName) {
                    if (!array_key_exists($catId, $dataArray)) {
                        $resByCatArray[$date_id]["data"][$group][$catId] = [
                            'sum' => 0,
                            'discount' => 0,
                            "name" => $catName,
                        ];
                    }
                    $resByCatArray[$date_id]['sum'] += $resByCatArray[$date_id]["data"][$group][$catId]['sum'];
                }
            }
            //ksort($resByCatArray[$date_id]["data"]);
            ksort($resByCatArray[$date_id]["data"]['БАР']);
            ksort($resByCatArray[$date_id]["data"]['КУХНЯ']);
        }

        return $this->render('@Restaurant/Orders/statByShop.html.twig', [
            'resByHourArray' => $resByHourArray,
            'resByCatArray' => $resByCatArray,
            'cats' => $cats,
            'totalSum' => $total_sum,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/stat/category/{category}/{dateStart}/{dateEnd}", name="stat_by_category", defaults={"category" = null, "dateStart" = null, "dateEnd" = null})
     */
    public function statByCategoryAction(Request $request, $category = null, $dateStart = "yesterday", $dateEnd = 'today')
    {
        $total_sum = 0;

        if ($category) {
            $category = $this->getDoctrine()->getRepository('DishBundle:DishCategory')->find($category);
        }
        if ($dateEnd == null) {
            $dateEnd = (new \DateTime($dateStart))->modify('+1 day')->format('d.m.Y');
        }
        $resByHourArray = [];
        $ordersByHourArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findDistributedByHourByCategory($category, $dateStart, $dateEnd);
//dump($ordersByHourArray);die();
        foreach ($ordersByHourArray as $data) {
            $resByHourArray[$data["oSid"]]["name"] = $data["oSname"];
            $resByHourArray[$data["oSid"]]["data"][$data["oCreatedAtHour"]] = [
                'sum' => $data["odSum"] * 1,
                "discount" => $data["odDiscount"] * 1,
                "quantity" => $data["odQuantity"] * 1,

            ];
            $total_sum += $data["odSum"] * 1;
        }

        foreach ($resByHourArray as $shop_id => $data) {
            $dataArray = $data["data"];

            $resByHourArray[$shop_id]['total'] = 0;
            $resByHourArray[$shop_id]['night'] = 0;

            for ($h = 0; $h <= 23; $h++) {
                if (!array_key_exists($h, $dataArray)) {
                    $resByHourArray[$shop_id]["data"][$h] = [
                        'sum' => 0,
                        "discount" => 0,
                        "quantity" => 0,
                    ];

                } else {
                    $resByHourArray[$shop_id]['total'] += $dataArray[$h]['sum'];
                    if (($h <= 9) || ($h >= 23)) $resByHourArray[$shop_id]['night'] += $dataArray[$h]['sum'];

                }


            }
            ksort($resByHourArray);
            ksort($resByHourArray[$shop_id]["data"]);
        }


        return $this->render('@Restaurant/Orders/statByCat.html.twig', [
            'resByHourArray' => $resByHourArray,
            'totalSum' => $total_sum,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'category' => $category,
        ]);
    }


    /**
     * @Route("/order/stat/shop/{shop}/{dateStart}/{dateEnd}", name="order_shop_summary", defaults={"dateStart" = null, "dateEnd" = null})
     * @ParamConverter("shop", class="RestaurantBundle:Shop", options={"id" = "shop"})
     */
    public function dishesInShop(Request $request, Shop $shop, $dateStart = "yesterday", $dateEnd = 'today')
    {
        /*
        if ($dateStart) {
            $dateStart = new \DateTime($dateStart);
        } else {
            $dateStart = new \DateTime("yesterday");
        }
        if ($dateEnd) {
            $dateEnd = new \DateTime($dateEnd);
        } else {
            $dateEnd = new \DateTime("today");
        }
        */

        switch ($dateStart) {
            default:
                $dateStart = new \DateTime($dateStart);
        }

        switch ($dateEnd) {
            default:
                $dateEnd = new \DateTime($dateEnd);
        }
        $total_sum = 0;

        $ordersByHourArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findDistributedByHour($shop, $dateStart, $dateEnd);
        $ordersByCatArray = $this->getDoctrine()->getRepository("OrderBundle:Order")->findDistributedByCat($shop, $dateStart, $dateEnd);


        /*     dump($ordersByHourArray);
             dump($ordersByCatArray);
             dump($dateStart);
             dump($dateEnd);

             die();*/

        $resByHourArray = [];
        foreach ($ordersByHourArray as $data) {
            $resByHourArray[$data["oCreatedAtDate"]]["data"][$data["oCreatedAtHour"]] = [
                'sum' => $data["odSum"] * 1,
                "discount" => $data["odDiscount"] * 1,
            ];
            $total_sum += $data["odSum"] * 1;
        }

        foreach ($resByHourArray as $date_id => $data) {
            $dataArray = $data["data"];
            for ($h = 0; $h <= 23; $h++) {
                if (!array_key_exists($h, $dataArray)) $resByHourArray[$date_id]["data"][$h] = [
                    'sum' => 0,
                    "discount" => 0,
                ];
            }
            ksort($resByHourArray[$date_id]["data"]);
        }


        $cats = [];

        $resByCatArray = [];
        foreach ($ordersByCatArray as $data) {

            $resByCatArray[$data["oCreatedAtDate"]][$data["oddcpId"]] = [
                'name' => $data["oddcpCat"],
                'sum' => $data["odSum"] * 1,
                "discount" => $data["odDiscount"] * 1,
            ];
            if (!in_array($data["oddcpId"], $cats)) {
                $cats[$data["oddcpId"]] = $data["oddcpCat"];
            }
        }


        foreach ($resByCatArray as $date => $data) {
            foreach ($cats as $catId => $catName) {
                if (!array_key_exists($catId, $data)) {
                    $resByCatArray[$date][$catId] = [
                        'name' => $catName,
                        'sum' => 0,
                        "discount" => 0,
                    ];
                }
            }
            ksort($resByCatArray[$date]);
        }
        /*
                dump($resByHourArray);
                dump($resByCatArray);
                die();
        */

        return $this->render("@Restaurant/Orders/ordersShopSummary.html.twig", [
            'resByHourArray' => $resByHourArray,
            'resByCatArray' => $resByCatArray,
            'cats' => $cats,
            'totalSum' => $total_sum,
            'shop' => $shop,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,

        ]);
    }


    /**
     * @Route("/order/stat/detailed/{shop}/{cat}/{dateStart}/{dateEnd}", name="order_list_shop_cat", defaults={"dateStart" = null, "dateEnd" = null})
     * @ParamConverter("shop", class="RestaurantBundle:Shop", options={"id" = "shop"})
     * @ParamConverter("category",  class="DishBundle:DishCategory", options={"id" = "cat"})
     */
    public function dishesInShopInCategory(Request $request, Shop $shop, DishCategory $category, $dateStart = null, $dateEnd = null)
    {

        $dishes = $this->getDoctrine()->getRepository("OrderBundle:OrderDish")->findByShopInDates($shop, $category, $dateStart, $dateEnd);
        dump($dishes);
        die();
        //dump($dishes);die();
//dump($dishes); die();
        $dishesArray = [];
        $total = [
            'sum' => 0,
            'quantity' => 0,
            'discount' => 0,
        ];
        foreach ($dishes as $index => $dishData) {
            $dish = $dishData[0];
            $dishesArray[$dish->getDish()->getId()] = [
                'name' => $dish->getDish()->getName(),
                'price' => $dishData["odDishPrice"],
                'sum' => $dishData["odDishSum"],
                'discount' => $dishData["odDishDiscount"],
                'quantity' => $dishData["odDishQuantity"],
            ];
            $total['sum'] += $dishData['odDishSum'];
            $total['quantity'] += $dishData['odDishQuantity'];
            $total['discount'] += $dishData['odDishDiscount'];
        }

        return $this->render("@Restaurant/Orders/ordersDetailed.html.twig", [
            'dishes' => $dishesArray,
            'shop' => $shop,
            'category' => $category,
            'total' => $total,
        ]);
    }


}
