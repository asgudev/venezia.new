<?php
namespace DishBundle\Entity\Repository;

use DishBundle\Entity\Dish;
use DishBundle\Entity\DishMeta;
use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;

class DishMetaRepository extends EntityRepository
{
    /**
     * @param Dish $dish
     * @param Shop $shop
     * @return DishMeta
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByDishAndShop(Dish $dish, Shop $shop)
    {
        $dishMeta = $this->createQueryBuilder('dm')
            ->andWhere("dm.shop = :shop")
            ->andWhere("dm.dish = :dish")
            ->setParameters([
                'shop' => $shop,
                'dish' => $dish,
            ])
            ->getQuery()
            ->getSingleResult();
        return $dishMeta;
    }
}