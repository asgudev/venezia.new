<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class SendMailPizzaPhotoRevisionCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('photo:revision:daily')
            ->setDescription('Send daily pizza photo to check');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {

            $date = '-1 day';
            $doctrine = $this->getContainer()->get('doctrine');
            $renderer = $this->getContainer()->get('templating');

            $toValidation = $doctrine->getRepository('ControlBundle:DishPhoto')->findBy([
                'isValidated' => false,
            ]);

            $mail = $renderer->render("@Restaurant/Custom/pizzaPhotoMail.html.twig", [
                'toValidation' => $toValidation,
            ]);

            $message = \Swift_Message::newInstance()
                ->setSubject('Daily Pizza Photo Revision ' . (new \DateTime($date))->format('d.m.Y'))
                ->setFrom('send@italbrest.com')
                ->setTo([
                    'margo@italbrest.com',
                    'natasha@italbrest.com',
                ])
                ->setBody(
                    $mail,
                    'text/html'
                );
            $this->getContainer()->get('mailer')->send($message);


        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}
