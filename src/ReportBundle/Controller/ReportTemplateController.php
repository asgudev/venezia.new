<?php

namespace ReportBundle\Controller;

use ReportBundle\Entity\ReportElement;
use ReportBundle\Entity\ReportTemplate;
use ReportBundle\Form\Type\ReportTemplateType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @Route("/admin/report")
 */
class ReportTemplateController extends Controller
{

    /**
     * @Route("/template/add", name="report_template_create")
     */
    public function createReportTemplateAction(Request $request)
    {
        $reportTemplate = new ReportTemplate();

        $form = $this->createForm(ReportTemplateType::class, $reportTemplate, [

        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $__fields = $reportTemplate->getFields();
            $reportTemplate->removeAllFields();

            foreach ($__fields as $__field) {
                $reportTemplate->addField($__field["name"]);
            }

            $em = $this->getDoctrine()->getManager();

            $em->persist($reportTemplate);
            $em->flush();

            return $this->redirectToRoute('report_template_edit', [
                'id' => $reportTemplate->getId(),
            ]);
        }

        return $this->render('@Report/ReportTemplate/editReportTemplate.html.twig', [
            'form' => $form->createView(),
        ]);
    }




    /**
     * @Route("/template/{id}/edit", name="report_template_edit")
     */
    public function editReportTemplateAction(Request $request, ReportTemplate $reportTemplate)
    {
        if (!$reportTemplate)
            throw new NotFoundHttpException();


        $form = $this->createForm(ReportTemplateType::class, $reportTemplate, [

        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($reportTemplate);
            $em->flush();

            return $this->redirectToRoute('report_template_edit', [
                'id' => $reportTemplate->getId(),
            ]);
        }

        return $this->render('@Report/ReportTemplate/editReportTemplate.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
