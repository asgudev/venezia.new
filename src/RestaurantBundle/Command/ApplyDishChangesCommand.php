<?php

namespace RestaurantBundle\Command;

use DishBundle\Entity\DishHistory;
use DishBundle\Entity\DishMeta;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ApplyDishChangesCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('restaurant:changes:apply');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        /** @var DishHistory[] $changes */
        $changes = $doctrine->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_CONFIRMED, 'ASC');

        try {
            $changesCount = 0;

            foreach ($changes as $__change) {
var_dump($__change->getId());
                $changesCount += 1;
                $dish = $__change->getDish();
//dump($__change);die();
                switch ($__change->getField()) {
                    case 'name':
                        $dish->setName($__change->getValue());
                        $em->persist($dish);
                        break;
                    case 'base_price':
                        $dish->setPrice($__change->getValue());
                        foreach ($dish->getDishMetas() as $dishMeta) {
                            $dish->setPrice($__change->getValue());
                            if ($dishMeta->getPrice() == $__change->getValue()) {
                                $dishMeta->setPrice($__change->getValue());
                                $em->persist($dishMeta);
                            }
                        }
                        break;
                    case 'price':
                        if ($__change->getShop() == null) {
                            var_dump($__change->getId());die();
                        }
                        $dishMeta = $dish->getShopMeta($__change->getShop());
                        if ($dishMeta) {
                            $dishMeta->setPrice($__change->getValue());
                            if (!$dish->hasShop($__change->getShop()))
                                $dish->addShop($__change->getShop());
                        } else {
                            $dishMeta = new DishMeta();
                            $dishMeta->setDish($dish)
                                ->setShop($__change->getShop())
                                ->setPrice($__change->getValue());
                            if (!$dish->hasShop($__change->getShop()))
                                $dish->addShop($__change->getShop());
                        }
                        $em->persist($dishMeta);
                }
                $__change->setStatus(DishHistory::STATUS_UPDATED);
                $__change->setUpdatedAt(new \DateTime());
                $em->persist($__change);
                $em->flush();
            }
            if ($changesCount > 0) {
//                var_dump(exec("/home/syncer/menu.sh &"));
            }

        } catch (\Exception $em) {
            dump($em);
            $output->write('failed');
        }
    }
}
