VNZ = window.VNZ || {};

$(function () {
    VNZ.detect = function () {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $("body").addClass("on-mobile");
        }
    };
    VNZ.detect();
});
;VNZ = window.VNZ || {};


$(function () {
    VNZ.sliderInstance = function (el) {
        this.$slider = $(el);

        if (!this.$slider.length) {
            return {};
        }

        this.transitionTime = 600;

        this.textClass = ".slide-text-block";

        this.$slidesUL = this.$slider.find(".slides");
        this.$slides = this.$slider.find(".slide");

        this.$goNext = this.$slider.find(".next");
        this.$goPrev = this.$slider.find(".prev");

        this.$sliderNav = this.$slider.find(".slides-nav");

        this.hideTexts = function (index) {
            this.$slides.eq(index).addClass("current");
            this.$slides.not(".current").find(this.textClass).css({
                opacity: 0,
                bottom: "-600px"
            });
            this.$slides.eq(index).removeClass("current");
        };

        this.goPrev = function () {
            if (this.$slidesUL.is(":animated")) {
                return false;
            }

            var $lastItem = this.$slides.last().clone();
            this.$slidesUL.prepend($lastItem);
            this.$slides = this.$slider.find(".slide");

            this.$slidesUL.css({
                marginLeft: ((-1) * VNZ.gridWidth * 2) + (($(window).width() - VNZ.gridWidth) / 2) + "px"
            });

            this.$slides.find(this.textClass).stop();
            this.$slides.eq(1).find(this.textClass).animate(
                {
                    opacity: 1,
                    bottom: "-128px"
                },
                this.transitionTime + 100,
                "easeOutQuint",
                $.proxy(function () {
                    this.hideTexts(1);
                }, this)
            );

            this.$slidesUL.cssAnimate({
                    marginLeft: ((-1) * VNZ.gridWidth) + (($(window).width() - VNZ.gridWidth) / 2) + "px"
                },
                this.animationTimeout,
                "easeOutQuint",
                $.proxy(function () {
                    this.$slides.last().remove();
                    var top = $(document).scrollTop();
                    $("html, body").animate({
                        scrollTop: top + 1 + "px"
                    }, 0);
                    $("html, body").animate({
                        scrollTop: top + "px"
                    }, 0);
                }, this));
        };

        this.goNext = function () {
            if (this.$slidesUL.is(":animated")) {
                return false;
            }

            var $lastItem = this.$slides.first().clone();
            this.$slidesUL.append($lastItem);

            this.$slides.find(this.textClass).stop();
            this.$slides.eq(2).find(this.textClass).css({
                opacity: 0,
                bottom: "-600px"
            });
            this.$slides.eq(2).find(this.textClass).animate(
                {
                    opacity: 1,
                    bottom: "-128px"
                },
                this.transitionTime + 100,
                "easeOutQuint",
                $.proxy(function () {
                    this.hideTexts(1);
                    $(".slide-hover").trigger("mousemove");
                }, this)
            );

            this.$slidesUL.cssAnimate(
                {
                    marginLeft: ((-1) * VNZ.gridWidth * 2) + (($(window).width() - VNZ.gridWidth) / 2) + "px"
                },
                this.animationTimeout,
                "easeOutQuint",
                $.proxy(function () {
                    this.$slides.first().remove();
                    this.$slides = this.$slider.find(".slide");

                    this.$slidesUL.css({
                        marginLeft: ((-1) * VNZ.gridWidth) + (($(window).width() - VNZ.gridWidth) / 2) + "px"
                    });

                    var top = $(document).scrollTop();
                    $("html, body").animate({
                        scrollTop: top + 1 + "px"
                    }, 0);
                    $("html, body").animate({
                        scrollTop: top + "px"
                    }, 0);
                }, this)
            );
        };

        this.$goNext.on("click", $.proxy(this.goNext, this));
        this.$goPrev.on("click", $.proxy(this.goPrev, this));

        this.slideClick = function (e) {
            if ($(window).width() / 2 < e.pageX) {
                this.goNext();
            } else {
                this.goPrev();
            }
        };
        $(this.$slidesUL).on("click", this.$slides, $.proxy(this.slideClick, this));

        this.resize = function () {
            this.$slides.css({
                width: VNZ.gridWidth + "px"
            });

            this.$sliderNav.css({
                width: VNZ.gridWidth + "px",
                marginLeft: (-1) * VNZ.gridWidth / 2 + "px"
            });

            var width = this.$slidesUL.width();
            var offset = VNZ.gridWidth;

            this.$slidesUL.css({
                marginLeft: ((-1) * offset) + (($(window).width() - VNZ.gridWidth) / 2) + "px"
            });
        };

        // Initialization

        // Last to first
        var $lastItem = this.$slides.last().clone();
        this.$slides.last().remove();
        this.$slidesUL.prepend($lastItem);

        this.$slides = this.$slider.find(".slide");

        this.hideTexts(1);

        $(window).on("resize", $.proxy(this.resize, this));
        this.resize();
    };
    VNZ.sliders = (function () {
        $(".slider-grid").each(function (i, el) {
            new VNZ.sliderInstance(el);
        });
    })();

    VNZ.sliderAsIsInstance = function (el) {
        this.$slider = $(el);

        if (!this.$slider.length) {
            return {};
        }

        this.transitionTime = 600;

        this.textClass = ".slide-text-block";

        this.$slidesUL = this.$slider.find(".slides");
        this.$slides = this.$slider.find(".slide");

        this.$goNext = this.$slider.find(".next");
        this.$goPrev = this.$slider.find(".prev");

        this.slidesOffsetSize = this.$slides.length;
        this.$slidesUL.prepend(this.$slides.clone());

        if (this.$slides.length < 4) {
            this.$slidesUL.append(this.$slides.clone());
        }

        this.$slidesUL = this.$slider.find(".slides");
        this.$slides = this.$slider.find(".slide");

        this.hideTexts = function (index) {
            index = index + this.slidesOffsetSize;

            this.$slides.eq(index).addClass("current");
            this.$slides.not(".current").find(this.textClass).stop(true, true).animate({
                opacity: 0,
                bottom: "-600px"
            });
            this.$slides.eq(index).removeClass("current");
        };

        this.resize = function () {
            var width = 0;
            for (var i = 0; i < this.slidesOffsetSize + 1; i++) {
                width = width + this.$slides.eq(i).width();
            }

            this.$slidesUL.css({
                marginLeft: ($(window).width() / 2 - width - this.$slides.eq(1).width() / 2) + "px"
            });
        };

        this.init = function () {
            var $lastItem = this.$slides.last().clone();
            this.$slides.last().remove();
            this.$slidesUL.prepend($lastItem);
            this.$slides = this.$slider.find(".slide");

            this.$slides.eq(0).show().css("opacity", 1);

            this.hideTexts(1);

            $(window).on("resize", $.proxy(this.resize, this));
            this.resize();
        };

        this.goPrev = function () {
            if (this.$slidesUL.is(":animated")) {
                return false;
            }

            var $lastItem = this.$slides.last().clone();
            this.$slides.last().remove();
            this.$slidesUL.prepend($lastItem);

            this.$slides = this.$slider.find(".slide");

            var width = 0;
            for (var i = 0; i < this.slidesOffsetSize + 2; i++) {
                width = width + this.$slides.eq(i).width();
            }

            this.$slidesUL.css({
                marginLeft: ($(window).width() / 2 - width - this.$slides.eq(2).width() / 2) + "px"
            });

            this.$slides.find(this.textClass).stop();
            this.$slides.eq(this.slidesOffsetSize + 1).find(this.textClass).css({
                opacity: 0,
                bottom: "-600px"
            });
            this.$slides.eq(this.slidesOffsetSize + 1).find(this.textClass).animate(
                {
                    opacity: 1,
                    bottom: "-128px"
                },
                this.transitionTime + 100,
                "easeOutQuint",
                $.proxy(function () {
                    this.hideTexts(1);
                }, this)
            );

            var width = 0;
            for (var i = 0; i < this.slidesOffsetSize + 1; i++) {
                width = width + this.$slides.eq(i).width();
            }

            this.$slidesUL.animate(
                {
                    marginLeft: ($(window).width() / 2 - width - this.$slides.eq(1).width() / 2) + "px"
                },
                this.animationTimeout,
                "easeOutQuint",
                $.proxy(function () {
                    var top = $(document).scrollTop();
                    $("html, body").animate({
                        scrollTop: top + 1 + "px"
                    }, 0);
                    $("html, body").animate({
                        scrollTop: top + "px"
                    }, 0);
                }, this)
            );
        };

        this.goNext = function () {
            if (this.$slidesUL.is(":animated")) {
                return false;
            }

            var $lastItem = this.$slides.first().clone();
            this.$slidesUL.append($lastItem);
            this.$slides = this.$slider.find(".slide");

            this.$slides.find(this.textClass).stop();
            this.$slides.eq(this.slidesOffsetSize + 2).find(this.textClass).css({
                opacity: 0,
                bottom: "-600px"
            });
            this.$slides.eq(this.slidesOffsetSize + 2).find(this.textClass).animate(
                {
                    opacity: 1,
                    bottom: "-128px"
                },
                this.transitionTime + 100,
                "easeOutQuint",
                $.proxy(function () {
                    this.hideTexts(1);
                }, this)
            );

            var width = 0;
            for (var i = 0; i < this.slidesOffsetSize + 1; i++) {
                width = width + this.$slides.eq(i).width();
            }

            this.$slidesUL.animate(
                {
                    marginLeft: ($(window).width() / 2 - width - this.$slides.eq(1).width() - this.$slides.eq(2).width() / 2) + "px"
                },
                this.animationTimeout,
                "easeOutQuint",
                $.proxy(function () {
                    this.$slides.first().remove();
                    this.$slides = this.$slider.find(".slide");

                    var width = 0;
                    for (var i = 0; i < this.slidesOffsetSize + 1; i++) {
                        width = width + this.$slides.eq(i).width();
                    }

                    this.$slidesUL.css({
                        marginLeft: ($(window).width() / 2 - width - this.$slides.eq(1).width() / 2) + "px"
                    });

                    var top = $(document).scrollTop();
                    $("html, body").animate({
                        scrollTop: top + 1 + "px"
                    }, 0);
                    $("html, body").animate({
                        scrollTop: top + "px"
                    }, 0);
                }, this)
            );
        };

        // Go!
        this.$goNext.on("click", $.proxy(this.goNext, this));
        this.$goPrev.on("click", $.proxy(this.goPrev, this));

        this.slideClick = function (e) {
            if ($(window).width() / 2 < e.pageX) {
                this.goNext();
            } else {
                this.goPrev();
            }
        };
        $(this.$slidesUL).on("click", this.$slides, $.proxy(this.slideClick, this));

        var images = [];
        this.progress = 0;
        this.$slides.each($.proxy(function (i, el) {
            var src = $(el).data("src");

            images.push(new Image());
            images[images.length - 1].onload = $.proxy(function () {
                var height = this.image.height;
                var width = this.image.width * (450 / height);

                $(this.slide).css({
                    width: width + "px"
                });

                $(this.slide).append("<img src='" + this.src + "' />");
                $(this.slide).addClass("loaded")

                this.sliderObject.progress++;
                if (this.sliderObject.progress == this.sliderObject.$slides.length) {
                    this.sliderObject.init();
                }
                ;
            }, {sliderObject: this, slide: el, src: src, image: images[images.length - 1]});
            images[images.length - 1].src = src;
        }, this));

        return this;
    };
    VNZ.slidersAsIs = (function () {
        $(".slider-asis").each(function (i, el) {
            new VNZ.sliderAsIsInstance(el);
        });
    })();

    VNZ.sliderKinfolk = (function () {
        this.$slides = $(".block-slider-kinfolk .slides .slide");

        if (!this.$slides.length) {
            return false;
        }

        this.swiper = false;
        this.initSwiper = function () {
            this.swiper = new Swiper('.swiper-container', {
                slidesPerView: "auto",
                spaceBetween: 0,
                freeMode: true,
                freeModeMomentumBounce: false,
                loop: true,
                loopedSlides: 10,
                grabCursor: true,
//                nextButton: $(".block-slider-kinfolk .btn-next"),
//                prevButton: $(".block-slider-kinfolk .btn-prev")
            });
            this.swiper.slideTo(0, 0);
        };
        this.initSwiper();

        this.$slidesWrap = $(".block-slider-kinfolk .slides");

        this.$slides.on("mousemove", function () {
            $(this).addClass("hover");
        });
        this.$slides.on("mouseleave", function () {
            $(this).removeClass("hover");
        });

        this.$prevBtn = $(".block-slider-kinfolk .btn-prev");
        this.$nextBtn = $(".block-slider-kinfolk .btn-next");

        this.$prevBtn.on("click", $.proxy(function () {
            this.swiper.slidePrev();
        }, this));
        this.$nextBtn.on("click", $.proxy(function () {
            this.swiper.slideNext();
        }, this));


        this.autoScroll = function () {
            if (this.$slides.hasClass("hover")) {
                return true;
            }
            this.$nextBtn.trigger("click");
        };
        this.scrollInterval = setInterval($.proxy(this.autoScroll, this), 5000);

//        this.buildScrollbar = function() {
//            $(".scrollyeah").scrollyeah({
//                centerIfFit: true
//            });
//            $(window).resize();
//        };

//        var counter = 0;
//        this.$slides.each(function(i, el) {
//            var img = new Image;
//            img.onload = function() {
//                counter++;
//
//                if(counter == this.$slides.length) {
//                    setTimeout($.proxy(function() {
//                        this.buildScrollbar();
//                    }, this), 500);
//                }
//
//                $(el).find(".slide-content").css({
//                    width: 800 + "px",
//                    height: 480 + "px"
//                });
//
//                $(el).animate({
//                    opacity: 1
//                }, 1000);
//                $(".scrollyeah").scrollyeah();
//            }(el, this.$slides, img);
//            img.src = $(el).find("img").attr("src");
//        });


//
//        this.$scroller = $(".block-slider-kinfolk .scrollyeah__shaft");

        $(window).on("resize", $.proxy(function () {
//            if ( $(window).width() > this.$scroller.width() ) {
//                this.$prevBtn.hide();
//                this.$nextBtn.hide();
//            } else {
//                this.$prevBtn.show();
//                this.$nextBtn.show();
//            };

            this.$slides = $(".block-slider-kinfolk .slides .slide");

            if ($(window).height() < 800) {
                var coef = 800 / $(window).height();
                if (coef > 1.4) {
                    coef = 1.4;
                }

                var fontSize = Math.floor(30 / 1.6);

                var width = Math.floor(800 / coef);
                var height = Math.floor(500 / coef);

                if (width % 2 !== 0) {
                    width++;
                }
                if (height % 2 !== 0) {
                    height++;
                }

                this.$slides.css({
                    width: width + "px",
                    height: height + "px",
                    maxWidth: width + "px"
                });
                this.$slides.find(".title").css({
                    fontSize: fontSize + "px"
                });
                this.$slidesWrap.css({
                    height: height + "px"
                });
            } else {
                this.$slides.css({
                    width: "800px",
                    height: "500px",
                    maxWidth: "800px"
                });
                this.$slidesWrap.css({
                    height: "500px"
                });
                this.$slides.find(".title").css({
                    fontSize: "30px"
                });
            }

            this.$slides.find(".slide-content").css({
                width: this.$slides.width() + "px",
                height: this.$slides.height() + "px"
            });

            this.swiper.update();
            //this.initSwiper();
        }, this));
        $(window).trigger("resize");
    })();

    VNZ.sliderHover = (function () {
        $(".slides-hover")
            .on("mousemove", function (e) {
                var left = e.clientX;

                $(this).find(".slide-hover").each(function (i, el) {
//                    console.log([i, $(el).offset().left, left])
                    if (
                        $(el).offset().left < left &&
                        $(el).offset().left + $(el).width() > left
                    ) {
                        $(el).addClass("hover");
                    } else {
                        $(el).removeClass("hover");
                    }
                });
            })
            .on("mouseleave", function () {
                $(".slide-hover").removeClass("hover");
            });
    })();
});
;VNZ = window.VNZ || {};

$(function () {
    VNZ.placesAlbum = function () {
        this.$popup = $(".block-restaurant-album");

        if (!this.$popup.length) {
            return false;
        }


        this.$slides = false;

        this.currentSlide = 0;

        this.open = function (id) {
            // if ($("body").hasClass("on-mobile")) {
            //     if (!$("body").hasClass('landing')) {
            //         return true;
            //     }
            //
            // }

            if (id === 0) {
                return true;
            }

            VNZ.preloadBackgroundsDelayed(".preload-background-delayed-restaurant-album");

            this.$slides.eq(this.currentSlide)
                .css({
                    opacity: "0",
                    left: "-100%"
                });
            this.$slides.eq(this.currentSlide).find(".block")
                .css({
                    opacity: "0",
                    left: "-100%"
                });

            this.currentSlide = this.$slides.filter(".popup-restaurant-" + id).index();

            this.$slides.eq(this.currentSlide)
//                .css({
//                    opacity: "1",
//                    left: "0"
//                });
//            this.$slides.eq(this.currentSlide).find(".block")
//                .css({
//                    opacity: "1",
//                    left: "0"
//                });

            this.$slides.eq(this.currentSlide)
                .css({
                    opacity: "1",
                    left: "0",
                    zIndex: "900"
                });
            this.$slides.eq(this.currentSlide).find(".block")
                .css({
                    opacity: "1",
                    left: "0"
                });

            this.$popup.addClass("visible");
        };

        this.bind = function () {
            // if ($("body").hasClass("on-mobile")) {
            //     if (!$("body").hasClass('landing')) {
            //         return true;
            //     }
            // }


            this.$slides = this.$popup.find(".restaurant-page");
            this.$list = $(".block-restaurants-list-album-trigger li, .block-restaurants-list-album-trigger .restaurants-list-album-trigger");

            this.$list.on("click", $.proxy(function (e) {
                e.originalEvent.stopPropagation();
                e.originalEvent.preventDefault();

                var id = $(e.currentTarget).data("id");

                this.open(id);

                return false;
            }, this));

            this.$popup.find(".btn-close").on("click", $.proxy(function () {
                this.$popup.addClass("close");

                setTimeout($.proxy(function () {
                    this.$popup.removeClass("visible").removeClass("close");
                }, this), 450);
            }, this));

            this.isAnimated = false;

            $(".block-restaurant-selector .view-as-album").on("click", $.proxy(function (e) {
                e.preventDefault();
                e.stopPropagation();

                this.open();
            }, this));
        };
        this.bindNextPrev = function () {
            this.$popup.find(".btn-next").on("click", $.proxy(function () {
                if (this.isAnimated) {
                    return false;
                }

                this.isAnimated = true;

                this.nextSlide = this.currentSlide + 1;
                if (this.nextSlide > this.$slides.length - 1) {
                    this.nextSlide = 0;
                }


                this.$slides.eq(this.nextSlide)
                    .css({
                        opacity: "0",
                        left: "0",
                        zIndex: "900"
                    })
                    .animate({
                        opacity: "1"
                    });
                this.$slides.eq(this.nextSlide).find(".block")
                    .css({
                        opacity: "0",
                        left: "0"
                    })
                    .animate({
                        opacity: "1"
                    });

                this.$slides.eq(this.currentSlide)
                    .css({
                        left: "0",
                        opacity: "1"
                    })
                    .delay(200)
                    .animate({
                            left: "-100%",
                            opacity: "0"
                        }, 800, "easeInSine",
                        $.proxy(function () {
                            this.that.isAnimated = false;
                            //$(this.el).css("zIndex", "100");
                        }, {el: this.$slides.eq(this.currentSlide), that: this}));
                this.$slides.eq(this.currentSlide).find(".block")
                    .css({
                        left: "0",
                        opacity: "1"
                    })
                    .animate({
                        left: "-50%",
                        opacity: "0"
                    }, 900, "easeInSine");

                this.currentSlide++;

                if (this.currentSlide > this.$slides.length - 1) {
                    this.currentSlide = 0;
                }

            }, this));
            this.$popup.find(".btn-prev").on("click", $.proxy(function () {
                if (this.isAnimated) {
                    return false;
                }

                this.isAnimated = true;

                this.nextSlide = this.currentSlide - 1;
                if (this.nextSlide < 0) {
                    this.nextSlide = this.$slides.length - 1;
                }

                this.$slides.eq(this.nextSlide)
                    .css({
                        opacity: "0",
                        left: "0",
                        zIndex: "900"
                    })
                    .animate({
                        opacity: "1"
                    });
                this.$slides.eq(this.nextSlide).find(".block")
                    .css({
                        opacity: "0",
                        left: "0"
                    })
                    .animate({
                        opacity: "1"
                    });

                this.$slides.eq(this.currentSlide)
                    .css({
                        left: "0",
                        opacity: "1",
                        zIndex: "900"
                    })
                    .delay(200)
                    .animate({
                            left: "100%",
                            opacity: "0"
                        }, 800, "easeInSine",
                        $.proxy(function () {
                            //$(this.el).css("zIndex", "100");
                            this.that.isAnimated = false;
                        }, {el: this.$slides.eq(this.currentSlide), that: this}));
                this.$slides.eq(this.currentSlide).find(".block")
                    .css({
                        left: "0",
                        opacity: "1"
                    })
                    .animate({
                        left: "50%",
                        opacity: "0"
                    }, 900, "easeInSine");

                this.currentSlide--;

                if (this.currentSlide < 0) {
                    this.currentSlide = this.$slides.length - 1;
                }
            }, this));
        };

        this.bind();
        this.bindNextPrev();

        return this;
    };

    VNZ.popupPlace = function (url) {
        this.$popup = $(".block-restaurant-popup");
        this.showPopup = function (data) {
            this.$popup.html(data);
            this.$popup.addClass("visible");

            this.bind();
            VNZ.preloadBackgrounds();
        };

        this.bind = function () {
            this.$popup.find(".btn-close").on("click", $.proxy(function () {
                this.$popup.addClass("close");

                setTimeout($.proxy(function () {
                    this.$popup.removeClass("visible").removeClass("close");
                    //this.$popup.html("");
                }, this), 450);
            }, this));
        };

        $.ajax({
            type: "GET",
            url: url,
            dataType: "html",
            cache: false,
            success: $.proxy(this.showPopup, this)
        });
    };

//    VNZ.placesPopup = (function() {
//        this.$list = $(".block-restaurants-list li");
//
//        if( ! this.$list.length ) {
//            return false;
//        };
//
//        this.$list.on("click", function(e) {
//            e.stopPropagation();
//            e.preventDefault();
//
//            var place = VNZ.popupPljace($(this).data("href"));
//
//            return false;
//        });
//    })();
    //VNZ.popupPlace("restaurant/BUONO");

    VNZ.placesMap = function () {
        this.map = false;

        this.$map = $("#places-map");
        if (!this.$map.length) {
            return false;
        }

        this.mapReady = function () {

        };

        this.loadMap = function (map) {
            this.mapReady = function () {
                this.map = map;

                var places = VNZ.places;
                var markers = [];

                for (var i = 0; i < places.length - 1; i++) {
                    var image = new google.maps.MarkerImage(places[i][2], new google.maps.length(40, 45));
                    image.anchor = new google.maps.Point(20, 45);

                    var l = new google.maps.LatLng(places[i][0], places[i][1]);
                    markers.push(new google.maps.Marker({
                        position: l,
                        map: this.map,
                        icon: image,
                        data_id: places[i][3],
                        data_network: places[i][4]
                    }));

                    google.maps.event.addListener(markers[markers.length - 1], 'click', function () {
                        if (this.data_id !== 0) {
                            VNZ.PlacesAlbums.open(this.data_id);
                        }
                        ;
                    });

                    if (places[i][4]) {
                        markers[markers.length - 1].setVisible(false);
                    }
                }

                google.maps.event.trigger(this.map, 'resize');
                this.map.setCenter(new google.maps.LatLng(VNZ.city.lat, VNZ.city.lon));

                VNZ.placesMarkers = markers;
            };
            VNZ.PlacesMap.mapReady();
        };

        $(".places-map-toggler").on("click", $.proxy(function () {
            var state = !$(".places-map-toggler").data("state");
            $(".places-map-toggler").data("state", state);

            $.each(VNZ.placesMarkers, function (i, el) {
                if (el.data_network) {
                    el.setVisible(!!state);
                }
            });
        }));

        $(".tabs").on("click", $.proxy(function () {
            google.maps.event.trigger(this.map, "resize");
            this.map.setCenter(new google.maps.LatLng(VNZ.city.lat, VNZ.city.lon));
        }, this));

        VNZ.mapsLoader = new VNZ.MapsLoader("places-map", $.proxy(this.loadMap, this));

        return this;
    };

    VNZ.mapContacts = function () {
        this.map = false

        this.$map = $("#google-map-contacts");
        if (!this.$map.length) {
            return false;
        }

        this.lat = this.$map.data("lat");
        this.lng = this.$map.data("lng");
        this.icon = this.$map.data("icon");

        this.loadMap = function (map) {
            this.map = map;

            var image = new google.maps.MarkerImage(this.icon, new google.maps.length(41, 45));
            image.anchor = new google.maps.Point(20, 45);

            var l = new google.maps.LatLng(this.lat, this.lng);
            new google.maps.Marker({
                position: l,
                map: this.map,
                icon: image
            });

            map.setCenter(new google.maps.LatLng(this.lat, this.lng));
            google.maps.event.trigger(this.map, 'resize');

            map.setZoom(14);
        };

        VNZ.mapsLoader = new VNZ.MapsLoader("google-map-contacts", $.proxy(this.loadMap, this));

        return this;
    };

    VNZ.stickyMenu = {
        inited: false,

        initialize: function () {
            if (this.inited) {
                return false;
            }

            this.inited = true;

            this.$menu = $(".slide-menu");

            if (!this.$menu.length) {
                return false;
            }

            this.menuVisible = true;
            this.firstHided = false;

            this.slideOffset = $(".slider-wide").offset().top;
            this.$closeButton = this.$menu.find(".slide-close");

            this.hideMenu = function () {
                this.menuVisible = !this.menuVisible;

                if (!this.menuVisible) {
                    this.$menu.find(".slide-close").delay(440).queue(function (next) {
                        $(this).addClass("open");
                        next();
                    });
                    this.$menu.animate({
                        left: (-1) * this.$menu.width() + "px"
                    }, 1000, "easeOutBack");
                } else {
                    this.$menu.find(".slide-close").removeClass("open");
                    this.setPosition();
                }
            };

            this.init = function () {
                this.$menu.find(".nav li").each(function (i, el) {
                    $(el).data("scroll-to")
                });

                this.startScroll = window.location.hash;
                if (this.startScroll.indexOf("#scroll-to-") !== false) {
                    this.startScroll = this.startScroll.substr(("#scroll-to-").length);
                } else {
                    this.startScroll = false;
                }
                ;

                this.$menu.find(".nav li").on("click", $.proxy(function (e) {
                    e.preventDefault();

                    var scrollTo = $(e.currentTarget).find("a").data("scroll-to");

                    this.slideScrollTo(scrollTo);
                }, this));

                this.slideScrollTo = function (scrollTo) {
                    $(".slide-to").each(function (i, el) {
                        if (scrollTo == $(el).data("slide-anchor")) {
                            $("html, body").animate({
                                scrollTop: $(el).offset().top - 60 + "px"
                            }, 500, "easeInOutQuad");
                            return true;
                        }
                    });
                };
                if (this.startScroll !== false) {
                    this.slideScrollTo(this.startScroll);
                }

                setTimeout($.proxy(function () {
                    if (window.location.hash !== "" && window.location.hash !== "#") {
                        this.slideScrollTo(window.location.hash.substr(1, window.location.hash.length));
                    }
                }, this), 100);

                // Скрываем пункты меню, которых нет в контенте
                this.$menu.find(".nav li").each(function (i, el) {
                    var scrollTo = $(el).find("a").data("scroll-to");

                    var found = false;
                    $(".slide-to").each(function (i, el) {
                        if (scrollTo == $(el).data("slide-anchor")) {
                            found = true;
                            return true;
                        }
                    });

                    if (!found) {
                        $(el).hide();
                    }
                });

                this.$closeButton.on("click", $.proxy(this.hideMenu, this));
            };
            this.init();

            this.setPosition = function (e) {
                if ($(window).width() >= 1720) {
                    this.firstHided = true;
                }

                if (this.$menu.height() < $(window).height()) {
                    if (this.$menu.height() + this.$menu.find(".logo").height() < $(window).height()) {
                        this.$menu.find(".logo").show();
                    }
                } else {
                    this.$menu.find(".logo").hide();
                }

                var scrollTop = $("html, body").scrollTop() || $("body").scrollTop();

                var top = scrollTop + ($(window).height() / 2) - (this.$menu.height() / 2);
                top = top < this.slideOffset ? this.slideOffset : top;

                this.$menu.css({
                    top: top + "px"
                });

                // Ширина меню
                var windowWidth = $(window).width();
                var menuWidth = 270;

                if (windowWidth < 1720) {
                    var delta = (1720 / windowWidth);
                    if (delta > 0) {
                        menuWidth = 150 + (120 / delta);
                    }
                }
                this.$menu.css({
                    width: menuWidth + "px"
                });

                // Не меняем левый отступ, если меню не видно
                if (this.menuVisible) {
                    this.$menu.dequeue().stop().animate({
                        left: 0 + "px"
                    }, 400);
                } else {
                    if (this.$menu.is(":animated")) {
                        return true;
                    }
                    ;

                    this.$menu.css({
                        left: (-1) * this.$menu.width() + "px"
                    });
                }

                var scrollTop = $("html, body").scrollTop() || $("body").scrollTop();

                if (
                    this.slideOffset < scrollTop &&
                    !this.firstHided
                ) {
                    if (this.menuVisible) {
                        this.firstHided = true;
                        this.hideMenu();
                    }
                }
            };
            $(window).on("scroll resize", $.proxy(this.setPosition, this));
            this.setPosition();

            return this;
        }
    };

    VNZ.videoInline = (function () {
        $(".video--js").each(function (i, el) {
            if ($(el).find(".video-wrap").length) {
                $(el).on("click", function () {
                    $(el).find(".video-play").cssFadeOut();

                    $(el).find(".video-wrap").html($(el).find(".video-code").html());

                    $(el).find(".video-wrap").cssFadeIn();
                    $(el).css({
                        backgroundImage: "none"
                    });
                });
            }
        });
    })();

    VNZ.placeLiker = (function () {
        setTimeout(function () {
            $(".share-wrap").removeClass("hover");
        }, 200);

        $(".item-likes").on("mouseenter", function () {
            $(this).closest(".share-wrap").addClass("hover");
        });
        $(".share-wrap").on("mouseleave", function () {
            $(this).removeClass("hover");
        });
    })();

    VNZ.placeBg = function () {
        if (VNZ.placeColor) {
            $("footer, footer .column-email input").css({
                backgroundColor: VNZ.placeColor
            });
        }
    };

    VNZ.serviceGallery = function (el) {
        this.$gallery = $(el);

        this.$slides = this.$gallery.find(".slides li");

        this.$slides.eq(0).addClass("current");

        this.$slidesTexts = this.$gallery.find(".item-description-service-content");

        this.goPrev = function () {
            this.$slides.filter(".current").animate({
                opacity: 0
            });
            var index = this.$slides.filter(".current").index();
            index--;

            if (index < 0) {
                index = this.$slides.length - 1;
            }
            ;

            this.$slides.filter(".current").removeClass("current");
            this.$slides.eq(index).addClass("current").animate({
                opacity: 1
            });

            this.$slidesTexts.fadeOut();
            this.$slidesTexts.eq(index).fadeIn();
        };
        this.goNext = function () {
            this.$slides.filter(".current").animate({
                opacity: 0
            });
            var index = this.$slides.filter(".current").index();
            index++;

            if (index > this.$slides.length - 1) {
                index = 0;
            }
            ;

            this.$slides.filter(".current").removeClass("current");
            this.$slides.eq(index).addClass("current").animate({
                opacity: 1
            });

            this.$slidesTexts.fadeOut();
            this.$slidesTexts.eq(index).fadeIn();
        };

        this.$gallery.find(".next").on("click", $.proxy(function () {
            this.goNext();
        }, this));
        this.$gallery.find(".prev").on("click", $.proxy(function () {
            this.goPrev();
        }, this));
    };
    VNZ.servicesGallery = (function () {
        this.$services = $(".block-services");

        if (!this.$services.length) {
            return false;
        }
        ;

        this.$services.find(".block-service").each(function (i, el) {
            new VNZ.serviceGallery(this);
        });
    })();

    VNZ.blockAjaxLoader = function (el) {
        this.$element = $(el);

        if (!this.$element.length) {
            return false;
        }
        ;

        this.$element.addClass("binded");

        this.url = this.$element.data("href");
        this.$acceptor = $(this.$element.data("acceptor"));
        this.offset = this.$element.data("offset");

        this.onload = function (data) {
            this.$acceptor.html(this.$acceptor.html() + data);
            VNZ.ellipsis();
            $(window).resize();

            if (data == "") {
                this.$element.hide();
            }

            setTimeout(function () {
                VNZ.blockLinks();
                VNZ.preloadBackgrounds();
            }, 100);
        };
        this.preload = function () {
            $.ajax({
                type: "GET",
                url: this.url,
                data: {
                    offset: this.offset
                },
                dataType: "html",
                cache: false,
                success: $.proxy(this.onload, this)
            });

            this.offset++;
        };

        this.$element.on("click", $.proxy(this.preload, this));
    };
    VNZ.blockAjaxLoad = function () {
        this.selector = ".btn-load-ajax";

        $(this.selector).not(".binded").each(function (i, el) {
            new VNZ.blockAjaxLoader(el);
        });
    };
    VNZ.blockAjaxLoad();

    VNZ.blockEventsLoad = (function () {
        this.$blockEvents = $(".block-events-ajax-load");

        if (!this.$blockEvents.length) {
            return false;
        }

        this.bind = function () {
            this.$blockEvents.find(".description-huge-nav a").on("click", $.proxy(function (e) {
                e.preventDefault();

                this.$blockEvents.animate({
                    opacity: "0.01"
                }, "fast", $.proxy(function () {
                    $.ajax({
                        type: "GET",
                        url: $(e.currentTarget).attr("href"),
                        data: {
                            is_index: $(e.currentTarget).data("isindex")
                        },
                        dataType: "html",
                        cache: false,
                        success: $.proxy(function (data) {
                            this.$blockEvents.html(data);
                            this.bind();

                            VNZ.blockLinks();
                            VNZ.preloadBackgrounds();

                            VNZ.showMoar();

                            this.$blockEvents.animate({
                                opacity: "1"
                            });
                        }, this)
                    });
                }, this));
            }, this));
        };
        this.bind();
    })();

    VNZ.netsDropdown = (function () {
        var height = $(".block-item-title--nets-dropdown-donor").height() + 139;
        $(".nets-dropdown").css({
            height: height + "px"
        });
        $(".nets-dropdown-ellipsis").css({
            top: height + 42 + "px"
        });

        $(".nets-dropdown-block").on("mouseenter", function () {
            $(this).find(".nets-dropdown-wrap").fadeIn();
        });
        $(".nets-dropdown-block").on("mouseleave", function () {
            $(this).find(".nets-dropdown-wrap").fadeOut();
        });
    })();

    VNZ.restaurantSearch = (function () {
        $(".block-restaurant-selector").not(".block-restaurant-selector--projects").find(".column-view-as li").on("click", function () {
            if ($(this).index() == 1) {
                $(".row-restaurant-search").hide();
                $(".restaurant-search-params").hide();

                VNZ.PlacesMap.mapReady();
            } else {
                $(".row-restaurant-search").show();

                if ($(".restaurant-search-params").hasClass("restaurant-search-params--visible")) {
                    $(".restaurant-search-params").show();
                }

                if ($(".restaurant-search-params").data("visible") == "yes") {
                    $(".restaurant-search-params").show();
                }
            }
        });

        this.searchRestaurants = function (params) {
            $(".restaurant-search__loader").fadeIn();

            $.ajax({
                url: "restaurant/filter/",
                method: "POST",
                data: params,
                dataType: "JSON",
                success: function (data) {
                    $(".restaurant-search__loader").fadeOut(function () {
                        // Картинками
                        $(".block-restaurants-list-album-trigger").animate({
                            opacity: 0.001
                        }, function () {
                            $(".restaurants-search .block-restaurants-list-album-trigger").html(data.pictures);
                            VNZ.preloadBackgrounds();
                            VNZ.PlacesAlbums.bind();
                            $(".block-restaurants-list-album-trigger").animate({
                                opacity: 1
                            });
                        });

                        // Списком
                        $(".block-list-restaurants__list").animate({
                            opacity: 0.001
                        }, function () {
                            $(".restaurants-search .block-list-restaurants__list").html(data.list);
                            VNZ.preloadBackgrounds();
                            $(".block-list-restaurants__list").animate({
                                opacity: 1
                            });
                        });
                    });
                }
            });
        };

        $(".restaurant-search-param__select").on("change", $.proxy(function () {
            this.searchRestaurants($(".restaurant-search-param__select").serialize());
        }, this));

        $(".restaurans-search-params__trigger").on("click", $.proxy(function (e) {
            e.preventDefault();

            $(".restaurant-search-params").slideToggle();

            if ($(".restaurant-search-params").data("visible") == "yes") {
                $(".restaurant-search-params").data("visible", "no");
                this.searchRestaurants({});
            } else {
                $(".restaurant-search-params").data("visible", "yes");
            }
        }, this));

        $(".restaurant-search__name--js").on("keyup", function () {
            var search_string = $(this).val().toUpperCase();

            $(".restaurants-search .btn-load-more").trigger("click");

            var reset = 0;
            $(".restaurants-search .block-restaurants-list .article").each(function (i, el) {
                $(el).css({
                    marginRight: "2.12766%"
                });

                if ($(this).find(".post-title").text().toUpperCase().indexOf(search_string) == -1) {
                    $(el).hide();
                    $(el).removeClass("post");
                } else {
                    $(el).addClass("post");

                    reset++;
                    if (reset == 4) {
                        $(el).css({
                            marginRight: 0
                        });
                        reset = 0;
                    }

                    $(el).show();
                }
            });

            $(".restaurants-search .block-list-restaurants .column p").each(function (i, el) {
                if ($(this).text().toUpperCase().indexOf(search_string) == -1) {
                    $(el).hide();
                } else {
                    $(el).show();
                }
            });
        });
    })();

    VNZ.PlacesMap = new VNZ.placesMap();
});
;VNZ = window.VNZ || {};

$(function () {
    VNZ.gridWidth = $(".block-header-main").width();
    VNZ.gridWidthUpdater = (function () {
        this.gridWidthUpdate = function () {
            VNZ.gridWidth = $(".block-header-main").width();
        };
        $(window).on("resize", $.proxy(this.gridWidthUpdate, this));
    })();

    /*
     Выпадайка для выбора города
     */
    VNZ.citySelector = (function () {
        $(".city-current, .cities").on("mouseenter", function () {
            $(".cities").addClass("open");
        });
        $(".city-current, .cities").on("mouseleave", function () {
            $(".cities").removeClass("open");
        });
    })();

    /*
     Плавное появление изображений
     */
    VNZ.preloadBackgrounds = function () {
        $(".preload").lazyload({
            effect: "fadeIn",
            threshold: 0,
            //skip_invisible : false,
            data_attribute: "url"
        });

        return true;

        this.$images = $(".preload-background").not(".loaded").not(".preload-background-delayed");

        var urls = [];
        this.$images.each(function (i, el) {
            urls.push($(el).data("url"));
            $(el).addClass("preload-background-loading");
        });

        this.imageLoad = function () {
            this.obj.$images.eq(this.index).css({
                backgroundImage: "url(" + this.url + ")"
            });
            this.obj.$images.eq(this.index).addClass("loaded");
            this.obj.$images.eq(this.index).removeClass("preload-background-loading");
        };

        $(urls).each($.proxy(function (i, el) {
            var image = new Image;
            image.onload = $.proxy(this.imageLoad, {obj: this, index: i, url: el});
            image.src = el;
        }, this));
    };

    /*
     Плавное появление delayed
     */
    VNZ.preloadBackgroundsDelayed = function (className) {
        this.$images = $(".preload-background").not(".loaded").filter(className);

        var urls = [];
        this.$images.each(function (i, el) {
            urls.push($(el).data("url"));
            $(el).addClass("preload-background-loading");
        });

        this.imageLoad = function () {
            this.obj.$images.eq(this.index).css({
                backgroundImage: "url(" + this.url + ")"
            });
            this.obj.$images.eq(this.index).addClass("loaded");
            this.obj.$images.eq(this.index).removeClass("preload-background-loading");
        };

        $(urls).each($.proxy(function (i, el) {
            var image = new Image;
            image.onload = $.proxy(this.imageLoad, {obj: this, index: i, url: el});
            image.src = el;
        }, this));
    };

    /*
     Спойлер для текстовых блоков
     */
    VNZ.spoiler = (function () {
        $(".view-more").each(function (i, el) {
            $(el).on("click", function (e) {
                e.preventDefault();

                $(this).parent().find(".more-text").slideToggle();
                $(this).toggleClass("view-more-opened");
            });
        });
    })();

    VNZ.showMoar = function () {
        $(".btn-show-more").on("click", function () {
            $(this).hide();
            $(this).closest(".block-posts").find(".block-hidden").removeClass("block-hidden");
        });
    };
    VNZ.showMoar();

    /*
     Табы по сайту
     */
    VNZ.tabs = (function () {
        $(".tabs").each(function (i, el) {
            $($(el).data("content")).find(".tab").eq(0).addClass("active");
            $(el).find("li").eq(0).addClass("active");

            $(el).find("li").not(".skip").on("click", function (e) {
                e.preventDefault();

                var index = $(this).index();

                $(
                    $(this).parent().data("content")
                ).find(".tab").removeClass("active").eq(index).addClass("active");

                $(this).parent().find("li").removeClass("active");
                $(this).addClass("active");
            });
        });
    })();

    /*
     Шапка сайта
     */
    VNZ.header = (function () {
        if ($(".scroll-to-anchor").length) {
            $(".block-header").removeClass("block-header-hidden");

            $('html, body').scrollTop($(".scroll-to-anchor").offset().top);
            VNZ.stickyMenu.initialize();
        }
        ;

        $(window).on("scroll", function () {
            if ($(".header-anchor").length == 0) {
                return true;
            }

            var fixOffset = $(".header-anchor").eq(0).offset().top;
            var fixBlockHeight = $(".header-anchor").eq(0).height();

            var scrollTop = $("html, body").scrollTop() || $("body").scrollTop();

            if (fixOffset <= scrollTop) {
                $(".block-header").addClass("block-header-fixed-hidden");
            } else {
                $(".block-header").removeClass("block-header-mini block-header-mini-fixed");
                $("#page").removeClass("header-mini");
            }

            if (fixOffset + fixBlockHeight <= scrollTop) {
                $(".block-header").addClass("block-header-fixed-visible");
                $(".block-header").addClass("block-header-mini block-header-mini-fixed");
                $("#page").addClass("header-mini");
            } else {
                $(".block-header").addClass("block-header-fixed-hidden");
                $(".block-header").removeClass("block-header-fixed-visible");
            }
        });
        $(window).trigger("scroll");

        $(".block-header").addClass("block-header-fixed-animated");
    })();

    /*
     Параллакс скролл для блоков
     TODO: Переписать или удалить
     */
    VNZ.parallaxScroll = (function () {
        return false;

        var scrollLoop = function () {
            var scrollTop = $("html, body").scrollTop() || $("body").scrollTop();
            var windowHeight = $(window).height();

            $(".parallax-scroll").each(function (i, el) {
                var offset = $(el).offset().top;
                var height = $(el).height();

                if (scrollTop + windowHeight > offset && scrollTop < offset + height) {
                    var percent = (100 - ((scrollTop) / (offset + height) * 100)) * 10;

                    if (percent > 100) {
                        percent = 100;
                    }
                    if (percent < 0) {
                        percent = 0;
                    }

                    $(el).css({
                        backgroundPosition: "100% " + percent + "%"
                    });
                }
            });
            requestAnimFrame(scrollLoop, $("#page")[0]);
        };
        scrollLoop();
    })();

    /*
     Навигация в отодвигающемся правом сайдбаре
     */
    VNZ.sideNav = (function () {
        $(".aside-nav-trigger").on("click", function (e) {
            e.preventDefault();

            $(".page-shift-ready").toggleClass("page-shifted");
            $(".aside-nav-bar").toggleClass("show");
        });
    })();

    /*
     Блочные ссылки
     */
    VNZ.blockLinks = function () {
        $(".block-link").on("mousedown", function (e) {
            $(this).data({
                x: e.pageX,
                y: e.pageY
            });
        });

        $(".block-link").on("mouseup", function (e) {
            if ($(this).data("x") == e.pageX && $(this).data("y") == e.pageY) {
                if (e.target.nodeName == "A") {
                    return true;
                }

                if ($(this).data("href")) {
                    e.preventDefault();
                    window.location.href = $(this).data("href");
                }
            }
        });
    };
    VNZ.blockLinks();

    /*
     Спойлер SOON
     */
    VNZ.sooner = (function () {
        $(".soon")
            .on("mouseenter", function (e) {
                $(".soon-pin").stop(true, true).hide();
                $(".soon-pin").css({
                    left: ($(this).offset().left + parseInt($(this).css("paddingLeft"), 10) + $(this).width() / 2) - $(".soon-pin").width() / 2 + "px",
                    top: ($(this).offset().top + $(this).height() / 2) - $(".soon-pin").height() / 2 + "px"
                }).stop().fadeIn().addClass("over");
            })
            .on("mouseleave", function (e) {
                $(".soon-pin").removeClass("over").stop().fadeOut();
            });
    })();

    /*
     Подписка на новости
     */
    VNZ.subscribe = (function () {
        $(".block-footer-subscribe .btn-huge-column").on("click", function (e) {
            e.preventDefault();

            this.$input = $(".block-footer-subscribe .column-email input");

            if (this.$input.val().trim() == "") {
                this.$input.addClass("error");
                return false;
            }

            $.ajax({
                type: "POST",
                url: "/subscribe",
                dataType: "html",
                cache: false,
                data: {
                    email: $(".block-footer-subscribe .column-email input").val()
                }
            });

            $(".block-footer-subscribe .column-title, .block-footer-subscribe .column-btn").animate({
                opacity: 0.01
            });
            $(".block-footer-subscribe .column-email input").fadeOut(function () {
                $(".block-footer-subscribe .column-email span").fadeIn();
            });
        });
    })();

    /*
     Троеточия, если не влезает инфа
     */
    VNZ.ellipsis = function () {
        $(window).resize(function () {
            $(".block-news-rows .title").ellipsis();
        });
        $(window).resize();
    };
    VNZ.ellipsis();

    /*
     Переключатель типа отображения ресторанов по хешу
     */
    VNZ.restaurantTypeHash = (function () {
        $(window).on('hashchange', function () {
            var hash = document.location.hash.substr(1, document.location.hash.length);

            switch (hash) {
                case "restaurant-pictures": {
                    $(".column.column-view-as li").eq(0).trigger("click");

                    break;
                }

                case "restaurant-map": {
                    $(".column.column-view-as li").eq(1).trigger("click");
                    VNZ.PlacesMap.mapReady();

                    break;
                }

                case "restaurant-list": {
                    $(".column.column-view-as li").eq(2).trigger("click");

                    break;
                }
            }
        });
    })();

    VNZ.scrollTopBtn = (function () {
        $(window).on("scroll resize", function () {
            var offset = $(window).scrollTop();

            if (offset > $(window).height()) {
                $(".scroll-top-btn").addClass("visible");
            } else {
                $(".scroll-top-btn").removeClass("visible");
            }
        });

        $(".scroll-top-btn").on("click", function () {
            $("body, html").animate({
                scrollTop: 0
            }, $(window).scrollTop() / 10, "easeOutExpo");
        });
    })();

    VNZ.photoGallery = function (gallery) {
        this.$gallery = $(".photos-gallery-fotorama-wrap");

        if (!this.$gallery.length) {
            return false;
        }

        this.$fotorama = this.$gallery.find(".fotorama");

        $.each(VNZ.galleryPhotos, $.proxy(function (i, src) {
            this.$fotorama.append(
                '<img data-full="/images/w1600-h1600/' + src + '" src="/images/w1200-h1200/' + src + '" alt=""/>'
            );
        }, this));

        this.$gallery.find(".controls .counter .from").text(VNZ.galleryPhotos.length);
        this.$fotorama.fotorama({
            shadows: false,
            arrows: false,
            loop: true
        });

        this.fotoramaInstance = this.$fotorama.data("fotorama");

        this.$gallery.find(".fotorama-fullscreen").on("click", $.proxy(function () {
            this.fotoramaInstance.requestFullScreen();
        }, this));

        this.triggerNavVisibility = function () {
            var $thumbs = this.$gallery.find(".fotorama__nav__shaft");

            if ($thumbs.hasClass("visible")) {
                $thumbs.animate({
                    opacity: 1,
                    height: "125px"
                });
            } else {
                $thumbs.animate({
                    opacity: 0.01,
                    height: 0
                });
            }
            ;
        };
        this.$gallery.find(".fotorama-toggle-thumbs").on("click", $.proxy(function () {
            var $thumbs = this.$gallery.find(".fotorama__nav__shaft");
            $thumbs.toggleClass("visible");

            this.triggerNavVisibility();
        }, this));

        this.$fotorama.on("fotorama:fullscreenenter", $.proxy(function () {
            if (!$(".fotorama--fullscreen .gallery-nav").length) {
                $(".fotorama--fullscreen").append(
                    $('<ul>').append($('.gallery-nav-fullscreen').clone()).html()
                );
                $(".fotorama--fullscreen .gallery-nav-fullscreen").addClass("navigator");

                $(".fotorama--fullscreen .gallery-nav-fullscreen.navigator.gallery-nav").show();
                $(".fotorama--fullscreen .gallery-nav-fullscreen.navigator.gallery-nav .counter .from").text(this.$gallery.find(".controls .counter .from").text());
                $(".fotorama--fullscreen .gallery-nav-fullscreen.navigator.gallery-nav .counter .number").text(this.$gallery.find(".controls .counter .number").text());

                $(".fotorama--fullscreen .gallery-nav-fullscreen.navigator.gallery-nav .next").on("click", $.proxy(function () {
                    this.fotoramaInstance.show(">");
                }, this));
                $(".fotorama--fullscreen .gallery-nav-fullscreen.navigator.gallery-nav .prev").on("click", $.proxy(function () {
                    this.fotoramaInstance.show("<");
                }, this));
                $(".fotorama--fullscreen .gallery-nav-fullscreen.navigator.gallery-nav .close").on("click", $.proxy(function () {
                    this.fotoramaInstance.cancelFullScreen();
                }, this));
            }
        }, this));

        this.$fotorama.on("fotorama:fullscreenexit", $.proxy(function () {
            $(".gallery-nav-fullscreen.navigator.gallery-nav").remove();
        }, this));

        this.$fotorama.on("fotorama:show", $.proxy(function (e) {
            this.$gallery.find(".controls .number").text(this.fotoramaInstance.activeIndex + 1);
            $(".fotorama--fullscreen .gallery-nav .counter .number").text(this.fotoramaInstance.activeIndex + 1);
        }, this));

        this.$gallery.find(".gallery-nav .next").on("click", $.proxy(function () {
            this.fotoramaInstance.show(">");
        }, this));
        this.$gallery.find(".gallery-nav .prev").on("click", $.proxy(function () {
            this.fotoramaInstance.show("<");
        }, this));
    };

    // Слайдер трендов и акций
    VNZ._newsSlider = function (el) {
        this.$slider = $(el);

        if (!this.$slider.length) {
            return false;
        }

        this.$items = this.$slider.find(".news-slide");
        this.$items.eq(0).show().addClass("current");

        this.goNext = function (e) {
            e.preventDefault();

            var index = this.$items.filter(".current").index();
            this.$items.filter(".current").removeClass("current");

            index++;

            if (index >= this.$items.length) {
                index = 0;
            }

            this.$items.eq(index).addClass("current");
        };
        this.goPrev = function (e) {
            e.preventDefault();

            var index = this.$items.filter(".current").index();
            this.$items.filter(".current").removeClass("current");

            index--;

            if (index < 0) {
                index = this.$items.length - 1;
            }

            this.$items.eq(index).addClass("current");
        };

        this.$slider.parent().find(".next").on("click", $.proxy(this.goNext, this));
        this.$slider.parent().find(".prev").on("click", $.proxy(this.goPrev, this));

        return this;
    };
    VNZ.newsSlider = (function () {
        $.each($(".news-slider"), function (i, el) {
            new VNZ._newsSlider(el);
        });
    })();

    // Бронирование
    VNZ.booking = (function () {
        $(".book--js").on("click", function (e) {
            if ($("body").hasClass("on-mobile")) {
                return true;
            }
            ;

            e.preventDefault();

            var place_id = $(this).data("place-id");

            if (!place_id) {
                place_id = "";
            }
            ;

            $.get("booking/" + place_id, function (data) {

                $("body").append(data);

                $(".datepicker-book--js").pickadate({
                    formatSubmit: "yyyy-mm-dd",
                    min: true,
                    disable: [
//                        new Date(2016,5,17),
                        new Date(2016, 11, 31),
                        new Date(2017, 0, 1)
                    ]
                });

                $(".phone-mask").mask("+7 (999) 999-99-99");
                $(".phone-mask--en").mask("+999999999999999");

                //$(".autogrow--js").autoGrow();
                //$(".selectize--js").selectize();

//                $(".popup-booking").css({
//                    marginTop: ($(window).height() / 2 - $(".popup-booking").height() / 2) - 80 + "px"
//                });

                $(".popup-booking").css({
                    marginTop: 40 + "px",
                    marginBottom: 40 + "px"
                });
                //setInterval(function() {
                //    $(".popup-booking").css({
                //        marginTop: ($(window).height() / 2 - $(".popup-booking").height() / 2) + 40 + "px"
                //    });
                //}, 50);

                $("body").find(".popup-wrap-booking").fadeIn();

                $(".popup-booking__close--js").on("click", function (e) {
                    e.preventDefault();
                    $(".popup-wrap").fadeOut(function () {
                        $(this).remove();
                    });
                });

                $(".btn-book-submit--js").on("click", function (e) {
                    e.preventDefault();


                    $(".popup-booking__form").find("input.required").each(function (i, el) {
                        if ($(el).val() == "") {
                            $(el).addClass("error");
                        } else {
                            $(el).removeClass("error");
                        }
                        ;
                    });
                    $(".popup-booking__form").find("select.required").each(function (i, el) {
                        if ($(el).val() == null) {
                            $(el).addClass("error");
                        } else {
                            $(el).removeClass("error");
                        }
                        ;
                    });

                    if ($(".popup-booking__form").find("input.error").length > 0) {
                        return false;
                    }
                    ;
                    if ($(".popup-booking__form").find("select.error").length > 0) {
                        return false;
                    }
                    ;

                    var data = $(this).closest("form").serialize();

                    $.ajax({
                        url: "booking/send",
                        type: "post",
                        data: data,
                        success: $.proxy(function () {


                            $(".popup-wrap-booking").fadeOut(function () {
                                $(this).remove();
                                VNZ.bookThanks.show();
                            });
                        }, this)
                    });
                });
            });
        });

        $("body").on("change", ".js-place_id", function (e) {
            $(".booking-phrase").hide();
            $(".js-booking-phrase--" + $(e.currentTarget).val()).show();
        });

        if (window.location.hash == "#booking") {
            $(".book--js").last().trigger("click");
        }
        ;

        //console.log(VNZ.show_booking);

        if (VNZ.show_booking == true) {
            $(".book--js").last().trigger("click");
        }
        ;
    });

    VNZ.bookThanks = {
        show: function () {
            $(".popup-wrap-thanks").fadeIn();
            $(".popup-booking-thanks").fadeIn();

            $(".popup-booking-thanks").css({
                marginTop: ($(window).height() / 2 - $(".popup-booking-thanks").height() / 2) + "px"
            });
        }
    };

// Банкеты
    VNZ.banquet = (function () {
        $(".banquet--js").on("click", function (e) {
            e.preventDefault();

            $.get("banquet/" + $(e.currentTarget).data("place-id"), function (data) {

                $("body").append(data);

                $(".datepicker-banquet--js").pickadate({
                    formatSubmit: "yyyy-mm-dd",
                    min: true
                });
                $(".phone-mask").mask("+7 (999) 999-99-99");
                $(".amount-mask").mask("999999999");

                $(".popup-banquet").css({
                    marginTop: ($(window).height() / 2 - $(".popup-banquet").height() / 2) - 80 + "px"
                });
                setInterval(function () {
                    $(".popup-banquet").css({
                        marginTop: ($(window).height() / 2 - $(".popup-banquet").height() / 2) - 80 + "px"
                    });
                }, 50);

                $("body").find(".popup-wrap-banquet").fadeIn();

                $(".popup-banquet__close--js").on("click", function (e) {
                    e.preventDefault();
                    $(".popup-wrap").fadeOut(function () {
                        $(this).remove();
                    });
                });

                $(".btn-banquet-submit--js").on("click", function (e) {
                    e.preventDefault();


                    $(".popup-banquet__form").find("input.required").each(function (i, el) {
                        if ($(el).val() == "") {
                            $(el).addClass("error");
                        } else {
                            $(el).removeClass("error");
                        }
                        ;
                    });
                    $(".popup-banquet__form").find("select.required").each(function (i, el) {
                        if ($(el).val() == null) {
                            $(el).addClass("error");
                        } else {
                            $(el).removeClass("error");
                        }
                        ;
                    });

                    if ($(".popup-banquet").find("input.error").length > 0) {
                        return false;
                    }
                    ;
                    if ($(".popup-banquet").find("select.error").length > 0) {
                        return false;
                    }
                    ;

                    var data = $(this).closest("form").serialize();

                    $.ajax({
                        url: "banquet/send",
                        type: "post",
                        data: data,
                        success: $.proxy(function () {

                            $(".popup-wrap-banquet").fadeOut(function () {
                                $(this).remove();
                                VNZ.banquetThanks.show();
                            });
                        }, this)
                    });
                });
            });
        });
    });
    VNZ.banquet();

    VNZ.banquetThanks = {
        show: function () {
            $(".popup-wrap-thanks").fadeIn();
            $(".popup-banquet-thanks").fadeIn();

            $(".popup-banquet-thanks").css({
                marginTop: ($(window).height() / 2 - $(".popup-banquet-thanks").height() / 2) + "px"
            });
        }
    };

// Сертификаты
    VNZ.sertificates = (function () {
        $(".certificate--js").on("click", function (e) {
            e.preventDefault();

            $.get("certificate/", function (data) {

                $("body").append(data);

                $(".datepicker--js").pickadate({
                    formatSubmit: "yyyy-mm-dd"
                });
                $(".phone-mask").mask("+7 (999) 999-99-99");
                $(".amount-mask").mask("999999999");

                $(".popup-certificate").css({
                    marginTop: ($(window).height() / 2 - $(".popup-certificate").height() / 2) - 80 + "px"
                });
                setInterval(function () {
                    $(".popup-certificate").css({
                        marginTop: ($(window).height() / 2 - $(".popup-certificate").height() / 2) - 80 + "px"
                    });
                }, 50);

                $("body").find(".popup-wrap-certificates").fadeIn();

                $(".popup-certificate__close--js").on("click", function (e) {
                    e.preventDefault();
                    $(".popup-wrap").fadeOut(function () {
                        $(this).remove();
                    });
                });

                $(".btn-certificate-submit--js").on("click", function (e) {
                    e.preventDefault();


                    $(".popup-certificate__form").find("input.required").each(function (i, el) {
                        if ($(el).val() == "") {
                            $(el).addClass("error");
                        } else {
                            $(el).removeClass("error");
                        }
                        ;
                    });
                    $(".popup-certificate__form").find("select.required").each(function (i, el) {
                        if ($(el).val() == null) {
                            $(el).addClass("error");
                        } else {
                            $(el).removeClass("error");
                        }
                        ;
                    });

                    if ($(".popup-certificate__form").find("input.error").length > 0) {
                        return false;
                    }
                    ;
                    if ($(".popup-certificate__form").find("select.error").length > 0) {
                        return false;
                    }
                    ;

                    var data = $(this).closest("form").serialize();

                    $.ajax({
                        url: "certificate/send",
                        type: "post",
                        data: data,
                        success: $.proxy(function () {

                            $(".popup-wrap-certificates").fadeOut(function () {
                                $(this).remove();
                                VNZ.certificateThanks.show();
                            });
                        }, this)
                    });
                });
            });
        });
    });
    VNZ.sertificates();

    VNZ.certificateThanks = {
        show: function () {
            $(".popup-wrap-thanks").fadeIn();
            $(".popup-certificate-thanks").fadeIn();

            $(".popup-certificate-thanks").css({
                marginTop: ($(window).height() / 2 - $(".popup-certificate-thanks").height() / 2) + "px"
            });
        }
    };

    // Фидбэки
    VNZ.feedbackThanks = {
        show: function () {
            $(".popup-wrap-thanks").fadeIn();
            $(".popup-feedback-thanks").fadeIn();

            $(".popup-feedback-thanks").css({
                marginTop: ($(window).height() / 2 - $(".popup-feedback-thanks").height() / 2) + "px"
            });
        }
    };

    VNZ.feedback = (function () {
        $(".feedback--js").on("click", function (e) {
            e.preventDefault();

            var place_id = $(this).data("place-id");

            if (!place_id) {
                place_id = "";
            }

            $.get("feedback/" + place_id, function (data) {
                $("body").append(data);

                $(".autogrow--js").autoGrow();
                //$(".selectize--js").selectize();
                $(".datepicker--js").pickadate({
                    formatSubmit: "yyyy-mm-dd"
                });

                $(".popup-feedback--textarea").css("height", "125px");

                $("body").find(".popup-wrap-feedback").fadeIn();
                $(".popup-close--js").on("click", function (e) {
                    e.preventDefault();
                    $(".popup-wrap").fadeOut(function () {
                        $(this).remove();
                    });
                });

                $(".btn-feedback-submit--js").on("click", function (e) {
                    e.preventDefault();

                    var data = $(this).closest("form").serialize();
                    data = data.replace("date=", "date_=");
                    data = data.replace("date_submit=", "date=");

                    $.ajax({
                        url: "feedback/send",
                        type: "post",
                        data: data,
                        success: function () {
                            $(".popup-wrap-feedback").fadeOut(function () {
                                $(this).remove();
                                VNZ.feedbackThanks.show();
                            });
                        }
                    });
                });
            });
        });

        if (window.location.hash == "#feedback") {
            $(".feedback--js").last().trigger("click");
        }
        ;
    });
    VNZ.feedback();

    // Чекбоксы
    VNZ.checkbox = function (el) {
        this.$checkbox = $(el);
        this.state = false;
        this.bg_off = "/assets/i/checkbox.png";
        this.bg_on = "/assets/i/checkbox-checked.png";

        this.toggle = function () {
            this.state = !this.state;

            if (this.state) {
                this.$checkbox.css({
                    backgroundImage: "url(" + this.bg_on + ")"
                });
            } else {
                this.$checkbox.css({
                    backgroundImage: "url(" + this.bg_off + ")"
                });
            }
        };
        this.$checkbox.on("click", $.proxy(this.toggle, this));
    };
    $(".checkbox").each(function (i, el) {
        new VNZ.checkbox(el);
    });

    // События
    VNZ.events = function () {
        $(".events-date-select").pickadate({
            formatSubmit: "dd-mm-yy",
            onClose: function () {
                $.get("event/date/" + $(".events-date-select").pickadate().val() + "?is_simple=1", function (html) {
                    $(".ajax-load").animate({
                        opacity: "0.01"
                    }, $.proxy(function () {
                        $(".ajax-load").html(this.html);
                        $(".ajax-load").animate({
                            opacity: "1"
                        });
                        VNZ.blockLinks();
                        VNZ.preloadBackgrounds();


                        VNZ.showMoar();

                        VNZ.events();
                    }, {html: html}));
                });
            }
        });
    };
    VNZ.events();

    VNZ.vacancies = function () {
        if (!$(".vacancies-list").length) {
            return false;
        }

        $(".vacancies-list__item__opener, .vacancies-list__item__title").on("click", function (e) {
            $(this).closest(".vacancies-list__item").find(".vacancies-list__item__opener").toggleClass("closed");
            $(this).closest(".vacancies-list__item").find(".vacancies-list__item__description").slideToggle();
        });
    };
    VNZ.vacancies();

    /* Реклама */
    VNZ.horizontalSlider = function () {
        $(".bnr-horizontal-trigger").on("click", function (e) {
            e.preventDefault();

            $(this).closest(".bnr-horizontal").toggleClass("open");
        });
    };
    VNZ.horizontalSlider();

    /* Поиск */
    VNZ.search = function () {
        $(".search-trigger-wrap").find("input").on("blur", function () {
            setTimeout($.proxy(function () {
                $(this).removeClass("open");
            }, this), 300);
        });
        $(".search-trigger").on("click", function (e) {
            e.preventDefault();

            if (!$(this).closest("li").find("input").hasClass("open")) {
                $(this).closest("li").find("input").addClass("open").focus();
            } else {
                $(this).closest("form").submit();
            }
        });

        $(".search-field-loupe").on("click", function (e) {
            e.preventDefault();

            $(this).closest("form").submit();
        });

        $(".search-periodics__view_more").on("click", function (e) {
            e.preventDefault();

            $(this).closest(".search-periodics").find(".search-periodics__item").show().addClass("bordered").last().removeClass("bordered");
            $(this).hide();
        });


    };
    VNZ.search();

    VNZ.liveTex = function () {
        window.requestAnimFrame = (function () {
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                function (callback) {
                    window.setTimeout(callback, 1000 / 60);
                };
        })();

        (function animloop() {
            requestAnimFrame(animloop);

            // RENDER
            if (!$(".lt-widget-wrap").length > 0) {
                return true;
            }
            ;

            if ($(".lt-widget-hidden").length > 0) {
                $(".lt-overlay").removeClass("visible");
            } else {
                $(".lt-overlay").addClass("visible");
            }
            ;

            if ($(".lt-online").length > 0) {
                $(".header__chat__wrap").show();
            } else {
                $(".header__chat__wrap").hide();
            }
            ;

            var name = $(".lt-chat-header__txt-name").text();
            name = name.split(" ");
            name = name[0];
            $(".lt-chat-header__txt-name").text(name);
        })();

        $(".header__chat").on("click", function () {
            $(".lt-label").trigger("click");
        });
    };
    VNZ.liveTex();
});
;$(function () {
    $('.js--theater-tickets').on('change', function (e) {
        var value = $(e.currentTarget).val();
        $('.js--theater-tickets').val(0);
        $(e.currentTarget).val(value);
    });

    $('.js--theater-submit').on('click', function (e) {
        e.preventDefault();

        var required_selects = ['.js--theater-tickets', '.js--theater-sets'];
        var required_selects_messages = ['Пожалуйста, выберите хотя бы один билет', 'Пожалуйста, выберите хотя бы один сет'];

        var halt = false;
        $.each(required_selects, function (i, elems) {
            var counter = 0;
            $(elems).each(function (j, el) {
                counter = counter + $(el).val();
            });
            if (counter == 0) {
                alert(required_selects_messages[i]);
                halt = true;
                return false;
            }
            ;
        });

        $('.theater__form input').each(function (i, el) {
            if ($.trim($(el).val()) == '') {
                alert('Пожалуйста, заполните все личные данные');
                halt = true;
                return false;
            }
            ;
        });

        if (halt) {
            return false;
        }
        ;

        $.ajax({
            type: "POST",
            url: '/spb/theater/send',
            data: $('.js--theater-form').serialize(),
            success: function () {
                //
            }
        });
    });
});
;$(function () {
    $(".admin__notice__reload--js").on("click", function (e) {
        e.preventDefault();
        window.location.reload();
    });
});
;VNZ = window.VNZ || {};

$(function () {
    $.ajaxSetup({
        // Disable caching of AJAX responses
        cache: false
    });


    VNZ.PlacesAlbums = new VNZ.placesAlbum();

    VNZ.mapContacts();
    //
    VNZ.preloadBackgrounds();
    new VNZ.placeBg();

    new VNZ.photoGallery();

    VNZ.booking();

    $(".autogrow--js").autoGrow();
    $(".datepicker--js").pickadate({
        formatSubmit: "yyyy-mm-dd",
        selectMonths: true,
        selectYears: 100,
        min: new Date(1918, 1, 1),
        max: new Date(),
    });

    $("#promo_card_phone, #client_phone").intlTelInput({
        autoFormat: true,
        autoHideDialCode: false,
        utilsScript: "/assets/js/utils.js",
        autoPlaceholder: "aggressive",
        nationalMode: false,
        initialCountry: "BY",
        preferredCountries: ["BY"]
    });

    // var supportedFlag = $.keyframe.isSupported();

//2.3344594595
    $.keyframe.define([{
        name: 'slide',
        '0%': {transform: "translate3d(0, 0, 0)"},
        '100%': {transform: "translate3d(-" + ($("body").height() * (2521 / 1080)) + "px, 0, 0)"}
    }]);

});
