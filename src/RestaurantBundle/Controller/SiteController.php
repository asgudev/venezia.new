<?php

namespace RestaurantBundle\Controller;

use FrontendBundle\Entity\WebShop;
use FrontendBundle\Entity\WebShopArticle;
use FrontendBundle\Entity\WebShopImage;
use FrontendBundle\Form\WebShopArticleType;
use FrontendBundle\Form\WebShopImageType;
use FrontendBundle\Form\WebShopType;
use DishBundle\Entity\DishCategory;
use DishBundle\Entity\CategoryDelegate;
use DishBundle\Entity\Dish;
use FrontendBundle\FrontendBundle;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use RestaurantBundle\Form\Type\ShopType;
use RestaurantBundle\Form\Type\PlaceEditType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Class ShopController
 * @package RestaurantBundle\Controller
 * @Route("/site")
 */
class SiteController extends Controller
{
    /**
     * @Route("/", name="site_admin_index")
     */
    public function indexAction()
    {

    }


    /**
     * @Route("/shop/list", name="site_shop_list")
     */
    public function shopAction()
    {
        $webShops = $this->getDoctrine()->getRepository('FrontendBundle:WebShop')->findAll();

        return $this->render("@Restaurant/Site/shops.html.twig", [
            'shops' => $webShops,
        ]);
    }

    /**
     * @Route("/shop/{id}/edit", name="site_shop_edit")
     */
    public function webShopEditAction(Request $request, WebShop $webShop)
    {
        if (!$webShop)
            throw new NotFoundHttpException();

        $webShops = $this->getDoctrine()->getRepository(WebShop::class)->findAll();

        $form = $this->createForm(WebShopType::class, $webShop);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($webShop);
            $em->flush();
        }

        return $this->render("@Restaurant/Site/shops.html.twig", [
            'shops' => $webShops,
            'form' => $form->createView(),
            'shop' => $webShop,
        ]);
    }

    /**
     * @Route("/gallery", name="site_gallery_list")
     */
    public function webShopGalleryAction(Request $request)
    {

        $photo = new WebShopImage();

        $form = $this->createForm(WebShopImageType::class, $photo);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();

            return $this->redirectToRoute('site_gallery_list');
        }

        $webPhotos = $this->getDoctrine()->getRepository('FrontendBundle:WebShopImage')->findBy([
            'isDeleted' => false,
        ]);


        return $this->render("@Restaurant/Site/gallery.html.twig", [
            'photos' => $webPhotos,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/gallery/{id}/edit", name="site_gallery_edit")
     */
    public function webShopGalleryEditAction(Request $request, WebShopImage $webShopImage)
    {
        if (!$webShopImage)
            throw new NotFoundHttpException();

        $form = $this->createForm(WebShopImageType::class, $webShopImage);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($webShopImage);
            $em->flush();

        }

        $webPhotos = $this->getDoctrine()->getRepository('FrontendBundle:WebShopImage')->findBy([
            'isDeleted' => false,
        ]);


        return $this->render("@Restaurant/Site/gallery.html.twig", [
            'photos' => $webPhotos,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/gallery/{id}/delete", name="site_gallery_delete")
     */
    public function webShopGalleryDeleteAction(Request $request, WebShopImage $webShopImage)
    {
        if (!$webShopImage)
            throw new NotFoundHttpException();

        $em = $this->getDoctrine()->getManager();
        $webShopImage->setIsDeleted(true);
        $em->persist($webShopImage);
        $em->flush();

        return $this->redirectToRoute('site_gallery_list');
    }

    /**
     * @Route("/articles", name="site_article_list")
     */
    public function webShopArticleAction(Request $request)
    {

        $article = new WebShopArticle();

        $form = $this->createForm(WebShopArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article->setCreatedBy($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('site_article_list');
        }

        $webShopArticles = $this->getDoctrine()->getRepository('FrontendBundle:WebShopArticle')->findBy([
            'isDeleted' => false,
        ]);

        return $this->render("@Restaurant/Site/articles.html.twig", [
            'articles' => $webShopArticles,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article/{id}/edit", name="site_article_edit")
     */
    public function webShopArticleEditAction(Request $request, WebShopArticle $webShopArticle)
    {
        if (!$webShopArticle)
            throw new NotFoundHttpException();

        $form = $this->createForm(WebShopArticleType::class, $webShopArticle);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($webShopArticle);
            $em->flush();

        }

        $webShopArticles = $this->getDoctrine()->getRepository('FrontendBundle:WebShopArticle')->findBy([
            'isDeleted' => false,
        ]);

        return $this->render("@Restaurant/Site/articles.html.twig", [
            'articles' => $webShopArticles,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article/{id}/delete", name="site_article_delete")
     */
    public function webShopArticleDeleteAction(Request $request, WebShopArticle $webShopArticle)
    {
        if (!$webShopArticle)
            throw new NotFoundHttpException();

        $em = $this->getDoctrine()->getManager();
        $webShopArticle->setIsDeleted(true);
        $em->persist($webShopArticle);
        $em->flush();

        return $this->redirectToRoute('site_article_list');
    }

}