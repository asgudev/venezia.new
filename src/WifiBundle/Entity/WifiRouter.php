<?php

namespace WifiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Wifi Router
 *
 * @ORM\Table(name="wifi_router")
 * @ORM\Entity()
 */
class WifiRouter
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="router_id", type="string")
     */
    private $routerId;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\Column(name="is_enabled", type="boolean")
     */
    private $isEnabled;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="WifiBundle\Entity\WifiConnect", mappedBy="router")
     */
    private $connects;

    public function __construct()
    {
        $this->isEnabled = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set routerId
     *
     * @param string $routerId
     *
     * @return WifiRouter
     */
    public function setRouterId($routerId)
    {
        $this->routerId = $routerId;

        return $this;
    }

    /**
     * Get routerId
     *
     * @return string
     */
    public function getRouterId()
    {
        return $this->routerId;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return WifiRouter
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return WifiRouter
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return WifiRouter
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Add client
     *
     * @param \WifiBundle\Entity\WifiClient $client
     *
     * @return WifiRouter
     */
    public function addClient(\WifiBundle\Entity\WifiClient $client)
    {
        $this->clients[] = $client;

        return $this;
    }

    /**
     * Remove client
     *
     * @param \WifiBundle\Entity\WifiClient $client
     */
    public function removeClient(\WifiBundle\Entity\WifiClient $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Add connect
     *
     * @param \WifiBundle\Entity\WifiConnect $connect
     *
     * @return WifiRouter
     */
    public function addConnect(\WifiBundle\Entity\WifiConnect $connect)
    {
        $this->connects[] = $connect;

        return $this;
    }

    /**
     * Remove connect
     *
     * @param \WifiBundle\Entity\WifiConnect $connect
     */
    public function removeConnect(\WifiBundle\Entity\WifiConnect $connect)
    {
        $this->connects->removeElement($connect);
    }

    /**
     * Get connects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConnects()
    {
        return $this->connects;
    }
}
