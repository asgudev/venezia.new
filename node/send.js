sequenceCommand = (__command) => {

    return {
        step: 0,
        start: () => {
            sequence.step = 1;
            sequence.send();
        },
        send: () => {
            if (sequence[sequence.step].crc) {
                let __data = Buffer.from(sequence[sequence.step].data.join(''));
                let __packet = Buffer.concat([__data, new Buffer([0x03])]);
                let crc = Buffer.from(calcBufferCRC(__packet), 'utf-8');
                console.log(calcBufferCRC(__packet))
                let __crcPacket = Buffer.concat([new Buffer([0x02]), __packet, crc,]);

                console.log(__crcPacket);
                client.write(__crcPacket);
            } else {
                let __data = sequence[sequence.step].data;
                console.log(__data);
                client.write(new Buffer([__data]));
            }
        },
        next: () => {
            sequence.step += 1;
            sequence.send();
        },
        1: {
            'data': command.ENQ,
            'success': command.ACK,
            'crc': false,
            'error': (data) => {
                console.log(data);
            }
        }, //ENQ
        2: {
            'data': [
                'S*1                                    ',
            ],
            'crc': true,
            'success': command.ACK,
            'error': (data) => {
                console.log(data);
            }
        }, //START PACKET
        3: {
            'data': [
                __command
            ],
            'success': command.ACK,
            'crc': true,
            'error': (data) => {
                console.log(data);
            }
        }, //DATA PACKET
        4: {
            'data': command.EOT,
            'success': command.EOT,
            'crc': false,
            'error': (data) => {
                console.log(data.toString());

            }
        }, //EOT
    }
};