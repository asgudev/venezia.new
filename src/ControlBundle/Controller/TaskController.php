<?php

namespace ControlBundle\Controller;

use ControlBundle\Entity\Task;
use ControlBundle\Entity\TaskComment;
use ControlBundle\Form\TaskCommentType;
use ControlBundle\Form\TaskType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class TaskController extends Controller
{




    /**
     * @Route("/task/list", name="task_list")
     */
    public function taskListAction(Request $request)
    {
        if ($this->isGranted("ROLE_TASK_MANAGER")) {
            $tasks = $this->getDoctrine()->getRepository('ControlBundle:Task')->findAll();
        } else {
            $tasks = $this->getDoctrine()->getRepository('ControlBundle:Task')->findMyTasks($this->getUser());
        }


        return $this->render('@Control/Task/list.html.twig', [
            'tasks' => $tasks,
        ]);

    }

    /**
     * @Route("/task/add", name="task_add")
     */
    public function addTaskAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $task = new Task();
        $task->setDueAt((new \DateTime())->modify("+1 week"));

        $editForm = $this->createForm(TaskType::class, $task);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $task->setCreatedBy($this->getUser());
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('task_view', ['id' => $task->getId()]);
        }


        return $this->render("@Control/Task/edit.html.twig", [
            'task' => $task,
            'form' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/task/{id}", name="task_view")
     */
    public function viewTaskAction(Request $request, Task $task)
    {
        if (!$task)
            throw new NotFoundHttpException();
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createForm(TaskType::class, $task);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($task);
            $em->flush();
        }

        $comment = new TaskComment();
        $commentForm = $this->createForm(TaskCommentType::class, $comment);
        $commentForm->handleRequest($request);

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment->setTask($task);
            $comment->setUser($this->getUser());

            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('task_view', ['id' => $task->getId()]);
        }

        return $this->render("@Control/Task/edit.html.twig", [
            'task' => $task,
            'form' => $editForm->createView(),
            'commentForm' => $commentForm->createView(),
        ]);

    }
    /**
     * @Route("/task/{id}/comment", name="task_comment")
     */
    public function commentTaskAction(Request $request, Task $task)
    {
        if (!$task)
            throw new NotFoundHttpException();

        return new JsonResponse();
    }
}
