<?php

namespace UserBundle\Form\Type\Frontend;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", TextType::class, [
                'label' => "Фамилия Имя",
            ])
            ->add("phone", TextType::class, [
                'label' => "Телефон",
            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Новый пароль',
                'required' => false,
            ])
            ->add("defaultLocale", ChoiceType::class, [
                'label' => "Язык системы",
                'choices' => [
                    'Русский' => 'ru',
                    'Анлийский' => 'en',
                    'Итальянский' => 'it',
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('shop', EntityType::class, [
                'label' => 'Текущее заведение',
                'class' => 'RestaurantBundle\Entity\Shop',
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => false,
            ])
            ->add("adminShops", EntityType::class, [
                'label' => 'Заведения',
                'class' => 'RestaurantBundle\Entity\Shop',
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('isFiscalGranted', CheckboxType::class, [
                'label' => 'Доступ к ФР',
                'required' => false,
            ])
        ;

        $user = $options["user"];
        if ($user->hasRole("ROLE_ADMIN")) {
            $builder
                ->add("enabled", CheckboxType::class, [
                    'label' => 'Активен',
                    'required' => false,
                ])
//                ->add("position", ChoiceType::class, [
//                    'label' => 'Должность',
//                    'choices' => [
//                        'Официант' => 0,
//                        'Администратор' => 1,
//                        'Бухгалтер' => 2,
//                    ],
//                ])
            ;
        }


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user' => null,
            'data_class' => 'UserBundle\Entity\User',
        ));
    }
}