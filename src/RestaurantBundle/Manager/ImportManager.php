<?php

namespace RestaurantBundle\Manager;

use Doctrine\ORM\EntityManager;
use DishBundle\Entity\DishCategory;
use DishBundle\Entity\Dish;
use DishBundle\Entity\DishHistory;
use DishBundle\Entity\DishMeta;

class ImportManager
{
    protected $em;
    protected $shops;
    protected $categories;
    protected $dishes;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->shops = $this->shopByAlias($em->getRepository("RestaurantBundle:Shop")->findAll());
        $this->categories = [];
        $this->dishes = [];
    }

    public function readChildGroup($group, $parent = null)
    {
        if ($this->isCategory($group)) {
            $data = $group->attributes();

            if (!array_key_exists((string)$data["Name"], $this->categories)) {
                $category = new Category();
                $category->setName((string)$data["Name"]);
                $category->setParent($parent);
                $this->em->persist($category);

                $this->categories[(string)$data["Name"]] = $category;
            } else {
                $category = $this->categories[(string)$data["Name"]];
            }
        }

        if ($this->hasChildGroup($group)) {
            foreach ($group->Group as $child) {
                if (isset($category)) {
                    $this->readChildGroup($child, $category);
                } else {
                    $this->readChildGroup($child);
                }
            }
        }
        if (isset($category)) $this->readDishes($group, $category);

        $this->em->flush();
        return $this->dishes;
        //
    }

    private function readDishes($group, $category)
    {
        foreach ($group->dish as $dishData) {
            $data = $dishData->attributes();

            /**
             * @var Dish $dish
             */
            //$dish = $this->em->getRepository("RestaurantBundle:Dish")->findByName((string)$data["Name"]);
            $name = preg_replace("/\s+/", " ", trim((string)$data["Name"]));
            if (!array_key_exists($name, $this->dishes)) {
                $dish = null;
            } else {
                $dish = $this->dishes[$name];
            }

            if ($dish === null) {
                $dish = new Dish();

                $dish->setName($name);
                $dish->setPrice((float)$data["Price"] * 10000);
                $dish->setCategory($category);
                $dish->setDescription((string)$data["Ingredients"]);

                $log = new DishHistory($dish);
                $dish->addChange($log);

                $this->em->persist($log);

                $dishMeta = new DishMeta();
                $dishMeta->setPrice((float)$data["Price"] * 10000);
                $dishMeta->setId1c((int)$data["id"]);

                $shop = $this->shops[(string)$data["Place"]];
                $dishMeta->setShop($shop);

                $logMeta = new DishHistory($dish);
                $logMeta->setPrice($dishMeta->getPrice());
                $logMeta->setShop($shop);
                $dish->addChange($logMeta);

                $this->em->persist($logMeta);

                $shop->addDish($dish);
                $dish->addShop($shop);

                $dishMeta->setDish($dish);
                $dish->addDishMeta($dishMeta);
            } else {
                $dishMeta = new DishMeta();

                $dishMeta->setPrice((float)$data["Price"] * 10000);
                $dishMeta->setId1c((int)$data["id"]);
                $shop = $this->shops[(string)$data["Place"]];
                $dishMeta->setShop($shop);

                if (!$dish->getShops()->contains($shop)) {
                    $shop->addDish($dish);
                    $dish->addShop($shop);

                    $logMeta = new DishHistory($dish);
                    $logMeta->setPrice($dishMeta->getPrice());
                    $logMeta->setShop($shop);
                    $dish->addChange($logMeta);

                    $this->em->persist($logMeta);
                } else {
                    $dishMeta->setWarning(true);
                }

                $dishMeta->setDish($dish);
                $dish->addDishMeta($dishMeta);

            }
            $this->dishes[$dish->getName()] = $dish;

            $this->em->persist($dish);
            $this->em->persist($dishMeta);
            $this->em->persist($shop);
        }
        //$this->em->flush();
    }

    private function hasChildGroup($group)
    {
        if (property_exists($group, "Group")) {
            return true;
        } else {
            return false;
        }
    }

    private function hasDishes($group)
    {
        if (property_exists($group, "dish")) {
            return true;
        } else {
            return false;
        }
    }

    private function isCategory($group)
    {
        if ($group->attributes()) {
            $attr = $group->attributes();
            if ($attr["type"] == "category") {
                return true;
            }
        }
        return false;
    }

    private function shopByAlias($shops)
    {
        $shopsArray = [];
        foreach ($shops as $shop) {
            if ($shop->getALias() != null) {
                $shopsArray[$shop->getAlias()] = $shop;
            }
        }
        return $shopsArray;
    }
}