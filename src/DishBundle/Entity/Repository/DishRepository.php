<?php

namespace DishBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use Gedmo\Translatable\TranslatableListener;
use DishBundle\Entity\DishCategory;
use DishBundle\Entity\Dish;
use RestaurantBundle\Entity\Shop;
use OrderBundle\Entity\OrderDish;

class DishRepository extends EntityRepository
{

    /**
     * @param $ids
     * @return Dish[]
     */
    public function findWithCompositionsByIds($ids) {
        return $this->createQueryBuilder('d')
            ->where('d.id IN (:ids)')
            ->addSelect("c")
            ->addSelect("ci")
            ->addSelect("cii")
            ->leftJoin("d.composition", "c")
            ->leftJoin('c.ingredients', 'ci')
            ->leftJoin("ci.ingredient", "cii")
            ->setParameters([
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();
    }


    public function findByIds($ids) {
        return $this->createQueryBuilder('d')
            ->where('d.id IN (:ids)')
            ->setParameters([
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();
    }


    public function findDishForWeb(Dish $dish) {
        return $this->createQueryBuilder('d')
            ->addSelect('s')
            ->addSelect('c')
            ->where('d.id = :id')
            ->andWhere('s.id not in (0,14)')
            ->leftJoin('d.shops', 's')
            ->leftJoin('d.category', 'c')
            ->setParameters([
                'id' => $dish->getId(),
            ])
            ->getQuery()
            ->getSingleResult();
    }

    public function findByName($name)
    {
        $dish = $this->createQueryBuilder('d')
            ->where("d.name = :name")
            ->setParameters([
                'name' => $name
            ])
            ->getQuery()
            ->getOneOrNullResult();
        return $dish;
    }

    /**
     * @param string $locale
     * @return Dish[]
     */
    public function findAllDishes($locale  = 'ru')
    {
        $dishes = $this->createQueryBuilder('d')
            ->addSelect('dc')
            ->addSelect('ds')
            ->addSelect('dm')
            ->where('d.isDeleted = false')
            ->leftJoin('d.category', 'dc')
            ->leftJoin('d.shops', 'ds')
            ->leftJoin('d.dishMetas', 'dm')
            ->setParameters([

            ]);

        $query = $dishes->getQuery();

        $query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
        $query->setHint(TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(TranslatableListener::HINT_FALLBACK, 1);

        return $query->getResult();
    }

    /**
     * @param DishCategory $category
     * @param string $locale
     * @return Dish[]
     */
    public function findDishesByCategory(DishCategory $category, $locale = 'ru')
    {
        $dishes = $this->createQueryBuilder('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->addSelect('ds')
            ->where('d.category = :category')
            ->andWhere('d.isDeleted = false')
            ->leftJoin('d.category', 'dc')
            ->leftJoin('d.dishMetas', 'dm')
            ->leftJoin('d.shops', 'ds')
            ->setParameters([
                'category' => $category,
            ]);

        $query = $dishes->getQuery();

        $query->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class);
        $query->setHint(TranslatableListener::HINT_TRANSLATABLE_LOCALE, $locale);
        $query->setHint(TranslatableListener::HINT_FALLBACK, 1);

        return $query->getResult();

    }

    public function findItemsInShop($shop)
    {
        $dishes = $this->createQueryBuilder('d')
            ->addSelect("c")
            ->where("d.isDeleted = false")
            ->innerJoin("d.category", "c")
            ->innerJoin("d.shops", "s", "WITH", "s.id = :shop")
            ->setParameter("shop", $shop)
            ->getQuery()
            ->getResult();
        return $dishes;
    }

    public function finbBy1cId($id)
    {
        return $this->createQueryBuilder('d')
            ->where('d.id1c = :id')
            ->orWhere('dm.id1c = :id')
            ->andWhere("d.isDeleted = false")
            ->leftJoin('d.dishMetas', 'dm')
            ->setParameters([
                'id' => $id
            ])
            ->getQuery()
            ->getResult();
    }

    public function findMenuDishesInShop(Shop $shop, $date = "today")
    {
        $qb = $this->createQueryBuilder('d');
        $dishes = $qb
            ->addSelect('dc')
            ->addSelect('dcp')
            ->andWhere('d.isDeleted = false')
            ->innerJoin("d.shops", "s", "WITH", "s.id = :shop")
            ->leftJoin('d.category', 'dc')
            ->leftJoin('dc.parent', 'dcp')
            ->setParameters([
                'shop' => $shop
            ])
            ->getQuery()
            ->getResult();
        return $dishes;
    }

    /**
     * @return Dish[]
     */
    public function findDishesWithShop(){
        return $this->createQueryBuilder('d')
            ->addSelect('ds')
            ->where('d.isDeleted = 0')
            ->leftJoin('d.shops', 'ds')
            ->getQuery()
            ->getResult();

    }

    /**
     * @return Dish[]
     */
    public function findDeletedDishes(){
        return $this->createQueryBuilder('d')
            ->where('d.isDeleted = 1')
            ->getQuery()
            ->getResult();

    }

    public function findConflictsDishes($deleted = false)
    {
        $dishes = $this->createQueryBuilder('d')
            ->where("d.isDeleted = :deleted")
            ->andWhere('dm.warning = 1')
            ->addSelect('dc')
            ->addSelect('ds')
            ->addSelect('dm')
            ->leftJoin('d.category', 'dc')
            ->leftJoin('d.shops', 'ds')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.warning = 1')
            ->setParameters([
                'deleted' => $deleted

            ])
            //->setMaxResults(550)
            ->getQuery()
            ->getResult();
        return $dishes;
    }

    /**
     * @param $name
     * @return Dish[]
     */
    public function findBytitle($name)
    {
        return $this->createQueryBuilder('d')
            ->where('d.name = :name')
            ->setParameters([
                'name' => $name
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Dish[]
     */
    public function findDishesForPizzaCheck(OrderDish $dish)
    {

        return $this->createQueryBuilder('d')
            ->leftJoin('d.category', 'c')
            ->leftJoin('d.shops', 's')
            ->where('c = :cat')
            ->orWhere('c.id = 32')

            ->andWhere('d.name LIKE :name')
            ->andWhere('d.isDeleted = false')
            ->andWhere('s.id = :order_shop')
            ->setParameters([
                'cat' => $dish->getDish()->getCategory(),
                'name' => "%пицца%",
                'order_shop' => $dish->getParentOrder()->getShop(),
            ])
            ->getQuery()
            ->getResult();
    }
}
