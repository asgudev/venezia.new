<?php

namespace DishBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RestaurantBundle\Entity\Warehouse;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Restaurant
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\IngredientTransferRepository")
 */
class IngredientTransfer
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Ingredient")
     */
    private $ingredient;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Warehouse", inversedBy="outcome")
     */
    private $from;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Warehouse", inversedBy="income")
     */
    private $to;

    /**
     * @ORM\Column(type="datetime", nullable=true, name="date")
     */
    private $date;



    /**
     * @ORM\Column(type="datetime", nullable=true, name="date_recieved")
     */
    private $dateRecieved;

    /**
     * @ORM\Column(type="float", nullable=true, name="quantity")
     */
    private $quantity;


    /**
     * @ORM\Column(name="transfer_id", type="string", length=11, nullable=true)
     */
    private $transferId;

    public function __construct($from, $to, $ingredient, $quantity, $transferId, $date)
    {
        $this->quantity = 0;
        $this->setFrom($from);
        $this->setTo($to);
        $this->setIngredient($ingredient);
        $this->setQuantity($quantity);
        $this->setTransferId($transferId);
        $this->date = new \DateTime($date);
        $this->dateRecieved = new \DateTime();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param mixed $ingredient
     * @return IngredientTransfer
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
        return $this;
    }

    /**
     * @return Warehouse
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     * @return IngredientTransfer
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return Warehouse
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     * @return IngredientTransfer
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return IngredientTransfer
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        if ($this->quantity == "") $this->quantity = 0;

        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return IngredientTransfer
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return IngredientTransfer
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateRecieved()
    {
        return $this->dateRecieved;
    }

    /**
     * @param mixed $dateRecieved
     * @return IngredientTransfer
     */
    public function setDateRecieved($dateRecieved)
    {
        $this->dateRecieved = $dateRecieved;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransferId()
    {
        return $this->transferId;
    }

    /**
     * @param mixed $transferId
     * @return IngredientTransfer
     */
    public function setTransferId($transferId)
    {
        $this->transferId = $transferId;
        return $this;
    }

}
