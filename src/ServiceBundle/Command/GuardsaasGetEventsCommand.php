<?php

namespace ServiceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class GuardsaasGetEventsCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('service:guardsaas:events')
            ->setDescription('Get guardsaas events');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $gservice = $this->getContainer()->get('guardsaas.service');

            $gdata = $gservice->getLastEvents();

            foreach ($gdata->items as $_gEvent)
            {

            }

            die();

        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}