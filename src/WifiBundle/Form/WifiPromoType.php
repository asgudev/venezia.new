<?php

namespace WifiBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WifiBundle\Entity\WifiPromo;
use WifiBundle\Entity\WifiRouter;

class WifiPromoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextType::class, [
                'label' => 'Сообщение',
            ])
            ->add('router', EntityType::class, [
                'label' => 'Место',
                'class' => WifiRouter::class,
                'choice_label' => 'description',
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('timeStart', TimeType::class,[
                'label' => "Начало",
                'widget' => 'single_text',

            ])
            ->add('timeEnd', TimeType::class,[
                'label' => "Окончание",
                'widget' => 'single_text',

            ])
            ->add('submit', SubmitType::class,[
                'label' => "Добавить"
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WifiPromo::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'wifi_promo';
    }
}
