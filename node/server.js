const fs = require('fs');
const configHTTPS = {
    key: fs.readFileSync('/etc/letsencrypt/live/admin.venezia.by/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/admin.venezia.by/fullchain.pem'),
    ca: fs.readFileSync('/etc/letsencrypt/live/admin.venezia.by/chain.pem'),
};
var app = require('express')(),
    http = require('http').Server(app),
    https = require('https').createServer(configHTTPS, app),
    io = require('socket.io')(http),
    ioSSL = require('socket.io')(https),
    bodyParser = require('body-parser'),
    YAML = require('yamljs');


var config = YAML.load('../app/config/parameters.yml')



// var barcodeScanner = require('./barcode')(io, config);
//var weightModule = require('./weight')(io, config);
// var terminalModule = require('./terminal')(io, config);


// var testModule = require('./test')(io, config);


https.listen(3000, function () {
    console.log('listening on *:3000');
});

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.post('/updateTable', function (req, res) {
console.log(req,res);
    ioSSL.emit("table_update", req.body);
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('thanks');
});

app.post('/updateTable/dev', function (req, res) {
    ioSSL.emit("table_update_dev", req.body);
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('thanks');
});

app.listen(3001);
