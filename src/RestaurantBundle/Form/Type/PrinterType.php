<?php

namespace RestaurantBundle\Form\Type;

use RestaurantBundle\Entity\Printer;
use RestaurantBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrinterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("shop", EntityType::class, [
                'class' => Shop::class,
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => false,
            ])
            ->add("name", TextType::class, [
                'label' => "Название",
                'required' => true,
                'attr' => [
                    'placeholder' => 'Название'
                ]
            ])
            ->add('transliterate', CheckboxType::class, [
                'label' => 'С переводом',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RestaurantBundle\Entity\Printer',
        ));
    }
}

