<?php

namespace ControlBundle\Entity\Repository;

use ControlBundle\Entity\DishPhoto;
use ControlBundle\Entity\Task;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\User;

class TaskRepository extends EntityRepository
{
    /**
     * @return Task[]
     */
    public function findAll()
    {
        return $this->createQueryBuilder('t')
            ->addSelect('au')
            ->addSelect('u')
            ->leftJoin('t.assignedTo', 'au')
            ->leftJoin('t.createdBy', 'u')
            ->getQuery()
            ->getResult();
    }


    /**
     * @return Task[]
     */
    public function findMyTasks(User $user)
    {
        return $this->createQueryBuilder('t')
            ->addSelect('au')
            ->addSelect('u')
            ->leftJoin('t.assignedTo', 'au')
            ->leftJoin('t.createdBy', 'u')
            ->where('au.id = :user')
            ->orWhere('u = :user')
            ->setParameters([
                'user' => $user->getId(),
            ])
            ->getQuery()
            ->getResult();
    }

}