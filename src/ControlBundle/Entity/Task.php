<?php

namespace ControlBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;


/**
 * Task
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ControlBundle\Entity\Repository\TaskRepository")
 */
class Task
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    const STATUS_NEW = 0;
    const STATUS_READ = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_DECLINED = 3;
    const STATUS_FINISHED = 4;
    const STATUS_CLOSED = 4;

    /**
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $createdBy;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(name="due_at", type="datetime", nullable=false)
     */
    private $dueAt;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User")
     */
    private $assignedTo;

    /**
     * @ORM\Column(name="last_notified_at", type="datetime", nullable=false)
     */
    private $lastNotifiedAt;

    /**
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\OneToMany(targetEntity="ControlBundle\Entity\TaskComment", mappedBy="task")
     */
    private $comments;

    public function __construct()
    {
        $this->status = self::STATUS_NEW;
        $this->createdAt = new \DateTime();
        $this->lastNotifiedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Task
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return Task
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Task
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDueAt()
    {
        return $this->dueAt;
    }

    /**
     * @param mixed $dueAt
     * @return Task
     */
    public function setDueAt($dueAt)
    {
        $this->dueAt = $dueAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    /**
     * @param mixed $assignedTo
     * @return Task
     */
    public function setAssignedTo($assignedTo)
    {
        $this->assignedTo = $assignedTo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastNotifiedAt()
    {
        return $this->lastNotifiedAt;
    }

    /**
     * @param mixed $lastNotifiedAt
     * @return Task
     */
    public function setLastNotifiedAt($lastNotifiedAt)
    {
        $this->lastNotifiedAt = $lastNotifiedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return Task
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return TaskComment[]
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     * @return Task
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }


}