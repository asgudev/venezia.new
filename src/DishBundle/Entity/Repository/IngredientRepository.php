<?php


namespace DishBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use DishBundle\Entity\Ingredient;

class IngredientRepository extends EntityRepository
{
    /**
     * @return Ingredient[]
     */
    public function findForWarehouse() {
        return $this->createQueryBuilder('i')
            ->where("i.isDeleted = 0")
            ->getQuery()
            ->getResult();

    }

    /**
     * @return Ingredient[]
     */
    public function findByName($name)
    {
        return $this->createQueryBuilder('i')
            ->where('i.name LIKE :name')
            ->setParameter("name", "%$name%")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

}
