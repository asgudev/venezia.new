<?php
namespace DishBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="category_discount")
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\CategoryDiscountRepository")
 */
class CategoryDiscount
{

    const TYPE_DISCOUNT = 0;
    const TYPE_PROMO = 1;

    const TYPES = [
        "Процент" => 'auto',
        "N за N" => 'nforn',
    ];

    const DAYS = [
        "Понедельник" => 1,
        'Вторник' => 2,
        'Среда' => 4,
        'Четверг' => 8,
        'Пятница' => 16,
        'Суббота' => 32,
        'Воскресенье' => 64,
    ];
    const DAYS_SHORT = [
        "ПН" => 1,
        'ВТ' => 2,
        'СР' => 4,
        'ЧТ' => 8,
        'ПТ' => 16,
        'СБ' => 32,
        'ВС' => 64,
    ];


    private function daysToArray($n)
    {
        $bin_powers = array();
        for ($bit = 0; $bit < count(self::DAYS); $bit++) {
            $bin_power = 1 << $bit;
            if ($bin_power & $n) $bin_powers[$bit] = $bin_power;
        }
        return $bin_powers;
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\DishCategory", inversedBy="discounts")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\Column(name="discount", type="text", nullable=true)
     */
    private $discount;

    /**
     * @ORM\Column(name="day", type="integer", nullable=true)
     */
    private $day;

    /**
     * @ORM\Column(name="time_start", type="time", nullable=true)
     */
    private $timeStart;

    /**
     * @ORM\Column(name="time_end", type="time", nullable=true)
     */
    private $timeEnd;

    /**
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     *
     * @return CategoryDiscount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set category
     *
     * @param \DishBundle\Entity\DishCategory $category
     *
     * @return CategoryDiscount
     */
    public function setCategory(\DishBundle\Entity\DishCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DishBundle\Entity\DishCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return CategoryDiscount
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set timeStart
     *
     * @param \DateTime $timeStart
     *
     * @return CategoryDiscount
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd
     *
     * @param \DateTime $timeEnd
     *
     * @return CategoryDiscount
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd
     *
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set day
     *
     * @param integer $day
     *
     * @return CategoryDiscount
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return integer
     */
    public function getDay()
    {
        return $this->day;
    }

    public function getDaysArray()
    {
        return $this->daysToArray($this->day);
    }

    /**
     * Get day
     *
     * @return integer
     */
    public function getTextDay()
    {
        $days = [
            -1 => "Все дни",
            1 => "Понедельник",
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье',
        ];
        return $days[$this->day];
    }

    /**
     * Get day
     *
     * @return string
     */
    public function getTextDays()
    {
        $array = $this->getDaysArray();
        $days = [];
        foreach ($array as $day) {
            $days[] = array_search($day, self::DAYS_SHORT);
        }
        return implode(", ", $days);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return CategoryDiscount
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


}
