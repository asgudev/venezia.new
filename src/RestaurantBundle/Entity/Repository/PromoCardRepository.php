<?php

namespace RestaurantBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\PromoCard;

class PromoCardRepository extends EntityRepository
{
    /**
     * @return PromoCard[]
     */
    public function findByType($type)
    {
        return $this->createQueryBuilder('p')
            ->addSelect('o')
            ->leftJoin('p.orders', 'o', 'WITH', 'o.status = 4')
            ->where('p.type = :type')
            ->setParameters([
                'type' => $type,
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @return PromoCard
     */
    public function findCard($code)
    {
        return $this->createQueryBuilder('p')
            ->addSelect('o')
            ->where('p.qrCode = :code')
            ->leftJoin('p.orders', 'o', 'WITH', 'o.status = 4')
            ->setParameters([
                'code' => $code,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * @return PromoCard[]
     */
    public function findEmployeeCards()
    {
        return $this->createQueryBuilder('p')
            ->addSelect('c')
            ->addSelect('o')
            ->addSelect('od')
            ->addSelect('ps')
            ->leftJoin('p.client', 'c')
            ->leftJoin('c.cardPickupShop', 'ps')
            ->leftJoin('p.orders', 'o', 'WITH', 'o.status = 4')
            ->leftJoin('o.dishes', 'od')
            ->where('c.isEmployee = 1')
            ->getQuery()
            ->getResult();

    }

    /**
     * @return PromoCard[]
     */
    public function findIssued($shop = null)
    {
        $cards = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->addSelect('o')
            ->innerJoin('p.client', 'c')
            ->leftJoin('p.orders', 'o')
            ->where('p.issuedAt is not null')
            ->andWhere('p.id != 1094')
            ->andWhere('p.type = 1')
//            ->andWhere('c.id is not null')
            ->orderBy('p.localId', "ASC");


        if ($shop) {
            $cards
                ->andWhere('c.cardPickupShop = :shop')
                ->setParameter('shop', $shop);
        }

        return $cards->getQuery()->getResult();
    }

    /**
     * @return PromoCard[]
     */
    public function findNotIssued($shop = null)
    {
        $cards = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->leftJoin('p.client', 'c')
            ->where('p.issuedAt is null')
            ->andWhere('p.type = 1')
            ->andWhere('c.id is not null')
            ->orderBy('p.localId', "ASC");


        if ($shop) {
            $cards
                ->andWhere('c.cardPickupShop = :shop')
                ->setParameter('shop', $shop);
        }

        return $cards->getQuery()->getResult();
    }


    /**
     * @return PromoCard[]
     */
    public function findActiveCards()
    {
        return $this->createQueryBuilder('p')
            ->addSelect('c')
            ->addSelect('cps.name')
            ->addSelect('(SUM(o.clientMoneyCard + o.clientMoneyCash) + p.cardSum) as cSum')
//            ->addSelect('o')
            ->innerJoin('p.client', 'c')
            ->leftJoin('c.cardPickupShop', 'cps')
            ->leftJoin('p.orders', 'o', 'WITH', 'o.status = 4')
//            ->leftJoin('o.dishes', 'od')
	    ->andWhere("p.id != 1094")
            ->groupBy("c.id")
            ->orderBy("p.createdAt", "DESC")
            ->addOrderBy("o.createdAt", "DESC")
//          ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $phone
     * @return PromoCard
     */
    public function findByPhone($phone)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->where('c.phone = :phone')
            ->leftJoin('p.client', 'c')
            ->setParameters([
                'phone' => $phone
            ]);

        return $qb
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByPhoneAndId($phone, $cardId)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->where('c.phone = :phone')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->eq('p.localId', $cardId),
                $qb->expr()->eq('p.cardId', $cardId)
            ))
            ->leftJoin('p.client', 'c')
            ->setParameters([
                'phone' => $phone
            ]);

        return $qb
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $id
     * @return PromoCard
     */
    public function findByCardId($id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.cardId = :id')
            ->andWhere('p.client is null')
            ->setParameters([
                'id' => $id
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

}
