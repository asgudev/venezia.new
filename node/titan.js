const fetch = require("node-fetch");
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'venezia_new'
});
let bodyParser = require("body-parser");
const express = require('express');
const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


app.post("/order", (request, response) => {

    let __orderId = request.body.order;

    connection.connect();

    connection.query('SELECT * FROM `orders`  WHERE `id` = ?', [__orderId], function (error, results, fields) {
        let order = {};

        order.cash = results[0].client_money_cash;
        order.card = results[0].client_money_card;

        connection.query('SELECT od.*, d.* FROM `order_dish` od LEFT JOIN `dish` d ON d.id = od.dish_id WHERE `order_id` = ?', [__orderId], function (error, results, fields) {
            let sum = 0;

            let __order = {"F": []};


            results.forEach((__dish) => {
                __order.F.push({
                    "S": {
                        "qty": __dish.quantity,
                        "code": __dish.id,
                        "price": __dish.price,
                        "name": __dish.name
                    }
                })
                //console.log(__dish);
            });

            __order.F.push({"P": {"sum": order.cash, "no": "1"}})
            __order.F.push({"P": {"sum": order.card, "no": "4"}})

            console.log(__order.F);

            fetch('http://192.168.31.102:3228/cgi/chk', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(__order)
            }).then(
                response => {
                    console.log(response);
                    if (response.status !== 200) {
                        console.log('Looks like there was a problem. Status Code: ' +
                            response.status);
                        return;
                    }
                    response.json().then(function (data) {
                        console.log(data);
                    });
                }
            );

            connection.end();
            response.send('order');
        });

    });
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
});