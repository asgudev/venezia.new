<?php

namespace FrontendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 */
class WebShopImage
{
    const TYPE_LOGO = 0;
    const TYPE_PREVIEW = 1;
    const TYPE_PANORAMA = 2;
    const TYPE_GALLERY = 3;
    const TYPE_GALLERY_MENU = 4;

    const IMAGE_TYPES = [
        self::TYPE_LOGO => 'Логотип',
        self::TYPE_PREVIEW => 'На главную',
        self::TYPE_PANORAMA => 'Панорама',
        self::TYPE_GALLERY => 'Галерея',
        self::TYPE_GALLERY_MENU => 'Меню',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="FrontendBundle\Entity\WebShop", inversedBy="images")
     */
    private $shop;

    /**
     * @ORM\Column(type="text", name="image", nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="text", name="title", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text", name="description", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @ORM\Column(name="is_deleted", type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="text", name="seo_alt", nullable=true)
     */
    private $seoAlt;


    public function __construct()
    {
        $this->type = self::TYPE_GALLERY;
        $this->isDeleted = false;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     * @return WebShopImage
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return WebShopImage
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return WebShopImage
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getImagePath()
    {
        return $this->getImageUploadDir() . "/" . $this->image;
    }

    protected function getImageUploadDir()
    {
        return 'uploads/images/shop';
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return WebShopImage
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return WebShopImage
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }


    /**
     * @Assert\File(maxSize="60000000")
     */
    private $imageFile;

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;
        // check if we have an old image path
        if (isset($this->image)) {
            // store the old name to delete after the update
            $this->temp = $this->image;
            $this->image = null;
        } else {
            $this->image = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getImageAbsolutePath()
    {
        return null === $this->image
            ? null
            : $this->getImageUploadRootDir() . '/' . $this->image;
    }

    protected function getImageUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getImageUploadDir();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preImageUpload()
    {
        if (null !== $this->getImageFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->image = $filename . '.' . $this->getImageFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }
        $this->getImageFile()->move($this->getImageUploadRootDir(), $this->image);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            //unlink($this->getImageUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->imageFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeImageUpload()
    {
        $file = $this->getImageAbsolutePath();
        if ($file) {
            //unlink($file);
        }
    }

    public function getTypeName($type) {
        return self::IMAGE_TYPES[$type];
    }

    /**
     * @return mixed
     */
    public function getisDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     * @return WebShopImage
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return WebShopImage
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeoAlt()
    {
        return $this->seoAlt;
    }

    /**
     * @param mixed $seoAlt
     * @return WebShopImage
     */
    public function setSeoAlt($seoAlt)
    {
        $this->seoAlt = $seoAlt;
        return $this;
    }


}
