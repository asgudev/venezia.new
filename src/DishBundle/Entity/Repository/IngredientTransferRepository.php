<?php

namespace DishBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use DishBundle\Entity\IngredientTransfer;
use RestaurantBundle\Entity\Shop;

class IngredientTransferRepository extends EntityRepository
{
    /**
     * @return IngredientTransfer[]
     */
    public function findLastTransfersList($dateStart) {
        return $this->createQueryBuilder('it')
            ->where('it.date >= :dateStart')
            ->leftJoin('it.to', 'w')
            ->setParameters([
                'dateStart' => $dateStart,
            ])
            ->orderBy('it.dateRecieved', "DESC")
            ->getQuery()
            ->getResult();
    }


    /**
     * @return IngredientTransfer[]
     */
    public function findAllPizzaInDateRange($dateStart, $dateEnd) {
        return $this->createQueryBuilder('it')
            ->where('it.date >= :dateStart')
            ->andWhere('it.date <= :dateEnd')
            ->andWhere("it.ingredient = 1")
            ->leftJoin('it.to', 'w')
            ->setParameters([
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->getQuery()
            ->getResult();
    }



    /**
     * @return IngredientTransfer[]
     */
    public function findInDateRange($shop, $dateStart, $dateEnd) {
        return $this->createQueryBuilder('it')
            ->where('it.date >= :dateStart')
            ->andWhere('it.date <= :dateEnd')
            ->andWhere('w.shop = :shop')
            ->leftJoin('it.to', 'w')
            ->setParameters([
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
                'shop' => $shop
            ])
            ->getQuery()
            ->getResult();
    }
}