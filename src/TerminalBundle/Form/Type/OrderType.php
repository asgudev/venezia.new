<?php

namespace TerminalBundle\Form\Type;

use RestaurantBundle\Entity\Client;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\ShopPlaceTable;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use OrderBundle\Entity\Order;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\User;

class OrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $shop = $options["shop"];
        $builder
            ->add('promo', EntityType::class, [
                'label' => 'Промо',
                'class' => PromoCard::class,
                'choice_label' => 'cardId',
                'multiple' => false,
                'required' => false,
                'expanded' => false,
            ])
            ->add('dishes', CollectionType::class, [
                'label' => 'Блюда',
                'entry_type' => OrderDishType::class,
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => false,
                'required' => false,
                'entry_options' => [
                    'shop' => $shop,
                ]
            ])
            ->add("place", EntityType::class, [
                'label' => 'Зал',
                'class' => ShopPlace::class,
                'choice_label' => 'name',
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) use ($shop) {
                    return $er->createQueryBuilder('p')
                        ->where('p.shop = :shop')
                        ->orWhere('p.id = 0')
                        ->setParameters([
                            'shop' => $shop,
                        ]);
                },
                'expanded' => false,
                'label_attr' => [
                    'class' => 'inline-label'
                ]
            ])
            ->add("table", EntityType::class, [
                'label' => "Стол",
                'class' => ShopPlaceTable::class,
                'choice_label' => 'name',
                'multiple' => false,
                'query_builder' => function (EntityRepository $er) use ($shop) {
                    return $er->createQueryBuilder('t')
                        ->where('p.shop = :shop')
                        ->leftJoin('t.place', 'p')
                        ->orWhere('t.id = 0')
                        ->setParameters([
                            'shop' => $shop
                        ]);
                },
                'expanded' => false,
                'label_attr' => [
                    'class' => 'inline-label'
                ]
            ])
            ->add("user", EntityType::class, [
                'label' => "Официант",
                'class' => User::class,
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => false,
                'label_attr' => [
                    'class' => 'inline-label'
                ]
            ])
            ->add('meta', HiddenType::class, [
                'mapped' => false,
            ])
//            ->add('cashType', ChoiceType::class, [
//                'label' => 'Оплата',
//                'choices' => [
//                    'Наличными' => Order::CASHTYPE_CASH,
//                    'Счет' => Order::CASHTYPE_INVOICE,
//                ],
//                'multiple' => false,
//                'expanded' => true,
//                'required' => true,
//                'data' => 0,
//            ])
//            ->add('clientMoneyCash')
//            ->add('clientMoneyCard')
//            ->add("client", EntityType::class, [
//                'label' => 'Клиент',
//                'class' => Client::class,
//                'choice_label' => 'name',
//                'multiple' => false,
//                'expanded' => false,
//                'empty_data' => null,
//                'placeholder' => 'Клиент',
//                'required' => false,
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('c')
//                        ->where('c.id != 2')
//                        ->orderBy('c.name', 'ASC');
//                },
//                'attr' => [
//                    'placeholder' => 'Клиент'
//                ]
//            ])
            ->add('save_order', SubmitType::class, [
                'label' => 'Заказать',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-primary disabled',
                ]
            ])
            ->add('update_order', SubmitType::class, [
                'label' => 'Применить',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-primary',
                ]
            ])
            ->add('print_order', SubmitType::class, [
                'label' => 'Счет',
                'attr' => [
                    'class' => 'md-btn md-btn-block',
                ]
            ])
            ->add('print_order_kitchen', SubmitType::class, [
                'label' => 'На кухни',
                'attr' => [
                    'class' => 'md-btn md-btn-block',
                ]
            ])
            ->add('pay_order', SubmitType::class, [
                'label' => 'Рассчитать',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-danger',
                ]
            ])
            ->add('close_order', SubmitType::class, [
                'label' => 'Закрыть',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-danger',
                ]
            ])
            ->add('allow_edit_order', SubmitType::class, [
                'label' => 'Разрешить',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-danger',
                ]
            ])
            ->add('delete_order', SubmitType::class, [
                'label' => 'Удалить',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-danger',
                ]
            ])
            ->add('return_order', SubmitType::class, [
                'label' => 'Восстановить',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-danger',
                ]
            ])
        ;

    }

    public function getDefaultOptions(array $options)
    {
        return array([
            'data_class' => 'OrderBundle\Entity\Order',
            'shop' => null,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OrderBundle\Entity\Order',
            'shop' => null,
        ));
    }


}