<?php

namespace RestaurantBundle\Command;

use ControlBundle\Entity\DishPhoto;
use RestaurantBundle\Entity\PromoCard;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GeneratePromoCardCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('promo:card:generate')
            ->setDescription('Generate promo cards')
            ->addOption('limit', '-l', InputOption::VALUE_REQUIRED, 'Promo card limit');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $doctrine = $this->getContainer()->get('doctrine');
            $validator = $this->getContainer()->get('barcode.validator');
            $em = $doctrine->getManager();
            //$dishPhotos = $doctrine->getRepository('ControlBundle:DishPhoto')->findNotValidated();
//            $dishPhotos[] = $doctrine->getRepository('ControlBundle:DishPhoto')->find(66548);

            $cardHashes = [];
            $n = 0;
            $limit = $input->getOption('limit');
            while ($n <= $limit) {
                $cardId = $validator->getEAN8(str_pad(rand(0, 9999999), 7, 0, STR_PAD_LEFT));
                $cardQr = substr(md5($cardId), 13, 8);

                if (!in_array($cardId, array_keys($cardHashes)) && !in_array($cardQr, $cardHashes)) {
                    $cardHashes[$cardId] = $cardQr;
                    $promoCard = new PromoCard();
                    $promoCard->setCardId($cardId);
                    $promoCard->setQrCode($cardQr);
                    $em->persist($promoCard);
                    $n++;
                }
            }
            $em->flush();

        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}