<?php

namespace FrontendBundle\Service;


class BarcodeValidator
{

    public function getEAN8($code) {
        return $code.$this->checkDigit8($code);
    }

    function checkDigit8($barcode)
    {
        //Compute the check digit
        $sum = 0;
        for ($i = 1; $i <= 5; $i += 2)
            $sum += $barcode[$i];

        for ($i = 0; $i <= 6; $i += 2)
            $sum += 3 * $barcode[$i];

        $r = $sum % 10;
        if ($r > 0)
            $r = 10 - $r;
        return $r;
    }

    function checkDigit13($barcode)
    {
        //Compute the check digit
        $sum = 0;
        for ($i = 1; $i <= 11; $i += 2)
            $sum += 3 * $barcode[$i];
        for ($i = 0; $i <= 10; $i += 2)
            $sum += $barcode[$i];
        $r = $sum % 10;
        if ($r > 0)
            $r = 10 - $r;
        return $r;
    }

    function testCheckDigit($barcode)
    {
        //Test validity of check digit
        $sum = 0;
        for ($i = 1; $i <= 11; $i += 2) {
            $sum += 3 * $barcode[$i];
        }
        for ($i = 0; $i <= 10; $i += 2) {
            $sum += $barcode[$i];
        }


        return ($sum + $barcode[12]) % 10 == 0;
    }
}