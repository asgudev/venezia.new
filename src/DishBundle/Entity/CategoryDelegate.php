<?php

namespace DishBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use RestaurantBundle\Entity\Shop;
use UserBundle\Entity\User;


/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class CategoryDelegate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\DishCategory", inversedBy="delegates")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop", inversedBy="delegates")
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $delegate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param \DishBundle\Entity\DishCategory $category
     *
     * @return CategoryDelegate
     */
    public function setCategory(\DishBundle\Entity\DishCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DishBundle\Entity\DishCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return CategoryDelegate
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set delegate
     *
     * @param \RestaurantBundle\Entity\Shop $delegate
     *
     * @return CategoryDelegate
     */
    public function setDelegate(\RestaurantBundle\Entity\Shop $delegate = null)
    {
        $this->delegate = $delegate;

        return $this;
    }

    /**
     * Get delegate
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getDelegate()
    {
        return $this->delegate;
    }
}
