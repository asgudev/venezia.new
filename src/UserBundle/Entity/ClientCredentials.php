<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RestaurantBundle\Entity\PromoCard;
use Symfony\Component\Validator\Constraints as Assert;
use OrderBundle\Entity\Order;

/**
 * @ORM\Table(name="client_credentials")
 * @ORM\Entity(repositoryClass="UserBundle\Entity\ClientCredentialsRepository")
 */
class ClientCredentials
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(name="sur_name", type="string", length=255, nullable=true)
     */
    private $surName;

    /**
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(name="use_delivery", type="string", length=255, nullable=true)
     */
    private $useDelivery;

    /**
     * @ORM\Column(name="visit_period", type="integer", nullable=true)
     */
    private $visitPeriod;

    /**
     * @ORM\Column(name="gender", type="boolean", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(name="family_status", type="smallint", nullable=true)
     */
    private $familyStatus;

    /**
     * @ORM\Column(name="hasChildren", type="boolean", nullable=true)
     */
    private $hasChildren;

    /**
     * @ORM\Column(name="birth_date", type="date")
     */
    private $birthDate;

    /**
     * @ORM\Column(name="phone", type="string", nullable=true, length=255)
     */
    private $phone;

    /**
     * @ORM\Column(name="email", type="string", nullable=true, length=255)
     */
    private $email;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity="RestaurantBundle\Entity\PromoCard", mappedBy="client")
     */
    private $card;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $cardPickupShop;

    /**
     * @ORM\OneToOne(targetEntity="OrderBundle\Entity\Order")
     */
    private $promoOrder;

    /**
     * @ORM\Column(name="is_employee", type="boolean", nullable=true)
     */
    private $isEmployee;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isActive = false;
        $this->createdAt = new \DateTime();
        $this->isEmployee = false;
    }

    /**
     * @return mixed
     */
    public function getisEmployee()
    {
        return $this->isEmployee;
    }

    /**
     * @param mixed $isEmployee
     * @return ClientCredentials
     */
    public function setIsEmployee($isEmployee)
    {
        $this->isEmployee = $isEmployee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return ClientCredentials
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * @param mixed $surName
     * @return ClientCredentials
     */
    public function setSurName($surName)
    {
        $this->surName = $surName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return ClientCredentials
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUseDelivery()
    {
        return $this->useDelivery;
    }

    /**
     * @param mixed $useDelivery
     * @return ClientCredentials
     */
    public function setUseDelivery($useDelivery)
    {
        $this->useDelivery = $useDelivery;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVisitPeriod()
    {
        return $this->visitPeriod;
    }

    /**
     * @param mixed $visitPeriod
     * @return ClientCredentials
     */
    public function setVisitPeriod($visitPeriod)
    {
        $this->visitPeriod = $visitPeriod;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return ClientCredentials
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFamilyStatus()
    {
        return $this->familyStatus;
    }

    /**
     * @param mixed $familyStatus
     * @return ClientCredentials
     */
    public function setFamilyStatus($familyStatus)
    {
        $this->familyStatus = $familyStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasChildren()
    {
        return $this->hasChildren;
    }

    /**
     * @param mixed $hasChildren
     * @return ClientCredentials
     */
    public function setHasChildren($hasChildren)
    {
        $this->hasChildren = $hasChildren;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     * @return ClientCredentials
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return ClientCredentials
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return ClientCredentials
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return ClientCredentials
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return PromoCard|null
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param mixed $card
     * @return ClientCredentials
     */
    public function setCard($card)
    {
        $this->card = $card;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return ClientCredentials
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCardPickupShop()
    {
        return $this->cardPickupShop;
    }

    /**
     * @param mixed $cardPickupShop
     * @return ClientCredentials
     */
    public function setCardPickupShop($cardPickupShop)
    {
        $this->cardPickupShop = $cardPickupShop;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return ClientCredentials
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return Order
     */
    public function getPromoOrder()
    {
        return $this->promoOrder;
    }

    /**
     * @param mixed $promoOrder
     * @return ClientCredentials
     */
    public function setPromoOrder($promoOrder)
    {
        $this->promoOrder = $promoOrder;
        return $this;
    }
}
