<?php

namespace WifiBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use WifiBundle\Entity\WifiClient;

class WifiClientRepository extends EntityRepository
{

    /**
     * @return mixed
     */
    public function findGroupedBelClientsCount($dateStart, $dateEnd)
    {
        $clients = $this->createQueryBuilder('wc')
            ->select('COUNT(wc) as wcCount')
            ->addSelect('s.id as osId')
            ->addSelect('s.name as osName')
            ->where('wcc.connectedAt BETWEEN :date_start AND :date_end')
            ->andWhere('wc.phone like :phone')
            ->leftJoin('wc.connects', 'wcc')
            ->leftJoin('wcc.router', 'wr')
            ->leftJoin('wr.shop', 's')
            ->setParameters([
                'date_start' => new \DateTime($dateStart),
                'date_end' => new \DateTime($dateEnd),
                'phone' => '+375%'
            ])
            ->addGroupBy('s.id')
        ;

        return $clients->getQuery()->getResult();
    }


    /**
     * @return mixed
     */
    public function findGroupedClientsCount($dateStart, $dateEnd)
    {
        $clients = $this->createQueryBuilder('wc')
            ->select('COUNT(wc) as wcCount')
            ->addSelect('s.id as osId')
            ->addSelect('s.name as osName')
            ->where('wcc.connectedAt BETWEEN :date_start AND :date_end')
            ->leftJoin('wc.connects', 'wcc')
            ->leftJoin('wcc.router', 'wr')
            ->leftJoin('wr.shop', 's')
            ->setParameters([
                'date_start' => new \DateTime($dateStart),
                'date_end' => new \DateTime($dateEnd),
            ])
            ->addGroupBy('s.id')
        ;

        return $clients->getQuery()->getResult();
    }

    /**
     * @return WifiClient[]
     */
    public function findClients($shop = null)
    {
        $clients = $this->createQueryBuilder('wc')
            ->addSelect('wr')
            ->addSelect('s')
            ->leftJoin('wc.router', 'wr')
            ->leftJoin('wr.shop', 's')
            ->where('wc.phone is not null');

        if ($shop != null) {
            $clients->andWhere('s.id = :shop_id')->setParameter('shop_id', $shop->getId());
        }

        $clients->addOrderBy('wc.connectedAt', 'DESC');

        $clients->setMaxResults('20');


        return $clients->getQuery()->getResult();
    }

    /**
     * @return WifiClient[]
     */
    public function findByPhone($phone)
    {
        return $this->createQueryBuilder('wc')
            ->where('wc.phone LIKE :phone')
            ->setParameters([
                'phone' => "%" . $phone . "%",
            ])
            ->getQuery()
            ->getResult();

    }


    /**
     * @return WifiClient
     */
    public function findByMac($mac)
    {
        return $this->createQueryBuilder('c')
            ->where('c.mac = :mac')
            //->andWhere('c.expiredAt > :now')
            //->andWhere('c.phone is null')
            ->setParameters([
                'mac' => $mac,
                //'now' => (new \DateTime('now'))
            ])
            ->getQuery()
            ->getOneOrNullResult();

    }

    public function findClientsInShop($dateStart, $dateEnd, $shop = null)
    {
        $clients = $this->createQueryBuilder('wc')
            ->addSelect('wcc')
            ->where('wcc.connectedAt BETWEEN :date_start AND :date_end')
            ->leftJoin('wc.connects', 'wcc')
            ->leftJoin('wcc.router', 'wr')
            ->setParameters([
                'date_start' => new \DateTime($dateStart),
                'date_end' => new \DateTime($dateEnd),
            ]);

        if ($shop) {
            $clients
                ->andWhere('wr.shop = :shop')->setParameter('shop', $shop);
        }


        return $clients->getQuery()->getResult();
    }

}