<?php

namespace FrontendBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use FrontendBundle\Entity\WebShop;
use FrontendBundle\Entity\WebShopImage;
use RestaurantBundle\Entity\Shop;
use function Doctrine\ORM\QueryBuilder;


class WebShopRepository extends EntityRepository
{
    /**
     * @param WebShop $webShop
     * @return WebShop
     */
    public function findFullShop(WebShop $webShop)
    {
        return $this->createQueryBuilder('ws')
            ->addSelect('wsi')
            ->where('ws = :ws')
            ->leftJoin('ws.images', 'wsi', "WITH", 'wsi.isDeleted = false')
            ->setParameters([
                'ws' => $webShop,
            ])
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @return WebShop[]
     */
    public function findShopsForSite()
    {
        return $this->createQueryBuilder('ws')
            ->addSelect('s')
            ->where('s.id not in (:ids)')
            ->andWhere('wsi.type in (:imageTypes)')
            ->leftJoin('ws.shop', 's')
            ->leftJoin("ws.images", "wsi", "WITH", 'wsi.isDeleted = false')
            ->setParameters([
                'ids' => [0, 14, 16],
                'imageTypes' => [WebShopImage::TYPE_LOGO, WebShopImage::TYPE_PREVIEW, WebShopImage::TYPE_PANORAMA],
            ])
            ->getQuery()
            ->getResult();
    }


    /**
     * @return Shop
     */
    public function findShopBySlug(WebShop $webShop)
    {
        return $this->createQueryBuilder('ws')
            ->addSelect('s')
            ->where('ws = :webShop')
            ->leftJoin('ws.shop', 's')
            ->leftJoin('ws.images', 'wsi', "WITH", 'wsi.isDeleted = false')
            ->setParameters([
                'webShop' => $webShop,
            ])
            ->getQuery()
            ->getResult();
    }

//
//    public function findForSidebar($limit = 20, $locale = 'ru')
//    {
//        $qb = $this->createQueryBuilder('a');
//        return $qb
//            ->addSelect('ap')
//            ->addSelect('af')
//            ->addSelect('ac')
//            ->addSelect('av')
//            ->addSelect('acs')
//            ->andWhere('a.isPublished = 1')
//            ->andWhere('a.locale = :locale')
//            ->andWhere($qb->expr()->orX(
//                $qb->expr()->andX(
//                    $qb->expr()->notIn("a.primary_category", ":ignore"),
//                    $qb->expr()->eq("a.isInNewsList", 1)
//                ),
//                $qb->expr()->andX(
//                    $qb->expr()->in("a.primary_category", ":ignore"),
//                    $qb->expr()->eq("a.isInNewsList", 1)
//                )
//            ))
//            ->andWhere('a.primary_category not in ()')
//            ->leftJoin('a.flatContent', 'af')
//            ->leftJoin('a.content', 'ac')
//            ->leftJoin('a.viewCnt', 'av')
//            ->leftJoin('a.primary_category', 'ap')
//            ->leftJoin('a.secondary_categories', 'acs')
//            ->orderBy('a.publishedAt', "DESC")
//            ->setParameters([
//                'locale' => $locale,
//                'ignore' => [10, 27, 32],
//            ])
//            ->setMaxResults($limit)
//            ->getQuery()
//            ->getResult();
//    }

}
