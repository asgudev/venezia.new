<?php

namespace ApiBundle\Controller;

use DishBundle\Entity\Dish;
use FrontendBundle\Entity\WebOrder;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\ShopPlaceTable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use TerminalBundle\Entity\DailyReport;
use UserBundle\Entity\User;


/**
 * Class OrderController
 * @package ApiBundle\Controller
 */
class OrderController extends Controller
{


    // curl -H 'Authorization: Bearer venezia_adm2' http://127.0.0.1/api/order/list
    /*

    curl -X POST https://reqbin.com/echo/post/json -H 'Content-Type: application/json' -H 'Authorization: Bearer venezia_adm2'  -d
     {"dishes": [{"category": 71, "description": null, "discount": 0, "dish": 13246, "dishDiscount": 0, "dishPrice": 0.2, "id": 13246, "image": [Object], "name": "Стакан", "price": 0.2, "quantity": 1, "time": 1, "weight": "1"}], "isEditable": true, "place": "1", "shop": "1", "table": "3"}
  //   {"cashtype": 0, "createdAt": "2021-08-18 10:02", "dishes": [{"discount": 0, "id": 11220, "name": "Пицца Карпаччо", "orderDishId": 15907761, "price": 19, "quantity": 1, "time": 1}, {"category": 32, "description": "сыр Моццарелла, помидорная смесь, ветчина, огурец маринованный, соус чесночный", "discount": 0, "dish": 11183, "dishDiscount": 0, "dishPrice": 16.5, "id": 11183, "image": [Object], "name": "Пицца Деревенская", "price": 16.5, "quantity": 1, "time": 1, "weight": "500"}], "id": 2904661, "isEditable": true, "localId": 139970, "meta": [], "place": 1, "shop": "1", "table": 19, "user": "Шимчук Ирина"}


    curl -X POST http://192.168.0.14/api/order/2904661/edit -H 'Content-Type: application/json' -H 'Authorization: Bearer venezia_adm2'  -d '{"cashtype": 0, "createdAt": "2021-08-18 10:02", "dishes": [{"discount": 0, "id": 11220, "name": "Пицца Карпаччо", "orderDishId": 15907761, "price": 19, "quantity": 1, "time": 1}, {"category": 32, "description": "сыр Моццарелла, помидорная смесь, ветчина, огурец маринованный, соус чесночный", "discount": 0, "dish": 11183, "dishDiscount": 0, "dishPrice": 16.5, "id": 11183, "image": [Object], "name": "Пицца Деревенская", "price": 16.5, "quantity": 1, "time": 1, "weight": "500"}], "id": 2904661, "isEditable": true, "localId": 139970, "meta": [], "place": 1, "shop": "1", "table": 19, "user": "Шимчук Ирина"}'

     */


    /**
     * @Route("/order/sync", name="api_order_sync")
     * @param Request $request
     * @return JsonResponse
     */
    public function syncOrder(Request $request)
    {

        $data = json_decode($request->getContent(), true);
        $this->get('user.logger')->info($request->getContent());


        $order = $this->get("order.manager")->syncOrder($data);


        return new JsonResponse($this->get("order.manager")->orderToJson($order));
    }


    /**
     * @Route("/order/{id}/request-change", name="api_order_request_change")
     * @param Order $order
     * @return JsonResponse
     */
    public function requestChangeOrder(Order $order)
    {
        $order->setIsChangeRequested(true);
        $order->setIsEditable(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        return new JsonResponse($this->get("order.manager")->orderToJson($order));
    }


    /**
     * @Route("/order/list", name="api_order_list")
     * @param Request $request
     * @return JsonResponse
     */
    public function listOrders(Request $request)
    {
        $request->headers->all();
        $token = preg_replace("#Bearer #", "", getallheaders()["Authorization"]);
        $this->get('user.logger')->info("Authorization " . $token);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => $token,
        ]);


        if (!$user) {
//            throw new AccessDeniedHttpException();
            return new JsonResponse([
                'error' => 'access denied',
            ], Response::HTTP_UNAUTHORIZED);
        }


        $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findTerminalShop($user->getShop()->getId());

        $reportStart = $this->get("redis.helper")->get("shift_start_" . $shop->getId());

        if (!$reportStart) {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($user->getShop());
            if (!$previousReport) {
                $previousReport = new DailyReport($shop);
                $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
            } else {
                $previousReport = $previousReport[0];
            }
            $reportStart = $previousReport->getDateClosed()->format("Y-m-d H:i:s");
            $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $previousReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);
        }


        $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftActive($reportStart, $shop, null, Order::STATUS_WAIT_FOR_CLOSE);


        if ($user->hasRole("ROLE_ADMINISTRATOR")) {
            $closed = $this->getDoctrine()->getRepository("OrderBundle:Order")->findShiftClosed($shop, null, $reportStart);
        } else {
            $closed = [];
        }

        return new JsonResponse([
            'orders' => array_map(function ($order) {
                return $this->get("order.manager")->orderToJson($order);
            }, $orders),
            'closed' => array_map(function ($order) {
                return $this->get("order.manager")->orderToJson($order);
            }, $closed),
        ]);

    }

    /**
     * @Route("/order/create", name="api_order_create")
     * @param Request $request
     * @return JsonResponse
     */
    public function createOrder(Request $request)
    {
        $request->headers->all();
        $token = preg_replace("#Bearer #", "", getallheaders()["Authorization"]);
        $this->get('user.logger')->info("Authorization " . $token);
//var_dump($token);die();
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => $token,
        ]);

        if (!$user) {
            return new JsonResponse([
                'error' => 'access denied',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $em = $this->getDoctrine()->getManager();
        $this->get('user.logger')->info("API ORDER " . $request->getContent());

        $data = json_decode($request->getContent(), true);

/*        if (!isset($data["shop"]) && !isset($data["shopId"])) {
            return new JsonResponse([
                'error' => 'no shop specified',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }*/
//var_dump($data["shop"] ?? $data["shopId"]);die();
//var_dump($data);die();

        if (!$data["shop"] && $data["shopId"]) $data["shop"] = $data["shopId"];

        $shop = $em->getReference(Shop::class, $data["shop"] );

        if ($data["shop"] == $this->getParameter("shop.id") || ($data["shop"] == 11) || !$shop->getVpnServerIP()) {
            $order = $this->get('order.manager')->handleOrder(new Order($user, $shop), $data, $user);
            $em->persist($order);
            $em->flush();

            return new JsonResponse([
                'id' => $order->getId(),
            ]);
        } elseif ($shop->getVpnServerIP()) {
            $shop = $this->getDoctrine()->getRepository(Shop::class)->find($data["shop"]);
            $ch = curl_init("http://" . $shop->getVpnServerIP() . $this->generateUrl('api_order_create'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: ' . $request->headers->get("authorization")]);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);

            $res = curl_exec($ch);
            $data = json_decode($res, true);

            if (!$data) {
                return new JsonResponse([
                    'error' => 'connection problem',
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            } elseif (array_key_exists("error", $data)) {
                return new JsonResponse([
                    'error' => $data['error'],
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            } else {
                return new JsonResponse([
                    'id' => $data['id'],
                ]);
            }
        }
    }

    /**
     * @Route("/order/delegate", name="api_order_delegate")
     * @param Request $request
     * @return JsonResponse
     */
    public function delegateOrder(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $order = new Order($em->getReference(User::class, $data["toUser"]), $em->getReference(Shop::class, $data["toShop"]));
        $order->setTable($em->getReference(ShopPlaceTable::class, $data["table"]));
        $order->setPlace($em->getReference(ShopPlace::class, $data["place"]));

        $dishChanges = [];
        foreach ($data["items"] as $item) {
            $orderDish = new OrderDish();
            $orderDish->setDish($em->getReference(Dish::class, $item["itemId"]));
            $orderDish->setQuantity($em->getReference(Dish::class, $item["quantity"]));
            $orderDish->setOrder($order);
            $dishChanges[] = [
                'dish' => $orderDish,
                'quantity' => $item["quantity"],
            ];
        }

        $this->get("terminal.print.service")->printDishChanges($dishChanges);

        return new JsonResponse([]);
    }


    /**
     * @Route("/order/{id}/edit", name="api_order_edit")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderEditAction(Request $request, Order $order)
    {
        $request->headers->all();

        $token = preg_replace("#Bearer #", "", getallheaders()["Authorization"]);
        $this->get('user.logger')->info("Authorization " . $token);

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => $token,
        ]);

        if (!$user) {
            return new JsonResponse([
                'error' => 'access denied',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $em = $this->getDoctrine()->getManager();

        $order = $this->get("order.manager")->handleOrder($order, json_decode($request->getContent(), true), $user);

        $em->persist($order);
        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @Route("/order/{id}/promo", name="api_order_set_promo")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderSetPromoAction(Request $request, Order $order)
    {
        $data = json_decode($request->getContent(), true);
        $promo = $this->getDoctrine()->getRepository(PromoCard::class)->findOneBy([
            'cardId' => $data['promo']
        ]);

        $order->setPromo($promo);
        $order = $this->get("order.manager")->handleOrder($order, [], $order->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        return new JsonResponse(['success']);
    }

    /**
     * @Route("/order/{id}", name="api_order")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function getOrder(Request $request, Order $order)
    {
        $this->get('user.logger')->info("Authorization " . $request->headers->get("authorization"));

        if (!$order) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($this->get("order.manager")->orderToJson($order));
    }


    /**
     * @Route("/order/{id}/print", name="api_order_print")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderPrintAction(Request $request, Order $order)
    {
        $this->get("terminal.print.service")->printOrder($order);

        $em = $this->getDoctrine()->getManager();
        $order->setStatus(Order::STATUS_WAIT_FOR_MONEY);
        $em->persist($order);
        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @Route("/order/{id}/close", name="api_order_close")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderCloseAction(Request $request, Order $order)
    {
        if (!$order)
            throw new NotFoundHttpException();

        $data = json_decode($request->getContent());
        if ($data) {
            $money = json_decode($request->getContent(), true);
        } else {
            $money = [
                'cash' => $request->get("cash"),
                'card' => $request->get("card"),
            ];
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => str_replace("Bearer ", "", $request->headers->get("Authorization")),
        ]);

        if (!$user)
            throw new AccessDeniedHttpException();

        $result = $this->get("order.manager")->close($order, $money, $user);

        return new JsonResponse(['result' => $result]);
    }

    /**
     * @Route("/order/{id}/restore", name="api_order_restore")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderRestoreAction(Request $request, Order $order)
    {
        if (!$order)
            throw new NotFoundHttpException();

        $em = $this->getDoctrine()->getManager();
        $order->setIsEditable(true);
        $order->setStatus(Order::STATUS_WAIT_FOR_MONEY);
        $em->persist($order);
        $em->flush();

        return new JsonResponse('success');
    }

    /**
     * @Route("/order/{id}/delete", name="api_order_delete")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderDeleteAction(Request $request, Order $order)
    {
        if (!$order)
            throw new NotFoundHttpException();

        $em = $this->getDoctrine()->getManager();
        $order->setIsEditable(true);
        $order->setStatus(Order::STATUS_DELETED);
        $order->setIsEditable(false);
//        $this->printCanceledDishes($order, $order->getDishes());
        $em->persist($order);
        $em->flush();

        return new JsonResponse('success');
    }

    /**
     * @Route("/order/{id}/allow_edit", name="api_order_allow_edit")
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function orderAllowEditAction(Request $request, Order $order)
    {
        if (!$order)
            throw new NotFoundHttpException();

        $em = $this->getDoctrine()->getManager();
        $order->setIsEditable(true);
        $order->setIsChangeRequested(false);
        $em->persist($order);
        $em->flush();

        return new JsonResponse('success');
    }


    /**
     * @Route("/weborder/{id}/printed", name="api_web_order_printed")
     * @param Request $request
     * @param WebOrder $order
     * @return JsonResponse
     */
    public function setWebOrderPrintedAction(Request $request, $id)
    {
        $webOrder = $this->getDoctrine()->getRepository(WebOrder::class)->find($id);
        if ($webOrder) {

            $webOrder->setStatus(Order::STATUS_PRINTED);
            $em = $this->getDoctrine()->getManager();
            $em->persist($webOrder);
            $em->flush();
        }

        $order = $this->getDoctrine()->getRepository(Order::class)->find($id);
        if ($order) {
            $order->setStatus(-1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
        }

        return new JsonResponse([]);
    }

    /**
     * @Route("/weborder/{id}/redirect", name="api_web_order_redirect")
     * @param Request $request
     * @param WebOrder $order
     * @return JsonResponse
     */
    public function redirectWebOrderAction(Request $request, WebOrder $order)
    {
        if (!$order)
            throw new NotFoundHttpException();

        $em = $this->getDoctrine()->getManager();

        $order->setStatus(Order::STATUS_PAID);
        $order->setPickupShop($em->getReference(Shop::class, $request->get('target')));

        $em->persist($order);
        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @Route("/site/{id}/orders", name="api_get_web_orders")
     * @param Request $request
     * @param Shop $shop
     * @return JsonResponse
     */
    public function getWebOrdersAction(Request $request, Shop $shop)
    {

        if (!$shop)
            throw new NotFoundHttpException();

        $webOrders = $this->getDoctrine()->getRepository('FrontendBundle:WebOrder')->findActiveWebOrders($shop);

        $webOrdersObj = [];

        foreach ($webOrders as $order) {
            $__order = [];
            $__order['id'] = $order->getId();
            $__order['createdAt'] = $order->getCreatedAt()->format("c");
            $__order['localId'] = $order->getLocalId();
            $__order['cashType'] = $order->getCashType();
            $__order['pickupTime'] = $order->getPickupTime();
            $__order['phone'] = $order->getPhone();
            $__order['shop'] = $order->getPickupShop()->getId();
            $__order['discount'] = $order->getPromo() ? $order->getPromo()->getCardDiscount() : 0;
            $__order['promo_id'] = $order->getPromo() ? $order->getPromo()->getId() : null;
            $__order['address'] = $order->getAddress();
            $__order['comment'] = $order->getComment();
            $__order['type'] = $order->getType();
            $__order['dishes'] = [];
            foreach ($order->getDishes() as $dish) {
                for ($i = 0; $i < $dish->getQuantity(); $i++) {
                    $__order['dishes'][] = [
                        'id' => $dish->getDish()->getId(),
                        'name' => $dish->getDish()->getName(),
                        'quantity' => 1,
                        'price' => $dish->getDish()->getPrice(),
                        'category' => $dish->getDish()->getCategory()->getId(),
                    ];
                }
            }

            $webOrdersObj[] = $__order;
        }

        $reportStart = $this->get("redis.helper")->get("shift_start_" . $shop->getId());

        if (!$reportStart) {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($shop);
            if (!$previousReport) {
                $previousReport = new DailyReport($shop);
                $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
            } else {
                $previousReport = $previousReport[0];
            }
            $reportStart = $previousReport->getDateClosed()->format("Y-m-d H:i:s");
            $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $previousReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);
        }

        $deliveryOrders = $this->getDoctrine()->getRepository(Order::class)->getDeliveryOrders($shop, $reportStart);

        $deliveryOrdersObj = [];

        foreach ($deliveryOrders as $order) {
            $__order = [];
            $__order['id'] = $order->getId();
            $__order['createdAt'] = $order->getCreatedAt();
            $__order['localId'] = $order->getLocalId();
            $__order['cashType'] = $order->getCashType();
//      $__order['pickupTime'] = $order->getPickupTime();
            $__order['phone'] = $order->getMeta() ? $order->getMeta()->getPhone() : null;
            $__order['shop'] = $shop->getId();

            $__order['ignoreDiscount'] = true;

            $__order['discount'] = 0;
            $__order['promo_id'] = null;
            $__order['address'] = null;
            $__order['comment'] = $order->getMeta() ? $order->getMeta()->getComment() : null;
            $__order['type'] = 0;
            $__order['user'] = $order->getUser()->getId();

            if ($order->getClient()) {
                $__order['client'] = $order->getClient()->getId();
            }

            $__order['dishes'] = [];
            foreach ($order->getDishes() as $dish) {
                $__order['dishes'][] = [
                    'id' => $dish->getDish()->getId(),
                    'name' => $dish->getDish()->getName(),
                    'quantity' => $dish->getQuantity(),
                ];
            }

            $deliveryOrdersObj[] = $__order;
        }

        return new JsonResponse(array_merge($webOrdersObj, $deliveryOrdersObj));
    }

    /**
     * @Route("/delivery/{id}/orders", name="api_get_delivery_orders")
     * @param Request $request
     * @param Shop $shop
     * @return JsonResponse
     */
    public function getDeliveryOrdersAction(Request $request, Shop $shop)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        $reportStart = $this->get("redis.helper")->get("shift_start_" . $shop->getId());

        if (!$reportStart) {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($user->getShop());
            if (!$previousReport) {
                $previousReport = new DailyReport($shop);
                $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
            } else {
                $previousReport = $previousReport[0];
            }
            $reportStart = $previousReport->getDateClosed()->format("Y-m-d H:i:s");
            $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $previousReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);
        }

        $deliveryOrders = $this->getDoctrine()->getRepository(Order::class)->getDeliveryOrders($shop, $reportStart);

        $deliveryOrdersObj = [];

        foreach ($deliveryOrders as $order) {
            $__order = [];
            $__order['id'] = $order->getId();
            $__order['createdAt'] = $order->getCreatedAt();
            $__order['localId'] = $order->getLocalId();
            $__order['cashType'] = $order->getCashType();
//      $__order['pickupTime'] = $order->getPickupTime();
            $__order['phone'] = $order->getMeta() ? $order->getMeta()->getPhone() : null;
            $__order['shop'] = $shop->getId();

            $__order['client'] = $order->getClient();
            $__order['ignoreDiscount'] = true;

            $__order['discount'] = 0;
            $__order['promo_id'] = null;
            $__order['address'] = null;
            $__order['comment'] = $order->getMeta() ? $order->getMeta()->getComment() : null;
            $__order['type'] = 0;
            $__order['user'] = $order->getUser()->getId();

            $__order['dishes'] = [];
            foreach ($order->getDishes() as $dish) {
                $__order['dishes'][] = [
                    'id' => $dish->getDish()->getId(),
                    'name' => $dish->getDish()->getName(),
                    'quantity' => $dish->getQuantity(),
                ];
            }

            $deliveryOrdersObj[] = $__order;
        }

        return new JsonResponse($deliveryOrdersObj);
    }

    /**
     * @Route("/order/{id}/resolve", name="api_order_resolve")
     * @param Request $request
     * @return JsonResponse
     */
    public function orderResolveAction(Request $request, Order $order)
    {
        $order->setStatus(Order::STATUS_DELETED);

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        return new JsonResponse([
            'success' => true,
        ]);
    }
}
