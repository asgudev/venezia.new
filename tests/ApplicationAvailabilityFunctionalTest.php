<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Route;

class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    private function cleanUrl(Route $route, bool $ssl) : string {
        $http = $ssl ? 'https://' : 'http://';
        return $http . $route->getHost() . $this->replaceId($route->getPath());
    }

    public function urlProvider()
    {


    }
}
