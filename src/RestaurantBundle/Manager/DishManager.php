<?php

namespace RestaurantBundle\Manager;

use Doctrine\ORM\EntityManager;

class DishManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    protected $repository;
    protected $dispatcher;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


}