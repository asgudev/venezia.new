<?php


namespace WifiBundle\Services;

use WifiBundle\Entity\WifiClient;

class SmscService
{

    public function __construct()
    {

    }

    public function sendMessage(WifiClient $client, $message)
    {
        if ($client->getPhone() != null) {

            $this->sendSms([
                'recipients' => $client->getPhone(),
                'message' => $message,
                //'urgent' => 1,
                'sender' => 'venezia.by'
            ]);

        }
        return;
    }

    public function sendCode(WifiClient $client)
    {
        $this->sendSms([
            'phones' => $client->getPhone(),
            'mes' => $client->getCode(),
        ]);

        return;
    }

    private function getCurl($method = 'api/user_balance', $data = [])
    {
        $ch = curl_init();

        $url = "https://smsc.ru/sys/send.php";

        $params = [
            'login' => 'veneziaby',
            'psw' => '159753qwe',
        ];

        $params = array_merge($params, $data);

        //dump($params);

        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_FAILONERROR => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params
        ));

        //dump($ch);die();

        return $ch;
    }

    public function sendSms($data)
    {
        $ch = $this->getCurl('', $data);
        $res = curl_exec($ch);
        //dump($res);
        return json_decode($res);
    }

    public function getBalance()
    {
        $ch = $this->getCurl('api/user_balance');
        $res = curl_exec($ch);

        return json_decode($res);

    }
}