<?php

namespace FrontendBundle\Controller;

use DishBundle\Entity\Dish;
use DishBundle\Entity\DishCategory;
use FrontendBundle\Entity\WebOrder;
use FrontendBundle\Entity\WebShop;
use FrontendBundle\Entity\WebShopArticle;
use OrderBundle\Entity\Order;
use RestaurantBundle\Entity\ShopPlaceTable;
use RestaurantBundle\Entity\Workplace;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UserBundle\CookieKeys;
use UserBundle\Entity\ClientCredentials;
use UserBundle\Entity\User;
use UserBundle\Entity\UserEvent;
use UserBundle\Form\Type\Frontend\ClientCredentialsFormType;

class DefaultController extends BaseController
{

    /**
     * @Route("/welcome", name="frontend_user_workplace")
     */
    public function welcomeAction(Request $request)
    {

        $cookies = $request->cookies;
        $uuid = $this->get("session")->get("workplace");

        if (!$cookies->has(CookieKeys::USER_KEY)) {
            return $this->redirectToRoute("frontend_workplace_auth", [
                'uuid' => $uuid,
            ]);
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'uid' => $cookies->get(CookieKeys::USER_KEY),
        ]);

        if (!$user) {
            $response = new Response();
            $response->headers->clearCookie(CookieKeys::USER_KEY, '/', null);

            return $this->render("@Frontend/New_v9/pacs/error.html.twig", [
                'message' => 'Отсканируйте код повторно',
            ], $response);
        }

        $workplace = $this->getDoctrine()->getRepository(Workplace::class)->findOneBy([
            'uuid' => $uuid,
        ]);

        if ($request->getMethod() == "POST") {
            $name = $request->get("name");
            $position = $request->get("position");

            $user->setName($name);

            $user->setPosition($position);

            switch ($user->getPosition()) {
                case "Официант":
                    $user->setEnabled(true);
                    $user->setIsFiscalGranted(false);
                    break;
                case "Администратор":
                    $user->setEnabled(true);
                    $user->addRole("ROLE_ADMINISTRATOR");
                    $user->setIsFiscalGranted(true);
                    break;
                default:
                    $user->setIsFiscalGranted(false);
                    break;
            }

            if ($request->get("password")) {
                $user->setPlainPassword($request->get("password"));
            }

            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
        }


        $lastEvent = $this->getDoctrine()->getRepository(UserEvent::class)->findLastEvent($user);

        $positions = [
            "Администратор" => "Администратор",
            "Официант" => "Официант",
            "Повар" => "Повар",
            "Другое" => "Другое",
        ];

        return $this->render('@Frontend/New_v9/pacs/welcome.html.twig', [
            'user' => $user,
            'workplace' => $workplace,
            'uuid' => $uuid,
            'lastEvent' => $lastEvent,
            'positions' => $positions,
        ]);
    }

    /**
     * @Route("/w/{uuid}/auth", name="frontend_workplace_auth")
     */
    public function workplaceAuthAction(Request $request, $uuid)
    {

        if ($request->getMethod() == "POST") {

            $phone = preg_replace("/[^0-9]/", '', $request->request->get("phone"));
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
                'phone' => $phone,
            ]);

            if (!$user) {
                $user = new User();
                $user->setUsername(md5(rand(0, 100000000)));
                $user->setPlainPassword(123);
                $user->setPhone($phone);


                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($user);
            }

            $cookie = new Cookie(CookieKeys::USER_KEY,
                $user->getUid(),
                (new \DateTime('now'))->modify("+1 year"),
                "/",
                null,
                false,
                false
            );
            $response = new RedirectResponse("/w/$uuid");
            $response->headers->setCookie($cookie);

            return $response;
        }

        return $this->render('@Frontend/New_v9/pacs/auth.html.twig', ['uuid' => $uuid]);
    }

    /**
     * @Route("/w/{uuid}/print", name="frontend_workplace_print")
     */
    public function workplacePrintAction(Request $request, $uuid)
    {
        return $this->render("@Frontend/New_v9/pacs/print.html.twig", ['uuid' => $uuid]);
    }

    /**
     * @Route("/w/{uuid}", name="frontend_workplace")
     */
    public function workplaceAction(Request $request)
    {
        $uuid = $request->get("uuid");

        /* @var Workplace $workplace */
        $workplace = $this->getDoctrine()->getRepository(Workplace::class)->findOneBy(["uuid" => $uuid]);

        $this->get("session")->set("workplace", $uuid);

        if (!$workplace) {
            throw new NotFoundHttpException();
        }
        $cookies = $request->cookies;
        if ($cookies->has(CookieKeys::USER_KEY)) {

            $uid = $cookies->get(CookieKeys::USER_KEY);
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
                'uid' => $uid,
            ]);

            $cams = [
                1 => "http://admin:v2705705@178.172.245.51:10801/ISAPI/Streaming/channels/1/picture",
                11 => "http://admin:12345abc@178.124.159.178:10802/ISAPI/Streaming/channels/1/picture",
            ];

            if ($cams[$workplace->getId()]) {
                $file = $workplace->getId() . "-" . time() . ".jpg";
                $command = 'curl ' . $cams[$workplace->getId()] . " -o '/www/var/events/$file'";
                exec($command);
            }

            /* @var UserEvent $lastEvent */
            $lastEvent = $this->getDoctrine()->getRepository(UserEvent::class)->findLastEvent($user);

            $userEvent = new UserEvent();
            $lastEventDiff = $lastEvent ? $userEvent->getDate()->getTimestamp() - $lastEvent->getDate()->getTimestamp() : 0;

            if ($lastEvent && $lastEventDiff < 300) {
                return $this->redirectToRoute("frontend_user_workplace");
            }

            $isExit = (
                $lastEvent &&
                $lastEvent->getEventType() == UserEvent::EVENT_USER_ENTER &&
                $lastEventDiff <= 16 * 3600
            ) ? UserEvent::EVENT_USER_EXIT : UserEvent::EVENT_USER_ENTER;

            $userEvent
                ->setUser($user)
                ->setEventType($isExit)
                ->setWorkplace($workplace);

            if ($file) {
                $userEvent->setPayload(json_encode(["image" => $file]));
            }

            if ($workplace->getShop()) {
                $user->setShop($workplace->getShop());
            }

            if ($user->getTelegramChatId()) {
                $msg = (new \DateTime())->format("d.m.Y H:i") . " " . $workplace->getName() . " " . ($userEvent->getEventType() == UserEvent::EVENT_USER_EXIT ? "выход" : 'вход');
                $this->get("telegram.service")->sendMessage($user->getTelegramChatId(), $msg);
                $this->get("telegram.service")->sendMessage(304344728, $user->getName() . " " . $msg);
            }

            $user->setEnabled($userEvent->getEventType() == UserEvent::EVENT_USER_ENTER);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->persist($userEvent);
            $em->flush();


        $jsonData = json_encode(["command" => "command", "target" => "shop-" . $workplace->getShop()->getId(), "data" => ['command' => 'update-config']]);
        $nodes = ["http://127.0.0.1:3000/command", "http://127.0.0.1:3001/command"];
        foreach ($nodes as $url) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData)
            ]);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        }

        $response = curl_exec($ch);



            return $this->redirectToRoute("frontend_user_workplace");
        } else {
            return $this->redirectToRoute('frontend_workplace_auth', [
                'uuid' => $uuid,
            ]);
        }
    }

    /**
     * @Route("/t/{uuid}", name="frontend_table")
     */
    public function tableAction(Request $request)
    {
        $uuid = $request->get("uuid");
        $table = $this->getDoctrine()->getRepository(ShopPlaceTable::class)->findOneBy(["uuid" => $uuid]);

        if (!$table) return new JsonResponse(['error'], 500);


        $shop = $table->getPlace()->getShop();


        return $this->render('@Frontend/New_v9/table.html.twig', ["uuid" => $uuid, "table" => $table]);
    }

    /**
     * @Route("/t/{uuid}/c/{type}", name="frontend_call_table")
     */
    public function tableCallAction(Request $request)
    {
        $uuid = $request->get("uuid");
        $type = $request->get("type");

        $table = $this->getDoctrine()->getRepository(ShopPlaceTable::class)->findOneBy(["uuid" => $uuid]);

        if (!$table) return new JsonResponse(['error'], 500);


        $shop = $table->getPlace()->getShop();

        $data = [
            'command' => 'call-waiter',
            'table' => $table->getId(),
            'type' => $type
        ];
        // Encode the data as JSON
        $jsonData = json_encode(["command" => "command", "target" => "shop-" . $shop->getId(), "data" => $data]);

        $nodes = ["http://127.0.0.1:3000/command", "http://127.0.0.1:3001/command"];
        foreach ($nodes as $url) {
        // Initialize cURL
        $ch = curl_init($url);

        // Set cURL options
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);

        // Execute the cURL request and get the response
        $response = curl_exec($ch);

        // Handle cURL error
        if ($response === false) {
            $error = curl_error($ch);
            curl_close($ch);
            return new JsonResponse(['error' => $error], 500);
        }

        // Close cURL resource
        curl_close($ch);
        }
        // Decode the response
        $responseData = json_decode($response, true);

        return new JsonResponse('');
    }


    /**
     * @Route("/map", name="frontend_map")
     */
    public function mapAction(Request $request)
    {
        return $this->render('@Frontend/New_v3/map.html.twig');
    }


    /**
     * @Route("/shops", name="frontend_shops")
     */
    public function indexAction(Request $request)
    {
        $shops = $this->getDoctrine()->getRepository('FrontendBundle:WebShop')->findShopsForSite();

        return $this->render('@Frontend/New_v3/index_new.html.twig', [
            'shops' => $shops,
        ]);
    }

    /**
     * @Route("/promocard", name="frontend_promo_program")
     */
    public function promoProgramAction(Request $request)
    {
        return $this->render('@Frontend/New_v3/promo_doc.html.twig', [

        ]);
    }

    /**
     * @Route("/vacancy", name="frontend_vacancy")
     */
    public function vacancyAction(Request $request)
    {
        $vacancies = $this->getDoctrine()->getRepository('FrontendBundle:WebShopArticle')->findByCategory(WebShopArticle::TYPE_VACANCY);

        return $this->render('@Frontend/New_v3/vacancy_new.html.twig', [
            'vacancies' => $vacancies,
        ]);
    }


    /**
     * @Route("/promos", name="frontend_promos")
     */
    public function promosAction(Request $request)
    {
        $promos = $this->getDoctrine()->getRepository('FrontendBundle:WebShopArticle')->findByCategory(WebShopArticle::TYPE_PROMO);

        $shops = [];
        foreach ($promos as $__promo) {
            $shops[$__promo->getShop()->getid()] = $__promo->getShop();
        }

        return $this->render('@Frontend/New_v3/promos_new.html.twig', [
            'promos' => $promos,
            'shops' => $shops,
        ]);
    }

    /**
     * @Route("/promos/{id}", name="frontend_show_promo")
     */
    public function showPromoAction(Request $request, WebShopArticle $promo)
    {
        if (!$promo)
            throw new NotFoundHttpException();

        return $this->render("@Frontend/New_v3/promo2.html.twig", [
            'promo' => $promo,
        ]);

    }

    /**
     * @Route("/card/login", name="frontend_client_card_login")
     */
    public function clientCardLoginAction(Request $request)
    {
        if ($request->isMethod("POST")) {
            $phone = preg_replace("/[^0-9]/", "", $request->get('phone'));
            $cardId = preg_replace("/[^0-9]/", "", $request->get('card'));

            $card = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findByPhoneAndId($phone, $cardId);

            if ($card) {
                return $this->redirectToRoute('frontend_client_card_cabinet', [
                    'code' => $card->getQrCode(),
                ]);
            } else {
                throw new NotFoundHttpException();
            }
        }


        return $this->render('@Frontend/New_v3/card_login.html.twig', [

        ]);
    }

    /**
     * @Route("/card/{code}", name="frontend_client_card_cabinet")
     */
    public function clientCardCabinetAction(Request $request, $code)
    {
        $card = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findCard($code);
        if (!$card)
            throw new NotFoundHttpException();

        $discount = $this->get("discount_counter.service")->getDiscount($card);

        return $this->render('@Frontend/New_v3/card_cabinet.html.twig', [
            'card' => $card,
            'discount' => $discount,
        ]);
    }

    /**
     * @Route("/promo/{code}", name="frontend_card_promo")
     */
    public function cardPromoAction(Request $request, $code)
    {

        $orderNumbers = $this->getDoctrine()->getRepository('OrderBundle:Order')->findLastOrderNumbers();
        $ids = [];

        /**
         * @var Order $order
         */
        $order = false;
        foreach ($orderNumbers as $number) {
            $id = $number['localId'];
            for ($i = 1; $i <= 20; $i++) {
                $str = $i . "_" . $id . "_" . $i . "_" . $id;
//                $this->get('user.logger')->info($str);
                if (md5($str) == $code) {
                    $order = $this->getDoctrine()->getRepository('OrderBundle:Order')->findLastInShopWithId($i, $id);
                    break 2;
                }
            }
        }

        if (!$order) {
            return $this->render("@Frontend/New_v3/create_card_fail.html.twig", [
                'message' => 'Счет с таким кодом не найден. Попробуйте позже',
            ]);
        }

        /*
        if (($order->getStatus() !== 4) && ($order->getCashType() !== 0)) {
            return $this->render("@Frontend/New_v3/create_card_fail.html.twig", [
                'message' => 'Неверный статус счета',
            ]);
        }
        */

        $hasCard = false;
        foreach ($order->getDishes() as $dish) {
            if ($dish->getDish()->getId() == 13703) $hasCard = true;
        }

        $clientOld = $this->getDoctrine()->getRepository('UserBundle:ClientCredentials')->findBy([
            'promoOrder' => $order
        ]);

        if ($clientOld) {
            return $this->render("@Frontend/New_v3/create_card_fail.html.twig", [
                'message' => 'Данный код уже использован',
            ]);
        }

        if ((($order->getDishSum() >= 30) && ($order->getShop()->getId() != 14)) || ($hasCard)) {

            $client = new ClientCredentials();
            $form = $this->createForm(ClientCredentialsFormType::class, $client);

            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $client->setPhone(preg_replace("/[^0-9]/", "", $client->getPhone()));
                    $client->setPromoOrder($order);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($client);
                    $em->flush();

                    return $this->render("@Frontend/New_v3/create_card_success.html.twig");
                }
            }

            return $this->render('@Frontend/New_v3/create_card.html.twig', [
                'form' => $form->createView(),
            ]);
        } else {
            return $this->render("@Frontend/New_v3/create_card_fail.html.twig", [
                'message' => 'Сумма счета недостаточна',
            ]);
        }
    }


    /**
     * @Route("/dish/{name}", name="frontend_show_dish")
     */
    public function showDishAction(Request $request, Dish $dish)
    {
        if (!$dish) {
            throw new NotFoundHttpException();
        }


        $dish = $this->getDoctrine()->getRepository('DishBundle:Dish')->findDishForWeb($dish);

        return $this->render('@Frontend/New/menu_item_show_new.html.twig', [
            'dish' => $dish,
        ]);
    }

    /**
     * @Route("/category/{id}", name="frontend_show_category")
     */
    public function showCategoryAction(Request $request, DishCategory $category)
    {
        dump($category);
        die();
    }

    /**
     * @Route("/takeaway/change-shop", name="frontend_change_takeaway_shop")
     */
    public function changeTakeawayShopAction(Request $request)
    {
        $this->get("session")->set("takeaway-shop", $request->get("shop"));
        $this->get('session')->set("order", []);


        return new JsonResponse([]);
    }

    /**
     * @Route("/delivery", name="frontend_delivery_menu")
     */
    public function showDeliveryAction(Request $request)
    {
        die();
        $this->get("session")->set("order_type", WebOrder::TYPE_DELIVERY);

        $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find(0);
        $categories = $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findDeliveryMenu($shop);


        $this->get("session.order.manager")->setQuantity(35587, 1);

        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findTakeawayShops();


        return $this->render('@Frontend/New/shop_menu_new.html.twig', [
            'categories' => $categories,
            'shop' => $shop,
            'shops' => $shops,
        ]);
    }

    /**
     * @Route("/", name="frontend_takeaway_menu")
     */
    public function showTakeawayAction(Request $request)
    {

//        return $this->render('@Frontend/New/block.html.twig', [ ]);

        $this->get("session")->set("order_type", WebOrder::TYPE_PICKUP);
        $sessionShop = $this->get("session")->get("takeaway-shop") == null ? 7 : $this->get("session")->get("takeaway-shop");

        $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find($sessionShop);
        $categories = $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findTakeawayMenu($shop);

        $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findTakeawayShops();

//        $this->get("session.order.manager")->changeQuantity(35587, -1);
//"orderSum" => $this->calcOrderSum($order),

        return $this->render('@Frontend/New/shop_menu_new.html.twig', [
            'categories' => $categories,
            'shop' => $shop,
            'shops' => $shops,
        ]);
    }

    /**
     * @Route("/{slug}", name="frontend_show_shop")
     */
    public function showShopAction(Request $request, WebShop $webShop)
    {
        if (!$webShop)
            throw new NotFoundHttpException();

        $this->get('user.logger')->info($webShop);

        $webShop = $this->getDoctrine()->getRepository('FrontendBundle:WebShop')->findFullShop($webShop);

        $categories = [];
        if ($webShop->getShowMenu()) {
            $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findPublicMenuByShop($webShop->getShop(), new \DateTime("now"));
        }


        return $this->render('@Frontend/New_v3/shop_new.html.twig', [
            'shop' => $webShop,
            'categories' => $categories,
        ]);
    }
}
