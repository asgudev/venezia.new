<?php

namespace ServiceBundle\Services;

use RestaurantBundle\Entity\PromoCard;
use Symfony\Component\DependencyInjection\Container;


class DiscountCounter
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getDiscount(PromoCard $card)
    {
        if ($card->getFullSum() >= 2000) {
            $discount = 20;
        } elseif ($card->getFullSum() >= 700) {
            $discount = 10;
        } else {
            $discount = 5;
        }

        if ($card->getCardDiscount() > $discount)
            $discount = $card->getCardDiscount();



        return $discount;
    }

}
