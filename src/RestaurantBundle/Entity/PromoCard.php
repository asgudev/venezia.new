<?php

namespace RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use OrderBundle\Entity\Order;
use UserBundle\Entity\ClientCredentials;
use UserBundle\Entity\User;

/**
 * @ORM\Table(name="promo_cards")
 * @ORM\Entity(repositoryClass="RestaurantBundle\Entity\Repository\PromoCardRepository")
 */
class PromoCard
{

    const TYPE_MANUAL = 0;
    const TYPE_CARD = 1;
    const TYPE_TOURIST = 2;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="card_id", type="string", unique=true, length=8)
     */
    private $cardId;

    /**
     * @ORM\Column(name="local_id", type="string", length=8, nullable=true)
     */
    private $localId;

    /**
     * @ORM\Column(name="card_discount", type="integer")
     */
    private $cardDiscount;

    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\ClientCredentials", inversedBy="card")
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="OrderBundle\Entity\Order", mappedBy="promo")
     */
    private $orders;

    /**
     * @ORM\Column(name="qr_code", type="string", unique=true, length=8)
     */
    private $qrCode;

    /**
     * @ORM\Column(name="card_sum", type="float", nullable=true)
     */
    private $cardSum;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true, options={"default": "0"})
     */
    private $isActive;

    /**
     * @ORM\Column(name="is_visible", type="boolean", nullable=true, options={"default": "0"})
     */
    private $isVisible;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $createdBy;

    /**
     * @ORM\Column(name="issued_at", type="datetime", nullable=true)
     */
    private $issuedAt;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $issuedBy;

    /**
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->isVisible = false;
        $this->isActive = false;
        $this->type = self::TYPE_CARD;
        $this->cardDiscount = 5;
        $this->cardSum = 0;
    }

    /**
     * Add order
     *
     * @param \OrderBundle\Entity\Order $order
     *
     * @return PromoCard
     */
    public function addOrder(\OrderBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \OrderBundle\Entity\Order $order
     */
    public function removeOrder(\OrderBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set cardId
     *
     * @param string $cardId
     *
     * @return PromoCard
     */
    public function setCardId($cardId)
    {
        $this->cardId = $cardId;

        return $this;
    }

    /**
     * Get cardId
     *
     * @return string
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return PromoCard
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set cardDiscount
     *
     * @param integer $cardDiscount
     *
     * @return PromoCard
     */
    public function setCardDiscount($cardDiscount)
    {
        $this->cardDiscount = $cardDiscount;

        return $this;
    }

    /**
     * Get cardDiscount
     *
     * @return integer
     */
    public function getCardDiscount()
    {

        return $this->cardDiscount;
    }

    /**
     * Set isVisible
     *
     * @param boolean $isVisible
     *
     * @return PromoCard
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * Get isVisible
     *
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PromoCard
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return PromoCard
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return PromoCard
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }


    /**
     * @return mixed
     */
    public function getQrCode()
    {
        return $this->qrCode;
    }

    /**
     * @param mixed $qrCode
     * @return PromoCard
     */
    public function setQrCode($qrCode)
    {
        $this->qrCode = $qrCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return PromoCard
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return ClientCredentials
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     * @return PromoCard
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return float
     */
    public function getOrderSum()
    {
        $sum = 0;
        $hasPromo = false;
        if ($this->getClient()) {
            $promoOrder = $this->getClient()->getPromoOrder();

            foreach ($this->getOrders() as $__order) {
//                if ($promoOrder->getId() == $__order->getId()) $hasPromo = true;
                $sum += $__order->getFullSum();
            }

/*            if ($this->getClient()->getPromoOrder())
                if (!$hasPromo)
                    $sum += $this->getClient()->getPromoOrder()->getFullSum();*/
        }
        return $sum;
    }

    /**
     * @return float
     */
    public function getFullSum()
    {
        return $this->getOrderSum() + $this->cardSum;
    }


    /**
     * @return Order
     */
    public function getLastOrder()
    {
        if (count($this->orders) > 0) {
            return $this->orders->first();
        } else {
            return $this->getClient()->getPromoOrder();
        }
    }


    /**
     * @return float
     */
    public function getCardSum()
    {
        return $this->cardSum;
    }

    public function addCardSum($sum)
    {
        $this->cardSum += $sum;
        return $this;
    }

    /**
     * @param mixed $cardSum
     * @return PromoCard
     */
    public function setCardSum($cardSum)
    {
        $this->cardSum = $cardSum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocalId()
    {
        return $this->localId;
    }

    /**
     * @param mixed $localId
     * @return PromoCard
     */
    public function setLocalId($localId)
    {
        $this->localId = $localId;
        return $this;
    }


    /**
     * @return \DateTime
     */
    public function getIssuedAt()
    {
        return $this->issuedAt;
    }

    /**
     * @param mixed $issuedAt
     * @return PromoCard
     */
    public function setIssuedAt($issuedAt)
    {
        $this->issuedAt = $issuedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIssuedBy()
    {
        return $this->issuedBy;
    }

    /**
     * @param mixed $issuedBy
     * @return PromoCard
     */
    public function setIssuedBy($issuedBy)
    {
        $this->issuedBy = $issuedBy;
        return $this;
    }

    public function getMostVisitedShop($return = 'name')
    {
        $shops = [];
        foreach ($this->getOrders() as $order) {
            @$shops[$order->getShop()->getName()] += 1;
        }
        arsort($shops);
        return count($shops) > 0 ? ($return == 'name' ? key($shops) : $shops[key($shops)]) : "";
    }

}
