<?php
namespace RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * ShopTime
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class ShopTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop", inversedBy="times")
     */
    private $shop;

    /**
     * @ORM\Column(name="day_of_week", type="smallint", nullable=false)
     */
    private $dayOfWeek;

    /**
     * @ORM\Column(name="time_start", type="time", nullable=false)
     */
    private $timeStart;

    /**
     * @ORM\Column(name="time_end", type="time", nullable=false)
     */
    private $timeEnd;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dayOfWeek.
     *
     * @param int $dayOfWeek
     *
     * @return ShopTime
     */
    public function setDayOfWeek($dayOfWeek)
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    /**
     * Get dayOfWeek.
     *
     * @return int
     */
    public function getDayOfWeek()
    {
        return $this->dayOfWeek;
    }

    /**
     * Set timeStart.
     *
     * @param \DateTime $timeStart
     *
     * @return ShopTime
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart.
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd.
     *
     * @param \DateTime $timeEnd
     *
     * @return ShopTime
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd.
     *
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set shop.
     *
     * @param \RestaurantBundle\Entity\Shop|null $shop
     *
     * @return ShopTime
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop.
     *
     * @return \RestaurantBundle\Entity\Shop|null
     */
    public function getShop()
    {
        return $this->shop;
    }
}
