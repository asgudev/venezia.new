<?php

namespace RestaurantBundle\Service;

use Doctrine\ORM\EntityManager;
use DishBundle\Entity\IngredientInWarehouse;
use DishBundle\Entity\IngredientTransfer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportParser
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
        $this->doctrine = $container->get('doctrine');
    }

    public function parseFile($fileName)
    {
        $xml = $this->loadXML($fileName);

        $this->parseTransfer($xml);
        $this->parseProduction($xml);
    }

    public function loadXML($fileName)
    {
        //curl -T 1.xml http://admin.venezia.by/api/upload/document
        $path = realpath(__DIR__ . "/../../../var/import/" . $fileName);
        $plainXML = file_get_contents($path);;
        $formatted = preg_replace("#v8msg:#", "", $plainXML);
        $formatted = preg_replace("#&#", "&amp;", $formatted);
        $xml = simplexml_load_string($formatted);

        return $xml;
    }

    private function rollbackTransferData($transfers)
    {
        /**
         * @var IngredientTransfer[] $transfers
         */
        foreach ($transfers as $__transfer) {

            $this->updateWarehouse($__transfer, true);
            $this->em->remove($__transfer);
            $this->em->flush();
        }
    }

    private function updateWarehouse(IngredientTransfer $transfer, $rollback = false)
    {
        //INSERT INTO `ingredient_transfer` (`id`, `ingredient_id`, `from_id`, `to_id`, `date`, `date_recieved`, `quantity`, `transfer_id`) VALUES (NULL, '15', NULL, '1', NULL, NULL, '3', '25444');

        /** @var IngredientInWarehouse $ingredientInTargetWarehouse */
        $ingredientInTargetWarehouse = $this->doctrine->getRepository('DishBundle:IngredientInWarehouse')->findOneBy([
            'ingredient' => $transfer->getIngredient(),
            'warehouse' => $transfer->getTo(),
        ]);

        if ($ingredientInTargetWarehouse == null) {
            $ingredientInTargetWarehouse = new IngredientInWarehouse($transfer->getIngredient(), $transfer->getTo());
        }

        $ingredientInTargetWarehouse->addQuantity(($rollback ? -1 : 1) * $transfer->getQuantity());

        $this->em->persist($ingredientInTargetWarehouse);

        if ($transfer->getFrom() != null) {
            /** @var IngredientInWarehouse $ingredientInTargetWarehouse */
            $ingredientInSourceWarehouse = $this->doctrine->getRepository('DishBundle:IngredientInWarehouse')->findOneBy([
                'ingredient' => $transfer->getIngredient(),
                'warehouse' => $transfer->getFrom(),
            ]);
            if ($ingredientInSourceWarehouse) {
                $ingredientInSourceWarehouse->addQuantity(($rollback ? 1 : -1) * $transfer->getQuantity());
                $this->em->persist($ingredientInSourceWarehouse);
            }
        }
        $this->em->flush();
    }

    private function parseProduction($xml)
    {
        if (($xml->Body) && ($xml->Body->{'ю23_ГотоваяПродукция'})) {
            foreach ($xml->Body->{'ю23_ГотоваяПродукция'} as $__ingrMove) {
                $transferId = (string)$__ingrMove->{'Номер'};
                $existingTransfers = $this->doctrine->getRepository('DishBundle:IngredientTransfer')->findBy([
                    'transferId' => $transferId,
                ]);

//var_dump($existingTransfers[0]->getId());die();
                if ($existingTransfers || $__ingrMove->{'ПометкаУдаления'} == "1") {
                    $this->rollbackTransferData($existingTransfers);
                }

                if ($__ingrMove->{'ПометкаУдаления'} == "1") continue;

                foreach ($__ingrMove->{'ТЧ'}->{'Row'} as $__ingr) {
                    $id1c = (string)$__ingr->{'Блюдо'};
                    $ingredient = $this->doctrine->getRepository('DishBundle:Ingredient')->findOneBy([
                        'id1c' => $id1c,
                    ]);

                    if ($ingredient) {
                        $fromWarehouse = null;
                        $toWarehouse = $this->doctrine->getRepository('RestaurantBundle:Warehouse')->findOneBy([
                            'name1c' => (string)$__ingrMove->{'МестоХранения'},
                        ]);

                        $ingredientTransfer = new IngredientTransfer($fromWarehouse, $toWarehouse, $ingredient, (string)$__ingr->{'Количество'}, $transferId, (string)$__ingrMove->{'Дата'});
                        $this->em->persist($ingredientTransfer);
                        $this->updateWarehouse($ingredientTransfer);
                        $this->em->flush();
                    }
                }
            }
        }
    }


    private function parseTransfer($xml)
    {
        if (($xml->Body) && ($xml->Body->{'ПеремещениеМХ'})) {
            foreach ($xml->Body->{'ПеремещениеМХ'} as $__ingrMove) {
                $transferId = (string)$__ingrMove->{'Номер'};

                $existingTransfers = $this->doctrine->getRepository('DishBundle:IngredientTransfer')->findBy([
                    'transferId' => $transferId,
                ]);

                if ($existingTransfers) {
                    $this->rollbackTransferData($existingTransfers);
                }

                foreach ($__ingrMove->{'ТЧ'}->{'Row'} as $__ingr) {
                    if ($xml->Body->{'ПеремещениеМХ'}->{'Склад'} == 'Мороженое Венеция') {
                        $ingredient = $this->doctrine->getRepository('DishBundle:Ingredient')->findOneBy([
                            'id1c' => (string)$__ingr->{'Товар'},
                        ]);

                        if ($ingredient) {
                            $fromWarehouse = $this->doctrine->getRepository('RestaurantBundle:Warehouse')->findOneBy([
                                'name1c' => (string)$__ingrMove->{'Склад'},
                            ]);
                            $toWarehouse = $this->doctrine->getRepository('RestaurantBundle:Warehouse')->findOneBy([
                                'name1c' => (string)$__ingrMove->{'Склад2'},
                            ]);
                            $ingredientTransfer = new IngredientTransfer($fromWarehouse, $toWarehouse, $ingredient, (string)$__ingr->{'КоличествоКг'}, $transferId, (string)$__ingrMove->{'Дата'});
                            $this->em->persist($ingredientTransfer);
                            $this->updateWarehouse($ingredientTransfer);

                            $this->em->flush();
                        }
                    } else {
                        $id1c = (string)$__ingr->{'Товар'};
var_dump($id1c);
                        if (in_array((string)$__ingr->{'Товар'}, ['19990', '1783'])) {
                            $id1c = "19990";
                        }
var_dump('---'.$id1c);
                        $ingredient = $this->doctrine->getRepository('DishBundle:Ingredient')->findOneBy([
                            'id1c' => $id1c,
                        ]);
                        if ($ingredient) {
                            $transferId = (string)$__ingrMove->{'Номер'};

                            $fromWarehouse = $this->doctrine->getRepository('RestaurantBundle:Warehouse')->findOneBy([
                                'name1c' => (string)$__ingrMove->{'Склад'},
                            ]);
                            $toWarehouse = $this->doctrine->getRepository('RestaurantBundle:Warehouse')->findOneBy([
                                'name1c' => (string)$__ingrMove->{'Склад2'},
                            ]);

                            $quantity = (string)$__ingr->{'КоличествоКг'};
var_dump($quantity);
                            if ($quantity != "") {
                                $ingredientTransfer = new IngredientTransfer($fromWarehouse, $toWarehouse, $ingredient, $quantity, $transferId, (string)$__ingrMove->{'Дата'});

                                $this->updateWarehouse($ingredientTransfer);
                                $this->em->persist($ingredientTransfer);
                            }
                        }

                    }
                }
            }
            $this->em->flush();
        }
    }
}
