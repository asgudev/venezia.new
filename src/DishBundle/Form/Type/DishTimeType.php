<?php

namespace DishBundle\Form\Type;

use DishBundle\Entity\DishTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishTimeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dayOfWeek', ChoiceType::class, [
                'label' => "День недели",
                'choices' => [
                    'Понедельник' => 1,
                    'Вторник' => 2,
                    'Среда' => 3,
                    'Четверг' => 4,
                    'Пятница' => 5,
                    'Суббота' => 6,
                    'Воскресенье' => 0,
                ],
            ])
            ->add('timeStart', TimeType::class, [
                'label' => 'Открытие',
                'widget' => 'single_text',
            ])
            ->add('timeEnd', TimeType::class, [
                'label' => 'Закрытие',
                'widget' => 'single_text',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DishTime::class,
        ));
    }
}