<?php

namespace RestaurantBundle\Command;

use ControlBundle\Entity\DishPhoto;
use ControlBundle\Entity\DishPhotoCheck;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PhotoRecognizeCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('photo:recognize')
            ->setDescription('Photo auto recognize');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $doctrine = $this->getContainer()->get('doctrine');
            $em = $doctrine->getManager();
            $photoPath = $this->getContainer()->get('kernel')->getRootDir() . '/../web/uploads/';

            /** @var DishPhoto $dishPhoto */
            $dishPhotos = $doctrine->getRepository('ControlBundle:DishPhoto')->findNotPredicted();

            foreach ($dishPhotos as $dishPhoto) {
                $filePath = $photoPath . "/photos/" . $dishPhoto->getPhotoPath();

                if (file_exists($filePath)) {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, '37.139.3.27');
                    curl_setopt($ch, CURLOPT_PORT, 61228);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, [
                        'photo' => curl_file_create(realpath($filePath))
                    ]);
                    $result = curl_exec($ch);
                    curl_close($ch);
                    if ($result) {
                        $data = json_decode($result, true);
                        $box = $data['box'];
                        $predicts = $data['predicts'];
                        $predictObj = [];
                        foreach ($predicts as $__predict) {
                            $predictObj[$__predict[0]] = $__predict[1];
                        }
                        $dishPhoto->setPredict($predictObj);
                        $em->persist($dishPhoto);
                        $em->flush();

                        $src_image = imagecreatefromjpeg($filePath);
                        $dst_image = imagecreatetruecolor(640, 640);
                        imagecopyresampled($dst_image, $src_image,
                            0, 0,
                            $box[1], $box[0],
                            640, 640,
                            $box[3] - $box[1], $box[2] - $box[0]
                        );
                        if ($dst_image !== FALSE) {
                            imagejpeg($dst_image, str_replace("photos", "cropped", $filePath));
                            imagedestroy($dst_image);
                        }
                        imagedestroy($src_image);
                    }
                }
            }
        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}