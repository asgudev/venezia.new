<?php

namespace ReportBundle\Form\Type;


use ReportBundle\Entity\Report;
use ReportBundle\Entity\ReportData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название отчета',
                'required' => true,
            ])
            ->add('data', CollectionType::class, [
                'label' => 'Показатели',
                'entry_type' => ReportDataType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Отправить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Report::class,
        ));
    }
}