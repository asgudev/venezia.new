<?php

namespace RestaurantBundle\Service;

class TOTP
{

    public function __construct()
    {

    }

    public function getToken($key, $options = [])
    {
        $options["period"] = $options["period"] ?? 30;
        $options["algorithm"] = $options["algorithm"] ?? 'SHA-1';
        $options["digits"] = $options["digits"] ?? 6;
        $key = $this->stringToHex($key);

        $options["timestamp"] = $options["timestamp"] ?? time() * 1000;
        $epoch = floor($options["timestamp"] / 1000);

        $time = str_pad($this->dec2hex(floor($epoch / $options["period"])), 16, '0', STR_PAD_LEFT);

//        $shaObj = new JsSHA($options["algorithm"], 'HEX');
//        $shaObj->setHMACKey($key, 'HEX');
//        $shaObj->update($time);

        dump($key, $time);

        $hmac = hash_hmac('sha1', $data, $key);

        dump($hmac);
        die();

        $offset = $this->hex2dec(substr($hmac, -1));
        $otp = ($this->hex2dec(substr($hmac, $offset * 2, 8)) & $this->hex2dec('7fffffff')) . '';
        $otp = substr($otp, max(strlen($otp) - $options["digits"], 0), $options["digits"]);
        return $otp;
    }

    private function stringToHex($str)
    {
        $hex = '';
        for ($i = 0; $i < strlen($str); $i++) {
            $charCode = dechex(ord($str[$i]));
            $hex .= str_pad($charCode, 2, '0', STR_PAD_LEFT);
        }
        return $hex;
    }

    private function dec2hex($s)
    {
        return ($s < 15.5 ? '0' : '') . dechex(round($s));
    }

    private function hex2dec($s)
    {
        return hexdec($s);
    }

    private function hmacsha1($key, $data)
    {
        $blocksize = 64;
        $hashfunc = 'sha1';
        if (strlen($key) > $blocksize)
            $key = pack('H*', $hashfunc($key));
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack(
            'H*', $hashfunc(
                ($key ^ $opad) . pack(
                    'H*', $hashfunc(
                        ($key ^ $ipad) . $data
                    )
                )
            )
        );
        return bin2hex($hmac);
    }

    private function leftpad($str, $len, $pad)
    {
        if ($len + 1 >= strlen($str)) {
            $str = str_repeat($pad, $len + 1 - strlen($str)) . $str;
        }
        return $str;
    }
}