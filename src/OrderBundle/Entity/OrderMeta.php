<?php

namespace OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Doctrine\Common\Collections\ArrayCollection;

use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\ShopPlaceTable;
use UserBundle\Entity\User;

/**
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class OrderMeta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="OrderBundle\Entity\Order", mappedBy="meta")
     */
    private $order;

    /**
     * @ORM\Column(type="text", nullable=true, name="time")
     */
    private $time;

    /**
     * @ORM\Column(type="text", nullable=true, name="phone")
     */
    private $phone;

    /**
     * @ORM\Column(type="text", nullable=true, name="comment")
     */
    private $comment;

    /**
     * @ORM\Column(type="text", name="other", nullable=true)
     */
    private $other;

    /**
     * @ORM\Column(type="integer", name="web_order", nullable=true)
     */
    private $webOrder;

    /**
     * @ORM\Column(type="integer", name="order_type", nullable=true)
     */
    private $orderType;

    /**
     * @ORM\Column(type="text", name="confirm_url", nullable=true)
     */
    private $confirmUrl;

    /**
     * @ORM\Column(type="text", name="reject_url", nullable=true)
     */
    private $rejectUrl;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     * @return OrderMeta
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return OrderMeta
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     * @return OrderMeta
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     * @return OrderMeta
     */
    public function setComment($comment)
    {
        $this->comment = preg_replace("#[\r\n]#s", "", $comment);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param mixed $other
     * @return OrderMeta
     */
    public function setOther($other)
    {
        $this->other = $other;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWebOrder()
    {
        return $this->webOrder;
    }

    /**
     * @param mixed $webOrder
     * @return OrderMeta
     */
    public function setWebOrder($webOrder)
    {
        $this->webOrder = $webOrder;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * @param mixed $orderType
     * @return OrderMeta
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getConfirmUrl()
    {
        return $this->confirmUrl;
    }

    /**
     * @param mixed $confirmUrl
     * @return OrderMeta
     */
    public function setConfirmUrl($confirmUrl)
    {
        $this->confirmUrl = $confirmUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRejectUrl()
    {
        return $this->rejectUrl;
    }

    /**
     * @param mixed $rejectUrl
     * @return OrderMeta
     */
    public function setRejectUrl($rejectUrl)
    {
        $this->rejectUrl = $rejectUrl;
        return $this;
    }

}
