<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('resto:import')
            ->setDescription('Update files entities');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
/*
            $doctrine = $this->getContainer()->get("doctrine");

            $em = $doctrine->getManager();

            $dishes = $em->getRepository("DishBundle:Dish")->findAll();
            //$shop = $em->getRepository("RestaurantBundle:Shop")->find(2);

            foreach ($dishes as $dish) {
                $log = new DishHistory($dish);
                $dish->addChange($log);

                $em->persist($log);
                $em->persist($dish);
            }
            $em->flush();


            die();*/
            $plainXML = file_get_contents("web/import/import.xml");

            $xml = simplexml_load_string($plainXML);

            $im = $this->getContainer()->get("restaurant.dish.import.manager");

            $im->readChildGroup($xml);

            //dump($xml);
            die();
        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}