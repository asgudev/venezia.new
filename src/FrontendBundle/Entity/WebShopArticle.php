<?php

namespace FrontendBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\DateTime;
use OrderBundle\Entity\Order;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;


/**
 * @ORM\Entity(repositoryClass="FrontendBundle\Entity\Repository\WebShopArticleRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 */
class WebShopArticle
{
    const TYPE_NEWS = 0;
    const TYPE_PROMO = 1;
    const TYPE_VACANCY = 2;


    const CATEGORY_TYPES = [
        self::TYPE_NEWS => 'Новость',
        self::TYPE_PROMO => 'Акция',
        self::TYPE_VACANCY => 'Вакансия',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="category", type="smallint", nullable=true)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="FrontendBundle\Entity\WebShop", inversedBy="articles")
     */
    private $shop;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $createdBy;

    /**
     * @ORM\Column(name="is_published", type="boolean", nullable=true)
     */
    private $isPublished;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(name="additional", type="text", nullable=true)
     */
    private $additional;

    /**
     * @ORM\Column(name="image", type="text", nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="text", name="seo_image_alt", nullable=true)
     */
    private $seoImageAlt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isPublished = true;
        $this->isDeleted = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     * @return WebShopArticle
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return WebShopArticle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return WebShopArticle
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisPublished()
    {
        return $this->isPublished;
    }

    /**
     * @param mixed $isPublished
     * @return WebShopArticle
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     * @return WebShopArticle
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return WebShopArticle
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return WebShopArticle
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return WebShopArticle
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }


    /**
     * @Assert\File(maxSize="60000000")
     */
    private $imageFile;

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;
        // check if we have an old image path
        if (isset($this->image)) {
            // store the old name to delete after the update
            $this->temp = $this->image;
            $this->image = null;
        } else {
            $this->image = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getImageAbsolutePath()
    {
        return null === $this->image
            ? null
            : $this->getImageUploadRootDir() . '/' . $this->image;
    }

    protected function getImageUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getImageUploadDir();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preImageUpload()
    {
        if (null !== $this->getImageFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->image = $filename . '.' . $this->getImageFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }
        $this->getImageFile()->move($this->getImageUploadRootDir(), $this->image);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            //unlink($this->getImageUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->imageFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeImageUpload()
    {
        $file = $this->getImageAbsolutePath();
        if ($file) {
            //unlink($file);
        }
    }

    public function getTypeName($type) {
        return self::CATEGORY_TYPES[$type];
    }


    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return WebShopArticle
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getImagePath()
    {
        return $this->getImageUploadDir() . "/" . $this->image;
    }

    protected function getImageUploadDir()
    {
        return 'uploads/images/articles';
    }


    /**
     * @return mixed
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @param mixed $additional
     * @return WebShopArticle
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getSeoImageAlt()
    {
        return $this->seoImageAlt;
    }

    /**
     * @param mixed $seoImageAlt
     * @return WebShopArticle
     */
    public function setSeoImageAlt($seoImageAlt)
    {
        $this->seoImageAlt = $seoImageAlt;
        return $this;
    }


}
