<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReportGenerateNamesCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('restaurant:report:generate:names')
            ->setDescription('Export data by report')
            ->addOption('report', '-r', InputOption::VALUE_REQUIRED, 'Export report');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $spool_path = $this->getContainer()->get('kernel')->getRootDir() . '/../var/export/';
        //$date = new \DateTime($input->getOption('date'));

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $reports = $doctrine->getRepository('TerminalBundle:DailyReport')->findBy([
            'name' => null,
        ]);


        try {

           foreach ($reports as $report) {

               $firstOrder = $doctrine->getRepository('OrderBundle:Order')->findFirstOrderInReport($report);
               if (!$firstOrder) continue;
               $report->setName($firstOrder->getCreatedAt()->format('d.m.Y'));
               $em->persist($report);
           }


            $em->flush();

            $output->write('success');

        } catch (\Exception $em) {
            dump($em);
            $output->write('failed');
        }
    }
}
