<?php

namespace WifiBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use WifiBundle\Entity\WifiClient;

class WifiConnectRepository extends EntityRepository
{

    /**
     * @return WifiClient[]
     */
    public function findPromoCount($dateStart, $dateEnd)
    {
        $clients = $this->createQueryBuilder('wc')
            ->select('COUNT(wc) as promoCount')
            ->addSelect('s.id as osId')
            ->addSelect('s.name as osName')
            ->where('wc.connectedAt BETWEEN :date_start AND :date_end')
            ->andWhere('wc.promoSent is not null')
            ->leftJoin('wc.router', 'wr')
            ->leftJoin('wr.shop', 's')
            ->setParameters([
                'date_start' => new \DateTime($dateStart),
                'date_end' => new \DateTime($dateEnd),
            ])
            ->addGroupBy('s.id');

        return $clients->getQuery()->getResult();
    }


    /**
     * @return mixed
     */
    public function findGroupedBelClientsCount($dateStart, $dateEnd)
    {
        $clients = $this->createQueryBuilder('wc')
            ->select('COUNT(DISTINCT(wcc)) as wcCount')
            ->addSelect('s.id as osId')
            ->addSelect('s.name as osName')
            ->where('wc.connectedAt BETWEEN :date_start AND :date_end')
            ->andWhere('wcc.phone like :phone')
            ->leftJoin('wc.client', 'wcc')
            ->leftJoin('wc.router', 'wr')
            ->leftJoin('wr.shop', 's')
            ->setParameters([
                'date_start' => new \DateTime($dateStart),
                'date_end' => new \DateTime($dateEnd),
                'phone' => '+375%'
            ])
            ->addGroupBy('s.id')
        ;

        return $clients->getQuery()->getResult();
    }


    /**
     * @return mixed
     */
    public function findGroupedClientsCount($dateStart, $dateEnd)
    {
        $clients = $this->createQueryBuilder('wc')
            ->select('COUNT(DISTINCT(wcc)) as wcCount')
            ->addSelect('s.id as osId')
            ->addSelect('s.name as osName')
            ->where('wc.connectedAt BETWEEN :date_start AND :date_end')
            ->leftJoin('wc.client', 'wcc')
            ->leftJoin('wc.router', 'wr')
            ->leftJoin('wr.shop', 's')
            ->setParameters([
                'date_start' => new \DateTime($dateStart),
                'date_end' => new \DateTime($dateEnd),
            ])
            ->addGroupBy('s.id')

        ;

        return $clients->getQuery()->getResult();
    }

    public function findClientsInShop()
    {

    }


}