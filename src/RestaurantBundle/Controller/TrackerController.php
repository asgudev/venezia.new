<?php

namespace RestaurantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UserBundle\Entity\UserEvent;

class TrackerController extends Controller
{
    /**
     * @Route("/tracker", name="tracker")
     */
    public function trackerAction(Request $request)
    {
        if ($request->get("screenshot") != null) {
            $imgData = str_replace(' ', '+', $request->get("screenshot"));
            $imgData = substr($imgData, strpos($imgData, ",") + 1);
            $imgData = base64_decode($imgData);

            $photoPath = $this->get('kernel')->getRootDir() . '/../web/uploads/photos/screen' . time() . "_" . uniqid() . ".png";

            $f = fopen($photoPath, "w+");
            fputs($f, $imgData);
            fclose($f);

            $userEvent = new UserEvent();
            $userEvent->setUser($this->getUser());
            $userEvent->setShop($this->getUser()->getShop());
            $userEvent->setDate(new \DateTime());
            $userEvent->setEventType(UserEvent::EVENT_USER_SCREENSHOT);
            $userEvent->setPayload(json_encode(['image' => $photoPath]));

            $em = $this->getDoctrine()->getManager();
            $em->persist($userEvent);
            $em->flush();
        } else {
            $events = json_decode($request->getContent(), true);
            foreach ($events as $__event) {
                $__event["user"] = $this->getUser()->getId();
                $__event["ip"] = $request->getClientIp();
                $this->get("redis.helper")->rpush($__event["ip"], json_encode($__event));
            }
        }

        return new JsonResponse();
    }
}