<?php

namespace WifiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GetCodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phone', TextType::class, [
                'label' => 'Телефон',
                'required' => true,
                'mapped' => false,
                'attr' => [
                    // 'disabled' => 'disabled'
                    // 'placeholder' => '+375 29 123-45-67'
                ]
            ])
            ->add('mac', HiddenType::class,[
                'mapped' => false,
            ])
//          ->add('agreement', CheckboxType::class, [
//                'required' => true,
//                'mapped' => false,
//            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Войти',
                'attr' => [
                    'class' => 'disabled'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'code';
    }
}
