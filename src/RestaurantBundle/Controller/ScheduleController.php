<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\Workplace;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\FatalErrorException;
use UserBundle\Entity\User;
use UserBundle\Entity\UserEvent;

/**
 * @Route("/admin")
 */
class ScheduleController extends Controller
{

    /**
     * @Route("/person/timing/{shop}", name="person_timing")
     */
    public function personEventsAction(Request $request, $shop = 1)
    {
        $dates = [];
        $begin = (new \DateTime('-15 days'))->setTime(0, 0, 0);
        $end = (new \DateTime('+2 days'))->setTime(0, 0, 0);

        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        $em = $this->getDoctrine()->getManager();
        $userEvents = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAllInRange($shop, $begin, $end);

        $usrEvent = [];
        foreach ($userEvents as $event) {
            $usrEvent[$event->getUser()->getName()][] = $event;
        }

        $userData = [];

        foreach ($usrEvent as $usr => $events) {
            $previous = false;
            $dayData = [];

            if (!array_key_exists($usr, $userData)) {
                $userData[$usr] = [];
                foreach ($period as $dt) {
                    $userData[$usr][$dt->format("Y-m-d")] = [];
                }
            }

            /**
             * @var UserEvent $event
             */
            foreach ($events as $event) {
                $dayData[$event->getDate()->format("Y-m-d")][] = $event;
                if ($event->getEventType() != null) {
                    $previous = $event;
                } else {
                    if ($previous) {
                        if ($previous->getEventType() == UserEvent::EVENT_USER_ENTER) {
                            $event->setEventType(UserEvent::EVENT_USER_EXIT);
                        }
                        if ($previous->getEventType() == UserEvent::EVENT_USER_EXIT) {
                            $event->setEventType(UserEvent::EVENT_USER_ENTER);
                        }
                    } else {
                        if (($event->getDate()->format("H") >= 6) && (count($dayData[$event->getDate()->format("Y-m-d")]) == 1)) {
                            $event->setEventType(UserEvent::EVENT_USER_ENTER);
                        }
                    }

                    $em->persist($event);
                    $previous = $event;
                }

                if ($event->getEventType() == UserEvent::EVENT_USER_ENTER) {
                    $userData[$usr][$event->getDate()->format("Y-m-d")]['enter'] = $event->getDate();
                }
                if ($event->getEventType() == UserEvent::EVENT_USER_EXIT) {
                    $userData[$usr][$event->getDate()->format("Y-m-d")]['exit'] = $event->getDate();
                }

                if (count($userData[$usr][$event->getDate()->format("Y-m-d")]) == 2) {
                    $userData[$usr][$event->getDate()->format("Y-m-d")]['time'] = $userData[$usr][$event->getDate()->format("Y-m-d")]['exit']->diff($userData[$usr][$event->getDate()->format("Y-m-d")]['enter']);
                }

//                if ((count($userData[$usr][$event->getDate()->format("Y-m-d")]) == 1) && (array_key_exists('enter', $userData[$usr][$event->getDate()->format("Y-m-d")]))) {
//                    $userData[$usr][$event->getDate()->format("Y-m-d")]['time'] = (new \DateTime())->diff($userData[$usr][$event->getDate()->format("Y-m-d")]['enter']);
//                }

            }
        }
//        dump($userData);die();
        $em->flush();
        $userEvents = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAllInRange($shop, $begin, $end);

        return $this->render("@Restaurant/Schedule/timing.html.twig", [
            'userEvents' => $userEvents,
            'userData' => $userData,
        ]);
    }

    /**
     * @Route("/person/event/{id}/delete", name="person_event_delete")
     */
    public function personEventDeleteAction(Request $request, UserEvent $event)
    {
        $events = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAfter($event);

        $em = $this->getDoctrine()->getManager();
        foreach ($events as $__event) {
            $__event->setEventType(null);
            $em->persist($__event);
        }
        $em->remove($event);
        $em->flush();

        return $this->redirectToRoute('person_timing', [
            'shop' => $event->getShop()->getId()
        ]);

    }

    /**
     * @Route("/person/event/{id}/update", name="person_event_update")
     */
    public function personEventUpdateAction(Request $request, UserEvent $event)
    {
        $em = $this->getDoctrine()->getManager();
        $event->setEventType($event->getEventType() == UserEvent::EVENT_USER_EXIT ? UserEvent::EVENT_USER_ENTER : UserEvent::EVENT_USER_EXIT);
        $em->persist($event);

        $events = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAfter($event);

        foreach ($events as $__event) {
            $__event->setEventType(null);
            $em->persist($__event);
        }

        $em->flush();

        return $this->redirectToRoute('person_timing', [
            'shop' => $event->getShop()->getId()
        ]);
    }


    /**
     * @Route("/person/work", name="person_work")
     */
    public function personWorktimeAction(Request $request)
    {
        $userEvents = $this->getDoctrine()->getRepository('UserBundle:UserEvent')->findAllToday(2);

        return $this->render("@Restaurant/Schedule/timing.html.twig", [
            'userEvents' => $userEvents,
        ]);
    }

    /**
     * @Route("/person/timesheet/{id}/{dateStart}/{dateEnd}", name="personal_timesheet")
     */
    public function personTimesheetAction(Request $request, User $user, $dateStart = "-1 day", $dateEnd = "now")
    {
        return new JsonResponse([]);
    }

    /**
     * @Route("/timesheet/{dateStart}/{dateEnd}", name="common_timesheet")
     */
    public function commonTimesheetAction(Request $request, $dateStart = "-1 day", $dateEnd = "now")
    {
        $dateStart = (new \DateTime($dateStart))->setTime(0, 0, 0);
        $dateEnd = (new \DateTime($dateEnd))->setTime(23, 59, 59);

        $dateRange = [];

        $ds = clone $dateStart;
        while ($ds <= $dateEnd) {
            $dateRange[] = clone $ds; // Set value to null or anything you need
            $ds->modify('+1 day');
        }

        /* var UserEvent[] $events */
        $events = $this->getDoctrine()->getRepository(UserEvent::class)->findTimesheetEventsInRange($dateStart, $dateEnd);
        $workplaces = $this->getDoctrine()->getRepository(Workplace::class)->findAll();

        $workplacesHours = $this->getWorkplacesWorkingHours($workplaces);

        $personData = [];

        foreach ($events as $event) {
            if (!array_key_exists($event->getUser()->getName(), $personData)) {
                $personData[$event->getUser()->getName()]['events'] = [];
                $personData[$event->getUser()->getName()]['user'] = $event->getUser();
            }

            $personData[$event->getUser()->getName()]['events'][] = $event;
        }

        foreach ($personData as $user => $events) {
            usort($personData[$user]['events'], function ($a, $b) {
                if ($a->getDate() == $b->getDate()) {
                    return 0;
                }

                return $a->getDate() < $b->getDate() ? -1 : 1;
            });

            ksort($personData[$user]);


            $personData[$user]['sheet'] = [];


            /* @var UserEvent $event */
            foreach ($personData[$user]['events'] as $event) {
                $day = $event->getDate()->format("Y-m-d");
                $time = (int)$event->getDate()->format("H");
                if ($time <= 4) {
                    $day = (new \DateTime($day))->modify("-1 day")->format("Y-m-d");
                }

                if (!isset($personData[$user]['sheet'][$day])) {
                    $personData[$user]['sheet'][$day] = [
                        'start' => null,
                        'end' => null,
                        'isLate' => false,
                        'hours' => 0,
                        'workplace' => $event->getWorkplace(),
                        "startPhoto" => false,
                        "endPhoto" => false,
                    ];
                }

                if ($event->getEventType() === UserEvent::EVENT_USER_ENTER) {
                    $personData[$user]['sheet'][$day]["start"] = $event;

                    $payload = json_decode($event->getPayload(), true) ?: ["image" => false];
                    $personData[$user]['sheet'][$day]["startPhoto"] = $payload["image"];

                    $shopStart = $this->getWorkplaceStart($workplacesHours, $event->getWorkplace()->getId(), $day, "start");

                    if ($this->calcTimeDiff($event->getDate(), $shopStart, 60, false) > 15) {
                        $personData[$user]['sheet'][$day]["isLate"] = true;
                    }
                } elseif ($event->getEventType() === UserEvent::EVENT_USER_EXIT) {

                    if ($personData[$user]['sheet'][$day]["end"]) {

                        if ($event->getDate() > $personData[$user]['sheet'][$day]["end"]->getDate()) {
                            $tmp = $personData[$user]['sheet'][$day]["end"];
                            $personData[$user]['sheet'][$day]["end"] = $event;
                            $payload = json_decode($event->getPayload(), true) ?: ["image" => false];
                            $personData[$user]['sheet'][$day]["endPhoto"] = $payload["image"];

                            $prevDate = $this->getPrevDate($day);

                            if (array_key_exists($prevDate, $personData[$user]['sheet'])) {
                                if (!$personData[$user]['sheet'][$prevDate]["end"]) {
                                    $personData[$user]['sheet'][$prevDate]["end"] = $tmp;
                                } else {
                                    var_dump($user);
                                    die();
                                    throw new FatalErrorException();
                                }
                            }
                        } else {
                            var_dump($event->getId());
                            die();
                        }

                    } else {
                        $personData[$user]['sheet'][$day]["end"] = $event;
                        $payload = json_decode($event->getPayload(), true) ?: ["image" => false];
                        $personData[$user]['sheet'][$day]["endPhoto"] = $payload["image"];
                    }
                }
            }

            foreach ($personData[$user]['sheet'] as $day => &$data) {
//                if (!$data["start"]) {
//                    $endEvent = $data["end"];
//                    $shopStart = $this->getWorkplaceStart($workplacesHours, $endEvent->getWorkplace()->getId(), $day, "start");
//
//                    $startEvent = (new UserEvent())
//                        ->setEventType(UserEvent::EVENT_USER_ENTER)
//                        ->setUser($endEvent->getUser())
//                        ->setDate($shopStart)
//                        ->setWorkplace($endEvent->getWorkplace());
//
//                    $data["start"] = $startEvent;
//                    $data["isLate"] = true;
//
//                    /* ?? persist ?? */
//                }
//
//                if (!$data["end"]) {
//                    $startEvent = $data["start"];
//
//                    $shopEnd = $this->getWorkplaceStart($workplacesHours, $startEvent->getWorkplace()->getId(), $day, "end");
//
//
//                    if ($shopEnd < $data["start"]->getDate()) {
//                        $shopEnd->modify("+1 day");
////                        $shopEnd = $this->getWorkplaceStart($workplacesHours, $startEvent->getWorkplace()->getId(), $this->getNextDate($day), "end");
//                    }
//
//
//                    $endEvent = (new UserEvent())
//                        ->setEventType(UserEvent::EVENT_USER_EXIT)
//                        ->setUser($startEvent->getUser())
//                        ->setDate($shopEnd)
//                        ->setWorkplace($startEvent->getWorkplace());
//                    $data["end"] = $endEvent;
//
//
//                    /* ??? persist ??? */
//                }

                if ($data["start"] && $data["end"]) {
                    $data["hours"] = $this->calcTimeDiff($data["start"]->getDate(), $data["end"]->getDate());
                }
            }
        }


        return $this->render("@Restaurant/Schedule/timesheet.html.twig", [
            'workplaces' => $workplaces,
            'personData' => $personData,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'days' => $dateRange,
        ]);
    }

    private function getWorkplacesWorkingHours($workplaces)
    {

        $data = [];
        foreach ($workplaces as $workplace) {
            $data[$workplace->getId()] = $workplace->getTimes();
        }

        return $data;
    }

    private function getWorkplaceStart($workplaces, $id, $date, $type = "start")
    {
        $dayOfWeek = $dayOfWeek = date('w', strtotime($date));

        $shopStartTime = $workplaces[$id][$dayOfWeek][$type];


        return new \DateTime($date . " " . $shopStartTime);
    }

    private function calcTimeDiff(\DateTime $date1, \DateTime $date2, $divisor = 3600, $abs = true)
    {
        if ($abs) {
            return round(abs($date1->getTimestamp() - $date2->getTimestamp()) * 2 / $divisor) / 2;
        } else {
            return round(($date1->getTimestamp() - $date2->getTimestamp()) * 2 / $divisor) / 2;

        }
    }

    private function getPrevDate($date)
    {
        $dateObj = \DateTime::createFromFormat('Y-m-d', $date);
        $dateObj->modify('-1 day');
        return $dateObj->format('Y-m-d');
    }

    /**
     * @Route("/person/calendar/{shop}", name="person_calendar")
     */
    public function personCalendarAction(Request $request, Shop $shop = null)
    {
        $personData = [];

        $begin = (new \DateTime())->modify('-20 days');
        $end = (new \DateTime())->modify('+20 days');
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);
        $users = $this->getDoctrine()->getRepository('UserBundle:User')->findBy([
            'shop' => $shop,
            'enabled' => 1,
        ]);
        $reportsData = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findReportsByPeriod($begin->format('Y-m-d'), $end->format('Y-m-d'), $shop);

        foreach ($users as $__user) {
            foreach ($period as $dt) {
                $personData[$__user->getName()][$dt->format("d.m.Y")] = 0;
            }
        }

        foreach ($reportsData as $__report) {
            $personData[$__report->getUser()->getName()][$__report->getName()] = 1;
        }


        return $this->render("@Restaurant/Schedule/calendar.html.twig", [
            'personData' => $personData,
        ]);
    }

    private function getNextDate($date)
    {
        $dateObj = \DateTime::createFromFormat('Y-m-d', $date);
        $dateObj->modify('+1 day');
        return $dateObj->format('Y-m-d');
    }
}
