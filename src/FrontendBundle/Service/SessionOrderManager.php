<?php


namespace FrontendBundle\Service;


use DishBundle\Entity\Dish;
use FrontendBundle\Entity\WebOrder;
use FrontendBundle\Entity\WebOrderDish;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionOrderManager
{
    const IS_PROMO = true;

    private $session;

    private $container;

    private $order;
    private $doctrine;

    public function __construct(ContainerInterface $container, SessionInterface $session)
    {
        $this->container = $container;

        $this->session = $session;
        $this->doctrine = $this->container->get("doctrine");

        $this->order = $session->get('order');
        if (!$this->order) {
            $this->order = [];
        }
    }

    public function setQuantity($dish, $quantity)
    {
//        if (self::IS_PROMO) {
//            $this->order = $this->calcPromo();
//        }

        $this->order[$dish] = [
            "quantity" => $quantity,
        ];
        $this->session->set("order", $this->order);

        return $this->order;
    }

    public function changeQuantity($dish, $quantity)
    {
        if (!array_key_exists($dish, $this->order)) {
            $this->order[$dish] = [
                "quantity" => 0,
            ];
        }

        $this->order[$dish]["quantity"] += $quantity;

        if ($this->order[$dish]["quantity"] <= 0)
            unset($this->order[$dish]);

        $this->recalcContainers();

        $this->session->set("order", $this->order);

        return $this->order;
    }

    private function recalcContainers()
    {
        unset($this->order[21177]);
        unset($this->order[34727]);

        $this->order[11177]["quantity"] = 0;
        $this->order[34727]["quantity"] = 0;

        foreach ($this->order as $dish => $data) {
            $dishObj = $this->doctrine->getRepository(Dish::class)->find($dish);
            if (!$dishObj) continue;
            if (in_array($dishObj->getCategory()->getId(), [32, 82])) {
                if (in_array($dishObj->getId(), [33807, 33787, 33767, 33827, 33847, 33867])) {
                    $this->order[34727]["quantity"] += $data["quantity"];
                } else {
                    $this->order[11177]["quantity"] += $data["quantity"];
                }
            }
        }
        $this->session->set("order", $this->order);

        return $this->order;
    }

    public function deleteDish($id)
    {

        if (array_key_exists($id, $this->order))
            unset($this->order[$id]);


        $this->recalcContainers();

        foreach ($this->order as $__dish => $__data) {
            $dishObj = $this->doctrine->getRepository(Dish::class)->find($__dish);
            if ($dishObj) {
                $this->order[$__dish]["price"] = $dishObj->getPrice();
            } else {
                unset($this->order[$__dish]);
            }
        }

        $this->recalcContainers();

        $this->session->set("order", $this->order);

        return $this->order;
    }


    public function clear()
    {
        $this->session->set("order", []);

        return $this->order;
    }


    public function setOrderToSession($order) {
        $this->order = $order;
        $this->session->set("order", $this->order);

        return $this->order;
    }

    public function getOrderFromSession()
    {
        return $this->order;
    }

    /**
     * @return WebOrder
     */
    public function getOrderObjectFromSession()
    {
        $em = $this->doctrine->getManager();


//        $this->calcPromo();
        $this->recalcContainers();

        $order = new WebOrder();
        $shop = $em->getReference(Shop::class, $this->container->getParameter('web_shop_id'));
        if ($this->order != null) {
            foreach ($this->order as $dish_id => $data) {
                if ($data["quantity"] <= 0) continue;

                $dish = new WebOrderDish();
                $dishObj = $this->doctrine->getRepository(Dish::class)->find($dish_id);
                if (!$dishObj) {
                    unset($this->order[$dish_id]);
                    continue;
                }

                $dish->setDish($dishObj);
                $dish->setDishPrice($dishObj->getPrice($shop));
                $dish->setDishDiscount(0);
                $dish->setQuantity($data["quantity"]);
                $order->addDish($dish);
            }
        }
//        dump($order->getCashSum());die();
        return $order;
    }


    public function calcPromo()
    {
        /**
         * @var Dish[] $dishesData
         */
        $dishesData = $this->container->get("doctrine")->getRepository(Dish::class)->findByIds(array_keys($this->order));

        $dishes = [];
        foreach ($dishesData as $__dish)
            $dishes[$__dish->getId()] = $__dish;


        foreach ($this->order as $dish => $data) {
            if (!array_key_exists($dish, $dishes)) continue;

            $dishObj = $dishes[$dish];

            if ($dishObj->getCategory()->getId() === 82) {
                if (!array_key_exists($dish - 10000, $this->order)) {
                    $this->order[$dish - 10000]["quantity"] = 0;
                }

                $this->order[$dish - 10000]["quantity"] += $this->order[$dish]["quantity"];
                unset($this->order[$dish]);
            }
        }
        $dishesData = $this->container->get("doctrine")->getRepository(Dish::class)->findByIds(array_keys($this->order));
        $dishes = [];
        foreach ($dishesData as $__dish)
            $dishes[$__dish->getId()] = $__dish;


        $ignoredDishes = [11180, 33767, 33787, 33807, 33827, 33541, 33867];

        $__order = [];
        $__pizzas = [];
        foreach ($this->order as $dish => $data) {
            if (!array_key_exists($dish, $dishes)) continue;
            if (in_array($dish, $ignoredDishes)) continue;
            if (in_array($dishes[$dish]->getCategory()->getId(), [32, 82])) {
                for ($i = 0; $i < $data["quantity"]; $i++) {
                    $__pizzas[] = [
                        'id' => $dish,
                        'quantity' => 1,
                        'price' => $dishes[$dish]->getPrice(),
                    ];
                }
                unset($this->order[$dish]);
            }
        }

        usort($__pizzas, function ($a, $b) {
            return ($a['price'] - $b['price'] < 0 ? -1 : 1);
        });


        $freeCnt = intval(count($__pizzas) / 2);
        for ($i = 0; $i < $freeCnt; $i++) {
            if (!in_array($__pizzas[$i]["id"], $ignoredDishes))
                $__pizzas[$i]["id"] += 10000;
        }

        foreach ($__pizzas as $pizza) {
            if (!array_key_exists($pizza['id'], $this->order))
                $this->order[$pizza['id']]["quantity"] = 0;

            $this->order[$pizza['id']]["quantity"] += 1;
        }

        return $this->order;
    }
}
