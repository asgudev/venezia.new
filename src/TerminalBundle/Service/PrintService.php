<?php


namespace TerminalBundle\Service;


use OrderBundle\Entity\Order;
use RestaurantBundle\Entity\Printer;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use TerminalBundle\Events\OrderEvent;

class PrintService
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function cancelOrder(Order $order) {
        $printData = [];
        foreach ($order->getDishes() as $orderDish) {
            if ($orderDish->getQuantity() == 0) continue;
            if (!$orderDish->getOrder()) continue;

            $order = $orderDish->getOrder();
            $printer = $orderDish->getDish()->getCategory()->getShopPrinter($order->getShop());

            if ($printer == null) {
                $printer = new Printer();
                $printer->setName($order->getShop()->getAdminPrinter());
            }

            $printData[$printer->getName()]['dishes'][] = [
                'dish' => $orderDish,
                'quantity' => -$orderDish->getQuantity(),
            ];
            $printData[$printer->getName()]['printer'] = $printer;


        }

        foreach ($printData as $printer => $data) {
            $this->container->get('terminal.print.storage')->printDishes($data['printer'], $order, $data['dishes']);
        }
    }

    public function printDishChanges($dishChanges)
    {

        $printData = [];
        foreach ($dishChanges as $dishChange) {
            $dish = $dishChange['dish'];
            $order = $dish->getOrder();

            $printer = $dish->getDish()->getCategory()->getShopPrinter($order->getShop());

            if ($printer == null) {
                $printer = new Printer();
                $printer->setName($order->getShop()->getAdminPrinter());
            }

            $printData[$printer->getName()]['dishes'][] = [
                'dish' => $dish,
                'quantity' => $dishChange['quantity'],
            ];
            $printData[$printer->getName()]['printer'] = $printer;

            if ($dish->getDish()->getId() === 43147) {
                $printData['cold']['dishes'][] = [
                    'dish' => $dish,
                    'quantity' => $dishChange['quantity'],
                ];
                $printData['cold']['printer'] = (new Printer())->setName("cold");
            }
        }

        foreach ($printData as $printer => $data) {
            $this->container->get('terminal.print.storage')->printDishes($data['printer'], $order, $data['dishes']);
        }
    }


    public function printOrder(Order $order)
    {
        $event = new OrderEvent($order);
        $dispatcher = $this->container->get('event_dispatcher');
        $dispatcher->dispatch(OrderEvent::ORDER_PRINT, $event);
    }

    public function printOrderToKitchens(Order $order)
    {
        $order = $this->container->get('doctrine')->getRepository("OrderBundle:Order")->findWithPrinters($order);
        $event = new OrderEvent($order);
        $dispatcher = $this->container->get('event_dispatcher');
        $dispatcher->dispatch(OrderEvent::ORDER_PRINT_KITCHEN, $event);
    }

    public function printAddedDishes($order, $dishes)
    {
        $this->container->get('terminal.print.storage')->printDirect($order, $dishes);
    }

    public function printCanceledDishes($order, $dishes)
    {
        $this->container->get('terminal.print.storage')->printDirect($order, $dishes, 1);
    }

}
