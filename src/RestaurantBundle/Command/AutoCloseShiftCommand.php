<?php

namespace RestaurantBundle\Command;


use TerminalBundle\Entity\DailyReport;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Bundle\FrameworkBundle\Console\Application;


class AutoCloseShiftCommand extends ContainerAwareCommand
{

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('restaurant:close:shift')
            ->setDescription('Close shift')
            ->addOption('shop', '-s', InputOption::VALUE_REQUIRED, 'Shop');
        //     php /var/www/html/current/bin/console rest:close:shift -e=prod --shop=14

    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $shop_id = $input->getOption('shop');
        $shop = $doctrine->getRepository('RestaurantBundle:Shop')->find($shop_id);
        $dailyReport = new DailyReport($shop);

        $previousReport = $doctrine->getRepository('TerminalBundle:DailyReport')->findLastReport($shop);
        if (!$previousReport) {
            $previousReport = new DailyReport($shop);
            $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
        } else {
            $previousReport = $previousReport[0];
        }

        $dailyReport->setDateStarted($previousReport->getDateClosed());
        if ($dailyReport->getDateClosed() === null) {
            $dailyReport->setDateClosed(new \DateTime());
        } else {
            $dailyReport->setDateClosed($dailyReport->getDateClosed());
        }
        $firstOrder = $doctrine->getRepository('OrderBundle:Order')->findFirstOrderInReport($dailyReport);

        if (!$firstOrder) return;
        
        $dailyReport->setName($firstOrder->getCreatedAt()->format('d.m.Y'));

        $em->persist($dailyReport);
        $em->flush();


        $kernel = $this->getContainer()->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $inputCommand = new ArrayInput(array(
            'command' => 'rest:report:export',
            '--report' => $dailyReport->getId(),
            '--env' => $this->getContainer()->getParameter('kernel.environment')
        ));
        $application->run($inputCommand);

    }
}