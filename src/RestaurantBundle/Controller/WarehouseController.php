<?php


namespace RestaurantBundle\Controller;

use DishBundle\Entity\Ingredient;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\HttpFoundation\JsonResponse;
use TerminalBundle\Entity\DailyReport;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class WarehouseController
 * @package RestaurantBundle\Controller
 */
class WarehouseController extends Controller
{


    /**
     * @Route("/shop/warehouse/update", name="shop_warehouse_update")
     */
    public function shopWarehouseUpdateAction(Request $request)
    {


        $ingrName = preg_replace("#, кг#", "", $request->get("ingr"));
        $ingr = $this->getDoctrine()->getRepository('DishBundle:Ingredient')->findOneBy([
            'id' => $ingrName,
        ]);
        switch ($request->get("type")) {
            case "correction":
                $correction = json_decode($this->get("redis.helper")->get("ingredient_correction"), true);

                if (!$correction)
                    $correction = [];
                $correction[$request->get("ingr")][$request->get("shop")] = $request->get("newVal");
                $this->get("redis.helper")->set("ingredient_correction", json_encode($correction, JSON_UNESCAPED_UNICODE), 72000);
                break;
            case "request":
                $ingrRequest = json_decode($this->get("redis.helper")->get("ingredient_request"), true);

                if (!$ingrRequest)
                    $ingrRequest = [];

                $ingrRequest[$request->get("ingr")][$request->get("shop")] = $request->get("newVal");
                $this->get("redis.helper")->set("ingredient_request", json_encode($ingrRequest, JSON_UNESCAPED_UNICODE), 72000);
                break;
            case "balance":
//                var_dump($request->get("shopId"));
                $warehouse = $this->getDoctrine()->getRepository("RestaurantBundle:Warehouse")->find($request->get("shopId"));

                $ingrInWarehouse = $this->getDoctrine()->getRepository("DishBundle:IngredientInWarehouse")->findIngredientInWarehouse($warehouse, $ingr);

                $ingrInWarehouse->setQuantity($request->get('newVal'));

                $em = $this->getDoctrine()->getManager();
                $em->persist($ingrInWarehouse);
                $em->flush();
                break;
        }


        return new JsonResponse([]);
    }

    /**
     * @Route("/shop/warehouse/summary", name="shop_warehouse_summary")
     */
    public function warehouseSummaryAction()
    {

        if (!$this->getUser())
            return $this->redirectToRoute('login');


        $ingredients = $this->getDoctrine()->getRepository(Ingredient::class)->findBy(['isDeleted' => 0]);
        $__ingredients = [];
        foreach ($ingredients as $ingredient)
            $__ingredients[$ingredient->getId()] = $ingredient;

        $ingredients = $__ingredients;

        if ($this->isGranted("ROLE_SUPPLY")) {
            $shops = $this->getDoctrine()->getRepository(Shop::class)->findAll();
        } else {
            $shops = $this->getUser()->getAdminShops();
        }
	
        $__shops = [];

        foreach ($shops as $shop) {
            $__shops[$shop->getId()] = $shop;
        }

        $shops = $__shops;

//var_dump(array_keys($shops));


        if ($this->getParameter("is_primary")) {

            $data =  $this->get("restaurant.warehouse.manager")->calculateWarehouse($shops);
//dump($data);die();
            return $this->render('@Control/Report/shopWarehouse.html.twig', [
                'requestCorrection' => $data["requestCorrection"],
                'practicalConsumption' => $data["practicalConsumption"],
                'request' => $data["request"],
                'sum' => $data["sum"],
                'requestCategories' => $data['ingrCategories'],
                'requestCategoriesNames' => $data['ingrCategoriesNames'],
                'consumptionSum' => $data["consumption_sum"],
                'data' => $data["data"],
                'shops' => $shops,
                "ingredients" => $ingredients,
                'transferLog' => $data["transferLog"],
            ]);
        } else {

            $data = $this->get("api.service")->call($this->generateUrl("warehouse_request"), "GET");
            $data = json_decode($data, true);

            return $this->render('@Control/Report/shopWarehouse.html.twig', [
                "request" => $data["request"],
                "requestCategories" => $data["requestCategories"],
                "ingredients" => $ingredients,
                'shops' => $shops,
                'requestCategoriesNames' => $data['requestCategoriesNames'],
            ]);
        }
    }
}
