<?php


namespace ReportBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use ReportBundle\Entity\Report;

class ReportRepository extends EntityRepository
{

    /**
     * @return Report[]
     */
    public function findLatest() {
        return $this->createQueryBuilder('r')
            ->orderBy('r.createdAt', "DESC")
            ->getQuery()
            ->getResult();
    }
}