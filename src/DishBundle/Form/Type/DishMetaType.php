<?php


namespace DishBundle\Form\Type;


use DishBundle\Entity\DishMeta;
use RestaurantBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\OptionsResolver\OptionsResolver;

class DishMetaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shop', EntityType::class, [
                'class' => Shop::class,
                'choice_label' => 'name',
                'label' => 'Заведение',
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('price', NumberType::class, [
                'label' => 'Базовая цена',
                'attr' => [
                    'placeholder' => 'Базовая цена',
                    'class' => 'customPrice md-input',
                ]
            ])
            ->add('isDelivery', CheckboxType::class, [
                'label' => 'Доставка',
                'required' => false,
            ])
//            ->add('id1c', NumberType::class, [
//                'label' => 'ID в 1C',
//                'required' => false,
//                'attr' => [
//                    'placeholder' => 'ID в 1C',
//                    'class' => 'md-input',
//                ]
//            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DishMeta::class,
        ));
    }
}
