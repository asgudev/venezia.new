<?php


namespace DishBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class CompositionIngredient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Composition", inversedBy="ingredients")
     */
    private $composition;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Composition", cascade={"persist"})
     */
    private $ingredient;

    /**
     * @ORM\Column(name="net_mass", type="string", nullable=true)
     */
    private $netMass;

    /**
     * @ORM\Column(name="gross_mass", type="string", nullable=true)
     */
    private $grossMass;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CompositionIngredient
     */
    public function setId(int $id): CompositionIngredient
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * @param mixed $composition
     * @return CompositionIngredient
     */
    public function setComposition($composition)
    {
        $this->composition = $composition;
        return $this;
    }

    /**
     * @return Composition
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param mixed $ingredient
     * @return CompositionIngredient
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNetMass()
    {
        return $this->netMass;
    }

    /**
     * @param mixed $netMass
     * @return CompositionIngredient
     */
    public function setNetMass($netMass)
    {
        $this->netMass = $netMass;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGrossMass()
    {
        return $this->grossMass;
    }

    /**
     * @param mixed $grossMass
     * @return CompositionIngredient
     */
    public function setGrossMass($grossMass)
    {
        $this->grossMass = $grossMass;
        return $this;
    }

}