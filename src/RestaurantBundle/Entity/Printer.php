<?php

namespace RestaurantBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Restaurant
 *
 * @ORM\Table(name="printers")
 * @ORM\Entity()
 */
class Printer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="device_name", type="string", nullable=true)
     */
    private $deviceName;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop", inversedBy="printers")
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\PrinterDevice")
     */
    private $printerDevice;


    /**
     * @ORM\Column(name="transliterate", type="boolean", nullable=false)
     */
    private $transliterate;

    const PRINTER_TYPES = [
        'Администратор' => 0,
        'Бар' => 1,
        'Холодный цех' => 2,
        'Горячий цех' => 3,
        'Пиццерия' => 4,
        'Мороженое' => 5,
        'Десерты' => 6,
        'Суши' => 7,
        'Кухня' => 8,
    ];

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\DishCategory", inversedBy="printers")
     */
    private $category;

    public function __construct()
    {
        $this->type = static::PRINTER_TYPES["Администратор"];
        $this->transliterate = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Printer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deviceName
     *
     * @param string $deviceName
     *
     * @return Printer
     */
    public function setDeviceName($deviceName)
    {
        $this->deviceName = $deviceName;

        return $this;
    }

    /**
     * Get deviceName
     *
     * @return string
     */
    public function getDeviceName()
    {
        return $this->deviceName;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return Printer
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Printer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set category
     *
     * @param \DishBundle\Entity\DishCategory $category
     *
     * @return Printer
     */
    public function setCategory(\DishBundle\Entity\DishCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DishBundle\Entity\DishCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set transliterate
     *
     * @param boolean $transliterate
     *
     * @return Printer
     */
    public function setTransliterate($transliterate)
    {
        $this->transliterate = $transliterate;

        return $this;
    }

    /**
     * Get transliterate
     *
     * @return boolean
     */
    public function getTransliterate()
    {
        return $this->transliterate;
    }

    /**
     * @return PrinterDevice
     */
    public function getPrinterDevice()
    {
        return $this->printerDevice;
    }

    /**
     * @param mixed $printerDevice
     * @return Printer
     */
    public function setPrinterDevice($printerDevice)
    {
        $this->printerDevice = $printerDevice;
        return $this;
    }


}
