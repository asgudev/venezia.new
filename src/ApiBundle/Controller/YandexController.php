<?php

namespace ApiBundle\Controller;

use DishBundle\Entity\Dish;
use DishBundle\Entity\DishCategory;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use OrderBundle\Entity\OrderMeta;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use UserBundle\Entity\User;

/**
 * @Route("/yandex", name="api_yandex")
 */
class YandexController extends Controller
{

    /**
     * @Route("/order/{id}/status", name="yandex_order_status", methods={"get"})
     */
    public function orderStatusAction(Request $request, Order $order)
    {

        $status = [
            '-1' => "NEW",
            "0" => "ACCEPTED_BY_RESTAURANT",
            "1" => "COOKING",
            "2" => "READY",
            "4" => "TAKEN_BY_COURIER",
            "5" => "CANCELLED",
        ];

        $data = [
            'status' => $status[$order->getStatus()],
        ];

        $response = new Response(json_encode($data, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/vnd.eats.order.v2+json');
        return $response;

    }

    /**
     * @Route("/order/{id}", name="yandex_order_update", methods={"put"})
     */
    public function orderUpdateAction(Request $request, Order $order)
    {
        $data = [];

        $response = new Response(json_encode($data, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/order/{id}", name="yandex_order_delete", methods={"delete"})
     */
    public function orderDeleteAction(Request $request, Order $order)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user || $user->getId() !== 39457) {
            throw new AccessDeniedHttpException();
        }

        if (!$order) {
            return new JsonResponse([
                'reason' => "Заведение не найдено"
            ], 404);
        }

        $order->setStatus(5);

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();


        $data = [

        ];

        $response = new Response(json_encode($data, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/order/{id}", name="yandex_order_info", methods={"get"})
     */
    public function orderInfoAction(Request $request, Order $order)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user || $user->getId() !== 39457) {
            throw new AccessDeniedHttpException();
        }


        $data = json_decode($order->getMeta()->getOther(), true);

        $data['items'] = [];

        foreach ($order->getDishes() as $dish) {
            $dish = [
                'id' => (string)$dish->getDish()->getId(),
                'quantity' => $dish->getQuantity(),
                'price' => $dish->getDishPrice(),
                'modifications' => [],
                'promos' => [],
            ];

            $data['items'][] = $dish;
        }

        $response = new Response(json_encode($data, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/vnd.eats.order.v2+json');
        return $response;
    }

    /**
     * @Route("/order/{id}", name="yandex_delete_order", methods={"DELETE"})
     */
    public function deleteOrder(Request $request, Order $order)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user || $user->getId() !== 39457) {
            throw new AccessDeniedHttpException();
        }

        if (!$order) {

        }

        //TODO: push by socket
        $order->setStatus(5);
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        $data = [];

        $response = new Response(json_encode($data, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/vnd.eats.order.v2+json');
        return $response;
    }


    /**
     * @Route("/order", name="yandex_create_order", methods={"POST"})
     */
    public function createOrder(Request $request)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user || $user->getId() !== 39457) {
            throw new AccessDeniedHttpException();
        }

        $this->get('user.logger')->info($request->getContent());

        $requestData = json_decode($request->getContent(), true);

        $em = $this->getDoctrine()->getManager();

        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($requestData['restaurantId']);

        $order = new Order($user, $shop);
        $order->setLocalId(explode($requestData['eatsId'], "-")[0]);
        $order->setClient($em->getReference(Client::class, 1342));
        foreach ($requestData["items"] as $item) {
            $orderDish = new OrderDish();
            $order->addDish($orderDish);

            $orderDish->setDish($em->getReference(Dish::class, $item["id"]));
            $orderDish->setQuantity($item["quantity"]);
            $orderDish->setDishPrice($item["price"]);
            $orderDish->setDishDiscount(0);
        }

        $orderMeta = new OrderMeta($order);

        $orderMeta->setComment($requestData["eatsId"] . " " . $requestData["comment"] . " " . "Кол-во человек " . $requestData["persons"]);
        $orderMeta->setPhone($requestData["deliveryInfo"]["phoneNumber"]);
        $orderMeta->setOther(json_encode($requestData));

        $order->setMeta($orderMeta);
        $orderMeta->setOrder($order);

//        dump($order);die();
        //TODO: push by socket
        $order->setStatus(-2);

        $em->persist($order);
        $em->flush();


        return new JsonResponse([
            'result' => 'OK',
            "orderId" => (string)$order->getId(),
        ]);
    }

    /**
     * @Route("/restaurants", name="yandex_restauurants_list", methods={"GET"})
     */
    public function getRestaurantsAction(Request $request)
    {

        $shops = $this->getDoctrine()->getRepository(Shop::class)->findTakeawayShops();
        $data = ['places' => []];

        foreach ($shops as $shop) {
            $shop = [
                'id' => (string)$shop->getId(),
                'title' => $shop->getName(),
                'address' => $shop->getAddress(),
            ];


            $data['places'][] = $shop;
        }


        $response = new Response(json_encode($data, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/security/oauth/token", name="yandex_auth_token", methods={"POST"})
     */
    public function openidAction(Request $request)
    {
        $clientId = $request->get("client_id");
        $clientSecret = $request->get("client_secret");
        $this->get('user.logger')->info($request->getContent());

        if ($clientId != 39457 || $clientSecret != "5fe7c3c2e12deeef392acb138b491b5d") {
            throw new AccessDeniedHttpException();
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            "username" => "yandex",
        ]);

        return new JsonResponse([
            "access_token" => $user->getApiToken(),
        ]);
    }

    /**
     * @Route("/menu/{id}/promos", name="yandex_shop_menu_promos")
     */
    public function getMenuPromosAction(Shop $shop, Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user) {
            return new JsonResponse([
                "reason" => "Неверный токен"
            ], 401);
        }

        if (!$shop) {
            return new JsonResponse([
                'reason' => "Заведение не найдено"
            ], 404);
        }


        return new JsonResponse([
            'promoItems' => [],
        ]);
    }

    /**
     * @Route("/menu/{id}/availability", name="yandex_shop_menu_availability")
     */
    public function getMenuAvailabilityAction(Shop $shop, Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user) {
            return new JsonResponse([
                "reason" => "Неверный токен"
            ], 401);
        }

        if (!$shop) {
            return new JsonResponse([
                'reason' => "Заведение не найдено"
            ], 404);
        }


        $data = [
            'items' => [],
            'modifiers' => [],
        ];

        $response = new Response(json_encode($data, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/vnd.eats.menu.availability.v2+json');
        return $response;
    }

    /**
     * @Route("/menu/{id}/composition", name="yandex_shop_menu")
     */
    public function getMenuAction(Shop $shop, Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'apiToken' => preg_replace("#Bearer\s+#", "", $request->headers->get("authorization"))
        ]);

        if (!$user) {
            return new JsonResponse([
                "reason" => "Неверный токен"
            ], 401);
        }

        if (!$shop) {
            return new JsonResponse([
                'reason' => "Заведение не найдено"
            ], 404);
        }

        $menu = $this->getDoctrine()->getRepository(DishCategory::class)->findYandexMenuByShop($shop, null !== $request->get("full"));

        if ($user->hasRole("ROLE_DELIVERY")) {
            $menu = array_filter($menu, function (DishCategory $category) {
                return $category->getOrderTypes() & DishCategory::ORDER_TYPE['Yandex'];
            });
        } else if ($user->hasRole("ROLE_APP")) {
            $menu = $this->getDoctrine()->getRepository(DishCategory::class)->findAppMenuByShop($shop, null !== $request->get("full"));
        } else {
            $menu = $this->getDoctrine()->getRepository(DishCategory::class)->findAppMenuByShop($shop, null !== $request->get("full"));
        }


        /**
         * @var DishCategory $category
         */
        foreach ($menu as $key => $category) {
            if ($user->hasRole("ROLE_DELIVERY")) {
                $menu[$key]->setDishes($category->getDishes()->filter(function (Dish $dish) {
                    return $dish->getOrderTypes() & Dish::ORDER_TYPE['Yandex'];
                }));
            } 
        }

        $categories = [];
        $items = [];
        foreach ($menu as $key => $category) {
            $categories[] = ["id" => (string)$category->getId(), "name" => $category->getName()];

            foreach ($category->getDishes() as $item) {
                $items[] = [
                    'id' => (string)$item->getId(),
                    'categoryId' => (string)$item->getCategory()->getId(),
                    'name' => $item->getName(),
                    'measure' => (int)$item->getWeight(),
                    'measureUnit' => "г",
                    'price' => $item->getPrice(),
                    'description' => $item->getDescription() ?: "",
                    'modifierGroups' => [],
                    'images' => [[
                        "hash" => $item->getImageHash() ?: "placeholder.jpg",
                        "url" => "https://venezia.by/" . $item->getImagePath(),
                    ]
                    ],
                ];
            }

        }

//        $menuToApi = $this->menuTree($menu, $shop);

        $menu = [
            'categories' => $categories,
            'items' => $items,
            'lastChange' => (new \DateTime())->format("Y-m-d\TH:i:s.uP"),
        ];


        $response = new Response(json_encode($menu, JSON_UNESCAPED_UNICODE));
        $response->setCharset('UTF-8');
        $response->headers->set('Content-Type', 'application/vnd.eats.menu.composition.v2+json');
        return $response;
    }

    private function menuTree($menu, $shop)
    {
        $__menu = [];
        /** @var DishCategory $__category */
        foreach ($menu as $__category) {
            $__menu[] = [
                "id" => $__category->getId(),
                "name" => $__category->getName(),
                "description" => $__category->getDescription(),
                "image" => [
                    "original" => $__category->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/" . $__category->getImage() : false,
                    "thumbnail" => $__category->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/media/cache/index/" . $__category->getImage() : false,
                ],
                "dishes" => $this->menuTreeDishes($__category->getDishes(), $shop),
                'ignoreDiscount' => $__category->getIgnoreDiscount(),
                'orderType' => $__category->getOrderTypes(),
                "order" => $__category->getOrder(),
            ];
        }

        return $__menu;
    }

    private function menuTreeDishes($dishes, $shop)
    {
        $__dishes = [];

        /** @var Dish $__dish */
        foreach ($dishes as $__dish) {
            $__dishes[] = [
                "id" => $__dish->getId(),
                "name" => $__dish->getName(),
                "category" => $__dish->getCategory()->getId(),
                "image" => [
                    "original" => $__dish->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/" . $__dish->getImage() : false,
                    "thumbnail" => $__dish->getImage() ? "https://" . $this->getParameter("frontend_domain") . "/media/cache/index/" . $__dish->getImage() : false,
                ],
                "price" => $__dish->getShopPrice($shop),
                "description" => $__dish->getDescription(),
                "weight" => $__dish->getWeight(),
                'isWeight' => $__dish->getIsWeight(),
                'ignoreDiscount' => $__dish->getIgnoreDiscount(),
            ];
        }
        return $__dishes;
    }

}
