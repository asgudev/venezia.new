<?php

namespace RestaurantBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\ClientCredentials;

class PromoCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('cardId', TextType::class, [
                'label' => 'Номер карты',
                'required' => true,
            ])
            ->add('localId', TextType::class, [
                'label' => 'Короткий номер',
                'required' => false,
            ])
            ->add('isEmployee', CheckboxType::class,[
                'label' => 'Является сотрудником',
                'required' => false,
                'mapped'=> false,
            ])
            ->add('client', EntityType::class, [
                'class' => ClientCredentials::class,
                'choice_label' => function ($client, $key, $index) {
                    /** @var ClientCredentials $client */
                    return $client->getFirstName() . ' ' . $client->getLastName();
                },
                'label' => 'Клиент',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'query_builder' => function (EntityRepository $qb) {
                    return $qb->createQueryBuilder('c')
                        ->where('cc.id is null')
                        ->leftJoin('c.card', 'cc');
                }
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Создать'
            ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RestaurantBundle\Entity\PromoCard',
        ));
    }
}