<?php

namespace ControlBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * IngredientData
 *
 * @ORM\Table(name="ingredient_data")
 * @ORM\Entity()
 */
class IngredientData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Ingredient")
     */
    private $ingredient;

    /**
     * @ORM\Column(name="income", type="float", nullable=false, options={"default" = 0})
     */
    private $income;

    /**
     * @ORM\Column(name="waste", type="float", nullable=false, options={"default" = 0})
     */
    private $waste;

    /**
     * @ORM\Column(name="balance", type="float", nullable=false, options={"default" = 0})
     */
    private $balance;

    /**
     * @ORM\ManyToOne(targetEntity="TerminalBundle\Entity\DailyReport", inversedBy="ingredients")
     */
    private $report;


    public function __construct()
    {
        $this->income = 0;
        $this->waste = 0;
        $this->balance = 0;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set income
     *
     * @param float $income
     *
     * @return IngredientData
     */
    public function setIncome($income)
    {
        $this->income = $income === null ? 0 : str_replace(',','.', $income);

        return $this;
    }

    public function addIncome($diff) {
        $this->income += $diff === null ? 0 : str_replace(',','.', $diff);
    }

    /**
     * Get income
     *
     * @return float
     */
    public function getIncome()
    {
        return $this->income;
    }

    /**
     * Set waste
     *
     * @param float $waste
     *
     * @return IngredientData
     */
    public function setWaste($waste)
    {
        $this->waste = $waste === null ? 0 : str_replace(',','.', $waste);

        return $this;
    }

    public function addWaste($diff) {
        $this->waste += $diff === null ? 0 : str_replace(',','.', $diff);
    }

    /**
     * Get waste
     *
     * @return float
     */
    public function getWaste()
    {
        return $this->waste;
    }

    /**
     * Set balance
     *
     * @param float $balance
     *
     * @return IngredientData
     */
    public function setBalance($balance)
    {
        $this->balance = $balance === null ? 0 : str_replace(',','.', $balance);

        return $this;
    }

    public function addBalance($diff) {
        $this->balance += $diff === null ? 0 : str_replace(',','.', $diff);
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set ingredient
     *
     * @param \DishBundle\Entity\Ingredient $ingredient
     *
     * @return IngredientData
     */
    public function setIngredient(\DishBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient
     *
     * @return \DishBundle\Entity\Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * Set report
     *
     * @param \TerminalBundle\Entity\DailyReport $report
     *
     * @return IngredientData
     */
    public function setReport(\TerminalBundle\Entity\DailyReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \TerminalBundle\Entity\DailyReport
     */
    public function getReport()
    {
        return $this->report;
    }
}
