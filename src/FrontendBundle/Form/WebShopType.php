<?php

namespace FrontendBundle\Form;

use FrontendBundle\Entity\WebOrder;
use FrontendBundle\Entity\WebShop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebShopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shortDescription', TextareaType::class, [
                'label' => 'Короткое описание',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Описание',
                'required' => false,
            ])
            ->add("social", CollectionType::class, [
                //'entry_type' => SocialType::class,
                'label' => 'Социальные сети',
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add("delivery", CollectionType::class, [
                //'entry_type' => SocialType::class,
                'label' => 'Доставки',
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('times', TextType::class, [
                'label' => 'Время работы',
                'required' => false,
            ])
            ->add('kitchen', TextType::class, [
                'label' => 'Кухня',
                'required' => false,
            ])
            ->add('seoTitle', TextType::class, [
                'label' => 'Title',
                'required' => false,
            ])
            ->add('seoDescription', TextType::class, [
                'label' => 'Description',
                'required' => false,
            ])
            ->add('seoKeywords', TextType::class, [
                'label' => 'Keywords',
                'required' => false,
            ])

            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить'
            ])

        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WebShop::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'shop';
    }
}
