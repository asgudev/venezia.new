<?php

namespace ApiBundle\Controller;

use ControlBundle\Entity\DishPhotoCheck;
use DishBundle\Entity\Ingredient;
use OrderBundle\Entity\OrderDish;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CustomController
 * @package ApiBundle\Controller
 */
class CustomController extends Controller
{

    /**
     * @Route("/ingredients", name="api_ingredients")
     */
    public function getIngredientsAction(Request $request)
    {
        $ingredients = $this->getDoctrine()->getRepository(Ingredient::class)->findBy([
//            'isDeleted' => false,
        ]);

        $ingredientsData = array_map(function (Ingredient $ingredient) {
            return [
                'id' => $ingredient->getId(),
                'name' => $ingredient->getName(),
                'unit' => $ingredient->getUnit(),
            ];
        }, $ingredients);

        return new JsonResponse($ingredientsData);
    }

    /**
     * @Route("/pizza/monthly", name="api_pizza_monthly")
     */
    public function pizzaMonthlyApiAction(Request $request)
    {
        $year = $request->get("year");
        if (!$year) {
            $year = (new \DateTime())->format("Y");
        }

        $conn = $this->getDoctrine()->getEntityManager()->getConnection();

        $stmt = $conn->prepare("SELECT * FROM stat_pizza WHERE year = " . $year);
        $stmt->execute();
        $viewData = $stmt->fetchAll();

        return new JsonResponse($viewData);
    }


    /**
     * @Route("/upload/document", name="api_load_document")
     */
    public function loadDocumentAction(Request $request)
    {
        //curl -T 1.xml http://admin.venezia.by/api/upload/document
        if ($request->get("test") != "") {
            $importName = $request->get("test");
        } else {
            $importName = time() . ".xml";
            file_put_contents(
                __DIR__ . "/../../../var/import/" . $importName,
                file_get_contents('php://input')
            );
        }

        $importParser = $this->get('restaurant.import_parser');
        $xml = $importParser->loadXML($importName);

        if ((!is_object($xml)) || (!$xml->Body)) return new Response("файл не получен");
        $importParser->parseFile($importName);


        if ($request->get("test") != "") {
            return $this->render('@Restaurant/layout.html.twig');
        } else {
            $response = new Response(
                $this->renderView('@Service/Api/xmlUploadResponse.html.twig', [
                    'documentNumber' => (string)$xml->Header->ReceivedNo
                ]),
                200
            );
            $response->headers->set('Content-Type', 'text/xml');

            return $response;
        }
    }


    /**
     * @Route("/telegram", name="api_telegram_webhook")
     */
    public function telegramWebhookAction(Request $request)
    {
        $sendMessageUrl = "https://api.telegram.org/bot705986542:AAEs_PjxThw5lDheCoL2Ux7BWJ8oo0fm6XI/sendMessage";
        $data = json_decode($request->getContent());

        if (preg_match("#\d{3},\d{5}#", $data->message->text)) {
            $user = $this->getDoctrine()->getRepository("UserBundle:User")->findOneBy([
                'cardId' => $data->message->text,
            ]);
            if (!$user) {
                $this->get('telegram.service')->sendMessage('304344728', 'Карта не найдена' . $request->getContent());
            } else {
                $user->setTelegramChatId($data->message->chat->id);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->get('telegram.service')->sendMessage($data->message->chat->id, $user->getName() . ". Карта привязана.");
                $this->get('telegram.service')->sendMessage('304344728', $user->getName() . ". Карта привязана.");
            }
        }


        $this->get('user.logger')->info(var_export($request->getContent(), true));
        return new Response("");
    }

    /**
     * @Route("/pizza/sell", name="api_pizza_sell")
     */
    public function pizzaSellApiAction(Request $request)
    {
        $__pizzaData = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaByPeriod($request->get("shop"), $request->get("start"), $request->get("end"));
        $__pizzaCnt = 0;
        foreach ($__pizzaData as $__pizza) {
            $__pizzaCnt += $__pizza->getQuantity();
        }

        return new JsonResponse([
            'cnt' => $__pizzaCnt,
        ]);
    }

    /**
     * @Route("/pizza/delegated", name="api_pizza_delegated")
     */
    public function pizzaTerminalDelegatedApiAction(Request $request)
    {


        $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find($request->get('shop'));
        $dateStart = $request->get('dateStart');
        $dateEnd = $request->get('dateEnd');

        $delegatedDishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findDelegatedDishesByPeriodToShop($shop, $dateStart, $dateEnd);

        $pizzas = [];
        foreach ($delegatedDishes as $orderDish)
            if (in_array($orderDish->getDish()->getCategory()->getId(), [32, 82]))
                $pizzas[] = $orderDish;

        $ingrs = [];
        /**
         * @var OrderDish[] $pizzas
         */
        foreach ($pizzas as $pizza) {
            foreach ($pizza->getDish()->getIngredients() as $ingredient) {
                if (!array_key_exists($ingredient->getIngredient()->getName(), $ingrs))
                    $ingrs[$ingredient->getIngredient()->getName()] = 0;

                $ingrs[$ingredient->getIngredient()->getName()] += ($ingredient->getQuantity() * $pizza->getQuantity());
            }
        }

        return new JsonResponse($ingrs);
//        return $this->render('@Restaurant/Custom/test.html.twig', [
//            'ingr' => $ingrs
//        ]);
    }


    /**
     * @Route("/pizza/check", name="api_pizza_terminal_check")
     */
    public function pizzaTerminalCheckApiAction(Request $request)
    {

        $user = $this->getDoctrine()->getRepository("UserBundle:User")->find($request->get('user'));

        if (($request->getMethod() == "POST") && ($request->get('id') != null)) {
            $em = $this->getDoctrine()->getManager();
//var_dump(123);die();

            $dishPhoto = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->find($request->get('id'));

            $dishCheck = new DishPhotoCheck();
            $dishCheck->setUser($user);
            $dishCheck->setDishPhoto($dishPhoto);
            $dishCheck->setRate($request->get('rate'));
            $dishCheck->setReview($request->get('review'));
            if ($request->get('option') > 0) {
                $dishCheck->setDish($em->getReference('DishBundle\Entity\Dish', $request->get('option')));
            } else {
                $dishCheck->setDish(null);
            }
            $dishPhoto->addCheckTime();

            $em->persist($dishPhoto);
            $em->persist($dishCheck);
            $em->flush();
        }

        if ($user) {
            $shop = $user->getShop();
        } else {
            $shop = null;
        }
        if ($data = $this->getRandomPhotoAndOptions($user, $shop)) {

            $options = [];
            foreach ($data['options'] as $__opt) {
                $options[] = [
                    'id' => $__opt->getId(),
                    'name' => $__opt->getName(),
                ];

            }

            return new JsonResponse([
                'options' => $options,
                'photo' => [
                    'path' => $data['photo']->getPhotoPath(),
                    'id' => $data['photo']->getId(),
                ]

            ]);
        } else {
            return new JsonResponse([
                'photo' => false,
                'options' => false,
            ]);
        }
    }


    private function getRandomPhotoAndOptions($user, $shop = null)
    {
        $checkCount = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->getCheckCount();

        $photos = [];
        $checkTimes = $checkCount["minChecks"];
        while ((count($photos) <= 0) && ($checkTimes <= $checkCount["maxChecks"])) {
            $orderDishPhotos = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->findLowChecked($checkTimes, $shop);
            foreach ($orderDishPhotos as $__photo) {
                $add = true;
                foreach ($__photo->getChecks() as $__check)
                    if ($__check->getUser()->getId() == $user->getId()) $add = false;

                if ($add)
                    $photos[] = $__photo;
            }
            $checkTimes += 1;
        }


        if (count($photos) > 0) {
            shuffle($photos);
            $orderDishPhoto = $photos[0];

            $randomDishes = $this->getDoctrine()->getRepository('DishBundle:Dish')->findDishesForPizzaCheck($orderDishPhoto->getOrderDish());
            shuffle($randomDishes);

            $options = [];
            $options[] = $orderDishPhoto->getOrderDish()->getDish();
            foreach ($orderDishPhoto->getChecks() as $__check)
                if ($__check->getDish() && (!in_array($__check->getDish(), $options))) $options[] = $__check->getDish();

            $i = 0;
            while (count($options) < 5) {
                if (($randomDishes[$i]->getId() != $orderDishPhoto->getOrderDish()->getDish()->getId()) && (!in_array($randomDishes[$i], $options)))
                    $options[] = $randomDishes[$i];
                $i++;
            }
            shuffle($options);

            return [
                'options' => $options,
                'photo' => $orderDishPhoto
            ];
        } else {
            return false;
        }
    }


}
