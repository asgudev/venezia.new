function compare(a, b) {
    if (a.time > b.time) return 1;
    if (b.time > a.time) return -1;

    return 0;
}


var playback = {
    history: false,
    container: false,
    clientRect: false,
    scaleX: false,
    scaleY: false,
    prev: false,
    init: function ($container, init) {
        playback.container = $container;
        playback.container.css({"width": init.client[0], "height": init.client[1]});
        playback.container.attr("src", init.path);

        playback.clientRect = playback.container[0].getBoundingClientRect();

        playback.scaleX = playback.clientRect.width / init.client[0];
        playback.scaleY = playback.clientRect.height / init.client[1];
    },
    load: (history) => {


        let __hist = history.map((item) => {
            item.time = new Date(item.time);
            return item;
        });
        playback.history = __hist.sort(compare);


    },
    mouseClick: (x, y) => {
        var ev = document.createEvent("MouseEvent");
        var el = document.elementFromPoint(x, y);
        ev.initMouseEvent(
            "click",
            true /* bubble */, true /* cancelable */,
            window, null,
            x, y, 0, 0, /* coordinates */
            false, false, false, false, /* modifier keys */
            0 /*left*/, null
        );
        el.dispatchEvent(ev);
    },
    clicker: (step) => {
        if (step.hasOwnProperty('coords')) {
            let x = step.coords[0] * playback.scaleX + playback.clientRect.left;
            let y = step.coords[1] * playback.scaleY + playback.clientRect.top;

            var d = $("<div />", {"class": "clickEffect"}).css({top: y + "px", left: x + "px"});

            $("body").append(d);

            d.click(() => {
                playback.showNext(playback.history.shift());
                $(this).remove();
            });
        } else {
            playback.showNext(playback.history.shift());
        }

        // playback.mouseClick(step.coords[0], step.coords[1]);
    },
    play: () => {
        playback.showNext(playback.history.shift());
    },
    showStep: (step) => {
        playback.clicker(step);
        console.log(step);
    },
    showNext: (step) => {
        playback.showStep(step);

        // var next = playback.history.shift();
        // if (!next) return;
        // playback.showNext(next);
        // var timeout = next.time - step.time;
        // setTimeout(() => {
        //     playback.showNext(next)
        // }, timeout);
    }
};
