<?php

namespace ReportBundle\Controller;

use ReportBundle\Entity\Report;
use ReportBundle\Entity\ReportData;
use ReportBundle\Entity\ReportElement;
use ReportBundle\Form\Type\ReportType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/admin/report")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/list", name="report_list")
     */
    public function listAction()
    {

        $reports = $this->getDoctrine()->getRepository('ReportBundle:Report')->findLatest();

        return $this->render('ReportBundle:Default:list.html.twig', [
            'reports' => $reports,
        ]);
    }



    /**
     * @Route("/add", name="create_report")
     */
    public function createReportAction(Request $request)
    {
        $report = new Report();

        if ($request->get('id') != null) {


        }
//        foreach ($reportTemplate->getFields() as $__field) {
//            $__reportData = new ReportData($__field);
//            $report->addData($__reportData);
//        }

        $report->setCreatedBy($this->getUser());

        $form = $this->createForm(ReportType::class, $report, [

        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            foreach ($report->getData() as $__data) {
                $__data->setReport($report);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($report);
            $em->flush();

            return $this->redirectToRoute('show_report', [
                'id' => $report->getId()
            ]);
        }


        return $this->render('@Report/Default/editReport.html.twig', [
            'report' => $report,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show_report")
     */
    public function showReportAction(Request $request, Report $report)
    {
        if (!$report)
            throw new NotFoundHttpException();

        return $this->render('@Report/Default/showReport.html.twig', [
            'report' => $report,
        ]);

    }

    /**
     * @Route("/element/search", name="search_report_element")
     */
    public function searchReportTemplateElementAction(Request $request)
    {
        /**
         * @var ReportElement[] $reportElements
         */
        $reportElements = $this->getDoctrine()->getRepository("ReportBundle:ReportElement")->findByNameOrId($request->get("q"));

        $response = [];
        foreach ($reportElements as $__element) {
            $response[] = [
                "id" => $__element->getId(),
                "text" => $__element->getName(),
            ];
        }

        return new JsonResponse($response);
    }
}
