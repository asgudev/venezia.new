<?php

namespace OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderChange
 *
 * @ORM\Table(name="order_changes")
 * @ORM\Entity(repositoryClass="OrderBundle\Entity\Repository\OrderChangeRepository")
 */
class OrderChange
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderBundle\Entity\OrderDish")
     */
    private $orderDish;

    /**
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    const ORDER_CHANGE_DISH_DELETE = 0;
    const ORDER_CHANGE_DISH_QUANTITY = 1;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(name="difference", type="integer", nullable=false)
     */
    private $difference;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $administrator;

    public function __construct()
    {
        $this->datetime = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return OrderChange
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return OrderChange
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set orderDish
     *
     * @param \OrderBundle\Entity\OrderDish $orderDish
     *
     * @return OrderChange
     */
    public function setOrderDish(\OrderBundle\Entity\OrderDish $orderDish = null)
    {
        $this->orderDish = $orderDish;

        return $this;
    }

    /**
     * Get orderDish
     *
     * @return \OrderBundle\Entity\OrderDish
     */
    public function getOrderDish()
    {
        return $this->orderDish;
    }

    /**
     * Set administrator
     *
     * @param \UserBundle\Entity\User $administrator
     *
     * @return OrderChange
     */
    public function setAdministrator(\UserBundle\Entity\User $administrator = null)
    {
        $this->administrator = $administrator;

        return $this;
    }

    /**
     * Get administrator
     *
     * @return \UserBundle\Entity\User
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * Set difference
     *
     * @param integer $difference
     *
     * @return OrderChange
     */
    public function setDifference($difference)
    {
        $this->difference = $difference;

        return $this;
    }

    /**
     * Get difference
     *
     * @return integer
     */
    public function getDifference()
    {
        return $this->difference;
    }
}
