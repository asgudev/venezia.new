<?php

namespace FrontendBundle\Controller;


use DishBundle\Entity\Dish;
use FrontendBundle\Entity\WebOrder;
use FrontendBundle\Form\TransactionType;
use FrontendBundle\Form\WebOrderType;
use OrderBundle\Entity\Order;
use RestaurantBundle\Entity\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CartController extends BaseController
{

    const IS_PROMO = true;

    /**
     * @Route("/cart", name="frontend_cart_show")
     */
    public function showCartAction(Request $request)
    {
//die();
        $order = $this->get("session.order.manager")->getOrderObjectFromSession();
        $em = $this->getDoctrine()->getManager();

        $shop = $this->get("session")->get("takeaway-shop");

        if ($this->get('session')->get("order_type") == WebOrder::TYPE_PICKUP) {
            $order->setPickupShop($em->getReference(Shop::class, $this->get("session")->get("takeaway-shop")));
        }

        $takeawayShops = array_map(function (Shop $shop) {
            return $shop->getId();
        }, $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findTakeawayShops());

        $delay = $this->get("redis.helper")->get("shop_delay_".$shop) ?: 30;

        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($this->get("session")->get("takeaway-shop"));

        $shopStartTime = $shop->getTodayTimes()["start"];
        $shopStartDateTime = (new \DateTime(date('Y-m-d') . " " . $shopStartTime));


        $pickupTime = ($shopStartDateTime > new \DateTime() ? $shopStartDateTime : new \DateTime())->modify("+".$delay." min")->format('d.m.Y H:i');

        $form = $this->createForm(WebOrderType::class, $order, [
            'action' => $this->generateUrl('frontend_cart_checkout'),
            'method' => 'POST',
            'takeaway_shops' => $takeawayShops,
            'type' => $this->get('session')->get("order_type"),
            'pickupTime' => explode(" ", $pickupTime)[1],
        ]);



        $form->get('pickupTime')->setData($pickupTime);

        return $this->render('@Frontend/Cart/cartShow.html.twig', [
            'preorder' => $order,
            'pickupTime' => $pickupTime,
            'form' => $form->createView(),
            'orderSum' => $this->calcOrderSum($order),
        ]);
    }

    /**
     * @Route("/cart/checkout", name="frontend_cart_checkout")
     */
    public function checkoutCartAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $preorder = $this->get("session.order.manager")->getOrderObjectFromSession();

        $order = new WebOrder();

        $form = $this->createForm(WebOrderType::class, $order, [
            'action' => $this->generateUrl('frontend_cart_checkout'),
            'method' => 'POST',
            'type' => $this->get('session')->get("order_type"),
        ]);
        $code = null;
        $promoCard = null;
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $order->setType($this->get('session')->get("order_type"));
                $order->setLocalId(1);

                foreach ($preorder->getDishes() as $dish) {
                    $dish->setOrder($order);
                    $order->addDish($dish);
                }

                if ($promoCard) {
                    $order->setPromo($promoCard);
                    foreach ($order->getDishes() as $orderDish) {
                        if (!$orderDish->getDish()->getIgnoreDiscount()) {
                            $orderDish->setDishPrice($orderDish->getDish()->getShopPrice(0) * (1 - $promoCard->getCardDiscount() / 100));
                            $orderDish->setDishDiscount($orderDish->getDish()->getShopPrice(0) - $orderDish->getDishPrice());
                        }
                    }
                }
                $order->setPhone(preg_replace("#\D#", "", $order->getPhone()));

                if ($order->getType() == WebOrder::TYPE_PICKUP) {
                    $order->setPickupShop($em->getReference(Shop::class, $this->get("session")->get("takeaway-shop")));
                } else {
                    $order->setPickupShop($em->getReference(Shop::class, 2));
                }

                if (($order->getCashType() != Order::CASHTYPE_ONLINE) || ($order->getType() == WebOrder::TYPE_DELIVERY)) {
                    $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
                    //var_dump($code);
                    $this->get('session')->set('confirmation_code', $code);

                    if ($order->getType() == WebOrder::TYPE_PICKUP) {
                        $this->get('sms.service')->sendMessage($order->getPhone(), "Код подтверждения " . $code . ". Заказ " . $order->getId() . " будет готов в " . ($order->getPickupTime()) . " по адресу " . $order->getPickupShop()->getAddress());
                    } else {
                        $this->get('sms.service')->sendMessage($order->getPhone(), "Код подтверждения " . $code);
                    }
                }
                $em->persist($order);
                $em->flush();
                $this->get('session')->set("order_id", $order->getId());
            }
        }
        //
        $transactionData = [];
        if ($order->getCashType() == Order::CASHTYPE_ONLINE) {
            $transactionData = [
                'ap_storeid' => $this->getParameter('payment.ap_storeid'),
                'ap_order_num' => $order->getId(),
                'ap_client_dt' => $order->getCreatedAt()->format('U'),
                'ap_amount' => $order->getDishSum(),

                'ap_currency' => 'BYN',
                'ap_invoice_desc' => 'Оплата счета ' . $order->getPickupShop()->getName() . " " . $order->getId(),
            ];

            //if ($this->getParameter("kernel.environment") == 'dev') {
            //$transactionData['ap_test'] = '1';
            //}

            $transactionData['ap_signature'] = $this->getSignature($transactionData);

            $transactionForm = $this->createForm(TransactionType::class, null, [
                'action' => $this->getParameter('payment.gate'),
                'method' => 'POST'
            ]);
        }

//        dump($promoCard);

        return $this->render('@Frontend/Cart/cartCheckout.html.twig', [
            //'code' => $code,
            'promoCard' => $promoCard,
            'order' => $order,
            'form' => $form->createView(),
            'transaction' => $transactionData
        ]);
    }

    private function getSignature($data)
    {
        $string = null;
        uksort($data, 'strnatcmp');

        foreach ($data as $param => $value)
            $string .= $value . ';';

        $string .= $this->getParameter('payment.secret1');
        $key = hash('sha256', $string);

        return $key;

    }

    /**
     * @Route("/cart/confirmation", name="frontend_cart_confirmation")
     */
    public function sendOrderConfirmationAction(Request $request)
    {
        if ($this->get('session')->get('confirmation_code') == $request->get('code')) {
            $order = $this->getDoctrine()->getRepository("FrontendBundle:WebOrder")->find($this->get('session')->get("order_id"));
            $order->setStatus(Order::STATUS_PAID);
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            return new JsonResponse('success', 200);
        } else {
            $tries = $this->get('session')->get('confirmation_tries');
            $tries += 1;
            $this->get('session')->set('confirmation_tries', $tries);
            return new JsonResponse('fail', 403);
        }
    }

    /**
     * @Route("/cart/success", name="frontend_cart_success")
     */
    public function successCartAction(Request $request)
    {
        $order = $this->getDoctrine()->getRepository("FrontendBundle:WebOrder")->find($this->get('session')->get("order_id"));

        if ($order->getStatus() >= Order::STATUS_PAID) {
            $orderType = $this->get('session')->get('order_type');
            $this->get('session')->clear();

            $this->get('session')->set('active_order', $order->getId());

            return $this->render('@Frontend/Order/orderSuccess.html.twig', [
                'order' => $order,
                'orderType' => $orderType,
            ]);
        } else {
            return $this->render('@Frontend/Order/orderFail.html.twig', [
                'order' => $order,
            ]);
        }
    }

    /**
     * @Route("/cart/update", name="frontend_cart_update")
     */
    public function updateToCartAction(Request $request)
    {

        $this->get("session.order.manager")->setOrderToSession($request->get("data"));
        $order = $this->get('session.order.manager')->getOrderObjectFromSession();

        return $this->render("@Frontend/Cart/__cartShowTable.html.twig", [
            'order' => $order,
            'orderSum' => $this->calcOrderSum($order),
        ]);

    }

    /**
     * @Route("/cart/add", name="frontend_cart_add")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function addToCartAction(Request $request)
    {
        //$data = json_decode($request->getContent());

        /**
         * @var Dish $dish
         */
        $dish = $this->get("doctrine")->getRepository(Dish::class)->find($request->get('id'));

        if (!$dish) {
            return new JsonResponse(false);
        }

        $orderData = $this->get('session.order.manager')->changeQuantity($dish->getId(), 1);
//dump($orderData);die();
//        if (!array_key_exists(35587, $orderData) && $this->get("session")->get("order_type") == WebOrder::TYPE_DELIVERY) {
//            $this->get("session.order.manager")->changeQuantity(35587, 1);
//        }


        if ($request->get("page") === "catalog") {
            $order = $this->get('session.order.manager')->getOrderObjectFromSession();

            return $this->render("@Frontend/Cart/cartInline.html.twig", [
                "order" => $order,
                "orderSum" => $this->calcOrderSum($order),
            ]);
        } else {
            foreach ($orderData as $__dish => $__data) {
                $orderData[$__dish]["price"] = $this->getDoctrine()->getRepository(Dish::class)->find($__dish)->getPrice();
            }
            return new JsonResponse($orderData);
        }
    }



    /**
     * @Route("/cart/delete", name="frontend_cart_remove")
     */
    public function deleteFromCartAction(Request $request)
    {
        $dish = $this->get("doctrine")->getRepository("DishBundle:Dish")->find($request->get('id'));

        if (!$dish)
            return new JsonResponse(false);

        $orderData = $this->get('session.order.manager')->deleteDish($request->get('id'));

        $order = $this->get('session.order.manager')->getOrderObjectFromSession();

        return $this->render("@Frontend/Cart/cartInline.html.twig", [
            "order" => $order,
            "orderSum" => $this->calcOrderSum($order),
        ]);
    }

    /**
     * @Route("/cart/clear", name="frontend_cart_clear")
     */
    public function clearCartAction(Request $request)
    {
        $this->get("session.order.manager")->clear();

        return new JsonResponse([

        ]);
    }
}
