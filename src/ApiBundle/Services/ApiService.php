<?php


namespace ApiBundle\Services;


use Symfony\Component\DependencyInjection\Container;

class ApiService
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function call($path, $type, $data = null, $json = false)
    {

        $ch = curl_init($this->container->getParameter('api_path') . $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($type == "POST")
            curl_setopt($ch, CURLOPT_POST, true);

        if ($data != null) {
            if ($json) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            }

        }


        $res = curl_exec($ch);

        return $res;
    }

}