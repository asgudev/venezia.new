<?php

namespace RestaurantBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use FrontendBundle\Entity\WebShop;
use FrontendBundle\Entity\WebShopImage;
use DishBundle\Entity\DishCategory;
use RestaurantBundle\Entity\Shop;

class ShopRepository extends EntityRepository
{

    public function findTakeawayShops() {
        return $this->createQueryBuilder('s')
            ->where('BIT_AND(s.orderTypes, 16) = 16')
            ->getQuery()
            ->getResult();
    }


    public function findFullShop($shop)
    {
        $shop = $this->createQueryBuilder('s')
            ->where("s.id = :shop")
            ->addSelect('p')
            ->addSelect('t')
            ->addSelect('sd')
            ->addSelect('sdm')
            ->addSelect('sdc')
            ->leftJoin('s.dishes', 'sd', "WITH", 'sd.isDeleted = false')
            ->leftJoin('sd.dishMetas', 'sdm', 'WITH', 'sdm.shop = :shop')
            ->leftJoin('sd.category', 'sdc')
            ->leftJoin('s.places', 'p')
            ->leftJoin('p.tables', 't')
            ->setParameters([
                'shop' => $shop
            ])
            ->getQuery()
            ->getSingleResult();
        return $shop;
    }

    public function findDelegates($shop)
    {
        return $this->createQueryBuilder('s')
            ->where('sd.delegate = :shop')
            ->leftJoin('s.delegates', 'sd')
            ->setParameters([
                'shop' => $shop
            ])
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $shop_id
     * @return Shop
     */
    public function findTerminalShop($shop_id)
    {
        return $this->createQueryBuilder('s')
            ->where('s.id = :shop_id')
            ->addSelect('sp')
            ->addSelect('spt')
            ->leftJoin('s.places', 'sp')
            ->leftJoin('sp.tables', 'spt')
            ->setParameters([
                'shop_id' => $shop_id
            ])
            ->getQuery()
            ->getOneOrNullResult();

    }


    public function findAdminShops($shops)
    {
        $ids = [];
        foreach ($shops as $shop) {
            $ids[] = $shop->getId();
        }

        $shops = $this->createQueryBuilder('s')
            ->where("s.id IN (:ids)")
            ->setParameters([
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();

        return $shops;
    }

    public function findShopWithPrinter(Shop $shop, DishCategory $category)
    {
        $shop = $this->createQueryBuilder('s')
            ->where('s = :shop')
            ->addSelect('sp')
            ->leftJoin('s.printers', 'sp', 'WITH', 'sp.category = :category')
            ->setParameters([
                'shop' => $shop,
                'category' => $category,
            ])
            ->getQuery()
            ->getOneOrNullResult();
        /*dump($shop);
        die();*/
        return $shop;
    }




}
