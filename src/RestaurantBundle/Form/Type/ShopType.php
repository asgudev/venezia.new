<?php

namespace RestaurantBundle\Form\Type;

use DishBundle\Entity\CategoryDiscount;
use DishBundle\Form\Type\CategoryDelegateType;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Service\BitToArray;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", TextType::class, [
                'label' => 'Название заведения',
            ])
            ->add("address", TextType::class, [
                'label' => 'Адрес',
                'required' => false,
            ])
            ->add("phone", TextType::class, [
                'label' => 'Телефон',
                'required' => false,
            ])
            ->add('times', ShopTimesType::class, [
                'label' => 'Время работы',
            ])
            ->add("customParams", ShopCustomParamsType::class, [
                'label' => "Прочие параметры",
            ])
            ->add("imageFile", FileType::class, [
                'label' => 'Фото',
                'required' => false,
                'file_path' => 'image',
            ])
            ->add("places", CollectionType::class, [
                'entry_type' => PlaceType::class,
                'label' => 'Площадка',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                'by_reference' => false,
            ])
            ->add("adminPrinter", TextType::class, [
                'label' => 'Принтер администратора',
                'required' => false,
            ])
            ->add("invoicePrinter", TextType::class, [
                'label' => 'Принтер для счетов',
                'required' => false,
            ])
            ->add("delegates", CollectionType::class, [
                'entry_type' => CategoryDelegateType::class,
                'label' => "Делегаты",
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'by_reference' => false,
            ])
            ->add('isBarService', CheckboxType::class, [
                'label' => 'Обслуживание за стойкой',
                'required' => false,
            ])
            ->add('fiscalType', ChoiceType::class, [
                'label' => 'Тип кассы',
                'choices' => [
                    'Касса' => Shop::FISCAL_TYPE_CASH,
                    'Фискальный' => Shop::FISCAL_TYPE_TERMINAL,
                ],
                'multiple' => false,
                'expanded' => false,
                'required' => true,
            ])
            ->add("orderTypes", ChoiceType::class, [
                'label' => "Типы заказов",
                'multiple' => true,
                'expanded' => true,
                'choices' => Shop::ORDER_TYPE
            ]);

        $builder
            ->get('orderTypes')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    $bittoarray = new BitToArray();
                    return $bittoarray->bitToArray($output, Shop::ORDER_TYPE);
                },
                function ($input) {
                    $bitmask = 0;
                    foreach ($input as $mask) {
                        $bitmask += $mask;
                    }
                    return $bitmask;
                }));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Shop::class,
        ));
    }
}
