<?php

namespace FrontendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OrderBundle\Entity\Order;

class CheckoutController extends BaseController
{
    /**
     * @Route("/checkout/success", name="frontend_checkout_success")
     */
    public function successCheckoutAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (!$data) {
            parse_str($request->getContent(), $data);
        }

        $f = fopen('test_success.log', 'a+');
        fputs($f, (new \DateTime())->format('d.m.Y H:i') . ' ' . $request->getContent() . "\r\n");
        fclose($f);

        if ($this->checkSignature($data) && $data['ap_operation_status'] == 'success') {
            $em = $this->getDoctrine()->getManager();
            $order = $this->getDoctrine()->getRepository('FrontendBundle:WebOrder')->find($data['ap_order_num']);
            if ($order->getStatus() != Order::STATUS_PRINTED) {
                $order->setStatus(Order::STATUS_PAID);
                $em->persist($order);
                $em->flush();
            }

            $this->get('session')->set('order', []);
        } else {
            return $this->redirectToRoute('frontend_cart_show');
        }

        return $this->render('@Frontend/Order/orderSuccess.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/checkout/result", name="frontend_checkout_result")
     */
    public function resultCheckoutAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (!$data) {
            parse_str($request->getContent(), $data);
        }

        if ($this->checkSignature($data)) {
            $em = $this->getDoctrine()->getManager();
            $order = $this->getDoctrine()->getRepository('FrontendBundle:WebOrder')->find($data['ap_order_num']);
            if ($order->getStatus() != Order::STATUS_PRINTED) {
                $order->setStatus(Order::STATUS_PAID_VERIFIED);
                $em->persist($order);
                $em->flush();
            }
            $this->get('session')->set('order', []);
        } else {
            return $this->redirectToRoute('frontend_cart_show');
        }


        $f = fopen('test_result.log', 'a+');
        fputs($f, (new \DateTime())->format('d.m.Y H:i') . ' ' . $request->getContent() . "\r\n");
        fclose($f);


        return new Response('');
    }

    /**
     * @Route("/checkout/fail", name="frontend_checkout_fail")
     */
    public function failCheckoutAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (!$data) {
            parse_str($request->getContent(), $data);
        }

        $f = fopen('test_fail.log', 'a+');
        fputs($f, (new \DateTime())->format('d.m.Y H:i') . ' ' . $request->getContent() . "\r\n");
        fclose($f);

        return new Response('');
    }

    private function checkSignature($data)
    {
        if (!isset($data['ap_signature']))
            return false;

        $addSignature = $data['ap_signature'];
        unset($data['ap_signature']);

        uksort($data, 'strnatcmp');
        $string = implode(';', $data) . ';' . $this->getParameter('payment.secret2');

        return hash('sha256', $string) == $addSignature;

    }
}