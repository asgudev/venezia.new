<?php

namespace DishBundle\Form\Type;

use DishBundle\Entity\DishCategory;
use DishBundle\Entity\Ingredient;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IngredientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название',
            ])
            ->add('unit', ChoiceType::class, [
                'label' => 'Ед. изм.',
                'choices' => [
                    'кг' => 'кг',
                    'шт' => 'шт',
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('id1c', TextType::class, [
                'label' => 'Код 1C',

            ])
            ->add("category", EntityType::class, [
                'label' => 'Категория',
                'class' => DishCategory::class,
                'multiple' => false,
                'expanded' => false,
                'choice_label' => 'name',
            ])
            ->add('shouldBeValidated', CheckboxType::class, [
                'label' => 'Блокирует закрытие смены',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить'
            ])
            ->add('delete', SubmitType::class, [
                'attr' => [
                    'class' => 'md-btn md-btn-danger'
                ],
                'label' => 'Удалить'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Ingredient::class,
        ));
    }
}
