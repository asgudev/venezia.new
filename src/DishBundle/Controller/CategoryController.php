<?php

namespace DishBundle\Controller;

use DishBundle\Entity\DishCategory;
use DishBundle\Entity\DishHistory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use DishBundle\Form\Type\DishCategoryType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/admin")
 */
class CategoryController extends Controller
{

    /**
     * @Route("/category/list", name="category_list")
     */
    public function categoryListAction()
    {
        $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findAll();

        return $this->render('DishBundle:Category:categoryList.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/{id}/print", name="category_list_print")
     */
    public function categoryListPrintAction(Request $request, DishCategory $category)
    {

        $category = $this->getDoctrine()->getRepository('DishBundle:DishCategory')->findFullCategory($category);

        return $this->render('DishBundle:Category:categoryPrint.html.twig', [
            'category' => $category
        ]);
    }

    /**
     * @Route("/category/list/sort", name="category_list_sort")
     */
    public function categoryListSortAction()
    {
        $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findAll();

        return $this->render('DishBundle:Category:categoryListEdit.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/list/sort/update", name="category_list_sort_ajax")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function categoryListSortUpdateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newOrder = $request->get("data");

        if ($newOrder) {
            foreach ($newOrder as $order) {
                $cat = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->find($order["category"]);
                $cat->setOrder($order["order"]);
                $em->persist($cat);
            }
            $em->flush();
        }

        $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findAll();

        $row = $this->get("twig")->loadTemplate("DishBundle:Category:categoryListEdit.html.twig")->renderBlock("category_list", [
            'categories' => $categories
        ]);

        return new JsonResponse([
            'status' => true,
            'data' => $row
        ]);
    }

    /**
     * @Route("/category/add", name="category_create")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function categoryCreateAction(Request $request)
    {
        $category = new DishCategory();
        $form = $this->createForm(DishCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $image = $category->getImageFile();
            if (($image != null) && ($image != "no_image.png")) {
                $fileName = md5(uniqid()) . '.' . $image->guessExtension();
                $imagessDir = $this->getParameter('kernel.root_dir') . '/../web/uploads/images';
                $image->move($imagessDir, $fileName);

                $category->setImage($fileName);
            }

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('category_edit', ['id' => $category->getId()]);
        }

        $shops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAdminShops($this->getUser()->getAdminShops());

        return $this->render('DishBundle:Category:categoryEdit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'shops' => $shops,

        ]);
    }

    /**
     * @Route("/category/{id}/edit", name="category_edit")
     */
    public function categoryEditAction(Request $request, $id)
    {
        /**
         * @var DishCategory $category
         */
        $category = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findFullCategory($id, $request->getLocale());

        if (!$category) {
            dump("error");
            die();
        }

        $em = $this->getDoctrine()->getEntityManager();


        $form = $this->createForm(DishCategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isValid()) {
            if (!$this->getUser()->hasRole('ROLE_BUHGALT'))
                throw new AccessDeniedException();

            $category->setTranslatableLocale($request->getLocale());

            $em->persist($category);
            $em->flush();
            return $this->redirectToRoute('category_edit', ['id' => $category->getId()]);
        }

        $category->setTranslatableLocale($request->getLocale());
        $em->refresh($category);

        $shops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAdminShops($this->getUser()->getAdminShops());
        $replication = $this->get('restaurant.replication')->getConnectedSlaves();

        $dishes = $this->getDoctrine()->getRepository('DishBundle:Dish')->findDishesByCategory($category, $request->getLocale());

/*        $pricesMixer = $this->get('restaurant.pending_price_mixer');
        $pendingChanges = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_NEW, "ASC", null, 999999);
        foreach ($dishes as $dish) {
            foreach ($pendingChanges as $change) {
                if ($dish->getId() == $change->getDish()->getId()) {
                    $pricesMixer->mixToDish($dish, $change);
                }
            }
        }
*/
        return $this->render('DishBundle:Category:categoryEdit.html.twig', [
            'category' => $category,
            'dishes' => $dishes,
            'form' => $form->createView(),
            'shops' => $shops,
            'replication' => $replication,

        ]);
    }


    /**
     * @Route("/get_categories_ajax", name="get_categories_ajax")
     */
    public function getCategoriesAjaxAction(Request $request)
    {
        $cats = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findAll();

        $cats_array = [];

        foreach ($cats as $cat) {
            $cats_array[] = [
                'id' => $cat->getId(),
                'name' => $cat->getName()
            ];
        }

        return new JsonResponse($cats_array);
    }
}
