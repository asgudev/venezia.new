<?php

namespace UserBundle\Services;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AfterLoginRedirection implements AuthenticationFailureHandlerInterface, AuthenticationSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->get('ajax')) {
            $result = array(
                'success' => false,
                'message' => 'Неверный пароль',
                'error' => true,
            );
            $response = new JsonResponse($result);
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        } else {
            return new RedirectResponse($request->headers->get('referer'));
        }
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();
        $roles = $token->getRoles();
        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $roles);

        if ($this->isTrustedTablet($request->headers->all())) {
            $redirection = new RedirectResponse($this->router->generate('terminal_index'));
            return $redirection;
        }


        if ((in_array('ROLE_ADMIN', $rolesTab)) || (in_array('ROLE_SUPER_ADMIN', $rolesTab))) {
            if ($user->getId() == 29) $redirection = new RedirectResponse($this->router->generate('clear_daily_bugs'));
            elseif ($user->getId() == 2 && $request->get('ajax')) {
//                $redirection = new RedirectResponse($this->router->generate('terminal_index'));
                $redirection = new JsonResponse([
                    'success' => true,
                    'target' => '/terminal/',
                ]);
            }
            else $redirection = new RedirectResponse($this->router->generate('index'));
        } elseif (in_array('ROLE_MARKETMAN', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('index'));
        } elseif (in_array('ROLE_SHOP_WATCHER', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('index'));
        } elseif (in_array('ROLE_SEO', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('site_shop_list'));
        } elseif (in_array('ROLE_PIZZA', $rolesTab, true)) {

            if ($request->get('ajax')) {
                $result = array(
                    'success' => true,
                    'target' => '/terminal/pizza',
                );
                $redirection = new JsonResponse($result);
            } else {
                $redirection = new RedirectResponse($this->router->generate('pizza_terminal'));
            }
        } elseif (in_array('ROLE_USER', $rolesTab, true)) {

            if ($request->get('ajax')) {
                $result = array(
                    'success' => true,
                    'target' => '/terminal',
                );
                $redirection = new JsonResponse($result);
            } else {
                $redirection = new RedirectResponse($this->router->generate('terminal_index'));
            }

        } else {
            $redirection = new RedirectResponse($this->router->generate('login'));
        }

        return $redirection;
    }

    private function isTrustedTablet($headers)
    {
        if (array_key_exists("asgumobileclient", $headers) && ($headers["asgumobileclient"][0] == true)) {
            return true;
        }

        return false;
    }
}
