<?php

namespace RestaurantBundle\Twig;


class PercentFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('percent', array($this, 'percentFilter')),
        );
    }

    public function percentFilter( $element, $input)
    {
        $sum = array_sum($input);


        return ($sum != 0) ? ($element / $sum) * 100 : 0;
    }

    public function getName()
    {
        return 'percent';
    }

}