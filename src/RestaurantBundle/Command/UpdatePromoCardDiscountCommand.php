<?php

namespace RestaurantBundle\Command;


use RestaurantBundle\Entity\PromoCard;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdatePromoCardDiscountCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('resto:promo:update')
            ->setDescription('Update promo card discount');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $discountService = $this->getContainer()->get('discount_counter.service');

            $promoCards = $this->getContainer()->get('doctrine')->getRepository('RestaurantBundle:PromoCard')->findAll();
            $em = $this->getContainer()->get('doctrine.orm.entity_manager');
            /** @var PromoCard[] $promoCards */
            foreach ($promoCards as $promoCard) {
                switch ($promoCard->getType()) {
                    case PromoCard::TYPE_CARD:
                        $calculatedDiscount = $discountService->getDiscount($promoCard);
                        if ($promoCard->getCardDiscount() != $calculatedDiscount) {
                            $promoCard->setCardDiscount($calculatedDiscount);
                            $em->persist($promoCard);
                        }
                        break;
                    case PromoCard::TYPE_TOURIST:
                        break;
                        if ($promoCard->getIssuedAt() && (((new \DateTime())->getTimestamp() - $promoCard->getIssuedAt()->getTimestamp()) > (3600))) {
                            $promoCard->setCardDiscount(15);
                            $em->persist($promoCard);
                            $em->flush();
                        }
                        break;
                }

            }
            $em->flush();
        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}
