<?php

namespace FrontendBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use DishBundle\Entity\Dish;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\Validator\Constraints\DateTime;
use OrderBundle\Entity\Order;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;
use UserBundle\Entity\User;


/**
 * @ORM\Entity(repositoryClass="FrontendBundle\Entity\Repository\WebOrderRepository")
 * @ORM\Table()
 */
class WebOrder
{

    const TYPE_PICKUP = 0;
    const TYPE_DELIVERY = 1;

    const STATUS_FAIL = -2;
    const STATUS_CANCEL = -1;
    const STATUS_OPENED = 0;
    const STATUS_WAIT_FOR_KITCHEB = 1;
    const STATUS_WAIT_FOR_CLOSE = 3;
    const STATUS_CLOSED = 4;
    const STATUS_DELETED = 5;

    const STATUS_WAIT_FOR_MONEY = 10;
    const STATUS_PAID = 20;
    const STATUS_PAID_VERIFIED = 25;
    const STATUS_PRINTED = 30;
    const STATUS_READY = 40;

    const CASHTYPE_CASH = 0;
    const CASHTYPE_ONLINE = 1;
    const CASHTYPE_INVOICE = 2;
    const CASHTYPE_INTERNAL = 3;
    const CASHTYPE_CASH_CARD = 4;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="FrontendBundle\Entity\WebOrderDish", mappedBy="order", cascade={"persist"})
     */
    private $dishes;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\PromoCard", inversedBy="orders")
     */
    private $promo;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(name="local_id", type="integer", nullable=false)
     */
    private $localId;

    /**
     * @ORM\Column(name="cash_type", type="smallint", nullable=true, options={"default" = 0})
     */
    private $cashType;

    /**
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = Order::STATUS_OPENED;

    }

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $pickupShop;

    /**
     * @ORM\Column(name="pickup_time", type="string", nullable=true)
     */
    private $pickupTime;

    /**
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(name="comment", type="text", length=300, nullable=true)
     */
    private $comment;



    /**
     * @return Shop
     */
    public function getPickupShop()
    {
        return $this->pickupShop;
    }

    /**
     * @param mixed $pickupShop
     * @return WebOrder
     */
    public function setPickupShop($pickupShop)
    {
        $this->pickupShop = $pickupShop;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPickupTime()
    {
        return $this->pickupTime;
    }

    /**
     * @param mixed $pickupTime
     * @return WebOrder
     */
    public function setPickupTime($pickupTime)
    {
        $this->pickupTime = $pickupTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return WebOrder
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return WebOrder
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     * @return WebOrder
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return WebOrder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return WebOrderDish[]
     */
    public function getDishes()
    {
        return $this->dishes;
    }

    /**
     * @param mixed $dishes
     * @return WebOrder
     */
    public function setDishes($dishes)
    {
        $this->dishes = $dishes;
        return $this;
    }

    /**
     * Add dish
     *
     * @param WebOrderDish $dish
     *
     * @return WebOrder
     */
    public function addDish(WebOrderDish $dish)
    {
        $this->dishes[] = $dish;
        $dish->setOrder($this);

        return $this;

    }

    /**
     * @return mixed
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param mixed $promo
     * @return WebOrder
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return WebOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocalId()
    {
        return $this->localId;
    }

    /**
     * @param mixed $localId
     * @return WebOrder
     */
    public function setLocalId($localId)
    {
        $this->localId = $localId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCashType()
    {
        return $this->cashType;
    }

    /**
     * @param mixed $cashType
     * @return WebOrder
     */
    public function setCashType($cashType)
    {
        $this->cashType = $cashType;
        return $this;
    }


    public function getFullSum()
    {
        $sum = 0;
        foreach ($this->dishes as $dish) {
            $sum += ($dish->getFullSum());
        }
        return $sum;
    }

    public function getDishSum()
    {
        $sum = 0;
        if ($this->dishes) {
            foreach ($this->dishes as $dish) {
                $sum += ($dish->getCashSum());
            }
        }
        return $sum;
    }

    public function getSumDiscount()
    {
        $sumDiscount = 0;
        foreach ($this->dishes as $dish) {
            $sumDiscount += ($dish->getDiscountSum());
        }
        return $sumDiscount;
    }


    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return WebOrder
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

//ALTER TABLE web_order_dish DROP parent_dish_id, DROP parent_order_id, DROP delegate_id, DROP dish_id, DROP order_id

}