#!/usr/bin/env bash
BACKUP_DIR=$(date '+%Y-%m-%d')
mkdir $BACKUP_DIR

mysqldump -uvenezia -p9WhsFYfbQjdQz7uR  venezia_new --ignore-table=venezia_new.orders --ignore-table=venezia_new.order_dish --ignore-table=venezia_new.daily_report --ignore-table=venezia_new.ingridient_data --ignore-table=venezia_new.delegate_order --ignore-table=venezia_new.order_changes --ignore-table=venezia_new.order_dish_photo --ignore-table=venezia_new.order_dish_photo_check --ignore-table=venezia_new.order_changes --ignore-table=venezia_new.wifi_client --ignore-table=venezia_new.wifi_connect --ignore-table=venezia_new.wifi_promo --ignore-table=venezia_new.wifi_router > $BACKUP_DIR/data_master.sql --replace --set-gtid-purged=OFF --no-create-info --lock-tables=false --skip-triggers --quick &
wait

./__sync_repl.sh 10.8.0.2  tnSdZaZzr4NF2JNruu6ND6amDnQ2qzbC venezia     $BACKUP_DIR \ 
	cafevenezia_master  TY3BMxBAxjARrS73hvppN9DEkWgj68rQ cafevenezia_slave eMqpnbuxcJMXgUDFSThzC8mLZjjXq3Z8 &

./__sync_repl.sh 10.8.0.16 YtfhBx5sTH73vdSJeAxrueJRqneyMGGT italia	$BACKUP_DIR \
	cafeitalia_master   xZP7mmFytwmssANARDhd3FRKgLTt4TgD cafeitalia_slave  emSBZTtPzcM4XB3FnKC5aejyXg2eqKhh &

./__sync_repl.sh 10.8.0.10 pYAyDL8CUsgEjNpRn4AFwTuCJtVAs6Kg metromilano $BACKUP_DIR \
	cafemetro_master    C5mjubJZJXHpmCtZKqp3Y6CWpXGu9fNQ cafemetro_slave   WbXFg92jFVHkhFHbFTKjA3mKwWpvzvbP &

./__sync_repl.sh 10.8.0.8  ZePMxrYsKV5qS7pC6qprQFXPq8czHtH7 urban       $BACKUP_DIR \
	cafeverdi_master    eZmYXJUKUrexvuaqaahGRQVeuFeb4HAe cafeverdi_slave   Musdx2YTxeRTCTaLQ6GDp4zNpzVwfkQZ &

./__sync_repl.sh 10.8.0.12 zVLcdUFREM62TH42h47ndUuLxXW6yKjQ rechica     $BACKUP_DIR \
	caferechica_master  pV8DHhusc74W72QC5RdmqJnHb6NEzfTA caferechica_slave TnEWWjFjpBVcPxuTxQ6qKjAr7gb4yrZP &

./__sync_repl.sh 10.8.0.6  L3jpyZZJcVCysvn7e6eWL7KkZSnCmUEt sov55	$BACKUP_DIR \
	cafealtaglio_master jxweLvMp3JJ9yzPTC5cTLmwjew7Ehmxk cafealtaglio_slave jrGxZyYFvyARL49tSNb2xGj3JWBrDrpj &

./__sync_repl.sh 10.8.0.24 HR2EqPTCLYgPDjb78Sahw8hNBQNv4ZE5 fusion1     $BACKUP_DIR \
	cafedionis1_master  qmd9uPPgScTAueEWU4YbnuZpNQ9DB6qB cafedionis1_slave T2PZ4mmTjHvjhv4gSBy9dWTvV9CmAEJa &

./__sync_repl.sh 10.8.0.20 vtkmJf78DXPKgXNNhd3YrUEVZvwuw65x vostok24    $BACKUP_DIR \
	cafeastor24_master  utQCYZGZuajMXPjByXA4AysXZ9LNx3G4 cafeastor24_slave sX4YZEuy886PszB4ZLBwdYdRJKPJ5hEv &

./__sync_repl.sh 10.8.0.22 QMLvRDtE4aM6ZxfVDaKZR7NjACfVFCQp draft	$BACKUP_DIR \
	cafedraft_master    vNhhbahcu4egmqvKp7MSd9C8BCbLRKXF cafedraft_slave vNhhbahcu4egmqvKp7MSd9C8BCbLRKXF &

./__sync_repl.sh 10.8.0.26 v2RKsLBvEUF4U4HXhaVbu25mBSGFUTge grancafe    $BACKUP_DIR \
	cafegran_master     zgUFkPMzehLBTMS5ZkhYgAyTdwuHFCF6 cafegran_slave 9zr7PzAdaY5ZMLWXKqMgDS55Np3yn9Qm &

./__sync_repl.sh 10.8.0.14 XGrGM2xcpPk2hqTzwtX5GJ5kaZywkTSk vokzal    $BACKUP_DIR \
        cafevokzal_master     uKf2nWMHHfkzScR6mrRhPeza5Ua9guD2 cafevokzal_slave XFW4RxMC49LdVJEGvau5V2LtyLGcYFVh &

wait

echo "FIN"
