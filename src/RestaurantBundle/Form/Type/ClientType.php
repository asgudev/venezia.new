<?php

namespace RestaurantBundle\Form\Type;

use RestaurantBundle\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", TextType::class, [
                'label' => 'Название',
            ])
            ->add("unp", TextType::class, [
                'label' => 'УНП'
            ])
            /*->add("bankNumber", TextType::class, [
                'label' => 'Номер счета',
                'required' => false,
            ])*/
            ->add('isExportable', CheckboxType::class, [
                'label' => 'Выгружать в 1с',
                'required' => false,
            ])
            ->add('trackInDelivery', CheckboxType::class, [
                'label' => 'Отслеживать в доставке',
                'required' => false,
            ])
            ->add('isDeleted', CheckboxType::class, [
                'label' => 'Отключить',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить'
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Client::class,
        ));
    }
}
