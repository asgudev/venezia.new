<?php

namespace TerminalBundle\Controller;


use TerminalBundle\Entity\DailyReport;
use ControlBundle\Entity\DishPhoto;
use ControlBundle\Entity\DishPhotoCheck;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class PizzaController extends Controller
{

    // rsync -avzh -e 'ssh -i /root/.ssh/id_rsa' /www/web/uploads/photos root@admin.venezia.by:/web/web/uploads

    const API_PATH = 'https://admin.venezia.by';

    /**
     * @Route("/terminal/pizza/submit", name="pizza_terminal_check_submit")
     */
    public function pizzaTerminalCheckSubmitAction(Request $request)
    {

        $data = [
            'id' => $request->get('id'),
            'user' => $this->getUser()->getId(),
            'option' => $request->get('option'),
            'rate' => $request->get('rate'),
            'review' => $request->get('review')
        ];

        $ch = curl_init(self::API_PATH . $this->generateUrl('api_pizza_terminal_check'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $response = curl_exec($ch);

        $data = json_decode($response);
        curl_close($ch);

        if ($data->photo) {
            return new JsonResponse([
                'photo' => $data->photo,
                'options' => $data->options,
            ]);
        } else {
            return new JsonResponse([
                'photo' => false
            ]);
        }
    }

    /**
     * @Route("/terminal/pizza/photo", name="pizza_terminal_get_photo")
     */
    public function getPhotoAction(Request $request)
    {
        //      $input = 'http://admin:v2705705@178.172.245.51:10801/ISAPI/Streaming/channels/1/picture'; // ven
        //      $input = 'http://admin:12345@192.168.1.26/Streaming/channels/1/picture'; // vostok
        //      $input = 'http://admin:Lk22pre03sid19ent@192.168.0.102/Streaming/channels/1/picture'; // vostok
        //     	$input = 'http://admin:QazWsx123@192.168.0.30/ISAPI/Streaming/channels/1/picture'; // fusion
        //     	$input = 'http://admin:159753qwe@192.168.1.64/ISAPI/Streaming/channels/1/picture'; //rechica
        //      $input = 'http://admin:QazWsx123@192.168.0.231/ISAPI/Streaming/channels/1/picture'; // metro
        //      $input = 'http://admin:QazWsx123@192.168.1.219/ISAPI/Streaming/channels/1/picture'; // metro

        $input = $this->getParameter("cam_path");

        $photoPath = $this->get('kernel')->getRootDir() . '/../web/uploads/photos/';

        $photo = time() . "_" . uniqid() . '.jpg';
        file_put_contents($photoPath . $photo, file_get_contents($input));


        $dish = $this->getDoctrine()->getRepository("OrderBundle:OrderDish")->findOneBy([
            'id' => $request->get('dish'),
        ]);


        return new JsonResponse([
            'photo' => $photo,
            'weight' => file_get_contents('http://127.0.0.1:3000'),
            'normalWeight' => $dish->getDish()->getWeight(),
        ]);
    }


    /**
     * @Route("/terminal/pizza/check", name="pizza_terminal_check")
     */
    public function pizzaTerminalCheckAction(Request $request)
    {
        if ($this->getUser()->hasRole('ROLE_ADMIN')) {
            $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();
        } else {
            $shops = [];
        }

        $ch = curl_init(self::API_PATH . $this->generateUrl('api_pizza_terminal_check'));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'user' => $this->getUser()->getId(),
        ]));

        $response = curl_exec($ch);

        $data = json_decode($response);

        curl_close($ch);

        return $this->render('TerminalBundle:Pizza:pizzaCheck.html.twig', [
            'shops' => $shops,
            'photo' => $data->photo,
            'options' => $data->options,
            'data' => $response,
        ]);

    }

    /**
     * @Route("/terminal/pizza", name="pizza_terminal")
     */
    public function pizzaTerminalAction(Request $request)
    {
        if ($this->getUser()->hasRole('ROLE_ADMIN')) {
            $shops = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->findAll();
        } else {
            $shops = [];
        }

        return $this->render('TerminalBundle:Pizza:pizza.html.twig', [
            'shops' => $shops,
        ]);
    }

    /**
     * @Route("/terminal/pizza/listener", name="pizza_terminal_listener")
     */
    public function pizzaTerminalListenerLoadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $dishPhoto = new DishPhoto();
        $dishPhoto->setWeight($request->get('weight'));
        $dishPhoto->setPhotoPath($request->get('filename'));
        $dishPhoto->setUser($this->getUser());
        $dishPhoto->setOrderDish($em->getReference('OrderBundle:OrderDish', $request->get('orderDishId')));

        $em->persist($dishPhoto);
        $em->flush();

        if ($dishPhoto->getOrderDish()->getOrder()->getShop()->getId() == 14) {
            $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find($this->getParameter("shop.id"));
            $this->get('terminal.print.storage')->printDeliveryTicketOrder($shop, $dishPhoto);
        }


        return new JsonResponse([
            'error' => false,
        ]);
    }

    /**
     * @Route("/terminal/pizza/load", name="pizza_terminal_load")
     */
    public function pizzaTerminalLoadAction(Request $request)
    {
        if ($this->getUser() != null) {
            $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findTerminalShop($this->getUser()->getShop());
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }


        $reportStart = $this->get("redis.helper")->get("shift_start_" . $shop->getId());

        if (!$reportStart) {
            $previousReport = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($this->getUser()->getShop());
            if (!$previousReport) {
                $previousReport = new DailyReport($shop);
                $previousReport->setDateClosed(new \DateTime('01-01-1970 00:00'));
            } else {
                $previousReport = $previousReport[0];
            }
            $reportStart = $previousReport->getDateClosed()->format("Y-m-d H:i:s");
            $this->get("redis.helper")->set("shift_start_" . $shop->getId(), $previousReport->getDateClosed()->format("Y-m-d H:i:s"), 3600);
        }


        $pizza = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findPizzaThisShift($this->getUser()->getShop(), $reportStart);

        $photos = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->findPhotoed($pizza);

        $pizzaArray = [];
        foreach ($pizza as $_pizza) {
            $pizzaArray[$_pizza->getId()] = $_pizza;
        }

        foreach ($photos as $photo) {
            unset($pizzaArray[$photo->getOrderDish()->getId()]);
        }

        return $this->render('@Terminal/Pizza/pizzaList.html.twig', [
            'pizza' => $pizzaArray,
        ]);
    }
}
