<?php

namespace RestaurantBundle\Command;

use DishBundle\Entity\Dish;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use OrderBundle\Entity\OrderMeta;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\ShopPlaceTable;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;

class CheckWebOrdersCommand extends ContainerAwareCommand
{


    /**
     * Configures the current command.
     */
    protected function configure()
    {

        $this
            ->setName('restaurant:check:weborder')
            ->setDescription('Export data by report')
            ->addOption('report', '-r', InputOption::VALUE_REQUIRED, 'Export report');
    }

    private function getApiOrders($shop)
    {
        $url = $this->getContainer()->getParameter("api_path") . $this->getContainer()->get('router')->generate('api_get_web_orders', ['id' => $shop]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);

        $data = json_decode($response, true);

        return $data;
    }

    private function apiSetPrinted($id)
    {
        var_dump($id . " printed");
        $url = $this->getContainer()->getParameter("api_path") . $this->getContainer()->get('router')->generate('api_web_order_printed', ['id' => $id]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);

        $data = json_decode($response);

        return $data;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $webOrders = [];

        try {
            $shops = array_unique(array_merge([$this->getContainer()->getParameter('shop.id')], $this->getContainer()->getParameter('relative_shops')));
            foreach ($shops as $__shopId) {
                $webOrders = array_merge($webOrders, $this->getApiOrders($__shopId));
            }

            foreach ($webOrders as $__webOrder) {
                $order = new Order($em->getReference(User::class, 1), $em->getReference(Shop::class, $__webOrder["shop"]));

                $order->setCashType($__webOrder["cashType"]);
                $order->setPlace($em->getReference(ShopPlace::class, 0));
                $order->setTable($em->getReference(ShopPlaceTable::class, 0));

                $lastOrder = $doctrine->getRepository("OrderBundle:Order")->findLastInShop($order->getShop());
                $order->setLocalId($lastOrder === null ? 1 : ($lastOrder->getLocalId() + 1));

                if (array_key_exists("promo_id", $__webOrder) && $__webOrder["promo_id"] != null) {
                    $order->setPromo($em->getReference(PromoCard::class, $__webOrder["promo_id"]));
                }

                $orderMeta = new OrderMeta($order);
                $order->setMeta($orderMeta);

                $orderMeta->setPhone($__webOrder["phone"]);
                $orderMeta->setTime($__webOrder["pickupTime"]);
                $orderMeta->setComment($__webOrder["phone"] . " | " . $__webOrder["address"] . " | " . $__webOrder["comment"]);
                $orderMeta->setWebOrder($__webOrder["id"]);
                $orderMeta->setOrderType($__webOrder["type"]);

                $order->setStatus(Order::STATUS_WAIT_FOR_CONFIRM);
                $order->setIsEditable(true);

                foreach ($__webOrder["dishes"] as $__dish) {
                    /** @var Dish $dish */
                    $dish = $em->getRepository('DishBundle:Dish')->find($__dish["id"]);
                    $orderDish = new OrderDish($order);
                    $orderDish->setDish($dish);
                    $orderDish->setQuantity($__dish["quantity"]);

                    $orderDish->setDishPrice($dish->getShopPrice($__webOrder["shop"]) * (1 - ((int)$__webOrder["discount"] / 100)));
                    $orderDish->setDishDiscount($dish->getShopPrice($__webOrder["shop"]) - $orderDish->getDishPrice());
                    $orderDish->setParentOrder($order);
                    $order->addDish($orderDish);
                }

                $printer = $this->getContainer()->get('terminal.print.storage');

//                $printer->printAdminWebOrder($order, $__webOrder, 1);
//                $printer->webOrderKitchensPrint($order, $__webOrder);
                $this->apiSetPrinted($__webOrder["id"]);
                $em->persist($orderMeta);
                $em->persist($order);
                $em->flush();
            }


        } catch (\Exception $em) {
            dump($em);
            $output->write('failed');
        }
    }
}
