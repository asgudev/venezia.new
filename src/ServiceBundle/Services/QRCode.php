<?php

namespace ServiceBundle\Services;

use chillerlan\QRCode\QRCode as BaseQrCode;
use chillerlan\QRCode\QROptions;
use Symfony\Component\DependencyInjection\Container;
use OrderBundle\Entity\Order;

class QRCode
{

    private $container;
    private $qr;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->qr = new QROptions([
            'version' => 4,
            'scale' => 11,
            'outputType' => \chillerlan\QRCode\QRCode::OUTPUT_IMAGE_PNG,
            'eccLevel' => \chillerlan\QRCode\QRCode::ECC_L,
        ]);


    }

    public function renderImage(Order $order)
    {
        $code = $order->getShop()->getId() . "_" . $order->getLocalId() . "_" . $order->getShop()->getId() . "_" . $order->getLocalId();

        $qrcode = new BaseQrCode($this->qr);
        $qrcode->render("https://venezia.by/promo/" . md5($code), 'qr.png');
    }
}