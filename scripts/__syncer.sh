#!/usr/bin/env bash

SLAVE_NAME=$3
SLAVE_IP=$1
SLAVE_PASS=$2
BACKUP_DIR=$(date '+%Y-%m-%d')


FULL_BACKUP_FILE="$BACKUP_DIR/backup_$3_slave.sql"
ORDER_BACKUP_FILE="$BACKUP_DIR/$3_slave.sql"

mysqldump -h $SLAVE_IP -p$SLAVE_PASS -P 3306 -ubackup venezia_new orders order_dish order_changes daily_report ingredient_data delegate_order order_dish_photo  > $ORDER_BACKUP_FILE --replace --no-create-info --set-gtid-purged=OFF --lock-tables=false --skip-triggers --quick &
mysqldump -h $SLAVE_IP -p$SLAVE_PASS -P 3306 -ubackup venezia_new > $FULL_BACKUP_FILE --set-gtid-purged=OFF  &
wait

mysql -h $SLAVE_IP -p$SLAVE_PASS -P 3306 -ubackup -e "truncate venezia_new.category_discount; truncate venezia_new.dish_meta;truncate venezia_new.shop_dish;truncate venezia_new.dish_history;" &
wait

mysql -h $SLAVE_IP -ubackup -p$SLAVE_PASS -P 3306 venezia_new < $BACKUP_DIR/data_master.sql &
mysql -uvenezia -p9WhsFYfbQjdQz7uR venezia_new < $ORDER_BACKUP_FILE &
wait

echo "FIN $3"

