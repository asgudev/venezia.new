<?php


namespace ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class ReportData
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {

    }

    /**
     * @ORM\ManyToOne(targetEntity="ReportBundle\Entity\Report", inversedBy="data")
     */
    private $report;

    /**
     * @ORM\Column(name="value", type="string", nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="ReportBundle\Entity\ReportElement", cascade={"persist"})
     */
    private $element;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param mixed $report
     * @return ReportData
     */
    public function setReport($report)
    {
        $this->report = $report;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return ReportData
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * @param mixed $element
     * @return ReportData
     */
    public function setElement($element)
    {
        $this->element = $element;
        return $this;
    }



}