<?php

namespace RestaurantBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{

    private function getShopStatus() {
        $ch = curl_init("http://127.0.0.1:3001/status");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch); 
        $data = json_decode($response, true);

        $status = [];


        foreach ($data as $_s) {
            $shopId = str_replace("shop-", "", $_s["name"]);
            $status[$shopId] = $_s;
        }


        return $status;
    }


    /**
     * @Route("/update/{id}", name="update")
     */
    public function updateAction(Request $request, $id)
    {

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://venezia.by:3001/command',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => json_encode(["command" => "command", "target" => "shop-".$id, "data" => ["command" => "update-asar"]]),
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
  ),
));

$response = curl_exec($curl);
        var_dump($response);die();

        return new JsonResponse($response);

    }


    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {

//die();
//        if ($this->getUser()->getIs2FA()) {
//            $twoFAPassed = $this->get("redis.helper")->get("user_" . $this->getUser()->getId() . "_2fa");
//
//            if (!$twoFAPassed) {
//                return $this->render();
//            }
//        }

//        if ($dateEnd == null) {
//            $dateEnd = (new \DateTime($dateStart))->modify('+1 day')->format('d.m.Y');
//        }
        $dateStart = $request->get("date") ?: "today";


//        $this->getDoctrine()->getRepository(Dish::class)->findB

        if ($this->isTrustedTablet($request->headers->all())) {
            return $this->redirectToRoute('terminal_index');
        }

        if ($this->getUser() == null) return $this->redirectToRoute('fos_user_security_login');

        if ($this->getUser()->hasRole("ROLE_ADMIN") || ($this->getUser()->hasRole("ROLE_SHOP_WATCHER"))) {

            $summ = 0;
            $orders = [];

            $shops = $this->getUser()->getAdminShops();

            $shopsData = $this->getDashboardData($dateStart);

            $changesData = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findTodayChangesCount();
            $changes = [];


            foreach ($changesData as $__data) {
                $changes[$__data['sID']] = $__data['ocCount'];
            }

            $newClients = $this->getDoctrine()->getRepository('UserBundle:ClientCredentials')->countNewClients();

            return $this->render("@Restaurant/Default/dashboard.html.twig", [
                'orders' => $orders,
                'summ' => $summ,
                'newClientsCounter' => $newClients,
                'status' => $this->getShopStatus(),
                //'trendData' => $graph,
                'shops' => $shops,
                'shopsData' => $shopsData,
                'changes' => $changes,
                'dateStart' => $dateStart,
//                'dateEnd' => $dateEnd,
            ]);
        }

        if ($this->getUser()->hasRole("ROLE_SEO")) {
            return $this->redirect($this->generateUrl('site_shop_list'));
        }

        if ($this->getUser()->hasRole("ROLE_PIZZA")) {
            return $this->redirect($this->generateUrl('pizza_terminal'));
        }

        if ($this->getUser()->hasRole("ROLE_USER")) {
            return $this->redirect($this->generateUrl('terminal_index'));
        }

    }

    private function isTrustedTablet($headers)
    {
        if (array_key_exists("asgumobileclient", $headers) && ($headers["asgumobileclient"][0] == true)) {
            return true;
        }

        return false;
    }

    private function getDashboardData($date)
    {
        $ch = curl_init($this->getParameter("api_path") . $this->generateUrl('api_dashboard_data', [
                'dateStart' => $date,
            ]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        return json_decode($response, true);
    }

    /**
     * @Route("/income_data", name="income_data_ajax")
     */
    public function getOrdersDataAction(Request $request)
    {
        $trendData = $this->getDoctrine()->getRepository('OrderBundle:Order')->findDataForDashboard($request->get('deep'));

        $graph = [];
        foreach ($trendData as $data) {
            $graph[$data['orderCreatedAtDate']] = round($data['orderSum'], 2);
        }

        return new JsonResponse($graph);
    }

    /**
     * @Route("/orders", name="orders")
     */
    public function ordersAction()
    {
        return $this->render('RestaurantBundle:Default:index.html.twig');
    }


}
