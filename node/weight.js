const YAML = require('yamljs'),
    express = require("express");

let app = express();
let lastWeight = 0;
let sent = false;

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

app.get('/', function (req, res) {
    res.send(lastWeight.toString());
});

let config = YAML.load('../app/config/parameters.yml');
let device_port = config.parameters.device_port;

if (device_port.match(/dev/)) {
    const SerialPort = require('serialport');
    const ByteLength = require('@serialport/parser-byte-length')

    let port = new SerialPort(device_port, {
        baudRate: 4800,
        dataBits: 8,
        stopBits: 1,
        parity: 'even',
    });
    const parser = port.pipe(new ByteLength({length: 2}))

    setInterval(function () {
        port.write(new Buffer([0x45]), function (err) {
            sent = true;
        });
    }, 1000);

    port.on('data', function (response) {
        sent = false;
        lastWeight = parseHex(response);
    });


    function parseHex(data) {
        let s = data.toString('hex');
        s = s.replace(/^(.(..)*)$/, "0$1");
        let a = s.match(/../g);
        a.reverse();
        let v2 = parseInt(a.join(""), 16);
        return v2/10;
    }
} else {
    const net = require("net");

    let client = new net.Socket();
    client.connect(5001, device_port, function () {
        setInterval(() => {
            let hexVal = new Uint8Array([0xF8, 0x55, 0xCE, 0x01, 0x00, 0x23, 0x23, 0x00]);
            client.write(hexVal);
        }, 1000);
    });

    client.on('data', function (data) {
        lastWeight = parseInt("0x" + data.toString('hex').substring(12, 20).match(/.{2}/g).reverse().join(""));
    });

    client.on('close', function () {
        console.log('Connection closed');
    });
}
