<?php


namespace DishBundle\Entity\Repository;


use DishBundle\Entity\CategoryDiscount;
use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\Shop;

class CategoryDiscountRepository extends EntityRepository
{

    public function findActiveNForNNow(Shop $shop)
    {
        $qb = $this->createQueryBuilder('cd')
            ->where('cd.shop = :shop')
            ->andWhere('cd.type = :type')
        ;

        $qb->setParameters([
            'shop' => $shop,
            'type' => 'nforn'
        ]);

        return $qb->getQuery()->getResult();
    }
}