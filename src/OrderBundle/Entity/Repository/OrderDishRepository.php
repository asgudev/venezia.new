<?php

namespace OrderBundle\Entity\Repository;

use TerminalBundle\Entity\DailyReport;
use Doctrine\ORM\EntityRepository;

use DishBundle\Entity\Dish;
use RestaurantBundle\Entity\Shop;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use function Doctrine\ORM\QueryBuilder;

class OrderDishRepository extends EntityRepository
{

    public function findReport2($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('di')
            ->addSelect('o')
            ->addSelect('s')
            ->leftJoin("od.order", "o")
            ->leftJoin("od.dish", 'd')
            ->leftJoin("d.category", 'dc')
            ->leftJoin('d.ingredients', 'di')
            ->leftJoin('o.shop', 's')
            ->andWhere('o.status < 5')
            ->andWhere('o.client is null')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('o.shop = :shop')
            ->andWhere("o.clientMoneyCash = 0")
            ->andWhere("o.clientMoneyCard = 0")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        return $dishes;
    }

    /**
     * @param $shop
     * @param $dateStart
     * @param $dateEnd
     * @return OrderDish[]
     */
    public function findPizzaByPeriod($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('di')
            ->addSelect('o')
            ->addSelect('s')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin('d.ingredients', 'di')
            ->leftJoin('o.shop', 's')
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('o.shop = :shop')
//            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 11177')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        return $dishes;
    }

    public function findDishSum($shop, $dateStart)
    {
        return $this->createQueryBuilder('od')
            ->select('SUM((od.dishPrice) * od.quantity) as dishSum')
            ->where('o.shop = :shop')
            ->andWhere('o.status = 4')
            ->andWhere('o.cashType = 0')
            ->andWhere('o.createdAt >= :dateStart')
            ->leftJoin('od.order', 'o')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $dateStart,
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function findDailyPizza(Shop $shop)
    {
        $data = $this->createQueryBuilder('od')
            ->select('count(od) AS y')
            ->addSelect('DATE(o.createdAt) AS x')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'd')
            ->where('d.name LIKE :pizza')
            ->andWhere('o.shop = :shop')
            ->setParameters([
                'shop' => $shop,
                'pizza' => "%Пицца%"
            ])
            ->addGroupBy('x')
            ->addOrderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getResult();

        return $data;
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     */
    public function findPizzaMonthlyPerDateRange($dateStart, $dateEnd)
    {
        $qb = $this->createQueryBuilder('od');
        $dishes = $this->_em->createQueryBuilder();
        $dishes
            ->select('_d.id')
            ->from("DishBundle:Dish", '_d')
            ->where('d.category IN (32,82)');

        $orders = $this->_em->createQueryBuilder();
        $orders
            ->select('_o.id')
            ->from('OrderBundle:Order', "_o")
            ->where('_o.createdAt >= :dateStart')
            ->andWhere('_o.createdAt <= :dateEnd');

        $pizza = $qb
            ->select("SUM(od.quantity) as dSum")
            ->addSelect('MONTH(o.createdAt) as dMonth')
            ->addSelect('s.id as dShop')
            ->innerJoin('od.parentOrder', "o")
            ->innerJoin('od.dish', "d", "WITH")
            ->leftJoin('o.shop', 's')
            ->where($qb->expr()->in("od.parentOrder", $orders->getDQL()))
            ->andWhere($qb->expr()->in('od.dish', $dishes->getDQL()))
            ->groupBy('dMonth')
            ->addGroupBy('s.id');

        $pizza->setParameters([
            "dateStart" => $dateStart,
            "dateEnd" => $dateEnd,
        ]);

        return $pizza->getQuery()->getResult();
    }



    /**
     * @param $dateStart
     * @param $dateEnd
     * @return OrderDish[]
     * @throws \Exception
     */
    public function findPizzaCountPerDateRange($dateStart, $dateEnd)
    {
        $data = $this->createQueryBuilder('od')
            ->select("COUNT(od.id) as dishCnt")
            ->addSelect('s.name as shopName')
            ->where('o.createdAt >= :dateStart')
            ->andWhere('o.createdAt <= :dateEnd')
            ->andWhere('d.category IN (32,82)')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('o.shop', 's')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'dateStart' => new \DateTime($dateStart),
                'dateEnd' => new \DateTime($dateEnd),
            ])
            ->addGroupBy('s.id')
            ->getQuery()
            ->getResult();

        return $data;
    }



    /**
     * @param $dateStart
     * @param $dateEnd
     * @return OrderDish[]
     * @throws \Exception
     */
    public function findPizzaPerDateRange($dateStart, $dateEnd)
    {
        $data = $this->createQueryBuilder('od')
//            ->select('od')
//            ->select('sum(od.quantity) AS odCount')
//            ->addSelect('DATE(o.createdAt) AS date')
//            ->addSelect('s.name AS shop')
            ->addSelect('di')
            ->addSelect('d')
            ->addSelect('o')
            ->addSelect('s')
            ->where('o.createdAt >= :dateStart')
            ->andWhere('o.createdAt <= :dateEnd')
            ->andWhere('d.category IN (32,82)')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('o.shop', 's')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'dateStart' => new \DateTime($dateStart),
                'dateEnd' => new \DateTime($dateEnd),
            ])
//            ->addGroupBy('date')
//            ->addGroupBy('o.shop')
            ->addOrderBy('s.id', "ASC")
            ->addOrderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getResult();

        return $data;
    }

    public function findDailyDish(Shop $shop, Dish $dish)
    {
        $data = $this->createQueryBuilder('od')
            ->select('count(od) AS y')
            ->addSelect('DATE(o.createdAt) AS x')
            ->where('od.dish = :dish')
            ->andWhere('o.shop = :shop')
            // ->andWhere('o.createdAt >= :date')
            ->leftJoin('od.order', 'o')
            ->setParameters([
                'dish' => $dish,
                'shop' => $shop,
                // 'date' => (new \DateTime("2017-09-01"))
            ])
            ->addGroupBy('x')
            ->addOrderBy('o.createdAt', 'ASC')
            ->getQuery()
            ->getResult();

        return $data;
    }


    public function findByShopInDates($shop, $category = null, $dateStart = null, $dateEnd = null)
    {

        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->addSelect("od.dishPrice as odDishPrice")
            ->addSelect("SUM(od.dishPrice * od.quantity) as odDishSum")
            ->addSelect("SUM(od.dishDiscount * od.quantity) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin("d.dishMetas", 'dm', 'WITH', 'dm.shop = :shop')
            ->where("o.shop = :shop")
            ->andWhere('od.quantity > 0')
            ->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $shop
            ]);

        if ($category) {
            $dishes->andWhere("d.category = :category")->setParameter("category", $category);
        }

        if ($dateStart) {
            $dateStart = new \DateTime($dateStart);
            $dishes->andWhere("o.createdAt > :dateStart")->setParameter("dateStart", $dateStart);
        }
        if ($dateEnd) {
            $dateEnd = new \DateTime($dateEnd);
            if ($dateStart == $dateEnd) {
                $dateEnd = $dateEnd->modify("+1 day");
            }
            $dishes->andWhere("o.createdAt < :dateEnd")->setParameter("dateEnd", $dateEnd);
        }
//dump($dishes->getQuery());die();

        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    public function findDeletionByDate($shop, $category = null, $dateStart = null, $dateEnd = null)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('o')
            ->addSelect('d')
            ->where('od.order IS NULL')
            ->andWhere('od.parentOrder = :shop')
            ->andWhere("o.createdAt > :dateStart")
            ->andWhere("o.createdAt < :dateEnd")
            ->innerJoin("od.parentOrder", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => (new \DateTime("today"))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime("today"))->setTime(23, 59, 59),
            ]);

        /*
        $dishes = $dishes->getQuery();
        dump($dishes); die();
        */

        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    public function findRawPizzaByReport(DailyReport $report)
    {
        return $this->createQueryBuilder('od')
            ->addSelect('o')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 11177')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.order', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->orderBy('d.name')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param DailyReport $report
     * @param bool $type
     * @return OrderDish[]
     */
    public function findPizzaByReport(DailyReport $report, $type = true)
    {
        $pizzaDishes = $this->createQueryBuilder('od');

        $pizzaDishes
            ->addSelect('o')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('o.status < 5')
            ->andWhere($pizzaDishes->expr()->orX(
                $pizzaDishes->expr()->andX(
                    $pizzaDishes->expr()->eq("o.shop", ":shop"),
                    $pizzaDishes->expr()->isNull("od.delegate")
                ),
                $pizzaDishes->expr()->eq("od.delegate", $report->getShop()->getId())
            ))
            ->addSelect('d')
            ->addSelect('di')
            ->leftJoin('od.dish', 'd')
            ->innerJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->orderBy('d.name');

        if ($type) {
            $pizzaDishes->leftJoin('od.parentOrder', 'o');
        } else {
            $pizzaDishes->leftJoin('od.order', 'o');
        }

        return $pizzaDishes->getQuery()->getResult();
    }


    public function findPizzaFromDate($shop, $date)
    {
        return $this->createQueryBuilder('od')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt > :dateStart ')
            ->andWhere('d.category IN (32,82)')
            ->andWhere('d.id <> 11177')
            ->andWhere('o.status <= 4')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.parentOrder', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $date,
            ])
            ->orderBy('o.id')
            ->getQuery()
            ->getResult();
    }

    public function findPizzaSumFromDate($shop, \DateTime $date)
    {
        return $this->createQueryBuilder('od')
            ->addSelect('SUM(di.quantity * od.quantity) as pizzaSum')
            ->addSelect('dii.name as diiName')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt > :dateStart')
            ->andWhere('d.category IN (32,82)')
            ->andWhere('d.id <> 11177')
            ->andWhere('o.status <= 4')
            ->andWhere('d.name NOT LIKE :str1')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.order', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->leftJoin('di.ingredient', 'dii')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $date,
                'str1' => '%добавка%',
            ])
            ->groupBy('dii.id')
            ->addGroupBy('d.category')
            ->orderBy('dii.id')
            ->getQuery()
            ->getResult();
    }

    public function findPizzaByDate($shop, $date = 'today')
    {
        return $this->createQueryBuilder('od')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('d.category = 32')
            ->andWhere('d.id <> 11177')
            ->andWhere('o.status <= 4')
            ->addSelect('d')
            ->addSelect('di')
            ->innerJoin('od.parentOrder', 'o')
            ->innerJoin('od.dish', 'd')
            ->leftJoin('d.ingredients', 'di')
            ->setParameters([
                'shop' => $shop,
                'dateStart' => (new \DateTime($date))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime($date))->setTime(23, 59, 59),
            ])
            ->orderBy('d.name')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findAllDishesFromDate($shop, $date)
    {
        $dishes = $this->createQueryBuilder('o')
            ->addSelect('od')
            ->addSelect('odc')
            ->addSelect('odcp')
            ->addSelect('odm')
            ->where('op.createdAt > :dateStart')
            ->andWhere('op.status <= 4')
            ->andWhere('op.shop = :shop')
//            ->andWhere('op.cashType = 0')
            ->innerJoin('o.parentOrder', 'op')
            ->innerJoin('o.dish', 'od')
            ->innerJoin('od.category', 'odc')
            ->innerJoin('odc.parent', 'odcp')
            ->leftJoin('od.dishMetas', 'odm', 'WITH', 'odm.shop = :shop')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $date,
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findAllDishesByDate($shop, $date = 'yesterday')
    {
        $dishes = $this->createQueryBuilder('o')
            ->addSelect('od')
            ->addSelect('odc')
            ->addSelect('odcp')
            ->addSelect('odm')
            ->where('op.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('op.status <= 4')
            ->andWhere('op.shop = :shop')
//            ->andWhere('op.cashType = 0')
            ->innerJoin('o.parentOrder', 'op')
            ->innerJoin('o.dish', 'od')
            ->innerJoin('od.category', 'odc')
            ->innerJoin('odc.parent', 'odcp')
            ->leftJoin('od.dishMetas', 'odm', 'WITH', 'odm.shop = :shop')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => (new \DateTime($date))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime($date))->setTime(23, 59, 59),
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findDishesByReport(DailyReport $dailyReport)
    {
        $dishes = $this->createQueryBuilder('o')
            ->addSelect('oo')
            ->addSelect('od')
            ->addSelect('odc')
            ->addSelect('odcp')
            ->addSelect('odm')
            ->addSelect('di')
            ->where('oo.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('oo.shop = :shop')
            //->andWhere('oo.localId > 0')
            ->andWhere('oo.status = :status')
            ->andWhere('od.isExportable = true')
            ->innerJoin('o.order', 'oo')
            ->innerJoin('o.dish', 'od')
            ->innerJoin('od.category', 'odc')
            ->innerJoin('odc.parent', 'odcp')
            ->leftJoin('od.dishMetas', 'odm')
            ->leftJoin('od.ingredients', 'di')
            ->setParameters([
                'shop' => $dailyReport->getShop(),
                'dateStart' => $dailyReport->getDateStarted(),
                'dateEnd' => $dailyReport->getDateClosed(),
                'status' => Order::STATUS_CLOSED,
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findGroupedDishesByReport(DailyReport $dailyReport)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect("SUM(od.dishPrice) as odDishPrice")
            ->addSelect("SUM(od.dishDiscount) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("o.cashType as oCashType")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->groupBy("od.dish")
            ->addGroupBy('o.cashType')
            ->setParameters([
                "shop" => $dailyReport->getShop(),
                'dateStart' => $dailyReport->getDateStarted(),
                'dateEnd' => $dailyReport->getDateClosed(),
            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findGroupedDishesByPeriod($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect("od.dishPrice as odDishPrice")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("SUM(od.dishPrice * od.quantity) as odCashSum")
            ->addSelect("SUM(od.dishDiscount * od.quantity) as odDiscountSum")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->leftJoin('o.client', 'c', 'WITH', 'c.isExportable = true')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->andWhere("o.status = 4")
            ->groupBy("od.dish")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,

            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findClientTableDishesByPeriod($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect('t.name as table')
            ->addSelect("od.dishPrice as odDishPrice")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("SUM(od.dishPrice * od.quantity) as odCashSum")
            ->addSelect("SUM(od.dishDiscount * od.quantity) as odDiscountSum")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->leftJoin('o.table', 't')
            //->leftJoin('o.client', 'c', 'WITH', 'c.isExportable = true')

            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->andWhere('t.isClient = true')
            ->groupBy('t')
            ->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,

            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    public function findSummedByDish($shop, $category = null, $dateStart = null, $dateEnd = null)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect('dm')
            ->addSelect("SUM(od.dishPrice) as odDishPrice")
            ->addSelect("SUM(od.dishDiscount) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("o.cashType as oCashType")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.dishMetas', 'dm', 'WITH', 'dm.shop = :shop')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->groupBy("od.dish")
            ->addGroupBy('o.cashType')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => (new \DateTime("yesterday"))->setTime(0, 0, 0),
                'dateEnd' => (new \DateTime("today"))->setTime(0, 0, 0),
            ]);

        /*
        $dishes = $dishes->getQuery();
        dump($dishes); die();
        */

        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    public function findLastOrders($dish)
    {
        $last = $this->createQueryBuilder('o')
            ->where('o.dish = :dish')
            ->addSelect('oo')
            ->addSelect('oos')
            ->innerJoin('o.order', 'oo')
            ->innerJoin('oo.shop', 'oos')
            ->setParameters([
                'dish' => $dish,
            ])
            ->orderBy('oo.createdAt', "DESC")
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();

        return $last;
    }

    public function findGroupedByReport(DailyReport $dailyReport)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->addSelect('o')
            //->addSelect("od.dishPrice as odDishPrice")
            //->addSelect("SUM(od.dishPrice * od.quantity) as odDishSum")
            //->addSelect("SUM(od.dishDiscount * od.quantity) as odDishDiscount")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("dc.name as dcName")
            ->leftJoin("od.order", "o")
            ->leftJoin("od.dish", 'd')
            ->leftJoin("d.category", 'dc')
            ->leftJoin("d.dishMetas", 'dm')
            ->where("o.shop = :shop")
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->groupBy("d")
            ->setParameters([
                "shop" => $dailyReport->getShop(),
                'dateStart' => $dailyReport->getDateStarted(),
                'dateEnd' => $dailyReport->getDateClosed(),
            ]);


        $dishes = $dishes->getQuery()->getResult();

        dump($dishes);
        die();

        return $dishes;
    }

    public function findAllDeletedDishes(Shop $shop)
    {
        $dishes = $this->createQueryBuilder('od')
            ->where('od.order IS NULL')
            ->andWhere('odp.shop = :shop')
            ->innerJoin('od.parentOrder', 'odp')
            ->setParameters([
                'shop' => $shop
            ]);


        return $dishes->getQuery()->getResult();
    }

    /**
     * @param Shop $shop
     * @return OrderDish[]
     */
    public function findAllDeletedParentDishes(Shop $shop)
    {
        $dishes = $this->createQueryBuilder('od')
            ->where('od.order IS NULL')
            ->andWhere('odp.shop = :shop')
            ->andWhere('od.parentDish is null')
            ->innerJoin('od.parentOrder', 'odp')
            ->setParameters([
                'shop' => $shop
            ]);


        return $dishes->getQuery()->getResult();
    }

    /**
     * @param Shop $shop
     * @return OrderDish[]
     */
    public function findAllDeletedChildDishes(Shop $shop)
    {
        $dishes = $this->createQueryBuilder('od')
            ->where('od.order IS NULL')
            ->andWhere('odp.shop = :shop')
            ->andWhere('od.parentDish is not null')
            ->innerJoin('od.parentOrder', 'odp')
            ->setParameters([
                'shop' => $shop
            ]);


        return $dishes->getQuery()->getResult();
    }


    public function findSumByReport(DailyReport $report)
    {
        return $this->createQueryBuilder('o')
            ->select('SUM(o.dishPrice * o.quantity) as odSum')
            ->where('oo.shop = :shop')
            ->andWhere('oo.status <= 4')
            ->andWhere('oo.createdAt BETWEEN :dateStart AND :dateEnd')
            ->innerJoin('o.order', 'oo')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param $shop
     * @param string $date
     */
    public function findIngredientsByPeriod($shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('dii.name as diName')
            ->addSelect('di')
            ->addSelect('d')
            ->addSelect('SUM(od.quantity * di.quantity) as diQuantity')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'd')
            ->innerJoin('d.ingredients', 'di')
            ->leftJoin('di.ingredient', 'dii')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->groupBy('dii')
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findDelegatedDishesByReport(DailyReport $report)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin("d.dishMetas", 'dm')
            ->where("o.shop = :shop")
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('od.delegate is not null')
            //->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ]);


        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findGroupedDelegatedDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as delegate')
            ->addSelect('os.name as odDelegate')
            ->addSelect('d.name as dName')
            ->addSelect('od.dishPrice  as odDishPrice')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->addSelect('SUM(od.dishDiscount * od.quantity) as odDiscountSum')
            ->addSelect('SUM(od.quantity) as odDishQuantity')
            ->innerJoin('od.order', 'o')
            ->innerJoin('o.shop', 'os')
            ->innerJoin('od.delegate', 'odd')
            ->innerJoin('od.dish', 'd')
            ->where('o.createdAt BETWEEN :dateStart AND :dateEnd')
            //->andWhere('o.shop = :shop')
            ->andWhere('od.delegate = :shop')
            ->andWhere('o.status = 4')
            ->addGroupBy('d.id')
            ->addGroupBy('os.id')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        //dump($dishes);die();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findGroupedRequestsDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as delegate')
            ->addSelect('odd.name as odDelegate')
            ->addSelect('d.name as dName')
            ->addSelect('od.dishPrice  as odDishPrice')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->addSelect('SUM(od.dishDiscount * od.quantity) as odDiscountSum')
            ->addSelect('SUM(od.quantity) as odDishQuantity')
            ->innerJoin('od.order', 'o')
            ->innerJoin('o.shop', 'os')
            ->innerJoin('od.delegate', 'odd')
            ->innerJoin('od.dish', 'd')
            ->where('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('o.shop = :shop')
            ->andWhere('od.delegate is not null')
            ->andWhere('o.status = 4')
            ->addGroupBy('d')
            ->addGroupBy('odDelegate')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        //dump($dishes);die();

        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findDelegatedDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('dm')
            ->addSelect('di')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->innerJoin("d.dishMetas", 'dm')
            ->leftJoin('d.ingredients', 'di')
            ->where("o.shop = :shop")
            ->andWhere('o.status < 5')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('od.delegate is not null')
            ->addGroupBy("od.dish")
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        return $dishes;
    }


    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findDelegatedDishesByPeriodToShop(Shop $shop, $dateStart, $dateEnd)
    {
/*        $dishes = $this->createQueryBuilder('od')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('di')
            ->addSelect('o')
            ->addSelect('s')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.ingredients', 'di')
            ->leftJoin('o.shop', 's')
            ->andWhere('od.delegate = :shop')
            ->andWhere($dishes->expr()->in('o.id', $orders->getDQL()))
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);*/

$orders = $this->_em->createQueryBuilder()
            ->select("o2.id")
            ->from("OrderBundle:Order", "o2")
            ->andWhere("o2.status <= 4")
            ->andWhere('o2.createdAt BETWEEN :dateStart AND :dateEnd')
        ;

        $dishes = $this->createQueryBuilder('od');

        $dishes->addSelect('d')
            ->addSelect('dc')
            ->addSelect('di')
            ->addSelect('o')
            ->addSelect('s')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('d.ingredients', 'di')
            ->leftJoin('o.shop', 's')
            ->andWhere('od.delegate = :shop')
            ->andWhere($dishes->expr()->in('o.id', $orders->getDQL()))
     ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ;


        $dishes = $dishes->getQuery()->getResult();
        return $dishes;
    }

    /**
     * @param $shop
     * @param string $date
     * @return OrderDish[]
     */
    public function findRequestedDishesByReport(DailyReport $report)
    {
        $dishes = $this->createQueryBuilder('od');

        $dishes->addSelect('d')
            ->addSelect('dc')
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd', "WITH", $dishes->expr()->eq("d.isExportable", true))
            ->innerJoin("d.category", 'dc')
            ->where("o.status <= :status")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->andWhere(
            //   $dishes->expr()->orX(
                $dishes->expr()->andX(
                    $dishes->expr()->eq("o.shop", $report->getShop()->getId()),
                    $dishes->expr()->neq("od.delegate", $report->getShop()->getId())
                )/*,
                    $dishes->expr()->andX(
                        $dishes->expr()->eq("o.shop", 14),
                        $dishes->expr()->eq("od.delegate", $report->getShop()->getId())
                    )

                )*/
            )

            //->addGroupBy("od.dish")
            ->setParameters([
                "status" => Order::STATUS_CLOSED,
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ]);


        $dishes = $dishes->getQuery()->getResult();

        return $dishes;
    }


    /**
     * @param $shop
     * @return OrderDish[]
     */
    public function findGroupedWriteoffDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as delegate')
            ->addSelect('d.name as dName')
            ->addSelect('d')
            ->addSelect('dc.name as dcName')
            ->addSelect('od.dishPrice  as odDishPrice')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->addSelect('SUM(od.dishDiscount * od.quantity) as odDiscountSum')
            ->addSelect('SUM(od.quantity) as odDishQuantity')
            ->addSelect('c.name as odWriteoff')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.client', 'c')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('d.category', 'dc')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('c.unp in (:unp)')
            ->addGroupBy('d')
            ->addGroupBy('c')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
                'unp' => ['44', '94', 'акция'],
            ]);


        $dishes = $dishes->getQuery()->getResult();
        //dump($dishes);die();

        return $dishes;
    }

    /**
     * @param $shop
     * @return OrderDish[]
     */
    public function findGroupedWriteoffBarDishesByPeriod(Shop $shop, $dateStart, $dateEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as delegate')
            ->addSelect('d.name as dName')
            ->addSelect('d')
            ->addSelect('dc.name as dcName')
            ->addSelect('od.dishPrice  as odDishPrice')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->addSelect('SUM(od.dishDiscount * od.quantity) as odDiscountSum')
            ->addSelect('SUM(od.quantity) as odDishQuantity')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.client', 'c')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('d.category', 'dc')
            ->where('o.shop = :shop')
            ->andWhere('o.createdAt BETWEEN :dateStart AND :dateEnd')
            ->andWhere('c.isExportable = false')
            ->addGroupBy('d')
            ->setParameters([
                "shop" => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ]);


        $dishes = $dishes->getQuery()->getResult();
        //dump($dishes);die();

        return $dishes;
    }

    public function findMarcoReportCashData(Shop $shop, $category = null, $parentCategory = null)
    {

        $orderData = $this->createQueryBuilder('od')
            ->addSelect('DATE(o.createdAt) AS oCreatedAtDate')
            ->addSelect('SUM(od.dishPrice * od.quantity) as odCashSum')
            ->leftJoin('od.order', 'o')
            ->where('o.shop = :shop')
            ->andWhere('o.status = 4')
            ->setParameters([
                "shop" => $shop,
            ])
            ->groupBy('oCreatedAtDate');

        if ($category != null) {
            $orderData
                ->leftJoin('od.dish', 'd')
                ->leftJoin('d.category', 'c')
                ->andWhere('c.id = :id')
                ->setParameter('id', $category);
        }
        if ($parentCategory != null) {
            $orderData
                ->leftJoin('od.dish', 'd')
                ->leftJoin('d.category', 'c')
                ->leftJoin('c.parent', 'cp')
                ->andWhere('cp.id = :id')
                ->setParameter('id', $parentCategory);
        }


        return $orderData->getQuery()->getResult();

    }

    /**
     * @param $shop
     * @param string $date
     */
    public function findMarcoReportIngrData($shop)
    {
        return $this->createQueryBuilder('od')
            ->addSelect('DATE(o.createdAt) AS oCreatedAtDate')
            ->addSelect('SUM(od.quantity * di.quantity) as diQuantity')
            ->addSelect('dii.name as dName')
            ->leftJoin('od.parentOrder', 'o')
            ->leftJoin('od.dish', 'd')
            ->innerJoin('d.ingredients', 'di')
            ->leftJoin('di.ingredient', 'dii')
            ->where('o.shop = :shop')
            ->andWhere('o.status = 4')
            ->andWhere('od.order is not null')
            ->setParameters([
                "shop" => $shop,
            ])
            ->groupBy('oCreatedAtDate')
            ->addGroupBy('dii')
            ->getQuery()
            ->getResult();
    }


    public function findFilteredData($shop, $dateStart, $dateEnd, $periodStart, $periodEnd)
    {
        $dishes = $this->createQueryBuilder('od')
            ->select('od as root')
            ->addSelect('d')
            ->addSelect('dc')
            ->addSelect('o')
            ->addSelect("od.dishPrice as odDishPrice")
            ->addSelect("SUM(od.quantity) as odDishQuantity")
            ->addSelect("SUM(od.dishPrice * od.quantity) as odCashSum")
            ->addSelect("SUM(od.dishDiscount * od.quantity) as odDiscountSum")
            ->innerJoin("od.order", "o")
            ->innerJoin("od.dish", 'd')
            ->innerJoin("d.category", 'dc')
            ->leftJoin('o.client', 'c', 'WITH', 'c.isExportable = true')
            ->where("o.shop = :shop")
            ->andWhere("o.createdAt BETWEEN :dateStart AND :dateEnd")
            ->andWhere('o.status = 4')
            ->andWhere("TIME(o.createdAt) BETWEEN :periodStart AND :periodEnd")
            ->groupBy("od.dish")
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
                'periodStart' => $periodStart,
                'periodEnd' => $periodEnd,

            ])
            ->getQuery()
            ->getResult();

        return $dishes;
    }


    public function findSeparatedData($users, DailyReport $report)
    {

        return $this->createQueryBuilder('o')
            ->select('SUM(o.dishPrice * o.quantity) as odSum')
            ->where('oo.shop = :shop')
            ->andWhere('oo.status = 4')
            ->andWhere('oo.createdAt BETWEEN :dateStart AND :dateEnd')
            ->innerJoin('o.order', 'oo')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ])
            ->getQuery()
            ->getSingleResult();
    }


    public function findPizzaThisShift($shop, $reportStart)
    {
        $dishes = $this->_em->createQueryBuilder()
            ->select("IDENTITY(di.dish)")
            ->from("DishBundle:DishIngredient", "di")
            ->where("di.ingredient IN (1,2)");

        $orders = $this->_em->createQueryBuilder()
            ->select("o.id")
            ->from("OrderBundle:Order", "o")
            ->where("o.createdAt >= :dateStart")
            ->andWhere('o.shop = :shop')
            ->andWhere("o.status <= 4");

        $pizza = $this->createQueryBuilder('od');
        $pizza
            ->addSelect('o2')
            ->addSelect('d2')
            ->leftJoin('od.order', 'o2')
            ->leftJoin('od.dish', 'd2')
            ->where($pizza->expr()->in("IDENTITY(od.order)", $orders->getDQL()))
            ->andWhere($pizza->expr()->in("od.dish", $dishes->getDQL()))
            ->setParameters([
                'shop' => $shop,
                'dateStart' => $reportStart
            ]);

//dump($pizza->getDQL());die();
        return $pizza
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $date
     * @return OrderDish[]
     */
    public function findMenubyDishes(\DateTime $date)
    {
        $orders = $this->createQueryBuilder('od')
            ->where('s.id = 14')
            ->andWhere('DATE(o.createdAt) = :date')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.shop', 's')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('d.category', 'c')
            ->setParameters([
                'date' => $date->format('Y-m-d'),
            ]);

        return $orders->getQuery()->getResult();
    }

}
