<?php

namespace WifiBundle\Services;

use WifiBundle\Entity\WifiClient;

class SmsService
{


    public function __construct()
    {

    }

    public function sendMessage(WifiClient $client, $message)
    {
        if ($client->getPhone() != null) {

            $this->sendSms([
                'recipients' => $client->getPhone(),
                'message' => $message,
                //'urgent' => 1,
                'sender' => 'venezia.by'
            ]);

        }
        return;
    }

    public function sendCode(WifiClient $client)
    {
        $this->sendSms([
            'recipients' => $client->getPhone(),
            'message' => $client->getCode(),
            'sender' => 'venezia.by'
        ]);

        return;
    }

    private function getCurl($method = 'api/user_balance', $data = [])
    {
        $ch = curl_init();

        $url = "http://cp.smsp.by";

        $params = [
            'r' => $method,
            'user' => 'itse@tut.by',
            'apikey' => 'clr937curo',
        ];

        $params = array_merge($params, $data);


        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_FAILONERROR => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params
        ));

        //dump($ch);die();

        return $ch;
    }

    public function sendSms($data)
    {
        $ch = $this->getCurl('api/msg_send', $data);
        $res = curl_exec($ch);
        return json_decode($res);
    }

    public function getBalance()
    {
        $ch = $this->getCurl('api/user_balance');
        $res = curl_exec($ch);

        return json_decode($res);

    }
}