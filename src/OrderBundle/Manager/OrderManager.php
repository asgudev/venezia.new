<?php


namespace OrderBundle\Manager;


use DishBundle\Entity\CategoryDiscount;
use DishBundle\Entity\Dish;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderChange;
use OrderBundle\Entity\OrderDish;
use OrderBundle\Entity\OrderMeta;
use RestaurantBundle\Entity\Client;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Entity\ShopPlaceTable;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\RouterInterface;
use TerminalBundle\Promotions\NForOne;
use TerminalBundle\Service\Fiscal;
use UserBundle\Entity\User;

class OrderManager
{
    const API_PATH = 'https://admin.venezia.by';

    private $container;
    private $doctrine;
    private $em;
    private $router;

    public function __construct(Container $container, RouterInterface $router)
    {
        $this->container = $container;
        $this->doctrine = $this->container->get("doctrine");
        $this->em = $this->doctrine->getManager();
        $this->router = $router;
    }

    /**
     * @param $data
     * @return Order
     */
    public function syncOrder($data)
    {
        $user = $this->container->get("doctrine")->getRepository(User::class)->find($data["userId"] ? $data["userId"] : 1);
        $shop = $this->em->getReference(Shop::class, $data["shopId"]);


        if ($data["masterId"]) {
            $order = $this->container->get("doctrine")->getRepository(Order::class)->find($data["masterId"]);
        } else {
 
            $order = new Order($user, $shop);
        }
$this->container->get('user.logger')->info($data["shopId"]);
$this->container->get('user.logger')->info($shop->getId());
        $order->setShop($shop);
        $order->setUser($user);

        $order->setTable($this->em->getReference(ShopPlaceTable::class, $data["tableId"]));
        $order->setPlace($this->em->getReference(ShopPlaceTable::class, $data["placeId"]));

        if ($data["localId"]) {
            $order->setLocalId($data["localId"]);
        } else {
            $order->setLocalId(0);
        }

        $order->setStatus($data["status"]);

        $order->setClientMoneyCash($data["cash"]);
        $order->setClientMoneyCard($data["card"]);

        if ($data["createdAt"]) {
            $order->setCreatedAt((\DateTime::createFromFormat("Y-m-d\TH:i:s.u\Z", $data["createdAt"]))->modify("+3 hours"));
        }
        if (array_key_exists("closedAt", $data)) {
            $order->setClosedAt((\DateTime::createFromFormat("Y-m-d\TH:i:s.u\Z", $data["closedAt"]))->modify("+3 hours"));
        }

        if ($data["client"] != 0) {
            $order->setClient($this->em->getReference(Client::class, $data["client"]));
            $order->setCashType(Order::CASHTYPE_INVOICE);
        } else {
          $order->setClient(null);
        }

        $order->setIsEditable(false);

        foreach ($order->getDishes() as $__dish) {
            $order->removeDish($__dish);
            $this->em->remove($__dish);
        }

        foreach ($data["items"] as $item) {
            if (!$item["quantity"]) continue;
            /*            if ($item["masterId"]) {
                            $orderItem = $this->container->get("doctrine")->getRepository(OrderDish::class)->find($item["masterId"]);
                        } else {*/
            $orderItem = new OrderDish();
            $orderItem
                ->setOrder($order)
                ->setDish($this->em->getReference(Dish::class, $item["item"]["id"]));

            if ($item["delegate"]) {
                $orderItem->setDelegate($this->em->getReference(Shop::class, $item["delegate"]));
            }

            $orderItem->setParentOrder($order);
            $order->addDish($orderItem);

//            }

            $orderItem->tmpID = $item["id"];

            if ($shop->getId() != 1) {
              $orderItem
                ->setQuantity($item["quantity"])
                ->setDishPrice($item["price"]-$item["discount"])
                ->setDishDiscount($item["discount"]);
            } else {
              $orderItem
                  ->setQuantity($item["quantity"])
                  ->setDishPrice($item["price"])
                  ->setDishDiscount($item["discount"]);
            }

            $this->em->persist($orderItem);
        }

        $this->em->persist($order);
        $this->em->flush();

        return $order;
    }

    public function handleOrder(Order $order, $data, $user)
    {
        // new order ??
        if (!$order->getId()) {
            $order->setLocalId($this->getLastOrderId($user->getShop()) + 1);
            $order->setStatus($user->isOrdersValidated() ? -2 /*Order::STATUS_WAIT_FOR_CONFIRM*/ : Order::STATUS_OPENED);
        }

        if (array_key_exists('table', $data)) {
            $order->setTable($this->em->getReference(ShopPlaceTable::class, $data["table"]));
        } else {
            if ($order->getTable() == null) {
                $order->setTable($this->em->getReference(ShopPlaceTable::class, 0));
            }
        }

        $order->setPlace($order->getTable()->getPlace());

        if (array_key_exists('meta', $data)) {
            $order->setMeta($this->makeMeta($order, $data['meta']));
        }

        if (array_key_exists('promo', $data) && $data['promo'] !== false) {
            $order->setPromo($this->em->getReference(PromoCard::class, $data["promo"]));
        }

        $orderDishChanges = $this->processDishes($order, $data['dishes']);


        $this->em->persist($order);
        $this->em->flush();


        if (array_key_exists('allow_edit_order', $data)) {
            $order->setIsEditable(true);
            $order->setIsChangeRequested(false);
            $this->forwardRequest($order, 'allow_edit', $user);
            $this->em->persist($order);
            $this->em->flush();
            return $order;
        } else {
            $order->setIsEditable($user->isOrdersValidated() ? true : false);
        }


        $nForNPromos = $this->getNForNDiscounts($order->getShop());

        foreach ($nForNPromos as $promo) {
            if ($this->isNForNAllowed($promo, $order)) {
                $nForOne = new NForOne($promo["category"], $promo["discount"], $this->em);
                $order = $nForOne->execute($order);

                $this->em->persist($order);
                $this->em->flush();
            }
        }


        if (array_key_exists('pay_order', $data)) {
            $order->setStatus(Order::STATUS_WAIT_FOR_MONEY);
            $this->container->get("terminal.print.storage")->printAdministratorOrder($order);
        }

        if (array_key_exists('delete_order', $data)) {
            $order->setStatus(Order::STATUS_DELETED);
            $order->setDeletedAt(new \DateTime());
            $this->forwardRequest($order, 'delete', $user);
            $this->container->get('terminal.print.service')->cancelOrder($order);
        }

        if (array_key_exists('return_order', $data)) {
            $order->setStatus(Order::STATUS_WAIT_FOR_MONEY);
            $this->forwardRequest($order, 'restore', $user);
        }

        if (!$user->isOrdersValidated()) {
            $this->container->get('terminal.print.service')->printDishChanges($orderDishChanges);
        }


        $this->em->persist($order);
        $this->em->flush();

        if ($order->getShop()->getId() != $this->container->getParameter("shop.id")) {
            $this->delegateRequest($order, $orderDishChanges);
        }

        foreach ($order->getDishes() as $orderDish) {
            if (!$orderDish->getOrder()) {
                $order->removeDish($orderDish);
            }
        }

//        dump($order->getDishes()->toArray());die();

        return $order;
    }

    public function getLastOrderId($shop)
    {
        $lastOrderId = $this->container->get("redis.helper")->get("last_order_" . $shop->getId());
        if (!$lastOrderId) {
            $lastOrder = $this->container->get("doctrine")->getRepository(Order::class)->findLastInShop($shop);
            if (!$lastOrder) {
                $lastOrderId = 1;
            } else {
                $lastOrderId = $lastOrder->getLocalId();
            }

            $this->container->get("redis.helper")->set("last_order_" . $shop->getId(), $lastOrderId, 3600);
        }

        $this->container->get("redis.helper")->set("last_order_" . $shop->getId(), $lastOrderId + 1, 3600);

        return $lastOrderId;
    }

    private function makeMeta(Order $order, $data)
    {
        $orderMeta = new OrderMeta($order);
        if (array_key_exists('phone', $data)) {
            $orderMeta->setPhone($order->getUser()->getName()." ".$data['phone']);
        }

        if (array_key_exists('comment', $data)) {
            $orderMeta->setComment(preg_replace("#\n#", " ", $order->getUser()->getName()." ".$data['comment']));
        }

        if (array_key_exists('time', $data)) {
            $orderMeta->setTime($data['time']);
        }

        if (array_key_exists('confirmUrl', $data)) {
            $orderMeta->setConfirmUrl($data['confirmUrl']);
        }

        if (array_key_exists('rejectUrl', $data)) {
            $orderMeta->setConfirmUrl($data['rejectUrl']);
        }

        $this->em->persist($orderMeta);

        return $orderMeta;
    }

    private function processDishes(Order $order, $dataDishes)
    {
        $dishChanges = [];

        foreach ($dataDishes as $__dish) {
            if (!array_key_exists('orderDishId', $__dish)) {
                $orderDish = new OrderDish($order);
                $orderDish
                    ->setDish($this->em->getReference(Dish::class, $__dish["id"]))
                    ->setQuantity($__dish["quantity"])
                    ->setParentOrder($order);

//$this->container->get('user.logger')->info(json_encode($data));

                if (array_key_exists('delegate', $__dish)) {
                    $orderDish->setDelegate($this->em->getReference(Shop::class, $__dish["delegate"]));
                }
                $order->addDish($orderDish);


                $dishChanges[] = [
                    'dish' => clone $orderDish,
                    'quantity' => $orderDish->getQuantity(),
                ];
            } else {
                $orderDish = $order->findDish($__dish['orderDishId']);

                if (!$orderDish) continue;

                $diff = $__dish['quantity'] - $orderDish->getQuantity();

                $orderDish->setQuantity($__dish['quantity']);

                if ($diff < 0) {
                    $orderChange = new OrderChange();
                    $orderChange
                        ->setAdministrator($order->getUser())
                        ->setType(OrderChange::ORDER_CHANGE_DISH_QUANTITY)
                        ->setOrderDish($orderDish)
                        ->setDifference($diff);
                    $this->em->persist($orderChange);
                }

                if (abs($diff) > 0) {
                    $dishChanges[] = [
                        'dish' => clone $orderDish,
                        'quantity' => $diff,
                    ];
                }
            }

            $this->setDishPrice($orderDish);
        }

        return $dishChanges;

        // work only with ID and Quantity
    }

    private function setDishPrice(OrderDish $orderDish)
    {
        $__shop = $orderDish->getOrder()->getShop();
        $shopPrice = $orderDish->getDish()->getShopPrice($__shop);
        $category = $orderDish->getDish()->getCategory();

        if (!$category->getIgnoreDiscount() && !$orderDish->getDish()->getIgnoreDiscount()) {
            //category discount
            $categoryDiscount = $category->getShopDiscount($__shop);
            $sellPrice = round($shopPrice * (1 - $categoryDiscount / 100), 2);

            $promoCard = $orderDish->getOrder()->getPromo();
            if ($promoCard) {
                $promoDiscount = $promoCard->getCardDiscount();
                if ($promoCard->getClient()->getBirthDate()->format("m-d") == (new \DateTime())->format("m-d")) {
                    $promoDiscount += 10;
                }
                if ($promoCard && !$category->getIgnoreDiscount() && !$orderDish->getDish()->getIgnoreDiscount()) {
                    $sellPrice = round($shopPrice * (1 - $promoDiscount / 100), 2);
                }
            }
        } else {
            $sellPrice = round($shopPrice, 2);
        }

        $orderDish->setDishPrice($sellPrice);
        $orderDish->setDishDiscount($shopPrice - $sellPrice);
    }

    private function forwardRequest(Order $order, $method, $user)
    {
        $this->container->get('user.logger')->info($method . ' ' . $_SERVER["REMOTE_ADDR"] . " " . $user->getId() . " " . $user->getName() . $order->getId());
        if ($this->container->getParameter('is_primary')) {
            $ch = curl_init();
            if ($method == "create") {
                curl_setopt($ch, CURLOPT_URL, "http://" . $order->getShop()->getVpnServerIP() . "/api/order/" . $method);
            } else if ($method == "delegate") {
                curl_setopt($ch, CURLOPT_URL, "http://" . $order->getShop()->getVpnServerIP() . "/api/order/" . $method);
            } else {
                curl_setopt($ch, CURLOPT_URL, "http://" . $order->getShop()->getVpnServerIP() . "/api/order/" . $order->getId() . "/" . $method);
            }

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_VERBOSE, true);

            $response = curl_exec($ch);
        }
    }

    public function getNForNDiscounts(Shop $shop)
    {

        $nForNPromosData = $this->em->getRepository(CategoryDiscount::class)->findActiveNForNNow($shop);


        return array_map(function (CategoryDiscount $categoryDiscount) {
            return [
                'category' => $categoryDiscount->getCategory()->getId(),
                'discount' => $categoryDiscount->getDiscount(),
                'timeStart' => $categoryDiscount->getTimeStart()->format("H:i:s"),
                'timeEnd' => $categoryDiscount->getTimeEnd()->format("H:i:s"),
                'days' => $this->sundayFirst(array_keys($categoryDiscount->getDaysArray())),
            ];
        }, $nForNPromosData);
    }

    private function sundayFirst($array)
    {
        return array_map(function ($i) {
            return $i + 1 == 7 ? 0 : $i + 1;
        }, $array);
    }

    private function isNForNAllowed($promo, Order $order)
    {
        if (!$order->getTable()->isToGo()) {
            return false;
        }

        if (!$this->isDayInRange($promo["days"])) {
            return false;
        }

        if (!$this->isTimeInRange($promo["timeStart"], $promo["timeEnd"])) {
            return false;
        }

        return true;
    }

    private function isDayInRange($days)
    {
        return in_array(date('w'), $days);
    }

    private function isTimeInRange($startTime, $endTime)
    {
        $currentTime = new \DateTime();
        $start = new \DateTime();
        $end = new \DateTime();

        // Set start time
        [$startHour, $startMinute, $startSecond] = explode(':', $startTime);
        $start->setTime((int)$startHour, (int)$startMinute, (int)$startSecond);

        // Set end time
        [$endHour, $endMinute, $endSecond] = explode(':', $endTime);
        $end->setTime((int)$endHour, (int)$endMinute, (int)$endSecond);

        // Check if current time is between start and end time
        return $currentTime >= $start && $currentTime <= $end;
    }

    private function delegateRequest(Order $order, $orderDishChanges)
    {
        $data = [
            'toShop' => $order->getShop()->getId(),
            'fromShop' => $order->getShop()->getId(),
            'table' => $order->getTable()->getId(),
            'place' => $order->getPlace()->getId(),
            'toUser' => $order->getUser()->getId(),
            'items' => []
        ];

        foreach ($orderDishChanges as $dishChange) {
            if (!$dishChange['dish']->getDelegate()) continue;

            $data['items'][] = [
                'itemId' => $dishChange['dish']->getDish()->getId(),
                'quantity' => $dishChange['quantity'],
            ];
        }

//var_dump(json_encode($data));die();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://" . $order->getShop()->getVpnServerIP() . "/api/order/delegate");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));


        $response = curl_exec($ch);

    }

    public function orderToJson(Order $order)
    {
        $meta = [];

        if ($order->getMeta()) {
            $meta = [
                'time' => $order->getMeta()->getTime(),
                'phone' => $order->getMeta()->getPhone(),
                'comment' => $order->getMeta()->getComment(),
                'type' => $order->getMeta()->getOrderType(),
            ];
        }

        $items = [];
        foreach ( $order->getDishes()->toArray() as $item) {
            $items[] = $this->orderDishToJson($item);
        }



        $orderData = [
            'id' => $order->getId(),
            'localId' => $order->getLocalId(),
            'masterId' => $order->getId(),
//            'user' => $order->getUser()->getName(),
            'userId' => $order->getUser()->getId(),
//            'table' => $order->getTable()->getId(),
            'tableId' => $order->getTable() ? $order->getTable()->getId() : null,
//            'place' => $order->getPlace()->getId(),
            'placeId' => $order->getPlace() ? $order->getPlace()->getId() : null,
            'createdAt' => $order->getCreatedAt()->format("c"),
            'cashtype' => $order->getCashType(),
            'status' => $order->getStatus(),
            'isEditable' => $order->getIsEditable(),
            'items' => $items,
/*            'items' => array_map(function ($orderDish) {
                return $this->orderDishToJson($orderDish);
            }, $order->getDishes()->toArray()),*/

            'meta' => $meta,
        ];

        return $orderData;
    }

    private function orderDishToJson(OrderDish $orderDish)
    {

        return [
//          'id' => $orderDish->getDish()->getId(),
//          'orderDishId' => $orderDish->getId(),
            'id' => $orderDish->getId(),
            'masterId' => $orderDish->getId(),
            'item' => $this->dishToJson($orderDish->getDish()),
            'name' => $orderDish->getDish()->getName(),
            'quantity' => $orderDish->getQuantity(),
            'price' => $orderDish->getDishPrice(),
            'discount' => $orderDish->getDishDiscount(),
            'tmpID' => $orderDish->tmpID,
            'time' => 1,
        ];
    }

    private function dishToJson(Dish $dish)
    {
        return [
            'id' => $dish->getId(),
            'name' => $dish->getName(),
        ];
    }

    public function close(Order $order, $closeData, User $user)
    {
        $order->setStatus(Order::STATUS_CLOSED);

        $order->setCashType(null);
        if (array_key_exists("client", $closeData) && $closeData['client'] != null) {
            $order->setCashType(Order::CASHTYPE_INVOICE);
            $order->setClient($this->em->getReference(Client::class, $closeData['client']));
        }

        if ($closeData['card'] + $closeData['cash'] == $order->getDishSum()) {
            $order->setCashType(Order::CASHTYPE_CASH);
            $order->setClientMoneyCash($closeData['cash']);
            $order->setClientMoneyCard($closeData['card']);
        }

        if ($order->getCashType() !== null) {
            $order->setClosedAt(new \DateTime());
            $order->setIsEditable(false);

            $this->em->persist($order);
            $this->em->flush();

            if ($order->getShop()->getId() == $this->container->getParameter('shop.id') && $user->isFiscalGranted() && $order->getCashType() == Order::CASHTYPE_CASH) {
                if (bccomp($order->getCashSum(), round($order->getDishSum(), 2)) == 0) {
                    if ($user->isFiscalGranted()) {
                        $this->container->get('terminal.fiscal')->printCheck($order, Fiscal::FISCAL_SELL, $order->getShop()->getFiscalModel(), $this->container->get('user.logger'));
                    }
                } else {
                    $order->setStatus(Order::STATUS_WAIT_FOR_MONEY);
                    $this->em->flush();
                }
            }
        }
    }

    public function delete(Order $order)
    {
        $order->setStatus(Order::STATUS_DELETED);
        $order->setIsEditable(false);

        return $order;
    }

    public function redirectOrder(Order $order, Shop $redirectTo)
    {

        $ch = curl_init($this->container->getParameter('api_path') . $this->container->get('router')->generate('api_web_order_redirect', [
                'id' => $order->getMeta()->getWebOrder()
            ]));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'target' => $redirectTo->getId(),
        ]);

        $res = curl_exec($ch);

        return $res;
    }

    private function orderDishSearch($dishes, $dish, $fields = ["dish", "quantity"])
    {
        $found = false;
        foreach ($dishes as $key => $__dish) {
            $fieldsMatchCnt = 0;
            foreach ($fields as $field) {
                if ($__dish[$field] == $dish[$field]) {
                    $fieldsMatchCnt++;
                }
            }
            if ($fieldsMatchCnt == count($fields)) {
                return $key;
            }
        }

        return $found;
    }
}
