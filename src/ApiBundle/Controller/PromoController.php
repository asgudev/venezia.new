<?php

namespace ApiBundle\Controller;

use ControlBundle\Entity\DishPhotoCheck;
use FrontendBundle\Entity\WebOrder;
use FrontendBundle\Entity\WebOrderDish;
use DishBundle\Entity\DishCategory;
use DishBundle\Entity\Dish;
use DishBundle\Entity\IngredientInWarehouse;
use DishBundle\Entity\IngredientTransfer;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use UserBundle\Entity\UserEvent;

/**
 * Class PromoController
 * @package ApiBundle\Controller
 */
class PromoController extends Controller
{
    /**
     * @Route("/promo/load", name="api_promo_card_load")
     */
    public function loadPromoCardAction(Request $request)
    {
        $card = $this->getDoctrine()->getRepository(PromoCard::class)->findOneBy([
            'cardId' => $request->get('code'),
        ]);

        if (!$card) {
            return new JsonResponse(false, 404);
        }

        $discount = $this->get("discount_counter.service")->getDiscount($card);

        switch ($card->getType()) {
            case PromoCard::TYPE_CARD:
                $discountName = $card->getClient()->getFirstName() . " (" . $discount . "%)";

                if ($card->getClient()->getBirthDate()->format("m-d") == (new \DateTime())->format("m-d")) {
                    $discountName = $card->getClient()->getFirstName() . " (" . $discount . "% + 10%) ДР";
                    $discount += 10;
                }
                break;
            case PromoCard::TYPE_TOURIST:
                $discountName = "Карта туриста";
                $discount = 20;

                if ($card->getIssuedAt() == null) {
                    $card->setIssuedAt(new \DateTime());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($card);
                    $em->flush();
                    $discountName = "Карта туриста активация";
                    $discount = 0;
                } else {
                    /*
                    $activationDiff = ((new \DateTime())->getTimestamp() - $card->getIssuedAt()->getTimestamp());
                    if ($activationDiff > 259200) {
                        $discountName = "Карта туриста истекла";
                        $discount = 0;
                    }
                    if ($activationDiff < 900) {
                        $discountName = "Карта туриста активация";
                        $discount = 0;
                    }*/
                    $discount = $card->getCardDiscount();
                }
                break;
        }

        return new JsonResponse([
            'id' => $card->getId(),
            'name' => $discountName,
            'discount' => $discount,
        ]);
    }

}
