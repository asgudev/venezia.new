<?php

namespace DishBundle\Form\Type;

use DishBundle\Entity\DishCategory;
use RestaurantBundle\Form\Type\PrinterType;
use RestaurantBundle\Service\BitToArray;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishCategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", TextType::class, [
                'label' => 'Название',
                'required' => true,
            ])
            ->add("description", TextareaType::class, [
                'label' => 'Описание',
                'required' => false,
            ])
            ->add('imageFile', FileType::class, [
                'required' => false,
                'data_class' => null,
                'file_path' => 'image',
            ])
            ->add('parent', EntityType::class, [
                'label' => 'Родительская категория',
                'class' => DishCategory::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false,
                'empty_data' => null,
                'placeholder' => 'Родительская категория',
                'required' => false,
            ])
            ->add('isTerminal', CheckboxType::class, [
                'label' => 'Отображать в терминале',
                'required' => false,
            ])
            ->add('discounts', CollectionType::class, [
                'label' => 'Скидка',
                'entry_type' => CategoryDiscountType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false,
            ])
            ->add("printers", CollectionType::class, [
                'entry_type' => PrinterType::class,
                'label' => 'Принтеры',
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'prototype' => true,
                'by_reference' => false,
            ])
            ->add('isGroupNames', CheckboxType::class, [
                'label' => 'Группировать по именам',
                'required' => false,
            ])
            ->add('ignoreDiscount', CheckboxType::class, [
                'label' => 'Игнорировать скидки',
                'required' => false,
            ])
            ->add('sortGroupOrder', ChoiceType::class, [
                'label' => 'Сортировка в группе',
                'choices' => [
                    'По убыванию цены' => 0,
                    'По возрастанию цены' => 1,
                    'По алфавиту' => 2,
                ],
                'required' => true,
            ])
            ->add("orderTypes", ChoiceType::class, [
                'label' => "Типы заказов",
                'multiple' => true,
                'expanded' => true,
                'choices' => DishCategory::ORDER_TYPE
            ])
            ->add('sortOrder', ChoiceType::class, [
                'label' => 'Сортировка вне группы',
                'choices' => [
                    'По убыванию цены' => 0,
                    'По возрастанию цены' => 1,
                    'По алфавиту' => 2,
                ],
                'required' => true,
            ]);

        $builder
            ->get('orderTypes')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    $bittoarray = new BitToArray();
                    return $bittoarray->bitToArray($output, DishCategory::ORDER_TYPE);
                },
                function ($input) {
                    $bitmask = 0;
                    foreach ($input as $mask) {
                        $bitmask += $mask;
                    }
                    return $bitmask;
                }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DishCategory::class,
        ));
    }
}
