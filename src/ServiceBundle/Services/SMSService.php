<?php

namespace ServiceBundle\Services;

class SMSService
{

    public function __construct()
    {

    }

    public function sendMessage($phone, $message)
    {
        $this->sendSms([
            'recipients' => $phone,
            'message' => $message,
            'sender' => 'venezia.by'
        ]);

        return;
    }

    private function getCurl($method = 'api/user_balance', $data = [])
    {
        $ch = curl_init();

        $url = "http://cp.smsp.by";

        $params = [
            'r' => $method,
            'user' => 'itse@tut.by',
            'apikey' => 'clr937curo',
        ];

        $params = array_merge($params, $data);

        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_FAILONERROR => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $params
        ));

        return $ch;
    }

    public function sendSms($data)
    {
        $ch = $this->getCurl('api/msg_send', $data);
        $res = curl_exec($ch);
        return json_decode($res);
    }

    public function getBalance()
    {
        $ch = $this->getCurl('api/user_balance');
        $res = curl_exec($ch);
        return json_decode($res);
    }
}