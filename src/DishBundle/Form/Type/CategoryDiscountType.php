<?php
namespace DishBundle\Form\Type;

use DishBundle\Entity\CategoryDiscount;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Service\BitToArray;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryDiscountType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("shop", EntityType::class, [
                'label' => 'Заведение',
                'class' => Shop::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false,
            ])
            ->add('discount', TextType::class, [
                'label' => 'Скидка, %',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Скидка % или n/n',
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => "Тип",
                'multiple' => false,
                'expanded' => false,
                'choices' => CategoryDiscount::TYPES,
            ])
            ->add("day", ChoiceType::class, [
                'label' => "День",
                'multiple' => true,
                'expanded' => false,
                'choices' => CategoryDiscount::DAYS
            ])
            ->add('timeStart', TimeType::class, [
                'label' => 'Начало',
                'widget' => 'single_text',
            ])
            ->add('timeEnd', TimeType::class, [
                'label' => 'Конец',
                'widget' => 'single_text',
            ]);

        $builder
            ->get('day')->addModelTransformer(new CallbackTransformer(
                function ($output) {
                    $bittoarray = new BitToArray();
                    return $bittoarray->bitToArray($output, CategoryDiscount::DAYS);
                },
                function ($input) {
                    $bitmask = 0;
                    foreach ($input as $mask) {
                        $bitmask += $mask;
                    }
                    return $bitmask;
                }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CategoryDiscount::class,
        ));
    }
}