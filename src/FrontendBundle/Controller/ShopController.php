<?php

namespace FrontendBundle\Controller;

use FrontendBundle\Entity\WebShop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use RestaurantBundle\Entity\Shop;


class ShopController extends BaseController
{



    /**
     * @Route("/{slug}/menu", name="frontend_show_shop_menu")
     */
    public function showShopMenuAction(Request $request, WebShop $webShop)
    {
        if (!$webShop)
            throw new NotFoundHttpException();


        $shop = $webShop->getShop();

        $categories = $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findTakeawayMenuByShop($shop);

        //dump($categories);        die();

        return $this->render('@Frontend/New/shop_menu_new.html.twig', [
            'shop' => $shop,
            'webShop' => $webShop,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/{slug}/gallery", name="frontend_show_shop_gallery")
     */
    public function showShopGalleryAction(Request $request, Shop $shop)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        return $this->render('@Frontend/New/shop_gallery_new.html.twig', [
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/{slug}/otzyvy", name="frontend_show_shop_review")
     */
    public function showShopReviewAction(Request $request, Shop $shop)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        return $this->render('@Frontend/New/shop_review_new.html.twig', [
            'shop' => $shop,
        ]);
    }


    /**
     * @Route("/{slug}/otzyvy/добавитьотзыв", name="frontend_add_shop_review")
     */
    public function addShopReviewAction(Request $request, Shop $shop)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        return $this->render('@Frontend/New/shop_review_add_new.html.twig', [
            'shop' => $shop,
        ]);
    }

    /**
     * @Route("/{slug}/kontakty", name="frontend_show_shop_contact")
     */
    public function showShopContactAction(Request $request, Shop $shop)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        return $this->render('@Frontend/New/shop_contact_new.html.twig', [
            'shop' => $shop,
        ]);
    }
}