function Place() {
    this.id = 0;
    this.name = null;
    this.container = null;


    this.tables = [];

    this.addTable = function (table) {
        this.tables.push(table);
    }
}

function Table() {
    const STATUS_FREE = -1;
    const STATUS_WAIT_FOR_KITCHEN = 1;
    const STATUS_BUSY = 0;
    const STATUS_WAIT_FOR_MONEY = 3;

    const SVGNS = "http://www.w3.org/2000/svg";

    this.id = 0;
    this.name = null;
    this.place = null;
    this.context = null;

    this.coords = {
        x: 0, y: 0, w: 1, h: 1
    };
    this.isToGo = false;

    this.status = STATUS_FREE;
    this.orderedAt = null;

    this.orderId = null;
    this.orderIdLocal = null;
    this.waiter = null;


    this.svgTable = null;
    this.svgTableObject = null;
    this.rect = null;

    this.removeOrder = function () {
        this.orderId = null;
        this.orderIdLocal = null;
        this.waiter = null;
        this.status = STATUS_FREE;

        this.drawTable();
    };

    this.setOrder = function (data) {
        this.orderId = data.orderId;
        this.orderIdLocal = data.orderIdLocal;
        this.waiter = data.waiter;
        this.status = data.status;

        this.drawTable();
    };


    this.drawTable = function () {
        if (this.coords) {
            if (this.svgTableObject === null) {
                this.svgTableObject = {
                    'group': document.createElementNS(SVGNS, 'g'),
                    'rect': document.createElementNS(SVGNS, 'rect'),
                    'name': this.createSvgText(this.name, {
                        x: this.coords.x + 5,
                        y: this.coords.y + 10,
                    }),
                    'order': this.createSvgText('test', {
                        x: this.coords.x + 5,
                        y: this.coords.y + 30,
                    }),
                    'waiter': this.createSvgText('test', {
                        x: this.coords.x + 5,
                        y: this.coords.y + 50,
                        w: this.coords.w - 10,
                    }, true),
                };
            }

            this.svgTableObject.rect.setAttributeNS(null, 'x', this.coords.x);
            this.svgTableObject.rect.setAttributeNS(null, 'y', this.coords.y);
            this.svgTableObject.rect.setAttributeNS(null, 'height', this.coords.h);
            this.svgTableObject.rect.setAttributeNS(null, 'width', this.coords.w);
            this.svgTableObject.rect.setAttributeNS(null, 'fill', '#' + this.getColor(this.status));


            this.svgTableObject.waiter.textContent = this.waiter ? this.waiter : '';
            this.svgTableObject.order.textContent = this.orderIdLocal ? 'Счет №' + this.orderIdLocal : '';


            if (this.svgTable === null) {
                this.svgTable = this.svgTableObject.group;
                this.svgTable.appendChild(this.svgTableObject.rect);
                this.svgTable.appendChild(this.svgTableObject.name);
                this.svgTable.appendChild(this.svgTableObject.order);
                this.svgTable.appendChild(this.svgTableObject.waiter);
                this.place.container.appendChild(this.svgTable);

                $(this.svgTableObject.group).on("click", {
                    context: this,
                }, this.tableClick);
            }
        }
    };

    this.reloadOrderForm = function ($form, data) {
        //$('#order_table_row select')[0].selectize.disable();
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: data,
            objectData: data,
            success: function (html) {
                $('#order_table_row').replaceWith($(html).find('#order_table_row'));
                $("#order_place").val(this.objectData['order[place]']);

                if (this.objectData.table !== undefined) {
                    $("#order_table").val(this.objectData.table);
                }
            }
        });
    };

    this.tableClick = function (event) {
        var context = event.data.context;

        if (context.status == STATUS_FREE) {
            closeOrder();

            $('#place_name_label').text(context.place.name);
            $('#table_name_label').text(context.name);

            $('#place_name').val(context.place.id);
            $('#table_name').val(context.id);

            $('#order_save_order').removeClass("disabled");

            $("#table_control").toggle();
            $("#order_control").toggle();

            order.table = context;
            order.tableId = context.id;
            order.placeId = context.place.id;
        } else {
            $.ajax({
                url: '/terminal/order/' + context.orderId + '/edit',
                method: "GET",
                success: function (response) {

                    $("#custom_order")
                        .replaceWith(response)
                        .show()
                        .data("status", 1);

                    $("#table_control").toggle();
                    $("#order_control").toggle();
                    $("#new_order").hide();
                }
            });
        }
    };

    this.createSvgText = function (text, coordinates, stretch) {
        var svg_text = document.createElementNS(SVGNS, 'text');
        svg_text.setAttributeNS(null, 'x', coordinates.x);
        svg_text.setAttributeNS(null, 'y', coordinates.y);
        if (stretch == true) {
            svg_text.setAttributeNS(null, 'textLength', coordinates.w);
            svg_text.setAttributeNS(null, 'lengthAdjust', 'spacingAndGlyphs');
        }
        svg_text.setAttributeNS(null, 'alignment-baseline', 'middle');
        svg_text.setAttribute('fill', '#000');
        svg_text.textContent = text;
        return svg_text;
    };

    this.getColor = function (status) {
        switch (status) {
            case (STATUS_FREE):
                return 'baf7ba';
                break;
            case (STATUS_BUSY):
                return 'ff9393';
                break;
        }
    };

}