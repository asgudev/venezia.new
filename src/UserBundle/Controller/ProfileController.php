<?php


namespace UserBundle\Controller;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Form\Type\Frontend\ProfileFormType;
use UserBundle\Services\UserKey;

/**
 * Controller managing the user profile
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends Controller
{
    /**
     * Show the user
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('FOSUserBundle:Profile:show.html.twig', array(
            'user' => $user
        ));
    }

    /**
     * Edit the user
     */
    public function editAction(Request $request)
    {
        if (!$this->getUser()->hasRole("ROLE_BUHGALT"))
            throw new AccessDeniedHttpException();

        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm(ProfileFormType::class, $user, [
            'user' => $user,
            'action' => $this->generateUrl('fos_user_profile_edit'),
        ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);

            if ($user->hasRole("ROLE_SUPER_ADMIN")) {

            } elseif ($user->hasRole("ROLE_ADMIN")) {

            } else {
                return $this->redirect($this->generateUrl("terminal_index"));
            }
        }

        return $this->render('FOSUserBundle:Profile:edit.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'qr' => base64_encode(UserKey::getUserKey($user) . "_" . time()),
            'userKey' => UserKey::getUserKey($user),
        ));
    }


    function stringToHex($str)
    {
        $hex = '';
        for ($i = 0; $i < strlen($str); $i++) {
            $charCode = dechex(ord($str[$i]));
            $hex .= str_pad($charCode, 2, '0', STR_PAD_LEFT);
        }
        return $hex;
    }
}
