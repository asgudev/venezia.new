<?php

namespace ControlBundle\Entity\Repository;


use ControlBundle\Entity\DishPhoto;
use Doctrine\ORM\EntityRepository;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;

class DishPhotoRepository extends EntityRepository
{
    /**
     * @return DishPhoto[]
     */
    public function findByOrder(Order $order)
    {
        return $this->createQueryBuilder('p')
            ->addSelect('od')
            ->addSelect('o')
            ->addSelect('u')
            ->where('o.id = :id')
            ->leftJoin('p.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->leftJoin("p.user", 'u')
            ->setParameters([
                'id' => $order->getId()
            ])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     */
    public function findCountInRangeStats($dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('dp')
            ->select("COUNT(dp.id) as photoCnt")
            ->addSelect('s.name as shopName')
            ->leftJoin('dp.orderDish', 'od')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.shop', 's')
            ->leftJoin('od.dish', 'd')
            ->where("dp.date >= :dateStart")
            ->andWhere("dp.date <= :dateEnd")
            ->setParameters([
                'dateStart' => new \DateTime($dateStart),
                'dateEnd' => new \DateTime($dateEnd),
            ])
            ->groupBy('s.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $dateStart
     * @param $dateEnd
     * @return DishPhoto[]
     */
    public function findAllInRangeStats($dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('dp')
            ->addSelect('od')
            ->addSelect('d')
            ->addSelect('o')
            ->addSelect('s')
            ->leftJoin('dp.orderDish', 'od')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.shop', 's')
            ->leftJoin('od.dish', 'd')
            ->where("dp.date >= :dateStart")
            ->andWhere("dp.date <= :dateEnd")
            ->setParameters([
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->getQuery()
            ->getResult();
    }


    public function findValidationStats($dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('dp')
            ->select("COUNT(dp.id) as uCount")
            ->addSelect('vu.name as name')
            ->leftJoin('dp.orderDish', 'od')
            ->leftJoin('dp.validatedBy', 'vu')
            ->leftJoin('od.parentOrder', 'o')
            ->leftJoin('o.shop', 's')
            ->where("dp.date >= :dateStart")
            ->andWhere("dp.date <= :dateEnd")
            ->andWhere('dp.isValidated = true')
            ->setParameters([
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->groupBy('vu.id')
            ->getQuery()
            ->getResult();
    }

    public function findPhotoStats($dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('dp')
            ->select("COUNT(dp.id) as pCount")
            ->addSelect('s.name as name')
            ->addSelect('AVG(TIME_TO_SEC(TIME_DIFF(dp.date, o.createdAt))) as avgTime')
            ->addSelect('MIN(TIME_TO_SEC(TIME_DIFF(dp.date, o.createdAt))) as minTime')
            ->addSelect('MAX(TIME_TO_SEC(TIME_DIFF(dp.date, o.createdAt))) as maxTime')
            ->leftJoin('dp.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->leftJoin('o.shop', 's')
            ->where("dp.date >= :dateStart")
            ->andWhere("dp.date <= :dateEnd")
            ->setParameters([
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->groupBy('s.id')
            ->getQuery()
            ->getResult();
    }


    public function findStatByDish($dateStart = null, $dateEnd = null)
    {
        $stat = $this->createQueryBuilder('dp')
            ->select("COUNT(dp) as dCount")
            ->addSelect("d.name as dName")
            ->addSelect('dp.isValidated as isValidated')
            ->leftJoin("dp.orderDish", "od")
            ->leftJoin("od.dish", "d")
            ->groupBy("d.id")
            ->addGroupBy('dp.isValidated');

        if ($dateStart) {
            $stat->andWhere("dp.date >= :dateStart")->setParameter('dateStart', $dateStart);
        }
        if ($dateEnd) {
            $stat->andWhere("dp.date <= :dateEnd")->setParameter('dateEnd', $dateStart);
        }


        return $stat->getQuery()
            ->getResult();
    }

    /**
     * @param DishPhoto $dishPhoto
     * @return DishPhoto
     */
    public function findFull($dishPhoto_id)
    {
        return $this->createQueryBuilder('p')
            ->addSelect('o')
            ->addSelect('od')
            ->addSelect('d')
            ->addSelect('u')
            ->addSelect('ou')
            ->addSelect('c')
            ->addSelect('cd')
            ->leftJoin('p.orderDish', 'od')
            ->leftJoin('od.dish', 'd')
            ->leftJoin('od.order', 'o')
            ->leftJoin('o.user', 'ou')
            ->leftJoin('p.checks', 'c')
            ->leftJoin('c.user', 'u')
            ->leftJoin('c.dish', 'cd')
            ->where('p.id = :id')
            ->setParameters([
                'id' => $dishPhoto_id
            ])
            ->getQuery()
            ->getSingleResult();
    }

    public function findLowChecked($checks, $shop)
    {
        $photos = $this->createQueryBuilder('p')
            ->where('p.checkTimes = :checks')
            ->andWhere('p.isValidated = false')
            ->andWhere('p.date < :date')
            ->addSelect('c')
            ->leftJoin('p.orderDish', 'od')
            ->leftJoin('od.order', 'o')
            ->leftJoin('p.checks', 'c')
            ->leftJoin('c.user', 'u')
            ->setParameters([
                'checks' => $checks,
                'date' => (new \DateTime())->modify('-5 min'),
            ])
            ->orderBy('p.date', "DESC");

        /*        if ($shop) {
                    $photos
                        ->andWhere('o.shop != :shop')
                        ->setParameter('shop', $shop);
                }*/

        return $photos->getQuery()
            ->getResult();
    }

    /**
     * @param int $limit
     * @return DishPhoto[]
     */
    public function findLast($limit = 20)
    {
        return $this->createQueryBuilder('p')
            ->addSelect('odd')
            ->addSelect('od')
            ->addSelect('o')
            ->addSelect('c')
            ->addSelect('cu')
            ->addSelect('pu')
            ->leftJoin('p.orderDish', 'od')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'odd')
            ->leftJoin('p.checks', 'c')
            ->leftJoin('c.user', 'cu')
            ->leftJoin('p.user', 'pu')
            ->where('p.isValidated = false')
            ->orderBy('p.date', "DESC")
            //     ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $limit
     * @return DishPhoto[]
     */
    public function findValidated($limit = 20)
    {
        return $this->createQueryBuilder('p')
            ->addSelect('odd')
            ->addSelect('od')
            ->addSelect('o')
            ->addSelect('c')
            ->leftJoin('p.orderDish', 'od')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'odd')
            ->leftJoin('p.checks', 'c')
            ->where('p.isValidated = true')
            ->orderBy('p.date', "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DishPhoto
     */
    public function findNotPredicted()
    {
        return $this->createQueryBuilder('p')
            ->where('p.predict is null')
            ->orWhere('p.predict = :empty')
            ->orderBy('p.date', "DESC")
            ->setParameters([
                'empty' => 'a:0:{}'
            ])
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return DishPhoto[]
     */
    public function findNotValidated()
    {
        return $this->createQueryBuilder('p')
            ->addSelect('odd')
            ->addSelect('od')
            ->addSelect('o')
            ->addSelect('c')
            ->leftJoin('p.orderDish', 'od')
            ->leftJoin('od.order', 'o')
            ->leftJoin('od.dish', 'odd')
            ->leftJoin('p.checks', 'c')
            ->where('p.isValidated = false')
//            ->andWhere('p.predict is not null')
            ->orderBy('p.date', "DESC")
//            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $limit
     * @return DishPhoto[]
     */
    public function findPhotoed($orderDishes)
    {
        return $this->createQueryBuilder('p')
            ->where('p.orderDish IN (:dishes)')
            ->setParameters([
                'dishes' => $orderDishes
            ])
            ->getQuery()
            ->getResult();
    }


    public function getCheckCount()
    {
        return $this->createQueryBuilder('p')
            ->select('MIN(p.checkTimes) as minChecks')
            ->addSelect('MAX(p.checkTimes) as maxChecks')
            ->getQuery()
            ->getSingleResult();
    }
}
