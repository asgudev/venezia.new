<?php
namespace TerminalBundle\Events;

use Symfony\Component\EventDispatcher\Event;
use OrderBundle\Entity\Order;


class OrderEvent extends Event
{
    /**
     * смена статуса
     */
    const ORDER_PRINT = 'order.print';
    const ORDER_PRINT_KITCHEN = 'order.print.kitchen';
    const ORDER_PRINT_BAR = 'order.print.bar';
    const ORDER_PRINT_DELEGATE = 'order.print.delegate';

    /**
     * @var \OrderBundle\Entity\Order
     */
    private $order;

    private $dish;

    private $printer;

    /**
     * @return mixed
     */
    public function getPrinter()
    {
        return $this->printer;
    }

    /**
     * @param mixed $printer
     */
    public function setPrinter($printer)
    {
        $this->printer = $printer;
    }

    /**
     * @param \OrderBundle\Entity\Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @param mixed $dish
     */
    public function setDish($dish)
    {
        $this->dish = $dish;
    }


}