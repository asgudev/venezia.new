<?php

namespace UserBundle\Form\Type\Frontend;

use RestaurantBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;

class ProfileFormType extends AbstractType
{
    /**
     * @param string $class The User class name
     */
    public function __construct()
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", TextType::class, [
                'label' => "Фамилия Имя",
            ])
//            ->add("email", EmailType::class, [
//                'label' => "Email адрес",
//                'required' => false,
//            ])
            ->add("phone", TextType::class, [
                'label' => "Телефон",
            ])
//            ->add("birth", DateType::class, [
//                'label' => "Дата рождения",
//                'widget' => 'single_text',
//                'format' => 'dd.MM.yyyy',
//                'required' => false,
//                'attr' => [
//                    'autocomlete' => 'off',
//                ]
//            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Новый пароль',
                'required' => false,
            ])
            ->add("defaultLocale", ChoiceType::class, [
                'label' => "Язык системы",
                'choices' => [
                    'Русский' => 'ru',
                    'Анлийский' => 'en',
                    'Итальянский' => 'it',
                ],
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('shop', EntityType::class, [
                'label' => 'Текущее заведение',
                'class' => Shop::class,
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => false,
            ])
            ->add("adminShops", EntityType::class, [
                'label' => 'Заведения',
                'class' => Shop::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
            ]);

        $user = $options["user"];
        if ($user->hasRole("ROLE_ADMIN")) {
            $builder
                ->add("enabled", CheckboxType::class, [
                    'label' => 'Активен',
                    'required' => false,
                ])
                ->add('isFiscalGranted', CheckboxType::class, [
                    'label' => 'Доступ к ФР',
                    'required' => false,
                ])
                ->add('isOrdersValidated', CheckboxType::class, [
                    'label' => 'Счета с подтверждением',
                    'required' => false,
                ])
                ->add("roles", ChoiceType::class, [
                    'label' => 'Права доступа',
                    'multiple' => true,
                    'expanded' => true,
                    'choices' => [
                        'Пользователь' => 'ROLE_USER',
                        'Пиццерист' => 'ROLE_PIZZA',
                        'Администратор заведения' => 'ROLE_ADMINISTRATOR',
                        'Поставка продуктов' => 'ROLE_SUPPLY',
                        'Маркетолог' => 'ROLE_MARKETMAN',
                        'Менеджер' => 'ROLE_SHOP_MANAGER',
                        'Бухгалтер' => 'ROLE_BUHGALT',
                        'СЕО' => 'ROLE_SEO',
                        'Админ' => 'ROLE_ADMIN',
                        'Доставка' => 'ROLE_DELIVERY',
                    ],
                ]);
        }


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user' => null,
            'data_class' => User::class,
        ));
    }
}
