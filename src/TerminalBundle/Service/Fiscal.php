<?php

namespace TerminalBundle\Service;

/*


Hbw5Gb53ndFcQ5Kz


a:6:{i:0;s:9:"ROLE_USER";i:1;s:10:"ROLE_ADMIN";i:2;s:14:"ROLE_MARKETMAN";i:3;s:12:"ROLE_BUHGALT";i:4;s:18:"ROLE_ADMINISTRATOR";i:5;s:17:"ROLE_SHOP_MANAGER";}

 */


use OrderBundle\Entity\Order;

class Fiscal
{
//netsh interface portproxy add v4tov4 listenport=4422 listenaddress=192.168.1.20 connectport=80 connectaddress=192.168.5.2
//8 017 245 14 02
// netsh interface portproxy add v4tov4 listenport=4422 listenaddress=192.168.0.142 connectport=80 connectaddress=169.254.153.160
    /*

    {
  "x01": "Цена не указана",
  "x02": "Количество не указано",
  "x03": "Отдел не указан",
  "x04": "Группа не указана",
  "x31": "Пользователь уже зарегистрирован",
  "x32": "Неверный пароль",
  "x33": "Неверный номер таблицы",
  "x34": "Доступ к таблице запрещен",
  "x35": "Умолчание не найдено",
  "x36": "Неверный индекс",
  "x37": "Неверное поле",
  "x38": "Таблица переполнена",
  "x39": "Неверная длина двоичных данных",
  "x3A": "Попытка модификации поля только для чтения",
  "x3B": "Неверное значение поля",
  "x3C": "Товар уже существует",
  "x3D": "По товару были продажи",
  "x3E": "Запрос запрещен",
  "x3F": "Неверная закладка",
  "x40": "Ключ не найден",
  "x41": "Процедура уже исполняется",
  "x42": "Количество товара отрицательно",
  "x8A": "Нет бумаги для контрольной ленты",
  "x8B": "Нет бумаги для чековой ленты",
  "x8C": "Переполнение фискальной памяти",
  "x8D": "Выдача сдачи запрещена",
  "x92": "Ошибка печати",
  "xA6": "Есть 3 или больше непереданных отчета",
  "xB6": "Ресторанный режим не активен",
  "xB7": "Ресторанный счет не открыт",
  "xB8": "Переполнение количества заказов",
  "xB9": "Ресторанный чек открыт",
  "xBA": "Неверный номер счета",
  "xBB": "Лента не пуста",
  "xBC": "Режим тренировки",
  "xBD": "Текущая дата неверна",
  "xBE": "Запрещено изменение времени",
  "xBF": "Истек сервисный таймер",
  "xC0": "Ошибка работы с платежным терминалом",
  "xC1": "Неверный номер налога",
  "xC2": "Неверный параметр у процедуры",
  "xC3": "Режим фискального принтера не активен",
  "xC4": "Изменялось название товара или его налог",
  "xC5": "СКНО занято",
  "xC6": "Ошибка обмена с СКНО",
  "xC7": "Смена в СКНО не открыта",
  "xC8": "СКНО переполнено",
  "xC9": "Ошибка статуса СКНО",
  "xCA": "Ошибка идентификации СКНО",
  "xCB": "Запрещена операция продажи",
  "xCE": "Неверный тип кода товара",
  "xCC": "Начата операция возврата",
  "xCF": "Не выведен отчет Z1",
  "xD0": "Не сделана инкассация денег",
  "xD1": "Сейф не закрыт",
  "xD2": "Печать ленты прервана",
  "xD3": "Достигнут конец текущей смены, или изменилась дата",
  "xD4": "Не указано значение процентной скидки по умолчанию",
  "xD5": "Не указано значение скидки по умолчанию",
  "xD6": "Дневной отчет не выведен",
  "xD7": "Дневной отчет уже выведен (и пуст)",
  "xD8": "Нельзя отменить товар на который сделана скидка без ее предварительной отмены",
  "xD9": "Товар не продавался в этом чеке",
  "xDA": "Нечего отменять",
  "xDB": "Отрицательная сумма продажи товара",
  "xDC": "Неверный процент",
  "xDD": "Нет ни одной продажи",
  "xDE": "Скидки запрещены",
  "xDF": "Неверная сумма платежа",
  "xE0": "Тип оплаты не предполагает введения кода клиента",
  "xE1": "Неверная сумма платежа",
  "xE2": "Идет оплата чека",
  "xE3": "Товар закончился",
  "xE4": "Номер группы не может меняться",
  "xE5": "Неверная группа",
  "xE6": "Номер отдела не может меняться",
  "xE7": "Неверный отдел",
  "xE8": "Нулевое произведение количества на цену",
  "xE9": "Переполнение внутренних сумм",
  "xEA": "Дробное количество запрещено",
  "xEB": "Неверное количество",
  "xEC": "Цена не может быть изменена",
  "xED": "Неверная цена",
  "xEE": "Товар не существует",
  "xEF": "Начат чек внесения-изъятия денег",
  "xF0": "Чек содержит продажи",
  "xF1": "Не существующий или запрещенный тип оплаты",
  "xF2": "Поле в строке переполнено",
  "xF3": "Отрицательная сумма по дневному отчету",
  "xF4": "Отрицательная сумма по чеку",
  "xF5": "Чек переполнен",
  "xF6": "Дневной отчет переполнен",
  "xF7": "Чек для копии не найден",
  "xF8": "Оплата чека не завершена",
  "xF9": "Кассир не зарегистрирован",
  "xFA": "У кассира нет прав на эту операцию",
  "xFB": "Нефискальный чек не открыт",
  "xFC": "Чек не открыт",
  "xFD": "Нефискальный чек уже открыт",
  "xFE": "Чек уже открыт",
  "xFF": "Переполнение ленты"
  }


     */
//SELECT t1.id, t1.local_id, (t1.client_money_cash+t1.client_money_card) - sum(t2.quantity * t2.dish_price) as diff FROM `orders` t1 left join order_dish t2 on t1.id = t2.order_id WHERE t1.created_at > DATE(NOW()) and t1.status = 4 and t1.cash_type = 0 group by t1.id having ABS(diff) >= 0.01
    const FISCAL_START = "5330303030003000303400";

    const REPORT_X = 2;
    const REPORT_Z = 1;

    const FISCAL_SELL = 'SELL';
    const FISCAL_RETURN = 'RTRN';

    const MODEL_GEPARD = 0;
    const MODEL_SENTO = 1;
    const MODEL_TITAN = 2;

    private $port;

    public function __construct($port = '/dev/ttyS0', $speed = 3, $timeout = 3)
    {

        $this->port = $port;
    }

    public function controlSumm($string)
    {
        $hex_bytes = str_split($string, 2);
        $dec_array = array_map(function ($s) {
            return hexdec($s);
        }, $hex_bytes);

        $summ = array_sum($dec_array);

        return dechex($summ);
    }

    public function sendDirectSum($sum, $cashtype)
    {
        $command = 'python ../python/direct.py ' . $cashtype . ' ' . $sum;
        //dump($command);die();
        exec($command);
    }

    public function printCheck(Order $order, $type = self::FISCAL_SELL, $model = self::MODEL_GEPARD, $logger)
    {

        $ch = curl_init("http://" . $order->getShop()->getFiscalPort() . "/cgi/chk");

        $titanOrder = [
            "F" => []
        ];

        $discountSum = 0;
        foreach ($order->getDishes() as $__dish) {
            if (($__dish->getParentDish() !== null) && ($__dish->getDishPrice() == 0)) continue;
            if ($__dish->getQuantity() == 0) continue;

            if ($__dish->getFullPrice() < 0) {
                $discountSum += abs($__dish->getFullPrice() * $__dish->getQuantity());
            } else {
                $titanOrder["F"][] = [
                    "S" => [
                        "qty" => $__dish->getQuantity(),
                        "code" => $__dish->getDish()->getId(),
                        "price" => ($__dish->getFullPrice()),
                        "name" => $__dish->getDish()->getName(),
                        "tax" => 0,
                    ]
                ];
            }

            if ($__dish->getDishDiscount() > 0) {
                $titanOrder["F"][] = [
                    "D" => [
                        "sum" => -$__dish->getDiscountSum(),
                        "all" => 1,
                    ]
                ];
//                $discountSum += abs($__dish->getDiscountSum());
            }
        }

        $discountSum = abs($discountSum);
        if ($discountSum > 0) {
            $titanOrder["F"][] = [
                "D" => [
                    "sum" => -$discountSum,
                    "all" => 1,
                ]
            ];
        }

        if ($order->getClientMoneyCash() > 0) {
            $titanOrder["F"][] = [
                "P" => [
                    "sum" => $order->getClientMoneyCash(),
                    "no" => 1,
                ]
            ];
        }
        if ($order->getClientMoneyCard() > 0) {
            $titanOrder["F"][] = [
                "P" => [
                    "sum" => $order->getClientMoneyCard(),
                    "no" => 4,
                ]
            ];
        }

        $jsonOrder = json_encode($titanOrder, JSON_UNESCAPED_UNICODE);

        $logger->info('sent ' . $jsonOrder);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonOrder);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonOrder)
            ]
        );

        if ($response = curl_exec($ch)) {
            $logger->info('recv ' . $response);
            curl_close($ch);
            $res = json_decode($response);
            return !property_exists($res, 'err');
        } else {
            curl_close($ch);
            $logger->info('curl ' . curl_error($ch));
            return false;
        }
    }

    public function getSums($port, $model = self::MODEL_TITAN)
    {
        if ($model == self::MODEL_TITAN) {
            $ch = curl_init("http://" . $port . "/cgi/chk");
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $res = json_decode(curl_exec($ch), true);

            $money = [
                'cash' => 0,
                'card' => 0,
            ];

            if ($res) {
                $hasZ1 = false;
                foreach ($res as $row) {
                    if (isset($row['Z1'])) {
                        $hasZ1 = true;
                    }
                }


                foreach ($res as $row) {
                    if ($hasZ1) {
                        if (!isset($row['Z1'])) {
                            continue;
                        } else {
                            $hasZ1 = false;
                        }
                    }

                    if (isset($row['F'])) {
                        foreach ($row['F'] as $_f) {
                            if (isset($_f['P'])) {
                                if ($_f['P']['no'] == 1) $money['cash'] += $_f['P']['sum'];
                                if ($_f['P']['no'] == 4) $money['card'] += $_f['P']['sum'];

                            }
                        }
                    }
                }
            }

            return $money;
        }
    }

    public function report($type, $model = self::MODEL_GEPARD, $port)
    {

        if ($model == self::MODEL_GEPARD) {
            $command = 'python ../python/report.py ' . $type;
            //dump($command);die();
            exec($command);
        } elseif ($model == self::MODEL_TITAN) {
            $url = "http://" . $port . "/cgi/proc/printreport?" . ($type == Fiscal::REPORT_X ? '10' : '0');
            $ch = curl_init($url);

            $request = [];

            $jsonOrder = json_encode($request, JSON_UNESCAPED_UNICODE);


            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonOrder);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($jsonOrder))
            );


            $response = curl_exec($ch);;
            curl_close($ch);
        }
    }


    private
    function a2b_hex($str)
    {

        $hex_bytes = str_split($str, 2);
        $code_array = array_map(function ($s) {
            return hexdec($s);
        }, $hex_bytes);

        $char_array = array_map(function ($s) {
            return chr($s);
        }, $code_array);

        return implode($char_array);
    }
}