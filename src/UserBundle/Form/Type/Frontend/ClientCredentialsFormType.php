<?php

namespace UserBundle\Form\Type\Frontend;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use RestaurantBundle\Entity\Shop;

class ClientCredentialsFormType extends AbstractType
{
    /**
     * @param string $class The User class name
     */
    public function __construct()
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("lastName", TextType::class, [
                'label' => 'Фамилия',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Фамилия (на латинице)',
                    'class' => '',
                    'pattern' => '^[a-zA-Z-\s]+$'
                ]
            ])
            ->add("firstName", TextType::class, [
                'label' => 'Имя',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Имя (на латинице)',
                    'class' => '',
                    'pattern' => '^[a-zA-Z-\s]+$',
                ]
            ])
            ->add("surName", TextType::class, [
                'label' => 'Отчество',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Отчество',
                    'class' => ''
                ]
            ])
            ->add('birthDate', DateType::class, [
                'label' => 'Дата рождения',
                'required' => true,
                'format' => 'yyyy-MM-dd',
                'widget' => 'single_text',
                'years' => range(date('Y') - 100, date('Y')),
                'attr' => [
                    'placeholder' => 'Дата рождения',
                    'class' => ' datepicker--js picker__input'
                ]
            ])
            ->add("gender", ChoiceType::class, [
                'label' => 'Пол',
                'attr' => [
                    'placeholder' => 'Пол',
                    'class' => ''
                ],
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Пол' => null,
                    'Мужской' => 1,
                    'Женский' => 0,
                ],
                'choice_attr' => function ($key, $val, $index) {
                    return $key === null ? ['disabled' => 'disabled'] : [];
                },
            ])
            ->add("familyStatus", TextType::class, [
                'label' => 'Семейное положение',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Семейное положение',
                    'class' => ''
                ]
            ])
            ->add("hasChildren", ChoiceType::class, [
                'label' => 'Есть ли у Вас дети',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Есть ли у Вас дети',
                    'class' => ''
                ],
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Есть ли у Вас дети' => null,
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'choice_attr' => function ($key, $val, $index) {
                    return $key === null ? ['disabled' => 'disabled'] : [];
                },
            ])
            ->add("phone", TextType::class, [
                'label' => 'Мобильный телефон',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Мобильный телефон',
                    'class' => ''
                ]
            ])
            ->add("address", TextType::class, [
                'label' => 'Адрес',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Адрес',
                    'class' => ''
                ]
            ])
            ->add("email", EmailType::class, [
                'label' => 'Контактный Email',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Контактный Email',
                    'class' => ''
                ]
            ])
            ->add("useDelivery", ChoiceType::class, [
                'choice_label' => null,
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Пользуетесь ли Вы доставкой' => null,
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'choice_attr' => function ($key, $val, $index) {
                    return $key === null ? ['disabled' => 'disabled'] : [];
                },
                'attr' => [
                    'placeholder' => 'Пользуетесь ли Вы доставкой',
                    'class' => ''
                ]
            ])
            ->add("visitPeriod", ChoiceType::class, [
                'label' => 'Как часто посещаете Заведения ITALSERVICE GROUP',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Как часто посещаете Заведений ITALSERVICE GROUP' => null,
                    'очень часто (несколько раз в неделю)' => 0,
                    'часто (несколько раз в месяц)' => 1,
                    'редко (несколько раз в пол года)' => 2,
                    'очень редко (1 раз в год)' => 3,
                ],
                'choice_attr' => function ($key, $val, $index) {
                    return $key === null ? ['disabled' => 'disabled'] : [];
                },
                'attr' => [
                    'placeholder' => 'Как часто посещаете Заведения ITALSERVICE GROUP',
                    'class' => ''
                ],
            ])
            ->add("cardPickupShop", EntityType::class, [
                'class' => Shop::class,
                'data_class' => Shop::class,
                'query_builder' => function (EntityRepository $qb) {
                    return $qb->createQueryBuilder('s')
                        ->where('s.id in (:ids)')
                        ->setParameters([
                            'ids' => [4, 5, 8],
                        ]);
                },
                'choice_label' => function (Shop $shop) {
                    return $shop->getName() . " " . $shop->getAddress();
                },
                'label' => 'В каком заведении хотите получить свою дисконтную накопительную карту',
                'attr' => [
                    'placeholder' => 'В каком заведении хотите получить свою дисконтную накопительную карту',
                    'class' => ''
                ]
            ])
            ->add('mailingAgreement', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'label' => 'Согласен(а) получать информацию о действующих скидках и акциях по e-mail',
                'attr' => [
                    'placeholder' => 'Согласен(а) получать информацию о действующих скидках и акциях по e-mail'
                ]
            ])
            ->add('processingAgreement', CheckboxType::class, [
                'mapped' => false,
                'required' => true,
                'label' => 'Согласен(а) на обработку персональных данных и получение от ООО «Лаилик»/ ИООО «Итал-Сервис» SMS-оповещений',
                'attr' => [
                    'placeholder' => 'Согласен(а) на обработку персональных данных и получение от ООО «Лаилик»/ ИООО «Итал-Сервис» SMS-оповещений'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Готово',
                'attr' => [
                    'class' => 'btn-huge btn-huge--booking btn-booking-submit--js'
                ]
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\ClientCredentials',
        ));
    }


    public function getBlockPrefix()
    {
        return 'client';
    }
}