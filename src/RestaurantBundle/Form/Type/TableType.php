<?php

namespace RestaurantBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class TableType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Имя",
                'attr' => [
                    'placeholder' => 'Новый стол',
                    'class' => 'md-input table_name',
                ]
            ])
            ->add('metaCoordinates', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'table_coords',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RestaurantBundle\Entity\ShopPlaceTable',
        ));
    }
}