from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Activation, Dropout, Flatten, Dense
from tensorflow.python.keras.preprocessing import image
from tensorflow.python.keras.applications import VGG16
from tensorflow.python.keras.optimizers import Adam
from keras.models import model_from_json
import numpy as np
import os
import yaml
import MySQLdb
import MySQLdb.cursors
import json
import sys

with open('../../app/config/parameters.yml', 'r') as f:
    parameters = yaml.load(f)['parameters']

num_classes = 22

conn = MySQLdb.connect(host="134.0.119.169", user="venezia", passwd="9WhsFYfbQjdQz7uR", db="venezia_new", cursorclass=MySQLdb.cursors.DictCursor, charset='utf8', use_unicode=True)
cursor = conn.cursor()
cursor.execute("SET NAMES utf8")

vgg16_net = VGG16(weights='imagenet', include_top=False, input_shape=(224, 224, 3))

vgg16_net.trainable = True
trainable = False
for layer in vgg16_net.layers:
    if layer.name == 'block5_conv1':
        trainable = True
    layer.trainable = trainable

model = Sequential()
model.add(vgg16_net)
model.add(Flatten())
model.add(Dense(768))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes))
model.add(Activation('sigmoid'))
model.summary()
model.compile(loss='categorical_crossentropy',
              optimizer=Adam(),
              metrics=['accuracy'])

# json_file = open('pizza_modelvgg16.json', 'r')
# loaded_model_json = json_file.read()
# json_file.close()
# model = model_from_json(loaded_model_json)
model.load_weights('model_data/pizza_768_9031.hdf5')

imgDir = "../../web/uploads/cropped/"
directory = os.listdir(os.fsencode(imgDir))
files_count = len(directory)
i = 1
for file in directory:
    filename = os.fsdecode(file)
    print(filename, str(i) + "/" + str(files_count), str(round(i * 100 / files_count, 2)) + "%")
    i += 1
    if filename.endswith(".jpg") or filename.endswith(".png"):
        img = image.load_img(imgDir + filename, target_size=(224, 224))
        img_vector = image.img_to_array(img)
        img_vector /= 255
        img_vector = np.expand_dims(img_vector, axis=0)

        predict = model.predict(img_vector)
        classes = ['Нет', 'Пицца Ассортита', 'Пицца Белорусская (закрытая)', 'Пицца Боскайола', 'Пицца Гавайская', 'Пицца Деревенская', 'Пицца Дьявола с маслинами', 'Пицца Каприччёза', 'Пицца Карбонара', 'Пицца Маргарита', 'Пицца Маринара', 'Пицца Ностромо', 'Пицца Прошутто', 'Пицца Три салями', 'Пицца Голоза', 'Пицца Кальцоне (закрытая)', 'Пицца с курицей и ананасом', 'Пицца Дзингара', 'Пицца Дьявола', 'Пицца Прошутто с грибами', 'Пицца с салями и луком', 'Пицца с курицей и грибами']
        result = dict(zip(classes, predict[0].tolist()))
        sorted_result = sorted(result.items(), key=lambda x: x[1], reverse=True)
        print(sorted_result[0])
        query = "UPDATE `order_dish_photo` SET `predict` = '%s' WHERE `photo_path`='%s'" % (str(json.dumps(result, ensure_ascii=False)), filename)
        print(query)
        a = cursor.execute(query)
        conn.commit()