<?php

namespace ServiceBundle\Services;

class Guardsaas
{

    public function __construct()
    {

    }

    public function getLastEvents($lastEventId = null)
    {
        $ch = curl_init();

        $request = [
            'limit' => 9999,
            'date' => (new \DateTime())->format("Y-m-d"),
//            'dateFrom' => '2018-12-25',
            'order' => 'asc',
            'events' => '16,17',
            'appkey' => 'f2mxWNdMXg0WJpmV6jucVuZ4NyAsSF5s',
        ];

        if ($lastEventId)
            $request['idFrom'] = $lastEventId;

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://app.guardsaas.com/reports/events/export');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

        $report = curl_exec($ch);


        if (curl_errno($ch)) {
            curl_close($ch);
            return false;
        } else {
            curl_close($ch);
            return json_decode($report);
        }
    }
}