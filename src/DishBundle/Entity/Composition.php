<?php


namespace DishBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\CompositionRepository")
 */
class Composition
{

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isDeleted = false;
        $this->ingredients = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $createdBy;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="descriptions", type="json", nullable=true)
     */
    private $descriptions = [
        'technology' => "",
        'serve' => "",
        'visual' => "",
        'color' => "",
        'taste' => "",
        'flavour' => "",
        'body' => "",
        'time' => "",
    ];

    /**
     * @ORM\Column(name="mass_proteins", type="string", nullable=true)
     */
    private $massProteins;

    /**
     * @ORM\Column(name="mass_fats", type="string", nullable=true)
     */
    private $massFats;

    /**
     * @ORM\Column(name="mass_carbohydrates", type="string", nullable=true)
     */
    private $massCarbohydrates;

    /**
     * @ORM\Column(name="energy_value_kk", type="string", nullable=true)
     */
    private $energyValueKK;

    /**
     * @ORM\Column(name="energy_value_dj", type="string", nullable=true)
     */
    private $energyValueDJ;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\CompositionIngredient", mappedBy="composition", orphanRemoval=true)
     */
    private $ingredients;

    /**
     * @ORM\Column(name="output_semifinished_mass", type="string", nullable=true)
     */
    private $outputSemifinishedMass;

    /**
     * @ORM\Column(name="output_finished_mass", type="string", nullable=true)
     */
    private $outputFinishedMass;

    /**
     * @ORM\Column(name="time", type="string", nullable=true)
     */
    private $time;

    /**
     * @ORM\Column(name="old_id", type="string", nullable=true)
     */
    private $oldId;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", options={"default":"0"}, nullable=true)
     */
    private $isDeleted;

    /**
     * @ORM\Column(name="should_be_validated", type="boolean", options={"default":"0"}, nullable=true)
     */
    private $shouldBeValidated;

    /**
     * @ORM\Column(name="track_in_warehouse", type="boolean", options={"default":"0"}, nullable=true)
     */
    private $trackInWarehouse;

    /**
     * @ORM\Column(name="show_in_request", type="boolean", options={"default":"0"},)
    **/
    private $showInRequest;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Composition
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return Composition
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Composition
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescriptions()
    {

        return $this->descriptions;
    }

    /**
     * @param mixed $descriptions
     * @return Composition
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getMassProteins()
    {
        return $this->massProteins;
    }

    /**
     * @param mixed $massProteins
     * @return Composition
     */
    public function setMassProteins($massProteins)
    {
        $this->massProteins = $massProteins;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMassFats()
    {
        return $this->massFats;
    }

    /**
     * @param mixed $massFats
     * @return Composition
     */
    public function setMassFats($massFats)
    {
        $this->massFats = $massFats;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMassCarbohydrates()
    {
        return $this->massCarbohydrates;
    }

    /**
     * @param mixed $massCarbohydrates
     * @return Composition
     */
    public function setMassCarbohydrates($massCarbohydrates)
    {
        $this->massCarbohydrates = $massCarbohydrates;
        return $this;
    }

    /**
     * @return CompositionIngredient[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @param mixed $ingredients
     * @return Composition
     */
    public function addIngredient(CompositionIngredient $ingredient)
    {
        $this->ingredients[] = $ingredient;
        $ingredient->setComposition($this);
        return $this;
    }

    public function removeIngredient(CompositionIngredient $ingredient)
    {
        $this->ingredients->removeElement($ingredient);
        $ingredient->setComposition(null);
    }

    public function hasIngredient(CompositionIngredient $ingredient)
    {
        return $this->ingredients->contains($ingredient);
    }

    /**
     * @param mixed $ingredients
     * @return Composition
     */
    public function setIngredients($ingredients)
    {
        return $this->ingredients = $ingredients;
    }


    /**
     * @return mixed
     */
    public function getOutputSemifinishedMass()
    {
        return $this->outputSemifinishedMass;
    }

    /**
     * @param mixed $outputSemifinishedMass
     * @return Composition
     */
    public function setOutputSemifinishedMass($outputSemifinishedMass)
    {
        $this->outputSemifinishedMass = $outputSemifinishedMass;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutputFinishedMass()
    {
        return $this->outputFinishedMass;
    }

    /**
     * @param mixed $outputFinishedMass
     * @return Composition
     */
    public function setOutputFinishedMass($outputFinishedMass)
    {
        $this->outputFinishedMass = $outputFinishedMass;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOldId()
    {
        return $this->oldId;
    }

    /**
     * @param mixed $oldId
     * @return Composition
     */
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getEnergyValueKK()
    {
        return $this->energyValueKK;
    }

    /**
     * @param mixed $energyValueKK
     * @return Composition
     */
    public function setEnergyValueKK($energyValueKK)
    {
        $this->energyValueKK = $energyValueKK;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnergyValueDJ()
    {
        return $this->energyValueDJ;
    }

    /**
     * @param mixed $energyValueDJ
     * @return Composition
     */
    public function setEnergyValueDJ($energyValueDJ)
    {
        $this->energyValueDJ = $energyValueDJ;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param mixed $isDeleted
     * @return Composition
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getShouldBeValidated()
    {
        return $this->shouldBeValidated;
    }

    /**
     * @param mixed $shouldBeValidated
     * @return Composition
     */
    public function setShouldBeValidated($shouldBeValidated)
    {
        $this->shouldBeValidated = $shouldBeValidated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrackInWarehouse()
    {
        return $this->trackInWarehouse;
    }

    /**
     * @param mixed $trackInWarehouse
     * @return Composition
     */
    public function setTrackInWarehouse($trackInWarehouse)
    {
        $this->trackInWarehouse = $trackInWarehouse;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     * @return Composition
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

}