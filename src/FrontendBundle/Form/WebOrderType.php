<?php

namespace FrontendBundle\Form;

use Doctrine\ORM\EntityRepository;
use FrontendBundle\Entity\WebOrder;
use DishBundle\Entity\Dish;
use RestaurantBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use OrderBundle\Entity\Order;
use TerminalBundle\Form\Type\OrderDishType;

class WebOrderType extends AbstractType
{


    private $takeaway_shops;


    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('dishes', CollectionType::class, [
                'label' => 'Блюда',
                'entry_type' => WebOrderDishType::class,
                'by_reference' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => false,
                'required' => false,
            ])
            ->add('phone', TextType::class, [
                'label' => 'Телефон',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Телефон',
                ],
            ])
            ->add('cashType', ChoiceType::class, [
                'label' => 'Метод оплаты',
                'required' => true,
                'data' => 1,
                'choices' => [
//                    'Онлайн' => 1,
                    ($options["type"] == WebOrder::TYPE_PICKUP ? 'На месте' : 'Наличными курьеру') => 0,
                ],
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Комментарий',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Комментарии к заказу: пиццу максимально острую, Андрей, сдачу с 100',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Оформить',
                'attr' => [
                    'class' => 'md-btn md-btn-block md-btn-primary disabled',
                ]
            ])
            ->add('pickupTime', TextType::class, [
                'label' => 'Время',
                'required' => true,
                'attr' => [
                    'placeholder' => '13:25',
                    'class' => 'timepicker',
                ],
            ])
        ;

//var_dump($options["pickupTime"]);
        if ($options["type"] == WebOrder::TYPE_PICKUP) {
            $builder
                ->add('pickupShop', EntityType::class, [
                    'label' => 'Забрать в',
                    'disabled' => true,
                    'class' => Shop::class,
                    'choice_label' => function (Shop $shop, $key, $index) {
                        return ($shop->getName() . " (" . $shop->getAddress() . ", " .
                            $shop->getTodayTimes()["start"] . " - " . $shop->getTodayTimes()["end"] . ")");
                    },
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('s')
                            ->where('s.id in (:ids)')
                            ->setParameters([
                                'ids' => $options["takeaway_shops"],
                            ]);
                    },
                    'choice_attr' => function (Shop $shop, $key, $value) use ($options) {
                        $todayWorkTime = $shop->getTodayTimes();
                        $timeStart = (new \DateTime($todayWorkTime["start"]))->modify("+30 min");
                        $timeEnd = (new \DateTime($todayWorkTime["end"]))->modify("-30 min");
                        return [
                            'data-time-start' => $options["pickupTime"],
                            'data-time-end' => $timeEnd->format('H:i'),
                        ];
                    },
                    'multiple' => false,
                    'expanded' => false,
                ])
              ;
        } elseif ($options["type"] == WebOrder::TYPE_DELIVERY) {
            $builder->add('address', TextType::class, [
                'label' => 'Адрес',
                'attr' => [
                    'placeholder' => 'Адрес: БЦ "Цельсий" 3 эт. офис 321',
                ],
            ]);
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WebOrder::class,
            'takeaway_shops' => null,
            'type' => WebOrder::TYPE_PICKUP,
            'pickupTime' => "",
        ]);
    }

    public function getBlockPrefix()
    {
        return 'order';
    }
}
