<?php

namespace DishBundle\Controller;

use DishBundle\Entity\Ingredient;
use DishBundle\Form\Type\IngredientType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/admin")
 */
class IngredientController extends Controller
{

    /**
     * @Route("/ingredient/list", name="admin_ingredient_list")
     */
    public function ingredientListAction(Request $request)
    {


        $ingredient = new Ingredient();
        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($ingredient);
                $em->flush();
                return $this->redirectToRoute('admin_ingredient_list');
            }
        }

        $ingredients = $this->getDoctrine()->getRepository('DishBundle:Ingredient')->findBy([
            'isDeleted' => false,
        ]);

        return $this->render("DishBundle:Ingredient:list.html.twig", [
            'ingredients' => $ingredients,
            'ingredientForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ingredient/{id}/edit", name="admin_ingredient_edit")
     */
    public function ingredientEditAction(Request $request, Ingredient $ingredient)
    {
        if (!$ingredient)
            throw new NotFoundHttpException();


        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($form->get("delete")->isClicked()) {
                    $ingredient->setIsDeleted(true);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($ingredient);
                $em->flush();
            }
        }

        $ingredients = $this->getDoctrine()->getRepository('DishBundle:Ingredient')->findBy([
            'isDeleted' => false,
        ]);


        return $this->render("DishBundle:Ingredient:list.html.twig", [
            'ingredients' => $ingredients,
            'form_title' => 'Редактирование',
            'ingredientForm' => $form->createView(),
        ]);
    }


}
