<?php

namespace RestaurantBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\Shop;

class PromoRepository extends EntityRepository
{

    /**
     * @param Shop $shop
     * @return PromoCard[]
     */
    public function findCurrentActive(Shop $shop)
    {
        $promos = $this->createQueryBuilder('p')
            ->where('p.shop = :shop')
            ->setParameters([
                'shop' => $shop->getId()
            ])
            ->getQuery()
            ->getResult();

        //    ->getQuery()
//        dump($promos);die();
        //    ->getResult();

        return $promos;
    }
}