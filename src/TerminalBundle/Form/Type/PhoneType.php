<?php

namespace TerminalBundle\Form\Type;

use FrontendBundle\Entity\WebOrder;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Entity\ShopPlaceTable;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

use OrderBundle\Entity\Order;
use Doctrine\ORM\EntityRepository;
use UserBundle\Entity\User;

class PhoneType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('comment', TextType::class, [
                'label' => "Имя",
            ])
            ->add('phone', TextType::class, [
                'label' => "Телефон",
                'data' => "+375",
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Добавить'
            ]);

    }

    public function getDefaultOptions(array $options)
    {
        return array([
            'data_class' => WebOrder::class,
            'shop' => null,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => WebOrder::class,
            'shop' => null,
        ));
    }


}