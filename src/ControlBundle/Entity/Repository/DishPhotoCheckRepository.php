<?php

namespace ControlBundle\Entity\Repository;


use ControlBundle\Entity\DishPhoto;
use Doctrine\ORM\EntityRepository;
use OrderBundle\Entity\OrderDish;

class DishPhotoCheckRepository extends EntityRepository
{

    public function findUserStats($dateStart, $dateEnd)
    {
        return $this->createQueryBuilder('dpc')
            ->select("COUNT(dpc) as uCount")
            ->addSelect('u.name as name')
            ->leftJoin('dpc.user', 'u')
            ->where("dpc.date >= :dateStart")
            ->andWhere("dpc.date <= :dateEnd")
            ->setParameters([
                'dateStart' => $dateStart,
                'dateEnd' => $dateEnd,
            ])
            ->groupBy('u.id')
            ->orderBy('uCount', "DESC")
            ->getQuery()
            ->getResult();
    }


    public function findPositiveStatByUser($dateStart = null, $dateEnd = null)
    {
        $stat = $this->createQueryBuilder('c')
            ->select('COUNT(c.id) as uCount')
            ->addSelect('u.name as uName')
            ->where('vd.id = d.id')
            ->leftJoin('c.user', 'u')
            ->leftJoin('c.dishPhoto', 'dp')
            ->leftJoin('c.dish', 'd')
            ->leftJoin('dp.validDish', 'vd')
            ->groupBy('u.id');

        if ($dateStart) {
            $stat->andWhere("dp.date >= :dateStart")->setParameter('dateStart', new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $stat->andWhere("dp.date <= :dateEnd")->setParameter('dateEnd', new \DateTime($dateEnd));
        }

        //dump($stat->getQuery());die();

        return $stat->getQuery()->getResult();
    }

    public function findStatByUser($dateStart = null, $dateEnd = null)
    {
        $stat = $this->createQueryBuilder('c')
            ->addSelect('COUNT(c.id) as uCount')
            ->addSelect('u.name as uName')
            ->addSelect('AVG(c.rate) as avgRate')
            ->leftJoin('c.user', 'u')
            ->leftJoin('c.dishPhoto', 'dp')
            ->groupBy('u.id');

        if ($dateStart) {
            $stat->andWhere("dp.date >= :dateStart")->setParameter('dateStart', new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $stat->andWhere("dp.date <= :dateEnd")->setParameter('dateEnd', new \DateTime($dateEnd));
        }

        return $stat->getQuery()
            ->getResult();
    }

    public function findStatByCook($dateStart = null, $dateEnd = null)
    {
        $stat = $this->createQueryBuilder('c')
            ->select('AVG(c.rate) as avgRate')
            ->addSelect('COUNT(c) as rateCount')
            ->addSelect('COUNT(DISTINCT(dp.id)) as photoCount')
            ->addSelect('pu.name as uName')
            ->leftJoin('c.dishPhoto', 'dp')
            ->leftJoin('dp.user', 'pu')
            ->orderBy('avgRate' ,"DESC")
            ->groupBy('pu.id')
        ;

        if ($dateStart) {
            $stat->andWhere("dp.date >= :dateStart")->setParameter('dateStart', new \DateTime($dateStart));
        }
        if ($dateEnd) {
            $stat->andWhere("dp.date <= :dateEnd")->setParameter('dateEnd', new \DateTime($dateEnd));
        }

        return $stat
            ->getQuery()
            ->getResult();
    }

}
