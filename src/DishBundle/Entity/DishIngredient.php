<?php

namespace DishBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class DishIngredients
 * @package DishBundle\Entity
 * @ORM\Table(name="dish_ingredients")
 * @ORM\Entity()
 */
class DishIngredient
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish", inversedBy="ingredients")
     */
    private $dish;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Ingredient", inversedBy="dishes")
     */
    private $ingredient;

    /**
     * @ORM\Column(name="quantity", type="float", nullable=true)
     */
    private $quantity;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return DishIngredients
     */
    public function setQuantity($quantity)
    {
        $this->quantity = str_replace(',', '.', $quantity);

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     * @return DishIngredient
     */
    public function setDish(\DishBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \DishBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * Set ingredient
     *
     * @param \DishBundle\Entity\Ingredient $ingredient
     *
     * @return DishIngredient
     */
    public function setIngredient(\DishBundle\Entity\Ingredient $ingredient = null)
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    /**
     * Get ingredient
     *
     * @return \DishBundle\Entity\Ingredient
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }
}
