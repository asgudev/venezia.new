<?php

namespace RestaurantBundle\Twig;


class ArraySumFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('array_sum', array($this, 'arraySum')),
        );
    }

    public function arraySum($input)
    {
        return array_sum($input);
    }

    public function getName()
    {
        return 'array_sum';
    }

}