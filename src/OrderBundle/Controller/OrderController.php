<?php

namespace OrderBundle\Controller;

use OrderBundle\Entity\Order;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Form\Type\OrderFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @package RestaurantBundle\Controller
 * @Route("/admin")
 */
class OrderController extends Controller
{

    /**
     * @Route("/order/list", name="order_list")
     */
    public function ordersListAction(Request $request)
    {

        $seacrh_form = $this->createForm(OrderFilterType::class, [
            'method' => 'GET',
        ]);

        $seacrh_form->handleRequest($request);

        if ($request->isMethod("POST")) {
            if ($request->get("order_filter")["id"]) {
                $id = $request->get("order_filter")["id"];
                $shop = $request->get("order_filter")["shop"];
                $orders = $this->getDoctrine()->getRepository('OrderBundle:Order')->findByNumber($id, $shop);
            } else {
                $datestart = $request->get("order_filter")["datestart"];
                $dateend = $request->get("order_filter")["dateend"];
                $shop = $request->get("order_filter")["shop"];
                $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findAllInRange($datestart, $dateend, $shop);

            }
            //dump($orders); die();
        } else {
            $orders = $this->getDoctrine()->getRepository("OrderBundle:Order")->findAllToday($this->getUser()->getShop());
        }
        $orderChanges = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findAllByDate(1);
        $orderDeletions = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findDeletionByDate($this->getUser()->getShop());


        return $this->render("@Restaurant/Orders/orderList.html.twig", [
            'orders' => $orders,
            'changes' => $orderChanges,
            'deletions' => $orderDeletions,
            'seacrh_form' => $seacrh_form->createView(),
        ]);
    }

    /**
     * @Route("/order/fix/list", name="order_fix_list")
     */
    public function ordersFixListAction(Request $request)
    {
        $userShops = $this->getUser()->getAdminShops()->toArray();

        $orders = [];

        foreach ($userShops as $shop) {
            $previousReports = $this->getDoctrine()->getRepository('TerminalBundle:DailyReport')->findLastReport($shop);

            $previousReport = count($previousReports) > 0 ? $previousReports[0] : null;

            if ($previousReport) {
                $semiClosedOrders = $this->getDoctrine()->getRepository(Order::class)->findSemiClosedOrders($previousReport);

                foreach ($semiClosedOrders as $__order) {
                    $orders[] = [
                        'order' => $__order,
                        'changes' => $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findByOrder($__order->getCopyOf()),
                    ];
                }
            }
        }

        return $this->render("@Order/Default/orderFix.html.twig", [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/order/{id}/resolve", name="order_resolve")
     */
    public function ordersResolveAction(Request $request, Order $order)
    {
        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($order->getShop());

        $url = "http://" . $shop->getVpnServerIP() . $this->generateUrl('api_order_resolve', [
                'id' => $order->getId(),
            ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: ' . $request->headers->get("authorization")]);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        $res = curl_exec($ch);

        $data = json_decode($res, true);

        $this->get('user.logger')->info("RESOLVE " . $this->getUser()->getName() . " " . $order->getShop()->getId() . " " . $order->getId() . " " . $data['success']);

        return new JsonResponse([
            'success' => $data ? $data['success'] : false,
        ]);
    }


    /**
     * @Route("/order/{id}/show", name="admin_order_show", requirements={
     *     "id": "\d+"
     * })
     */
    public function orderShowAction(Request $request, Order $order)
    {
        if (!$order)
            throw new NotFoundHttpException();

        $changes = $this->getDoctrine()->getRepository('OrderBundle:OrderChange')->findByOrder($order);
        $photos = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->findByOrder($order);


        return $this->render("@Restaurant/Orders/orderShow.html.twig", [
            'order' => $order,
            'changes' => $changes,
            'photos' => $photos,
        ]);
    }
}
