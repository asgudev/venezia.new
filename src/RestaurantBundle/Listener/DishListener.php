<?php

namespace RestaurantBundle\Listener;

use Doctrine\ORM\EntityManager;
use RestaurantBundle\Events\DishEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class DishListener implements EventSubscriberInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager;
     */
    protected $em;

    /**
     * @var ContainerInterface
     */
    protected $container;


    protected $environment;

    public function __construct(EntityManager $em, ContainerInterface $container, $environment)
    {
        $this->em = $em;
        $this->container = $container;
        $this->environment = $environment;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            DishEvent::TABLE_UPDATE => 'updateTable',
        ];
    }

    public function updateTable(DishEvent $event)
    {

        $node = "https://admin.venezia.by:3000";

        if ($this->environment == "dev") {
            $url = $node . "/updateTable/dev";
        } else {
            $url = $node . "/updateTable";
        }

        $data = json_encode([
            'id' => $event->getDish()->getId(),
            'type' => $event->getType(),
            'data' => $event->getData(),
        ]);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)
        ]);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($curl);
        curl_close($curl);
    }

}
