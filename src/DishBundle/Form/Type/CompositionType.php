<?php


namespace DishBundle\Form\Type;


use DishBundle\Entity\Composition;
use DishBundle\Entity\CompositionIngredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompositionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", TextType::class, [
                'label' => "Название",
                'required' => true,
            ])
            ->add("technologyDescription", TextareaType::class, [
                'label' => "Технология приготовления",
                'property_path' => "descriptions[technology]",
                'required' => false,
            ])
            ->add("time", TextType::class, [
                'label' => "Время приготовления, мин",
                'required' => false,
            ])
            ->add("serveDescription", TextareaType::class, [
                'label' => "Правила оформления, подачи",
                'property_path' => "descriptions[serve]",
                'required' => false,
            ])
            ->add("visualDescription", TextareaType::class, [
                'label' => "Внешний вид",
                'property_path' => "descriptions[visual]",
                'required' => false,
            ])
            ->add("colorDescription", TextareaType::class, [
                'label' => "Цвет",
                'property_path' => "descriptions[color]",
                'required' => false,
            ])
            ->add("tasteDescription", TextareaType::class, [
                'label' => "Вкус",
                'property_path' => "descriptions[taste]",
                'required' => false,
            ])
            ->add("flavourDescription", TextareaType::class, [
                'label' => "Запах",
                'property_path' => "descriptions[flavour]",
                'required' => false,
            ])
            ->add("bodyDescription", TextareaType::class, [
                'label' => "Консистенция",
                'property_path' => "descriptions[body]",
                'required' => false,
            ])
            ->add("timeDescription", TextareaType::class, [
                'label' => "Срок годности и условия хранения",
                'property_path' => "descriptions[time]",
                'required' => false,
            ])

            ->add("ingredients", CollectionType::class, [
                "label" => "",
                "entry_type" => CompositionIngredientType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "delete_empty" => true,
                "by_reference" => false,

            ])
            ->add("massProteins", TextType::class, [
                'label' => 'Белки',
                'required' => false,
            ])
            ->add("massFats", TextType::class, [
                'label' => 'Жиры',
                'required' => false,
            ])
            ->add("massCarbohydrates", TextType::class, [
                'label' => 'Углеводы',
                'required' => false,
            ])
            ->add("energyValueDJ", TextType::class, [
                'label' => 'Энергетическая ценность, ДЖ',
                'required' => false,
            ])
            ->add("energyValueKK", TextType::class, [
                'label' => 'Энергетическая ценность, KK',
                'required' => false,
            ])
            ->add("outputSemifinishedMass", TextType::class, [
                'label' => 'Масса полуфабрикат',
                'required' => false,
            ])
            ->add("outputFinishedMass", TextType::class, [
                'label' => 'Выход готовой продукции',
                'required' => false,
            ])
            ->add("shouldBeValidated", CheckboxType::class, [
                'label' => 'Блокирует закрытие смены',
                'required' => false,
            ])
            ->add("trackInWarehouse", CheckboxType::class, [
                'label' => 'Отслеживать в поставке продуктов',
                'required' => false,
            ])
            ->add("submit", SubmitType::class, [
                'label' => 'Сохранить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Composition::class,
        ));
    }
}