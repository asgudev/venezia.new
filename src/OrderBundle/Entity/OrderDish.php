<?php

namespace OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use RestaurantBundle\Entity\Shop;
use UserBundle\Entity\User;

/**
 * Dish
 *
 * @ORM\Table(name="order_dish")
 * @ORM\Entity(repositoryClass="OrderBundle\Entity\Repository\OrderDishRepository")
 */
class OrderDish
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderBundle\Entity\Order", inversedBy="dishes")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish", cascade={"persist"})
     */
    private $dish;

    /**
     * @ORM\Column(name="quantity", type="float", nullable=false)
     */
    private $quantity;

    /**
     * @ORM\Column(name="dish_price", type="float", nullable=false)
     */
    private $dishPrice;

    /**
     * @ORM\Column(name="dish_discount", type="float", nullable=false)
     */
    private $dishDiscount;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $delegate;

    /**
     * @ORM\ManyToOne(targetEntity="OrderBundle\Entity\Order")
     */
    private $parentOrder;

    /**
     * @ORM\ManyToOne(targetEntity="OrderBundle\Entity\OrderDish")
     */
    private $parentDish;

    private $parentIndex;

    /**
     * @ORM\Column(name="time", type="smallint", nullable=true)
     */
    private $time;

    public $isPrinted;

    public $tmpID;

    public function __construct(Order $order = null)
    {
        $this->order = $order;
        $this->time = 1;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param \OrderBundle\Entity\Order $order
     *
     * @return OrderDish
     */
    public function setOrder(\OrderBundle\Entity\Order $order = null)
    {

        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \OrderBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     * @return OrderDish
     */
    public function setDish(\DishBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \DishBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * Set dishPrice
     *
     * @param integer $dishPrice
     *
     * @return OrderDish
     */
    public function setDishPrice($dishPrice)
    {
        $this->dishPrice = round($dishPrice, 2);

        return $this;
    }

    /**
     * Set dishDiscount
     *
     * @param integer $dishDiscount
     *
     * @return OrderDish
     */
    public function setDishDiscount($dishDiscount)
    {
        $this->dishDiscount = round($dishDiscount, 2);

        return $this;
    }

    /**
     * Get dishPrice
     *
     * @return integer
     */
    public function getDishPrice()
    {
        return $this->dishPrice;
    }



    /**
     * Get dishDiscount
     *
     * @return integer
     */
    public function getDishDiscount()
    {
        return $this->dishDiscount;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return OrderDish
     */
    public function setQuantity($quantity)
    {
        if ($this->getDish()->getIsWeight()) {
            $this->quantity = $quantity;

        } else {
            $this->quantity = $quantity;

        }
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


    /**
     * Set delegate
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return OrderDish
     */
    public function setDelegate(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->delegate = $shop;

        return $this;
    }

    /**
     * Get delegate
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getDelegate()
    {
        return $this->delegate;
    }

    /**
     * Set parentOrder
     *
     * @param \OrderBundle\Entity\Order $parentOrder
     *
     * @return OrderDish
     */
    public function setParentOrder(\OrderBundle\Entity\Order $parentOrder = null)
    {
        $this->parentOrder = $parentOrder;

        return $this;
    }

    /**
     * Get parentOrder
     *
     * @return \OrderBundle\Entity\Order
     */
    public function getParentOrder()
    {
        return $this->parentOrder;
    }

    public function getCashSum()
    {
        return ($this->dishPrice * $this->quantity);
    }

    public function getDiscountSum()
    {
        return ($this->dishDiscount * $this->quantity);
    }

    public function getFullPrice()
    {
        return ($this->dishPrice + $this->dishDiscount);
    }

    public function getFullSum()
    {
        return ($this->getFullPrice() * $this->quantity);
    }


    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * Set parentDish
     *
     * @param \OrderBundle\Entity\OrderDish $parentDish
     *
     * @return OrderDish
     */
    public function setParentDish(\OrderBundle\Entity\OrderDish $parentDish = null)
    {
        $this->parentDish = $parentDish;

        return $this;
    }

    /**
     * Get parentDish
     *
     * @return \OrderBundle\Entity\OrderDish
     */
    public function getParentDish()
    {
        return $this->parentDish;
    }


    public function getParentIndex()
    {
        return $this->parentIndex;
    }

    public function setParentIndex($index)
    {
        $this->parentIndex = $index;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTmpID()
    {
        return $this->tmpID;
    }

    /**
     * @param mixed $tmpID
     * @return OrderDish
     */
    public function setTmpID($tmpID)
    {
        $this->tmpID = $tmpID;
        return $this;
    }


}
