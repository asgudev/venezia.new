<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use UserBundle\Form\Type\Frontend\ProfileFormType;
use UserBundle\Form\Type\Frontend\UserCreateType;
use UserBundle\Services\UserKey;

class DefaultController extends Controller
{

    /**
     * @Route("/users", name="user_list")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function usersAction()
    {
        $users = $this->getDoctrine()->getRepository("UserBundle:User")->findBy([
        ]);


        return $this->render("@User/Default/userList.html.twig", [
            'users' => $users
        ]);
    }

    /**
     * @Route("/login_tablet", name="login_tablet")
     */
    public function loginTouchAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository("UserBundle:User")->findUsersInShops(array_merge([$this->getParameter('shop.id')], $this->getParameter("relative_shops")));

        return $this->render("@User/Default/welcomeTablet.html.twig", [
            'users' => $users,
        ]);
    }

    public function welcomeAction(Request $request)
    {
        return $this->render("@User/Default/welcome.html.twig", []);
    }


    /**
     * @Route("/user/changeShop", name="change_user_shop_ajax")
     */
    public function changeShopAjax(Request $request)
    {
        $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find($request->get('shop'));
        if (!$shop) die();

        $this->getUser()->setShop($shop);
        $userManager = $this->get('fos_user.user_manager');

        $userManager->updateUser($this->getUser());

        return new JsonResponse([
            'status' => true,
        ]);
    }

    /**
     * @Route("/user/2fa", name="user_2fa")
     */
    public function twoFAUserAction(Request $request)
    {
        $userKey = UserKey::getUserKey($this->getUser());

        if ($request->getMethod() == "POST") {
            $this->container->get("redis.helper")->set(UserKey::getRedisKey($this->getUser()), time(), 60 * 60 * 4);

            return $this->redirect("/");
        }

        return $this->render("@User/Default/2fa.html.twig", [
            'userKey' => $userKey
        ]);
    }

    /**
     * @Route("/user/add", name="user_create")
     */
    public function addUserAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = new User();

        $userForm = $this->createForm(UserCreateType::class, $user, [
            'user' => $this->getUser(),
        ]);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted()) {
            if ($userForm->isValid()) {
                $user->setUsername(md5(time()));
                $user->setEnabled(true);


                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($user);

                return $this->redirectToRoute('user_edit', [
                    'id' => $user->getId(),
                ]);
            }
        }

        return $this->render('@User/Profile/create.html.twig', [
            'form' => $userForm->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/{id}/disable", name="user_disable")
     */
    public function deleteUserAction(Request $request, User $user)
    {
        if ($user->getId() == 2) {
            $this->get('user.logger')->info($this->getUser()->getName() . '(' . $this->getUser()->getId() . ')' . ' tried to delete root user');
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager();
        $user->setEnabled(false);
        $em->persist($user);
        $em->flush();

        $this->get('user.logger')->info($this->getUser()->getName() . '(' . $this->getUser()->getId() . ')' . ' deleted user ' . $user->getName() . '(' . $user->getId() . ')');

        return $this->redirectToRoute('user_list');
    }

    /**
     * @Route("/user/{id}/enable", name="user_enable")
     */
    public function enableUserAction(Request $request, User $user)
    {
        if ($user->getId() == 2) {
            $this->get('user.logger')->info($this->getUser()->getName() . '(' . $this->getUser()->getId() . ')' . ' tried to enable root user');
            return $this->redirectToRoute('index');
        }
        $em = $this->getDoctrine()->getManager();
        $user->setEnabled(true);
        $em->persist($user);
        $em->flush();

        $this->get('user.logger')->info($this->getUser()->getName() . '(' . $this->getUser()->getId() . ')' . ' enabled user ' . $user->getName() . '(' . $user->getId() . ')');

        return $this->redirectToRoute('user_list');
    }

    /**
     * @Route("/user/{id}/edit", name="user_edit")
     */
    public function editUserAction(Request $request, User $user)
    {
        if (($user->getId() == 2) && ($this->getUser()->getId() != 2)) {
            $this->get('user.logger')->info($this->getUser()->getName() . '(' . $this->getUser()->getId() . ')' . ' tried to edit root user');
            return $this->redirectToRoute('index');
        }
        if (!$user) {
            die("no user");
        }

        $form = $this->createForm(ProfileFormType::class, $user, [
            'user' => $this->getUser(),
            'action' => $this->generateUrl('user_edit', [
                'id' => $user->getId()
            ]),
        ]);


        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('user.logger')->info($this->getUser()->getName() . '(' . $this->getUser()->getId() . ')' . ' updated user profile ' . $user->getName() . '(' . $user->getId() . ')');

            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);

            return $this->render("@User/Profile/edit.html.twig", [
                'form' => $form->createView(),
                'qr' => base64_encode(UserKey::getUserKey($user) . "_" . time()),
                'userKey' => UserKey::getUserKey($user),
                'user' => $user,
            ]);
        }

        return $this->render("@User/Profile/edit.html.twig", [
            'form' => $form->createView(),
            'qr' => base64_encode(UserKey::getUserKey($user) . "_" . time()),
            'userKey' => UserKey::getUserKey($user),
            'user' => $user,
        ]);
    }
}
