<?php


namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncWithMasterCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('sync:master')
            ->setDescription('Sync to master');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $master = mysqli_connect("10.8.0.10", "venezia", "9WhsFYfbQjdQz7uR", "venezia_new", "3306");
            $local = $this->getContainer()->get("doctrine")->getManager()->getConnection();

            $date = (new \DateTime("now"))->modify("-24 hour")->format("Y-m-d H:i:s");
            $sql = "SELECT id FROM orders WHERE created_at >= '$date'";
            $stmt = $local->prepare($sql);
            $stmt->execute();
            $localData = $stmt->fetchAll();

            $localIds = [];
            foreach ($localData as $__id) {
                $localIds[] = $__id["id"];
            }

            $masterQ = mysqli_query($master, "SELECT id FROM orders WHERE created_at >= '$date'");
            $masterData = mysqli_fetch_all($masterQ);

            $masterIds = [];
            foreach ($masterData as $__id) {
                $masterIds[] = $__id[0];
            }

            $diff = array_unique(array_merge($localIds, $masterIds));

            if (count($diff) == 0) return;

            $localOrdersStmt = $local->prepare("SELECT * FROM orders WHERE id IN (" . implode(",", $diff) . ")");
            $localOrdersStmt->execute();
            $localOrdersData = $localOrdersStmt->fetchAll();

            foreach ($localOrdersData as $__order) {
                $orderQuery = preg_replace("#\'\'#", "null", "REPLACE INTO orders (" . implode(",", array_keys($__order)) . ") VALUES ('" . implode("','", array_values($__order)) . "');");

                mysqli_query($master, $orderQuery);
                print mysqli_error($master);
                print $orderQuery."\n";

                $clearMasterDishes = "DELETE FROM order_dish WHERE parent_order_id = ". $__order['id'];
                mysqli_query($master, $clearMasterDishes);

                $localOrderDishStmt = $local->prepare("SELECT * FROM order_dish WHERE parent_order_id = " . $__order['id']);
                $localOrderDishStmt->execute();
                $localOrderDishData = $localOrderDishStmt->fetchAll();
//var_dump("SELECT * FROM order_meta WHERE id = " . $__order['meta_id']);die();
                if ($__order['meta_id']) {
                 $orderMetaStmt = $local->prepare("SELECT * FROM order_meta WHERE id = " . $__order['meta_id']);
                 $orderMetaStmt->execute();
                 $orderMetaData = $orderMetaStmt->fetchAll()[0];
                 $orderMetaQuery = preg_replace("#\'\'#", "null", "REPLACE INTO order_meta (" . implode(",", array_keys($orderMetaData)) . ") VALUES ('" . implode("','", array_values($orderMetaData)) . "');");

                 mysqli_query($master, $orderMetaQuery);
                 print mysqli_error($master);
                 print $orderMetaQuery."\n";
                }

                foreach ($localOrderDishData as $__orderDish) {
                    $orderDishQuery = preg_replace("#\'\'#", "null", "REPLACE INTO order_dish (" . implode(",", array_keys($__orderDish)) . ") VALUES ('" . implode("','", array_values($__orderDish)) . "');");
                    mysqli_query($master, $orderDishQuery);
                    print mysqli_error($master);
                    print $orderDishQuery."\n";
                }
            }



            $date = (new \DateTime("now"))->modify("-73 hour")->format("Y-m-d H:i:s");

            $localReportStmt = $local->prepare("SELECT * FROM daily_report WHERE date_started >= '$date'");
            $localReportStmt->execute();
            $localReportData = $localReportStmt->fetchAll();

            foreach ($localReportData as $__report) {
                $reportQuery = preg_replace("#\'\'#", "null", "REPLACE INTO daily_report (" . implode(",", array_keys($__report)) . ") VALUES ('" . implode("','", array_values($__report)) . "');");

                mysqli_query($master, $reportQuery);
                print mysqli_error($master);
                print $reportQuery."\n";


                $localReportIngredientsStmt = $local->prepare("SELECT * FROM ingredient_data WHERE report_id = " . $__report['id']);
                $localReportIngredientsStmt->execute();
                $localReportIngredientsData = $localReportIngredientsStmt->fetchAll();

                foreach ($localReportIngredientsData as $__ingredientData) {
                    $ingredientDataQuery = preg_replace("#\'\'#", "null", "REPLACE INTO ingredient_data (" . implode(",", array_keys($__ingredientData)) . ") VALUES ('" . implode("','", array_values($__ingredientData)) . "');");
                    mysqli_query($master, $ingredientDataQuery);
                    print mysqli_error($master);
                    print $ingredientDataQuery."\n";
                }
            }



        } catch (\Exception $ex) {
            dump($ex);
        }
    }

}
