<?php


namespace ReportBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ReportBundle\Entity\Repository\ReportRepository")
 * @ORM\Table()
*/
class Report
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->data = new ArrayCollection();
    }

    /**
     * @ORM\Column(name="name", nullable=false, type="string")
     */
    private $name;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\ReportData", mappedBy="report", cascade={"persist"})
     */
    private $data;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return Report
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return Report
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     * @return Report
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        return $this;
    }

    /**
     * @return ReportData[]
     */
    public function getData()
    {
        return $this->data;
    }



    /**
     * @param ReportData $data
     * @return Report
     */
    public function addData(ReportData $data)
    {
        $this->data->add($data);
        $data->setReport($this);
        return $this;
    }

    /**
     * @param ReportData $data
     */
    public function removeData(ReportData $data)
    {
        $this->data->removeElement($data);
        $data->setReport(null);
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }



}