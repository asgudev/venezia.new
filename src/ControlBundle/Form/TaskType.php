<?php

namespace ControlBundle\Form;

use ControlBundle\Entity\IngredientData;
use ControlBundle\Entity\Task;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;

class TaskType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => "Задача",
                'required' => true,
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Описание',
                'required' => true,
            ])
            ->add('dueAt', DateTimeType::class, [
                'label' => 'Выполнить до',
                'date_widget' => "single_text",
                'date_format' => 'YYYY-MM-dd',
                'time_widget' => 'single_text',
            ])
            ->add('assignedTo', EntityType::class, [
                'label' => 'Исполнители',
                'class' => User::class,
                'choice_label' => 'name',
                'multiple' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Task::class,
        ));
    }
}