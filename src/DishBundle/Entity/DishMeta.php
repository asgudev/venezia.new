<?php

namespace DishBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * DishMeta
 *
 * @ORM\Table(name="dish_meta")
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\DishMetaRepository")
 */
class DishMeta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\Dish", inversedBy="dishMetas")
     * @ORM\JoinColumn(name="dish_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $dish;

    /**
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(name="is_delivery", type="boolean", nullable=true)
     */
    private $isDelivery;


    public function __construct()
    {
        $this->warning = false;
        $this->isDelivery = true;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return DishMeta
     */
    public function setPrice($price)
    {
        $this->price =  str_replace(',', '.', $price);

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     */
    public function setDish(\DishBundle\Entity\Dish $dish = null)
    {
        $this->dish = $dish;

        return $this;
    }

    /**
     * Get dish
     *
     * @return \DishBundle\Entity\Dish
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @return bool
     */
    public function isDelivery(): bool
    {
        return $this->isDelivery;
    }

    /**
     * @param bool $isDelivery
     * @return DishMeta
     */
    public function setIsDelivery(bool $isDelivery): DishMeta
    {
        $this->isDelivery = $isDelivery;
        return $this;
    }


}
