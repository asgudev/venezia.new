<?php

namespace DishBundle\Controller;

use DishBundle\Entity\DishCategory;
use DishBundle\Entity\Dish;
use DishBundle\Entity\DishHistory;
use DishBundle\Entity\DishMeta;
use RestaurantBundle\Events\DishEvent;
use RestaurantBundle\Entity\Shop;
use DishBundle\Form\Type\DishType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class DishController extends Controller
{


    /*

    DELETE FROM `dish_meta` WHERE id IN( SELECT cid FROM ( SELECT t1.id AS cid FROM dish_meta t1 LEFT JOIN dish t2 ON t1.dish_id = t2.id WHERE t1.price = t2.price ) AS c )
    DELETE FROM `dish_meta` WHERE id IN( SELECT cid FROM ( SELECT t1.id AS cid FROM dish_meta t1 LEFT JOIN dish t2 ON t1.dish_id = t2.id WHERE t2.is_deleted = 1 ) AS c )

     */

    /**
     * @Route("/dish/list", name="dish_list")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function dishListAction(Request $request)
    {
        $dishes = $this->getDoctrine()->getRepository("DishBundle:Dish")->findAllDishes($request->getLocale());
        $shops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAdminShops($this->getUser()->getAdminShops());
        $replication = $this->get('restaurant.replication')->getConnectedSlaves();

        $this->get('user.logger')->info($this->getUser()->getName() . ' requested full dish list');

        $pendingChanges = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_NEW, "ASC", null, 999999);

        $pricesMixer = $this->get('restaurant.pending_price_mixer');

        foreach ($dishes as $dish) {
            foreach ($pendingChanges as $change) {
                if ($dish->getId() == $change->getDish()->getId()) {
                    $pricesMixer->mixToDish($dish, $change);
                }
            }
        }

        return $this->render("DishBundle:Dish:dishList.html.twig", [
            'dishes' => $dishes,
            'shops' => $shops,
            'replication' => $replication,
        ]);
    }

    /**
     * @Route("/dish/list/deleted", name="dish_list_deleted")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function dishDeletedList()
    {
        $dishes = $this->getDoctrine()->getRepository("DishBundle:Dish")->findDeletedDishes();
        $shops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAdminShops($this->getUser()->getAdminShops());

        return $this->render("DishBundle:Dish:dishListDeleted.html.twig", [
            'dishes' => $dishes,
            'shops' => $shops,
        ]);
    }

    /**
     * @Route("/dish/search", name="dish_search")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function dishSearchAction(Request $request)
    {
        $dishes = $this->getDoctrine()->getRepository('DishBundle:Dish')->finbBy1cId($request->get('id'));
        $shops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAdminShops($this->getUser()->getAdminShops());
        $slaves = $this->get('restaurant.replication')->getConnectedSlaves();

        return $this->render("DishBundle:Dish:dishList.html.twig", [
            'dishes' => $dishes,
            'shops' => $shops,
            'slaves' => $slaves,
        ]);
    }

    /**
     * @Route("/dish/list/conflicts", name="dish_list_conflicts")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function dishConflictList()
    {
        $dishes = $this->getDoctrine()->getRepository("DishBundle:Dish")->findConflictsDishes();

        return $this->render("DishBundle:Dish:dishListConflicts.html.twig", [
            'dishes' => $dishes,
        ]);
    }

    /**
     * @Route("/dish/add", name="dish_add")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function addDishAction(Request $request)
    {
        $dish = new Dish();

        $form = $this->createForm(DishType::class, $dish, []);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $log = new DishHistory($dish);
            $log->setUser($this->getUser());
            $dish->addChange($log);

            foreach ($dish->getDishMetas() as $dishMeta) {
                $dishMeta->setDish($dish);
                $meta_log = new DishHistory($dish);
                $meta_log->setUser($this->getUser());
                $meta_log->setValue($dishMeta->getPrice());
                $meta_log->setShop($dishMeta->getShop());
                $em->persist($meta_log);
            }

            $em->persist($log);
            $em->persist($dish);
            $em->flush();

            return $this->redirectToRoute('dish_edit', ['id' => $dish->getId()]);
        }

        return $this->render("DishBundle:Dish:dishEdit.html.twig", [
            'form' => $form->createView(),
            'dish' => $dish,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/dish/updateAjax", name="dish_update_ajax")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function updateDishAjaxAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $dish = $this->getDoctrine()->getRepository("DishBundle:Dish")->find($request->get("dish"));
        $shops = $this->getUser()->getAdminShops();

        $this->get('user.logger')->info($this->getUser()->getName() . '(' . $this->getUser()->getId() . ')' . ' updated dish ' . $dish->getName() . '(' . $dish->getId() . ') referrer ' . $request->headers->get('referer'));

        $log = new DishHistory($dish);
        $log->setUser($this->getUser());


        if ($request->get("type") === 'name') {
            $log->setField('name')
                ->setValue($request->get("value"));
            $em->persist($log);
            $em->flush();
        }

        if ($request->get("type") === 'set_category') {
            $cat = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->find($request->get("value"));
            $dish->setCategory($cat);
            $cat->addDish($dish);
            $em->persist($cat);
        }


        if ($request->get("type") === 'base') {
            if ($request->get("newPrice") != 0) {
                $oldPrice = $dish->getPrice();
//dump($oldPrice);die();
//                $dish->setPrice($request->get("newPrice"));
                $log->setField('base_price')
                    ->setValue($request->get("newPrice"));


                foreach ($dish->getDishMetas() as $dishMeta) {
                    if ($dishMeta->getPrice() == $oldPrice) {
                          $dmChange = new DishHistory($dish);
                          $dmChange->setUser($this->getUser());
                          $dmChange->setField("price");
                          $dmChange->setValue($request->get("newPrice"));
                          $dmChange->setShop($dishMeta->getShop());
                          $em->persist($dmChange);
//                        $dishMeta->setPrice($request->get("newPrice"));
//                        $em->persist($dishMeta);
                    }
                }

                $em->persist($log);
            } else {
                foreach ($dish->getShops() as $shop) {
                    $dish->removeShop($shop);
                    $shop->removeDish($dish);
                    $em->persist($shop);
                    $em->persist($dish);
                }
            }
        } elseif ($request->get("type") === 'special') {
            $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->find($request->get("shop"));
//var_dump($request->get("newPrice"));die();
            if ($request->get("newPrice") != 0) {
                $log->setField('price')
                    ->setValue($request->get("newPrice"))
                    ->setShop($shop);
                $em->persist($log);
            } else {
//var_dump(234);
                $dish->removeShop($shop);
                $shop->removeDish($dish);
                $em->persist($shop);
            }
        }

        $em->persist($dish);
        $em->flush();

        $row = $this->get("twig")->load("DishBundle:Dish:dishTable.html.twig")->renderBlock("dish_list_row", [
            'dish' => $dish,
            'shops' => $shops,
            '_locale' => $request->getLocale(),
        ]);

        $updateEvent = new DishEvent($dish, "replace", $row);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(DishEvent::TABLE_UPDATE, $updateEvent);

        return new JsonResponse([
            'data' => $row
        ]);

        $pricesMixer = $this->get('restaurant.pending_price_mixer');
        $pendingChanges = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_NEW, "ASC", null, 999999);
        foreach ($pendingChanges as $change) {
            if ($dish->getId() == $change->getDish()->getId()) {
                $pricesMixer->mixToDish($dish, $change);
            }
        }

        $row = $this->get("twig")->load("DishBundle:Dish:dishTable.html.twig")->renderBlock("dish_list_row", [
            'dish' => $dish,
            'shops' => $shops,
            '_locale' => $request->getLocale(),
        ]);

        $updateEvent = new DishEvent($dish, "replace", $row);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(DishEvent::TABLE_UPDATE, $updateEvent);

        return new JsonResponse([
            'data' => $row
        ]);
    }


    /**
     * @Route("/dish/{id}/restore", name="dish_restore")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function restoreDishAction(Request $request, Dish $dish)
    {
        $dish->setIsDeleted(false);
        $em = $this->getDoctrine()->getEntityManager();
        $em->flush();

        return new JsonResponse([
            'status' => true,
            'command' => 'row.remove(); alert("Восстановлено")',
        ]);
    }

    /**
     * @Route("/dish/{id}/delete", name="dish_delete")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function deleteDishAction(Request $request, Dish $dish)
    {
        if (!$dish) {
            dump("error");
            die();
        }

        $em = $this->getDoctrine()->getEntityManager();
        $dish->setIsDeleted(true);
        $em->flush();


        if ($request->get("ajax") !== null) {

            $updateEvent = new DishEvent($dish, "delete");
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(DishEvent::TABLE_UPDATE, $updateEvent);

            return new JsonResponse([
                'status' => true,
                'command' => 'row.remove(); alert("Удалено")',
            ]);
        } else {
            return $this->redirectToRoute("dish_list");
        }
    }

    /**
     * @Route("/dish/change/{id}/update", name="dish_change_update")
     * @Security("has_role('ROLE_SHOP_MANAGER')")
     */
    public function updateDishChangeAction(Request $request, DishHistory $dishHistory)
    {
        if (!$dishHistory)
            throw new NotFoundHttpException();

        $dishHistory->setStatus($request->get('status'));
        $dishHistory->setConfirmedAt(new \DateTime());
        $dishHistory->setConfirmedBy($this->getUser());
        $em = $this->getDoctrine()->getManager();

        $em->persist($dishHistory);
        $em->flush();

        return new JsonResponse(true);
    }

    /**
     * @Route("/dish/changes", name="dish_changes")
     */
    public function showDishChangesAction(Request $request)
    {
        $new = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_NEW, 'ASC', null, 99999);
        $confirmed = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_CONFIRMED);
        $declined = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_DECLINED);
        $updated = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_UPDATED, 'DESC', null, 100);

        return $this->render('DishBundle:Dish:changes.html.twig', [
            'new' => $new,
            'confirmed' => $confirmed,
            'declined' => $declined,
            'updated' => $updated,
        ]);
    }

    /**
     * @Route("/dish/{id}/edit", name="dish_edit")
     */
    public function editDishAction(Request $request, Dish $dish)
    {
  
        if (!$dish) {
            dump("error");
            die();
        }
        $em = $this->getDoctrine()->getEntityManager();

        $dish->setTranslatableLocale($request->getLocale());
        $em->refresh($dish);


        $shops = [];
        foreach ($this->getUser()->getAdminShops() as $__shop) {
            $shops[] = $__shop->getId();
        }

            // dump($this->getUser()->getAdminShops());

        $form = $this->createForm(DishType::class, $dish, [
            'shops' => $shops
        ]);

        $allShops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAll();
        $nonUserShops = array_filter($allShops, function(Shop $shop) use ($shops) {
          return !in_array($shop->getId(), $shops);
          return true;
        });

       
        $shopsBefore = clone $dish->getShops();
        
        $activeInNonUser = array_filter($shopsBefore->toArray(), function(Shop $shop) use ($nonUserShops) {
          return in_array($shop, $nonUserShops);
        });
        
        $form->handleRequest($request);

        foreach($activeInNonUser as $__shop) {
          $dish->addShop($__shop);
        }

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (!$this->getUser()->hasRole('ROLE_BUHGALT')) throw new AccessDeniedException();
                if ($form->get("delete")->isClicked()) {
                    $dish->setIsDeleted(true);
                    $em->flush();
                    return $this->redirectToRoute("dish_list");
                }

                foreach ($dish->getDishMetas() as $dishMeta) {
                    $dishMeta->setDish($dish);
                }
                foreach ($dish->getTimes() as $time) {
                    $time->setDish($dish);
                }

                $uow = $em->getUnitOfWork();

                $oldDish = $uow->getOriginalEntityData($dish);

                foreach ($dish->getDishMetas() as $dishMeta) {
                    $oldDishMeta = $uow->getOriginalEntityData($dishMeta);
                    if ((array_key_exists("price", $oldDishMeta)) && ($oldDishMeta["price"] != $dishMeta->getPrice())) {
                        $log = new DishHistory($dish);
                        $log->setUser($this->getUser())
                            ->setField('price')
                            ->setValue($dishMeta->getPrice())
                            ->setShop($dishMeta->getShop());
                        $dishMeta->setPrice($oldDishMeta['price']);
                        $em->persist($log);
                    }
                    if ($oldDishMeta == []) {
                        $log = new DishHistory($dish);
                        $log->setUser($this->getUser())
                            ->setField('price')
                            ->setValue($dishMeta->getPrice())
                            ->setShop($dishMeta->getShop());

                        $em->persist($log);
                        $dish->removeDishMeta($dishMeta);
                        $dish->removeShop($dishMeta->getShop());
                    }
                }

                if ($oldDish['name'] !== $dish->getName()) {
                    $log = new DishHistory($dish);
                    $log->setUser($this->getUser())
                        ->setField('name')
                        ->setValue($dish->getName());
                    $dish->setName($oldDish['name']);
                    $em->persist($log);
                }

                $uow->computeChangeSets();
                $changes = $uow->getEntityChangeSet($dish);

                foreach ($changes as $field => $change) {
                    if ($field == "price") {

                foreach ($dish->getDishMetas() as $dishMeta) {
                    if ($dishMeta->getPrice() == $oldDish["price"]) {
                          $dmChange = new DishHistory($dish);
                          $dmChange->setUser($this->getUser());
                          $dmChange->setField("price");
                          $dmChange->setValue($dish->getPrice());
                          $dmChange->setShop($dishMeta->getShop());
                          $em->persist($dmChange);
//                        $dishMeta->setPrice($request->get("newPrice"));
//                        $em->persist($dishMeta);
                    }
                }
                        $log = new DishHistory($dish);
                        $log->setUser($this->getUser());
                        $log->setField('base_price');
                        $log->setValue($dish->getPrice());
                        $em->persist($log);
                        $dish->setPrice($oldDish["price"]);
                    }
                }



                $dish->setTranslatableLocale($request->getLocale());


//                dump($dish->getTimes());die();
//dump($dish);
                $em->persist($dish);
                $em->flush();

                return $this->redirectToRoute('dish_edit', ['id' => $dish->getId()]);
            } else {
                //dump($form->getErrors());
                //die();
            }
        }


        $lastOrders = $this->getDoctrine()->getRepository("OrderBundle:OrderDish")->findLastOrders($dish);

        $dish->setTranslatableLocale($request->getLocale());
        $em->refresh($dish);


        return $this->render("DishBundle:Dish:dishEdit.html.twig", [
            'form' => $form->createView(),
            'dish' => $dish,
            'lastOrders' => $lastOrders,
        ]);
    }

    /**
     * @Route("/dish/{id}/list/sort", name="dish_list_sort")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function categoryListSortAction(Request $request, DishCategory $category)
    {
        $dishes = $this->getDoctrine()->getRepository("DishBundle:Dish")->findBy([
            'category' => $category,
            'isDeleted' => 0
        ], [
            'order' => 'ASC'
        ]);

        return $this->render('DishBundle:Dish:dishListSort.html.twig', [
            'dishes' => $dishes,
            'category' => $category,
        ]);
    }

    /**
     * @Route("/dish/list/{id}/sort/update", name="dish_list_sort_ajax")
     * @Security("has_role('ROLE_BUHGALT')")
     */
    public function categoryListSortUpdateAction(Request $request, DishCategory $category)
    {
        $em = $this->getDoctrine()->getManager();
        $newOrder = $request->get("data");

        if ($newOrder) {
            foreach ($newOrder as $order) {

                $dish = $this->getDoctrine()->getRepository("DishBundle:Dish")->find($order["dish"]);
                $dish->setOrder((int)$order["order"]);
                $em->persist($dish);
            }
            $em->flush();
        }

        $dishes = $this->getDoctrine()->getRepository("DishBundle:Dish")->findBy([
            'category' => $category,
            'isDeleted' => 0
        ], [
            'pri' => 'ASC'
        ]);

        $row = $this->get("twig")->loadTemplate("DishBundle:Dish:dishListSort.html.twig")->renderBlock("dish_list", [
            'dishes' => $dishes,
            'category' => $category,
        ]);

        return new JsonResponse([
            'status' => true,
            'data' => $row
        ]);
    }

private function sendToWS($dish) {

        $node = "192.168.0.81:3000";

        if ($this->environment == "dev") {
            $url = $node."/updateTable/dev";
        } else {
            $url = $node."/updateTable";
        }

        $data = json_encode([
            'id' => $dish->getId(),
            'type' => $event->getType(),
            'data' => $event->getData(),
        ]);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($curl);
        curl_close($curl);


}

}
