<?php

namespace ControlBundle\Controller;


use ControlBundle\Entity\DishPhoto;
use ControlBundle\Entity\DishPhotoCheck;
use RestaurantBundle\Service\BitToArray;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PhotoController extends Controller
{
    /**
     * @Route("/admin/photo/test", name="photo_validator_test")
     */
    public function photoValidateTesAction(Request $request)
    {
        $qr = $this->get('qr_code.service');

        die();
//        $validator = $this->get('control.photo.validator');
//        $dishPhoto = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->find(65688);
//        $validator->shouldPhotoBeValidated($dishPhoto);
//        die();
    }

    /**
     * @Route("/photo/listener", name="photo_listener")
     */
    public function photoListenerAction(Request $request)
    {
        $data = json_decode($request->getContent());

        $em = $this->getDoctrine()->getManager();
        $dishPhoto = new DishPhoto();
        $dishPhoto->setDate(new \DateTime());
        $dishPhoto->setPhotoPath($data->photo);
        $dishPhoto->setWeight($data->weight);
        $dishPhoto->setUser($this->getUser());

        $em->persist($dishPhoto);
        $em->flush();


        return new Response();

    }

    /**
     * @Route("/admin/photolist/{dateStart}/{dateEnd}", name="photo_list")
     */
    public function photoListAction(Request $request, $dateStart = null, $dateEnd = null)
    {

        $statByUserAll = $this->getDoctrine()->getRepository('ControlBundle:DishPhotoCheck')->findStatByUser($dateStart, $dateEnd);
        $statByCookAll = $this->getDoctrine()->getRepository('ControlBundle:DishPhotoCheck')->findStatByCook($dateStart, $dateEnd);
        $positiveStatByUserAll = $this->getDoctrine()->getRepository('ControlBundle:DishPhotoCheck')->findPositiveStatByUser($dateStart, $dateEnd);

        $__statByUser = [];
        foreach ($statByUserAll as $__stat) {
            $__statByUser[$__stat['uName']]['avg'] = $__stat['avgRate'];
            $__statByUser[$__stat['uName']]['all'] = $__stat['uCount'];
            $__statByUser[$__stat['uName']]['positive'] = 0;
        }
        foreach ($positiveStatByUserAll as $__stat) {
            $__statByUser[$__stat['uName']]['positive'] = $__stat['uCount'];
        }

        $statByUser = [];
        foreach ($__statByUser as $__user => $__stat) {
            $statByUser[] = [
                'name' => $__user,
                'avg' => $__stat['avg'],
                'all' => $__stat['all'],
                'positive_all' => $__stat['positive'],
            ];
        }

//        $statByDish = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->findStatByDish();
	$statByDish = [];

        $photos = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->findLast();
        $pizzaList = [];
        $invalid = [];
        $validator = $this->get('control.photo.validator');
        foreach ($photos as $photo) {
            $dish = $photo->getOrderDish()->getDish();

            $conclusion = $validator->shouldBeManualValidated($photo);
            if (count($conclusion) > 0) {
                $invalid[] = $photo;
                $photo->problems = $conclusion;
            }

            if (!array_key_exists($dish->getId(), $pizzaList))
                $pizzaList[$dish->getId()] = $dish->getName();
        }
        $pizzaList[-1] = "Нет";


        $photosData = $this->getDoctrine()->getRepository("ControlBundle:DishPhoto")->findCountInRangeStats($dateStart, $dateEnd);
        $dishData = $this->getDoctrine()->getRepository("OrderBundle:OrderDish")->findPizzaCountPerDateRange($dateStart, $dateEnd);

        $statByShop = [];

        foreach ($photosData as $__photo) {
            $shopId = $__photo["shopName"];
            if (!array_key_exists($shopId, $statByShop)) {
                $statByShop[$shopId] = [
                    'photos' => 0,
                    'dishes' => 0,
                ];
            }
            $statByShop[$shopId]["photos"] = $__photo["photoCnt"];
        }

        foreach ($dishData as $__dish) {
            $shopId = $__dish["shopName"];
            if (!array_key_exists($shopId, $statByShop)) {
                $statByShop[$shopId] = [
                    'photos' => 0,
                    'dishes' => 0,
                ];
            }
            $statByShop[$shopId]["dishes"] = $__dish["dishCnt"];
        }


        return $this->render('@Control/Photo/photo.html.twig', [
            'photos' => $photos,
            'invalid' => $invalid,
            'statByUser' => $statByUser,
            'statByCook' => $statByCookAll,
            'statByShop' => $statByShop,
            'positiveByUserAll' => $positiveStatByUserAll,
            'statByDish' => $statByDish,
            'pizzaList' => $pizzaList,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

    /**
     * @Route("/admin/photo/{id}/validate", name="photo_validate")
     */
    public function photoValidateAction(Request $request, DishPhoto $dishPhoto)
    {
        $em = $this->getDoctrine()->getManager();
        $dishPhoto->setIsValidated(true);
        $dishPhoto->setValidatedBy($this->getUser());
        if ($request->get('dishId')) {
            if ($request->get('dishId') == '-1') {
                $dishPhoto->setValidDish(null);
            } else {
                $dishPhoto->setValidDish($em->getReference('DishBundle:Dish', $request->get('dishId')));
            }
        } else {
            $dishPhoto->setValidDish($dishPhoto->getOrderDish()->getDish());
        }
        $em->persist($dishPhoto);
        $em->flush();

        return new JsonResponse(true);
    }

    /**
     * @Route("/admin/photo/info", name="photo_info")
     */
    public function photoInfoAction(Request $request)
    {

        $photo = $this->getDoctrine()->getRepository('ControlBundle:DishPhoto')->findFull($request->get('id'));
        $validator = $this->get('control.photo.validator');
        $photo->problems = $validator->shouldBeManualValidated($photo);

        return $this->render('@Control/Photo/info.html.twig', [
            'photo' => $photo
        ]);
    }

    /**
     * @Route("/admin/photo/{id}/delete", name="photo_delete")
     */
    public function photoDeleteAction(Request $request, DishPhoto $photo)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($photo);
        $em->flush();

        return $this->redirectToRoute('photo_list');
    }
}
