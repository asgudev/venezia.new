<?php

namespace OrderBundle\Entity\Repository;


use TerminalBundle\Entity\DailyReport;
use Doctrine\ORM\EntityRepository;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderChange;

class OrderChangeRepository extends EntityRepository
{

    /**
     * @param string $dateStart
     * @param string $dateEnd
     * @return OrderChange[]
     */
    public function findGroupedByUser($dateStart = 'now', $dateEnd = 'now')
    {
        return $this->createQueryBuilder('oc')
            ->addSelect('o')
            ->addSelect('od')
            ->addSelect('u')
            ->leftJoin('oc.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->leftJoin('o.user', 'u')
            ->where('oc.datetime >= :dateStart')
            ->andWhere('oc.datetime <= :dateEnd')
            ->setParameters([
                'dateStart' => new \DateTime($dateStart),
                'dateEnd' => new \DateTime($dateEnd),
            ])
            ->orderBy('oc.datetime', "ASC")
            ->getQuery()
            ->getResult();

    }


    public function findTodayChangesCount()
    {
        return $this->createQueryBuilder('oc')
            ->select('COUNT(oc) as ocCount')
            ->addSelect('s.id as sID')
            ->leftJoin('oc.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->leftJoin('o.shop', 's')
            ->where('DATE(oc.datetime) = DATE(:date)')
            ->setParameters([
                'date' => new \DateTime(),
            ])
            ->groupBy('s.id')
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $date
     * @return OrderChange[]
     */
    public function findByDate($dateStart, $dateEnd = null, $shop = null)
    {
        $changes = $this->createQueryBuilder('oc')
            ->addSelect('od')
            ->addSelect('o')
            ->addSelect('s')
            ->addSelect('u')
            ->addSelect('d')
            ->where('oc.datetime >= DATE(:dateStart)')
            ->andWhere('oc.datetime <= DATE(:dateEnd)')
            ->leftJoin('oc.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->leftJoin('o.shop', 's')
            ->leftJoin('o.user', 'u')
            ->leftJoin('od.dish', 'd')
            ->setParameters([
                'dateStart' => (new \DateTime($dateStart))->setTime(0,0,0),
                'dateEnd' => (new \DateTime($dateEnd))->setTime(23,59,59),
            ]);

        if ($shop) {
            $changes->andWhere('o.shop = :shop')->setParameter('shop', $shop);
        }

        if ($dateEnd) {

        }
        if ($shop) {
            $changes->andWhere('s.id = :shop')->setParameter('shop', $shop);
        }
        return $changes
            ->getQuery()
            ->getResult();
    }

    public function findChangesByReport(DailyReport $report)
    {
        return $this->createQueryBuilder('oc')
            ->select("COUNT(oc) AS ocCount")
            ->leftJoin('oc.orderDish', 'ocod')
            ->leftJoin('ocod.parentOrder', 'oco')
            ->where('oc.datetime BETWEEN :day_start AND :day_end')
            ->andWhere('oco.shop = :shop')
            ->setParameters([
                'day_start' => $report->getDateStarted(),
                'day_end' => $report->getDateClosed(),
                'shop' => $report->getShop(),
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllFromDate($shop = null, $date)
    {
        $changes = $this->createQueryBuilder('c')
            ->where('o.createdAt > :dateStart')
            ->andWhere('c.type = 0')
            ->andWhere('o.shop = :shop')
            ->addSelect('o')
            ->addSelect('od')
            ->leftJoin('c.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->setParameters([
                'dateStart' => $date,
                'shop' => $shop,
            ])
            ->getQuery()
            ->getResult();

        return $changes;
    }

    public function findAllByDate($shop = null, $date = 'today')
    {
        $changes = $this->createQueryBuilder('c')
            ->where('o.createdAt BETWEEN :day_start AND :day_end')
            ->andWhere('c.type = 0')
            ->andWhere('o.shop = :shop')
            ->addSelect('o')
            ->addSelect('od')
            ->leftJoin('c.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->setParameters([
                'day_start' => (new \DateTime($date))->setTime(00, 00, 00),
                'day_end' => (new \DateTime($date))->setTime(23, 59, 59),
                'shop' => $shop,
            ])
            ->getQuery()
            ->getResult();

        return $changes;
    }

    public function findAllDeletionsByDate($shop = null, $date)
    {
        $changes = $this->createQueryBuilder('c')
            ->where('o.createdAt BETWEEN :day_start AND :day_end')
            ->andWhere('c.type = :type')
            ->addSelect('o')
            ->addSelect('od')
            ->leftJoin('c.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->setParameters([
                'type' => OrderChange::ORDER_CHANGE_DISH_DELETE,
                'day_start' => (new \DateTime($date))->setTime(00, 00, 00),
                'day_end' => (new \DateTime($date))->setTime(23, 59, 59),
            ]);

        if ($shop !== null) {
            $changes
                ->andWhere('o.shop = :shop')
                ->setParameter('shop', $shop);
        }

        $changes = $changes->getQuery()->getResult();

        return $changes;
    }

    /**
     * @param DailyReport $report
     * @return OrderChange[]
     */
    public function findByReport(DailyReport $report)
    {
        $changes = $this->createQueryBuilder('c')
            ->where('c.datetime BETWEEN :dateStart AND :dateEnd')
            ->andWhere('o.shop = :shop')
            ->addSelect('o')
            ->addSelect('od')
            ->leftJoin('c.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
            ]);

        $changes = $changes->getQuery()->getResult();

        return $changes;
    }

    /**
     * @param DailyReport $report
     * @return OrderChange[]
     */
    public function findDeletionsByReport(DailyReport $report)
    {
        $changes = $this->createQueryBuilder('c')
            ->where('c.datetime BETWEEN :dateStart AND :dateEnd')
            ->andWhere('o.shop = :shop')
            ->andWhere('c.type = :type')
            ->addSelect('o')
            ->addSelect('od')
            ->leftJoin('c.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->setParameters([
                'shop' => $report->getShop(),
                'dateStart' => $report->getDateStarted(),
                'dateEnd' => $report->getDateClosed(),
                'type' => OrderChange::ORDER_CHANGE_DISH_DELETE,
            ]);

        $changes = $changes->getQuery()->getResult();

        return $changes;
    }

    /**
     * @param Order $order
     * @return OrderChange[]
     */
    public function findByOrder(Order $order)
    {
        return $this->createQueryBuilder('oc')
            ->leftJoin('oc.orderDish', 'od')
            ->leftJoin('od.parentOrder', 'o')
            ->where('o.id = :id')
            ->setParameters([
                'id' => $order->getId(),
            ])
            ->getQuery()
            ->getResult();
    }
}
