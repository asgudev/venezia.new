<?php

namespace RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dish
 *
 * @ORM\Table(name="clients")
 * @ORM\Entity(repositoryClass="RestaurantBundle\Entity\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="unp", type="string", length=255)
     */
    private $unp;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_number", type="string", nullable=true, length=255)
     */
    private $bankNumber;

    /**
     * @ORM\OneToMany(targetEntity="OrderBundle\Entity\Order", mappedBy="client")
     */
    private $orders;

    /**
     * @ORM\Column(name="is_exportable", type="boolean", options={"default" = 1})
     */
    private $isExportable;

    /**
     * @ORM\Column(name="track_in_delivery", type="boolean", options={"default" = 0})
     */
    private $trackInDelivery;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", options={"default" = 0})
     */
    private $isDeleted;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set unp
     *
     * @param string $unp
     *
     * @return Client
     */
    public function setUnp($unp)
    {
        $this->unp = $unp;

        return $this;
    }

    /**
     * Get unp
     *
     * @return string
     */
    public function getUnp()
    {
        return $this->unp;
    }

    /**
     * Set bankNumber
     *
     * @param string $bankNumber
     *
     * @return Client
     */
    public function setBankNumber($bankNumber)
    {
        $this->bankNumber = $bankNumber;

        return $this;
    }

    /**
     * Get bankNumber
     *
     * @return string
     */
    public function getBankNumber()
    {
        return $this->bankNumber;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->isExportable = true;
        $this->isDeleted = false;
    }

    /**
     * Add order
     *
     * @param \OrderBundle\Entity\Order $order
     *
     * @return Client
     */
    public function addOrder(\OrderBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \OrderBundle\Entity\Order $order
     */
    public function removeOrder(\OrderBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set isExportable
     *
     * @param boolean $isExportable
     *
     * @return Client
     */
    public function setIsExportable($isExportable)
    {
        $this->isExportable = $isExportable;

        return $this;
    }

    /**
     * Get isExportable
     *
     * @return boolean
     */
    public function getIsExportable()
    {
        return $this->isExportable;
    }


    /**
     * @return mixed
     */
    public function getTrackInDelivery()
    {
        return $this->trackInDelivery;
    }

    /**
     * @param mixed $trackInDelivery
     */
    public function setTrackInDelivery($trackInDelivery): void
    {
        $this->trackInDelivery = $trackInDelivery;
    }

    /**
     * @return bool
     */
    public function getIsDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     * @return Client
     */
    public function setIsDeleted(bool $isDeleted): Client
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

}
