<?php

namespace TerminalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/terminal")
 */
class PromoCardController extends Controller
{

    const API_PATH = 'https://admin.venezia.by';

    /**
     * @Route("/promo/load", name="terminal_promo_card_load")
     */
    public function promoCardLoadAction(Request $request)
    {
        $path = self::API_PATH . $this->generateUrl('api_promo_card_load');

        $ch = curl_init(self::API_PATH . $this->generateUrl('api_promo_card_load'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'code' => $request->get('code')
        ]));
        $response = curl_exec($ch);

        $data = json_decode($response);
        curl_close($ch);

        if ($data->id) {
            return new JsonResponse([
                'cardId' => $data->id,
                'discount' => $data->discount,
                'name' => $data->name,
            ]);
        } else {
            return new JsonResponse([
                'card' => false
            ]);
        }
    }


    /**
     * @Route("/card/list", name="terminal_promo_card_list")
     */
    public function promoCardListAction(Request $request)
    {
        $notIssuedCards = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->findNotIssued($this->getUser()->getShop());


        return $this->render("@Terminal/Card/cardList.html.twig", [
            'notIssuedCards' => $notIssuedCards,
        ]);
    }

    /**
     * @Route("/card/issue", name="terminal_promo_card_issue")
     */
    public function promoCardIssueAction(Request $request)
    {
        $promoCard = $this->getDoctrine()->getRepository("RestaurantBundle:PromoCard")->find($request->get('card'));

        if ($promoCard) {
            $em = $this->getDoctrine()->getManager();

            $promoCard->setIssuedAt(new \DateTime());
            $promoCard->setIssuedBy($this->getUser());

            $em->persist($promoCard);
            $em->flush();
        }

        return new JsonResponse();


    }

}
