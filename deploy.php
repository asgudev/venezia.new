<?php
require 'recipe/symfony.php';

set('writable_use_sudo', false);
set('repository', 'git@bitbucket.org:asgudev/venezia.new.git');

serverList('app/config/servers.yml');


set('keep_releases', 3);

set('shared_files', [
    'app/config/parameters.yml',
    'web/.htaccess'
]);

set('shared_dirs', [
    'var/logs',
    'var/sessions',

    'web/uploads',
    'web/media',

    'var/export',
    'var/marco',
]);

set('writable_dirs', [
    'var/cache',
    'var/logs',
    'var/sessions',
    'web/uploads',
    'web/media',
    'app/Resources/translations',
    'var/export',
    'var/marco',
]);

set('bin_dir', 'bin');
set('var_dir', 'var');


task('deploy:assetic:dump', function () {
    if (!get('dump_assets')) {
        return;
    }
    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console assets:install --env={{env}} --no-debug {{release_path}}/web');
})->desc('Dump assets');
/*
task('database:update', function () {
    run('{{bin/php}} {{release_path}}/' . trim(get('bin_dir'), '/') . '/console doctrine:schema:update --force');
})->desc('Update database');


after('deploy', 'database:update');
*/

task('npm:install', function () {
    if (!get('dump_assets')) {
        return;
    }
    run('{{release_path}}/' . 'node/npm install');
})->desc('Dump assets');

//after('deploy', 'npm:install');
