<?php

namespace RestaurantBundle\Controller;

use DishBundle\Entity\CategoryDelegate;
use DishBundle\Entity\DishHistory;
use OrderBundle\Entity\Order;
use RestaurantBundle\Entity\ShopPlace;
use RestaurantBundle\Entity\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use RestaurantBundle\Form\Type\ShopType;
use RestaurantBundle\Form\Type\PlaceEditType;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


/**
 * Class ShopController
 * @package RestaurantBundle\Controller
 * @Route("/admin")
 */
class ShopController extends Controller
{


    /**
     * @Route("/shop/list", name="shop_list")
     */
    public function shopList()
    {
        //$shops = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findAll();
        $shops = $this->getUser()->getAdminShops();

        return $this->render("@Restaurant/Shop/shopList.html.twig", [
            'shops' => $shops
        ]);
    }

    /**
     * @Route("/shop/{id}/places/list", name="shop_places_list")
     */
    public function shopPlacesList(Shop $shop)
    {
        $places = $this->getDoctrine()->getRepository('ShopPlace')->findBy(["shop" => $shop]);

        dump($places);
        die();
    }

    /**
     * @Route("/shop/{id}/places/list", name="shop_places_list")
     */
    public function shopAddPlaceAction()
    {

    }

    /**
     * @Route("/shop/place/{id}/edit", name="shop_place_edit")
     */
    public function shopEditPlaceAction(Request $request, ShopPlace $place)
    {
        if (!$place) {
            die("123");
        }

        $form = $this->createForm(PlaceEditType::class, $place);

        $form->handleRequest($request);
        if ($form->isValid()) {

            foreach ($place->getTables() as $table) {
                $table->setPlace($place);
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($place);
            $em->flush();

            return $this->render('@Restaurant/Shop/placeEdit.html.twig', [
                'place' => $place,
                'form' => $form->createView(),
            ]);
        }

        return $this->render('@Restaurant/Shop/placeEdit.html.twig', [
            'place' => $place,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/shop/add", name="shop_add")
     */
    public function addShopAction(Request $request)
    {
        $shop = new Shop();
        $form = $this->createForm(ShopType::class, $shop, [

        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($shop);
            $em->flush();

            return $this->redirectToRoute('shop_edit', ['id' => $shop->getId()]);
        }

        return $this->render("@Restaurant/Shop/shopEdit.html.twig", [
            'form' => $form->createView(),
            'shop' => $shop,
            'changes' => [],
            'form_title' => 'Добавить заведение',
        ]);
    }

    /**
     * @Route("/shop/{id}/edit", name="shop_edit")
     */
    public function editShopAction(Request $request, $id)
    {
        /**
         * @var Shop $shop
         */
        $shop = $this->getDoctrine()->getRepository("RestaurantBundle:Shop")->findFullShop($id);

        if (!$shop) {
            dump("error");
            die();
        }
        $form = $this->createForm(ShopType::class, $shop);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                /**
                 * @var CategoryDelegate $delegate
                 */
                foreach ($shop->getDelegates() as $delegate) {
                    $delegate->setShop($shop);
                }


                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($shop);
                $em->flush();

                return $this->redirectToRoute('shop_edit', ['id' => $shop->getId()]);
            } else {
                dump($form->getErrors());
                die();
            }
        }

        $catDiscounts = $this->getDoctrine()->getRepository("DishBundle:CategoryDiscount")->findByShop($shop);
        $changes = $this->getDoctrine()->getRepository("DishBundle:DishHistory")->findBy([
            'shop' => $shop
        ], [
            'date' => "DESC"
        ], 30);

        return $this->render("@Restaurant/Shop/shopEdit.html.twig", [
            'form' => $form->createView(),
            'shop' => $shop,
            'changes' => $changes,
            'form_title' => 'Редактировать заведение',
            'catDiscounts' => $catDiscounts,
        ]);
    }

    /**
     * @Route("/realisation/search", name="realisation_search")
     */
    public function realisationSearchAction(Request $request)
    {
        $dateStart = (new \DateTime($request->get('dateStart')));
        $dateEnd = (new \DateTime($request->get('dateEnd')));
//
//        $dateStart = '17.03.2017';
//        $dateEnd = '27.03.2017';


        $shop = $this->getDoctrine()->getRepository('RestaurantBundle:Shop')->find($request->get('shop'));

        $periodStart = (new \DateTime($request->get('periodStart') . ":00"))->format('H:i:s');
        $periodEnd = (new \DateTime($request->get('periodEnd') . ":00"))->format('H:i:s');

//        $periodStart = '10:00:00';
//        $periodEnd = '12:00:00';


        $filteredData = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findFilteredData($shop, $dateStart, $dateEnd, $periodStart, $periodEnd);

        return new JsonResponse([
            'data' => $this->renderView('@Dish/Dish/filteredList.html.twig', [
                'filteredData' => $filteredData,
            ]),
        ]);
    }

    /**
     * @Route("/realisation/shop/{shop}/{dateStart}/{dateEnd}", name="shop_realisation", defaults={"dateStart" = null, "dateEnd" = null})
     */
    public function statByCatAndShopAction(Request $request, Shop $shop, $dateStart = "today", $dateEnd = null)
    {
        $dateStart = new \DateTime($dateStart);
        $dateEnd = new \DateTime($dateEnd);

        if (!$this->getUser()->getAdminShops()->contains($shop))
            throw new AccessDeniedHttpException();

        $dishes = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findGroupedDishesByPeriod($shop, $dateStart, $dateEnd);
        $delegated = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findGroupedDelegatedDishesByPeriod($shop, $dateStart, $dateEnd);
        $ingredients = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findIngredientsByPeriod($shop, $dateStart, $dateEnd);
        $writeoff = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findGroupedWriteoffDishesByPeriod($shop, $dateStart, $dateEnd);
        // $writeoffBar = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findGroupedWriteoffBarDishesByPeriod($shop, $dateStart, $dateEnd);

        $requests = $this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findGroupedRequestsDishesByPeriod($shop, $dateStart, $dateEnd);
        $clientDishes = $this->parseClientTableData($this->getDoctrine()->getRepository('OrderBundle:OrderDish')->findClientTableDishesByPeriod($shop, $dateStart, $dateEnd));

        $clientOrders = $this->getDoctrine()->getRepository(Order::class)->findClientOrdersByDate($shop, $dateStart, $dateEnd);

        return $this->render('@Dish/Dish/realisation.html.twig', [
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'shop' => $shop,
            'dishes' => $dishes,
            'delegated' => $delegated,
            'ingredients' => $ingredients,
            'writeoff' => $writeoff,
            'clientDishes' => $clientDishes,
            'clientOrders' => $clientOrders,
            'requests' => $requests,
            'filteredData' => [],
        ]);
    }

    private function parseClientTableData($clientDishes)
    {
        $clientData = [];

        foreach ($clientDishes as $clientDish) {
            $clientData[$clientDish['table']][] = $clientDish;
        }

        return $clientData;

    }

    /**
     * @Route("/shop/{id}/menu/constructor", name="menu_constructor")
     */
    public function menuConstructorAction(Request $request, Shop $shop, $date = "now")
    {
        $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findPublicMenuByShop($shop, $date);

        if ($request->getMethod() == "POST") {
            $shop->setMenuTemplate($request->get('menu'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($shop);
            $em->flush();
            return new JsonResponse('success');
        }

        $pendingChanges = $this->getDoctrine()->getRepository('DishBundle:DishHistory')->findByStatus(DishHistory::STATUS_NEW, "ASC", $shop, 999999);

        $pricesMixer = $this->get('restaurant.pending_price_mixer');

        foreach ($categories as $category)
            $pricesMixer->mixToCategory($category, $pendingChanges);


        return $this->render('@Restaurant/Shop/menuConstructor.html.twig', [
            'shop' => $shop,
            'categories' => $categories,
            'date' => $date,
        ]);
    }

    /**
     * @Route("/shop/{id}/menu/{date}", name="menu_print")
     */
    public function menuPrintAction(Request $request, Shop $shop, $date = "now")
    {
        $categories = $this->getDoctrine()->getRepository("DishBundle:DishCategory")->findMenuByShop($shop, $date);

        return $this->render('@Restaurant/Shop/printMenu.html.twig', [
            'shop' => $shop,
            'categories' => $categories,
            'date' => $date,
        ]);
    }


}
