<?php

namespace DishBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Ingredient
 * @package DishBundle\Entity
 * @ORM\Table(name="ingredients")
 * @ORM\Entity(repositoryClass="DishBundle\Entity\Repository\IngredientRepository")
 */
class Ingredient
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="unit", type="string", nullable=false)
     */
    private $unit;

    /**
     * @ORM\Column(name="should_be_validated", type="boolean", options={"default"="0"})
     */
    private $shouldBeValidated;

    /**
     * @ORM\Column(name="warehouse", type="float", nullable=true)
     */
    private $warehouse;

    /**
     * @ORM\OneToMany(targetEntity="DishBundle\Entity\DishIngredient", mappedBy="ingredient")
     */
    private $dishes;

    /**
     * @ORM\Column(name="id1c", type="string", nullable=true)
     */
    private $id1c;

    /**
     * @ORM\ManyToOne(targetEntity="DishBundle\Entity\DishCategory")
     */
    private $category;

    /**
     * @ORM\Column(name="is_deleted", type="boolean", options={"default": 0})
     */
    private $isDeleted;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dishes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isControlled = false;
        $this->isDeleted = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Ingredient
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add dish
     *
     * @param \DishBundle\Entity\Dish $dish
     *
     * @return Ingredient
     */
    public function addDish(\DishBundle\Entity\Dish $dish)
    {
        $this->dishes[] = $dish;

        return $this;
    }

    /**
     * Remove dish
     *
     * @param \DishBundle\Entity\Dish $dish
     */
    public function removeDish(\DishBundle\Entity\Dish $dish)
    {
        $this->dishes->removeElement($dish);
    }

    /**
     * Get dishes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDishes()
    {
        return $this->dishes;
    }

    /**
     * @return mixed
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param mixed $unit
     * @return Ingredient
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShouldBeValidated()
    {
        return $this->shouldBeValidated;
    }

    /**
     * @param mixed $shouldBeValidated
     * @return Ingredient
     */
    public function setShouldBeValidated($shouldBeValidated)
    {
        $this->shouldBeValidated = $shouldBeValidated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * @param mixed $warehouse
     * @return Ingredient
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getId1c()
    {
        return $this->id1c;
    }

    /**
     * @param mixed $id1c
     * @return Ingredient
     */
    public function setId1c($id1c)
    {
        $this->id1c = $id1c;
        return $this;
    }


    /**
     * @return DishCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Ingredient
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }


    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     * @return Ingredient
     */
    public function setIsDeleted(bool $isDeleted): Ingredient
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

}
