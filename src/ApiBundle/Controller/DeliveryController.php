<?php


namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use OrderBundle\Entity\Order;

class DeliveryController extends Controller
{

    public function getDeliveryOrdersAction(Request $request)
    {

        return new JsonResponse();
    }


    public function updateDeliveryOrderAction(Request $request, Order $order)
    {

        return new JsonResponse();
    }

}