<?php

namespace RestaurantBundle\Controller;


use RestaurantBundle\Entity\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WifiBundle\Entity\WifiClient;
use WifiBundle\Entity\WifiPromo;
use WifiBundle\Form\WifiPromoType;


/**
 * @Route("/admin")
 */
class WifiClientController extends Controller
{

    /**
     * @Route("/wifi-client/promo/{id}/delete", name="wifi_client_promo_delete")
     */
    public function wifiClientPromoDeleteAction(Request $request, WifiPromo $wifiPromo)
    {
        if (!$wifiPromo)
            throw new NotFoundHttpException();


        $wifiPromo->setIsDeleted(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($wifiPromo);
        $em->flush();

        return $this->redirectToRoute('wifi_client_list');
    }


    /**
     * @Route("/wifi-client/{id}/history/{dateStart}/{dateEnd}", name="wifi_client_history")
     */
    public function wifiClientHistoryAction(Request $request, WifiClient $wifiClient, $dateStart = 'yesterday', $dateEnd = 'today')
    {
        if (!$wifiClient)
            throw new NotFoundHttpException();

        $wifiConnects = $this->getDoctrine()->getRepository('WifiBundle:WifiConnect')->findByClient($wifiClient);


        return $this->render('@Restaurant/WifiClient/wifiClientHistory.html.twig', [
            'connects' => $wifiConnects,
            'client' => $wifiClient,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,

        ]);
    }

    /**
     * @Route("/wifi-client/{id}/unsubscribe", name="wifi_client_unsubscribe")
     */
    public function wifiClientUnsubscribeAction(Request $request, WifiClient $wifiClient)
    {
        if (!$wifiClient)
            throw new NotFoundHttpException();

        $em = $this->getDoctrine()->getManager();

        $wifiClient->setIsSubscribed(false);
        $em->persist($wifiClient);
        $em->flush();


        return new JsonResponse(true);
    }

    /**
     * @Route("/wifi-client/search", name="wifi_client_search")
     */
    public function wifiClientSearchAction(Request $request)
    {
        $phone = $request->get('phone');

        $clientsData = $this->getDoctrine()->getRepository('WifiBundle:WifiClient')->findByPhone($phone);

        $clients = [];

        foreach ($clientsData as $client) {
            $clients[] = [
                'id' => $client->getId(),
                'phone' => $client->getPhone(),
                'isSubscribed' => $client->getIsSubscribed(),
            ];
        }

        return new JsonResponse($clients);
    }

    /**
     * @Route("/wifi-client/list/{dateStart}/{dateEnd}", name="wifi_client_list")
     */
    public function wifiClientsListAction(Request $request, $dateStart = 'today', $dateEnd = 'tomorrow')
    {
        $data = [];


        $clients = $this->getDoctrine()->getRepository('WifiBundle:WifiConnect')->findGroupedClientsCount($dateStart, $dateEnd);
        $clientsBel = $this->getDoctrine()->getRepository('WifiBundle:WifiConnect')->findGroupedBelClientsCount($dateStart, $dateEnd);

        $promoSent = $this->getDoctrine()->getRepository('WifiBundle:WifiConnect')->findPromoCount($dateStart, $dateEnd);

        foreach ($clients as $client) {
            $data[$client['osName']]['count'] = $client['wcCount'];
            $data[$client['osName']]['promoCount'] = 0;
            $data[$client['osName']]['osId'] = $client['osId'];

        }

        foreach ($clientsBel as $client) {
            $data[$client['osName']]['countBel'] = $client['wcCount'];
            $data[$client['osName']]['promoCount'] = 0;
        }

        foreach ($promoSent as $_promoData) {
            $data[$_promoData['osName']]['promoCount'] = $_promoData['promoCount'];
        }


        $wifiPromo = new WifiPromo();
        $wifiPromoForm = $this->createForm(WifiPromoType::class, $wifiPromo);
        $wifiPromoForm->handleRequest($request);

        if ($wifiPromoForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($wifiPromo);
            $em->flush();
        }


        $wifiPromos = $this->getDoctrine()->getRepository('WifiBundle:WifiPromo')->findBy([
            'isDeleted' => false,
        ]);

        $__wifiPromosCount = $this->getDoctrine()->getRepository('WifiBundle:WifiPromo')->findSentPromos($dateStart, $dateEnd);

        $wifiPromosCount = [];
        foreach ($__wifiPromosCount as $item) {
            $wifiPromosCount[$item['wpId']] = $item['wpCount'];
        }

        return $this->render('@Restaurant/WifiClient/wifiClientList.html.twig', [
            'data' => $data,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'promos' => $wifiPromos,
            'promoForm' => $wifiPromoForm->createView(),
            'wifiPromosCount' => $wifiPromosCount,

        ]);
    }

    /**
     * @Route("/wifi-client/{shop}/{dateStart}/{dateEnd}", name="wifi_client_shop_list")
     */
    public function wifiClientsListShopAction(Request $request, Shop $shop, $dateStart = 'today', $dateEnd = 'tomorrow')
    {
        $clients = $this->getDoctrine()->getRepository('WifiBundle:WifiClient')->findClientsInShop($dateStart, $dateEnd, $shop);

        return $this->render('@Restaurant/WifiClient/wifiClientShopList.html.twig', [
            'clients' => $clients,
            'shop' => $shop,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);

    }

    /**
     * @Route("/wifi-client/{dateStart}/{dateEnd}", name="wifi_client_overall_list")
     */
    public function wifiClientsListOverallAction(Request $request, $dateStart = 'today', $dateEnd = 'tomorrow')
    {
        $clients = $this->getDoctrine()->getRepository('WifiBundle:WifiClient')->findClientsInShop($dateStart, $dateEnd);

        return $this->render('@Restaurant/WifiClient/wifiClientShopList.html.twig', [
            'clients' => $clients,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);

    }
}