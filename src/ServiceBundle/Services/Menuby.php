<?php

namespace ServiceBundle\Services;


use Symfony\Component\DependencyInjection\Container;

class Menuby
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function login()
    {

        $ch = curl_init('http://rest-api.menu.by:8022/api/login');
        $data = [
            "rest_id" => '1472',
            "pass" => md5(123456),
        ];

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = json_decode(curl_exec($ch));

        if ($res->status) {
            return $res->token;
        }

        return false;
    }

    public function receiveOrders($orders)
    {

    }

    public function getOrders()
    {
        $data = [
            "a" => '1472',
            "lng" => '6',
        ];

        $ch = $this->getCurl('http://rest-api.menu.by:8022/api/orders?' . http_build_query($data));

        $orders = json_decode(curl_exec($ch));

        return $orders;
    }

    private function getCurl($url)
    {
        $ch = curl_init($url);

        $token = file($this->container->get('kernel')->getRootDir() . '/config/menuby')[0];

        $authorization = "Authorization: Bearer $token";
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', $authorization]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return $ch;
    }

}