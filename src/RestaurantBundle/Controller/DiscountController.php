<?php

namespace RestaurantBundle\Controller;

use DishBundle\Entity\CategoryDiscount;
use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Form\Type\PromoCardType;
use RestaurantBundle\Form\Type\PromoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class DiscountController extends Controller
{
    /**
     * @Route("/category/discounts", name="category_discount_list")
     */
    public function categoryDiscountsAction(Request $request)
    {
        $discounts = $this->getDoctrine()->getRepository("DishBundle:CategoryDiscount")->findAll();
        $promos = $this->getDoctrine()->getRepository('RestaurantBundle:PromoCard')->findBy([
            'type' => PromoCard::TYPE_MANUAL
        ]);

        $promo = new PromoCard();
        $form = $this->createForm(PromoCardType::class, $promo, [
            'action' => $this->generateUrl('category_promo_add')
        ]);

        return $this->render('@Dish/Category/categoryDiscount.html.twig', [
            'discounts' => $discounts,
            'promos' => $promos,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/discount/{id}/delete", name="discount_delete")
     */
    public function deleteDiscountAction(Request $request, CategoryDiscount $discount)
    {
        $em = $this->getDoctrine()->getManager();

        $this->get('user.logger')->info($this->getUser()->getName() . ' delete discount ' . $discount->getDiscount() . ' from ' . $discount->getCategory()->getName());

        $em->remove($discount);
        $em->flush();


        return $this->redirectToRoute('category_discount_list');
    }

    /**
     * @Route("/discount/promo/{id}/delete", name="promo_delete")
     */
    public function deletePromoAction(Request $request, PromoCard $promo)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($promo);
        $em->flush();

        return $this->redirectToRoute('category_discount_list');
    }

    /**
     * @Route("/discount/promo/add", name="category_promo_add")
     */
    public function categoryAddPromoAction(Request $request)
    {
        $promo = new PromoCard();
        $form = $this->createForm(PromoType::class, $promo, [
            'action' => $this->generateUrl('category_promo_add')
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $promo->setType(PromoCard::TYPE_MANUAL);
            $em = $this->getDoctrine()->getManager();
            $em->persist($promo);
            $em->flush();
        }

        return $this->redirectToRoute('category_discount_list');
    }

    /**
     * @Route("/discount/promo/{id}/listorders", name="promoted_order")
     */
    public function promotedOrdersAction(Request $request)
    {

        return $this->redirectToRoute('category_discount_list');
    }

}