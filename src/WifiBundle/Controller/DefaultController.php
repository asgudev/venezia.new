<?php

namespace WifiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WifiBundle\Entity\WifiClient;
use WifiBundle\Entity\WifiConnect;
use WifiBundle\Form\GetCodeType;
use WifiBundle\Form\LoginType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="wifi_index")
     */
    public function defaultAction(Request $request)
    {
        return $this->render('WifiBundle:Default:index.html.twig');
    }

    /**
     * @Route("/register", name="wifi_register")
     * @Route("/register/", name="wifi_register2")
     */
    public function registerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $phoneForm = $this->createForm(GetCodeType::class);

        $phoneForm->handleRequest($request);
        if ($phoneForm->isSubmitted() && $phoneForm->isValid()) {
            $phone = preg_replace("#[^0-9+]#", "", $phoneForm->get('phone')->getData());
            //TODO validate phone

//            if ($wifiClient->getPhone() && ($wifiClient->getPhone() != $phone)) {
//                $wifiClient = clone $wifiClient;
//                $em->persist($wifiClient);
//            }

            $wifiClient = $this->getDoctrine()->getRepository('WifiBundle:WifiClient')->findByMac($phoneForm->get('mac')->getData());

            if (!$wifiClient) $wifiClient = new WifiClient($phoneForm->get('mac')->getData());


            $wifiClient->setPhone($phone);

            if (substr($phone, 0, 4) == "+375") {
                $this->get('wifi.sms.service')->sendCode($wifiClient);
            } else if (substr($phone, 0, 3) == "+79") {
                $wifiClient->setCode(1234);
                $this->get('wifi.smsc.service')->sendCode($wifiClient);
            } else {
                $wifiClient->setCode(1234);
                $this->get('wifi.smsc.service')->sendCode($wifiClient);
            }

            $em->persist($wifiClient);
            $em->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse('success');
            } else {
                return $this->redirectToRoute('wifi_login', $this->get('session')->get('wifidog'));
            }
        }


        return $this->redirectToRoute('wifi_login', $this->get('session')->get('wifidog'));
    }

    /**
     * @Route("/error", name="wifi_error")
     */
    public function errorAction(Request $request)
    {


        return $this->render('WifiBundle:Default:index.html.twig');
    }


    /**
     * @Route("/login", name="wifi_login")
     * @Route("/login/", name="wifi_login2")
     */
    public function loginAction(Request $request)
    {
        //http://wifi.venezia.front/login?gw_address=192.168.77.1&gw_id=225613237A05&gw_port=2060&mac=d0%3Ad2%3Ab0%3A2b%3A97%3A54&url=http%3A%2F%2Fcaptive.apple.com%2Fhotspot-detect.html
        $em = $this->getDoctrine()->getManager();

        $wifiRouter = $this->getDoctrine()->getRepository('WifiBundle:WifiRouter')->findOneBy([
            'routerId' => $request->get('gw_id'),
            'isEnabled' => true,
        ]);

        $wifiClient = $this->getDoctrine()->getRepository('WifiBundle:WifiClient')->findByMac($request->get('mac'));

        if ($wifiClient) {
            if ($wifiClient->getIsActive()) {
                $wifiConnect = new WifiConnect($wifiClient, $wifiRouter);
                $this->get('wifi.promo.service')->promo($wifiConnect);
                $em->persist($wifiConnect);
                $em->flush();

                $url = 'http://' . $request->get('gw_address') . ':' . $request->get('gw_port') . '/wifidog/auth?token=' . $wifiClient->getToken();
                return $this->redirect($url);
            }
            if ($wifiClient->getIsBanned()) {
                die('banned');
            }
        } else {
            $wifiClient = new WifiClient($request->get('mac'));
        }

        $phoneForm = $this->createForm(GetCodeType::class);
        $phoneForm->get('mac')->setData($request->get('mac'));
        $phoneForm->handleRequest($request);
        if ($phoneForm->isSubmitted() && $phoneForm->isValid()) {
            $phone = preg_replace("#[^0-9+]#", "", $phoneForm->get('phone')->getData());

            $wifiClient->setPhone($phone);

            if (substr($phone, 0, 4) == "+375") {
                $this->get('wifi.sms.service')->sendCode($wifiClient);
            } else if (substr($phone, 0, 3) == "+79") {
                $wifiClient->setCode(1234);
                $this->get('wifi.smsc.service')->sendCode($wifiClient);
            } else {
                $wifiClient->setCode(1234);
                $this->get('wifi.sms.service')->sendCode($wifiClient);
            }

            $em->persist($wifiClient);
            $em->flush();

            return $this->redirectToRoute('wifi_login');
        }

        $error = false;

        $loginForm = $this->createForm(LoginType::class);
        $loginForm->handleRequest($request);
        if ($loginForm->isSubmitted() && $loginForm->isValid()) {
            $code = $loginForm->get('code')->getData();

            if ($wifiClient->getCode() !== $code) {
                $error = 'Неверный код';
            } else {
                $wifiClient->setIsActive(true);

                $wifiConnect = new WifiConnect($wifiClient, $wifiRouter);
                $this->get('wifi.promo.service')->promo($wifiConnect);
                $em->persist($wifiConnect);
                $em->flush();

                $url = 'http://' . $request->get('gw_address') . ':' . $request->get('gw_port') . '/wifidog/auth?token=' . $wifiClient->getToken();
                return $this->redirect($url);
            }
        }

        $wifiClient->setMac($request->get('mac'));
        $em->persist($wifiClient);
        $em->flush();


        return $this->render('WifiBundle:Default:login_marco.html.twig', [
            'wifiRouter' => $wifiRouter,
            'loginForm' => $loginForm->createView(),
            'phoneForm' => $phoneForm->createView(),
            'queryString' => http_build_query($request->query->all()),
            'error' => $error,
        ]);

    }

    /**
     * @Route("/auth", name="wifi_auth")
     * @Route("/auth/", name="wifi_auth2")
     */
    public function authAction(Request $request)
    {
        return new Response('Auth: 1');

        // return $this->render('WifiBundle:Default:index.html.twig');
    }

    /**
     * @Route("/ping", name="wifi_ping")
     * @Route("/ping/", name="wifi_ping2")
     */
    public function pingAction(Request $request)
    {
        return new Response('Pong');
    }


    /**
     * @Route("/portal", name="wifi_portal")
     * @Route("/portal/", name="wifi_portal2")
     */
    public function portalAction(Request $request)
    {
        $blocked = [
            'captive.apple.com'
        ];

        $isBlocked = false;

        $redirectUrl = $this->get('session')->get('wifidog')['url'];
        foreach ($blocked as $__url) {
            if (strpos($redirectUrl, $__url) !== false) {
                $isBlocked = true;
            }
        }

        $wifiRouter = $this->getDoctrine()->getRepository('WifiBundle:WifiRouter')->findOneBy([
            'routerId' => $this->get('session')->get('wifidog')['gw_id'],
            'isEnabled' => true,
        ]);


        return $this->render('@Wifi/Default/portal.html.twig', [
            'redirectUrl' => $redirectUrl,
            'wifiRouter' => $wifiRouter,
            'isBlocked' => $isBlocked,
        ]);

        //show captive portal or redirect
    }

    /**
     * @Route("/message", name="wifi_message")
     * @Route("/message/", name="wifi_message2")
     */
    public function gwMessageAction(Request $request)
    {
        return new Response($request->get('message'));
    }

    /**
     * @Route("/clear", name="wifi_clear")
     */
    public function clearAction(Request $request)
    {
        $this->get('session')->invalidate(true);
        return new Response();
    }


}

//https://sexvolg.vip/core/api/51b6076ed7a7f544f55a9f5868fc8359.php?id=3505&sum=1&crc=df591d2ebc044d39a5aa89b6797dcfa6
