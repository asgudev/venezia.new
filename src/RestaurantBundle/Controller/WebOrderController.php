<?php


namespace RestaurantBundle\Controller;

use FrontendBundle\Entity\WebOrder;
use RestaurantBundle\Entity\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @package RestaurantBundle\Controller
 * @Route("/admin")
 */
class WebOrderController extends Controller
{
    /**
     * @Route("/weborder/{id}/list", name="web_order_list")
     */
    public function webOrdersListAction(Request $request, Shop $shop)
    {
        if (!$shop)
            throw new NotFoundHttpException();

        $orders = $this->getDoctrine()->getRepository("FrontendBundle:WebOrder")->findPayed();

        return $this->render("@Restaurant/WebOrder/list.html.twig", [
            "orders" => $orders,
        ]);
    }

    /**
     * @Route("/weborder/{id}/show", name="web_order_show")
     */
    public function showWebOrderAction(Request $request, WebOrder $order)
    {
        if (!$order)
            throw new NotFoundHttpException();

        return $this->render("@Restaurant/WebOrder/orderShow.html.twig", [
            'order' => $order,
        ]);
    }
}