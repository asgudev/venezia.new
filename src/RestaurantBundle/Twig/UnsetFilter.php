<?php

namespace RestaurantBundle\Twig;


class UnsetFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('unset', array($this, 'unsetElement')),
        );
    }

    public function unsetElement($input, $element)
    {
        $__arr = $input;
        unset($__arr[$element]);
        return $__arr;
    }

    public function getName()
    {
        return 'unset';
    }

}