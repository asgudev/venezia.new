<?php

namespace DishBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use DishBundle\Entity\DishHistory;
use RestaurantBundle\Entity\Shop;

class DishHistoryRepository extends EntityRepository
{
    /**
     * @return DishHistory[]
     */
    public function findNewChanges()
    {
        return $this->createQueryBuilder('dh')
            ->where('dh.date > :date')
            ->andWhere('dh.status = :status')
            ->setParameters([
                'date' => (new \DateTime())->modify('-1 hour'),
                'status' => DishHistory::STATUS_NEW,
            ])
            ->getQuery()
            ->getResult();
    }


    /**
     * @return DishHistory[]
     */
    public function findByStatus($status, $order = 'DESC', Shop $shop = null, $limit = 100)
    {
        $changes = $this->createQueryBuilder('dh')
            ->addSelect('d')
            ->addSelect('dm')
            ->leftJoin('dh.dish', 'd')
            ->leftJoin('d.dishMetas', 'dm')
            ->where('dh.status = :status')
            ->setParameters([
                'status' => $status,
            ]);

        if ($shop) {
            $changes
                ->andWhere('dh.shop = :shop OR dh.shop is null')
                ->setParameter('shop', $shop);
        }

        return $changes->orderBy('dh.date', $order)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}