function Place() {
    this.id = 0;
    this.name = null;
    this.container = null;

    this.tables = [];

    this.addTable = function (table) {
        this.tables.push(table);
    }
}

function Table() {
    const STATUS_FREE = -1;
    const STATUS_WAIT_FOR_KITCHEN = 1;
    const STATUS_BUSY = 0;
    const STATUS_WAIT_FOR_MONEY = 3;

    const SVGNS = "http://www.w3.org/2000/svg";

    this.id = 0;
    this.name = null;
    this.place = null;
    this.context = null;

    this.coords = {
        x: 0, y: 0, w: 1, h: 1
    };

    this.status = STATUS_FREE;
    this.orderedAt = null;

    this.orderId = null;
    this.orderIdLocal = null;
    this.waiter = null;


    this.svgTable = null;
    this.svgTableObject = null;
    this.rect = null;

    this.removeOrder = function () {
        this.orderId = null;
        this.orderIdLocal = null;
        this.waiter = null;
        this.status = STATUS_FREE;

        this.drawTable();
    };

    this.setOrder = function (data) {
        this.orderId = data.orderId;
        this.orderIdLocal = data.orderIdLocal;
        this.waiter = data.waiter;
        this.status = data.status;

        this.drawTable();
    };


    this.drawTable = function () {
        if (this.svgTableObject === null) {
            this.svgTableObject = {
                'group': document.createElementNS(SVGNS, 'g'),
                'rect': document.createElementNS(SVGNS, 'rect'),
                'name': this.createSvgText(this.name, {
                    x: this.coords.x + 5,
                    y: this.coords.y + 10,
                }),
                'order': this.createSvgText('test', {
                    x: this.coords.x + 5,
                    y: this.coords.y + 30,
                }),
                'waiter': this.createSvgText('test', {
                    x: this.coords.x + 5,
                    y: this.coords.y + 50,
                    w: this.coords.w - 10,
                }, true),
            };
        }

        this.svgTableObject.rect.setAttributeNS(null, 'x', this.coords.x);
        this.svgTableObject.rect.setAttributeNS(null, 'y', this.coords.y);
        this.svgTableObject.rect.setAttributeNS(null, 'height', this.coords.h);
        this.svgTableObject.rect.setAttributeNS(null, 'width', this.coords.w);
        this.svgTableObject.rect.setAttributeNS(null, 'fill', '#' + this.getColor(this.status));


        this.svgTableObject.waiter.textContent = this.waiter ? this.waiter : '';
        this.svgTableObject.order.textContent = this.orderIdLocal ? 'Счет №' + this.orderIdLocal : '';


        if (this.svgTable === null) {
            this.svgTable = this.svgTableObject.group;
            this.svgTable.appendChild(this.svgTableObject.rect);
            this.svgTable.appendChild(this.svgTableObject.name);
            this.svgTable.appendChild(this.svgTableObject.order);
            this.svgTable.appendChild(this.svgTableObject.waiter);
            this.place.container.appendChild(this.svgTable);

            $(this.svgTableObject.group).on("click", {
                context: this,
            }, this.tableClick);
        }
    };

    this.reloadOrderForm = function ($form, data) {
        //$('#order_table_row select')[0].selectize.disable();
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data: data,
            objectData: data,
            success: function (html) {
                $('#order_table_row').replaceWith($(html).find('#order_table_row'));
                $("#order_place").val(this.objectData['order[place]']);

                if (this.objectData.table !== undefined) {
                    $("#order_table").val(this.objectData.table);
                }
            }
        });
    };

    this.tableClick = function (event) {
        var context = event.data.context;

        if (context.status == STATUS_FREE) {
            closeOrder();
            /*if ($("#order_place").val() == context.place.id) {

             $("#order_table").val(context.id);
             } else {
             var data = {};
             data['order[place]'] = context.place.id;

             context.reloadOrderForm($("#order_form"), {
             'order[place]': context.place.id,
             'table': context.id,
             });
             }
             */
            $('#place_name_label').text(context.place.name);
            $('#table_name_label').text(context.name);

            $('#place_name').val(context.place.id);
            $('#table_name').val(context.id);

            $('#order_save_order').removeClass("disabled");

            $("#table_control").toggle();
            $("#order_control").toggle();


            current_table_id = context.id;
        } else {
            $.ajax({
                url: '/order/' + context.orderId + '/edit',
                method: "GET",
                success: function (response) {
                    $("#custom_order").replaceWith(response);
                    $("#custom_order").show();
                    $("#custom_order").data("status", 1);

                    $("#table_control").toggle();
                    $("#order_control").toggle();
                    $("#new_order").hide();
                }
            });
        }
    };

    this.createSvgText = function (text, coordinates, stretch) {
        var svg_text = document.createElementNS(SVGNS, 'text');
        svg_text.setAttributeNS(null, 'x', coordinates.x);
        svg_text.setAttributeNS(null, 'y', coordinates.y);
        if (stretch == true) {
            svg_text.setAttributeNS(null, 'textLength', coordinates.w);
            svg_text.setAttributeNS(null, 'lengthAdjust', 'spacingAndGlyphs');
        }
        svg_text.setAttributeNS(null, 'alignment-baseline', 'middle');
        svg_text.setAttribute('fill', '#000');
        svg_text.textContent = text;
        return svg_text;
    };

    this.getColor = function (status) {
        switch (status) {
            case (STATUS_FREE):
                return 'baf7ba';
                break;
            case (STATUS_BUSY):
                return 'ff9393';
                break;
        }
    };

}
/*
 Table.prototype = {
 draw: function (context, optionalColor) {

 switch (this.status) {
 case STATUS_FREE:
 this.context.fillStyle = 'rgba(156,244,156,0.7)';
 break;
 case STATUS_WAIT_FOR_KITCHEN:
 this.context.fillStyle = 'rgba(33,150,243,0.7)';
 break;
 case STATUS_BUSY:
 this.context.fillStyle = 'rgba(255,0,0,0.7)';
 break;
 case STATUS_WAIT_FOR_MONEY:
 this.context.fillStyle = 'rgba(220,205,65,0.7)';
 break;
 default:
 this.context.fillStyle = 'rgba(220,205,65,0.7)';
 }
 this.context.fillRect(this.coords.x, this.coords.y, this.coords.w, this.coords.h);

 this.print_text();
 },
 print_text: function () {
 this.context.textBaseline = "middle";
 this.context.lineWidth = 1;
 this.context.fillStyle = "#000";
 this.context.font = "12pt sans-serif";

 var text = this.name;

 textX = this.coords.x + this.coords.w / 2 - this.context.measureText(text).width / 2;
 textY = this.coords.y + this.coords.h / 2;
 this.context.fillText(text, textX, textY - 8);

 if (this.status == STATUS_WAIT_FOR_KITCHEN) {
 var time = Math.ceil(((new Date()) - this.orderedAt) / 1000);
 time = time.toString().toHHMMSS();
 textX = this.coords.x + this.coords.w / 2 - this.context.measureText(time).width / 2;
 textY = this.coords.y + this.coords.h / 2;
 this.context.fillText(time, textX, textY + 8);
 }
 }

 };

 String.prototype.toHHMMSS = function () {
 var sec_num = parseInt(this, 10); // don't forget the second param
 var minutes = Math.floor(sec_num / 60);
 var seconds = sec_num - (minutes * 60);

 if (minutes < 10) {
 minutes = "0" + minutes;
 }
 if (seconds < 10) {
 seconds = "0" + seconds;
 }
 return (minutes + ':' + seconds);
 };

 function addTable(id, name, coords, place_id, context) {
 var table = new Table;

 table.id = id;
 table.coords = coords;
 table.name = name;
 table.context = context;
 table.place_id = place_id;

 tables.push(table);
 }

 function setTableStatus(table, status, orderedAt) {
 table.status = status;
 table.orderedAt = orderedAt;
 }

 function getTableById(id) {
 var rtable = null;
 tables.forEach(function (table, index) {
 if (table.id == id) {
 rtable = table;
 }
 });
 return rtable;
 }

 function clearTablesStatus() {
 tables.forEach(function (table, index) {
 table.status = 0;
 table.orderedAt = null;
 });
 }

 function updateTables() {
 clearTablesStatus()
 $("#order_list tr").each(function () {
 var data = $(this).data();
 if ($.isEmptyObject(data) == false) {
 setTableStatus(getTableById(data.table), data.status, new Date(data.createdat));
 }
 });
 }

 function reloadOrderList() {
 $.ajax({
 url: "/terminal",
 method: "GET",
 success: function (response) {
 $("#order_list").replaceWith($(response).find("#order_list"));
 updateTables();
 }
 });
 }*/