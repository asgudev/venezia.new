var fs = require('fs'),
    request = require('request'),
    HID = require('node-hid');


var dataStr = '';
var dataArray = {
    30: 1,
    31: 2,
    32: 3,
    33: 4,
    34: 5,
    35: 6,
    36: 7,
    37: 8,
    38: 9,
    39: 0,
};

function Barcode(io, config) {
    return;
    console.log(HID.devices());


    try {
        var device = new HID.HID(5050, 24);

        device.on('data', function (data) {
            var p = data.readInt8(2);

            if (p) {
                if (p.toString() == '40') {
                    console.log(dataStr);
                    io.emit("scanner_data", dataStr);
                    dataStr = '';
                } else {
                    dataStr = dataStr + dataArray[p.toString()];
                }
            }
        });
    } catch (ex) {
        console.log(ex);
        io.emit("scanner_error", 'error');

    }
}


module.exports = Barcode;
