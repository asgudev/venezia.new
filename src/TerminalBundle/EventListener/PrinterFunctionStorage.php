<?php

namespace TerminalBundle\EventListener;

use ControlBundle\Entity\DishPhoto;
use Doctrine\ORM\EntityManager;
use Exception;
use IntlDateFormatter;
use OrderBundle\Entity\Order;
use OrderBundle\Entity\OrderDish;
use RestaurantBundle\Entity\Printer;
use RestaurantBundle\Entity\Shop;
use RestaurantBundle\Service\KEscPos;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class PrinterFunctionStorage
{

    const DIRECT_ADDED = 0;
    const DIRECT_DELETED = 1;
    private $em;
    private $container;

    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function printDirect(Order $order, $dishes, $type = self::DIRECT_ADDED)
    {
        $splitOrder = [];
        if ($type == self::DIRECT_ADDED) {

            /**
             * @var OrderDish $dish
             */
            foreach ($dishes as $dish) {
                $printer = $dish->getDish()->getCategory()->getShopPrinter($order->getShop());

                if (($order->getShop()->getId() == 13) && ($dish->getDish()->getId() == 11160)) {
                    $printer = new Printer();
                    $printer->setName('bowling');
                }

                if ($printer == null) {
                    $printer = new Printer();
                    $printer->setName($order->getShop()->getAdminPrinter());
                }
                $splitOrder[$printer->getName()]['dishes'][] = $dish;
                $splitOrder[$printer->getName()]['printer'] = $printer;
            }


            foreach ($splitOrder as $printer => $data) {
                $this->printOrder($data['printer'], $order, $data['dishes']);
            }
        } elseif ($type == self::DIRECT_DELETED) {

            /**
             * @var OrderDish $dish
             */
            foreach ($dishes as $dish_data) {
                if (array_key_exists('dish', $dish_data)) {
                    $dish = $dish_data['dish'];
                    $diff = $dish_data['diff'];
                } else {
                    $dish = $dish_data;
                    $diff = $dish_data->getQuantity();
                }
                $printer = $dish->getDish()->getCategory()->getShopPrinter($order->getShop());

                if (($order->getShop()->getId() == 13) && ($dish->getDish()->getId() == 11160)) {
                    $printer = new Printer();
                    $printer->setName('bowling');
                }

                if ($printer == null) {
                    $printer = new Printer();
                    $printer->setName($order->getShop()->getAdminPrinter());
                }
                if ($printer == 'none') continue;
                $splitOrder[$printer->getName()]['dishes'][] = [
                    'dish' => $dish,
                    'quantity' => $diff,
                ];
                $splitOrder[$printer->getName()]['printer'] = $printer;
            }


            foreach ($splitOrder as $printer => $data) {
                $this->printOrderCanceledDishes($data['printer'], $order, $data['dishes']);
            }
        }
    }

    public function printOrder(Printer $printer, Order $order, $dishes)
    {
        try {
            $escpos = new KEscPos($order->getShop()->getId() == 12 ? 'RONGTA' : 'TM-T88IV AFU', $printer->getName(), true, true);
        } catch (Exception $ex) {
            error_log("Printer not found " . $printer->getName());
            return;
        }

        if ($order->getUser()->getId() == 1 && $order->getMeta() != null) {
            $escpos->Line($order->getMeta()->getComment());
        }


        $formatter = new IntlDateFormatter('ru_RU', IntlDateFormatter::FULL, IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM Y г. H:mm');
        $date = $formatter->format(new \DateTime('now'));

        $escpos->Font("A");
        $escpos->Align("center");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        $escpos->Line("Заказ №" . $order->getLocalId() . ".1");
        $escpos->Line($order->getShop()->getName());
        $escpos->Double(true);
        $escpos->Height('normal');
        $escpos->Font("B");
        $escpos->XFeed();
        $escpos->Line($date);
        $escpos->Font("B");
        $escpos->Align("left");
        $escpos->Line("Официант: " . $order->getUser()->getName());
        $escpos->Height('double');
        $escpos->Line("Стол: " . $order->getTable()->getName() . " (" . $order->getTable()->getPlace()->getName() . ")");

        if ($order->getMeta() != null) {
            if ($order->getMeta()->getTime() != "")
                $escpos->Line("Приготовить к: " . $order->getMeta()->getTime());

            if ($order->getMeta()->getPhone() != "")
                $escpos->Line("Телефон: " . $order->getMeta()->getPhone());

            if ($order->getMeta()->getComment() != "")
                $escpos->Line("Комментарий: " . $order->getMeta()->getComment());
        }

        $escpos->Height('normal');
        $escpos->Line("----------------------------------------");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Align('left');
        //$escpos->Line("    Наименование                           К-во      ");
        $escpos->Font("A");
        $escpos->Double(true);

        $groupedByTime = [];

        foreach ($dishes as $dish) {
            $groupedByTime[$dish->getTime()][] = $dish;
        }

        krsort($groupedByTime);


        foreach ($groupedByTime as $time => $dishes) {
            $escpos->Font("A");
            $escpos->Align('center');
            $escpos->Double(true);
            $escpos->Line("Заказ №" . $order->getLocalId());

            $escpos->Line($time . ' очередь');

            $escpos->Align("left");
            $escpos->Height('normal');
            $escpos->Line("Официант: " . $order->getUser()->getName());
            $escpos->Height('double');
            $escpos->Line("Стол: " . $order->getTable()->getName() . " (" . $order->getTable()->getPlace()->getName() . ")");

            $escpos->Align('left');
            $escpos->Height('double');

            /**
             * @var OrderDish $dish
             */
            foreach ($dishes as $dish) {
                $dishName = strtr($dish->getDish()->getName(), [' ' => ' ',]);
                if ($dish->getParentDish() !== null) $dishName = "   " . $dishName;
                $dish_line = $dishName . '-' . $dish->getQuantity();
                $escpos->Line($dish_line);

                /*if ($printer->getTransliterate()) {
                    $dishEntity = $dish->getDish();
                    $dishEntity->setTranslatableLocale('it');
                    $this->em->refresh($dishEntity);
                    $dish_line = ($dishEntity->getName()) . '-' . $dish->getQuantity();
                    $escpos->Line($dish_line);
                    $escpos->Line("---------------------------------");
                }*/

            }
            $escpos->Line("");
            $escpos->Line("---------------------------------");
            $escpos->Line("");
            $escpos->Cut();

        }
        $escpos->Close();
    }

    public function printOrderCanceledDishes(Printer $printer, Order $order, $dishes)
    {

        try {
            $escpos = new KEscPos($order->getShop()->getId() == 12 ? 'RONGTA' : 'TM-T88IV AFU', $printer->getName(), true, true);
        } catch (Exception $ex) {
            error_log("Printer not found " . $printer->getName());
            return;
        }

        $formatter = new IntlDateFormatter('ru_RU', IntlDateFormatter::FULL, IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM Y г. H:mm');
        $date = $formatter->format(new \DateTime('now'));

        $escpos->Font("A");
        $escpos->Align("center");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        $escpos->Line("Заказ №" . $order->getLocalId() . ".1");
        $escpos->Double(false);
        $escpos->Height('normal');
        $escpos->Font("B");
        $escpos->XFeed();
        $escpos->Line($date);
        $escpos->Font("B");
        $escpos->Align("left");
        $escpos->Line("Официант: " . $order->getUser()->getName());
        $escpos->Line("Стол: " . $order->getTable()->getName() . " (" . $order->getTable()->getPlace()->getName() . ")");

        $escpos->Line("----------------------------------------");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Align('left');
        //$escpos->Line("    Наименование                           К-во      ");
        $escpos->Font("A");
        $escpos->Double(true);

        foreach ($dishes as $dish_data) {
            $dish = $dish_data['dish'];
            $quantity = $dish_data['quantity'];

            $dishName = strtr($dish->getDish()->getName(), [' ' => ' ',]);

            $dish_line = $dishName . ' - отмена ' . $quantity;
            $escpos->Line($dish_line);
            /*if ($printer->getTransliterate()) {
                $dishEntity = $dish->getDish();
                $dishEntity->setTranslatableLocale('it');
                $this->em->refresh($dishEntity);
                $dish_line = ($dishEntity->getName()) . ' - отмена ' . $quantity;
                $escpos->Line($dish_line);
                $escpos->Line("---------------------------------");
            }*/

        }


        $escpos->Cut();
        $escpos->Close();
    }

    public function webOrderKitchensPrint(Order $order, $extra)
    {
        $splitOrder = [];
        $separateByCatOrder = [];
        $delegates = [];
        foreach ($order->getDishes() as $dish) {
            $printer = $dish->getDish()->getCategory()->getShopPrinter($order->getShop());
            if ($printer == null) {
                $printer = new Printer();
                $printer->setName($order->getShop()->getAdminPrinter());
            }
            if (($printer == 'none') || ($printer == null)) continue;
            /*
                        if ($dish->getDelegate() !== null) {
                            $delegates[$printer->getName()]['dishes'][] = $dish;
                            $delegates[$printer->getName()]['printer'] = $printer;
                            continue;
                        }
            */
            if ($this->container->getParameter('shop.split_dishes')) {
                $separateByCatOrder[$printer->getName()]['dishes'][$dish->getDish()->getCategory()->getId()][] = $dish;
                $separateByCatOrder[$printer->getName()]['printer'] = $printer;
            } else {
                $splitOrder[$printer->getName()]['dishes'][] = $dish;
                $splitOrder[$printer->getName()]['printer'] = $printer;
            }

        }
        /*
                foreach ($delegates as $printer => $data) {
                    $this->functions->printDelegatedOrder($data['printer'], $order, $data['dishes']);
                }*/

        if ($this->container->getParameter('shop.split_dishes')) {
            foreach ($separateByCatOrder as $printer => $data) {
                foreach ($data['dishes'] as $cat => $dishes) {
                    $this->printWebOrder($data['printer'], $order, $dishes, $extra);
                }

            }
        } else {
            foreach ($splitOrder as $printer => $data) {
                $this->printWebOrder($data['printer'], $order, $data['dishes'], $extra);
            }
        }
    }

    public function printWebOrder(Printer $printer, Order $order, $dishes, $extra)
    {
        try {
            $escpos = new KEscPos($order->getShop()->getId() == 12 ? 'RONGTA' : 'TM-T88IV AFU', $printer->getName(), true, true);
        } catch (Exception $ex) {
            error_log("Printer not found " . $printer->getName());
            return;
        }

        $formatter = new IntlDateFormatter('ru_RU', IntlDateFormatter::FULL, IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM Y г. H:mm');
        $date = $formatter->format($order->getCreatedAt());

        $escpos->Font("A");
        $escpos->Align("center");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        $escpos->Line("Заказ с сайта №" . $extra->id . ".1");
        $escpos->Double(false);
        $escpos->Height('normal');
        $escpos->Font("B");
        $escpos->XFeed();
        $escpos->Align('left');
        $escpos->Line('Создан: ' . $date);

        $escpos->Line('Телефон: ' . $extra->phone);
//        $escpos->Line('Адрес: ' . $order->getAddress());
        $escpos->Line('Комментарий: ' . $extra->comment);
        $escpos->Line('Время: ' . $extra->pickupTime);

        $escpos->Font("B");
        $escpos->Align("left");
        /*$escpos->Line("Официант: " . $order->getUser()->getName());*/


        $escpos->Line("----------------------------------------");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Align('left');
        //$escpos->Line("    Наименование                           К-во      ");
        $escpos->Font("A");
        $escpos->Double(true);

        $groupedByTime = [];


        foreach ($dishes as $dish) {
            $dishName = strtr($dish->getDish()->getName(), [' ' => ' ',]);
            $dish_line = $dishName . '-' . $dish->getQuantity();
            $escpos->Line($dish_line);

            /*if ($printer->getTransliterate()) {
                $dishEntity = $dish->getDish();
                $dishEntity->setTranslatableLocale('it');
                $this->em->refresh($dishEntity);
                $dish_line = ($dishEntity->getName()) . '-' . $dish->getQuantity();
                $escpos->Line($dish_line);
                $escpos->Line("---------------------------------");
            }*/

        }
        $escpos->Line("");
        $escpos->Line("---------------------------------");
        $escpos->Line("");
        $escpos->Cut();


        $escpos->Close();
    }

    public function printDeliveryTicketOrder(Shop $shop, DishPhoto $orderDishPhoto)
    {
        try {
            $escpos = new KEscPos('TM-T88IV AFU', $shop->getAdminPrinter(), true, true);
        } catch (Exception $ex) {
            error_log("Printer not found " . $shop->getAdminPrinter());
            return;
        }
        $orderDish = $orderDishPhoto->getOrderDish();

        $formatter = new IntlDateFormatter('ru_RU', IntlDateFormatter::FULL, IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM Y г. H:mm');
        $date = $formatter->format(new \DateTime('now'));

        $escpos->Font("A");
        $escpos->Align("center");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        $escpos->Line("Заказ №" . $orderDish->getOrder()->getLocalId());
        $escpos->Line($shop->getName());
        $escpos->Line($shop->getAddress());
        $escpos->Align("left");
        $escpos->Line($orderDish->getDish()->getName());
        $escpos->Line("Получен " . $orderDish->getOrder()->getCreatedAt()->format("d.m.Y H:i:s"));
        $escpos->Line("Приготовлен " . $orderDishPhoto->getDate()->format("d.m.Y H:i:s"));


        $escpos->XFeed();
        $escpos->Line($date);

        $escpos->Line("");
        $escpos->Line("---------------------------------");
        $escpos->Line("");
        $escpos->Cut();

        $escpos->Close();
    }

    public function printDishes(Printer $printer, Order $order, $dishes)
    {
        try {
            $escpos = new KEscPos($order->getShop()->getId() == 12 ? 'RONGTA' : 'TM-T88IV AFU', $printer->getName(), true, true);
        } catch (Exception $ex) {
            error_log("Printer not found " . $printer->getName());
            return;
        }

        $this->printOrderHeader($escpos, $order);

        if ($order->getMeta() != null) {
            if ($order->getMeta()->getTime() != "")
                $escpos->Line("Приготовить к: " . $order->getMeta()->getTime());

            if ($order->getMeta()->getPhone() != "")
                $escpos->Line("Телефон: " . $order->getMeta()->getPhone());

            if ($order->getMeta()->getComment() != "")
                $escpos->Line("Комментарий: " . $order->getMeta()->getComment());
        }


        $escpos->Align('left');
        $escpos->Height('double');

        foreach ($dishes as $__dish) {
            if ($__dish['quantity'] == 0) continue;

            $dish = $__dish['dish'];
            $dishName = strtr($dish->getDish()->getName(), [' ' => ' ',]);
            $escpos->Line($dishName . ' - ' . abs($__dish['quantity']) . " " . ($__dish['quantity'] < 0 ? "ОТМЕНА" : ""));

            /*if ($printer->getTransliterate()) {
                $dishEntity = $dish->getDish();
                $dishEntity->setTranslatableLocale('it');
                $this->em->refresh($dishEntity);
                $dish_line = ($dishEntity->getName()) . '-' . $dish->getQuantity();
                $escpos->Line($dish_line);
                $escpos->Line("---------------------------------");
            }*/
        }
        $escpos->Line("");
        $escpos->Line("---------------------------------");
        $escpos->Line("");
        $escpos->Cut();

        $escpos->Close();
    }

    private function printOrderHeader($escpos, $order)
    {

        $formatter = new IntlDateFormatter('ru_RU', IntlDateFormatter::FULL, IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM Y г. H:mm');
        $date = $formatter->format(new \DateTime('now'));

        $escpos->Font("A");
        $escpos->Align('center');
        $escpos->Double(true);
        $escpos->Line("Заказ №" . $order->getLocalId());

        $escpos->Align("left");
        $escpos->Height('normal');
        $escpos->Line("Официант: " . $order->getUser()->getName());
        $escpos->Height('double');
        $escpos->Line("Стол: " . $order->getTable()->getName() . " (" . $order->getTable()->getPlace()->getName() . ")");
        $escpos->Line($date);

    }

    public function printAdministratorOrder(Order $order, $type = 1)
    {

//        dump($order);die();
//        $formatter = new \IntlDateFormatter('ru_RU', \IntlDateFormatter::FULL, \IntlDateFormatter::FULL);
//        $formatter->setPattern('d MMMM Y г. H:mm');
//        $date = $formatter->format($order->getCreatedAt());

        $date = $order->getCreatedAt()->format('d.m.Y H:i');

        if (($order->getCashType() > Order::CASHTYPE_ONLINE) || ($type == 0)) {
            $printer = $order->getShop()->getInvoicePrinter();
        } else {
            $printer = $order->getShop()->getAdminPrinter();
        }

        $groupedOrder = $this->groupOrder($order, $type);

        if (!$printer) return;

        try {
            $escpos = new KEscPos($order->getShop()->getId() == 12 ? 'RONGTA' : 'TM-T88IV AFU', $printer, true, true);
        } catch (Exception $ex) {
            return;
        }

        $escpos->Font("A");
        $escpos->Align("center");

        $escpos->Image(false, realpath('static/logo.png'));

        $escpos->Height('double');
        $escpos->Double(true);
        $escpos->Line($order->getShop()->getName());
        $escpos->Height('normal');
        $escpos->Double(false);
        $escpos->Line($order->getShop()->getAddress());
        $escpos->XFeed();

        //$escpos->Line();
        $escpos->Line($this->container->getParameter('shop.organisation'));
        $escpos->Line('224000, г. Брест, ул. Интернациональная, 5');
        $escpos->Line($order->getShop()->getPhone());
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        //$escpos->Line("Счет №" . $order->getId() . ".1");
        $escpos->Line("Счет №" . $order->getLocalId());

        $escpos->Double(false);
        $escpos->Height('normal');
        $escpos->Font("B");
        $escpos->XFeed();
        $escpos->Line($date);
        $escpos->Font("B");
        $escpos->Align("left");
        $escpos->Line(mb_strtoupper("Официант: " . $order->getUser()->getName()));
        $escpos->Line(mb_strtoupper("Стол: " . $order->getTable()->getName() . " (" . $order->getTable()->getPlace()->getName() . ")"));
        if ($order->getClient() != null) {
            $escpos->Line("Клиент: " . $order->getClient()->getName() . " (" . $order->getClient()->getUnp() . ")");
        }
        if ($order->getUser()->getId() == 1 && $order->getMeta() != null) {
            $escpos->Line($order->getMeta()->getComment());
        }

        $escpos->Line("--------------------------------------------------------");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Align('left');
        $escpos->Line("    Наименование             К-во      Цена        Сумма");
        $escpos->Double(false);

        $sum = 0;
        $discount = 0;
        $hasCode = false;
        foreach ($groupedOrder as $dishGroupName => $dishGroup) {
            $escpos->Double(false);
            /**
             * @var OrderDish $dish
             */
            foreach ($dishGroup['dishes'] as $dish) {
                if ($dish->getQuantity() == 0) continue;
                if (!$dish->getOrder()) continue;

                if ($dish->getDish()->getId() == 13703) $hasCode = true;
                $escpos->Align('left');
                $price = (($dish->getDishPrice() + $dish->getDishDiscount()) * $dish->getQuantity());

                $dishName = strtr($dish->getDish()->getName(), [' ' => ' ',]);

                $dish_line = $this->strPad($dishName, 26)
                    . str_pad($dish->getQuantity(), 10, ' ', STR_PAD_BOTH)
                    . str_pad(number_format($dish->getDishPrice() + $dish->getDishDiscount(), 2, '.', ' '), 10, ' ', STR_PAD_BOTH)
                    . str_pad(number_format($price, 2, '.', ' '), 10, ' ', STR_PAD_LEFT);

                $escpos->Line($dish_line);

                $sum += $price;
            }
            $escpos->Double(true);
            $escpos->Align('right');

            if ($dishGroup['sum'] < 0) {
                $dishGroup['sum'] = 0;
            }
            $escpos->Line('Итого ' . $dishGroupName . ': ' . number_format($dishGroup['sum'], 2, '.', ' ') . ' ');

            $discount += $dishGroup['discount'];
        }
        $escpos->XFeed();
        $escpos->Align('right');
        $escpos->Double(true);
        $escpos->Height('normal');
        if ($sum < 0) $sum = 0;
        $escpos->Line('Итого: ' . number_format($sum, 2, '.', ' ') . ' р.');

        if ($order->getPromo()) {
            if ($order->getSumDiscount() > 0) {
                $escpos->Height('normal');
                $escpos->Line('Скидка: ' . $order->getPromo()->getCardDiscount() . "% (" . number_format($discount, 2, '.', ' ') . ' р.)');

                if (!in_array($order->getPromo()->getId(), [1, 3, 4, 1847, 12027])) {
                    $escpos->Line('Сумма ваших накоплений: ' . $order->getPromo()->getCardSum() . ' р.');
                }
            }
        } else {
            if ($discount > 0) {
                $escpos->Height('normal');
                $escpos->Line('Скидка: ' . number_format($discount, 2, '.', ' ') . ' р.');
            }
        }

        $escpos->XFeed();
        $escpos->Double(true);
        $escpos->Height('double');

        $escpos->Align('right');
        $result = $sum - $discount;

        if ($result < 0) $result = 0;
        $escpos->Line('К ОПЛАТЕ: ' . number_format(($result), 2, '.', ' ') . ' р.');

        $escpos->Height('normal');
        $escpos->Double(false);
        $escpos->XFeed();
        $escpos->Font("A");
        $escpos->Double(true);
        $escpos->Align("center");
        $escpos->Line("Спасибо! Мы всегда рады видеть Вас!");
        $escpos->XFeed();
        $escpos->Double(false);
        $escpos->Font("B");
        $escpos->Align("left");
        $escpos->Line("Счет проверил метрдотель");
        $escpos->Line("____________________                        _________");
        $escpos->Line("(подпись официанта)                         (подпись)");
        $escpos->Double(true);
        $escpos->Align("center");
        $escpos->Line("СПАСИБО");
//        dump($escpos);die();


        $this->container->get("qr_code.service")->renderImage($order);
        if ((($order->getPromo() == null) && ($result >= 35) && ($order->getShop()->getId() != 14)) || ($hasCode)) {
            $escpos->Image(false, realpath('qr.png'));
            $validTo = $order->getCreatedAt()->modify("+2 weeks")->format('d.m.Y H:i');
            $escpos->Line("Сканируй QR-код, стань участником дисконтной программы!");
            $escpos->Line("КОД ДЕЙСТВИТЕЛЕН ДО " . $validTo);
        }

        $escpos->Cut();
        $escpos->Close();
    }

    private function groupOrder(Order $order, $type)
    {
        $grouped = [];
        $sum = 0;
        foreach ($order->getDishes() as $orderDish) {
            if (!$orderDish->getOrder()) continue;

            $dish = $orderDish->getDish();
            $groupName = $dish->getCategory()->getParent()->getName();
            if (($type === 0) && ($dish->getCategory()->getParent()->getId() === 22)) {
                $order->setCashType(0);
                $orderDish->setOrder(null);
                $sum += $orderDish->getFullSum();
                continue;
            }
            if (!array_key_exists($groupName, $grouped)) {
                $grouped[$groupName] = [
                    'dishes' => [],
                    'sum' => 0,
                    'discount' => 0,
                ];
            }

            $grouped[$groupName]['dishes'][] = $orderDish;
            $grouped[$groupName]['sum'] += $orderDish->getFullSum();
            $grouped[$groupName]['discount'] += $orderDish->getDiscountSum();
        }
        if ($order->getPromo() != null) {
            $order->getPromo()->addCardSum($sum);
        }


        return $grouped;
    }

    private function strPad($str, $length)
    {
        $strlen = mb_strlen($str);
        $newstr = $str;

        if (mb_strlen($str) > $length) {
            $newstr = mb_substr($str, 0, $length);

        } else {
            $add = $length - $strlen;
            for ($i = 0; $i < $add; $i++) {
                $newstr .= ' ';
            }
        }


        return $newstr;
    }

    public function printDelegatedOrder(Printer $printer, Order $order, $dishes)
    {
        try {
            $escpos = new KEscPos($order->getShop()->getId() == 12 ? 'RONGTA' : 'TM-T88IV AFU', $printer->getName(), true, true);
        } catch (Exception $ex) {
            error_log("Printer not found " . $printer->getName());
            return;
        }

        $formatter = new IntlDateFormatter('ru_RU', IntlDateFormatter::FULL, IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM Y г. H:mm');
        $date = $formatter->format($order->getCreatedAt());

        $escpos->Font("A");
        $escpos->Align("center");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        $escpos->Line("Заказ №" . $order->getLocalId() . ".1");
        $escpos->Line($order->getShop()->getName());

//        $escpos->Double(false);
        $escpos->Height('normal');
        $escpos->Font("B");
        $escpos->XFeed();
        $escpos->Line($date);
        $escpos->Font("B");
        $escpos->Align("left");
        $escpos->Line("Официант: " . $order->getUser()->getName());
        $escpos->Line("Стол: " . $order->getTable()->getName() . " (" . $order->getTable()->getPlace()->getName() . ")");

        $escpos->Height('normal');
        $escpos->Double(false);
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Align('left');
        $escpos->Line("    Наименование                           К-во      ");
        $escpos->Double(false);

        /**
         * @var OrderDish $dish
         */
        foreach ($dishes as $dish) {
            $dishName = strtr($dish->getDish()->getName(), [' ' => ' ',]);
            $dish_line = $this->strPad($dishName, 41) . str_pad($dish->getQuantity(), 10, ' ', STR_PAD_BOTH);
            $escpos->Line($dish_line);
        }

        $escpos->Cut();
        $escpos->Close();


    }

    public function printAdminWebOrder(Order $order, $extra, $ex = 0, $type = 1)
    {

        $formatter = new IntlDateFormatter('ru_RU', IntlDateFormatter::FULL, IntlDateFormatter::FULL);
        $formatter->setPattern('d MMMM Y г. H:mm');
        $date = $formatter->format($order->getCreatedAt());

        $printer = $order->getShop()->getAdminPrinter() ?: $order->getShop()->getInvoicePrinter();

        $groupedOrder = $this->groupWebOrder($order, $type);


        if (!$printer) return;

        try {
            $escpos = new KEscPos($order->getShop()->getId() == 12 ? 'RONGTA' : 'TM-T88IV AFU', $printer, true, true);
        } catch (Exception $ex) {
            return;
        }

        $escpos->Font("A");
        $escpos->Align("center");

        $escpos->Image(false, realpath('static/logo.png'));

        $escpos->Height('double');
        $escpos->Double(true);
        $escpos->Line($order->getShop()->getName());
        $escpos->Height('normal');
//        $escpos->Double(false);
        $escpos->Line($order->getShop()->getAddress());
        $escpos->XFeed();

        //$escpos->Line();
        $escpos->Line($this->container->getParameter('shop.organisation'));
        $escpos->Line('224030, г. Брест, ул. Интернациональная, 5');
        $escpos->Line($order->getShop()->getPhone());
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        //$escpos->Line("Счет №" . $order->getId() . ".1");
        if ($ex == 1) {
            $escpos->Line("Заказ с сайта №" . $extra->id);

        }
        $escpos->Double(false);
        $escpos->Height('normal');
        $escpos->XFeed();
        $escpos->Font("B");
        $escpos->Align("left");
        $escpos->Line('Создан: ' . $date);

        $escpos->Line('Телефон: ' . $extra->phone);
//        $escpos->Line('Адрес: ' . $extra->address);
        $escpos->Line('Комментарий: ' . $extra->comment);
        $escpos->Line('Время: ' . $extra->pickupTime);

        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Height('double');
        if ($order->getCashType() == Order::CASHTYPE_ONLINE) {
            $escpos->Line("ОПЛАЧЕНО ОНЛАЙН");
        } else {
            $escpos->Line("ОПЛАТА НА МЕСТЕ");
        }
        $escpos->Double(false);
        $escpos->Height('normal');
        $escpos->Font("B");

        //$escpos->Line("Официант: " . $order->getUser()->getName());

        $escpos->Line("--------------------------------------------------------");
        $escpos->Font("B");
        $escpos->Double(true);
        $escpos->Align('left');
        $escpos->Line("    Наименование             К-во      Цена        Сумма");
        $escpos->Double(false);

        $sum = 0;
        $discount = 0;
        foreach ($groupedOrder as $dishGroupName => $dishGroup) {
            $escpos->Double(false);
            /**
             * @var OrderDish $dish
             */
            foreach ($dishGroup['dishes'] as $dish) {
                if ($dish->getQuantity() == 0) continue;

                $escpos->Align('left');
                $price = (($dish->getDishPrice() + $dish->getDishDiscount()) * $dish->getQuantity());

                $dishName = strtr($dish->getDish()->getName(), [' ' => ' ',]);

                $dish_line = $this->strPad($dishName, 26)
                    . str_pad($dish->getQuantity(), 10, ' ', STR_PAD_BOTH)
                    . str_pad(number_format($dish->getDishPrice() + $dish->getDishDiscount(), 2, '.', ' '), 10, ' ', STR_PAD_BOTH)
                    . str_pad(number_format($price, 2, '.', ' '), 10, ' ', STR_PAD_LEFT);

                $escpos->Line($dish_line);

                $sum += $price;
            }
            $escpos->Double(true);
            $escpos->Align('right');

            if ($dishGroup['sum'] < 0) {
                $dishGroup['sum'] = 0;
            }
            $escpos->Line('Итого ' . $dishGroupName . ': ' . number_format($dishGroup['sum'], 2, '.', ' ') . ' ');

            $discount += $dishGroup['discount'];
        }
        $escpos->XFeed();

        $escpos->Align('right');
        $escpos->Double(true);
        $escpos->Height('normal');
        if ($sum < 0) $sum = 0;
        $escpos->Line('Итого: ' . number_format($sum, 2, '.', ' ') . ' р.');


        if ($order->getSumDiscount() > 0) {
            $escpos->Height('normal');
            $escpos->Line('Скидка: ' . number_format($discount, 2, '.', ' ') . ' р.');
        }

        $escpos->XFeed();
        $escpos->Double(true);
        $escpos->Height('double');

        $escpos->Align('right');
        $result = $sum - $discount;

        if ($result < 0) $result = 0;
        $escpos->Line('К ОПЛАТЕ: ' . number_format(($result), 2, '.', ' ') . ' р.');


        $escpos->Height('normal');
        $escpos->Double(false);
        $escpos->XFeed();
        $escpos->Font("A");
        $escpos->Double(true);
        $escpos->Align("center");
        $escpos->Line("Спасибо! Мы всегда рады видеть Вас!");
        $escpos->XFeed();
        $escpos->Double(false);
        $escpos->Font("B");
        $escpos->Align("left");
        $escpos->Line("Счет проверил метрдотель");
        $escpos->Line("____________________                        _________");
        $escpos->Line("(подпись официанта)                         (подпись)");
        $escpos->Cut();
        $escpos->Close();
    }

    private function groupWebOrder(Order $order, $type)
    {
        $grouped = [];

        foreach ($order->getDishes() as $orderDish) {
            $dish = $orderDish->getDish();

            $groupName = $dish->getCategory()->getParent()->getName();
            if (($type === 0) && ($dish->getCategory()->getParent()->getId() === 22)) {
                //&& ($order->getCashType() === Order::CASHTYPE_CASH)
                //$order->setStatus(Order::STATUS_CLOSED);
                $orderDish->setOrder(null);
                continue;
            }
            if (!array_key_exists($groupName, $grouped)) {
                $grouped[$groupName] = [
                    'dishes' => [],
                    'sum' => 0,
                    'discount' => 0,
                ];
            }

            $grouped[$groupName]['dishes'][] = $orderDish;
            $grouped[$groupName]['sum'] += $orderDish->getFullSum();
            $grouped[$groupName]['discount'] += $orderDish->getDiscountSum();
        }


        return $grouped;
    }

    private function groupByTime($data)
    {

    }

    private function transliterate($str)
    {
        $cyr = [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
            'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П',
            'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'
        ];
        $lat = [
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
            'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', 'a', 'i', 'y', 'e', 'yu', 'ya',
            'A', 'B', 'V', 'G', 'D', 'E', 'Io', 'Zh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P',
            'R', 'S', 'T', 'U', 'F', 'H', 'Ts', 'Ch', 'Sh', 'Sht', 'A', 'I', 'Y', 'e', 'Yu', 'Ya'
        ];
        return str_replace($cyr, $lat, $str);
        /*
        $textcyr = str_replace($cyr, $lat, $textcyr);
        $textlat = str_replace($lat, $cyr, $textlat);
        echo("$textcyr $textlat");*/
    }


}
