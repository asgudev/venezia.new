<?php

namespace ApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * Class ApiController
 * @package ApiBundle\Controller
 */
class ApiController extends Controller
{

    /**
     * @Route("/recorder", name="recorder")
     */
    public function recorderAction(Request $request)
    {
        $conn = mysqli_connect("localhost", "records", "TmT8t2XwVi77FuLP", "records");

        $data = json_decode($request->getContent(), true);
        $spool_path = $this->get('kernel')->getRootDir() . '/../var/records/';
        $fileName = $data["session"]."_".uniqid().".json";
        $path = $spool_path . $fileName;

        $f = fopen($path, "w+");
        fputs($f, $data["events"]);
        fclose($f);

        $query = "INSERT INTO `records`(`user`, `url`, `session`, `date`, `record_file`) VALUES (".$data["user"].", '".$data["url"]."', '". $data["session"]."', NOW(), '".$fileName."' )";

        mysqli_query($conn, $query);

        return new JsonResponse($fileName);
    }

    /**
     * @Route("/push", name="push")
     */
    public function pushAction(Request $request)
    {
        $this->get('user.logger')->info("AUTH " . $request->headers->get("authorization"));
        $this->get('user.logger')->info("PUSH TOKEN " . $request->getContent());

        return new JsonResponse();
    }

    /**
     * @Route("/chat", name="chat")
     */
    public function chatAction(Request $request)
    {
        $this->get('user.logger')->info("AUTH " . $request->headers->get("authorization"));
        $this->get('user.logger')->info("CHAT " . $request->getContent());

        return new JsonResponse();
    }

}
