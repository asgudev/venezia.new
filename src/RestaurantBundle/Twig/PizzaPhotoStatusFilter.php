<?php

namespace RestaurantBundle\Twig;

use ControlBundle\Entity\DishPhotoCheck;
use RestaurantBundle\Service\BitToArray;

class PizzaPhotoStatusFilter extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('pizza_photo_status', array($this, 'photoStatusFilter')),
        );
    }

    public function photoStatusFilter($value)
    {
        $bta = new BitToArray();
        return $bta->bitToReadableArray($value, DishPhotoCheck::REVIEW_MASK);
    }

    public function getName()
    {
        return 'pizza_photo_status';
    }

}