<?php
namespace RestaurantBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Place
 *
 * @ORM\Table(name="place")
 * @ORM\Entity(repositoryClass="RestaurantBundle\Entity\Repository\PlaceRepository")
 */
class ShopPlace
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop", inversedBy="places")
     */
    private $shop;

    /**
     * @ORM\OneToMany(targetEntity="ShopPlaceTable", mappedBy="place", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "asc"})
     */
    private $tables;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tables = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ShopPlace
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shops
     *
     * @param \RestaurantBundle\Entity\Shop $shops
     *
     * @return ShopPlace
     */
    public function setShops(\RestaurantBundle\Entity\Shop $shops = null)
    {
        $this->shops = $shops;

        return $this;
    }

    /**
     * Get shops
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShops()
    {
        return $this->shops;
    }

    /**
     * Add table
     *
     * @param \RestaurantBundle\Entity\ShopPlaceTable $table
     *
     * @return ShopPlace
     */
    public function addTable(\RestaurantBundle\Entity\ShopPlaceTable $table)
    {
        $this->tables[] = $table;

        return $this;
    }

    /**
     * Remove table
     *
     * @param \RestaurantBundle\Entity\ShopPlaceTable $table
     */
    public function removeTable(\RestaurantBundle\Entity\ShopPlaceTable $table)
    {
        $this->tables->removeElement($table);
    }

    /**
     * Get tables
     *
     * @return ShopPlaceTable[]
     */
    public function getTables()
    {
        return $this->tables;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return ShopPlace
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }
}
