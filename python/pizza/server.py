#!/usr/local/bin/python3.6

import imghdr
import os
import numpy as np
from keras import backend as K
from keras.models import load_model
from PIL import Image
from yad2k.models.keras_yolo import yolo_eval, yolo_head
from recognizer import Recognizer
import yaml
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
import cgi
import time


sess = K.get_session()

with open("model_data/coco_classes.txt") as f:
    class_names = f.readlines()
class_names = [c.strip() for c in class_names]

with open("model_data/yolo_anchors.txt") as f:
    anchors = f.readline()
    anchors = [float(x) for x in anchors.split(',')]
    anchors = np.array(anchors).reshape(-1, 2)

yolo_model = load_model("model_data/yolo.h5")

model_output_channels = yolo_model.layers[-1].output_shape[-1]

model_image_size = yolo_model.layers[0].input_shape[1:3]

yolo_outputs = yolo_head(yolo_model.output, anchors, len(class_names))
input_image_shape = K.placeholder(shape=(2,))
boxes, scores, classes = yolo_eval(
    yolo_outputs,
    input_image_shape,
    score_threshold=0.3,
    iou_threshold=0.5)

recognizer = Recognizer()

class httpRequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD': 'POST',
                     'CONTENT_TYPE': self.headers['Content-Type'],
                     })
        if (type(form['photo'].file).__name__ == 'StringIO'):
            filename = "../web/uploads/photos/" +  form['photo'].value
        else:
            data = form['photo'].file.read()
            filename = "photos/tmp_" + str(int(time.time())) + ".jpg"
            open(filename, "wb").write(data)

        image = Image.open(filename)
        width, height = image.size
        resized_image = image.resize(tuple(reversed(model_image_size)), Image.BICUBIC)
        image_data = np.array(resized_image, dtype='float32')

        image_data /= 255.
        image_data = np.expand_dims(image_data, 0)  # Add batch dimension.

        out_boxes, out_scores, out_classes = sess.run(
            [boxes, scores, classes],
            feed_dict={
                yolo_model.input: image_data,
                input_image_shape: [image.size[1], image.size[0]],
                K.learning_phase(): 0
            })

        pizza = False
        for i, c in reversed(list(enumerate(out_classes))):
            if (c != 53):
                continue
            predicted_class = class_names[c]
            box = out_boxes[i]
            score = out_scores[i]

            top, left, bottom, right = box

            top = max(0, np.floor(top + 0.5).astype('int32'))
            left = max(0, np.floor(left + 0.5).astype('int32'))
            bottom = min(image.size[1], np.floor(bottom + 0.5).astype('int32'))
            right = min(image.size[0], np.floor(right + 0.5).astype('int32'))
            area = (bottom - top) * (right - left)

            if (area > 200000):
                pizza = True
                cropped = image.crop((left, top, right, bottom))

        if pizza:
            cropped = cropped.resize((640, 640), Image.ANTIALIAS)
        else:
            box = [0, int((width-height) / 2), height, int((width+height)/2)]
            cropped = image.resize((640, 640), Image.ANTIALIAS)


        results = recognizer.recognize(cropped)
        sorted_result = sorted(results.items(), key=lambda x: x[1], reverse=True)

        crop_box = np.array(box, dtype='int')

        response_data = {}
        response_data['box'] = crop_box.tolist()
        response_data['predicts'] = sorted_result[:5]

        self.send_response(200)
        self.send_header('Content-type', 'application/json; charset=utf-8')
        self.end_headers()
        self.wfile.write(bytes(str(json.dumps(response_data, ensure_ascii=False)), "utf8"))

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html; charset=utf-8')
        self.end_headers()
        self.wfile.write(bytes("HELLO WORLD", "utf8"))
        return

def run():
    print('starting server...')
    server_address = ('37.139.3.27', 61228)
    httpd = HTTPServer(server_address, httpRequestHandler)
    print('running server...')
    httpd.serve_forever()

run()