<?php

namespace FrontendBundle\Controller;

use RestaurantBundle\Entity\PromoCard;
use RestaurantBundle\Form\Type\PromoCardType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/card")
 */
class CardController extends Controller
{

    /**
     * @Route("/register", name="card_create")
     */
    public function cardCreateAction(Request $request)
    {
        $bv = $this->get('barcode.validator');

        $card = new PromoCard();

        $form =  $this->createForm(PromoCardType::class, $card);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($card);
                $em->flush();

                return $this->render('@Frontend/Card/createCardSuccess.html.twig');
            }
        }

        return $this->render('@Frontend/Card/createCard.html.twig', [
            'form' => $form->createView()
        ]);
    }
}