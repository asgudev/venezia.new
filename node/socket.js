const express = require("express");
const http = require("http");
const socketIo = require("socket.io");

const app = express();
app.use(express.json());
const server = http.createServer(app);
const io = socketIo(server);

const PORT = process.env.PORT || 3000;

class SocketServer {
    clients = {};

    start() {
        this.setup();

        server.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
        });
    }

    setup() {
        app.get("/", (req, res) => {
            res.send("");
        });

        app.get("/status", (req, res) => {
            res.json(Object.keys(this.clients));
        });

        app.post("/command", (req, res) => {
            const {command, target, data} = req.body;

            console.log(`Command to ${target}`, command, data);

            if (this.clients[target]) {
                console.log("sent");
                this.clients[target].socket.emit("message", data);
            }

            res.json({command, target, data});
        });

        io.on("connection", (socket) => {
            const clientIp = socket.handshake.address;

            console.log(new Date(), `Client connected: ${socket.id} ${clientIp}`);

            socket.on("register", (name) => {
                this.clients[name] = {name, socket};
                socket.emit("registered", {id: socket.id, name});
                console.log(new Date(),`Client registered: ${name} (${socket.id}) ${clientIp}`);
            });

            socket.on("message", (req) => {
console.log(JSON.stringify(req));
                const {target, data} = req;
                const targetSocket = this.clients[target]?.socket;

                if (targetSocket) {
                    targetSocket.emit("message", data);
                } else {
                    socket.emit("error", {message: "Target client not found"});
                }
            });

            socket.on("disconnect", () => {
                console.log(new Date(),`Client disconnected: ${socket.id}`);
                delete this.clients[socket.id];
            });
        });
    }
}

const socketServer = new SocketServer();

socketServer.start();
