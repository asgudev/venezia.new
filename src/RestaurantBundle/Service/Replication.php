<?php

namespace RestaurantBundle\Service;

use Doctrine\ORM\EntityManager;

class Replication
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getConnectedSlaves()
    {
        $replication = [
            'slaves' => [],
            'masters' => []
        ];
        return $replication;
        $connection = $this->em->getConnection();
        $slaveStatus = $connection->prepare("SHOW SLAVE HOSTS");
        $slaveStatus->execute();
        $slaves = $slaveStatus->fetchAll();

        foreach ($slaves as $slave) {
            $replication['slaves'][] = $slave['Server_id'];
        }

        $masterStatus = $connection->prepare("SHOW SLAVE STATUS");
        $masterStatus->execute();
        $masters = $masterStatus->fetchAll();

        foreach ($masters as $master) {
            if (($master['Slave_IO_Running'] == 'Yes') && ($master['Slave_SQL_Running'] == 'Yes')) {
                $replication['masters'][] = $master['Master_Server_Id'];
            }
        }

        return $replication;
    }
}
