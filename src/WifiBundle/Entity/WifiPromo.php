<?php

namespace WifiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * WifiPromo
 *
 * @ORM\Table(name="wifi_promo")
 * @ORM\Entity(repositoryClass="WifiBundle\Entity\Repository\WifiPromoRepository")
 */
class WifiPromo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="WifiBundle\Entity\WifiRouter")
     */
    private $router;

    /**
     * @ORM\Column(name="time_start", type="time")
     */
    private $timeStart;

    /**
     * @ORM\Column(name="time_end", type="time")
     */
    private $timeEnd;

    /**
     * @ORM\Column(name="is_deleted", type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\OneToMany(targetEntity="WifiBundle\Entity\WifiConnect", mappedBy="promoSent")
     */
    private $connects;


    public function __construct()
    {
        $this->isDeleted = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return WifiPromo
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set timeStart
     *
     * @param \DateTime $timeStart
     *
     * @return WifiPromo
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd
     *
     * @param \DateTime $timeEnd
     *
     * @return WifiPromo
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd
     *
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set router
     *
     * @param \WifiBundle\Entity\WifiRouter $router
     *
     * @return WifiPromo
     */
    public function setRouter(\WifiBundle\Entity\WifiRouter $router = null)
    {
        $this->router = $router;

        return $this;
    }

    /**
     * Get router
     *
     * @return \WifiBundle\Entity\WifiRouter
     */
    public function getRouter()
    {
        return $this->router;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return WifiPromo
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Add connect
     *
     * @param \WifiBundle\Entity\WifiConnect $connect
     *
     * @return WifiPromo
     */
    public function addConnect(\WifiBundle\Entity\WifiConnect $connect)
    {
        $this->connects[] = $connect;

        return $this;
    }

    /**
     * Remove connect
     *
     * @param \WifiBundle\Entity\WifiConnect $connect
     */
    public function removeConnect(\WifiBundle\Entity\WifiConnect $connect)
    {
        $this->connects->removeElement($connect);
    }

    /**
     * Get connects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConnects()
    {
        return $this->connects;
    }
}
