<?php

namespace TerminalBundle\Entity;

use ControlBundle\Entity\IngredientData;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DishBundle\Entity\Ingredient;
use RestaurantBundle\Entity\Shop;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * DailyReport
 *
 * @ORM\Table(name="daily_report")
 * @ORM\Entity(repositoryClass="TerminalBundle\Entity\Repository\DailyReportRepository")
 */
class DailyReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="ControlBundle\Entity\IngredientData", mappedBy="report", cascade={"persist"})
     * @@ORM\OrderBy({"id" = "ASC"})
     */
    private $ingredients;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Shop")
     */
    private $shop;

    /**
     * @ORM\Column(type="datetime", name="date_started", nullable=true)
     */
    private $dateStarted;

    /**
     * @ORM\Column(type="datetime", name="date_closed", nullable=true)
     */
    private $dateClosed;


    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(name="card_sum", type="decimal", scale=2, nullable=true)
     */
    private $cardSum;

    /**
     * @ORM\Column(name="fiscal_cash_sum", type="decimal", scale=2, nullable=true)
     */
    private $fiscalCashSum;

    /**
     * @ORM\Column(name="fiscal_card_sum", type="decimal", scale=2, nullable=true)
     */
    private $fiscalCardSum;


    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
        $this->ingredients = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add ingredient
     *
     * @param \ControlBundle\Entity\IngredientData $ingredient
     *
     * @return DailyReport
     */
    public function addIngredient(\ControlBundle\Entity\IngredientData $ingredient)
    {
        $this->ingredients[] = $ingredient;
        $ingredient->setReport($this);

        return $this;
    }

    /**
     * Remove ingredient
     *
     * @param \ControlBundle\Entity\IngredientData $ingredient
     */
    public function removeIngredient(\ControlBundle\Entity\IngredientData $ingredient)
    {
        $this->ingredients->removeElement($ingredient);
    }

    /**
     * Get ingredients
     *
     * @return IngredientData[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }


    /**
     * @param $ingrId
     * @return IngredientData|null
     */
    public function getIngredientData($ingrId)
    {
        foreach ($this->ingredients as $ingredient) {
            if ($ingredient->getIngredient()->getId() == $ingrId) {
                return $ingredient;
            }
        }
        return null;
    }

    /**
     * Set shop
     *
     * @param \RestaurantBundle\Entity\Shop $shop
     *
     * @return DailyReport
     */
    public function setShop(\RestaurantBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \RestaurantBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set dateClosed
     *
     * @param \DateTime $dateClosed
     *
     * @return DailyReport
     */
    public function setDateClosed($dateClosed)
    {
        $this->dateClosed = $dateClosed;

        return $this;
    }

    /**
     * Get dateClosed
     *
     * @return \DateTime
     */
    public function getDateClosed()
    {
        return $this->dateClosed;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return DailyReport
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dateStarted
     *
     * @param \DateTime $dateStarted
     *
     * @return DailyReport
     */
    public function setDateStarted($dateStarted)
    {
        $this->dateStarted = $dateStarted;

        return $this;
    }

    /**
     * Get dateStarted
     *
     * @return \DateTime
     */
    public function getDateStarted()
    {
        return $this->dateStarted;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCardSum()
    {
        return $this->cardSum;
    }

    /**
     * @param mixed $cardSum
     * @return DailyReport
     */
    public function setCardSum($cardSum)
    {
        $this->cardSum = $cardSum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiscalCashSum()
    {
        return $this->fiscalCashSum;
    }

    /**
     * @param mixed $fiscalCashSum
     * @return DailyReport
     */
    public function setFiscalCashSum($fiscalCashSum)
    {
        $this->fiscalCashSum = $fiscalCashSum;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiscalCardSum()
    {
        return $this->fiscalCardSum;
    }

    /**
     * @param mixed $fiscalCardSum
     * @return DailyReport
     */
    public function setFiscalCardSum($fiscalCardSum)
    {
        $this->fiscalCardSum = $fiscalCardSum;
        return $this;
    }

    public function getMoney()
    {
        return $this->fiscalCardSum + $this->fiscalCashSum;
    }
}
