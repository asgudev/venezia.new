<?php

namespace RestaurantBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class PizzaPhotoRevisionMonthlyMailCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('photo:revision:monthly')
            ->setDescription('Send monthly pizza photo to check');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $date = '-1 day';
            $doctrine = $this->getContainer()->get('doctrine');
            $renderer = $this->getContainer()->get('templating');

            $dateStart = new \DateTime('0:00 last wednesday');
            $dateEnd = new \DateTime('0:00 this wednesday');

            $userData = $doctrine->getRepository('ControlBundle:DishPhotoCheck')->findUserStats($dateStart, $dateEnd);
            $photoData = $doctrine->getRepository('ControlBundle:DishPhoto')->findPhotoStats($dateStart, $dateEnd);
            $validationData = $doctrine->getRepository('ControlBundle:DishPhoto')->findValidationStats($dateStart, $dateEnd);

            $aiStats = [];
            $photos = $doctrine->getRepository('ControlBundle:DishPhoto')->findAllInRangeStats($dateStart, $dateEnd);

            foreach ($photos as $__photo) {
                $pizza = $__photo->getOrderDish()->getDish()->getName();
                if (!array_key_exists($pizza, $aiStats))
                    $aiStats[$pizza] = [
                        'count' => 0,
                        'aiValid' => 0,
                    ];

                $aiStats[$pizza]['count'] += 1;
                if ($pizza == key($__photo->getPredict()))
                    $aiStats[$pizza]['aiValid'] += 1;
            }

            $pCount = 0;
            $aiValid = 0;
            foreach ($aiStats as $__pizza => $__stat) {
                $aiStats[$__pizza]['percent'] = ($__stat['aiValid'] * 100 / $__stat['count']);
                $pCount += $__stat['count'];
                $aiValid += $__stat['aiValid'];
            }
            $aiRate = [
                'count' => $pCount,
                'aiValid' => $aiValid,
                'percent' => ($aiValid * 100 / $pCount),
            ];


            $mail = $renderer->render("@Restaurant/Custom/pizzaPhotoMailMonthly.html.twig", [
                'userData' => $userData,
                'photoData' => $photoData,
                'validationData' => $validationData,
                'aiStats' => $aiStats,
                'aiRate' => $aiRate,

            ]);


            $message = \Swift_Message::newInstance()
                ->setSubject('Monthly Pizza Photo Revision ' . (new \DateTime($date))->format('d.m.Y'))
                ->setFrom('send@italbrest.com')
                ->setTo([
                    'iprotoss@bk.ru', /*'lena@italbrest.com', 'italservice@mail.ru'*/
                ])
                ->setBody(
                    $mail,
                    'text/html'
                );
            $this->getContainer()->get('mailer')->send($message);


        } catch (\Exception $em) {
            dump($em->getMessage());
            dump($em->getLine());
            die("123");
        }
    }
}