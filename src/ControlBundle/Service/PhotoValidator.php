<?php


namespace ControlBundle\Service;


use ControlBundle\Entity\DishPhoto;
use ControlBundle\Entity\DishPhotoCheck;
use RestaurantBundle\Service\BitToArray;
use Symfony\Component\DependencyInjection\Container;

class PhotoValidator
{
    const MAX_COOKING_TIME = 30 * 60;
    const MIN_INDENTITY_PERCENT = 50;
    const MIN_QUALITY_PERCENT = 50;

    const MAX_UNDERWEIGHT_PERCENT = 5;
    const MAX_OVERWEIGHT_PERCENT = 15;

    private $container;
    private $checkData;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->checkData = [];
        $this->bta = new BitToArray();
    }


    public function shouldBeManualValidated(DishPhoto $dishPhoto)
    {
        $conclusion = [];
        $this->checkData = $this->getCheckData($dishPhoto);

//        if (count($this->checkData) < 2) return [];

        if (!$this->workTimeTest($dishPhoto)) {
            $conclusion[] = 'Большое время (Long cook time)';
        }
        if (!$this->emptyDishTest($dishPhoto)) {
            $conclusion[] = 'Пустое фото (Empty photo)';
        }

        if (!$this->dishWeightTest($dishPhoto)) {

            if ($dishPhoto->getOrderDish()->getDish()->getWeight() != 0) {
                $diff = ($dishPhoto->getWeight() - $dishPhoto->getOrderDish()->getDish()->getWeight());
                $diffPercent = ($diff * 100 / $dishPhoto->getOrderDish()->getDish()->getWeight());

                $conclusion[] = 'Проблема с весом (Weight problem)' . " " . ($diffPercent > 0 ? "+" : "") . number_format($diffPercent, 2, '.', ' ') . "% (" . $diff . "гр)";
            } else {
                var_dump($dishPhoto->getOrderDish()->getDish()->getId());
//die();
            }
        }

        if (!$this->dishIdentityTest($dishPhoto)) {
            $conclusion[] = 'Фото не соответствует блюду (Incorrect dish)';
        }

        if (count($conclusion) > 0) {
            foreach ($this->checkData as $__check) {
                $problems = $this->bta->bitToReadableArray($__check['review'], DishPhotoCheck::REVIEW_MASK);

                foreach ($problems as $__problem) {
                    if (!in_array($__problem, $conclusion))
                        $conclusion[] = $__problem;
                }
            }
        }

//        if (!$this->dishQualityCheck($dishPhoto)) {
//            return true;
//        }

        return $conclusion;
    }


    public function shouldBeAutoValidated(DishPhoto $dishPhoto)
    {
        $this->checkData = $this->getCheckData($dishPhoto);

        echo $dishPhoto->getId() . " ";

        if (!$this->workTimeTest($dishPhoto)) {
            echo "failed workTimeCheck\n";
            return false;
        }

        if (!$this->dishIdentityTest($dishPhoto)) {
            echo "failed dishIdentityCheck\n";
            return false;
        }

        if (!$this->dishWeightTest($dishPhoto)) {
            echo "failed dishIdentityCheck\n";
            return false;
        }

        echo "validated\n";
        return true;
    }

    private function emptyDishTest(DishPhoto $dishPhoto)
    {
        $predicts = $dishPhoto->getTopPredicts();
        //if ($predicts == null) var_dump($dishPhoto->getId());
        $topPredict = @key($predicts);
//        dump($predicts[$topPredict] );
        if (@count($predicts) > 0 && ($topPredict == "Нет") && ($predicts[$topPredict] >= 0.994)) {
            return false;
        }

        return true;
    }

    private function dishQualityTest(DishPhoto $dishPhoto)
    {
        $rate = 0;
        foreach ($this->checkData as $__check) {
            $rate += $__check["rate"];
        }
        if ((count($this->checkData) == 0) || (($rate / count($this->checkData)) <= self::MIN_QUALITY_PERCENT)) {
            return false;
        }

        return true;
    }

    private function dishWeightTest(DishPhoto $dishPhoto)
    {
        if ($dishPhoto->getWeight() != 0) {
            if ($dishPhoto->getWeight() <= $dishPhoto->getOrderDish()->getDish()->getWeight() * (100 - self::MAX_UNDERWEIGHT_PERCENT) / 100) {
                return false;
            }
            if ($dishPhoto->getWeight() >= $dishPhoto->getOrderDish()->getDish()->getWeight() * (100 + self::MAX_OVERWEIGHT_PERCENT) / 100) {
                return false;
            }
        }
        return true;
    }


    private function dishIdentityTest(DishPhoto $dishPhoto)
    {
        // TODO: !!!OPTIMIZE!!!
        $dishName = $dishPhoto->getOrderDish()->getDish()->getName();

        $data = $this->checkData;

        $data[] = [
            "name" => ($dishPhoto->getTopPredicts() == null ?: key($dishPhoto->getTopPredicts()))
        ];

        $valid = 0;
        foreach ($data as $__check) {
            if ($dishName == $__check["name"]) $valid += 1;
        }

        $validness = $valid * 100 / count($data);
        //echo $validness . "% ";

        if (($validness <= self::MIN_INDENTITY_PERCENT) || (count($data) < 2)) {
            return false;
        }

        return true;
    }

    private function workTimeTest(DishPhoto $dishPhoto)
    {
if ($dishPhoto->getOrderDish()->getParentOrder() == null) {
var_dump($dishPhoto->getOrderDish()->getId());
//die();
}
        $workTime = $dishPhoto->getDate()->getTimestamp() - $dishPhoto->getOrderDish()->getParentOrder()->getCreatedAt()->getTimestamp();

        if ($workTime > self::MAX_COOKING_TIME)
            return false;

        return true;
    }

    private function getCheckData(DishPhoto $dishPhoto)
    {
        $checkData = [];
        foreach ($dishPhoto->getChecks() as $check) {
            $checkData[] = [
                "name" => $check->getDish() != null ? $check->getDish()->getName() : "Нет",
                "rate" => $check->getRate(),
                "review" => $check->getReview(),
            ];
        }

        return $checkData;
    }

}
