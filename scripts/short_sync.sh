#!/usr/bin/env bash
BACKUP_DIR=$(date '+%Y-%m-%d')
mkdir $BACKUP_DIR

mysqldump -uvenezia -p9WhsFYfbQjdQz7uR  venezia_new > $BACKUP_DIR/backup.sql &
mysqldump -uvenezia -p9WhsFYfbQjdQz7uR  venezia_new  order_dish_photo > $BACKUP_DIR/order_dish_photo.sql --replace --set-gtid-purged=OFF --no-create-info --lock-tables=false --skip-triggers --quick & 
mysqldump -uvenezia -p9WhsFYfbQjdQz7uR  venezia_new --ignore-table=venezia_new.ingredient_data --ignore-table=venezia_new.orders --ignore-table=venezia_new.order_dish --ignore-table=venezia_new.daily_report --ignore-table=venezia_new.ingridient_data --ignore-table=venezia_new.order_changes --ignore-table=venezia_new.order_dish_photo --ignore-table=venezia_new.order_dish_photo_check --ignore-table=venezia_new.order_changes --ignore-table=venezia_new.wifi_client --ignore-table=venezia_new.wifi_connect --ignore-table=venezia_new.wifi_promo --ignore-table=venezia_new.wifi_router > $BACKUP_DIR/data_master.sql --replace --set-gtid-purged=OFF --no-create-info --lock-tables=false --skip-triggers --quick &
wait

#./__sync_orders.sh 10.8.0.2  tnSdZaZzr4NF2JNruu6ND6amDnQ2qzbC venezia     $BACKUP_DIR &
#./__sync_orders.sh 10.8.0.16 YtfhBx5sTH73vdSJeAxrueJRqneyMGGT italia      $BACKUP_DIR &
#./__sync_orders.sh 10.8.0.10 pYAyDL8CUsgEjNpRn4AFwTuCJtVAs6Kg metromilano $BACKUP_DIR &
./__sync_orders.sh 10.8.0.8  ZePMxrYsKV5qS7pC6qprQFXPq8czHtH7 urban       $BACKUP_DIR &

#./__sync_orders.sh 10.8.0.12 zVLcdUFREM62TH42h47ndUuLxXW6yKjQ rechica     $BACKUP_DIR &
#./__sync_orders.sh 10.8.0.6  L3jpyZZJcVCysvn7e6eWL7KkZSnCmUEt sov55       $BACKUP_DIR &
#./__sync_orders.sh 10.8.0.24 HR2EqPTCLYgPDjb78Sahw8hNBQNv4ZE5 fusion1     $BACKUP_DIR &
#./__sync_orders.sh 10.8.0.20 vtkmJf78DXPKgXNNhd3YrUEVZvwuw65x vostok24    $BACKUP_DIR &

#./__sync_orders.sh 10.8.0.22 QMLvRDtE4aM6ZxfVDaKZR7NjACfVFCQp draft       $BACKUP_DIR &
#./__sync_orders.sh 10.8.0.26 v2RKsLBvEUF4U4HXhaVbu25mBSGFUTge grancafe    $BACKUP_DIR &
#./__sync_orders.sh 10.8.0.14 XGrGM2xcpPk2hqTzwtX5GJ5kaZywkTSk vokzal    $BACKUP_DIR &

wait
./repl.sh
mysql -uvenezia -p9WhsFYfbQjdQz7uR  venezia_new < $BACKUP_DIR/order_dish_photo.sql &

echo "FIN"
